# SynBad - A RDF-based genetic design framework

## Overview

SynBad is a graph-based Java Framework for the design and modeling of genetic circuits, based on RDF, SBOL and SVPs. Data are stored in an RDF triple store, and parsed into SBOL-derived domain models. The SVP domain model allows for the composition and simulation of genetic circuits from simple encapsulated building blocks.

### Modules

* synbad-core
  - Includes modules for Graph, Workspace, Traversal, Views and SBOL APIs.
* synbad-svp
  - Includes extension modules for working with SVPs, including a flow-based model extension, virtual parts repository API and SBML compiler and simulator.
* synbad-ea
  - Includes modules for the Evolutionary Algorithm framework, notably synbad-ea-svp-synbad, for working with the EAs using the SynBad SVP data model.
* synbad-ui
  - Work in progress modules for a SynBad CAD UI built on the Netbeans Platform.
* synbad-examples
  - A number of examples that demonstrate the use of the SynBad Framework APIs

## Dependency Installation

1. Install Newcastle University JARs
2. Install COPASI JAR

### ICOS JARs

JARs are provided in the dependencies folder, and can be installed on *nix systems by running `install-dependencies.sh`  in the same directory.

Otherwise, the commands in the script can be used to install the JARs on Windows.

### COPASI JARs

Download the Java Language Bindings for your platform at http://copasi.org/Download. Synbad currently used version 4.16.104. Install using the command below.

```mvn install:install-file -Dfile=copasi.jar -DgroupId=org.copasi -DartifactId=copasi -Dversion=4.16.104 -Dpackaging=jar```

## Build instructions

Navigate to the project's root directory and run:

```mvn clean install```