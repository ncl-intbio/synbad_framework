/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.propertiespanel;

import java.awt.GridBagLayout;
import java.util.Collection;
import javax.swing.JPanel;

/**
 *
 * To implement a property panel, extend PropertyPanel.Adapter<T>, typed for the
 * Objects whose properties which will be displayed.
 * 
 * Annotate the class with @ServiceProvider(service=PropertyPanel.class), and pass
 * a class object to the super constructor.
 * 
 * @author owengilfellon
 */
public interface SBPropertiesPanel  {

    public Class getPanelClass();
    public String getDisplayName();

    public abstract class ASBPropertiesPanel<T> extends JPanel implements SBPropertiesPanel
    {

        //private Lookup.Result<T> result = null;
        private Collection<? extends T> instances = null;
        private SBPropertiesTopComponent container = null;
        protected GridBagLayout layout = null;
        private final Class<T> clazz;

        public ASBPropertiesPanel(Class<T> clazz) {
            //result = Utilities.actionsGlobalContext().lookupResult(clazz);
            //result.addLookupListener(this);
            layout = new GridBagLayout();
            this.setLayout(new GridBagLayout());
            this.clazz = clazz;
        }

        @Override
        public Class getPanelClass() {
            return clazz;
        }

     /*   @Override
        public void setContainer(SBPropertiesTopComponent container) {
            this.container = container;
        }
        
        @Override
        public void requestOpen() {
            container.requestOpen(this);
        }

        @Override
        public void resultChanged(LookupEvent ev) {
            instances = result.allInstances();
            if(!instances.isEmpty()) {
                hasResult(instances);
                requestOpen();
            }
        }*/

        /**
         * Do something with the search result (i.e. display properties)
         * @param instances 
         */
        public abstract void hasResult(Collection<? extends T> instances);
        
        public abstract String getDisplayName();
    }
}
