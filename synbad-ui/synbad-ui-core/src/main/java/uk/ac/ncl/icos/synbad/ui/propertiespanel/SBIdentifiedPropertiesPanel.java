/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.propertiespanel;

import java.awt.GridBagConstraints;
import java.util.Collection;
import org.openide.util.LookupEvent;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=SBPropertiesPanel.class)
public class SBIdentifiedPropertiesPanel extends SBPropertiesPanel.ASBPropertiesPanel<SBTopLevelNode> {
   
    private final PartPropertiesPanel panel;

    public SBIdentifiedPropertiesPanel() {
        super(SBTopLevelNode.class);
        panel = new PartPropertiesPanel();
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.BOTH;
        add(panel, c);
    }

    @Override
    public void hasResult(Collection<? extends SBTopLevelNode> instances) {
        SBTopLevelNode node = instances.iterator().next();
        panel.setPart(node);
    }

    @Override
    public String getDisplayName() {
        return "Part Properties";
    }
}
