/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.workspace;

import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public class PropertiedNode extends AbstractNode {

    public PropertiedNode(Children children, Lookup lookup) {
        super(children, lookup);
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getName() {
        return getLookup().lookup(SBIdentified.class).getName();
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return getName();
    }
    
    
    
    

    @Override
    public String getHtmlDisplayName() {
        return getName();
    }
    
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
       
        try {
            final SBIdentified propertied = getLookup().lookup(SBIdentified.class);

            set.put(new PropertySupport.ReadOnly<URI>("identity", URI.class, "Identity", "The entity's globally unique identifier.") {
                @Override
                public URI getValue() throws IllegalAccessException, InvocationTargetException {
                    return propertied.getIdentity();
                }
            });
            
            set.put(new PropertySupport.ReadOnly<URI>("persistentIdentity", URI.class, "Persistent Identity", "Persistent Identity") {
                @Override
                public URI getValue() throws IllegalAccessException, InvocationTargetException {
                    return propertied.getPersistentId();
                }
            });
            
            set.put(new PropertySupport.ReadOnly<String>("version", String.class, "Version", "The version of the entity.") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return propertied.getVersion();
                }
            });
            
            set.put(new PropertySupport.ReadOnly<String>("displayId", String.class, "Display ID", "The display identity based on the identity.") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return propertied.getDisplayId();
                }
            });

            Node.Property name = new PropertySupport.Reflection(propertied, String.class, "name");
            name.setName("name");
            name.setDisplayName("Name");
            name.setShortDescription("The entity's name");
            name.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(name);
            
            Node.Property description = new PropertySupport.Reflection(propertied, String.class, "description");
            description.setName("description");
            description.setDisplayName("Description");
            description.setDisplayName("The entity's description.");
            description.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(description);

            sheet.put(set);
            
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return sheet;
    }
    
    
    @Override
    public boolean canDestroy() {
        return true;
    }   


}