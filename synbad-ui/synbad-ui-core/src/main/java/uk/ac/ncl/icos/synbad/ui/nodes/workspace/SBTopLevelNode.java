/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.workspace;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;

public class SBTopLevelNode extends SBEntityNode  {

    public SBTopLevelNode(SBIdentified definition, Children children, Lookup lookup) {
        super(children, lookup);
       /* try {
            Sheet sheet = new Sheet();
            Set set = new Sheet.Set();
            set.put(new PropertySupport.Reflection<>( definition, String.class, "name"));
            set.put();
            
            set.put(new PropertySupport.ReadOnly<String>("version", String.class, "Version", "Version") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return definition.getVersion();
                }
            });
            set.put(new PropertySupport.Reflection<>( definition, String.class, "description"));
            setSheet(sheet);
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }*/
    }
    
    public SBTopLevelNode(SBIdentified definition) {
        super(Children.LEAF, Lookups.fixed(definition));
       /* try {
            Sheet sheet = new Sheet();
            Set set = new Sheet.Set();
            set.put(new PropertySupport.Reflection<>( definition, String.class, "name"));
            set.put();
            
            set.put(new PropertySupport.ReadOnly<String>("version", String.class, "Version", "Version") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return definition.getVersion();
                }
            });
            set.put(new PropertySupport.Reflection<>( definition, String.class, "description"));
            setSheet(sheet);
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }*/
    }

    @Override
    public String getName() {
        return "[TopLevel]: " + super.getName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        try {
            Property nameProp = new PropertySupport.Reflection(getLookup().lookup(SBIdentified.class), String.class, "getName", null);
            nameProp.setName("name");
            nameProp.setDisplayName("Name");
            nameProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(nameProp);
            
            Property parameterTypeProp = new PropertySupport.Reflection(getLookup().lookup(SBIdentified.class), String.class, "getDescription", null);
            parameterTypeProp.setName("description");
            parameterTypeProp.setDisplayName("Description");
            parameterTypeProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(parameterTypeProp);

            Property displayIdProp = new PropertySupport.ReadOnly<String>("getDisplayId", String.class, "Display ID", "Display ID") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return getLookup().lookup(SBIdentified.class).getDisplayId();
                }
            };
            displayIdProp.setName("displayId");
            displayIdProp.setDisplayName("Display ID");
            displayIdProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(displayIdProp);
     
            Property versionProp = new PropertySupport.ReadOnly<String>("getVersion", String.class, "Version", "Version") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return getLookup().lookup(SBIdentified.class).getVersion();
                }
            };
            versionProp.setName("version");
            versionProp.setDisplayName("Version");
            versionProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(versionProp);

        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        

        sheet.put(set);
        return sheet;

    }

    
    
    
    
    
    
    @Override
    public Action getPreferredAction() {
        return new AbstractAction("Open view") {
            @Override
            public void actionPerformed(ActionEvent e) {   
                /*
                IEntity definition = getLookup().lookup(IEntity.class);
                VModel model = new VModelImpl(definition,  definition.getWorkspace());
                EntityViewProvider provider = Lookup.getDefault().lookup(EntityViewProvider.class);                   
                TopComponent window = provider.getView(model.getDefaultView());
                window.open();
                window.requestActive();*/
            }
        };
    }   

/*    public static class DefinitionChildFactory extends ChildFactory<SBInstance> implements SBSubscriber {

        private final TopLevel definition;
       

        public DefinitionChildFactory(TopLevel definition) {
            this.definition = definition;

//            workspace.getDispatcher().registerHandler(WorkspaceEvent.InstanceEvent.class , this);
 //           workspace.getWorkspace().getDispatcher().addFilter(entity.getIdentity(), this);
        }

        @Override
        protected synchronized boolean createKeys(List<SBInstance> toPopulate) {
            return true;
        }

        @Override
        protected Node createNodeForKey(SBInstance key) {
            return new SBNestedNode(key);
        }
        
        
        
        @Override
        public void onEvent(SBEvent e) {
            //refresh(true);
        }


    }*/
}
