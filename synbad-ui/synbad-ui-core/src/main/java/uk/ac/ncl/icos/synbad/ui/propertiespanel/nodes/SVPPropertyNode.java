/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.propertiespanel.nodes;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.util.List;
import javax.swing.Action;
import org.openide.actions.NewAction;
import org.openide.actions.PasteAction;
import org.openide.actions.ReorderAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

/**
 *
 * @author owengilfellon
 */
public class SVPPropertyNode extends AbstractNode {
    
    private final uk.ac.ncl.intbio.virtualparts.entity.Property property;
    
    public SVPPropertyNode(uk.ac.ncl.intbio.virtualparts.entity.Property property) {     
        super(Children.LEAF, new ProxyLookup(Lookups.fixed(property)));
        this.property = property;
        String s = property.getName();
        setName(s);
        setDisplayName(s);
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        return getName();
    }
 
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
 
        try {
            Property valueProp = new PropertySupport.Reflection(property, String.class, "getValue", null);
            valueProp.setName("value");
            valueProp.setDisplayName("Value");
            valueProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(valueProp);

            Property interactionType = new PropertySupport.Reflection(property, String.class, "getDescription", null);
            interactionType.setName("description");
            interactionType.setDisplayName("Description");
            interactionType.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(interactionType);

        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }

        sheet.put(set);
        return sheet;
    }
    
    @Override
    public Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    protected void createPasteTypes(Transferable t, List s) {
        super.createPasteTypes(t, s);
        PasteType paste = getDropType( t, DnDConstants.ACTION_COPY, -1 );
        if( null != paste )
            s.add( paste );
    }
    
    @Override
    public Action[] getActions(boolean context) {
        return new Action[] {
            SystemAction.get( NewAction.class ),
            SystemAction.get( PasteAction.class ),
            SystemAction.get( ReorderAction.class)};
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   
}