/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.model;

import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;

public class SBVEdgeNode extends PropertiedNode  {

    public SBVEdgeNode(Children children, Lookup lookup) {
        super(children, lookup);
        SBViewEdge e = getLookup().lookup(SBViewEdge.class);
        setName(e.getEdge().toString());       
    }
    
    public SBVEdgeNode(SBViewEdge edge) {
        this(Children.LEAF, Lookups.fixed(edge));
    }

    @Override
    public String getName() {
        SBViewEdge e = getLookup().lookup(SBViewEdge.class);
        return e.getFrom().toString() + " to " + e.getTo().toString();
    }
}
