/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.model;

import java.awt.Image;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.view.SBView;

/**
 *
 * @author owengilfellon
 */
public class SBViewNode extends AbstractNode {

    
    public SBViewNode(SBView view) {
        super(Children.LEAF, Lookups.singleton(view));        
        setName(view.toString());
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        return "[View]: " + getName();
    }
    /*
    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        final SBView view = getLookup().lookup(SBView.class);
        set.put(new PropertySupport.ReadOnly<Integer>("id", Integer.class, "ID", "View ID") {
            
            @Override
            public Integer getValue() throws IllegalAccessException, InvocationTargetException {
                return new Integer(view.getViewId());
            }
        });
        sheet.put(set);
        return sheet;
    }

    
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
   
    @Override
    public Action[] getActions(boolean context) {
        List<? extends Action> actions = Utilities.actionsForPath("Actions/ModuleNode");
        return actions.toArray( new Action[actions.size()] );
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   



    @Override
    public Action getPreferredAction() {
        return new AbstractAction("Open view") {
            @Override
            public void actionPerformed(ActionEvent e) {    
                SBView view = getLookup().lookup(SBView.class);
                EntityViewProvider provider = Lookup.getDefault().lookup(EntityViewProvider.class);                   
                TopComponent window = provider.getView(view);
                window.open();
                window.requestActive();
            }
        };
    } */  
}
