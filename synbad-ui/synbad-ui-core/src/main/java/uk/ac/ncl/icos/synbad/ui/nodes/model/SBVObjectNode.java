/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.model;

import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;

public class SBVObjectNode extends PropertiedNode  {

    public SBVObjectNode(Children children, Lookup lookup) {
        super(children, lookup);
        SBViewIdentified e = getLookup().lookup(SBViewIdentified.class);
        setName(e.getDisplayId());
        //setName(!e.getObject().getName().isEmpty() ? e.getObject().getName() : e.getObject().getDisplayId());
        super.setIconBaseWithExtension(getIconBase());   
    }
    
    public SBVObjectNode(SBViewIdentified entity) {
        this(Children.LEAF, Lookups.fixed(entity));
    }
    
    private boolean hasRole(SBValued entity, Role role) {
        return entity.getValues(SynBadTerms.Sbol.role).stream()
                .filter(v -> v.isURI())
                .anyMatch(v -> v.asURI().equals(role.getUri()));
    }


    private String getIconBase()
    {
        SBValued entity = getLookup().lookup(SBValued.class);

        if(hasRole(entity, ComponentRole.Promoter)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/promoter16.png"; 
        } else if (hasRole(entity, ComponentRole.Promoter)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/operator16.png";
        } else if (hasRole(entity, ComponentRole.RBS)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/rbs16.png";
        } else if (hasRole(entity, ComponentRole.CDS)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/cds16.png";
        } else if (hasRole(entity, ComponentRole.Terminator)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/terminator16.png";
        } else if (hasRole(entity, ComponentRole.Shim)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/shim16.png";
        } 
        
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";

    }
    
    /*
    @Override
    protected Sheet createSheet() {
        super.createSheet();        
        Sheet sheet = getSheet();
        Sheet.Set set = Sheet.createPropertiesSet();
        SBEntity entity = getLookup().lookup(SBEntity.class);
        try {
            Property idProp = new PropertySupport.Reflection<>(entity, URI.class, "getIdentity");
            idProp.setName("Identity");
            idProp.setShortDescription("The entity's globally unique identifier.");
           // Property displayIdProp = new PropertySupport.Reflection<>(entity, String.class, "getDisplayId");
           // displayIdProp.setName("Display Identity");
          //  displayIdProp.setShortDescription("The display identity based on the identity");
            Property versionProp = new PropertySupport.Reflection<>(entity, String.class, "getVersion");
            versionProp.setName("Version");
            versionProp.setShortDescription("The version of the entity");
            Property descriptionProp = new PropertySupport.Reflection<>(entity, String.class, "getDescription", "setDescription");
            descriptionProp.setName("Version");
            descriptionProp.setShortDescription("The version of the entity");

            set.put(idProp);
           // set.put(displayIdProp);
            set.put(versionProp);
            set.put(descriptionProp);
            
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        sheet.put(set);
        return sheet;
    }*/
   
/*
    @Override
    public Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    protected void createPasteTypes(Transferable t, List s) {
        super.createPasteTypes(t, s);
        PasteType paste = getDropType( t, DnDConstants.ACTION_COPY, -1 );
        if( null != paste )
            s.add( paste );
    }
    
    @Override
    public Action[] getActions(boolean context) {
        
        List<? extends Action> actions = Utilities.actionsForPath("Actions/SBEntityNode");
        return actions.toArray( new Action[actions.size()] );
        
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    } 
    

    
    public static class InstanceChildFactory extends ChildFactory<PortRecord> {

        private final SBEntity instance;
       

        public InstanceChildFactory(SBEntity instance) {
            this.instance = instance;
        }

        @Override
        protected synchronized boolean createKeys(List<PortRecord> toPopulate) {
           return true;
        }

        @Override
        protected Node createNodeForKey(PortRecord key) {
            return new PortNode(key);
        }


    }*/
}
