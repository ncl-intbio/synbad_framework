/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui;

import java.awt.Color;
import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import org.netbeans.swing.outline.DefaultOutlineCellRenderer;
import org.openide.nodes.Node.Property;
import org.openide.util.Exceptions;

/**
 *
 * @author owengilfellon
 */
public class CellRenderer extends DefaultOutlineCellRenderer {
  
    @Override
    public Component getTableCellRendererComponent(final JTable table,
                                                   final Object value,
                                                   final boolean isSelected,
                                                   final boolean hasFocus,
                                                   final int row,
                                                   final int column) {

        Component cell = null;
        Object valueToDisplay = value;
        if (value instanceof Property) {

            try {
                valueToDisplay = ((Property) value).getValue();
            } catch (IllegalAccessException ex) {
                Exceptions.printStackTrace(ex);
            } catch (InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
            }

        }
        if (valueToDisplay != null) {
          TableCellRenderer renderer = table.getDefaultRenderer(valueToDisplay.getClass());
          if (renderer != null) {
             cell = renderer.getTableCellRendererComponent(table, valueToDisplay, isSelected,
                                                           hasFocus, row, column);
          }
        } else {
             cell = super.getTableCellRendererComponent(table, valueToDisplay, isSelected, hasFocus, row, column);
        }
        if (cell != null) {

           Color foregroundColor = Color.black;
           cell.setForeground(foregroundColor);

        }
        return cell;
    }


    
}
