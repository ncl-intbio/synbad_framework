/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.project.obj;


import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.stream.Collectors;
import org.netbeans.spi.actions.AbstractSavable;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonFullExporter;

/**
 *
 * @author owengilfellon
 */
public class SBProjecttSaveable extends AbstractSavable {
        
    private final SBProjectDataObject obj;
    
    public SBProjecttSaveable(SBProjectDataObject obj) {
        this.obj = obj;
        register();
    }

    @Override
    protected String findDisplayName() {
       return obj.getName();
    }

    @Override
    protected void handleSave() throws IOException {
        OutputStream stream = obj.getPrimaryFile().getOutputStream();
        SBJsonFullExporter e = new SBJsonFullExporter();
        SBWorkspaceManager service = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        Set<SBWorkspace> workspaces = service.listWorkspaces().stream().map(ws -> service.getWorkspace(ws)).collect(Collectors.toSet());
        e.export(stream, workspaces);
        stream.close();
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof SBProjectDataObject) {
            return ((SBProjectDataObject)obj).equals(this.obj);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return obj.hashCode();
    }
} 
