/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.project.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.RequestProcessor;
import uk.ac.ncl.icos.synbad.ui.services.SBProjectService;
import uk.ac.ncl.icos.synbad.ui.project.obj.SBProjectDataObject;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.browser.actions.openProject"
)
@ActionRegistration(
        displayName = "#CTL_openProject"
)
@ActionReference(path = "Menu/File", position = 100)
@Messages("CTL_openProject=Open Project")
public final class OpenProject implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
         
        try {
            File home = new File(System.getProperty("user.home"));
            
            File toAdd = new FileChooserBuilder("user-dir").setTitle("Open Project").
                setDefaultWorkingDirectory(home)
                .setFileFilter(new FileNameExtensionFilter("SynBad Projects", "synbad"))
                .setFilesOnly(true)
                .setApproveText("Open")
                .showOpenDialog();
            
            if (toAdd == null) { return; }
            
            final DataObject obj = DataObject.find(FileUtil.toFileObject(toAdd));
            
            if(obj == null || !(obj instanceof SBProjectDataObject)) { System.out.println("NOT A PROJECT!"); return; }
            
            RequestProcessor.getDefault().post(() -> {
                try (ProgressHandle progress =  ProgressHandle.createHandle("Opening project")) {
                    progress.start();
                    progress.progress( "Opening project..." );
                    SBProjectDataObject project = (SBProjectDataObject) obj;
                    SBProjectService projectService = Lookup.getDefault().lookup(SBProjectService.class);
                    projectService.openProject(project);
                    progress.progress( "...done" );
                    progress.finish();
                }
            });
            
        } catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        }
    } 
}
