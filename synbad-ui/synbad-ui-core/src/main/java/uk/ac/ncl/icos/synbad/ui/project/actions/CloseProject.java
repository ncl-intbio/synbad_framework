/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.project.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import uk.ac.ncl.icos.synbad.ui.project.obj.SBProjectDataObject;
import uk.ac.ncl.icos.synbad.ui.services.SBProjectService;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.browser.actions.closeProject"
)
@ActionRegistration(
        displayName = "#CTL_closeProject"
)
@ActionReference(path = "Menu/File", position = 150)
@Messages("CTL_closeProject=Close Project")
public final class CloseProject implements ActionListener {


    public CloseProject(SBProjectDataObject context) {

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        SBProjectService projectService = Lookup.getDefault().lookup(SBProjectService.class);
        projectService.closeProject();
    } 

    
}
