package uk.ac.ncl.icos.synbad.ui.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;


/**
 *
 * @author owengilfellon
 */
public interface SBEditorProvider<T extends SBIdentified> {
    
    public TopComponent getEditor(T object);
    
    public String getName();
    
}
 