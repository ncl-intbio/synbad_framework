/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.model;

import java.awt.Image;
import java.io.IOException;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.model.SBModelWithView;
import uk.ac.ncl.icos.synbad.view.SBView;

/**
 *
 * @author owengilfellon
 */
public class SBModelWithViewsNode extends AbstractNode {

    public SBModelWithViewsNode(SBModelWithView model) {
        super(Children.create(new ViewFactory(model), true), Lookups.singleton(model));        
        setName(model.toString());
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getHtmlDisplayName() {
        return getName();
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }  
    
    @Override
    public void destroy() throws IOException {
        super.destroy();
    }
    
    public static class ViewFactory extends ChildFactory<SBView> implements SBSubscriber {

        private final SBModelWithView model;
        
        public ViewFactory(SBModelWithView model) {
            this.model = model;
        }

        @Override
        protected synchronized boolean createKeys(List<SBView> toPopulate) {
            toPopulate.addAll(model.getViews());
            return true;
        }

        @Override
        protected Node createNodeForKey(SBView key) {
            return new SBViewNode(key);
        }
        
        @Override
        public void onEvent(SBEvent e) {
            this.refresh(true);
        }
    }
}
