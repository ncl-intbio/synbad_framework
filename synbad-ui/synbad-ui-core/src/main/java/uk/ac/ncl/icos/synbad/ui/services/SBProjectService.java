/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.apache.commons.io.IOUtils;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.ui.project.obj.SBProjectDataObject;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.io.SBWorkspaceExport;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonImporter;

/**
 *
 * @author owengilfellon
 */
public interface SBProjectService  extends Lookup.Provider {
    
    public void openProject(SBProjectDataObject project);
    
    public void closeProject();
    
    public SBProjectDataObject getProject();
    
    public boolean hasOpenProject();
    
    @ServiceProvider(service = SBProjectService.class)
    public class DefaultProjectManagerProvider implements SBProjectService {
        
        private final InstanceContent content;
        private final AbstractLookup lookup;

        public DefaultProjectManagerProvider() {
            this.content = new InstanceContent();
            this.lookup = new AbstractLookup(content);
        }

        @Override
        public SBProjectDataObject getProject() {
            return lookup.lookup(SBProjectDataObject.class);
        }

        @Override
        public boolean hasOpenProject() {
            return getProject() != null;
        }
  
        @Override
        public void openProject(SBProjectDataObject project) {  
            
            closeProject();
            
            SBWorkspaceManager workspaceManager = Lookup.getDefault().lookup(SBWorkspaceManager.class);
            SBWorkspaceExport workspaceExport = project.getExport();
            SBJsonImporter jsonImporter = new SBJsonImporter();
            
            for(SBWorkspaceExport.WorkspaceJson ws : workspaceExport.getWorkspaces()) {
                
                // Create workspace
                
                SBWorkspace workspace = workspaceManager.getWorkspace(URI.create(ws.getUri()));
                
                // Import each context into workspace
                
                for(SBWorkspaceExport.ContextJsonPair context : ws.getContexts()) {
                    try {
                        InputStream inStream = IOUtils.toInputStream(context.getJson().toString(), "UTF-8");
                        jsonImporter.importWs(inStream, workspace, new URI[] { URI.create(context.getUri())});
                        if(workspaceManager.getCurrentWorkspace() == null)
                            workspaceManager.setCurrentWorkspace(workspace.getIdentity());
                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }
            
            // Add project data object to Lookup
            
            content.add(project);
        }
        
        @Override
        public void closeProject() {
            
            SBProjectDataObject project = getLookup().lookup(SBProjectDataObject.class);
            
            if(project == null)
                return;
            
            // TO-DO: prompt to save project?
            
            SBWorkspaceManager wsService = Lookup.getDefault().lookup(SBWorkspaceManager.class);
            
            for(URI uri : wsService.listWorkspaces()) {
                wsService.closeWorkspace(uri);
            }
            
            content.remove(project);
        }
        

        @Override
        public Lookup getLookup() {
            return lookup;
        }
    }
    
}
