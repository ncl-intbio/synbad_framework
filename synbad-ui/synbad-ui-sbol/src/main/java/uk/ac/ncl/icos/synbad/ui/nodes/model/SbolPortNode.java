/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.nodes.model;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;

import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SbolPortNode extends PropertiedNode  {

    private final SBViewIdentified owner;
    private final SBViewIdentified port;
    
    public SbolPortNode(SBPortDirection direction, String name, SBViewIdentified owner, SBViewIdentified port) {
        super(Children.LEAF, Lookups.fixed(direction, port));
        setName(name);
        super.setIconBaseWithExtension(getIconBase());
        this.owner = owner;
        this.port = port;
    }

    public SBViewIdentified getOwner() {
        return owner;
    }
    
    public SBViewIdentified getPort() {
        return port;
    }
    
    /*
    
    public SBVPortNode(SBViewPort instance, SBView view) {
        super(Children.LEAF, Lookups.fixed(instance, instance.getData(), view));
        setName(instance.getData().toString());
        super.setIconBaseWithExtension(getIconBase());
    }*/

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public PasteType getDropType(Transferable t, final int action, int index) {
        final Node dropNode = NodeTransfer.node( t, DnDConstants.ACTION_COPY_OR_MOVE+NodeTransfer.CLIPBOARD_CUT );
        /*
        if( null != dropNode ) {
            
            final GRNTreeNode node = dropNode.getLookup().lookup(GRNTreeNode.class);
            
            if( node instanceof LeafNode && null != node  && !this.equals( dropNode.getParentNode() )) {
                
                return new PasteType() {
                    
                    @Override
                    public Transferable paste() throws IOException {
                        getChildren().add(new Node[] { new SVPModuleNode(node) } );
                        if( (action & DnDConstants.ACTION_MOVE) != 0 ) {
                            dropNode.getParentNode().getChildren().remove( new Node[] {dropNode} );
                        }
                        return null;
                    }
                };
            }
        }*/
        return null;
    }
    
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    protected void createPasteTypes(Transferable t, List s) {
        super.createPasteTypes(t, s);
        PasteType paste = getDropType( t, DnDConstants.ACTION_COPY, -1 );
        if( null != paste )
            s.add( paste );
    }
    /*
    @Override
    public Action[] getActions(boolean context) {
        
        List<? extends Action> actions = Utilities.actionsForPath("Actions/ModuleNode");
        return actions.toArray( new Action[actions.size()] );
        
    }*/
    
    @Override
    public boolean canDestroy() {
        return true;
    }   

}
