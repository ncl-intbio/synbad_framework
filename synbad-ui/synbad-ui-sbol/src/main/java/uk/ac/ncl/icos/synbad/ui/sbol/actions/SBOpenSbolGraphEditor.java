/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.actions;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.model.SbolModel;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.sbol.visualisation.SbolGraphStreamPanel;

/**
 *
 * @author Owen
 */
@ActionID(
        category = "Edit",
        id = "uk.ac.ncl.icos.synbad.svp.ui.actions.SBOpenSbolGraphEditor"   
)
@ActionRegistration(
        displayName = "Open Graph Editor"
)
@ActionReference(path = "Menu/Edit", position = 1475)
public class SBOpenSbolGraphEditor extends AbstractAction {
    

    private final ModuleDefinition module;
    
    public SBOpenSbolGraphEditor(ModuleDefinition module) {
        super("Graph Editor");
        this.module = module;
    }
       
    @Override
    public void actionPerformed(ActionEvent e) {   

        SBWorkspace ws = module.getWorkspace();
        if(ws==null)
            return;

        SbolModel model = new SbolModel(ws, module);
        SbolHView view = model.createView();
        view.nodeSet().stream().forEach(n -> view.expand(n));

        SbolGraphStreamPanel panel = new SbolGraphStreamPanel();
        panel.setView(view.getRoot(), view);
               
        TopComponent window = new CloneableTopComponent() {
        };
        
        window.setName("SBOL Graph");
        
        window.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        window.add(panel, c);
        window.revalidate();
        window.setVisible(true);
        panel.run();
        window.open();
        window.requestActive();
       
    }

}
