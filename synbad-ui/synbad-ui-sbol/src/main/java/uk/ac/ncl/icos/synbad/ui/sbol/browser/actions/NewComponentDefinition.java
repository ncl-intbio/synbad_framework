/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.browser.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.ui.sbol.dialog.CreateComponentPanel;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.browser.actions.NewComponentDefinition"
)
@ActionRegistration(
        displayName = "#CTL_NewComponentDefinition"
)
@Messages("CTL_NewComponentDefinition=New Component Definition")
@ActionReference(path = "Actions/ComponentDefinitionPanel", position = 1475)
public final class NewComponentDefinition implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        
        SBWorkspace manager = Lookup.getDefault().lookup(SBWorkspaceManager.class).getCurrentWorkspace();
        
        CreateComponentPanel form = new CreateComponentPanel();
        String msg = "New ComponentDefinition";
        DialogDescriptor dd = new DialogDescriptor(form, msg);
        dd.createNotificationLineSupport();
        dd.setValid(form.isValid());
        
        if(!form.isValid()) {
            dd.getNotificationLineSupport().setErrorMessage(form.getMessage());
        }
       
        form.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals("Valid")) {
                    boolean wasValid = (Boolean)evt.getOldValue();
                    boolean isValid = (Boolean)evt.getNewValue();
                    if(wasValid != isValid)
                        dd.setValid(isValid);
                } else if(evt.getPropertyName().equals("Msg")) {
                    if(form.getMessage().isEmpty()) {
                        dd.getNotificationLineSupport().clearMessages();
                    } else {
                        dd.getNotificationLineSupport().setErrorMessage(form.getMessage());
                    }
                }
            } 
        });
        
        Object result = DialogDisplayer.getDefault().notify(dd);
        
        if (result != NotifyDescriptor.OK_OPTION)
            return;

        SBIdentity identity = manager.getIdentityFactory().getIdentity(form.getPrefix(), form.getDisplayId(), form.getVersion());
        URI COMPONENTDEFINITION_TYPE = URI.create(ComponentDefinition.TYPE);
        SBDataDefManager m = SBDataDefManager.getManager();
        Set<Type> types = form.getTypes().stream().map(t -> m.getDefinition(ComponentType.class, t)).collect(Collectors.toSet());
        Set<Role> roles = form.getRoles().stream().map(t -> m.getDefinition(ComponentRole.class, t)).collect(Collectors.toSet());        
        SBAction action = new SbolActionBuilder(manager, manager.getContextIds())
          .createComponentDefinition(identity.getIdentity(), types.toArray(new Type[] {}), roles.toArray(new Role[] {}))
          .createValue(identity.getIdentity(), URI.create(SynBadTerms.SbolIdentified.hasName), SBValue.parseValue(form.getIdentifiedName()), COMPONENTDEFINITION_TYPE)
          .createValue(identity.getIdentity(), URI.create(SynBadTerms.SbolIdentified.hasDescription), SBValue.parseValue(form.getDescription()), COMPONENTDEFINITION_TYPE)
          .build();
        manager.perform(action);
       
    }
}
