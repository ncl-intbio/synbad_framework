/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import javax.swing.Action;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Utilities;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.filter.SBFilterByDataClass;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.ui.sbol.nodes.SBComponentDefinition;
import uk.ac.ncl.icos.synbad.ui.browser.AbstractBrowserPanel;

/**
 *
 * @author owengilfellon
 */
public class ComponentPanel extends AbstractBrowserPanel {

    BeanTreeView view;
    
    public ComponentPanel() {
        super();
        initComponents();
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        explorerManager.setRootContext(new AbstractNode(Children.LEAF));
        
        view = new BeanTreeView();
        view.setRootVisible(false);
//        view.addPropertyColumn("name", "Name");
//        view.addPropertyColumn("displayId", "Display ID");
//        view.addPropertyColumn("version", "Version");
//        view.addPropertyColumn("description", "Description");
        
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    
    
    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
        RootComponentNode rootNode = new RootComponentNode(Children.create(new ComponentPanel.ComponentPanelNodeFactory(workspace), true));
        this.explorerManager.setRootContext(rootNode);
        
        
        
//        Property[] properties = Arrays.asList(rootNode.getChildren().getNodes()).stream().flatMap(n -> Arrays.asList(n.getPropertySets()).stream()).map(p -> p.getProperties()).findFirst().orElse(null);
//        if(properties!=null)
//            view.setProperties(properties);
        
    }

    @Override
    public void clearWorkspace() {
        this.explorerManager.setRootContext(Node.EMPTY);
        this.workspace = null;
    }

    private class ComponentPanelNodeFactory extends ChildFactory.Detachable<ComponentDefinition> implements SBSubscriber {

        private final SBWorkspace workspace;

        public ComponentPanelNodeFactory(SBWorkspace workspace) {
            this.workspace = workspace;
        }
        
        @Override
        protected void addNotify() {
            super.addNotify(); 
            this.workspace.getDispatcher().subscribe(new SBFilterByDataClass(ComponentDefinition.class), this);
        }

        @Override
        protected void removeNotify() {
            super.removeNotify();
            this.workspace.getDispatcher().unsubscribe(this);
        }

        @Override
        protected boolean createKeys(List<ComponentDefinition> toPopulate) {
           
            for(ComponentDefinition definition : workspace.getObjects(ComponentDefinition.class, workspace.getContextIds())) {
                toPopulate.add(definition);
            }
            
            return true;
        }

        @Override
        protected Node createNodeForKey(ComponentDefinition key) {
            return new SBComponentDefinition(key);
        }

        @Override
        public void onEvent(SBEvent e) {
            refresh(true);
        }

    }
    
    class RootComponentNode extends AbstractNode {

        public RootComponentNode(Children children) {
            super(children);
        }

        @Override
        public Action[] getActions(boolean context) {
            List<? extends Action> actions = Utilities.actionsForPath( "Actions/ComponentDefinitionPanel" );
            return actions.toArray( new Action[actions.size()] );
        }
    }
}
