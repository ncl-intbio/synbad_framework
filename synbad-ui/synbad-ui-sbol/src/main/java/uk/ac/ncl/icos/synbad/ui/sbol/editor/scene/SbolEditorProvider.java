/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.editor.scene;

import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.ui.services.SBEditorProvider;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=SBEditorProvider.class)
public class SbolEditorProvider implements SBEditorProvider<ModuleDefinition> {

    @Override
    public TopComponent getEditor(ModuleDefinition moduleDefinition) {
        return new SbolTopComponent(moduleDefinition);
    }

    @Override
    public String getName() {
        return "Svp View";
    }
    
    
    
}
