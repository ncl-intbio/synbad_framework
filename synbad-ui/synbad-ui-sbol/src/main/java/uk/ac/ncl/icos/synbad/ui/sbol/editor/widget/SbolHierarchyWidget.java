/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.editor.widget;

import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.widget.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.editor.widget.EntityWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.MapsToPinWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.PinOwner;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 */
public class SbolHierarchyWidget extends EntityWidget implements PinOwner<SBViewValued, PropertiedNode> {

    private static final Logger logger = LoggerFactory.getLogger(SbolHierarchyWidget.class);
    
    private final Map<SBViewIdentified, Anchor> externalInPorts;
    private final Map<SBViewIdentified, Anchor> externalOutPorts;
    private final Map<SBViewIdentified, Anchor> internalInPorts;
    private final Map<SBViewIdentified, Anchor> internalOutPorts;
    
    public SbolHierarchyWidget(ASBGraphPinScene scene, SBVObjectNode entity) {
        super(scene, entity);
        
        this.externalInPorts = new HashMap<>();
        this.externalOutPorts = new HashMap<>();
        this.internalInPorts = new HashMap<>();
        this.internalOutPorts = new HashMap<>();

//        getActions().addAction(ActionFactory.createAcceptAction(new EntityAcceptProvider()));
    }

    @Override
    public void addWidget(Widget widget)
    {
     //   logger.debug("Adding widget [ {} ] to [ {} ]", widget.getLookup().lookup(SBViewIdentified.class), getLookup().lookup(SBViewIdentified.class));
        getEntitiesLayer().addChild(widget);
    }

    public void removeWidget(Widget widget) {
     //   logger.debug("Removing widget [ {} ] from [ {} ]", widget.getLookup().lookup(SBViewIdentified.class), getLookup().lookup(SBViewIdentified.class));
        getEntitiesLayer().removeChild(widget);
    }
    
    @Override
     public Widget addPin(PropertiedNode port) {

        SBViewIdentified pInstance = port.getLookup().lookup(SBViewIdentified.class);
        SBPortDirection direction = port.getLookup().lookup(SBPortDirection.class);
        //logger.debug("Adding pin: [ {} ]", pInstance);
        MapsToPinWidget pin = new MapsToPinWidget(getScene(), port);
        //MapsTo mapsTo = (MapsTo)pInstance.getObject();
        
        if(direction == SBPortDirection.IN) {
            inPinWidget.addChild(pin); 
            externalInPorts.put(pInstance, pin.getExternalAnchor());
            internalInPorts.put(pInstance, pin.getInternalAnchor());
        } else {
            outPinWidget.addChild(pin);
            externalOutPorts.put(pInstance, pin.getExternalAnchor());
            internalOutPorts.put(pInstance, pin.getInternalAnchor());
        }

        return pin;
    }

    @Override
    public Anchor getAnchor(SBViewValued port, boolean internal) {
        
        logger.debug("Get anchor for: {}", port);
        
        if(internal) {
            if(internalInPorts.containsKey(port))
                return AnchorFactory.createProxyAnchor(minimisedState, internalInPorts.get(port), getInAnchor());
            else
              return AnchorFactory.createProxyAnchor(minimisedState, internalOutPorts.get(port), getOutAnchor());
        } else {
            if(externalInPorts.containsKey(port))
                return AnchorFactory.createProxyAnchor(minimisedState, externalInPorts.get(port), getInAnchor());
            else
               return AnchorFactory.createProxyAnchor(minimisedState, externalOutPorts.get(port), getOutAnchor());
        }/**/

    }  
    

}
