/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.editor.widget.action;

import java.awt.Point;
import java.util.Collection;
import java.util.HashSet;
import org.netbeans.api.visual.action.MoveProvider;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.widget.SbolHierarchyWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceComponentWidget;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.widget.SbolModuleWidget;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;


/**
 *
 * @author owengilfellon
 */
public class SVPMover implements MoveProvider {

    private Point point = null;
    private ASBGraphPinScene scene;

    public SVPMover(ASBGraphPinScene scene) {
        this.scene = scene;
    }

    @Override
    public void movementStarted(Widget widget) {

        if(widget instanceof SequenceComponentWidget) {
            SequenceComponentWidget w = (SequenceComponentWidget) widget;
            
            
            SBViewIdentified vo = widget.getLookup().lookup(SBViewIdentified.class);
            SBViewIdentified parentVo = (SBViewIdentified) scene.getSBView().getParent(vo);
            Widget parent = scene.findWidget(parentVo);
            if(SbolHierarchyWidget.class.isAssignableFrom(parent.getClass()))
                ((SbolHierarchyWidget)parent).removeWidget(w);
   
            scene.getMovementLayer().addChild(widget);
            widget.setPreferredLocation(point);
        }
    }

    @Override
    public void movementFinished(Widget widget) {
        
        scene.getMovementLayer().removeChild(widget);
        Collection<SbolModuleWidget> newParents = new HashSet<>();
        Point newPos = null;

        for(Object node : scene.getNodes()) {

            Widget w =  scene.findWidget(node);

            if(w != widget && w.isHitAt(w.convertSceneToLocal(point))) {
                newPos = w.convertSceneToLocal(point);
                SbolModuleWidget found = scene.findWidget(node) instanceof SbolModuleWidget
                        ? (SbolModuleWidget)scene.findWidget(node)
                        : null;
                if(found!=null)
                    newParents.add(found);

            }
        }

        if(!newParents.isEmpty()) {
            
            newParents.iterator().next().addWidget((SequenceComponentWidget)widget);
            
            return;
        }

        scene.getEntitiesLayer().addChild(widget);
    }

    @Override
    public Point getOriginalLocation(Widget widget) {
        point = widget.getParentWidget().convertLocalToScene(widget.getLocation());
        return point;
    }

    @Override
    public void setNewLocation(Widget widget, Point location) {
        point = location;
        widget.setPreferredLocation(location);
    }
        
    
}
