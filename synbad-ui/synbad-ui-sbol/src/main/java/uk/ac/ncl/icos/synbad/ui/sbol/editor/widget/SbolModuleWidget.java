/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.editor.widget;

import java.util.List;
import java.util.stream.Collectors;
import org.netbeans.api.visual.widget.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceComponentWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceWidget;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.scene.SBSbolScene;
import uk.ac.ncl.icos.synbad.sbol.algorithm.ComponentOrderer;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;

/**
 *
 * @author owengilfellon
 */
public class SbolModuleWidget extends SbolHierarchyWidget  {

    private final SequenceWidget listWidget;
    private final static Logger logger = LoggerFactory.getLogger(SbolModuleWidget.class);

    public SbolModuleWidget(SBSbolScene scene, SBVObjectNode entity) {
        super(scene, entity);
        listWidget = new SequenceWidget(scene);
       // getActions().addAction(ActionFactory.createAcceptAction(new PartAcceptProvider(scene.getWorkspace())));
    }
    @Override
    public void addWidget(Widget widget) {
        SBViewIdentified object = widget.getLookup().lookup(SBViewIdentified.class);
        
        if(widget instanceof SequenceComponentWidget && object.isClass(FunctionalComponent.class)) {
            logger.debug("Adding widget to component list: [ {} ]", widget.getLookup().lookup(SBViewIdentified.class));
            if(listWidget.isEmpty()) {
                
                getEntitiesLayer().addChild(listWidget);
            }
            
            Component c = (Component)object.getObject();

            
            
            List<Component> componentsFromWidgets = listWidget.getComponents().stream().map(w -> w.getLookup().lookup(SBViewIdentified.class))
                    .map(vo -> vo.getObject()).filter(com -> com != null && Component.class.isAssignableFrom(com.getClass()))
                    .map(com -> (Component)com)
                    .collect(Collectors.toList());
            
            int index = 0;            
            for(Component comp : componentsFromWidgets) {
               if(ComponentOrderer.precedes(c, comp))
                    index ++;
             };

            listWidget.addComponent(index, widget);
        } else {
            super.addWidget(widget);
        }
    }
    
    @Override
    public void removeWidget(Widget widget) {
        if(widget instanceof SequenceComponentWidget) { 
            logger.debug("Removing widget from component list: [ {} ]", widget.getLookup().lookup(SBViewIdentified.class));
            listWidget.removeComponent(widget);
            if(listWidget.isEmpty())
                getEntitiesLayer().removeChild(listWidget);
        } else {
            super.removeWidget(widget);
        }
    }

    /*
    public class SvpModuleLayout implements Layout {


        @Override
        public void layout (Widget widget) {
            for (Widget child : widget.getChildren ()) {
                child.resolveBounds (null, null);
            }
        }

        @Override
        public boolean requiresJustification (Widget widget) {
            return true;
        }

        @Override
        public void justify (Widget widget) {
            
            int top_margin = 30;
            int track_margin = 30;
            
            Rectangle bounds = widget.getClientArea();
            List<Widget> children = ((SvpModuleWidget)widget).getEntitiesLayer().getChildren();

            if(!listWidget.isEmpty())
                listWidget.resolveBounds(null, new Rectangle(0, top_margin, widget.getClientArea().width, widget.getClientArea().height));
            
            for(Widget w : children) {
                if(!(w instanceof SVPModuleListWidget)) {
                     w.resolveBounds(null, new Rectangle());
                } 
               
            }
        }
    }*/
}
