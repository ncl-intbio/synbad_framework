/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.synbad.ui.sbol.nodes.SBModuleDefinitionNode;

/**
 *
 * @author owengilfellon
 */
public class ModuleDefinitionTransferable implements Transferable {

    private final SBModuleDefinitionNode node;
    
    public ModuleDefinitionTransferable(SBModuleDefinitionNode node) {
        this.node = node;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { ModuleDefinitionFlavor.MODULE_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == ModuleDefinitionFlavor.MODULE_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return node;
    }
    
    public static class ModuleDefinitionFlavor extends DataFlavor {
    
        public static final DataFlavor MODULE_FLAVOR = new ModuleDefinitionFlavor();

        public ModuleDefinitionFlavor() {
             super(SBModuleDefinitionNode.class, "ModuleDefinitionData");
        }
    }
}
