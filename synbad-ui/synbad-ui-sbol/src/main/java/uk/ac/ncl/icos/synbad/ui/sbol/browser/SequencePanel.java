/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import javax.swing.Action;
import org.openide.explorer.view.TreeTableView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.filter.SBFilterByDataClass;
import uk.ac.ncl.icos.synbad.sbol.object.Sequence;
import uk.ac.ncl.icos.synbad.ui.browser.AbstractBrowserPanel;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBEntityNode;

/**
 *
 * @author owengilfellon
 */
public class SequencePanel extends AbstractBrowserPanel {

    TreeTableView view;
    
    public SequencePanel() {
        super();
        initComponents();
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        explorerManager.setRootContext(new AbstractNode(Children.LEAF));
        
        view = new TreeTableView();
        view.setRootVisible(false);

        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    
    
    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
        RootComponentNode rootNode = new RootComponentNode(Children.create(new SequencePanel.SequencePanelNodeFactory(workspace), true));
        this.explorerManager.setRootContext(rootNode);
    }

    @Override
    public void clearWorkspace() {
        this.explorerManager.setRootContext(Node.EMPTY);
        this.workspace = null;
    }

    private class SequencePanelNodeFactory extends ChildFactory.Detachable<Sequence> implements SBSubscriber {

        private final SBWorkspace workspace;

        public SequencePanelNodeFactory(SBWorkspace workspace) {
            this.workspace = workspace;
            
        }

        @Override
        protected void addNotify() {
            super.addNotify(); 
            this.workspace.getDispatcher().subscribe(new SBFilterByDataClass(Sequence.class), this);
        }

        @Override
        protected void removeNotify() {
            super.removeNotify();
            this.workspace.getDispatcher().unsubscribe(this);
        }
        
        

        @Override
        protected boolean createKeys(List<Sequence> toPopulate) {
            workspace.getObjects(Sequence.class, workspace.getContextIds()).stream().forEach(toPopulate::add);
            return true;
        }

        @Override
        protected Node createNodeForKey(Sequence key) {
            return new SBEntityNode(key);
        }

        @Override
        public void onEvent(SBEvent e) {
            refresh(true);
        }
    }
    
    class RootComponentNode extends AbstractNode {

        public RootComponentNode(Children children) {
            super(children);
        }

        @Override
        public Action[] getActions(boolean context) {
            return new Action[] {
             
            };
        }
    }
}
