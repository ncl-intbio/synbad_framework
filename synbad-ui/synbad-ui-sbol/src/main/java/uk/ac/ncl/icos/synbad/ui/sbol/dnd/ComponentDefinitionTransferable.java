/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.synbad.ui.sbol.nodes.SBComponentDefinition;

/**
 *
 * @author owengilfellon
 */
public class ComponentDefinitionTransferable implements Transferable {

    private final SBComponentDefinition node;
    
    public ComponentDefinitionTransferable(SBComponentDefinition node) {
        this.node = node;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { ComponentDefinitionFlavor.COMPONENT_DEF_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == ComponentDefinitionFlavor.COMPONENT_DEF_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return node;
    }
    
    public static class ComponentDefinitionFlavor extends DataFlavor {
    
        public static final DataFlavor COMPONENT_DEF_FLAVOR = new ComponentDefinitionFlavor();

        public ComponentDefinitionFlavor() {
             super(SBComponentDefinition.class, "ComponentDefinitionData");
        }
    }
}
