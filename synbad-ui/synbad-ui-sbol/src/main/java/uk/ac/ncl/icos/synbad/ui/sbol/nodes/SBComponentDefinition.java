/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.nodes;

import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;

public class SBComponentDefinition extends SBTopLevelNode  {

    public SBComponentDefinition(ComponentDefinition definition) {
        super(definition);
    }

    @Override
    public String getName() {
        ComponentDefinition def = getLookup().lookup(ComponentDefinition.class);
        if(!def.getName().isEmpty())
            return def.getName();
        return def.getDisplayId();
    }
   
    
    
    @Override
    public Action getPreferredAction() {
        return new AbstractAction("Open view") {
            @Override
            public void actionPerformed(ActionEvent e) {   
                /*
                IEntity definition = getLookup().lookup(IEntity.class);
                VModel model = new VModelImpl(definition,  definition.getWorkspace());
                EntityViewProvider provider = Lookup.getDefault().lookup(EntityViewProvider.class);                   
                TopComponent window = provider.getView(model.getDefaultView());
                window.open();
                window.requestActive();*/
            }
        };
    }   

}
