/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.obj;

import java.util.Arrays;
import java.util.Collection;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Location;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.Model;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.sbol.object.Sequence;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceAnnotation;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceConstraint;
import uk.ac.ncl.icos.synbad.workspace.objects.SBDomainObjectProvider;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = SBDomainObjectProvider.class)
public class SbolObjectProvider implements SBDomainObjectProvider {

    @Override
    public Collection<Class<?>> getDomainObjects() {
        return Arrays.asList(
            uk.ac.ncl.icos.synbad.sbol.object.Collection.class,
            Component.class,
            ComponentDefinition.class,
            FunctionalComponent.class,
            Interaction.class,
            Location.Cut.class,
            Location.Range.class,
            MapsTo.class,
            Model.class,
            Module.class,
            ModuleDefinition.class,
            Participation.class,
            Sequence.class,
            SequenceAnnotation.class,
            SequenceConstraint.class
        );
    }
    
}
