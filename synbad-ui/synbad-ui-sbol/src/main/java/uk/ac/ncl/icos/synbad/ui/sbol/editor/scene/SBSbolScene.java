/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.editor.scene;

import java.awt.Color;
import java.awt.Dimension;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.anchor.AnchorShapeFactory;
import org.netbeans.api.visual.router.Router;
import org.netbeans.api.visual.router.RouterFactory;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModelEdgeType;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.editor.widget.action.ToolchainManager;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.widget.SbolHierarchyWidget;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.widget.SbolModuleWidget;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVEdgeNode;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewEdge;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewObject;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.MapsToPinWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.PinOwner;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceComponentWidget;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SbolPortNode;

/**
 *
 * @author owengilfellon
 */
public class SBSbolScene extends ASBGraphPinScene<
        SBVObjectNode, 
        SBVEdgeNode, 
        SbolPortNode, 
        SbolHView> {

    protected final Map<SBViewIdentified, SBVObjectNode> entityNodeMap = new HashMap<>();
    protected final Map<SBViewIdentified, Set<SbolPortNode>> portNodeMap = new HashMap<>();
    protected final Map<SBViewEdge, SBVEdgeNode> wireNodeMap = new HashMap<>();
    
    private final Collection<SbolInteractionRole> inDirections = Arrays.asList(
                SbolInteractionRole.INHIBITED, 
                SbolInteractionRole.MODIFIED, 
                SbolInteractionRole.PRODUCT, 
                SbolInteractionRole.PROMOTER, 
                SbolInteractionRole.STIMULATED);

    private static final Logger LOGGER = LoggerFactory.getLogger(SBSbolScene.class);

    public SBSbolScene( SbolHView view ) {
        super(view);
        setMinimumSize(new Dimension(800, 600));
        
        LOGGER.debug("Creating editor from [ {} ] in [ {} ]", view.getRoot(), view);

        addNode(new SBVObjectNode((SBViewIdentified)view.getRoot()));
        getPriorActions().addAction(new ToolchainManager());
        setActiveTool("default");
        view.subscribe(new SBEventFilter.DefaultFilter(), this);
    }
    
    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(getSBView()));
    }
    
    @Override
    protected Widget attachNodeWidget(SBVObjectNode n) {
        
        LOGGER.debug("Attach node widget: {}", n);
        
        SbolViewObject vo = n.getLookup().lookup(SbolViewObject.class);
        Widget e = null;
        
        // if we're adding the root
        
        if(vo == getSBView().getRoot()) {   
            
            LOGGER.debug("View object is root: [ {} ]", vo);
            e = new SbolModuleWidget(this, n);
            getEntitiesLayer().addChild(e);
            getEntitiesLayer().revalidate();
        } 
        
        // if we're adding a bottom level component with an icon
        
        else  if( vo.isClass(Component.class) ||
            (vo.isClass(FunctionalComponent.class) && getSBView().getAllDescendants(vo).isEmpty())) {
            
            LOGGER.debug("Creating sequence component widget for : [ {} ]", vo);

            e = new SequenceComponentWidget(this, n);
            Widget parent = findWidget(entityNodeMap.get(getSBView().getParent(vo)));
            
            if(parent == null) {
                LOGGER.error("Could not find parent widget for: [ {} ]", vo);
                return null;
            }

            if(parent instanceof SbolModuleWidget) {
               LOGGER.debug("Attaching sequence component widget: [ {} ] to [ {} ]", vo, parent.getLookup().lookup(SBViewIdentified.class));
               ((SbolModuleWidget)parent).addWidget((SequenceComponentWidget)e);
            }
        } 
        
        // if we're adding a level in the hierarchy
        
        else {
            
            SbolViewObject node = getSBView().getParent(n.getLookup().lookup(SbolViewObject.class));
            
            if(node == null)
                LOGGER.error("Could not find parent node of: [ {} ]", node);
            
            Widget parent = findWidget(entityNodeMap.get(node));
            
            if(parent == null)
                LOGGER.error("Could not find widget for parent: [ {} ]", node);
            
            if(parent!=null && SbolHierarchyWidget.class.isAssignableFrom(parent.getClass())) {
                SbolHierarchyWidget sbolParent = ((SbolHierarchyWidget)parent);
                if(sbolParent.getMinimisedState().getBooleanState())
                        sbolParent.setMinimisedState(false);
                e = new SbolModuleWidget(this, n);
                sbolParent.addWidget(e);
            }
        }
        
        entityNodeMap.put(n.getLookup().lookup(SBViewIdentified.class), n);
        return  e;
    }

    @Override
    protected void detachNodeWidget(SBVObjectNode node, Widget widget) {
        super.detachNodeWidget(node, widget);
        entityNodeMap.remove(node.getLookup().lookup(SBViewIdentified.class));
    }

    @Override
    protected Widget attachPinWidget(SBVObjectNode n, SbolPortNode p) {
        
        LOGGER.debug("Adding pin widget: [ {} ] to [ {} ]", 
                p.getLookup().lookup(SBViewIdentified.class).getDisplayId(), 
                n.getLookup().lookup(SBViewIdentified.class).getDisplayId());
        
        Widget owner = findWidget(n);

        if(owner != null && PinOwner.class.isAssignableFrom(owner.getClass())) {
            
            SBViewIdentified obj = p.getPort();
            if(!portNodeMap.containsKey(p)) {
                portNodeMap.put(obj, new HashSet<>());
            }
            portNodeMap.get(obj).add(p);
            return ((PinOwner)owner).addPin(p);
        }
        
        return null;
    }

    @Override
    protected void detachPinWidget(SbolPortNode pin, Widget widget) {
        
        LOGGER.debug("Detaching pin widget: [ {} ] from [ {} ]", pin, widget);
        
        super.detachPinWidget(pin, widget); 
        portNodeMap.remove(pin.getLookup().lookup(SBViewIdentified.class));
    }
    
    public boolean isMapsToEdge(SbolViewEdge edge) {
        
        SbolHView view = getSBView();
        
        return (view.getEdgeSource(edge).getObject() instanceof MapsTo ||  view.getEdgeTarget(edge).getObject() instanceof MapsTo)
                && (edge.getEdge().getEdge().equals(MapsToRefinement.useLocal.getUri()) ||
            edge.getEdge().getEdge().equals(MapsToRefinement.useRemote.getUri()) ||
            edge.getEdge().getEdge().equals(MapsToRefinement.verifyIdentical.getUri()));
        
    }
    
  /*  public void processMapsToEdge(SBViewIdentified ) {
        
    }*/

    @Override
    protected Widget attachEdgeWidget(SBVEdgeNode e) {
        
        // TODO Could be interaction or MapsToEdges
        
        

        SbolHView view = (SbolHView)getSBView();
        SbolViewEdge vEdge = e.getLookup().lookup(SbolViewEdge.class);
        
        LOGGER.debug("Attaching edge widget: [ {} ]", ((URI)vEdge.getEdge().getEdge()).toASCIIString());
   
        SynBadEdge edge = (SynBadEdge)vEdge.getEdge();
        ConnectionWidget w = new ConnectionWidget(this);
      
        // if the edge is between two ports, then add connection widget
   
        SBViewIdentified port = null;       
        SbolViewObject from = null;
        SbolViewObject to = null;

        // is either a participation edge

        if(edge.getEdge().toASCIIString().equals(SynBadTerms.SbolInteraction.participant)) {
            SbolViewObject participant = vEdge.getFrom();
            SbolInteractionRole role = ((Participation)participant.getObject()).getRole();
            port = participant;
            SbolViewObject interaction = (SbolViewObject)getSBView()
                    .getTraversal().v(vEdge.getFrom())
                    .e(SBDirection.IN, SynBadTerms.SbolInteraction.hasParticipation)
                    .v(SBDirection.IN).getResult().streamVertexes().findFirst().orElse(null);
            if( inDirections.contains(role)) {
                from = interaction;
                to = vEdge.getTo();
            } else {
                from = vEdge.getTo();
                to = interaction;
            }
        } else if (edge.getEdge().toASCIIString().equals(SynBadTerms.SbolMapsTo.hasLocalInstance) ||
                edge.getEdge().toASCIIString().equals(SynBadTerms.SbolMapsTo.hasRemoteInstance)) {
            
            SbolViewObject mapsTo = vEdge.getFrom();
            MapsToRefinement refinement = ((MapsTo)mapsTo.getObject()).getRefinement();
            port = mapsTo;
            SbolViewObject owner  = (SbolViewObject)getSBView()
                    .getTraversal().v(mapsTo)
                    .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasMapsTo)
                    .v(SBDirection.IN).getResult().streamVertexes().findFirst().orElse(null);
         
            
            if(refinement == MapsToRefinement.useLocal && edge.getEdge().toASCIIString().equals(SynBadTerms.SbolMapsTo.hasLocalInstance)) {
                from = vEdge.getTo();
                to = owner;
                
            } else {
                from = owner;
                to = vEdge.getTo();
            }
        }

        // or a remote or local edge

      //  SBViewIdentified from = (fromPort.getData() instanceof MapsTo) ? view.getOwner(fromPort) : fromPort;
      ///  SBViewIdentified to = (toPort.getData() instanceof MapsTo) ? view.getOwner(toPort) : toPort;

        if(from != null && to != null) {

            boolean isFromParent = view.getParent(to) == from;
            boolean isToParent = view.getParent(from) == to;

            // get the nodes associated with the owners

            SBVObjectNode fromPortOwner = entityNodeMap.get(from);
            SBVObjectNode toPortOwner = entityNodeMap.get(to);

            PinOwner fromWidget = (PinOwner) findWidget(fromPortOwner);
            PinOwner toWidget =  (PinOwner) findWidget(toPortOwner);

            // isFrom / isTo are used to identify whether the edge is
            // internal or external to the widget

            Anchor fromAnchor = fromWidget.getAnchor(port, isFromParent);
            Anchor toAnchor = toWidget.getAnchor(port, isToParent);

            w.setSourceAnchor(fromAnchor);
            w.setTargetAnchor(toAnchor);

            w.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
            w.setLineColor(Color.WHITE);

            Router r = SbolHierarchyWidget.class.isAssignableFrom(fromWidget.getClass()) ?
                    RouterFactory.createOrthogonalSearchRouter(((SbolHierarchyWidget)fromWidget).getEntitiesLayer()) : null;
               //     RouterFactory.createOrthogonalSearchRouter(pWidget.getEntitiesLayer());

            if(r != null)
                w.setRouter(r);
            w.setControlPointCutDistance(10);
            getConnectionsLayer().addChild(w);
            wireNodeMap.put(vEdge, e);
        }
        
        

        return w;
    }

    @Override
    protected void attachEdgeSourceAnchor(SBVEdgeNode e, SbolPortNode p, SbolPortNode p1) {
        
        LOGGER.debug("Attaching edge source anchor: [ {} ] via [ {} ] and [ {} ]", e, p, p1);
        
        
        if(p1 != null) {
            
            URI[] contexts = p1.getLookup().lookup(SBIdentified.class).getContexts();
            SynBadEdge edge = (SynBadEdge) e.getLookup().lookup(SBViewEdge.class).getEdge();

            SBPort from = (SBPort)getWorkspace().getEdgeSource(edge, contexts);
            SBPort to = (SBPort)getWorkspace().getEdgeTarget(edge, contexts);
            
            SBIdentified fromOwner = from.getOwner();
            SBIdentified toOwner = to.getOwner();

            boolean isFromParent = false;

            if (getWorkspace().outgoingEdges(fromOwner, null, toOwner.getClass(), toOwner.getContexts())
                    .stream().anyMatch(oe -> oe.getTo().equals(toOwner.getIdentity())))
                isFromParent = true;
            
            SbolViewObject fromPort = p1.getLookup().lookup(SbolViewObject.class);
            
            Anchor fromAnchor = ((SbolHierarchyWidget)findWidget(getSBView().getParent(fromPort)))
                    .getAnchor(fromPort, isFromParent);
            
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            edgeWidget.setSourceAnchor (fromAnchor);
            ((MapsToPinWidget)findWidget(p1)).setIsConnected(true);
            
        } else if (p != null) {
            ((MapsToPinWidget)findWidget(p)).setIsConnected(false);
        }

        if(p1 == null) {
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            getConnectionsLayer().removeChild(edgeWidget);
        }
    }

    @Override
    protected void attachEdgeTargetAnchor(SBVEdgeNode e, SbolPortNode p, SbolPortNode p1) { 
        
        LOGGER.debug("Attaching edge target anchor: [ {} ] via [ {} ] and [ {} ]", e, p, p1);
        
        if(p1 != null) {
            
            URI[] contexts = p1.getLookup().lookup(SBIdentified.class).getContexts();
            
            SynBadEdge edge = (SynBadEdge) e.getLookup().lookup(SBViewEdge.class).getEdge();
            
            SBPort from = (SBPort)getWorkspace().getEdgeSource(edge, contexts);
            SBPort to = (SBPort)getWorkspace().getEdgeTarget(edge, contexts);
            
            SBIdentified fromOwner = from.getOwner();
            SBIdentified toOwner = to.getOwner();

            boolean isToParent = false;

            if (getWorkspace().outgoingEdges(toOwner, null, fromOwner.getClass(), toOwner.getContexts())
                    .stream().anyMatch(oe -> oe.getTo().equals(fromOwner.getIdentity())))
                isToParent = true;
            
            SbolViewObject toPort = p1.getLookup().lookup(SbolViewObject.class);
            
            Anchor targetAnchor = ((SbolHierarchyWidget)findWidget(getSBView().getParent(toPort))).getAnchor(toPort, isToParent);
            
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            edgeWidget.setTargetAnchor(targetAnchor);
            edgeWidget.setTargetAnchorShape(AnchorShapeFactory.createTriangleAnchorShape(10, true, false));
            ((MapsToPinWidget)findWidget(p1)).setIsConnected(true);
            
        } else if (p != null) {
            ((MapsToPinWidget)findWidget(p)).setIsConnected(false);
        }
    }

    @Override
    protected void detachEdgeWidget(SBVEdgeNode edge, Widget widget) {
        
        LOGGER.debug("Detaching edge widget: [ {} ] from [ {} ]", edge, widget);
        
        super.detachEdgeWidget(edge, widget); 
        wireNodeMap.remove(edge.getLookup().lookup(SBViewEdge.class));
    }
    
    public SBPortDirection getPortDirection(SbolInteractionRole role) {
        return inDirections.contains(role) ? SBPortDirection.IN : SBPortDirection.OUT;
    }
    
    public SBPortDirection getPortDirection(SBViewIdentified<MapsTo> mapsTo, SBViewIdentified component) {
       SBPortDirection d = mapsTo.getObject().getRefinement() == MapsToRefinement.useLocal 
            && !component.getIdentity().equals(mapsTo.getObject().getLocalInstance().getIdentity()) ?
               SBPortDirection.IN : SBPortDirection.OUT;
       //logger.debug("Got direction: {}", d);
       return d;
    }
    
    public SbolPortNode getPortNode(SBViewIdentified owner, SBViewIdentified port) {
        
        SbolPortNode node = null;
        if(port.getObject() instanceof Participation) {
            Participation p = (Participation)port.getObject();
            node = new SbolPortNode(getPortDirection(p.getRole()), port.getName(), owner, port);
        } else if (port.getObject() instanceof MapsTo) {
            node = new SbolPortNode(getPortDirection((SBViewIdentified<MapsTo>)port, owner), port.getName(), owner, port);
        } else {
            LOGGER.error("Could not get Port from {}", port);
        } 
        
        return node;
        
    }
    
    @Override
    public void onEvent(SBEvent e) {
        
        LOGGER.debug("Processing evt: {}", e);

        if(SBGraphEvent.class.isAssignableFrom(e.getClass()) && ((SBGraphEvent) e).getGraphEntityType() == SBGraphEntityType.NODE) {      
            SBGraphEvent<? extends SBViewIdentified> evt = (SBGraphEvent<? extends SBViewIdentified>) e;
            if(!evt.getData().isClass(MapsTo.class) && !evt.getData().isClass(Participation.class))
                processNodeEvent(evt);
        } else if(SBGraphEvent.class.isAssignableFrom(e.getClass()) && ((SBGraphEvent) e).getGraphEntityType() == SBGraphEntityType.EDGE) {
            
            SBGraphEvent<? extends SBViewEdge> evt = (SBGraphEvent<? extends SBViewEdge>) e;

            processEdgeEvent(evt);
        } 

        revalidate();
    }
    
    public void processNodeEvent(SBGraphEvent<? extends SBViewIdentified> evt) {
        
        LOGGER.debug("Processing node evt: {}", evt);
        
        if(evt.getType() == SBGraphEventType.VISIBLE || evt.getType() == SBGraphEventType.ADDED ) {

            if(entityNodeMap.containsKey(evt.getData()))
                return;

            SBVObjectNode objNode = new SBVObjectNode(evt.getData());
            
            
            LOGGER.debug("Processing node: {}", evt.getData().getDisplayId());
            
            addNode(objNode);

            // check if owns a mapsTo and add svpComponent port

            ((SbolHView)getSBView()).getTraversal().v((SbolViewObject)evt.getData())
                .e(SBDirection.IN,
                        SynBadTerms.SbolMapsTo.hasLocalInstance, 
                        SynBadTerms.SbolMapsTo.hasRemoteInstance)
                .v(SBDirection.IN)
                .getResult().streamVertexes().forEach( portDef -> {
                    LOGGER.debug("Processing mapsTo port: [{}]", portDef.getIdentity().toASCIIString());
                    SbolPortNode portNode = getPortNode(evt.getData(), (SBViewIdentified<? extends SBIdentified>)portDef);
                    addPin((SBVObjectNode)objNode, portNode);
                });
            
            // check if is participant of interaction and add port

            ((SbolHView)getSBView()).getTraversal().v((SbolViewObject)evt.getData())
                .e(SBDirection.IN, SynBadTerms.SbolInteraction.participant)
                .v(SBDirection.IN)
                .getResult().streamVertexes().forEach(portDef -> {
                    LOGGER.debug("Processing participant port: [{}]", portDef.getIdentity().toASCIIString());
                    SbolPortNode portNode = getPortNode(evt.getData(), (SBViewIdentified<? extends SBIdentified>)portDef);
                    addPin((SBVObjectNode)objNode, portNode);
                });

            // check if is interaction  with participants and add ports;
            
            ((SbolHView)getSBView()).getTraversal().v((SbolViewObject)evt.getData())
                .e(SBDirection.OUT, SynBadTerms.SbolInteraction.hasParticipation)
                .v(SBDirection.OUT)
                .getResult().streamVertexes().forEach(portDef -> {
                    LOGGER.debug("Processing interaction port: [{}]", portDef.getIdentity().toASCIIString());
                    SbolPortNode portNode = getPortNode(evt.getData(), (SBViewIdentified<? extends SBIdentified>)portDef);
                    addPin((SBVObjectNode)objNode, portNode);
                });
            
        } else if(evt.getType() == SBGraphEventType.HIDDEN) {
            
            if(!entityNodeMap.containsKey(evt.getData()))
                return;
            
            // get parent
            
            Widget w = findWidget(entityNodeMap.get(evt.getData())).getParentWidget();
            
            // We can only minimise hierarchy widgets
            
            if(w == null || !(w instanceof SbolHierarchyWidget))
                return;
            
            // if parent is not minimise, do so
            
            SbolHierarchyWidget widget = (SbolHierarchyWidget) w;
            
            if(!widget.getMinimisedState().getBooleanState()) {
                widget.setMinimisedState(true);
            }
            
        } else if (
                evt.getType() == SBGraphEventType.REMOVED) {
            if(entityNodeMap.containsKey(evt.getData()))
                removeNode(entityNodeMap.get(evt.getData()));
            if(portNodeMap.containsKey(evt.getData())) {
                
                // TO DO: Remove ports associated with the node
                // portNodeMap.get(evt.getData()).remove(portNodeMap.get(evt.getData()).stream().filter(p -> p.geto))
            }
        }
    }
    
    public void processEdgeEvent(SBGraphEvent<? extends SBViewEdge> evt) {
        
        LOGGER.debug("Processing edge event: {}", evt);
        
        if( evt.getType() == SBGraphEventType.VISIBLE ||  evt.getType() == SBGraphEventType.ADDED ) {

            SBViewEdge<SbolViewObject, ?, SynBadEdge> edge = (SBViewEdge<SbolViewObject, ?, SynBadEdge>) evt.getData();
         
            if(edge.getEdge().getEdge().equals(URI.create(SynBadTerms.SbolInteraction.participant)) && edge.getFrom().isClass(Participation.class))
               addEdge(new SBVEdgeNode((SBViewEdge)evt.getData()));
            else if(edge.getEdge().getEdge().equals(URI.create(SynBadTerms.SbolMapsTo.hasRemoteInstance)) || edge.getEdge().getEdge().equals(URI.create(SynBadTerms.SbolMapsTo.hasLocalInstance))) {
                
                // only add mapsTo if both to and from are visible
                
                if((!getSBView().isVisible(edge.getFrom()) || !getSBView().isVisible(edge.getTo()))) 
                    return;
                
                addEdge(new SBVEdgeNode((SBViewEdge)evt.getData()));
            }
        } else if( evt.getType() == SBGraphEventType.HIDDEN || evt.getType() == SBGraphEventType.REMOVED || evt.getType() == SBGraphEventType.REMOVED ) {
            if(wireNodeMap.containsKey(evt.getData()))
                removeEdge(wireNodeMap.get(evt.getData()));
        }
        
        /*
        GridGraphLayout<SBVObjectNode, SBVEdgeNode> ggLayout = new  GridGraphLayout<>();
        SceneLayout layout = LayoutFactory.createSceneGraphLayout(this, ggLayout);
        layout.invokeLayoutImmediately();*/
        
        
        //revalidate();
    }

    
    private boolean isTypeOf(ModelEdgeType type, SBViewEdge edge) {
        return ((SBViewEdge<?, ?, SynBadEdge>)edge).getEdge().getEdge().equals(type.getUri());
    }
    
    class PortRecord {
        
        private final SBViewIdentified owner;
        private final SBViewIdentified port;
        
        public PortRecord(SBViewIdentified owner, SBViewIdentified port) {
            this.owner = owner;
            this.port = port;
        }

        public SBViewIdentified getOwner() {
            return owner;
        }

        public SBViewIdentified getPort() {
            return port;
        }
        
    }
 

    
}
