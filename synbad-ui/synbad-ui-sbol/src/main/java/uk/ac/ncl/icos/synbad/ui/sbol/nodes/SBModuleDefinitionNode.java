/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.nodes;

import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.ui.sbol.actions.SBOpenSbolGraphEditor;

public class SBModuleDefinitionNode extends SBTopLevelNode  {

     private Action[] actions = new Action[]{};
     
    
    public SBModuleDefinitionNode(ModuleDefinition definition) {
        super(definition);
        actions = new Action[] {
            new SBOpenSbolGraphEditor(definition)};
    }

    @Override
    public String getName() {
        ModuleDefinition def = getLookup().lookup(ModuleDefinition.class);
        if(!def.getName().isEmpty())
            return def.getName();
        return def.getDisplayId();
    }
    
    @Override
    protected Sheet createSheet() {

        Sheet sheet = super.createSheet();
        Sheet.Set set = Sheet.createPropertiesSet();

        ModuleDefinition md = getLookup().lookup(ModuleDefinition.class);
        
        try {
            
            
            Property nameProp = new PropertySupport.Reflection(getLookup().lookup(SBIdentified.class), String.class, "getName", null);
            nameProp.setName("name");
            nameProp.setDisplayName("Name");
            nameProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(nameProp);
            
            Property parameterTypeProp = new PropertySupport.Reflection(getLookup().lookup(SBIdentified.class), String.class, "getDescription", null);
            parameterTypeProp.setName("description");
            parameterTypeProp.setDisplayName("Description");
            parameterTypeProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(parameterTypeProp);

            Property displayIdProp = new PropertySupport.ReadOnly<String>("getDisplayId", String.class, "Display ID", "Display ID") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return getLookup().lookup(SBIdentified.class).getDisplayId();
                }
            };
            displayIdProp.setName("displayId");
            displayIdProp.setDisplayName("Display ID");
            displayIdProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(displayIdProp);
     
            Property versionProp = new PropertySupport.ReadOnly<String>("getVersion", String.class, "Version", "Version") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return getLookup().lookup(SBIdentified.class).getVersion();
                }
            };
            versionProp.setName("version");
            versionProp.setDisplayName("Version");
            versionProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(versionProp);

        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        

        sheet.put(set);
        return sheet;

    }

    
    @Override
    public Action[] getActions(boolean context) {
        return actions;
    }
    
    @Override
    public Action getPreferredAction() {
       return actions[0];
    } 

    public static class DefinitionChildFactory extends ChildFactory<SbolInstance> implements SBSubscriber {

        private final SBIdentified definition;
       

        public DefinitionChildFactory(SBIdentified definition) {
            this.definition = definition;

//            workspace.getDispatcher().registerHandler(WorkspaceEvent.InstanceEvent.class , this);
 //           workspace.getWorkspace().getDispatcher().addFilter(entity.getIdentity(), this);
        }

        @Override
        protected synchronized boolean createKeys(List<SbolInstance> toPopulate) {
            return true;
        }

        @Override
        protected Node createNodeForKey(SbolInstance key) {
            return new SBNestedNode(key);
        }
        
        
        
        @Override
        public void onEvent(SBEvent e) {
            //refresh(true);
        }


    }
}
