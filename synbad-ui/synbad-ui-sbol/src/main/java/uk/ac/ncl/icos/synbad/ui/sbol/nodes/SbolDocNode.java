/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.sbol.nodes;

import java.awt.Image;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author owengilfellon
 */
public class SbolDocNode extends AbstractNode {

    public SbolDocNode(SbolDocDO doc) {
        super(Children.LEAF, Lookups.fixed(doc));
        super.setIconBaseWithExtension(getIconBase());
    }

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getName() {
        return "[SBOL]: " + getLookup().lookup(SbolDocDO.class).getName();
    }
    

    @Override
    public String getHtmlDisplayName() {
        return getName();
    }

    
    @Override
    public boolean canDestroy() {
        return true;
    }   


}