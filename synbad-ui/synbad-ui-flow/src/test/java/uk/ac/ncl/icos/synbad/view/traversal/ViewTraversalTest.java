/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.traversal;

import java.net.URI;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */
public class ViewTraversalTest {

    private final URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspaceUnmerged");
  //  private final SBWorkspaceManager m;
    private SBWorkspace ws;
    private ModuleDefinition subtilinReceiver;
    
    public ViewTraversalTest() {
        setupWorkspace();
    }
    
    private void setupWorkspace() {
        ws = Lookup.getDefault().lookup(SBWorkspaceManager.class).getWorkspace(workspaceURI);
        subtilinReceiver = ExampleFactory.getSubtilinReporter(ws);
    }
     /*  
    @Test
    public void testGraph() {

        DefaultModel model = new DefaultModel(ws, subtilinReceiver);
    
        //sw.getCursor(def).
        SBViewObject root = model.getView().findViewNodes(subtilinReceiver).iterator().next();
        SBGraph<SBViewObject, SBViewEdge> graph =  new ViewTraversalSource(model.getView())
                .v(root)
                .e(SynBadTerms2.SbolModule.hasModule)
                .v(SBDirection.OUT)
                .e(SynBadTerms2.Sbol.definedBy)
                .v(SBDirection.OUT)
                .e(SynBadTerms2.SbolModule.hasModule)
                .v(SBDirection.OUT)
                .e(SynBadTerms2.Sbol.definedBy)
                .v(SBDirection.OUT).getResult().toGraph();
 
        // Graph contains the paths of all traversers merged into a graph
        
        //assert(graph.edgeSet().size() == 26);
        //assert(graph.nodeSet().size() == 27);    

    }
    
    @Test
    public void testStartFromAllNodes() {
 
       DefaultModel model = new DefaultModel(ws, subtilinReceiver);
        
        new ViewTraversalSource<>(model.getView())
            .v()
            .e(SBDirection.OUT, SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
            .has(SynBadTerms2.Sbol.role, ComponentRole.Terminator.getUri())
            .getResult().streamVertexes().map(o -> o.getObject().getIdentity().toASCIIString()).forEach(System.out::println);

        // Graph contains the paths of all traversers merged into a graph
    }
    
       @Test
    public void testEdges() {
    
        DefaultModel model = new DefaultModel(ws, subtilinReceiver);
        SBViewObject root = model.getView().findViewNodes((SBIdentified)subtilinReceiver).iterator().next();
        SBCursor<SBValued, SynBadEdge> c = ws.getCursor(root, ws.getContextIds());        
        Map<String, Set<SBViewEntity>> map = new ViewTraversalSource(model.getView())
            .v(root)
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
                
            // traverse to definition, filter by role, traverse back and label
            // those that were returned by filter.
                
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
            .has(SynBadTerms2.Sbol.role, URI.create("http://www.synbad.org/Degradation"))
            .e(SBDirection.IN)
            .v(SBDirection.IN)
            .as("Degradation")
            .getResult()
            .getLabelledEntities(SBViewEntity.class);
        
        
       // assert(map.get("Degradation").size() == 3);

        // Graph contains the paths of all traversers merged into a graph
    }
    
    
    @Test
    public void testTraversal() {
       DefaultModel model = new DefaultModel(ws, subtilinReceiver);
        SBViewObject<SBIdentified> root = model.getView().findViewNodes((SBIdentified)subtilinReceiver).iterator().next();
        // At the second level, there should be two promoters - PSpaRK and PSpaS
        
       new ViewTraversalSource<>(model.getView())
            .v(root)
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
                .as("DefinitionsLevel1")
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
                .as("DefinitionsLevel2")
            .has(SynBadTerms2.Sbol.role, ComponentRole.Promoter.getUri())
            
            .getResult()
            .streamVertexes().map(v -> v.getObject().getIdentity().toASCIIString())
            .count();
    }   

    @Test
    public void testLabelling() {
        DefaultModel model = new DefaultModel(ws, subtilinReceiver);
        SBViewObject root = model.getView().findViewNodes(subtilinReceiver).iterator().next();
       
        Map<String, Set<SBViewEntity>> map = new ViewTraversalSource<>(model.getView())
            .v(root)
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
                .as("DefinitionsLevel1")
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
                .as("DefinitionsLevel2")
            .e(SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.OUT)
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
                .as("DefinitionsLevel3")
            .e(SynBadTerms2.SbolModule.hasFunctionalComponent)
            .v(SBDirection.OUT)
                .as("FunctionalComponent")
            .e(SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.OUT)
                .as("ComponentDefinition")
            .getResult()
            .getLabelledEntities(SBViewEntity.class);
       
        assert(!map.get("DefinitionsLevel1").isEmpty());
        assert(!map.get("DefinitionsLevel2").isEmpty());
        assert(!map.get("DefinitionsLevel3").isEmpty());
        assert(!map.get("FunctionalComponent").isEmpty());
        assert(!map.get("ComponentDefinition").isEmpty());    
    }
  */
    
}
