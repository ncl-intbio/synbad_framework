/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svpfragment.nodes;

import org.netbeans.junit.MockServices;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBModelNode;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.Enumeration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.model.DefaultSBModel;
import uk.ac.ncl.icos.synbad.model.SBModel;

import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */
public class EntityNodeTest {

    Logger LOGGER = LoggerFactory.getLogger(EntityNodeTest.class);
    
    private SBModel model;
    private SBWorkspaceManager m;
    private final static String workspace = "http://www.synbad.org/entityNodeTest";
    private SBWorkspace ws;
    
    public EntityNodeTest() {

    }
    
    private SBModel setupWorkspace() {
        ws = m.getWorkspace(URI.create(workspace));
        ModuleDefinition def = ExampleFactory.getSubtilinReporter(ws);
        return new DefaultSBModel(ws, def);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        this.model = setupWorkspace();
    }
    
    @After
    public void tearDown() {
        this.m.closeWorkspace(model.getRootNode().getWorkspace().getIdentity());
    }
    
    @Test
    public void testModelNodes() {
        LOGGER.debug("==================== Model ======================");
        SBModelNode n = new SBModelNode(model);
        System.out.println(n);
        if(n.getChildren()!=null) {
            for(Enumeration<Node> e  = n.getChildren().nodes(); e.hasMoreElements();)  {
                LOGGER.debug("\t + {}", e.nextElement());
            }
        }
    }
    
    @Test
    public void testDefinitionNodes() throws IllegalAccessException, InvocationTargetException {
        /*for(SBDefinition definition : model.getRoot().getObjects(SBDefinition.class)) {
            SBTopLevelNode n = new SBTopLevelNode(definition);
            System.out.println(n.getName());
            for(Enumeration<Node> e  = n.getChildren().nodes(); e.hasMoreElements();)  {
                System.out.println("\t + " + e.nextElement());
            }
            for(PropertySet p : n.getPropertySets()) {
                for(Property property : p.getProperties()) {
                    System.out.println("\t - " + property.getValue() + " : " + property.getDisplayName() + " : " + property.getShortDescription());
                }
            }
            //assert(n.getName().equals(definition.getDisplayId()));
        }*/
    }
    
    @Test
    public void testPointerNodes() throws IllegalAccessException, InvocationTargetException {
        /*for(SBInstance pointer : model.getWorkspace().getObjects(SBInstance.class)) {
            SBPointerNode n = new SBPointerNode(pointer);
            System.out.println(n.getName());
            for(PropertySet p : n.getPropertySets()) {
                for(Property property : p.getProperties()) {
                    System.out.println(property.getValue() + " : " + property.getDisplayName() + " : " + property.getShortDescription());
                }
            }
            //assert(n.getName().equals(pointer.getDisplayId()));
        }*/
    }

  /*
    @Test
    public void TestEntityNode() {
        JFrame frame = new JFrame("EntityNodes");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        TopComponent tc = new ExplorerTopComponent();
        
        frame.setMinimumSize(new Dimension(800, 600));
        frame.getContentPane().add(tc, c);
        tc.setVisible(true);
        frame.pack();
        frame.setVisible(true);
    }
    
    public final class ExplorerTopComponent extends TopComponent implements ExplorerManager.Provider {

        private transient final ExplorerManager manager;
        private final JScrollPane scrollPane;
        
        public ExplorerTopComponent() {
            manager = new ExplorerManager();
            manager.setRootContext(new AbstractNode(Children.LEAF));
            associateLookup(ExplorerUtils.createLookup(manager, getActionMap()));
            scrollPane = new BeanTreeView();
            setLayout(new BorderLayout());
            add(scrollPane, BorderLayout.CENTER);
        }

        @Override
        public ExplorerManager getExplorerManager() {
            return manager;
        }
    }*/
}
