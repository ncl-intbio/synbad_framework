/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.widget;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.RoundRectangle2D;
import java.util.HashMap;
import java.util.Map;

import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.model.ObjectScene;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.netbeans.api.visual.widget.general.IconNodeWidget;
import org.openide.nodes.Node;

import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.editor.widget.EntityWidget;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.ui.flow.nodes.SBVPortNode;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */

public class SequenceComponentWidget extends IconNodeWidget implements PinOwner<SBViewIdentified, PropertiedNode>  {
    
    private static final Image promoter = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/promoter.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);
    private static final Image rbs = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/translational-start-site.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);
    private static final Image operator = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/operator.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);
    private static final Image cds = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/cds.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);
    private static final Image terminator = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/terminator.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);
    private static final Image shim = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/shim24.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);
    private static final Image userdefined = ImageUtilities.loadImage("uk/ac/ncl/icos/synbad/synbad/svpfragment/userdefined.png").getScaledInstance(30, 30, Image.SCALE_SMOOTH);

    private boolean minimised = false;
    private final SBWorkspace ws;
    private final SBVObjectNode node;
    private final SBViewIdentified<? extends SBIdentified> viewObject;
    private final SBIdentified instance;
    private final Scene scene;
    private final ComponentRole role;
    private final Color backgroundColor;
    
    private final Map<SBViewIdentified, Anchor> ports;
    
    private static final Logger logger = LoggerFactory.getLogger(SequenceComponentWidget.class);
    
    private static final SBDataDefManager manager = SBDataDefManager.getManager();

    public SequenceComponentWidget(Scene scene, SBVObjectNode node) 
    {
        super(scene);
        this.scene = scene;
        this.node = node;
        this.instance = (SBIdentified) node.getLookup().lookup(SBViewIdentified.class).getObject();
        this.viewObject = node.getLookup().lookup(SBViewIdentified.class);
        this.ws = instance.getWorkspace();

        SBIdentified identified = instance;
        if(SbolInstance.class.isAssignableFrom(identified.getClass())) {
        }
        
        
        this.role = getComponentRole(identified);
        setIcon();

        getLabelWidget().setFont(new Font("Lucida Sans", Font.PLAIN, 12));
        setLabel(instance.getDisplayId());
        backgroundColor = getBackgroundColor();
      
        
        getLabelWidget().setBorder(BorderFactory.createEmptyBorder(5));
        getLabelWidget().setBackground(scene.getLookFeel().getBackground());
       // getActions().addAction(new DeleteAction());
        getActions().addAction(((ObjectScene)scene).createObjectHoverAction());
        getActions().addAction(((ObjectScene)getScene()).createSelectAction());
       // getActions().addAction(((ObjectScene)scene).create);
        /*
        if(viewObject.isClass(FunctionalComponent.class))
            getActions("default").addAction(ActionFactory.createMoveAction(new MoveStrategy() {
                
                


                @Override
                public Point locationSuggested(Widget widget, Point point, Point point1) {
                    
                    // Force revalidation of scene to update connectionwidgets
                    
                    Widget parent = ((ObjectScene)scene).findWidget()
                            
                            
                            widget.getLookup().lookup(SBViewIdentified.class).getObject()
                    
                    SequenceComponentWidget.this.scene.revalidate();
                    
                    
                    
                    
                    int x = (point1.x - getLeftOffset()) <= 1 ? 1+getLeftOffset(): point1.x;
                    int y = (point1.y - getTopOffset()) <= 1 ? 1+getTopOffset() : point1.y;                
                    return new Point(x, y);
                }}, ActionFactory.createDefaultMoveProvider()));*/

        /*
            getActions().addAction(
                ActionFactory.createMoveAction(
                    ActionFactory.createFreeMoveStrategy(),
                    new SVPMover((ASBGraphPinScene)getScene())));*/
        
        this.ports = new HashMap<>();

    }

    @Override
    public Lookup getLookup() {
        return Lookups.fixed(node, instance, viewObject);
    }
    
    public EntityWidget getNextModuleParent() {
        return (EntityWidget)getParentWidget()
                            .getParentWidget()
                            .getParentWidget()
                            .getParentWidget()
                            .getParentWidget()
                            .getParentWidget()
                            .getParentWidget();
    }
    
    private ComponentRole getComponentRole(SBIdentified definition) {
            ComponentRole role = definition.getValues(SynBadTerms.Sbol.role).stream()
                .filter(v -> v.isURI())
                .filter(v -> manager.containsDefinition(ComponentRole.class, v.asURI().toASCIIString()))
                .map( v -> manager.getDefinition(ComponentRole.class, v.asURI().toASCIIString())).findFirst().orElse(ComponentRole.Generic);

            
            logger.debug("Identified [ {} ] as [ {} ]", definition, role);
            return role;
        
    }
    
    public boolean isSequenceComponent() {
        SBViewIdentified vo = getLookup().lookup(SBViewIdentified.class);
        return !( vo.isClass(Component.class) ||
            (vo.isClass(FunctionalComponent.class) &&
                ((ASBGraphPinScene)getScene()).getSBView().getAllDescendants(vo).isEmpty()));
    }
    
    

    private void setIcon()
    {

        switch (role) {
            case Promoter:
                setImage(promoter);
                break;
            case RBS:
                setImage(rbs);
                break;
            case CDS:
                setImage(cds);
                break;
            case Operator:
                setImage(operator);
                break;
            case Terminator:
                setImage(terminator);
                break;
            case Shim:
                setImage(shim);
                break;
            case Generic:
                setImage(userdefined);
                break;
        }

    }

    
    
    private Color getBackgroundColor()
    {

        if(role == null)
            return null;
        
        switch (role) {
            case Promoter:
                return Color.GREEN;
                
            case RBS:
                return Color.ORANGE;
                
            case CDS:
                return Color.BLUE;
                
            case Operator:
                return Color.YELLOW;
                
            case Terminator:
                return Color.RED;
                
            case Shim:
                 return Color.CYAN;
                
            case Generic:
                return Color.PINK;
                
        }
        
        return null;
        
    } 


    @Override
    public void notifyStateChanged(ObjectState previousState, ObjectState state) {
        if(state.isSelected()) {
            getLabelWidget().setOpaque(true);
             getLabelWidget().setBorder(BorderFactory.createRoundedBorder(5, 5, (Color)getScene().getLookFeel().getBackground(
                    ObjectState.createNormal().deriveObjectHovered(
                            state.isSelected())), Color.gray));
        } else if (state.isWidgetAimed()) {
            getLabelWidget().setOpaque(true);
            getLabelWidget().setBackground(Color.RED);
        }
        else if(state.isWidgetHovered()) {
            getLabelWidget().setOpaque(true);
            getLabelWidget().setBorder(BorderFactory.createRoundedBorder(5, 5, (Color)getScene().getLookFeel().getBackground(
                    ObjectState.createNormal().deriveObjectHovered(
                            state.isHovered())), Color.gray));
        }
        else {
            getLabelWidget().setOpaque(false);
            getLabelWidget().setBorder(BorderFactory.createEmptyBorder(5));
        }
    }
    
    @Override
    protected void paintWidget() {
        super.paintWidget();
        
        
        
        Color gradientColor1 =  backgroundColor;
        Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                gradientColor1.getGreen(),
                gradientColor1.getBlue(),
                gradientColor1.getAlpha() - 150) : null;
        
        if(gradientColor1 != null && gradientColor2 != null) {
            GradientPaint gradient = new GradientPaint(0,0, gradientColor1,0, (getBounds().height), gradientColor2);
            Graphics2D g2 = getGraphics();
            g2.setPaint(gradient);
            g2.fill(new RoundRectangle2D.Float(0, 0, getBounds().width, getBounds().height, 20, 20));
        }
    }

     @Override
     public Widget addPin(PropertiedNode port) {

        SBViewIdentified pInstance = port.getLookup().lookup(SBViewIdentified.class);
        SBPortDirection direction = port.getLookup().lookup(SBPortDirection.class);
        //logger.debug("Adding pin: [ {} ]", pInstance);
        //MapsToPinWidget pin = new MapsToPinWidget(getScene(), port);
        if(SBVPortNode.class.isAssignableFrom(port.getClass())) {
            PortPinWidget pin = new PortPinWidget(scene, (SBVPortNode)port);
            //addChild(pin);
            //if(isSequenceComponent())
                ports.put(pInstance, AnchorFactory.createDirectionalAnchor(this, AnchorFactory.DirectionalAnchorKind.VERTICAL, 20));
            //else 
              //  ports.put(pInstance, AnchorFactory.createDirectionalAnchor(this, AnchorFactory.DirectionalAnchorKind.HORIZONTAL));
              return pin;
        }
        
        return null;
        
    }

    @Override
    public Anchor getAnchor(SBViewIdentified port, boolean internal) {
        logger.debug("Get anchor for: {}", port);
        return ports.get(port);
    }  
    
     @Override
    public String toString() {
        return getLookup().lookup(Node.class).getName();
    }

}
