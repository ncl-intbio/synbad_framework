/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.widget;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JPanel;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;

/**
 *
 * @author owengilfellon
 */
public class GroupBracket extends Widget {
    
    
    private static final Color module_edge = new Color(100, 255, 100);
    public static int HEADER = 0;
    public static int FOOTER = 1;
    final private int curve;
    final private int orientation;
    private boolean hovered = false;

    public GroupBracket(Scene scene, int curve, int orientation) {
        super(scene);
        assert orientation >= 0 && orientation <=1;
        this.curve = curve; 
        this.orientation = orientation;
        setBackground(module_edge);
//        setPreferredSize(new Dimension(20, getParentWidget().getPreferredSize().height));
    }

    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

    @Override
    protected void paintWidget() {
        super.paintWidget(); //To change body of generated methods, choose Tools | Templates.
  
        Graphics2D g2 = getGraphics();
      
        g2.setPaint(hovered ? Color.white : getBackground());
        
        Shape s1;
        Shape s2;
        if(orientation == HEADER) {
           s1 =  new RoundRectangle2D.Double(
                    getBounds().x,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
           s2 =  new Rectangle(
                   (getBounds().x + getBounds().width / 2), 
                   getBounds().y, getBounds().width, getBounds().height);
            
        } else {          
            s1 = new RoundRectangle2D.Double(
                    getBounds().x,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            s2 =  new Rectangle(
                  getBounds().x,
                    getBounds().y,
                    getBounds().x + getBounds().width / 2, 
                    getBounds().height);

            
        }
        
         
        Area positiveArea = new Area(s1);
        Area negativeArea = new Area(s2);
        positiveArea.subtract(negativeArea);
        g2.fill(positiveArea);
        
    }/*
   
 
    @Override
    public void paint(Graphics g) {

        Graphics2D g2 = (Graphics2D)g;
        Shape positiveSpace;
        Shape negativeSpace;
        g2.setPaint(hovered ? getBackground().brighter() : getBackground());
        
        if(orientation == HEADER) {
            positiveSpace = new RoundRectangle2D.Double(
                    getBounds().x,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            negativeSpace = new RoundRectangle2D.Double(
                    getBounds().x + 10,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height, 
                    curve, curve);
            setBounds(getBounds().x,
                    getBounds().y,
                    20,
                    getBounds().height);
        } else {          
            positiveSpace = new RoundRectangle2D.Double(
                    getBounds().x,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            negativeSpace = new RoundRectangle2D.Double(
                    getBounds().x - 10,
                    getBounds().y,
                    getBounds().width,
                    getBounds().height,
                    curve, curve);
            setBounds(getBounds().x,
                    getBounds().y,
                    20,
                    getBounds().height);
        }
        
        Area positiveArea = new Area(positiveSpace);
        Area negativeArea = new Area(negativeSpace);
        positiveArea.subtract(negativeArea);
        g2.fill(positiveArea);
    }*/
}
