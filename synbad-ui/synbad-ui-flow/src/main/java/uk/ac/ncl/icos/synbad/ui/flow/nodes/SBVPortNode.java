/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.nodes;

import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;

import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;

/**
 *
 * @author owengilfellon
 */
public class SBVPortNode extends PropertiedNode  {

    private static final SBDataDefManager manager = Lookup.getDefault().lookup(SBDataDefManager.class);
    
    public SBVPortNode(SBViewPort instance) {
        super(Children.LEAF, Lookups.fixed(instance, instance.getObject()));
        setName(instance.getObject().toString());
        super.setIconBaseWithExtension(getIconBase());
    }
    
    /*
    
    public SBVPortNode(SBViewPort instance, SBView view) {
        super(Children.LEAF, Lookups.fixed(instance, instance.getData(), view));
        setName(instance.getData().toString());
        super.setIconBaseWithExtension(getIconBase());
    }*/

    private String getIconBase()
    {
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
       
        try {
            //final SBIdentified entity = getLookup().lookup(VPort.class).getData().getValue();

            final SBIdentified entity = getLookup().lookup(SBIdentified.class);

            
            set.put(new PropertySupport.ReadOnly<URI>("identity", URI.class, "Identity", "Identity") {
                @Override
                public URI getValue() throws IllegalAccessException, InvocationTargetException {
                    return entity.getIdentity();
                }
            });
            
            set.put(new PropertySupport.ReadOnly<URI>("persistentIdentity", URI.class, "Persistent Identity", "Persistent Identity") {
                @Override
                public URI getValue() throws IllegalAccessException, InvocationTargetException {
                    return entity.getPersistentId();
                }
            });
            
            set.put(new PropertySupport.ReadOnly<String>("version", String.class, "Version", "Version") {
                @Override
                public String getValue() throws IllegalAccessException, InvocationTargetException {
                    return entity.getVersion();
                }
            });
            
            
            Node.Property name = new PropertySupport.Reflection(entity, String.class, "name");
            name.setName("name");
            name.setDisplayName("Name");
            name.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(name);
            
            Node.Property description = new PropertySupport.Reflection(entity, String.class, "description");
            description.setName("description");
            description.setDisplayName("Description");
            description.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(description);
            
            Node.Property displayId = new PropertySupport.Reflection(entity, String.class, "displayID");
            displayId.setName("displayID");
            displayId.setDisplayName("Display ID");
            displayId.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(displayId);

            sheet.put(set);
            
        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return sheet;
    }
    

    @Override
    public PasteType getDropType(Transferable t, final int action, int index) {
        final Node dropNode = NodeTransfer.node( t, DnDConstants.ACTION_COPY_OR_MOVE+NodeTransfer.CLIPBOARD_CUT );
        /*
        if( null != dropNode ) {
            
            final GRNTreeNode node = dropNode.getLookup().lookup(GRNTreeNode.class);
            
            if( node instanceof LeafNode && null != node  && !this.equals( dropNode.getParentNode() )) {
                
                return new PasteType() {
                    
                    @Override
                    public Transferable paste() throws IOException {
                        getChildren().add(new Node[] { new SVPModuleNode(node) } );
                        if( (action & DnDConstants.ACTION_MOVE) != 0 ) {
                            dropNode.getParentNode().getChildren().remove( new Node[] {dropNode} );
                        }
                        return null;
                    }
                };
            }
        }*/
        return null;
    }
    
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    protected void createPasteTypes(Transferable t, List s) {
        super.createPasteTypes(t, s);
        PasteType paste = getDropType( t, DnDConstants.ACTION_COPY, -1 );
        if( null != paste )
            s.add( paste );
    }
    
    @Override
    public Action[] getActions(boolean context) {
        
        List<? extends Action> actions = Utilities.actionsForPath("Actions/ModuleNode");
        return actions.toArray( new Action[actions.size()] );
        
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   

    @Override
    public String toString() {
        return getLookup().lookup(SBViewPort.class).getIdentity().toASCIIString();
    }

    
    
}
