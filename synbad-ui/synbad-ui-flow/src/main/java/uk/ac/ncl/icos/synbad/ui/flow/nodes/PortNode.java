/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.nodes;

import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;

/**
 *
 * @author owengilfellon
 */
public class PortNode extends PropertiedNode  {
 
    public PortNode(SBPortInstance instance) {
        super(Children.LEAF, Lookups.singleton(instance));
    }

    @Override
    public Image getIcon(int type) {
        return super.getIcon(type);
    }

    @Override
    public String getName() {
        return super.getName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    

    @Override
    public String getHtmlDisplayName() {
        SBPort port = getLookup().lookup(SBPortInstance.class).getDefinition();
        String signal = port.isProxy() ? "<font color='0000ff'><i>ProxyPort: </i></font>" : "<font color='0000ff'><i>Port: </i></font>";
        String direction = "<font color='000000'>" + port.getPortDirection() + ":</font>";
        String type = "<font color='000000'>" + port.getPortType() + "</font>";
        String state = port.getPortState() != null ? ":<font color='000000'>" + port.getPortState() + "</font>" : "";
        return signal + direction + type +  state;
    }
 
    @Override
    public Node.Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Node.Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
  
    
    @Override
    public boolean canDestroy() {
        return true;
    }   

}
