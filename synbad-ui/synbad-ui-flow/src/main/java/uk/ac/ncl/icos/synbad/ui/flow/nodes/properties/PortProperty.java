/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.nodes.properties;

import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.PropertySupport;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;

/**
 *
 * @author owengilfellon
 */
public class PortProperty extends PropertySupport.ReadOnly<SBPort> { 

    private final SBPort signal;
    
    public PortProperty(SBPort signal, String name, String displayName, String shortDescription) {
        super(name, SBPort.class, displayName, shortDescription);
        this.signal = signal;
    }

    @Override
    public SBPort getValue() throws IllegalAccessException, InvocationTargetException {
        return signal;
    }
}
