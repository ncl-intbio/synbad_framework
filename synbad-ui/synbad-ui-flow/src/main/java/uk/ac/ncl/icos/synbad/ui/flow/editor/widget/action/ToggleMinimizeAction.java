/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.widget.action;

import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.MinimiseAbility;

/**
 *
 * @author owengilfellon
 */
public class ToggleMinimizeAction extends WidgetAction.Adapter {

    @Override
    public State mouseClicked(Widget widget, WidgetMouseEvent event) {
        if(event.getClickCount()==2) {
            if(widget.getParentWidget() instanceof MinimiseAbility) {
                MinimiseAbility w = (MinimiseAbility) widget.getParentWidget();
                if(w.isCollapsed())
                    w.expandWidget();
                else
                    w.collapseWidget();
            }
        }
        return State.CONSUMED;
    }
}
