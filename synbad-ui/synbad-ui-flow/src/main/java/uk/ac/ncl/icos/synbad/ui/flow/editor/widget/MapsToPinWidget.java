/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.widget;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.LabelWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.ui.flow.editor.style.PinStyle;
import uk.ac.ncl.icos.synbad.ui.flow.editor.style.PinStyles;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.anchor.PinAnchor;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class MapsToPinWidget extends Widget implements Lookup.Provider {

    private final SBDataDefManager defManager = Lookup.getDefault().lookup(SBDataDefManager.class);
    
    private final SBPortDirection direction;
    private boolean connected = false;
    private final SBIdentified port;
    private final PropertiedNode portNode;
    private final List<PinStyle> styles = new ArrayList<>();
    private final LabelWidget label;
    private final PinHoleWidget pinHole;


    public MapsToPinWidget(Scene scene, PropertiedNode portNode) {
        super(scene);
        this.portNode = portNode;
        this.port = (SBIdentified)portNode.getLookup().lookup(SBViewIdentified.class).getObject();
        this.label = new LabelWidget(scene);
        this.label.setMinimumSize(new  Dimension(10, 15));

        /*
        for(Annotation a : port.getValue().getAnnotations(SynBadTerms.Port.portType)) {
           label.setLabel(defManager.getDefinition(PortType.class, a.getURIValue().toASCIIString()).toString());
        }*/
        
        this.direction = portNode.getLookup().lookup(SBPortDirection.class);
        pinHole = new PinHoleWidget(scene, direction);

        if(direction == SBPortDirection.IN) {
            setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.LEFT_TOP, 5));
            addChild(pinHole);
            addChild(label);
        } else {
            setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.RIGHT_BOTTOM, 5));
            addChild(label);
            addChild(pinHole);
        }
        addStyle(new PinStyles.DefaultPinStyle());
        applyStyles();
    }
    
    
    public Anchor getInternalAnchor() {
        Anchor a = new PinAnchor(this, 2, direction, true);
        return a;
    }
    
    public Anchor getExternalAnchor() {
       Anchor a = new PinAnchor(this, 2, direction, false);
       return a;
    }
    
    public boolean isConnected() {
        return connected;
    }
    
    public boolean addStyle(PinStyle style)
    {
       return styles.add(style);
    }
    
    public boolean removeStyle(PinStyle style)
    {
       return styles.remove(style);
    }
    
    public void setIsConnected(boolean connected) {
        this.connected = connected;
        applyStyles();
    }
    
    private void applyStyles() {
        for(PinStyle style : styles)
            style.applyStyle(this);
    }
    
    private class PinHoleWidget extends Widget {
        
        private boolean connected = false;
        private SBPortDirection direction;

        public PinHoleWidget(Scene scene, SBPortDirection direction) {
            super(scene);
            this.direction = direction;
            setPreferredSize(new Dimension(15, 15));
        }
        
        public boolean isConnected() {
            return connected;
        }
        
        public void setIsConnected(boolean connected) {
            this.connected = connected;
        }

        @Override
        protected void paintWidget() {
            super.paintWidget();
            getGraphics().setColor(new Color(0, 255, 0, 100));
           // getGraphics().fill(new Ellipse2D.Float(2, 2, 16, 16));
            getGraphics().setColor(Color.white);
            getGraphics().draw(new Ellipse2D.Float(0, 0, 15, 15));
            getGraphics().draw(new Ellipse2D.Float(2, 2, 11, 11));
        }

    }

    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(port, portNode));
    }
    
    
    
}
