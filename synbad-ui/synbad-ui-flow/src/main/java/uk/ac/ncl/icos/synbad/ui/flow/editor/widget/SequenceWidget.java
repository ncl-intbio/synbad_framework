/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.widget;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.widget.ComponentWidget;
import org.netbeans.api.visual.widget.Scene;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.ui.editor.style.EntityStyle;
import static uk.ac.ncl.icos.synbad.ui.editor.style.HierarchyStyles.asTransparent;
import uk.ac.ncl.icos.synbad.ui.editor.widget.action.DeleteAction;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;



/**
 *
 * @author owengilfellon
 */
public final class SequenceWidget extends Widget
{
    
    private static final Color module = new Color(0, 255, 0, 20);
    private static final Color module_edge = new Color(100, 255, 100);
    private static final Color highlight = Color.white;
    
    private static final Logger logger = LoggerFactory.getLogger(SequenceWidget.class);
    
    private final Lookup lookup;
    private final Widget header;
    //private final Widget headerPanel;
    //private final Widget footerPanel;
    private final Widget footer;
    private final Widget componentList;

    public SequenceWidget(final Scene scene) {
        
        super(scene);
        
        this.lookup = new ProxyLookup(Lookups.fixed(this, scene));
        setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.CENTER, 3));

        this.header = new GroupBracket(scene, 20, GroupBracket.HEADER);
        //this.headerPanel.setSize(new Dimension(20, 80));
        this.header.setOpaque(false);
        this.header.setPreferredSize(new Dimension(20,80));
        addChild(header);
        
        this.componentList = new Widget(scene) {
            @Override
            protected void paintWidget() {
                super.paintWidget();
                setBackground(new Color(255, 255, 255, 50));
                Color gradientColor1 =  getBackground() instanceof Color ? (Color)getBackground() : null;
                Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                        gradientColor1.getGreen(),
                        gradientColor1.getBlue(),
                        30) : null;

                if(gradientColor1 != null && gradientColor2 != null) {
                    GradientPaint gradient = new GradientPaint(0, 0, gradientColor2,0, (getBounds().height), gradientColor1);
                    Graphics2D g2 = getGraphics();
                    g2.setPaint(gradient);
                    g2.fill(new RoundRectangle2D.Float(0, 0, getBounds().width, getBounds().height, 20, 20));
                }
            }
        };
        this.componentList.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 20));
        componentList.setOpaque(false);
        addChild(componentList);
       
        this.footer = new GroupBracket(scene, 20, GroupBracket.FOOTER);
        this.footer.setPreferredSize(new Dimension(20, 80));
        this.footer.setOpaque(false);
        //this.footer = new ComponentWidget(scene, footerPanel);
        //this.footer.setPreferredSize(new Dimension(20,80));
        addChild(footer);
        
        applyStyle();
        
        getActions().addAction(scene.createWidgetHoverAction());
        getActions().addAction(new DeleteAction());
        
        
        getActions().addAction(ActionFactory.createMoveAction());
        
        // ^^^^^ Removed for now, buggy ^^^^^^
        
        //getActions("none").addAction(((InteractionsImpl)getScene()).getSelectAction());
        //getActions("none").addAction(ActionFactory.createAcceptAction(new SVPAcceptProvider()));
        
       // setBorder(getLookup().lookup(Scene.class).getLookFeel().getBorder(ObjectState.createNormal()));
    }

    public void applyStyle()
    {
         Color background = ///isInteractionModule ? i_module :
                    module;
            Color foreground = //isInteractionModule ? i_module_edge : 
                    module_edge;
            
        ObjectState state = getState();
        setBorder(BorderFactory.createRoundedBorder(20, 20, 5, 5, asTransparent(background, 30), foreground));
      //      setBorder(BorderFactory.createLineBorder(5,  foreground));
    
            
        if(state.isSelected()) {
            setBackground(background);
            setForeground(highlight);
            setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(background, 30), highlight));
//            setBorder(BorderFactory.createLineBorder(5, highlight));
        } else {
            setBackground(background);
            setForeground(foreground);
            setBorder(BorderFactory.createRoundedBorder(20, 20, 0, 0, asTransparent(background, 30), foreground));
//            setBorder(BorderFactory.createLineBorder(5,  foreground));
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    public boolean addComponent(Widget widget)
    {
        logger.debug("Adding component widget: [ {} ]", widget.getLookup().lookup(SBViewIdentified.class));
        componentList.addChild(widget);
        return componentList.getChildren().contains(widget);
    }
    
    public boolean addComponent(int index, Widget widget)
    {
        logger.debug("Adding component widget at index {}: [ {} ]", index, widget.getLookup().lookup(SBViewIdentified.class));
        componentList.addChild(index, widget);
        Widget w = componentList.getChildren().get(index);
        return w.equals(widget);
    }

    public List<Widget> getComponents()
    {
        return componentList.getChildren();
    }
    
    public boolean removeComponent(Widget widget)
    {
        logger.debug("Removing component widget: [ {} ]", widget.getLookup().lookup(SBViewIdentified.class));
        componentList.removeChild(widget);
        return !componentList.getChildren().contains(widget);
    }
    
    public boolean isEmpty() {
        return componentList.getChildren().isEmpty();
    }
    
    
     @Override
    protected void paintWidget() {
        super.paintWidget();
        Color gradientColor1 =  getBackground() instanceof Color ? (Color)getBackground() : null;
        Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                gradientColor1.getGreen(),
                gradientColor1.getBlue(),
                gradientColor1.getAlpha() - 10) : null;
        
        if(gradientColor1 != null && gradientColor2 != null) {
            GradientPaint gradient = new GradientPaint(0,0, gradientColor1,0, (getBounds().height), gradientColor2);
            Graphics2D g2 = getGraphics();
            g2.setPaint(gradient);
            g2.fill(new RoundRectangle2D.Float(0, 0, getBounds().width, getBounds().height, 20, 20));
        }
    }

}
