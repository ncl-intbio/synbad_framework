/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.style;

import org.netbeans.api.visual.widget.Widget;


/**
 *
 * @author owengilfellon
 */
public interface PinStyle {
    
    public void applyStyle(Widget widget);
    
}
