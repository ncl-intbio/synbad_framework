/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.widget;

import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 * @param <P> The View Object type of the port used 
 * @param <N> The Node type of the port used
 */
public interface PinOwner<P extends SBViewValued, N extends PropertiedNode> {

    Widget addPin(N port);

    Anchor getAnchor(P port, boolean internal);
    
}
