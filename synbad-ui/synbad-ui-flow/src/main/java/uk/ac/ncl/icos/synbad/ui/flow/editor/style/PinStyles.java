/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.flow.editor.style;

import java.awt.Color;
import org.netbeans.api.visual.border.BorderFactory;
import org.netbeans.api.visual.widget.Widget;

/**
 *
 * @author owengilfellon
 */
public class PinStyles {
    
    public static class DefaultPinStyle implements PinStyle {

        @Override
        public void applyStyle(Widget widget) {
           widget.setBorder(BorderFactory.createRoundedBorder(10, 10, 5, 5, new Color(0, 0, 0, 0), Color.white));
        }
        
    }
    
}
