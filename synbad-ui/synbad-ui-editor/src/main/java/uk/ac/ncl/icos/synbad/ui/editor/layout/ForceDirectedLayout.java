/*
 *   ForceDirectedLayout.java
 *
 *   Copyright (c) 2009 Ruben Laguna <ruben.laguna at gmail.com>. All rights reserved.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.ncl.icos.synbad.ui.editor.layout;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.netbeans.api.visual.graph.layout.GraphLayout;
import org.netbeans.api.visual.graph.layout.UniversalGraph;
import org.netbeans.api.visual.model.ObjectScene;

/**
 *
 * @author ecerulm
 */
public class ForceDirectedLayout<N, E> extends GraphLayout<N, E> {

    private Random random = new Random();
    private boolean firstTime = true;
    
    private double MIN_DISTANCE = 50.0;

    @Override
    protected void performGraphLayout(UniversalGraph<N, E> graph) {
        Collection<N> allNodes = graph.getNodes();
        performNodesLayout(graph, allNodes);
    }
    
    private void firstTimeSetUp(UniversalGraph<N, E> graph, Collection<N> allNodes, ObjectScene scene) {
        List<Point> list = new ArrayList<Point>();
        for (Iterator<N> it = allNodes.iterator(); it.hasNext();) {
            N object = it.next();
            Point point = new Point(random.nextInt(400), random.nextInt(400));
            list.add(point);
        }

        for (int i = 0; i < (list.size() / 2); i++) {
            Point p = list.get(i);

            for (int j = i + 1; j < list.size(); j++) {
                Point p2 = list.get(j);
                double distance = Math.sqrt(Math.pow(p2.x - p.x, 2) + Math.pow(p2.y - p.y, 2));
                if (distance < 40) {
                    p2.translate(30, 30);
                }
            }

        }
        ;

        for (Iterator<N> it = allNodes.iterator(); it.hasNext();) {
            N object = it.next();
            Point p = list.remove(0);
            setResolvedNodeLocation(graph, object, p);
            scene.findWidget(object).setPreferredLocation(p);
        }
        firstTime = false;

        return;
    }

    @Override
    protected void performNodesLayout(UniversalGraph<N, E> graph, Collection<N> nodes) {
        ObjectScene scene = graph.getScene();
        Collection<N> allNodes = nodes;
        Map<N, Point> forceForEachNode = new HashMap<N, Point>();
   
        if (firstTime) {
            firstTimeSetUp(graph, allNodes, scene);
        }

        //initialize forceForEachNode map
        for (Iterator<N> it = allNodes.iterator(); it.hasNext();) {
            N object = it.next();
            forceForEachNode.put(object, new Point(0, 0));
        }


        //Calculate repulsion forces
        for (Iterator<N> it = allNodes.iterator(); it.hasNext();) {
            N thisNode = it.next();
            HashSet<N> allOtherNodes = new HashSet<N>(allNodes);
            allOtherNodes.remove(thisNode);
            for (Iterator<N> it1 = allOtherNodes.iterator(); it1.hasNext();) {
                N otherNode = it1.next();

                Point thisWidget = scene.findWidget(thisNode).getLocation();
                Point otherWidget = scene.findWidget(otherNode).getLocation();
                if ((null != thisWidget) && (null != otherWidget)) {
                    int x = thisWidget.x - otherWidget.x;
                    int y = thisWidget.y - otherWidget.y;
                    double sum = Math.pow(x, 2) + Math.pow(y, 2);
                    double distance = Math.sqrt(sum);
                    double minDist = MIN_DISTANCE;
                    x = ((int) (x * (minDist / distance))); //inversely proportional to the distance
                    y = ((int) (y * (minDist / distance)));
                    //Point force = new Point(x,y);
                    Point currentForce = forceForEachNode.get(thisNode);
                    currentForce.translate(x, y);
                }
            }
        }

        // get got all the repulsion forces.

        //now the attraction forces.
        for (Iterator<N> it = allNodes.iterator(); it.hasNext();) {
            N thisNode = it.next();
            for (E edge : graph.findNodeEdges(thisNode, true, false)) {
                N otherNode = graph.getEdgeTarget(edge);
                if (otherNode != null) {
                    Point thisWidget = scene.findWidget(thisNode).getLocation();
                    Point otherWidget = scene.findWidget(otherNode).getLocation();
                    if ((null != thisWidget) && (null != otherWidget)) {

                        int x = thisWidget.x - otherWidget.x;
                        int y = thisWidget.y - otherWidget.y;
                        double sum = Math.pow(x, 2) + Math.pow(y, 2);
                        double distance = Math.sqrt(sum);
                        double minDist = MIN_DISTANCE;
                        x = -((int) (x * (minDist / distance ))); //directly proportional to the distance
                        y = -((int) (y * ( minDist / distance )));

                        //Point force = new Point(x,y);
                        Point currentForce = forceForEachNode.get(thisNode);
                        currentForce.translate(x, y);
                        currentForce = forceForEachNode.get(otherNode);
                        currentForce.translate(-x, -y);
                    }
                }
            }
        }

        //Use the force  (silly joke)
        for (Iterator<N> it = allNodes.iterator(); it.hasNext();) {
            N object = it.next();
            Point force = forceForEachNode.get(object);
            int x = force.x / 50;
            int y = force.y / 50;
            if (x > 0) {
                x = Math.min(x, 15);
            } else {
                x = Math.max(x, -15);
            }
            if (y > 0) {
                y = Math.min(y, 15);
            } else {
                y = Math.max(y, -15);
            }
           // x = x+((int) (x * (((float)random.nextInt(5)) / 100.0)));
            //y = y+((int) (y * (((float)random.nextInt(5)) / 100.0)));
            x = x + (random.nextInt(7)-3);
            y = y + (random.nextInt(7)-3);


            Point newPosition = scene.findWidget(object).getLocation();
            if (null != newPosition) {
                newPosition.translate(x, y);
                setResolvedNodeLocation(graph, object, newPosition);
            }

        }
    }
}
