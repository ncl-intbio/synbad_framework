/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.editor.style;

import uk.ac.ncl.icos.synbad.ui.editor.widget.EntityWidget;

/**
 *
 * @author owengilfellon
 */
public interface EntityStyle {
    
    public void apply(EntityWidget widget);
    
}
