/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.editor.widget;

import uk.ac.ncl.icos.synbad.ui.editor.widget.action.InterModuleMoveProvider;
import uk.ac.ncl.icos.synbad.ui.editor.widget.action.ClickToFront;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.action.WidgetAction;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.model.ObjectState;
import org.netbeans.api.visual.model.StateModel;
import org.netbeans.api.visual.router.Router;
import org.netbeans.api.visual.router.RouterFactory;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.ui.editor.style.EntityStyle;
import uk.ac.ncl.icos.synbad.ui.editor.style.HierarchyStyles.ComponentStyle;
import uk.ac.ncl.icos.synbad.ui.editor.style.HierarchyStyles.InteractionStyle;
import uk.ac.ncl.icos.synbad.ui.editor.style.HierarchyStyles.ModuleStyle;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 */
public abstract class EntityWidget extends Widget implements  StateModel.Listener, Lookup.Provider {

    private final SBDataDefManager defManager = Lookup.getDefault().lookup(SBDataDefManager.class);
    
    private final SBViewValued<SBIdentified> instance;
    private final SBVObjectNode entityNode;
    protected StateModel minimisedState;
    private List<EntityStyle> styles = new ArrayList<>();
    
    private final Lookup lookup;
    
    private final ASBGraphPinScene scene;

    private final LayerWidget hierarchywindow;
    private final HeaderWidget header;
    private final Widget contents;
    private final Widget entitiesContainer;
    protected final Widget inPinWidget;
    protected final Widget outPinWidget;
    private final LayerWidget entityViews;
    
    private final Router router;
    
//    private final  Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
//            Set<SBVObjectNode> nodes = entityViews.getChildren().stream().map(c -> c.getLookup().lookup(SBVObjectNode.class)).filter(n -> n!=null).collect(Collectors.toSet());
//            hierarchyLayout.layoutNodes(scene, nodes);
//            service.schedule(this, 10, TimeUnit.MILLISECONDS);
//        }
//    };
//    private ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

//    private GraphLayout hierarchyLayout = new ForceDirectedLayout<>();
    //private SceneLayout hierarchyLayout;
    //GridGraphLayout<SBVObjectNode, SBVEdgeNode> ggLayout = new  GridGraphLayout<>();
    
    private static final Logger logger = LoggerFactory.getLogger(EntityWidget.class);


    public EntityWidget(ASBGraphPinScene scene, SBVObjectNode entity) {
        
        super(scene);
        
        this.scene = scene; 
        this.entityNode = entity;
        this.instance = entity.getLookup().lookup(SBViewValued.class);
        this.lookup = new ProxyLookup(Lookups.fixed(this, entityNode, instance));
        
        assert(entityNode != null);
        assert(instance != null);
        

        // ======================================
        // Hierarchy window
        // ======================================
        
        hierarchywindow = new LayerWidget(scene);
        header = new HeaderWidget(scene, instance.getObject());
        contents = new Widget(scene);
        inPinWidget = new Widget(scene);
        entityViews = new LayerWidget(scene);
        entitiesContainer = new Widget(scene);
        outPinWidget = new Widget(scene);

         
        
       // ggLayout = new  GridGraphLayout<>();
       // ggLayout = ggLayout.setChecker(true).setGaps(10, 10);       
       // ggLayout.layoutNodes(scene, entityViews.getChildren().stream().map(c -> c.getLookup().lookup(SBVObjectNode.class)).filter(n -> n!=null).collect(Collectors.toSet()));
       // hierarchyLayout = LayoutFactory.createDevolveWidgetLayout(entityViews, new ModuleLayout(), true);       
        //hierarchyLayout = LayoutFactory.createDevolveWidgetLayout(entityViews, LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 40), true);
//       hierarchyLayout = new ForceDirectedLayout<SBVObjectNode, SBVEdgeNode>();//LayoutFactory.createDevolveWidgetLayout(entityViews, ggLayout, true);
        
       hierarchywindow.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 0));
        //contents.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 20));
        contents.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 20));
        inPinWidget.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 10));
        outPinWidget.setLayout(LayoutFactory.createVerticalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 10));
        
        hierarchywindow.addChild(header);
        hierarchywindow.addChild(contents);
        contents.addChild(inPinWidget);
        contents.addChild(entitiesContainer);
        contents.addChild(outPinWidget);
        
        entitiesContainer.addChild(entityViews);
        //entityViews.setLayout(LayoutFactory.createHorizontalFlowLayout(LayoutFactory.SerialAlignment.JUSTIFY, 50));
        addChild(hierarchywindow);
        
        // ======================================
        // State Model
        // ======================================

        minimisedState = new StateModel();
        minimisedState.setBooleanState(true);
        minimisedState.addListener(this);
        contents.setVisible(!minimisedState.getBooleanState());

        // ======================================
        // Actions
        // ======================================
        
        if(instance.is(Module.TYPE) || instance.is(ModuleDefinition.TYPE))
            addStyle(new ModuleStyle());
        else if(instance.is(Component.TYPE) || instance.is(FunctionalComponent.TYPE))
            addStyle(new ComponentStyle());
        else if(instance.is(Interaction.TYPE))
            addStyle(new InteractionStyle());

        createActions("default");
        getActions().addAction(new ClickToFront());
        getActions("default").addAction(scene.createObjectHoverAction());
        getActions("default").addAction(scene.createSelectAction());
        getActions().addAction(ActionFactory.createActionMapAction());
        getActions("default").addAction(ActionFactory.createResizeAction());
        createActions("move");
        getActions("move").addAction(ActionFactory.createMoveAction(
            ActionFactory.createFreeMoveStrategy(),
            new InterModuleMoveProvider(scene, this)));
     
            header.getActions().addAction(new WidgetAction.Adapter(){
                
                @Override
                public WidgetAction.State mouseClicked(Widget widget, WidgetAction.WidgetMouseEvent event) {
                    
                    if(event.getClickCount()==2) {
                        logger.debug("Toggling [ {} ]", instance);
                        if(scene.getSBView().getAllChildren(instance).isEmpty())
                            return WidgetAction.State.REJECTED;  
                        minimisedState.toggleBooleanState();
                        return WidgetAction.State.CONSUMED;   
                    } else {
                        return WidgetAction.State.REJECTED;  
                    }
                }
            });
       

        getActions("default").addAction(
            ActionFactory.createMoveAction((Widget widget, Point point, Point point1) -> {
                // Force revalidation of scene to update connectionwidgets
                EntityWidget.this.scene.revalidate();
                int x = (point1.x - getLeftOffset()) <= 1 ? 1+getLeftOffset(): point1.x;
                int y = (point1.y - getTopOffset()) <= 1 ? 1+getTopOffset() : point1.y;
                return new Point(x, y); }, 
            ActionFactory.createDefaultMoveProvider())
        );

        this.router = RouterFactory.createOrthogonalSearchRouter(getEntitiesLayer());
        applyStyle();
        initSetup();
    }

    public Router getRouter() {
        return router;
    }

    public SBVObjectNode getEntityNode() {
        return entityNode;
    }
    
    public void setMinimisedState(boolean state) {
        minimisedState.setBooleanState(state);
    }

    public StateModel getMinimisedState() {
        return minimisedState;
    }

    public void initSetup() {
      // hierarchyLayout.invokeLayout();
        Set<SBVObjectNode> nodes = entityViews.getChildren().stream().map(c -> c.getLookup().lookup(SBVObjectNode.class)).filter(n -> n!=null).collect(Collectors.toSet());
        //logger.debug("Laying out {} nodes", nodes.size());
       // if(nodes.size() > 0)
//           hierarchyLayout.layoutNodes(scene, nodes);
       
    }

    public boolean addStyle(EntityStyle style)
    {
       return styles.add(style);
    }
    
    public boolean removeStyle(EntityStyle style)
    {
       return styles.remove(style);
    }
    
    public void applyStyle()
    {
        for(EntityStyle style : styles) {
            style.apply(this);
        }
    }

    public HeaderWidget getHeaderWidget()
    {
        return header;
    }
    
    public Widget getHierarchyView() {
        return hierarchywindow;
    }

    
    public abstract  void addWidget(Widget widget);

    public LayerWidget getEntitiesLayer() {
        return entityViews;
    }

    @Override
    public void stateChanged() {
        SBHCursor c = scene.getSBView().createCursor(entityNode.getLookup().lookup(SBViewValued.class));
       
        if(minimisedState.getBooleanState() && !scene.getSBView().isContracted(c.getCurrentPosition())) {
            logger.debug("Collapsing: [ {} ]", instance);
            c.collapse();
          //  service.shutdown();
        } else if(!minimisedState.getBooleanState() && scene.getSBView().isContracted(c.getCurrentPosition())){
            logger.debug("Expanding: [ {} ]", instance);
             c.expand();
           //  if(service.isShutdown())
            //     service = Executors.newSingleThreadScheduledExecutor();
            // hierarchyLayout = new ForceDirectedLayout();
             //service.schedule(runnable, 10, TimeUnit.MILLISECONDS);
        }
           
       // hierarchyLayout.invokeLayout();
        
        contents.setVisible(!minimisedState.getBooleanState());/**/
        initSetup();
    }
    
    public Anchor getInAnchor() {
        return AnchorFactory.createDirectionalAnchor(this, AnchorFactory.DirectionalAnchorKind.HORIZONTAL);

    }
    
    public Anchor getOutAnchor() {
        return AnchorFactory.createDirectionalAnchor(this, AnchorFactory.DirectionalAnchorKind.HORIZONTAL);
    }

   
    @Override
    protected void notifyStateChanged(ObjectState previousState, ObjectState state) {
        super.notifyStateChanged(previousState, state); //To change body of generated methods, choose Tools | Templates.
        applyStyle();
    }
    
    protected int getTopOffset() {
        return getBorder().getInsets().top;
    }
    protected int getLeftOffset() {
        return getBorder().getInsets().left;
    }

    @Override
    protected void paintWidget() {
        super.paintWidget();
        Color gradientColor1 =  getBackground() instanceof Color ? (Color)getBackground() : null;
        Color gradientColor2 =  gradientColor1 != null ? new Color(gradientColor1.getRed(),
                gradientColor1.getGreen(),
                gradientColor1.getBlue(),
                gradientColor1.getAlpha() - 100) : null;
        
        if(gradientColor1 != null && gradientColor2 != null) {
            GradientPaint gradient = new GradientPaint(0,0, gradientColor1,0, (getBounds().height), gradientColor2);
            Graphics2D g2 = getGraphics();
            g2.setPaint(gradient);
            g2.fill(new RoundRectangle2D.Float(0, 0, getBounds().width, getBounds().height, 20, 20));
        }
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }
    
     @Override
    public String toString() {
        return getLookup().lookup(Node.class).getName();
    }
}
