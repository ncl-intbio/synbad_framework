/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.editor.layout;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.visual.layout.Layout;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 */
public class ModuleLayout implements Layout {
    
    private int max_width = 800;
    private int spacing = 200;
    
    @Override
    public void layout (Widget widget) {
        justify(widget);
    }

    @Override
    public boolean requiresJustification (Widget widget) {
        return true;
    }

    @Override
    public void justify (Widget widget) {
        
        Rectangle bounds = widget.getClientArea();
        List<Widget> children = widget.getChildren();
        
        List<Widget> interactions = new ArrayList<>();
        List<Widget> functionalComponents = new ArrayList<>();
        List<Widget> modules = new ArrayList<>();
        
        
        
        for(Widget w : children) {
            SBViewValued o = w.getLookup().lookup(SBViewValued.class);
            if(o.isClass(Interaction.class))
                interactions.add(w);
            else if (o.isClass(FunctionalComponent.class))
                functionalComponents.add(w);
            else if (o.isClass(Module.class))
                modules.add(w);
        }
        
        int index = 0;
        int row = 0;
        
        for(Widget w : interactions) {
            Point p = new Point(index++ * spacing, row * spacing);
            w.resolveBounds(p, new Rectangle(p, w.getBounds().getSize()));
            if(max_width > index * 400) {
                row ++;
                index = 0;
            }
                
        }
        
        for(Widget w : functionalComponents) {
            Point p = new Point(index++ * spacing, row * spacing);
            w.resolveBounds(p, new Rectangle(p, w.getBounds().getSize()));
            if(max_width > index * spacing) {
                row ++;
                index = 0;
            }
                
        }
        
        for(Widget w : modules) {
            Point p = new Point(index++ * 400, row * 400);
            w.resolveBounds(p, new Rectangle(p, w.getBounds().getSize()));
            if(max_width > index * 400) {
                row ++;
                index = 0;
            }
                
        }
        
                    
            index++;
            

    }
}
