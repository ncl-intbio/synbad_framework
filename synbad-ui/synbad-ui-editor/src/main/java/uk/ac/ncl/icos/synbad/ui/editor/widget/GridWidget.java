/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.editor.widget;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import org.netbeans.api.visual.widget.LayerWidget;
import org.netbeans.api.visual.widget.Scene;

/**
 *
 * @author owengilfellon
 */
public class GridWidget extends LayerWidget {
    
    int gridDistance = 20;
    int zoom = 1;
    int gridUnits = 10;
    double oldZoomFactor = 0;
    double zoomFactor = 0;
    int zoomedDistance = 0;

    public GridWidget(Scene scene) {
        super(scene);
        setOpaque(true);
        setVisible(true);
       
    }

    @Override
    protected void paintWidget() {
        super.paintWidget();//To change body of generated methods, choose Tools | Templates.

        oldZoomFactor = zoomFactor;
        zoomFactor = getScene().getZoomFactor();
        
        Graphics2D g = getGraphics();
        g.setStroke(new BasicStroke((float)(1.0 / zoomFactor)));
        
        if(oldZoomFactor != zoomFactor) {
            if((gridDistance * zoom) * zoomFactor <  (gridDistance / 2) )
                zoom = zoom * 2;
        
            if((gridDistance * zoom) * zoomFactor >  (gridDistance * 2) && zoom > 1)
                zoom = zoom / 2;
            
            zoomedDistance = gridDistance * zoom;
            
            
        }

        int minX = getScene().getBounds().x;
        int maxX = minX + getScene().getBounds().width;
        int minY = getScene().getBounds().y;
        int maxY = minY + getScene().getBounds().height;

        g.setColor(Color.darkGray);
        g.fillRect(minX, minY, maxX, maxY);
        g.setColor(Color.darkGray.brighter());
        
        for(int x = 0; x < (maxX - minX); x += zoomedDistance) {
            if(x % (zoomedDistance * 10) == 0) {
                g.setColor(new Color(Color.orange.getRed(), Color.orange.getGreen(), Color.orange.getBlue(), 100));
                g.drawLine(x + minX, minY, x + minX, maxY);
                g.setColor(Color.darkGray.brighter());
            } else {
                g.drawLine(x + minX, minY, x + minX, maxY);
            }
        } 
        for(int y = 0; y < (maxY - minY); y += zoomedDistance) {

            if(y % (zoomedDistance * 10) == 0) {
                g.setColor(new Color(Color.orange.getRed(), Color.orange.getGreen(), Color.orange.getBlue(), 100));
                g.drawLine(minX, y + minY, maxX, y + minY);
                g.setColor(Color.darkGray.brighter());
            }
            else {
                g.drawLine(minX, y + minY, maxX, y + minY);
            }
        }
            
    }
    
}
