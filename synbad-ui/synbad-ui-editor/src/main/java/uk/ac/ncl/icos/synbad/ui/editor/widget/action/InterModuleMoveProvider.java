/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.editor.widget.action;

import java.awt.Point;
import org.netbeans.api.visual.action.MoveProvider;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.editor.widget.EntityWidget;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;


/**
 *
 * @author owengilfellon
 */
public class InterModuleMoveProvider implements MoveProvider {

    private Point point = null;
    private ASBGraphPinScene scene;
    private EntityWidget parent;
    
    public InterModuleMoveProvider(ASBGraphPinScene scene, EntityWidget parent) {
        this.scene = scene;
    }

    @Override
    public void movementStarted(Widget widget) {
        widget.getParentWidget().removeChild(widget);
        scene.getMovementLayer().addChild(widget);
        widget.setPreferredLocation(point);
    }

    @Override
    public void movementFinished(Widget widget) {
        scene.getMovementLayer().removeChild(widget);
        Widget newParent = null;
        Point newPos = null;

        for(Object node : scene.getNodes()) {
            Widget w =  scene.findWidget(node);

            if(w != widget && w.isHitAt(w.convertSceneToLocal(point))) {
                newPos = w.convertSceneToLocal(point);
                newParent = scene.findWidget(node);
            }
        }

        if(newParent!=null) {
            SBVObjectNode n = (SBVObjectNode)scene.findObject(widget);
            SBIdentified instance = (SBIdentified) n.getLookup().lookup(SBViewValued.class).getObject();

            if(parent.getLookup().lookup(SBViewValued.class).getObject() != instance) {

                ((EntityWidget)newParent).addWidget((EntityWidget)widget);
                widget.setPreferredLocation(widget.getParentWidget().convertSceneToLocal(point));

                return;
            } 
        }

        scene.getEntitiesLayer().addChild(widget);
    }

    @Override
    public Point getOriginalLocation(Widget widget) {
        point = widget.getParentWidget().convertLocalToScene(widget.getLocation());
        return point;
    }

    @Override
    public void setNewLocation(Widget widget, Point location) {
        point = location;
        widget.setPreferredLocation(location);
    }
        
    
}
