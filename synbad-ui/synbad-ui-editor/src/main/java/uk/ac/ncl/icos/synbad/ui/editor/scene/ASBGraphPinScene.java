/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.editor.scene;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.ui.editor.widget.GridWidget;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.graph.GraphPinScene;
import org.netbeans.api.visual.layout.LayoutFactory;
import org.netbeans.api.visual.widget.LayerWidget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.view.SBHView;

/**
 *
 * @author owengilfellon
 */
public abstract class ASBGraphPinScene<N, E, P, V extends SBHView>  //SBPView<? extends SBIdentified, ? extends SBIdentified, ? extends SynBadEdge,
      //  ? extends SBViewComponent<? extends SBIdentified>, ? extends ViewEdge<? extends SBIdentified, ? extends SynBadEdge>, ? extends SBViewPort<? extends SBIdentified>>> 

    extends GraphPinScene<N, E, P> implements SBSubscriber {

    private final GridWidget background = new GridWidget(this);
    private final LayerWidget entities = new LayerWidget(this);
    private final LayerWidget movement = new LayerWidget(this);
    private final LayerWidget connections = new LayerWidget(this);
    private final V view;
    private final SBWorkspace ws;
    
    private static final Logger logger = LoggerFactory.getLogger(ASBGraphPinScene.class);

    public ASBGraphPinScene(V view ) {
        super();
        setLayout(LayoutFactory.createOverlayLayout());
        addChild(background);
        addChild(entities);
        addChild(connections);
        addChild(movement);
        getActions().addAction(ActionFactory.createWheelPanAction());
        getActions().addAction(ActionFactory.createMouseCenteredZoomAction(1.1));
        this.view = view;
        this.ws = SBValued.class.isAssignableFrom(view.getRoot().getClass()) ?
                ((SBValued)view.getRoot()).getWorkspace() :
                null;
    }

    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(view));
    }

    public LayerWidget getMovementLayer(){
        return movement;
    }
    
    public LayerWidget getEntitiesLayer(){
        return entities;
    }
    
    public LayerWidget getConnectionsLayer(){
        return entities;
    }

    public V getSBView() {
        return view;
    }
    
    public SBWorkspace getWorkspace() {
        return ws;
    }

}
