/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.vpr.interactionproperties.nodes;

import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.io.IOException;
import java.util.List;
import javax.swing.Action;
import org.openide.actions.NewAction;
import org.openide.actions.PasteAction;
import org.openide.actions.ReorderAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.Sheet;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.icos.synbad.ui.vpr.dnd.PartTransferable;
import uk.ac.ncl.icos.synbad.ui.vpr.nodes.VprPartNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class SVPInternalEventNode extends AbstractNode {
    
    public SVPInternalEventNode(Interaction interaction) {     
        super(Children.LEAF, new ProxyLookup(Lookups.fixed(interaction)));
        setName(getLookup().lookup(Interaction.class).getName());
        setDisplayName(getLookup().lookup(Interaction.class).getName());
    }

    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        /*
        GRNTreeNode to = getLookup().lookup(GRNTreeNode.class);
        GRNTreeNode from = getLookup().lookup(SVPPartNode.class).getLookup().lookup(GRNTreeNode.class);
        
        List <Interaction> interactions = SVPManager.getSVPManager().getInteractions(from.getSVP(), to.getSVP());
        assert(interactions.size() == 1);
        
        for (Interaction interaction : interactions)
        {
            try {

                interaction.getDescription();
                interaction.getFreeTextMath();
                interaction.getIsInternal();
                interaction.getIsReaction();
                interaction.getIsReversible();
                interaction.getMathName();
                interaction.getInteractionType();
                List<Parameter> parameters = interaction.getParameters();
                for(Parameter p : parameters) {
                    p.getName();
                    p.getParameterType();
                    p.getValue();
                    p.getScope();
                    p.getEvidenceType();
                }
                
                Property interactionType = new PropertySupport.Reflection(interaction, String.class, "getInteractionType", null);
                interactionType.setName("interactionType");
                interactionType.setDisplayName("Type");
                interactionType.setValue("suppressCustomEditor", Boolean.TRUE);
                set.put(interactionType);

            } catch (NoSuchMethodException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

        sheet.put(set);*/
        return sheet;


    }
    

    @Override
    public PasteType getDropType(Transferable t, final int action, int index) {
        final Node dropNode = NodeTransfer.node( t, DnDConstants.ACTION_COPY_OR_MOVE+NodeTransfer.CLIPBOARD_CUT );
        
        if( null != dropNode ) {
            
            final Part node = dropNode.getLookup().lookup(Part.class);
            
            if(null != node ) {
                
                return new PasteType() {
                    
                    @Override
                    public Transferable paste() throws IOException {
                        getChildren().add(new Node[] { new VprPartNode(node) } );
                        if( (action & DnDConstants.ACTION_MOVE) != 0 ) {
                            dropNode.getParentNode().getChildren().remove( new Node[] {dropNode} );
                        }
                        return null;
                    }
                };
            }
        }
        return null;
    }
    
    @Override
    public Transferable drag() throws IOException {
        return new PartTransferable(new VprPartNode(getLookup().lookup(Part.class)));
    }
    
    @Override
    public Cookie getCookie(Class clazz) {
        Children ch = getChildren();
        
        if (clazz.isInstance(ch)) {
            return (Cookie) ch;
        }
        
        return super.getCookie(clazz);
    }
    
    @Override
    protected void createPasteTypes(Transferable t, List s) {
        super.createPasteTypes(t, s);
        PasteType paste = getDropType( t, DnDConstants.ACTION_COPY, -1 );
        if( null != paste )
            s.add( paste );
    }
    
    @Override
    public Action[] getActions(boolean context) {
        return new Action[] {
            SystemAction.get( NewAction.class ),
            SystemAction.get( PasteAction.class ),
            SystemAction.get( ReorderAction.class)};
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   
}