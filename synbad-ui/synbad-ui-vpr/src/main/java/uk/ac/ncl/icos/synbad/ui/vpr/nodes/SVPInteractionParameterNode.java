/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.vpr.nodes;

import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

/**
 *
 * @author owengilfellon
 */
public class SVPInteractionParameterNode extends AbstractNode {
    
    private final Parameter parameter;
    
    public SVPInteractionParameterNode(Parameter parameter) {     
        super(Children.LEAF, new ProxyLookup(Lookups.fixed(parameter)));
        setName(parameter.getName());
        setDisplayName(parameter.getName());
        this.parameter = parameter;
    }
 
    @Override
    protected Sheet createSheet() {

        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        try {
            Property nameProp = new PropertySupport.Reflection(parameter, String.class, "getName", null);
            nameProp.setName("name");
            nameProp.setDisplayName("Name");
            nameProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(nameProp);
            
            Property parameterTypeProp = new PropertySupport.Reflection(parameter, String.class, "getParameterType", null);
            parameterTypeProp.setName("type");
            parameterTypeProp.setDisplayName("Type");
            parameterTypeProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(parameterTypeProp);

            Property valueProp = new PropertySupport.Reflection(parameter, Double.class, "getValue", null);
            valueProp.setName("value");
            valueProp.setDisplayName("Value");
            valueProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(valueProp);

            Property evidenceTypeProp = new PropertySupport.Reflection(parameter, String.class, "getEvidenceType", null);
            evidenceTypeProp.setName("evidenceType");
            evidenceTypeProp.setDisplayName("Evidence Type");
            evidenceTypeProp.setValue("suppressCustomEditor", Boolean.TRUE);
            set.put(evidenceTypeProp);

        } catch (NoSuchMethodException ex) {
            Exceptions.printStackTrace(ex);
        }
        

        sheet.put(set);
        return sheet;

    }
    
    @Override
    public Action[] getActions(boolean context) {
        return new Action[] {};
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }   
}