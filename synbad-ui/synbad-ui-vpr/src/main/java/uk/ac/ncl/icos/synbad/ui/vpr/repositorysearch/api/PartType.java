/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.vpr.repositorysearch.api;

/**
 *
 * @author owengilfellon
 */
public enum PartType {
    Promoter("Promoter"),
    Operator("Operator"),
    RBS("RBS"),
    FunctionalPart("FunctionalPart"),
    Shim("Shim"),
    Terminator("Terminator"),
    None("None");
    
    private String text;
    
    private PartType(String text)
    {
        this.text = text;
    }
    
    public static PartType fromString(String text)
    {
        if (text != null) {
            for (PartType o : PartType.values()) {
                if (text.equalsIgnoreCase(o.text)) {
                  return o;
                }
            }
        }
        
        return PartType.None;
    }

    @Override
    public String toString() {
        return text;
    }
}
