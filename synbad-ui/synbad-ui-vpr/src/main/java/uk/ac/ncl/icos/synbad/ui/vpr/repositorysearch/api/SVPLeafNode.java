/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.vpr.repositorysearch.api;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.lookup.Lookups;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;



/**
 *
 * @author owengilfellon
 */
public class SVPLeafNode extends AbstractNode {
        
    public SVPLeafNode(SBIdentified definition) {
        super(Children.LEAF, Lookups.singleton(definition));
    }
    
}
