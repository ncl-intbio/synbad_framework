/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.editor.widget;

import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.PortPinWidget;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorFactory;
import org.netbeans.api.visual.model.StateModel;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.editor.widget.EntityWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.PinOwner;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.ui.flow.nodes.SBVPortNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;

/**
 *
 * @author owengilfellon
 */
public class SvpHierarchyWidget extends EntityWidget implements  StateModel.Listener, Lookup.Provider, PinOwner<SBViewPort, SBVPortNode> {

    private static final Logger logger = LoggerFactory.getLogger(SvpHierarchyWidget.class);
    
    private final Map<SBViewPort, Anchor> externalInPorts;
    private final Map<SBViewPort, Anchor> externalOutPorts;
    private final Map<SBViewPort, Anchor> internalInPorts;
    private final Map<SBViewPort, Anchor> internalOutPorts;
    
    public SvpHierarchyWidget(ASBGraphPinScene scene, SBVObjectNode entity) {
        super(scene, entity);
        this.externalInPorts = new HashMap<>();
        this.externalOutPorts = new HashMap<>();
        this.internalInPorts = new HashMap<>();
        this.internalOutPorts = new HashMap<>();
        // getActions().addAction(ActionFactory.createAcceptAction(new EntityAcceptProvider()));
    }
    
    @Override
    public void addWidget(Widget widget)
    {
        logger.debug("Adding widget [ {} ] to [ {} ]", widget.getLookup().lookup(SBViewIdentified.class), getLookup().lookup(SBViewIdentified.class));
        getEntitiesLayer().addChild(widget);
    }

    public void removeWidget(Widget widget) {
        logger.debug("Removing widget [ {} ] from [ {} ]", widget.getLookup().lookup(SBViewIdentified.class), getLookup().lookup(SBViewIdentified.class));
        getEntitiesLayer().removeChild(widget);
    }
    
     public Widget addPin(SBVPortNode port) {

        SBViewPort pInstance = port.getLookup().lookup(SBViewPort.class);
        logger.debug("Adding pin: [ {} ]", pInstance);
        PortPinWidget pin = new PortPinWidget(getScene(), port);

        if(pInstance.getDirection() == SBPortDirection.IN) {
            inPinWidget.addChild(pin); 
            externalInPorts.put(pInstance, pin.getExternalAnchor());
            internalInPorts.put(pInstance, pin.getInternalAnchor());
        } else {
            outPinWidget.addChild(pin);
            externalOutPorts.put(pInstance, pin.getExternalAnchor());
            internalOutPorts.put(pInstance, pin.getInternalAnchor());
        }

        return pin;
    }

    public Anchor getAnchor(SBViewPort port, boolean internal) {
        
        if(internal) {
            if(port.getDirection() == SBPortDirection.IN)
                return AnchorFactory.createProxyAnchor(minimisedState, internalInPorts.get(port), getInAnchor());
            else
                return AnchorFactory.createProxyAnchor(minimisedState, internalOutPorts.get(port), getOutAnchor());
        } else {
            if(port.getDirection() == SBPortDirection.IN)
                return AnchorFactory.createProxyAnchor(minimisedState, externalInPorts.get(port), getInAnchor());
            else
                return AnchorFactory.createProxyAnchor(minimisedState, externalOutPorts.get(port), getOutAnchor());
        }/**/

    }  

    @Override
    public String toString() {
        return getLookup().lookup(Node.class).getName();
    }

    


}
