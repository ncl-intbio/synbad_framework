/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.editor.scene;

import org.netbeans.api.visual.widget.Widget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;

import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModelEdgeType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceComponentWidget;
import uk.ac.ncl.icos.synbad.ui.svp.editor.widget.SvpHierarchyWidget;
import uk.ac.ncl.icos.synbad.ui.svp.editor.widget.SvpModuleWidget;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVEdgeNode;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.ui.flow.nodes.SBVPortNode;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;

/**
 *
 * @author owengilfellon
 */
public class SvpScene extends SBHFlowScene<SvVo, SvpWire, SvpViewPort, SvpPView> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpScene.class);
    
    public SvpScene( SvpPView view ) {
        super(view);
        //addNode(new SBVObjectNode(view.getRoot()));
    }
    
    
    public boolean isSequenceComponent(SvVo obj) {
        return obj.is(Component.TYPE) || ( obj.is(FunctionalComponent.TYPE) && getSBView().getAllChildren(obj).isEmpty());
    }
    
    public boolean isEntityComponent(SvVo obj) {
        return obj.is(Interaction.TYPE) || obj.is(Module.TYPE) || obj.is(ModuleDefinition.TYPE) ||
                 (obj.is(FunctionalComponent.TYPE) && !getSBView().getAllChildren(obj).isEmpty());
    }
            
                
    public boolean hasProxyWire( SvmVo parent, SBViewComponent object) {

       return parent.getPorts().stream().filter(p -> p.isProxy())
               .anyMatch(p -> p.getProxied().getOwner().equals(object));
        
        
    }
    @Override
    protected Widget attachNodeWidget(SBVObjectNode n) {
        
        LOGGER.trace("SvpScene: Attaching node : [ {} ]", n);
        
        SvVo obj = n.getLookup().lookup(SvVo.class);
        
        if(obj == null) {
            LOGGER.error("SvpScene: Could not retrieve ViewObject from: [ {} ]", n);
            return null;
        }

        Widget e = null;

        // Adding Sequence Component
        
        if(isSequenceComponent(obj) ) {

            e = new SequenceComponentWidget(this, n);
            
            SvVo p = getSBView().getParent(obj);
            
            if(p == null) {
                LOGGER.error("SvpScene: Could not find parent in view for {}", obj.getDisplayId());
                return null;
            }
            
            Widget parent = findWidget(entityNodeMap.get(p));

            if(parent == null) {
                LOGGER.error("SvpScene: Could not find parent widget for: [ {} ]", p.getName());
                return null;
            }

            if(!SvpModuleWidget.class.isAssignableFrom(parent.getClass())) {
                LOGGER.error("SvpScene: Widget for {} is not an SvpModuleWidget", p.getName());
                return null;
            }
                
            LOGGER.debug("SvpScene: Adding node [ {} ] to [ {} ]", obj, parent.getLookup().lookup(SvVo.class));
            ((SvpModuleWidget)parent).addWidget((SequenceComponentWidget)e);   

//            if(hasProxyWire((SvmVo)p, obj)) {
//                
//            }
    
        } 
        
        // adding entity widget
        
        else if(isEntityComponent(obj)) {   
            
            LOGGER.trace("SvpScene: Creating module widget for : [ {} ]", obj);
            e = new SvpModuleWidget(this, n);
            
            if(obj == getSBView().getRoot()) {    
                
                LOGGER.debug("SvpScene: Adding as root : [ {} ]", obj);
                getEntitiesLayer().addChild(e);
                getEntitiesLayer().revalidate();
                
            } else {
                
                SvVo p = getSBView().getParent(obj);
            
                if(p == null) {
                    LOGGER.error("SvpScene: Could not find parent in view for {}", obj.getDisplayId());
                    return null;
                }

                Widget parent = findWidget(entityNodeMap.get(p));

                if(parent == null) {
                    LOGGER.error("SvpScene: Could not find parent widget for: [ {} ]", p.getName());
                    return null;
                }
                
                if(!SvpHierarchyWidget.class.isAssignableFrom(parent.getClass())) {
                    LOGGER.error("SvpScene: Widget for {} is not an SvpModuleWidget", p.getName());
                    return null;
                }
                
                LOGGER.debug("SvpScene: Adding node [ {} ] to [ {} ]", n.getName(), p.getDisplayId());

                ((SvpHierarchyWidget)parent).addWidget((SvpHierarchyWidget)e);
            
            }
        }

        
        entityNodeMap.put(obj, n);
        return  e;
    }

    @Override
    protected Widget attachEdgeWidget(SBVEdgeNode e) {
         LOGGER.debug("SvpScene: Adding edge [ {} ]", e.getName());
        return super.attachEdgeWidget(e); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Widget attachPinWidget(SBVObjectNode n, PropertiedNode p) {
         LOGGER.debug("SvpScene: Adding pin [ {} ]", p.getName());
        return super.attachPinWidget(n, p); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    protected void detachNodeWidget(SBVObjectNode node, Widget widget) {
        LOGGER.debug("SvpScene: Removing [ {} ]", node.getName());
        super.detachNodeWidget(node, widget); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void detachEdgeWidget(SBVEdgeNode edge, Widget widget) {
        LOGGER.debug("SvpScene: Removing [ {} ]", edge.getName());
        super.detachEdgeWidget(edge, widget); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void detachPinWidget(PropertiedNode pin, Widget widget) {
        LOGGER.debug("SvpScene: Removing [ {} ]", pin.getName());
        super.detachPinWidget(pin, widget); //To change body of generated methods, choose Tools | Templates.
    }
   
    


    private boolean isCisWiring(SvpWire edge) {
        
        
        SvVo from = edge.getFrom().getOwner();
        SvVo to = edge.getTo().getOwner();
        
        
        return from.getValues(SynBadTerms.Sbol.role).stream()
                     .anyMatch(v -> { return v.isURI() && v.asURI().equals(ModuleRole.SVPPart.getUri()); }) && 
                to.getValues(SynBadTerms.Sbol.role).stream()
                     .anyMatch(v -> { return v.isURI() && v.asURI().equals(ModuleRole.SVPPart.getUri()); });
    }
    
    private boolean isTypeOf(ModelEdgeType type, SBViewEdge edge) {
        return ((SBViewEdge<?, ?, SynBadEdge>)edge).getEdge().getEdge().equals(type.getUri());
    }
    
    public boolean isRootNodeEvent(SBGraphEvent evt) {
        return getSBView().getRoot() == evt.getData() 
                && (evt.getType() == SBGraphEventType.ADDED);
    }
    
    public boolean isRemovedEvent(SBGraphEvent evt) {
        return (evt.getType() == SBGraphEventType.REMOVED);
    }
    
    
     public void processNodeEvent(SBGraphEvent evt) {
        
       // LOGGER.debug("SvpScene: Processing node evt: {}", evt);
        
        if(evt.getType() == SBGraphEventType.VISIBLE ) {

            if(entityNodeMap.containsKey(evt.getData()))
                return;
            
            SBVObjectNode objNode = new SBVObjectNode((SBViewIdentified)evt.getData());
            
            LOGGER.trace("SvpScene: Processing node: {}", ((SBViewIdentified)evt.getData()).getDisplayId());

            addNode(objNode);

            // check if owns a mapsTo and add svpComponent port

        } else if(evt.getType() == SBGraphEventType.HIDDEN) {
            
            if(!entityNodeMap.containsKey(evt.getData()))
                return;
            
            // get parent
            
            Widget w = findWidget(entityNodeMap.get(evt.getData())).getParentWidget();
            
            // We can only minimise hierarchy widgets
            
            if(w == null || !SvpModuleWidget.class.isAssignableFrom(w.getClass())) {
                LOGGER.error("SvpScene: Widget for {} is not an SvpModuleWidget", ((SBViewIdentified)evt.getData()).getDisplayId());
                return;
            }
            
            // if parent is not minimise, do so
            
            SvpModuleWidget widget = (SvpModuleWidget) w;
            
            if(!widget.getMinimisedState().getBooleanState()) {
                widget.setMinimisedState(true);
            }
            
            
        } else if (isRemovedEvent(evt)) {
            if(entityNodeMap.containsKey(evt.getData()))
                removeNode(entityNodeMap.get(evt.getData()));
            if(portNodeMap.containsKey(evt.getData())) {
                
                // TO DO: Remove ports associated with the node
                // portNodeMap.get(evt.getData()).remove(portNodeMap.get(evt.getData()).stream().filter(p -> p.geto))
            }
        }
    }
     
     public void processEdgeEvent(SBGraphEvent evt) {
        
        //LOGGER.debug("SvpScene: Processing edge event: {}", evt);
        
        if( evt.getType() == SBGraphEventType.VISIBLE ) {

            SvpWire edge = (SvpWire) evt.getData();
            LOGGER.debug("SvpScene: Processing edge: {}", edge);
            addEdge(new SBVEdgeNode((SvpWire)evt.getData()));
           
        } else if( evt.getType() == SBGraphEventType.HIDDEN  ) {
            if(wireNodeMap.containsKey(evt.getData()))
                removeEdge(wireNodeMap.get(evt.getData()));
        }
        
        /*
        GridGraphLayout<SBVObjectNode, SBVEdgeNode> ggLayout = new  GridGraphLayout<>();
        SceneLayout layout = LayoutFactory.createSceneGraphLayout(this, ggLayout);
        layout.invokeLayoutImmediately();*/
        
        
        //revalidate();
    }
    
    @Override
    public void onEvent(SBEvent e) {
    
        LOGGER.debug("Received Event: [ {} ]", e);

        SBEventType type = e.getType();
        
        if(e instanceof SBGraphEvent && ((SBGraphEvent)e).getGraphEntityType() == SBGraphEntityType.NODE) 
        {
            SBGraphEvent<SvVo> evt = (SBGraphEvent<SvVo>) e;
            processNodeEvent(evt);
        } 
        else if(e instanceof SBGraphEvent && ((SBGraphEvent)e).getGraphEntityType() == SBGraphEntityType.PORT) 
        {
            
            SBGraphEvent<SvpViewPort> evt = (SBGraphEvent<SvpViewPort>) e;
            
            if( type == SBGraphEventType.ADDED && getSBView().isVisible(getSBView().getPortOwner(evt.getData()))) {
              addPin(entityNodeMap.get( getSBView().getPortOwner(evt.getData())), new SBVPortNode(evt.getData()));
            } else if( type == SBGraphEventType.REMOVED) {
                PropertiedNode pn = portNodeMap.get(evt.getData());
                if(this.isPin(pn))
                    removePin(pn);
                else {
                    if(pn!=null) {
                        SBViewPort port = pn.getLookup().lookup(SBViewPort.class);
                        portNodeMap.remove(port);
                    }     
                }
            }
        } 
        else if(e instanceof SBGraphEvent && ((SBGraphEvent)e).getGraphEntityType() == SBGraphEntityType.EDGE) 
        {
            SBGraphEvent<SvpWire> evt = (SBGraphEvent<SvpWire>) e;
            SBPortInstance p1 = (SBPortInstance) evt.getData().getFrom().getObject();
            SBPortInstance p2 = (SBPortInstance) evt.getData().getTo().getObject();
            if(p1.getDefinition().getOwner().is(Svp.TYPE) && p2.getDefinition().getOwner().is(Svp.TYPE))
                 return;
            processEdgeEvent(evt);
        }

        revalidate();
    }
}
