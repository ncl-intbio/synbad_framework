/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import javax.swing.Action;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Utilities;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.filter.SBFilterByDataClass;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.nodes.SBSvmNode;
import uk.ac.ncl.icos.synbad.ui.browser.AbstractBrowserPanel;
/**
 *
 * @author owengsilfellosn
 */
public class SvmPanel extends AbstractBrowserPanel {

    BeanTreeView view;
    
    public SvmPanel() {
        super();
        initComponents();
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        explorerManager.setRootContext(new AbstractNode(Children.LEAF));
        
        /*
        view = new OutlineView ("Property");
        view.getOutline().setRootVisible(false);
        view.addPropertyColumn("name", "Name");
        view.addPropertyColumn("displayId", "Display ID");
        view.addPropertyColumn("version", "Version");
        view.addPropertyColumn("description", "Description");
        */
        
        view = new BeanTreeView();
        view.setRootVisible(false);
        
        
        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    
    
    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
        RootSvpModuleNode rootNode = new RootSvpModuleNode(Children.create(new SvmPanel.SvmPanelNodeFactory(workspace), true));
        this.explorerManager.setRootContext(rootNode);
    }

    @Override
    public void clearWorkspace() {
        this.explorerManager.setRootContext(Node.EMPTY);
        this.workspace = null;
    }

    private class SvmPanelNodeFactory extends ChildFactory.Detachable<SvpModule> implements SBSubscriber {

        private final SBWorkspace workspace;

        public SvmPanelNodeFactory(SBWorkspace workspace) {
            this.workspace = workspace;
        }

        @Override
        protected void addNotify() {
            super.addNotify(); //To change body of generated methods, choose Tools | Templates.
            this.workspace.getDispatcher().subscribe(new SBFilterByDataClass(SvpModule.class), this);
        }
        
        @Override
        protected void removeNotify() {
            super.removeNotify(); //To change body of generated methods, choose Tools | Templates.
            this.workspace.getDispatcher().unsubscribe(this);
        }
        
        @Override
        protected boolean createKeys(List<SvpModule> toPopulate) {
            for(SvpModule definition : workspace.getObjects(SvpModule.class, workspace.getContextIds())) {
                toPopulate.add(definition);
            }
            
            return true;
        }

        @Override
        protected Node createNodeForKey(SvpModule key) {
            return new SBSvmNode(key, new SBSvmNode.SvmChildFactory(key));
        }

        @Override
        public void onEvent(SBEvent e) {
            refresh(true);
        }
    }
    
    class RootSvpModuleNode extends AbstractNode {

        public RootSvpModuleNode(Children children) {
            super(children);
        }
        
         @Override
        public Action[] getActions(boolean context) {
            List<? extends Action> actions = Utilities.actionsForPath( "Actions/SvmPanel" );
            return actions.toArray( new Action[actions.size()] );
        }
    }
}
