/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.nodes;

import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;
import javax.swing.Action;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Index;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ExTransferable;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.filter.SBFilterConstructor;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.actions.SBOpenSvmFlowEditor;
import uk.ac.ncl.icos.synbad.ui.svp.actions.SBOpenSvmGraphEditor;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvmTransferable;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvpTransferable;

public class SBSvmNode extends SBTopLevelNode  {
    
    private Action[] actions = new Action[]{};
    private final SBWorkspace workspace;
    private final SvmChildFactory factory;
    private final InstanceContent ic;

    public SBSvmNode(SvpModule definition, SvmChildFactory factory) {
        this(definition, factory, new InstanceContent());
    }
        
    private SBSvmNode(SvpModule definition, SvmChildFactory factory, InstanceContent ic) {
        super(definition, Children.create(factory, true), new ProxyLookup(Lookups.fixed(definition), new AbstractLookup(ic)));
        ic.add(new Index.Support() {

            @Override
            public Node[] getNodes() {
                return getChildren().getNodes();
            }

            @Override
            public int getNodesCount() {
                return getNodes().length;
            }

            @Override
            public void reorder(int[] perm) {
                factory.reorder(perm);
            }
        });
        
        this.ic = ic;
        actions = new Action[] {
          new SBOpenSvmFlowEditor(definition),
            new SBOpenSvmGraphEditor(definition)
        };
       
        this.factory = factory;
        workspace = definition.getWorkspace();
    }
    
    @Override
    public String getName() {
        SvpModule def = getLookup().lookup(SvpModule.class);
        if(!def.getName().isEmpty())
            return def.getName();
        return def.getDisplayId();
    }

    

    @Override
    public Action[] getActions(boolean context) {
        return actions;
    }
    
    @Override
    public Action getPreferredAction() {
       return actions[0];
    }

    @Override
    public boolean canCopy() {
        return true;
    }

    @Override
    public boolean canCut() {
        return true;
    }

    @Override
    public boolean canDestroy() {
        return true;
    }

    @Override
    public void destroy() throws IOException {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    @Override
    public Transferable drag() throws IOException {
        return new SvmTransferable(this);
    }
    
     @Override
        public Transferable clipboardCut() throws IOException {
            Transferable deflt = super.clipboardCut();
            ExTransferable added = ExTransferable.create(deflt);
            added.put(new ExTransferable.Single(SvmTransferable.SvmFlavor.SVM_FLAVOR) {
                protected SvpModule getData() {
                    return getLookup().lookup(SvpModule.class);
                }
            });
            return added;
        }
    
     @Override
    public PasteType getDropType(final Transferable t, int arg1, int arg2) {

        if (t.isDataFlavorSupported(SvmTransferable.SvmFlavor.SVM_FLAVOR)) {

            return new PasteType() {

                @Override
                public Transferable paste() throws IOException {
                    try {
                        SvpModule m = (SvpModule) t.getTransferData(SvmTransferable.SvmFlavor.SVM_FLAVOR);
                        SvpModule p = getLookup().lookup(SvpModule.class);
                        SvpActionBuilderImpl b = new SvpActionBuilderImpl(workspace,  workspace.getContextIds());
                        
                        SBIdentity parentId = workspace.getIdentityFactory().getIdentity(p.getIdentity());
                        SBIdentity childId = workspace.getIdentityFactory().getIdentity(m.getIdentity());
                        
                        List<Component> components = p.getDnaDefinition().getOrderedComponents();
                         
                        int index = arg2 <= 0 ? 0 : arg2 < components.size() ? arg2 : components.size() - 1;
                        SBIdentity precedes = index < components.size() - 1 ? workspace.getIdentityFactory().getIdentity(components.get(index).getIdentity()) : null;
                        
                        b.addSvm(parentId, childId, precedes);
                        
                        workspace.perform(b.build());
                        final Node node = NodeTransfer.node(t, NodeTransfer.DND_MOVE + NodeTransfer.CLIPBOARD_CUT);
                        if (node != null) {
                            node.destroy();
                        }
                    } catch (UnsupportedFlavorException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                    return null;
                }
            };
        } else if (t.isDataFlavorSupported(SvpTransferable.SvpFlavor.SVP_FLAVOR)) {

            return new PasteType() {

                @Override
                public Transferable paste() throws IOException {
                    try {
                        Svp m = (Svp) t.getTransferData(SvpTransferable.SvpFlavor.SVP_FLAVOR);
                        SvpModule p = getLookup().lookup(SvpModule.class);
                        SvpActionBuilderImpl b = new SvpActionBuilderImpl(workspace,  workspace.getContextIds());
                        
                        SBIdentity parentId = workspace.getIdentityFactory().getIdentity(p.getIdentity());
                        SBIdentity childId = workspace.getIdentityFactory().getIdentity(m.getIdentity());
                        
                        List<Component> components = p.getDnaDefinition().getOrderedComponents();
                         
                        int index = arg2 <= 0 ? 0 : arg2 < components.size() ? arg2 : components.size() - 1;
                        SBIdentity precedes = index < components.size() - 1 ? workspace.getIdentityFactory().getIdentity(components.get(index).getIdentity()) : null;
                        
                        b.addSvm(parentId, childId, precedes);
                        
                        workspace.perform(b.build());
                        final Node node = NodeTransfer.node(t, NodeTransfer.DND_MOVE + NodeTransfer.CLIPBOARD_CUT);
                        if (node != null) {
                            node.destroy();
                        }
                    } catch (UnsupportedFlavorException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                    return null;
                }
            };
        } else {
            return null;
        }
        
        
    }
        
        
        
    public static class SvmChildFactory extends ChildFactory<SBFlowObject> implements SBSubscriber {

        private final SvpModule definition;
        private final SBWorkspace workspace;
        private final List<SBFlowObject> children;
        private static final Logger LOGGER = LoggerFactory.getLogger(SvmChildFactory.class);
       

        public SvmChildFactory(SvpModule definition) {
            this.definition = definition;
            this.workspace = definition.getWorkspace();
            this.workspace.getDispatcher().subscribe(
                new SBFilterConstructor()
                    .setParentIdentity(definition.getIdentity())
                    .setParentClazz(ModuleDefinition.class).build(), this);
            children = definition.getOrderedChildren();
        }

        @Override
        protected boolean createKeys(List<SBFlowObject> toPopulate) {
            toPopulate.addAll(children);
            /*workspace.outgoingEdges(definition).stream().forEach(System.out::println);
            definition.getInteractions().stream().forEach(
                    (SvpInteraction i) ->  {
                        System.out.println(i); 
                        toPopulate.add(i); 
                    });*/
            return true;
        }

         public void reorder(int[] perm) {
             
            //Arrays.stream(perm).forEach(System.out::println);
            //System.out.println("---");

                // we will reorder at most 1 node
                
                int oldIndex = -1;
                int newIndex = -1;
                
                for(int i = 0; i < perm.length; i++) {
                    
                    // if one of the sequence based nodes has changed
                    
                    if(i != perm[i] && i < children.size()) {
                        oldIndex = i;
                        newIndex = perm[i];
                    }
                }
                
                if(oldIndex == -1 || newIndex == -1)
                    return;
                
                SBFlowObject moved = children.get(oldIndex);
                SBFlowObject preceded = newIndex < children.size() - 1 ? children.get(newIndex) : null;
                
                //System.out.println("Moving " + moved + ":" + oldIndex + " before " + preceded +":" + newIndex);
                
                SvpActionBuilderImpl b = new SvpActionBuilderImpl(workspace,  workspace.getContextIds());
                b.removeChild(definition.getIdentity(), oldIndex);
                
                SBIdentity parentId = workspace.getIdentityFactory().getIdentity(definition.getIdentity());
                SBIdentity childId = workspace.getIdentityFactory().getIdentity(moved.getIdentity());
                SBIdentity precededId = preceded == null ? null : workspace.getIdentityFactory().getIdentity(preceded.getIdentity());
                
                if(SvpModule.class.isAssignableFrom(moved.getClass())) {
                    b.addSvm(parentId, childId, precededId);
                } else if (Svp.class.isAssignableFrom(moved.getClass())) {
                    b.addSvp(parentId, childId, precededId);
                } 
                
                workspace.perform(b.build());
                
                children.clear();
                children.addAll(definition.getOrderedChildren());
                
                refresh(true);
    

        }

        
        @Override
        protected Node createNodeForKey(SBFlowObject key) {            
            if(Svp.class.isAssignableFrom(key.getClass()))
                return new SBSvpNode((Svp)key);
            else if(SvpModule.class.isAssignableFrom(key.getClass()))
                return new SBSvmNode((SvpModule)key, new SvmChildFactory((SvpModule)key));
            else if(SvpInteraction.class.isAssignableFrom(key.getClass()))
                return new SBSviNode((SvpInteraction)key);
         
            return null;
        }

        @Override
        public void onEvent(SBEvent e) {
            LOGGER.debug("onEvent: {}", e);
            children.clear();
            children.addAll(definition.getOrderedChildren());
            refresh(true);
        }
    }

}
