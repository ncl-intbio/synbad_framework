/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URI;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.dialog.CreateSvm;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;


@ActionID(       
        category = "File",
        id = "uk.ac.ncl.icos.synbad.svp.ui.actions.NewSvm"
)
@ActionRegistration(
        displayName = "#CTL_NewSvm"
)
@Messages("CTL_NewSvm=New SVM")
@ActionReference(path = "Actions/SvmPanel", position = 1475)
public final class NewSvm implements ActionListener {

 
    public NewSvm() {
      
    }

    @Override
    public void actionPerformed(ActionEvent e) {
          
        SBWorkspace manager = Lookup.getDefault().lookup(SBWorkspaceManager.class).getCurrentWorkspace();
        
        CreateSvm form = new CreateSvm();
        String msg = "New SVM";
        DialogDescriptor dd = new DialogDescriptor(form, msg);
        dd.createNotificationLineSupport();
        dd.setValid(form.isValid());
        
        if(!form.isValid()) {
            dd.getNotificationLineSupport().setErrorMessage(form.getMessage());
        }
       
        form.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals("Valid")) {
                    boolean wasValid = (Boolean)evt.getOldValue();
                    boolean isValid = (Boolean)evt.getNewValue();
                    if(wasValid != isValid)
                        dd.setValid(isValid);
                } else if(evt.getPropertyName().equals("Msg")) {
                    if(form.getMessage().isEmpty()) {
                        dd.getNotificationLineSupport().clearMessages();
                    } else {
                        dd.getNotificationLineSupport().setErrorMessage(form.getMessage());
                    }
                }
            } 
        });
        
        Object result = DialogDisplayer.getDefault().notify(dd);
        
        if (result != NotifyDescriptor.OK_OPTION)
            return;

        SBIdentity identity = manager.getIdentityFactory().getIdentity(form.getPrefix(), form.getDisplayId(), form.getVersion());
        URI SVM_TYPE = URI.create(SvpModule.TYPE);
        SBDataDefManager m = SBDataDefManager.getManager();    
        SvpActionBuilderImpl b  = new SvpActionBuilderImpl(manager, manager.getContextIds())
          .createSvm(identity);

        if(!form.getIdentifiedName().isEmpty()) {
            b = b.createValue(identity.getIdentity(), URI.create(SynBadTerms.SbolIdentified.hasName), SBValue.parseValue(form.getIdentifiedName()), SVM_TYPE);
        }

        if(!form.getDescription().isEmpty()) {
            b = b.createValue(identity.getIdentity(), URI.create(SynBadTerms.SbolIdentified.hasDescription), SBValue.parseValue(form.getDescription()), SVM_TYPE);              
        }

        SBAction action = b.build();
        manager.perform(action);
       
    }
}
