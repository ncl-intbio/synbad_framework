/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.editor.scene;

import java.awt.Color;
import java.awt.Dimension;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.netbeans.api.visual.anchor.Anchor;
import org.netbeans.api.visual.anchor.AnchorShape;
import org.netbeans.api.visual.anchor.AnchorShapeFactory;
import org.netbeans.api.visual.widget.ConnectionWidget;
import org.netbeans.api.visual.widget.Widget;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModelEdgeType;
import uk.ac.ncl.icos.synbad.ui.editor.scene.ASBGraphPinScene;
import uk.ac.ncl.icos.synbad.ui.editor.widget.action.ToolchainManager;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.PinOwner;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.widget.SbolHierarchyWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceComponentWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.PortPinWidget;
import uk.ac.ncl.icos.synbad.ui.svp.editor.widget.SvpModuleWidget;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.view.object.DefaultPViewEdge;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVEdgeNode;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.PropertiedNode;
import uk.ac.ncl.icos.synbad.view.DefaultSBPView;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;
/**
 *
 * @author owengilfellon
 * @param <V>
 */
public abstract class SBHFlowScene<N extends SBViewValued, E extends SBViewEdge, P extends SBViewPort,
        V extends DefaultSBPView<N,E,P>> extends ASBGraphPinScene<SBVObjectNode, SBVEdgeNode, PropertiedNode, V> {

    protected final Map<SBViewIdentified, SBVObjectNode> entityNodeMap = new HashMap<>();
    protected final Map<SBViewPort, PropertiedNode> portNodeMap = new HashMap<>();
    protected final Map<SBViewEdge, SBVEdgeNode> wireNodeMap = new HashMap<>();
    
    private static final Logger logger = LoggerFactory.getLogger(SBHFlowScene.class);

    public SBHFlowScene( V view ) {
        super(view);
        setMinimumSize(new Dimension(800, 600));
        
        logger.debug("Creating editor from [ {} ] in [ {} ]", view.getRoot(), view);
        
        addNode(new SBVObjectNode((SBViewIdentified)view.getRoot()));
        getPriorActions().addAction(new ToolchainManager());
        setActiveTool("default");
        view.subscribe(new SBEventFilter.DefaultFilter(), this);
    }
    
    
    
    @Override
    public Lookup getLookup() {
        return new ProxyLookup(super.getLookup(), Lookups.fixed(getSBView()));
    }
    
    public SBVObjectNode getNode(SBViewIdentified object) {
        return entityNodeMap.get(object);
    }
    
    public PropertiedNode getNode(SBViewPort port) {
        return portNodeMap.get(port);
    }
    
    public SBVEdgeNode getNode(SBViewEdge edge) {
        return wireNodeMap.get(edge);
    }
    
    @Override
    protected Widget attachNodeWidget(SBVObjectNode n) {
        
        SBViewIdentified vo = n.getLookup().lookup(SBViewIdentified.class);
        
        if(vo == null) {
            logger.error("No SBViewIdentified in Lookup: {}", vo.toString());
            return null;
        }
        
        logger.trace("Attaching node widget: [ {} ]", vo);
        
        SbolHierarchyWidget e = new SbolHierarchyWidget(this, n);

        // If we're adding the design root, add to scene's entity layer
        
        if(n.getLookup().lookup(SBViewIdentified.class) == getSBView().getRoot()) {
            logger.trace("View object is root: [ {} ]", vo);
            getEntitiesLayer().addChild(e);
            getEntitiesLayer().revalidate();
        } 
        
        // Otherwise, find the node's parent widget and add as child
        
        else {
            
            SBViewValued node = getSBView().getParent((N)n.getLookup().lookup(SBViewValued.class));
            
            if(node == null) {
                logger.error("Could not find parent node of: [ {} ]", node);
                return null;
            }
                 
            Widget parent = findWidget(entityNodeMap.get(node));
            
            if(parent == null) {
                logger.error("Could not find widget for parent: [ {} ]", node);
                return null;
            }
                
            if(!SbolHierarchyWidget.class.isAssignableFrom(parent.getClass())) {
                logger.error("{} is not an SbolHierarchyWidget", node.toString());
                return null;
            }
            
            logger.trace("Adding widget: {}", parent);
            ((SbolHierarchyWidget)parent).addWidget(e);
             entityNodeMap.put(n.getLookup().lookup(SBViewIdentified.class), n);
        }
        
        
        return  e;
    }

    @Override
    protected void detachNodeWidget(SBVObjectNode node, Widget widget) {
        super.detachNodeWidget(node, widget);
        entityNodeMap.remove(node.getLookup().lookup(SBViewIdentified.class));
    }

    @Override
    protected Widget attachPinWidget(SBVObjectNode n, PropertiedNode p) {
        
        logger.trace("Attaching pin widget: [ {} ] to [ {} ]", p, n);
        
        Widget owner = findWidget(n);
        
        if(owner == null) {
            logger.error("Could not find owner widget of {}", n.getName());
            return null;
        }
        
        if(!PinOwner.class.isAssignableFrom(owner.getClass())) {
            logger.error("Owner is not a PinOwner: {}", n.getName());
            return null;
        }
            
        Widget w = ((PinOwner)owner).addPin(p);
        
        if(w == null) {
            logger.error("addPin failed: {}", p.getName());
            return null;
        }
        
        portNodeMap.put(p.getLookup().lookup(SBViewPort.class), p);
        return w;
    }

    @Override
    protected void detachPinWidget(PropertiedNode pin, Widget widget) {
        
        logger.trace("Detaching pin widget: [ {} ] from [ {} ]", pin, widget);
        
        super.detachPinWidget(pin, widget); 
        portNodeMap.remove(pin.getLookup().lookup(SBViewPort.class));
    }

    @Override
    protected Widget attachEdgeWidget(SBVEdgeNode e) {

        logger.trace("Attaching edge widget: [ {} ]", e);
        
        DefaultSBPView<N, E, P> view = getSBView();
        DefaultPViewEdge vEdge = e.getLookup().lookup(DefaultPViewEdge.class);
        
        if(vEdge == null) {
            logger.error("Could not get SBPViewEdge from {}", e.getName());
            return null;
        }
        
        //SynBadEdge edge = (SynBadEdge)vEdge.getEdge();

        ConnectionWidget w = new ConnectionWidget(this);
      
        // if the edge is between two ports, then add connection widget
    
        
        SBViewPort fromPort =  //(isTypeOf(ModelEdgeType.PROXY, vEdge)
               // && ((VPort)vEdge.getFrom()).getData().getValue().getPortDirection() == PortDirection.OUT)
               // ? (VPort)vEdge.getTo() : 
                view.getEdgeSourcePort((E)vEdge) ;


        SBViewPort toPort =  // (isTypeOf(ModelEdgeType.PROXY, vEdge)
                // && fromPort.getData().getValue().getPortDirection() == PortDirection.OUT)
                // ? (VPort)vEdge.getFrom() : 
                 view.getEdgeTargetPort((E)vEdge);


        if(fromPort == null) {
            logger.error("Edge has no source port: {}", vEdge.getEdge());
            return null;
        }
        
        if(toPort == null) {
            logger.error("Edge has no target port: {}", vEdge.getEdge());
            return null;
        }
        
        // get owners - does getOwner still do this!?

        SBViewValued from = view.getPortOwner((P)fromPort);
        SBViewValued  to = view.getPortOwner((P)toPort);

        if(from == null) {
            logger.error("Owner of {} could not be found in view", fromPort.getIdentity().toASCIIString());
            return null;
        }
        
        if(to == null) {
            logger.error("Owner of {} could not be found in view", toPort.getIdentity().toASCIIString());
            return null;
        }
        
        boolean isFromParent = view.getParent((N)to) == from;
        boolean isToParent = view.getParent((N)from) == to;

        // get the nodes associated with the owners

        SBVObjectNode fromPortOwner = entityNodeMap.get(from);
        SBVObjectNode toPortOwner = entityNodeMap.get(to);

        if(fromPortOwner == null) {
            logger.error("Node for {} could not be found", from.getIdentity().toASCIIString());
            return null;
        }
        
        if(toPortOwner == null) {
            logger.error("Node for {} could not be found", to.getIdentity().toASCIIString());
            return null;
        }
        
        Widget w1 = findWidget(fromPortOwner);
        Widget w2 = findWidget(toPortOwner);
        
        if(w1 == null) {
            logger.error("Widget for {} could not be found", fromPortOwner.getName());
            return null;
        }
        
        if(w2 == null) {
            logger.error("Widget for {} could not be found", toPortOwner.getName());
            return null;
        }

        if(!PinOwner.class.isAssignableFrom(w1.getClass()) || !PinOwner.class.isAssignableFrom(w2.getClass())) {
            logger.error("Owner of pin is not a PinOwner");
            return null;
        }
            

        PinOwner fromWidget = (PinOwner) w1;
        PinOwner toWidget = (PinOwner) w2;

        // isFrom / isTo are used to identify whether the edge is
        // internal or external to the widget

        Anchor fromAnchor = fromWidget.getAnchor(fromPort, isFromParent);
        Anchor toAnchor = toWidget.getAnchor(toPort, isToParent);

        w.setSourceAnchor(fromAnchor);
        w.setTargetAnchor(toAnchor);

        w.setTargetAnchorShape(AnchorShape.TRIANGLE_FILLED);
        w.setLineColor(Color.WHITE);

        Widget owner = findWidget(fromPortOwner);
        if(SvpModuleWidget.class.isAssignableFrom(owner.getClass())) {
            SvpModuleWidget svmw = (SvpModuleWidget) ((SvpModuleWidget)owner).getNextModuleParent();
            w.setRouter(svmw.getRouter());
        } else if(SequenceComponentWidget.class.isAssignableFrom(owner.getClass())) {
            SvpModuleWidget svmw = (SvpModuleWidget)((SequenceComponentWidget)owner).getNextModuleParent();
            w.setRouter(svmw.getRouter());
        }

        w.setControlPointCutDistance(10);
        getConnectionsLayer().addChild(w);
        

        wireNodeMap.put(vEdge, e);

        return w;
    }

    @Override
    protected void attachEdgeSourceAnchor(SBVEdgeNode e, PropertiedNode p, PropertiedNode  p1) {
        
        logger.trace("Attaching edge source anchor: [ {} ] via [ {} ] and [ {} ]", e, p, p1);
        
        if(p1 != null) {
            
            URI[] contexts = p1.getLookup().lookup(SBIdentified.class).getContexts();
            
            SynBadEdge edge = (SynBadEdge) e.getLookup().lookup(SBViewEdge.class).getEdge();

            SBPort from = (SBPort)getWorkspace().getEdgeSource(edge, contexts);
            SBPort to = (SBPort)getWorkspace().getEdgeTarget(edge, contexts);
            
            SBIdentified fromOwner = from.getOwner();
            SBIdentified toOwner = to.getOwner();

            boolean isFromParent = false;
            
            

            if (getWorkspace().outgoingEdges(fromOwner, null, toOwner.getClass(), toOwner.getContexts())
                    .stream().anyMatch(oe -> oe.getTo().equals(toOwner.getIdentity())))
                isFromParent = true;
            
            SBViewPort fromPort = p1.getLookup().lookup(SBViewPort.class);
            Anchor fromAnchor = ((SbolHierarchyWidget)findWidget(getSBView().getPortOwner((P)fromPort)))
                    .getAnchor(fromPort, isFromParent);
            
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            edgeWidget.setSourceAnchor (fromAnchor);
            ((PortPinWidget)findWidget(p1)).setIsConnected(true);
            
        } else if (p != null) {
            ((PortPinWidget)findWidget(p)).setIsConnected(false);
        }

        if(p1 == null) {
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            getConnectionsLayer().removeChild(edgeWidget);
        }
    }

    @Override
    protected void attachEdgeTargetAnchor(SBVEdgeNode e, PropertiedNode p, PropertiedNode p1) { 
        
        logger.trace("Attaching edge target anchor: [ {} ] via [ {} ] and [ {} ]", e, p, p1);
        
        if(p1 != null) {
            
            URI[] contexts = p1.getLookup().lookup(SBIdentified.class).getContexts();
            
            SynBadEdge edge = (SynBadEdge) e.getLookup().lookup(SBViewEdge.class).getEdge();

            
            // these should be PortInstances....
            
            
            SBPort from = (SBPort)getWorkspace().getEdgeSource(edge, contexts);
            SBPort to = (SBPort)getWorkspace().getEdgeTarget(edge, contexts);
            
            SBIdentified fromOwner = from.getOwner();
            SBIdentified toOwner = to.getOwner();

            boolean isToParent = false;

            
            if (getWorkspace().outgoingEdges(toOwner, null, fromOwner.getClass(), toOwner.getContexts())
                    .stream().anyMatch(oe -> oe.getTo().equals(fromOwner.getIdentity())))
                isToParent = true;
            
            SBViewPort toPort = p1.getLookup().lookup(SBViewPort.class);
            
            Anchor targetAnchor = ((SbolHierarchyWidget)findWidget(getSBView().getPortOwner((P)toPort))).getAnchor(toPort, isToParent);
            
            ConnectionWidget edgeWidget = (ConnectionWidget) findWidget (e);
            edgeWidget.setTargetAnchor(targetAnchor);
            edgeWidget.setTargetAnchorShape(AnchorShapeFactory.createTriangleAnchorShape(10, true, false));
            ((PortPinWidget)findWidget(p1)).setIsConnected(true);
            
        } else if (p != null) {
            ((PortPinWidget)findWidget(p)).setIsConnected(false);
        }
    }

    @Override
    protected void detachEdgeWidget(SBVEdgeNode edge, Widget widget) {
        
        logger.trace("Detaching edge widget: [ {} ] from [ {} ]", edge, widget);
        
        super.detachEdgeWidget(edge, widget); 
        wireNodeMap.remove(edge.getLookup().lookup(SBViewEdge.class));
    }

    /*
    public SBEventTypeDefaultEventType getDefaultEventType(SBEventType type) {
        if(type == DefaultHEventType.VISIBLE ||
            type == DefaultEventType.ADDED ||
            type == DefaultHEventType.ADDED ) {
                return DefaultEventType.ADDED;
        } else if(type == DefaultHEventType.HIDDEN ||
            type == DefaultEventType.REMOVED||
            type == DefaultHEventType.REMOVED) {
                return DefaultEventType.REMOVED;
            }
        
        return null;
    }*/
    
  

    
    private boolean isTypeOf(ModelEdgeType type, SBViewEdge edge) {
        return ((SBViewEdge<?, ?, SynBadEdge>)edge).getEdge().getEdge().equals(type.getUri());
    }
 

    
}
