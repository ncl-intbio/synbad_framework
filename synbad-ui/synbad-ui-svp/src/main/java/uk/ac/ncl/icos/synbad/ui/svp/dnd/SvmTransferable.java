/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.synbad.ui.sbol.nodes.SBComponentDefinition;
import uk.ac.ncl.icos.synbad.ui.svp.nodes.SBSvmNode;
import uk.ac.ncl.icos.synbad.ui.svp.nodes.SBSvpNode;

/**
 *
 * @author owengilfellon
 */
public class SvmTransferable implements Transferable {

    private final SBSvmNode node;
    
    public SvmTransferable(SBSvmNode node) {
        this.node = node;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { SvmFlavor.SVM_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == SvmFlavor.SVM_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return node;
    }
    
    public static class SvmFlavor extends DataFlavor {
    
        public static final DataFlavor SVM_FLAVOR = new SvmFlavor();

        public SvmFlavor() {
             super(SBSvmNode.class, "SvmData");
        }
    }
}
