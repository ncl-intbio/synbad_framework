/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.project.browser.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;

@ActionID(
        category = "Edit",
        id = "uk.ac.ncl.icos.synbad.project.browser.actions.ExportSBMLAction"
)
@ActionRegistration(
        iconBase = "uk/ac/ncl/icos/synbad/synbad/svpfragment/sbml16.png",
        displayName = "Export SBML"
)
@ActionReferences({
    @ActionReference(path = "Menu/Edit", position = 1500, separatorBefore = 1450),
    @ActionReference(path = "Toolbars/File", position = 500),
    @ActionReference(path = "Loaders/application/x-nbsettings/Actions", position = 965, separatorBefore = 932)
})
public final class ExportSBMLAction implements ActionListener {

    private final CompilableCookie context;

    public ExportSBMLAction(CompilableCookie context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        
        FileObject fo = context.getObj().getPrimaryFile().getParent();
        assert fo.isFolder();

        /*
        SBMLSerialiser serialiser = new SBMLSerialiser();
        SBMLDocument document = serialiser.exportNetwork(context.getObj().getLookup().lookup(GRNTree.class));
        
        try {
            FileObject sbmlFile = fo.createData(context.getObj().getPrimaryFile().getName()+"_sbml.xml");
            SBMLWriter writer = new SBMLWriter();
            OutputStream stream = sbmlFile.getOutputStream();
            writer.write(document, stream);
            stream.close();
        } catch (IOException | XMLStreamException | SBMLException ex) {
            Exceptions.printStackTrace(ex);
        }*/
    }
}
