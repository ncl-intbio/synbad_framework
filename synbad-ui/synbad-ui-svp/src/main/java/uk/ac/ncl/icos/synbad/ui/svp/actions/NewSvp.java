/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.ui.svp.dialog.CreateSvp;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.svp.ui.actions.NewSvp"
)
@ActionRegistration(
        displayName = "#CTL_NewSvp"
)
@Messages("CTL_NewSvp=New SVP")
@ActionReference(path = "Actions/SvpPanel", position = 1475)
public final class NewSvp implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        
        SBWorkspace manager = Lookup.getDefault().lookup(SBWorkspaceManager.class).getCurrentWorkspace();
        
        CreateSvp form = new CreateSvp();
        String msg = "New SVP";
        DialogDescriptor dd = new DialogDescriptor(form, msg);
        dd.createNotificationLineSupport();
        dd.setValid(form.isValid());
        
        if(!form.isValid()) {
            dd.getNotificationLineSupport().setErrorMessage(form.getMessage());
        }
       
        form.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(evt.getPropertyName().equals("Valid")) {
                    boolean wasValid = (Boolean)evt.getOldValue();
                    boolean isValid = (Boolean)evt.getNewValue();
                    if(wasValid != isValid)
                        dd.setValid(isValid);
                } else if(evt.getPropertyName().equals("Msg")) {
                    if(form.getMessage().isEmpty()) {
                        dd.getNotificationLineSupport().clearMessages();
                    } else {
                        dd.getNotificationLineSupport().setErrorMessage(form.getMessage());
                    }
                }
            } 
        });
        
        Object result = DialogDisplayer.getDefault().notify(dd);
        
        if (result != NotifyDescriptor.OK_OPTION)
            return;

            SBIdentity identity = manager.getIdentityFactory().getIdentity(form.getPrefix(), form.getDisplayId(), form.getVersion());
            URI SVP_TYPE = URI.create(Svp.TYPE);
            SBDataDefManager m = SBDataDefManager.getManager();
            Set<Role> roles = form.getRoles().stream().map(t -> m.getDefinition(ComponentRole.class, t)).collect(Collectors.toSet());
            ComponentRole role = roles.stream().map(r -> (ComponentRole)r).findFirst().orElse(null);
            SvpActionBuilderImpl b = null;
            
            switch(role) {
                case CDS:
                    b = new SvpActionBuilderImpl(manager, manager.getContextIds())
                            .createCDS(identity, 0);
                    break;
                case Promoter:
                    b = new SvpActionBuilderImpl(manager, manager.getContextIds())
                            .createPromoter(identity, 0);
                    break;
                case Operator:
                    b = new SvpActionBuilderImpl(manager, manager.getContextIds())
                            .createOperator(identity);
                    break;
                case RBS:
                    b = new SvpActionBuilderImpl(manager, manager.getContextIds())
                        .createRBS(identity, 0, 0);
                    break;
                case Terminator:
                    b = new SvpActionBuilderImpl(manager, manager.getContextIds())
                            .createTerminator(identity);
                    break;
            }
            
            if(b==null)
                return;
            
            if(!form.getIdentifiedName().isEmpty()) {
                b = b.createValue(identity.getIdentity(), URI.create(SynBadTerms.SbolIdentified.hasName), SBValue.parseValue(form.getIdentifiedName()), SVP_TYPE);
            }
            
            if(!form.getDescription().isEmpty()) {
                b = b.createValue(identity.getIdentity(), URI.create(SynBadTerms.SbolIdentified.hasDescription), SBValue.parseValue(form.getDescription()), SVP_TYPE);              
            }
            
            manager.perform(b.build());
   
    }
}
