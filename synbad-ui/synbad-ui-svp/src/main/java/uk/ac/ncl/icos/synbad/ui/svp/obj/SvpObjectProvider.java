/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.obj;

import java.util.Arrays;
import java.util.Collection;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBPort;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBProxyPort;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.svp.obj.Parameter;
import uk.ac.ncl.icos.synbad.svp.obj.Property;
import uk.ac.ncl.icos.synbad.svp.obj.SviInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.workspace.objects.SBDomainObjectProvider;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = SBDomainObjectProvider.class)
public class SvpObjectProvider implements SBDomainObjectProvider {

    @Override
    public Collection<Class<?>> getDomainObjects() {
        return Arrays.asList(
            SviInstance.class,
            SvmInstance.class,
            SvpInstance.class,
            Svp.class,
            SvpInteraction.class,
            SvpModule.class,
            SvpParticipant.class,
            Parameter.class,
            Property.class,
            DefaultSBPort.class,
            DefaultSBPortInstance.class,
            DefaultSBProxyPort.class,
            SBWire.class
        );
    }
    
}
