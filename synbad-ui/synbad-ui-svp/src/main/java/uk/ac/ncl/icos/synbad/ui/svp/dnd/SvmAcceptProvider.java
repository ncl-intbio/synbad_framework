/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.dnd;

import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import org.netbeans.api.visual.action.AcceptProvider;
import org.netbeans.api.visual.action.ConnectorState;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvmTransferable.SvmFlavor;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;

/**
 *
 * @author owengilfellon
 */

public class SvmAcceptProvider implements AcceptProvider {
    
    private final SBWorkspace workspace;
    private static final Logger LOGGER = LoggerFactory.getLogger(SvmAcceptProvider.class);
    
    public SvmAcceptProvider(SBWorkspace workspace)
    {
        this.workspace = workspace;
    }

    @Override
    public ConnectorState isAcceptable(Widget widget, Point point, Transferable transferable) {
        return isSvm(transferable) ?  ConnectorState.ACCEPT : ConnectorState.REJECT;
    }

    @Override
    public void accept(Widget widget, final Point point, Transferable transferable) {

        // get location of point
        
        // get position on sequence widget
        
        // add at appropriate point
        
        SvpModule parent = widget.getLookup().lookup(SvmVo.class).getSvpModule();
        SvpModule child = getSvmFromTransferable(transferable);
       
        if(parent == null || child == null)
            return;
        
        SvpActionBuilderImpl b = new SvpActionBuilderImpl(workspace, parent.getContexts());
        
        
            SBIdentity parentId = workspace.getIdentityFactory().getIdentity(parent.getIdentity());
            SBIdentity childId = workspace.getIdentityFactory().getIdentity(child.getIdentity()); 
            b.addSvm(parentId, childId, null);
            
            LOGGER.debug("Adding {} to {}", childId.getDisplayID(), parentId.getDisplayID());
            workspace.perform(b.build());

    }

    private boolean isSvm(Transferable transferable) {

        SvpModule p = getSvmFromTransferable(transferable);
        return p != null; 
    }
    
    private SvpModule getSvmFromTransferable(Transferable transferable) {

        Object o = null;
        
        try {
            o = transferable.getTransferData(SvmFlavor.SVM_FLAVOR);
        } catch (IOException | UnsupportedFlavorException ex) {
            return null;
        }
        
        if(!Node.class.isAssignableFrom(o.getClass()))
            return null;

        try {
            return ((Node)o).getLookup().lookup(SvpModule.class);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }
}     
