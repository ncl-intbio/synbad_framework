/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.dialog;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;

/**
 *
 * @author Owen
 */
public class CreateSvp extends javax.swing.JPanel implements DocumentListener, ListSelectionListener, PropertyChangeListener {
  
    private final SBDataDefManager m = SBDataDefManager.getManager();
    private final Set<ComponentType> types = m.getDefinition(ComponentType.class);
    private final Set<ComponentRole> roles = m.getDefinition(ComponentRole.class);
    
    private boolean valid = false;
    private String message = "";

    /**
     * Creates new form CreateComponentPanel
     */
    public CreateSvp() {
        initComponents();
        identifiedPanel.addPropertyChangeListener(this);
        roleField.addListSelectionListener(this);
        valid = checkValid();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        roleLabel = new javax.swing.JLabel();
        roleScrollPane = new javax.swing.JScrollPane();
        roleField = new javax.swing.JList<>();
        identifiedPanel = new uk.ac.ncl.icos.synbad.ui.sbol.dialog.CreateIdentifiedPanel();

        org.openide.awt.Mnemonics.setLocalizedText(roleLabel, org.openide.util.NbBundle.getMessage(CreateSvp.class, "CreateSvp.roleLabel.text")); // NOI18N

        roleField.setModel(new javax.swing.AbstractListModel<String>() {
            Set<ComponentRole> types = m.getDefinition(ComponentRole.class);
            String[] strings = types.stream().map(ct -> ct.toString()).collect(Collectors.toList()).toArray(new String[] {});
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        roleField.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        roleScrollPane.setViewportView(roleField);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(roleLabel)
                .addGap(53, 53, 53)
                .addComponent(roleScrollPane)
                .addContainerGap())
            .addComponent(identifiedPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(identifiedPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(roleScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(roleLabel)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private uk.ac.ncl.icos.synbad.ui.sbol.dialog.CreateIdentifiedPanel identifiedPanel;
    private javax.swing.JList<String> roleField;
    private javax.swing.JLabel roleLabel;
    private javax.swing.JScrollPane roleScrollPane;
    // End of variables declaration//GEN-END:variables

    public String getPrefix() {
        return identifiedPanel.getPrefix();
    }
    
    public String getDisplayId() {
        return identifiedPanel.getDisplayId();
    }
    
    public String getVersion() {
        return identifiedPanel.getVersion();
    }
    
    public String getIdentifiedName() {
        return identifiedPanel.getIdentifiedName();
    }
    
    public String getDescription() {
        return identifiedPanel.getDescription();
    }
   
    public List<String> getRoles() {
        return roleField.getSelectedValuesList();
    }

    public String getMessage() {
        return message;
    }

    private boolean checkValid() {
                
        if(!identifiedPanel.getIsValid()) {
            updateMessage(identifiedPanel.getMessage());
            return false;
        }
        
        if(roleField.getSelectedValuesList().isEmpty()) {
            updateMessage("No role selected");
            return false;
        }
        
        updateMessage("");
        return true;
    }

    private void updateValid() {
        boolean oldValid = this.valid;
        this.valid = checkValid();
        firePropertyChange("Valid", oldValid, this.valid);    
    }
    
    private void updateMessage(String message) {
        String oldMsg = this.message;
        this.message = message;
        firePropertyChange("Msg", oldMsg, message);    
    }
    
    @Override
    public void insertUpdate(DocumentEvent e) {
        updateValid();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateValid();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updateValid();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        updateValid();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        updateValid();
        //firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue());
    }
}
