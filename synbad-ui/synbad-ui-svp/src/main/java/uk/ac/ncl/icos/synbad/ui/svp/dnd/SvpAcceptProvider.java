/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.dnd;

import java.awt.Point;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import org.netbeans.api.visual.action.AcceptProvider;
import org.netbeans.api.visual.action.ConnectorState;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvpTransferable.SvpFlavor;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;

/**
 *
 * @author owengilfellon
 */

public class SvpAcceptProvider implements AcceptProvider {
    
    private final SBWorkspace workspace;
    private final Logger LOGGER = LoggerFactory.getLogger(SvpAcceptProvider.class);
    public SvpAcceptProvider(SBWorkspace workspace)
    {
        this.workspace = workspace;
    }

    @Override
    public ConnectorState isAcceptable(Widget widget, Point point, Transferable transferable) {
        return isSvp(transferable) ?  ConnectorState.ACCEPT : ConnectorState.REJECT;
    }

    @Override
    public void accept(Widget widget, final Point point, Transferable transferable) {

        // get location of point
        
        // get position on sequence widget
        
        // add at appropriate point
        
        SvmVo svmvo = widget.getLookup().lookup(SvmVo.class);
        
        
        if(svmvo == null) {
            SBViewIdentified vo = widget.getLookup().lookup(SBViewIdentified.class);
            if(vo == null) {
                LOGGER.error("Could not find any VO from {}", widget.toString());
            }
            LOGGER.error("Could not find SvmVo from {}", widget.toString());
            return;
        }
        
        SvpModule parent = svmvo.getSvpModule();
        Svp child = getSvpFromTransferable(transferable);
       
        if(parent == null || child == null)
            return;
        
        SvpActionBuilderImpl b = new SvpActionBuilderImpl(workspace,  parent.getContexts());
   
            SBIdentity parentId = workspace.getIdentityFactory().getIdentity(parent.getIdentity());
            SBIdentity childId = workspace.getIdentityFactory().getIdentity(child.getIdentity()); 
            
            LOGGER.debug("Adding {} to {}", childId.getDisplayID(), parentId.getDisplayID());
            
            b.addSvp(parentId, childId, null);
            
            workspace.perform(b.build());

    }

    private boolean isSvp(Transferable transferable) {

        Svp p = getSvpFromTransferable(transferable);
        return p != null; 
    }
    
    private Svp getSvpFromTransferable(Transferable transferable) {

        Object o = null;
        
        try {
            o = transferable.getTransferData(SvpFlavor.SVP_FLAVOR);
        } catch (IOException | UnsupportedFlavorException ex) {
            return null;
        }
        
        if(!Node.class.isAssignableFrom(o.getClass()))
            return null;

        try {
            return ((Node)o).getLookup().lookup(Svp.class);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }
}     
