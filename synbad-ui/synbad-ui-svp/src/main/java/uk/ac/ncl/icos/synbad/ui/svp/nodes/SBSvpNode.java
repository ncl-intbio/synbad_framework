/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.nodes;

import java.awt.datatransfer.Transferable;
import java.io.IOException;
import javax.swing.Action;
import org.openide.actions.CopyAction;
import org.openide.actions.CutAction;
import org.openide.actions.DeleteAction;
import org.openide.actions.MoveDownAction;
import org.openide.actions.MoveUpAction;
import org.openide.util.datatransfer.ExTransferable;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvpTransferable;

public class SBSvpNode extends SBTopLevelNode  {

    public SBSvpNode(Svp definition) {
        super(definition);
        //super.setIconBaseWithExtension(getIconBase());   
    }
    private boolean hasRole(Svp entity, Role role) {
        return entity.getValues(SynBadTerms.Sbol.role).stream()
                .filter(v -> v.isURI())
                .anyMatch(v -> v.asURI().equals(role.getUri()));
    }

    @Override
    public Transferable drag() throws IOException {
        return new SvpTransferable(this);
    }
    
    @Override
    public Action[] getActions(boolean context) {
        return new Action[]{
                    CutAction.get(CutAction.class),
                    CopyAction.get(CopyAction.class),
                    DeleteAction.get(DeleteAction.class),
                    MoveUpAction.get(MoveUpAction.class),
                    MoveDownAction.get(MoveDownAction.class)
                };
    }
    
    @Override
    public boolean canCut() {
        return true;
    }
    @Override
    public boolean canCopy() {
        return true;
    }
    @Override
    public boolean canDestroy() {
        return true;
    }
    
    @Override
    public Transferable clipboardCut() throws IOException {
        Transferable deflt = super.clipboardCut();
        ExTransferable added = ExTransferable.create(deflt);
        added.put(new ExTransferable.Single(SvpTransferable.SvpFlavor.SVP_FLAVOR) {
            protected SvpModule getData() {
                return getLookup().lookup(SvpModule.class);
            }
        });
        return added;
    }

    
    /*
    private String getIconBase()
    {
        SBIdentified entity = getLookup().lookup(SBIdentified.class);

        if(hasRole(entity, ComponentRole.Promoter)) {
            return "uk/ac/ncl/icos/synbad/ui/sbolv/promoter16.png"; 
        } else if (hasRole(entity, ComponentRole.Promoter)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/operator16.png";
        } else if (hasRole(entity, ComponentRole.RBS)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/rbs16.png";
        } else if (hasRole(entity, ComponentRole.CDS)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/cds16.png";
        } else if (hasRole(entity, ComponentRole.Terminator)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/terminator16.png";
        } else if (hasRole(entity, ComponentRole.Shim)) {
            return "uk/ac/ncl/icos/synbad/project/nodeicons/shim16.png";
        } 
        
        return "uk/ac/ncl/icos/synbad/project/nodeicons/userdefined16.png";

    }*/

    @Override
    public String getName() {
        Svp def = getLookup().lookup(Svp.class);
        if(!def.getName().isEmpty())
            return def.getName();
        return def.getDisplayId();
    }
}
