/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.editor.widget;

import java.util.List;
import org.netbeans.api.visual.action.ActionFactory;
import org.netbeans.api.visual.widget.Widget;
import org.openide.nodes.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceComponentWidget;
import uk.ac.ncl.icos.synbad.ui.flow.editor.widget.SequenceWidget;
import uk.ac.ncl.icos.synbad.ui.svp.editor.scene.SvpScene;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvmAcceptProvider;
import uk.ac.ncl.icos.synbad.ui.svp.dnd.SvpAcceptProvider;
import uk.ac.ncl.icos.synbad.ui.nodes.model.SBVObjectNode;
import uk.ac.ncl.icos.synbad.ui.svp.nodes.SBSvmNode;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SvpModuleWidget extends SvpHierarchyWidget  {

    private final SequenceWidget listWidget;
    private final static Logger logger = LoggerFactory.getLogger(SvpModuleWidget.class);
    private final SBSvmNode node;

    public SvpModuleWidget(SvpScene scene, SBVObjectNode entity) {
        super(scene, entity);
        SvmVo vo = entity.getLookup().lookup(SvmVo.class);
        node = vo == null ? null : new SBSvmNode(vo.getSvpModule(), new SBSvmNode.SvmChildFactory(vo.getSvpModule()));
        listWidget = new SequenceWidget(scene);
        getActions().addAction(ActionFactory.createAcceptAction(new SvmAcceptProvider(scene.getWorkspace())));
        getActions().addAction(ActionFactory.createAcceptAction(new SvpAcceptProvider(scene.getWorkspace())));
       // getActions().addAction(ActionFactory.createAcceptAction(new PartAcceptProvider(scene.getWorkspace())));
    }
    
    public SvpModuleWidget getNextModuleParent() {
        return (SvpModuleWidget)getParentWidget()
                            .getParentWidget()
                            .getParentWidget()
                            .getParentWidget()
                            .getParentWidget();
    }
    
    @Override
    public void addWidget(Widget widget) {
        
        SvVo object = widget.getLookup().lookup(SvVo.class);
        
        if(widget instanceof SequenceComponentWidget && object.isClass(FunctionalComponent.class)) {
            
            if(listWidget.isEmpty() && listWidget.getParentWidget() == null) {
                getEntitiesLayer().addChild(listWidget);
            }
            
            // get position and order to add widget
            
            SvpScene scene = (SvpScene)getScene();
            SvmVo parent = scene.getSBView().getParent(object);
            
            if(parent == null) {
                logger.error("SvpModuleWidet:Could not find parent of: {}", object.getDisplayId());
                return;
            }
            
            
            
            List<SvVo> children = parent.getOrderedSequenceComponents();
            int index = children.indexOf(object);
            
            // if component precedes other components, get following component
            
            if(0 <= index && index < children.size() - 1 )
            {
                for(int i = index; i<children.size(); i++) {
                    SvVo preceded = children.get(i);
                    SBVObjectNode node = scene.getNode(preceded);
                    
                    if(node != null) {
                        Widget w = scene.findWidget(node);
                        
                        if(w == null) {
                            logger.error("SvpModuleWidet:Could not find widget for {}", node.getName());
                        }
                        
                        if(listWidget.getComponents().contains(w)) {
                            int index2 = listWidget.getComponents().indexOf(w);
                            logger.debug("SvpModuleWidet: Adding widget to component list: [ {} ] at index {}", 
                                    widget.getLookup().lookup(SBViewIdentified.class), index2);
                            boolean b = listWidget.addComponent(index2, widget);
                          
                            if(!b)
                                logger.error("SvpModuleWidet:Could not add widget for: {}", node.getName());
                            return;
                        }
                    }
                }
            }
            
            boolean b = listWidget.addComponent(widget);
            logger.debug("SvpModuleWidet: Adding widget to component list at end: [ {} ]", object.getDisplayId()); 
            if(!b)
                logger.error("SvpModuleWidet:Could not add widget for: {}", object.getDisplayId());

        } else {
            super.addWidget(widget);
        }
    }

    @Override
    public void removeWidget(Widget widget) {
        if(widget instanceof SequenceComponentWidget) { 
            
            SBViewIdentified obj = widget.getLookup().lookup(SBViewIdentified.class);
            
            if(obj == null) {
                logger.error("SvpModuleWidet:Could not get SBViewIdentified from {}", ((SequenceComponentWidget) widget).toString());
                return;
            }
            
            logger.debug("SvpModuleWidet:Removing widget from component list: [ {} ]", obj);
            
            boolean b = listWidget.removeComponent(widget);
            
            if(!b) {
                logger.error("SvpModuleWidet:Could not remove widget for: {}", obj.getDisplayId());
            }
            
            if(listWidget.isEmpty()) {
                getEntitiesLayer().removeChild(listWidget);
               
            }
                
        } else {
            super.removeWidget(widget);
        }
    }
    
    
    
     @Override
    public String toString() {
        return getLookup().lookup(Node.class).getName();
    }


    /*
    public class SvpModuleLayout implements Layout {


        @Override
        public void layout (Widget widget) {
            for (Widget child : widget.getChildren ()) {
                child.resolveBounds (null, null);
            }
        }

        @Override
        public boolean requiresJustification (Widget widget) {
            return true;
        }

        @Override
        public void justify (Widget widget) {
            
            int top_margin = 30;
            int track_margin = 30;
            
            Rectangle bounds = widget.getClientArea();
            List<Widget> children = ((SvpModuleWidget)widget).getEntitiesLayer().getChildren();

            if(!listWidget.isEmpty())
                listWidget.resolveBounds(null, new Rectangle(0, top_margin, widget.getClientArea().width, widget.getClientArea().height));
            
            for(Widget w : children) {
                if(!(w instanceof SVPModuleListWidget)) {
                     w.resolveBounds(null, new Rectangle());
                } 
               
            }
        }
    }*/
}
