/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.stream.FactoryConfigurationError;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.RequestProcessor;
import org.sbolstandard.core2.SBOLWriter;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.ui.project.obj.SBProjectDataObject;
import uk.ac.ncl.icos.synbad.ui.services.SBProjectService;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonFullExporter;

@ActionID(
        category = "File",
        id = "uk.ac.ncl.icos.synbad.project.NewExampleProject"
)
@ActionRegistration(
        displayName = "#CTL_NewExampleProject"
)
@ActionReference(path = "Menu/File", position = 1)
@Messages("CTL_NewExampleProject=New Example Project")
public final class NewExampleProject implements ActionListener {
    
    private static final URI mainWorkspace = URI.create("http://www.synbad.org/defaultworkspace/1.0");
    
    private static final AtomicInteger fileCount = new AtomicInteger(0);

    @Override
    public void actionPerformed(ActionEvent e) {
        File home = new File(System.getProperty("user.home"));
        final File projectFile = new FileChooserBuilder("user-dir")
            .setTitle("Create Project")
            .setDefaultWorkingDirectory(home)
            .setFileFilter(new FileNameExtensionFilter("SynBad Projects", "synbad"))
            .setFilesOnly(true)
            .setApproveText("Open")
            .showSaveDialog();

        RequestProcessor.getDefault().post(() -> {
            try (ProgressHandle progress =  ProgressHandle.createHandle("Creating example project")) {
                progress.start();
                progress.progress( "Creating example project..." );

                FileObject fileObject;
                fileObject = FileUtil.createData(projectFile);

                SBWorkspaceManager workspaceManager = Lookup.getDefault().lookup(SBWorkspaceManager.class);
                SBProjectService projectService = Lookup.getDefault().lookup(SBProjectService.class);

                if(projectService.getProject() != null) {
                    projectService.closeProject();
                }

                URI context = URI.create("http://www.synbad.org/maincontext/1.0");
                SBWorkspace workspace = workspaceManager.getWorkspace(mainWorkspace);
                workspace.createContext(context);

                ExampleFactory.setupWorkspace(workspace, new URI[] { context });
                SBJsonFullExporter exporter = new SBJsonFullExporter();

                try (OutputStream os = fileObject.getOutputStream()) {
                    exporter.export(os, Collections.singleton(workspace));
                    os.flush();
                } catch (FactoryConfigurationError | IOException ex) {
                    Exceptions.printStackTrace(ex);
                }   

                progress.progress( "Opening example project..." );

                DataObject obj = null;

                try {
                    obj = DataObject.find(fileObject);
                } catch (DataObjectNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }

                if(obj == null || !(obj instanceof SBProjectDataObject)) { System.out.println("NOT A PROJECT!"); return; }

                SBProjectDataObject project = (SBProjectDataObject) obj;
                projectService.openProject(project);
                progress.progress( "...done" );
                progress.finish();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        });
        
         
       
    }
}

