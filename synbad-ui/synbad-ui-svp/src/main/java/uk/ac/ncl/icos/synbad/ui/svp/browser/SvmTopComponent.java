/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.browser;

import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerUtils;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;


/**
 * Top component which displays something.
 */

@ConvertAsProperties(
        dtd = "-//uk.ac.ncl.icos.synbad.svp.browser//SvmBrowser//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "SvmTopComponent",
        //iconBase="SET/PATH/TO/ICON/HERE", 
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "svp", openAtStartup = true)
@ActionID(category = "Window", id = "uk.ac.ncl.icos.synbad.svp.ui.browser.SvmTopComponent")
@ActionReference(path = "Menu/Window", position = 423)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_SvmAction",
        preferredID = "SvmTopComponent"
)
@Messages({
    "CTL_SvmAction=Svm Browser",
    "CTL_SvmTopComponent=Svm Browser",
    "HINT_SvmTopComponent=Svm Browser"
})/**/

public final class SvmTopComponent extends TopComponent implements LookupListener  {

    //private final ProjectManager manager;

    private List<Action> actions = new ArrayList<>();
    private final Result<SBWorkspace> res;
    
    public SvmTopComponent() {
        initComponents();
        setName(Bundle.CTL_SvmTopComponent());
        setToolTipText(Bundle.HINT_SvmTopComponent());
        SBWorkspaceManager cp =  Lookup.getDefault().lookup(SBWorkspaceManager.class);
        res = cp.getLookup().lookupResult(SBWorkspace.class);
        res.allItems();
        res.addLookupListener(this);
        associateLookup(ExplorerUtils.createLookup(svmPanel1.getExplorerManager(), getActionMap())); 
        
       // FileObject menu = FileUtil.getConfigFile("SvmMenu");
//        MenuBar bar = new MenuBar(DataFolder.findFolder(menu)); 
    }
    
    @Override
    public void resultChanged(LookupEvent le) {
        System.out.println("Result Changed!");
        if(res.allInstances().isEmpty()){
            System.out.println("Clearing workspaces!");
            svmPanel1.clearWorkspace();
        }
        else {
            SBWorkspace workspace = res.allInstances().iterator().next();
            if(workspace != null && workspace instanceof SBWorkspace){
                // System.out.println("Setting workspace!");
                 svmPanel1.setWorkspace(workspace);
            }
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        svmPanel1 = new uk.ac.ncl.icos.synbad.ui.svp.browser.SvmPanel();

        setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(svmPanel1, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private uk.ac.ncl.icos.synbad.ui.svp.browser.SvmPanel svmPanel1;
    // End of variables declaration//GEN-END:variables
    
    @Override
    public void componentOpened() {
        //project = manager.getLookup().lookup(SbolDocDO.class);
    }

    @Override
    public Action[] getActions() {
        return super.getActions(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void componentClosed() { }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");

    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    
}
