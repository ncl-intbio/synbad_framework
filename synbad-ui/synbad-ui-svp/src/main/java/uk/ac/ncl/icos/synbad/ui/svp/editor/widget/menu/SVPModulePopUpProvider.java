/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.editor.widget.menu;

import java.awt.Point;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import org.netbeans.api.visual.action.PopupMenuProvider;
import org.netbeans.api.visual.widget.Widget;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;

/**
 *
 * @author owengilfellon
 */
public class SVPModulePopUpProvider implements PopupMenuProvider{

    SBIdentified module;

    public SVPModulePopUpProvider(SBIdentified module) {
        this.module = module;
    }
    
    @Override
    public JPopupMenu getPopupMenu(Widget widget, Point localLocation) {

        JPopupMenu menu = new JPopupMenu("SVP Menu");

        
        JMenu importSignal = new JMenu("Import Signal");
        JMenu exportSignal = new JMenu("Export Signal");

        // ......

        
        if(exportSignal.getItemCount() > 0)
            menu.add(exportSignal);
        if(importSignal.getItemCount() > 0)
            menu.add(importSignal);
      
        return menu;
    }
    
}
