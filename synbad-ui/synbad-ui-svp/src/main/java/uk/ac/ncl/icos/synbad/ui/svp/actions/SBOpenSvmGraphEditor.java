/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.svp.editor.SvpGraphEditorProvider;

/**
 *
 * @author Owen
 */
@ActionID(
        category = "Edit",
        id = "uk.ac.ncl.icos.synbad.svp.ui.actions.SBOpenSvmGraphEditor"   
)
@ActionRegistration(
        displayName = "Open Graph Editor"
)
@ActionReference(path = "Menu/Edit", position = 1475)
public class SBOpenSvmGraphEditor extends AbstractAction {
 
    private final SvpModule module;
    
    public SBOpenSvmGraphEditor(SvpModule module) {
        super("Graph Editor");
        this.module = module;
    }
       
    @Override
    public void actionPerformed(ActionEvent e) {   
        SvpGraphEditorProvider p = Lookup.getDefault().lookup(SvpGraphEditorProvider.class);
        TopComponent window = p.getEditor(module);
        window.open();
        window.requestActive();
    }
}
