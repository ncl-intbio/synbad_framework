/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.browser;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import org.openide.explorer.view.BeanTreeView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.filter.SBFilterByDataClass;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;
import uk.ac.ncl.icos.synbad.ui.browser.AbstractBrowserPanel;

/**
 *
 * @author owengilfellon
 */
public class SviPanel extends AbstractBrowserPanel {

    BeanTreeView view;
    
    public SviPanel() {
        super();
        initComponents();
        
        GridBagLayout layout = new GridBagLayout();
        this.setLayout(layout);
        explorerManager.setRootContext(new AbstractNode(Children.LEAF));
        
        view = new BeanTreeView();
        view.setRootVisible(false);
//        view.addPropertyColumn("name", "Name");
//        view.addPropertyColumn("displayId", "Display ID");
//        view.addPropertyColumn("version", "Version");
//        view.addPropertyColumn("description", "Description");

        GridBagConstraints fill = new GridBagConstraints();
        fill.gridx= GridBagConstraints.REMAINDER;
        fill.gridy= GridBagConstraints.REMAINDER;
        fill.fill = GridBagConstraints.BOTH;     
        fill.weightx=1.0;
        fill.weighty=1.0;
        add(view, fill);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    
    
    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.workspace = workspace;
        RootSvpInteractionNode rootNode = new RootSvpInteractionNode(Children.create(new SviPanel.SviPanelNodeFactory(workspace), true));
        this.explorerManager.setRootContext(rootNode);
    }

    @Override
    public void clearWorkspace() {
        this.explorerManager.setRootContext(Node.EMPTY);
        this.workspace = null;
    }

    private class SviPanelNodeFactory extends ChildFactory.Detachable<SvpInteraction> implements SBSubscriber {

        private final SBWorkspace workspace;

        public SviPanelNodeFactory(SBWorkspace workspace) {
            this.workspace = workspace;
            
        }

        @Override
        protected void addNotify() {
            super.addNotify(); //To change body of generated methods, choose Tools | Templates.
            this.workspace.getDispatcher().subscribe(new SBFilterByDataClass(SvpInteraction.class), this);
        }

        @Override
        protected void removeNotify() {
            super.removeNotify(); //To change body of generated methods, choose Tools | Templates.
            this.workspace.getDispatcher().unsubscribe(this);
        }

        @Override
        protected boolean createKeys(List<SvpInteraction> toPopulate) {

            for(SvpInteraction definition : workspace.getObjects(SvpInteraction.class, workspace.getContextIds())) {

                toPopulate.add(definition);
            }
            
            return true;
        }

        @Override
        protected Node createNodeForKey(SvpInteraction key) {
            return new SBTopLevelNode(key);
        }

        @Override
        public void onEvent(SBEvent e) {
            refresh(true);
        }

    }
    
    class RootSvpInteractionNode extends AbstractNode {

        public RootSvpInteractionNode(Children children) {
            super(children);
        }
    }
}
