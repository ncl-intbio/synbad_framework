/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.nodes;

import uk.ac.ncl.icos.synbad.ui.nodes.workspace.SBTopLevelNode;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;

public class SBSviNode extends SBTopLevelNode  {

    public SBSviNode(SvpInteraction definition) {
        super(definition);
        //super.setIconBaseWithExtension(getIconBase());   
    }
    
    @Override
    public String getName() {
        SvpInteraction def = getLookup().lookup(SvpInteraction.class);
        if(!def.getName().isEmpty())
            return def.getName();
        return def.getDisplayId();
    }
}
