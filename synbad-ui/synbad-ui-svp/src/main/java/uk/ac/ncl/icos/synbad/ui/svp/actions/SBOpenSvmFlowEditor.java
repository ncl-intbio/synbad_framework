/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.ui.svp.editor.SvpEditorProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;

/**
 *
 * @author Owen
 */
@ActionID(
        category = "Edit",
        id = "uk.ac.ncl.icos.synbad.svp.ui.actions.SBOpenSvmFlowEditor"   
)
@ActionRegistration(
        displayName = "Open Flow Editor"
)
@ActionReference(path = "Menu/Edit", position = 1475)
public class SBOpenSvmFlowEditor extends AbstractAction {
    

    private final SvpModule module;
    
    public SBOpenSvmFlowEditor(SvpModule module) {
        super("Flow Editor");
        this.module = module;
    }
       
    @Override
    public void actionPerformed(ActionEvent e) {   
//        SBWorkspaceManager service = Lookup.getDefault().lookup(SBWorkspaceManager.class);
//        SBWorkspace ws = service.getWorkspace(module.getWorkspaceId());
//        if(ws==null)
//            return;
        
//        final List<TopComponent> window = new ArrayList<>();
//        
//        Runnable tsk = () -> {
//            final ProgressHandle progr =  ProgressHandleFactory.createHandle("Opening " + module.getDisplayId());
//            progr.start();
//            progr.progress( "Opening view..." );

        SvpEditorProvider provider = Lookup.getDefault().lookup(SvpEditorProvider.class);
        TopComponent tc = provider.getEditor(module);
//            window.add(provider.getEditor(view));
//            progr.progress( "...done" );
//            progr.finish();
//        };

        tc.open();
        tc.requestActive();

//        Task task = RequestProcessor.getDefault().post(tsk);
//        task.addTaskListener((t) -> {
//            
//            // TODO: Need to move this back into main thread?
//            
//            if(t.isFinished() && !window.isEmpty()) {
//                window.get(0).open();
//                window.get(0).requestActive();
//            }
//        });
    }
}
