/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.editor;

import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.TopComponent;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.ui.services.SBEditorProvider;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service=SBEditorProvider.class)
public class SvpGraphEditorProvider implements SBEditorProvider<SvpModule> {

    @Override
    public TopComponent getEditor(SvpModule svpModule) {
        return new SvpGraphTopComponent(svpModule);
    }

    @Override
    public String getName() {
        return "Svp Graph View";
    }
}
