/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ui.svp.dnd;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import uk.ac.ncl.icos.synbad.ui.sbol.nodes.SBComponentDefinition;
import uk.ac.ncl.icos.synbad.ui.svp.nodes.SBSvpNode;

/**
 *
 * @author owengilfellon
 */
public class SvpTransferable implements Transferable {

    private final SBSvpNode node;
    
    public SvpTransferable(SBSvpNode node) {
        this.node = node;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] flavors = { SvpFlavor.SVP_FLAVOR };
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return flavor == SvpFlavor.SVP_FLAVOR;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return node;
    }
    
    public static class SvpFlavor extends DataFlavor {
    
        public static final DataFlavor SVP_FLAVOR = new SvpFlavor();

        public SvpFlavor() {
             super(SBSvpNode.class, "SvpData");
        }
    }
}
