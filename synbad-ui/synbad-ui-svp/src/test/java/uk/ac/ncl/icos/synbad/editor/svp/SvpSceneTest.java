/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import uk.ac.ncl.icos.synbad.ui.svp.editor.scene.SvpScene;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.net.URI;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import org.sbolstandard.core2.SBOLValidationException;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */
public class SvpSceneTest {
    
    private final URI workspaceURI = URI.create("http://www.synbad.org/testSvpView");
    private SBWorkspaceManager m;
    private SBWorkspace ws;
    private SvpModule subtilinReceiver;

    public static void main(String[] args) throws SBOLValidationException {
        SvpSceneTest test = new SvpSceneTest();
        test.setUp();
        test.testGraphVisualisation();
        //test.tearDown();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
//        MockServices.setServices(
//                SBWorkspaceManagerImpl.class,
//                SvpPortInstanceUpdater.class,
//                DefaultPortManager.class,
//                DefaultInteractionManager.class,
//                SvpInteractionUpdater.class,
//                DefaultModelFactory.class,
//                SvpExportedFcUpdater.class,
//                SvpEnvironmentConstantUpdater.class,
//                SvpModelProvider.class,
//                SvpCisWireUpdater.class,
//                SvpTransWireUpdater.class,
//                SvpSignalModulationWireUpdater.class
//        );
        this.m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        this.subtilinReceiver = ExampleFactory.setupWorkspace();
        this.ws = subtilinReceiver.getWorkspace();
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(ws.getIdentity());
    }


    @Test
    public void testGraphVisualisation() {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        
        SvpModel model = new  SvpModel(ws, subtilinReceiver);
        SvpPView target = model.getMainView();

//        final SvpScene scene = new SvpScene(target);
//        JScrollPane pane = new JScrollPane();
//        pane.setViewportView(scene.createView());
//        frame.setMinimumSize(new Dimension(800, 600));
//        frame.getContentPane().add(pane, c);
//        
//        pane.setVisible(true);
//        frame.pack();
//        frame.setVisible(true);
    }
}
