/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.editor.svp;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.net.URI;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.sbolstandard.core2.SBOLValidationException;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.ui.sbol.editor.scene.SBSbolScene;
import uk.ac.ncl.icos.synbad.sbol.model.SbolModel;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */
public class SbolSceneTest {
    
    private final URI workspaceURI = URI.create("http://www.synbad.org/testSvpView");
    private SBWorkspaceManager m;
    private SBWorkspace ws;
    private ModuleDefinition subtilinReceiver;

    public static void main(String[] args) throws SBOLValidationException {
        SbolSceneTest test = new SbolSceneTest();
        test.testGraphVisualisation();
    }

    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
//        MockServices.setServices(
//                SBWorkspaceManagerImpl.class,
//                SvpPortInstanceUpdater.class,
//                DefaultPortManager.class,
//                DefaultInteractionManager.class,
//                SvpInteractionUpdater.class,
//                DefaultModelFactory.class,
//                SvpExportedFcUpdater.class,
//                SvpEnvironmentConstantUpdater.class,
//                SvpModelProvider.class,
//                SvpCisWireUpdater.class,
//                SvpTransWireUpdater.class,
//                SvpSignalModulationWireUpdater.class
//        );

        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        ws = m.getWorkspace(workspaceURI);
        subtilinReceiver = ExampleFactory.getSubtilinReporter(ws);


    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(ws.getIdentity());
    }

    @Test
    public void testGraphVisualisation() {
        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        
        SbolModel model = new SbolModel(ws, subtilinReceiver);
        SbolHView target = model.getMainView();

//        final SBSbolScene scene = new SBSbolScene(target);
//        JScrollPane pane = new JScrollPane();
//        pane.setViewportView(scene.createView());
//        frame.setMinimumSize(new Dimension(800, 600));
//        frame.getContentPane().add(pane, c);
//        
//        pane.setVisible(true);
//        frame.pack();
//        frame.setVisible(true);
    }
}
