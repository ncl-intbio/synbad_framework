/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.action;

import java.net.URI;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public interface SBFlowBuilder extends SBActionBuilder {

    <T extends SBFlowBuilder> T createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType);

    <T extends SBFlowBuilder> T createPortInstance(URI identity, URI owner, URI ownerType, URI definition);

    <T extends SBFlowBuilder> T createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType);

    <T extends SBFlowBuilder> T createWire(SBIdentity identity, SBDefinition parent, SBPortInstance from, SBPortInstance to);
    
}
