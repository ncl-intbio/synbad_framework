/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.definition;


import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefProvider;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.servicemanager.SBService;

/**
 *
 * @author owengilfellon
 */

@ServiceProvider( service = SBDataDefProvider.class)
public class FlowDefinitionProvider implements SBDataDefProvider {

    @Override
    public void addDefinitions(SBDataDefManager manager) {
      
        for(SBPortDirection direction : SBPortDirection.values()) {
            manager.addDefinition(direction); 
        }
        
        manager.addSynonym("Input", SBPortDirection.IN);
        manager.addSynonym("Output", SBPortDirection.OUT);
        manager.addSynonym("Modifier", SBPortDirection.IN);

    }
}
