/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.action.FlowActionBuilder;
import uk.ac.ncl.icos.synbad.flow.action.FlowActions;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;


/**
 *
 * @author owengilfellon
 */
public abstract class ASBComponent extends ASBIdentified implements SBFlowObject {
    
    transient private static final Logger logger = LoggerFactory.getLogger(ASBComponent.class);
    private static final long serialVersionUID = 6730852137348192353L;

    public ASBComponent(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    
    @Override
    public List<SBFlowObject> getChildren() {
       return getChildren(SBFlowObject.class);
    }
    
    @Override
    public <T extends SBFlowObject> List<T> getChildren(Class<T> childClass) {
        URI[] contexts = getContexts();
        return ws.outgoingEdges(this, SynBadTerms.SynBadEntity.hasChild, childClass, contexts )
            .stream().map(e -> ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), childClass, contexts))      
            .collect(Collectors.toList());
    }

    @Override
    public SBFlowObject getParent() {
        List<SynBadEdge> edges = ws.incomingEdges(this, SynBadTerms.SynBadEntity.hasChild, SBFlowObject.class, getContexts());        
        return edges.stream().map(e -> ws.getEdgeSource(e, getContexts())).map(s -> s.as(SBFlowObject.class).get()).findFirst().orElse(null);
    }
    /*
    @Override
    public <T extends SBDefinition> SBInstance<T> addChild(T definition, SBIdentity childIdentity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/

  

    private String getPortId(SBPortDirection direction, PortType type, PortState state) {

        String stateString = state == null ? "" : state + "_";
        String number = state == null ?
                    ""+getPorts().stream().filter(port
                        -> port.getPortDirection() == direction
                        && port.getPortType().equals(type)).count() :
                    ""+getPorts().stream().filter(port
                        -> port.getPortDirection() == direction
                        && port.getPortType().equals(type)
                        && port.getPortState().equals(state)).count();


        return  direction + "_" +
                type + "_" +
                stateString + number;

    }
    
  


    @Override
    public SBPort createPort(boolean isPublic, SBPortDirection direction, PortType type, PortState state, URI identityConstraint) {

        if(direction == null)
            throw new NullPointerException("direction cannot be null");

        if(type == null)
            throw new NullPointerException("type cannot be null");

        String prefix = getPrefix();
        String prefixSeperator = SBIdentityHelper.getNamespaceSeperatorFromId(getIdentity());
        String parentDisplayId = getDisplayId();
        
        
        String portId = prefix + prefixSeperator + parentDisplayId + "/" + getPortId(direction, type, state);
        if(!getVersion().isEmpty())
            portId = portId.concat("/".concat(getVersion()));
        URI uri = URI.create(portId);
        SBIdentity identity = ws.getIdentityFactory().getIdentity(uri);
             ws.perform(FlowActions.createPort(identity, direction, type, state, identityConstraint, getIdentity(), getType(),  getWorkspace(), getContexts()));
             return ws.getObject(identity.getIdentity(), SBPort.class, getContexts());

        
    }

    @Override
    public void removePort(SBPort port) {
        ws.perform(new FlowActionBuilder(getWorkspace(), getContexts())
            .removeObjectAndDependants(port.getIdentity(), getIdentity()).build());
                
    }

    public boolean containsPort(URI identity) {
        return getPorts().stream().anyMatch(p -> p.getIdentity().equals(identity));
    }
    
    /*
    
    public SBPortInstance getPortByDefinition(URI identity) {

        List<SBPortInstance> ports = getPorts().stream()
                .filter(p -> p.getDefinition().getIdentity().equals(identity))
                .collect(Collectors.toList());
        
        if(ports.size() == 1)
            return ports.get(0);
        else if (ports.isEmpty())
            return null;
        else throw new IllegalStateException("Duplicate Instance URIs found ");
    }
    
    public SBPortInstance getPort(URI identity) {

        return getPorts().stream()
                .filter(p -> p.getDefinition().getIdentity().equals(identity))
                .findFirst().orElse(null);
    }
    
     @Override
    public SBPortInstance createPortInstance(SBIdentity identity, SBPort definition) {
        URI[] contexts = getContexts();
        ws.perform(FlowActions.createPortInstance(identity.getIdentity(), getIdentity(), definition.getIdentity(), getWorkspace(), contexts));
        return ws.getObject(identity.getIdentity(), SBPortInstance.class, contexts);
    }
    
    
    @Override
    public Collection<SBPortInstance> getPorts() {
        URI[] contexts = getContexts();
        return ws.outgoingEdges(this, SynBadTerms.SynBadEntity.hasPort, SBPortInstance.class, contexts)
            .stream().map(e -> (SBPortInstance)ws.getEdgeTarget(e)).collect(Collectors.toList());
    }
    
    public Collection<SBPortInstance> getPorts(SBPortDirection direction) {
        return getPorts().stream().filter(p -> p.getPortDirection() == direction).collect(Collectors.toList());
    }

*/
      @Override
    public Collection<SBPort> getPorts() {  
        URI[] contexts = getContexts();
        return ws.outgoingEdges(this, SynBadTerms.SynBadEntity.hasPort, SBPort.class, contexts )
            .stream().map(e -> {
                return ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), SBPort.class, contexts);
                    })      
            .collect(Collectors.toList());

    }

    @Override
    public Collection<SBPort> getPorts(SBPortDirection direction) {
        return getPorts().stream().filter(p -> p.getPortDirection() == direction).collect(Collectors.toSet());
    }

    @Override
    public Collection<SBPort> getPorts(PortType type) {
        return getPorts().stream().filter(p -> p.getPortType() == type).collect(Collectors.toSet());
    }

    @Override
    public Collection<SBPort> getPorts(SBPortDirection direction, PortType type) {
        return getPorts().stream().filter(p -> {
            return p.getPortDirection() == direction && p.getPortType() == type;
                }).collect(Collectors.toSet());
    }
}