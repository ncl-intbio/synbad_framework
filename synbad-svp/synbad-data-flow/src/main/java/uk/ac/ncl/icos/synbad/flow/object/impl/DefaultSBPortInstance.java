/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.sbol.object.ChildObject;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowInstance;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = DefaultSBPortInstance.TYPE)
public class DefaultSBPortInstance extends ASBIdentified implements SBPortInstance {
    
    public static final String TYPE = "http://www.synbad.org/PortInstance";
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBPortInstance.class);
    private static final long serialVersionUID = 8884121169972207252L;

    public DefaultSBPortInstance(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    @Override
    public SBPort getDefinition() {
        return ws.outgoingEdges(this, SynBadTerms.SynBadPortInstance.hasDefinition, SBPort.class, getContexts())
            .stream().map(e -> {
               // System.out.println("Processing Edge: " + e);
                return ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), SBPort.class, getContexts());
                    })
            .findFirst().orElse(null);
    }

    @Override
    public boolean isWired() {

        boolean isWired;
        
        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(ws, getContexts())) {
            isWired = graph.getTraversal().v(this)
                    //.e(SBDirection.IN, SynBadTerms.SynBadPortInstance.hasPortInstance)
                    //.v(SBDirection.IN)
                    .e(SBDirection.IN, SynBadTerms.SynBadWire.isFrom, SynBadTerms.SynBadWire.isTo)
                    .v(SBDirection.IN).getResult().streamVertexes().count() > 0;
        }
        
        return isWired;
    }

    @Override
    public Set<SBWire> getWires() {
        
        Set<SBWire> wires;
                
        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(ws, getContexts())) {
//        String[] directions   = direction.length == 0 ?
//                new String[] {SynBadTerms.SynBadWire.isFrom, SynBadTerms.SynBadWire.isTo}:
//                Arrays.asList(direction).stream().map((SBPortDirection d) -> {
//                    return d == SBPortDirection.IN ? SynBadTerms.SynBadWire.isTo : SynBadTerms.SynBadWire.isFrom;
//        }).collect(Collectors.toSet()).toArray(new String[] {});
//        
            String direction = getPortDirection() == SBPortDirection.IN ? SynBadTerms.SynBadWire.isTo : SynBadTerms.SynBadWire.isFrom;

            wires =  graph.getTraversal().v(this)
                .e(SBDirection.IN, direction)
                .v(SBDirection.IN).getResult()
                .streamVertexes()
                .filter(v -> v.is(SBWire.TYPE))
                .map(obj -> (SBWire) obj)
                .collect(Collectors.toSet());

            graph.close();

            return wires;
        }
    }

    @Override
    public SBPortDirection getPortDirection() {
        return getDefinition().getPortDirection();
    }

    @Override
    public boolean isProxy() {
        return getDefinition().isProxy();
    }

            
    @Override
    public boolean isConnectable(SBPortInstance port) {
        
        // No wires to and from self
        
        if(port.getOwner().getIdentity().equals(this.getOwner().getIdentity())) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Same parent");
            return false;
        }
            
        if(port.getPortDirection() == getPortDirection() && !port.isProxy()) {

            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Same direction");
            return false;
        }
        
        SBPortInstance from = getPortDirection() == SBPortDirection.OUT ? this : port;
        SBPortInstance to = getPortDirection() == SBPortDirection.IN ? this : port;

        // Types must match
        
        if(port.getPortType() != getPortType()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Different types");
             return false;
        }
           
        // If states are defined, they must match

        if(port.getPortState() != null && port.getPortState() != getPortState()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Different states");
             return false;
        } 
        
        // If identities are specified, they must match
           
        if(getIdentityConstraint() != null && 
            port.getIdentityConstraint() != null && 
            !port.getIdentityConstraint().equals(getIdentityConstraint())) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Constraints don't match");
            return false;
        }
        
        // If is trans interaction, must insert between input and output
     //   LOGGER.debug("Is connectable!");
        return true;
    }

    @Override
    public PortState getPortState() {
        return getDefinition().getPortState();
    }

    @Override
    public PortType getPortType() {
        return getDefinition().getPortType();
    }

    @Override
    public URI getIdentityConstraint() {
        return getDefinition().getIdentityConstraint();
    }
    
    @Override
    public SBFlowInstance getOwner() {
        return ws.incomingEdges(this, SynBadTerms.SynBadPortInstance.hasPortInstance,ChildObject.class, getContexts()).stream()
                .map(e -> ws.getEdgeSource(e, getContexts()).getIdentity())
                .map(uri -> ws.getObject(uri, SBFlowInstance.class, getContexts())).findFirst().orElse(null);
    }
  

    
        public boolean isPublic() {
            return getDefinition().isPublic();
        }

}

