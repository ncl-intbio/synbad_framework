/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.view.object;

import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.view.SBPView;

import java.io.Serializable;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;

/**
 *
 * Represents a point of connection in the model. MapsTo and SVP
 * ports are examples of explicit ports, edges between components
 * and interactions are between implicit ports.
 * 
 * Implicit ports could be derived from and enforce the data model.
 * For example, a component definition provides 0-* component ports.
 * Provides 1 empty port, and if that is taken generates another empty
 * port. If there is an upper limit (i.e. 0-1), then it limits the number of
 * generated ports. IF removed, leave at least 1 empty port.
 * 
 * 
 * 
 * @author owengilfellon
 */
public class DefaultViewWire<N extends SBViewPort<O>, O extends SBValued, E extends SBEdge> extends DefaultSBViewEdge<N, O, E> implements  Serializable {

    private static final long serialVersionUID = -5753282622743534589L;

    public DefaultViewWire(E edge, N from, N to, SBPView view) {
        super(edge, from, to, view);
    }

    public DefaultViewWire(E edge, N from, N to,  long id, SBPView view) {
        super(edge, from, to, id, view);
    }   
}
