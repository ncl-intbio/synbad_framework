/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = DefaultSBPort.TYPE)
public class DefaultSBPort extends ASBPort  {

    public static final String TYPE = "http://www.synbad.org/Port";
    
    private static final SBDataDefManager dataDefinitionManager = SBDataDefManager.getManager();
    private static final long serialVersionUID = 4052466659675746067L;

    public DefaultSBPort(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values ) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    @Override
    public SBPortDirection getPortDirection() {
        return getValues(SynBadTerms.SynBadPort.hasDirection , URI.class).stream()
           // .filter(v -> dataDefinitionManager.containsDefinition(SBPortDirection.class, v.toASCIIString()))
            .map(v -> dataDefinitionManager.getDefinition(SBPortDirection.class, v.toASCIIString()))
            .findFirst().orElse(null);
    }
    
    @Override
    public PortType getPortType() {
        return getValues(SynBadTerms.SynBadPort.hasPortType, URI.class ).stream()
            .filter(v -> dataDefinitionManager.containsDefinition(PortType.class, v.toASCIIString()))
            .map(v -> dataDefinitionManager.getDefinition(PortType.class, v.toASCIIString()))
            .findFirst().orElse(null);
    }

    @Override
    public URI getIdentityConstraint() {
         return getValues(SynBadTerms.SynBadPort.hasIdentityConstraint, URI.class ).stream().findFirst().orElse(null);
    }

    @Override
    public PortState getPortState() {
        return getValues(SynBadTerms.SynBadPort.hasPortState).stream()
            .filter(SBValue::isURI)
            .filter(v -> dataDefinitionManager.containsDefinition(PortState.class, v.asURI().toASCIIString()))
            .map(v -> dataDefinitionManager.getDefinition(PortState.class, v.asURI().toASCIIString()))
            .findFirst().orElse(null);
    }
    
    public boolean isWired() {
        return !ws.incomingEdges(this, SynBadTerms.SynBadWire.isFrom, SBWire.class, getContexts()).isEmpty() ||
                !ws.incomingEdges(this, SynBadTerms.SynBadWire.isTo, SBWire.class, getContexts()).isEmpty();
    }
    
    @Override
    public boolean isProxy() {
        return false;
    }
}