/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = SBWire.TYPE)
public class SBWire extends ASBIdentified {
    
    public final static String TYPE = "http://www.synbad.org/Wire";
    private static final long serialVersionUID = 1798068683815494633L;

    public SBWire(SBIdentity identity, SBWorkspace workspaceId, SBValueProvider values) {
        super(identity, workspaceId, values);
    }
    
    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    public SBPortInstance getFrom() {
        return ws.outgoingEdges(this, SynBadTerms.SynBadWire.isFrom, SBPortInstance.class, getContexts()).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), SBPortInstance.class, getContexts()))
            .findFirst().orElse(null);
    }

    public SBPortInstance getTo() {
        return ws.outgoingEdges(this, SynBadTerms.SynBadWire.isTo, SBPortInstance.class, getContexts()).stream()
            .map(e -> ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), SBPortInstance.class, getContexts()))
            .findFirst().orElse(null);
    }
}
