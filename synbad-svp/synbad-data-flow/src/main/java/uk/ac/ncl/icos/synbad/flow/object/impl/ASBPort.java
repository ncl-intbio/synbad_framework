/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import java.net.URI;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateValue;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

/**
 *
 * @author owengilfellon
 */
public abstract class ASBPort extends ASBIdentified implements SBPort {

    private static final long serialVersionUID = -5859830425224836843L;

    public ASBPort(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
            super(identity, workspaceIdentity, values);
        }
        
        @Override
        public SBIdentified getOwner() {
           return ws.incomingEdges(this, SynBadTerms.SynBadEntity.hasPort, SBIdentified.class, getContexts())
               .stream().map(e -> ws.getObject(ws.getEdgeSource(e, getContexts()).getIdentity(), SBIdentified.class, getContexts()))
               .findFirst().orElse(null);
        }
 
        public void setIsPublic(boolean isPublic) {
            apply(new CreateValue(getIdentity(), SynBadTerms.SynBadPort.isPublic, SBValue.parseValue(isPublic), getType(), ws, getContexts()));
        }

        @Override
        public boolean isPublic() {
            return getValues(SynBadTerms.SynBadPort.isPublic, Boolean.class)
                .stream().findFirst().orElse(false);
        }

        @Override
        public abstract SBPortDirection getPortDirection();

        @Override
        public abstract boolean isProxy();

        @Override
        public abstract PortState getPortState();

        @Override
        public abstract PortType getPortType();

    }