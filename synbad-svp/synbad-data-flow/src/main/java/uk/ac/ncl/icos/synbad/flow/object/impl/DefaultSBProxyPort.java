/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = DefaultSBProxyPort.TYPE)
public class DefaultSBProxyPort  extends ASBPort {
    
    public final static String TYPE = "http://www.synbad.org/ProxyPort";
    private final static Logger logger = LoggerFactory.getLogger(DefaultSBProxyPort.class);
    private static final long serialVersionUID = -2840332952949313051L;


    public DefaultSBProxyPort(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super( identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    @Override
    public SBPortDirection getPortDirection() {
        return getProxied().getPortDirection();
    }
    
    @Override
    public PortType getPortType() {
        return getProxied().getPortType();
        
    }

    @Override
    public PortState getPortState() {

        
        return getProxied().getPortState();
    }

    @Override
    public URI getIdentityConstraint() {
        return getProxied().getIdentityConstraint();
    }

    @Override
    public boolean isProxy() {
        return true;
    }

    public SBPort getProxied() {
        
        SBPort port =  ws.outgoingEdges(this, SynBadTerms.SynBadPort.proxies, SBPort.class, getContexts())
            .stream().map(e -> {
                logger.trace("Finding proxy node using {}", e);

                SBPort p = ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), SBPort.class, getContexts());
                logger.trace("Found: {}:{}", p, p.getClass().getSimpleName());

               return p;
            }).findFirst().orElse(null);
        
        if(port == null) {
            SBValue value = getValue(SynBadTerms.SynBadPort.proxies);
            URI uri = value != null && value.isURI() ? value.asURI() : null;
            SBIdentity id = uri != null ? ws.getIdentityFactory().getIdentity(uri) : null;
            
            logger.error("Could not retrieve proxied port [ {} ] for proxy [ {} ]", 
                    id == null ? "null" :id.getIdentity().toASCIIString(), 
                    getIdentity());
            
            return null;
        }
        
        return port;
        
    }
    

} 