/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.view.object;

import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import java.util.Set;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 * Represents a view instance of a SynBad object that extends {@link SBFlowObject}.
 * @author owengilfellon
 */
public interface SBViewComponent<N extends SBValued, P extends SBViewPort> extends SBViewValued<N>{

    public Set<P> getPorts();

}
