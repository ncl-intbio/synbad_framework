/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.flow.object.api.SBModule;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import java.net.URI;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.action.FlowActions;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowInstance;


/**
 *
 * @author owengilfellon
 */
public abstract class ASBModule extends ASBComponent implements SBModule {
    
    transient private static final Logger logger = LoggerFactory.getLogger(ASBModule.class);
    private static final long serialVersionUID = 6528442579266754136L;

    public ASBModule(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }

    /*
    @Override
    public <T extends SBDefinition> SBInstance<T> addChild(T definition, SBIdentity childIdentity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/

    @Override
    public Collection<SBWire> getWires() {
        URI[] contexts = getContexts();
        return ws.outgoingEdges(this, SynBadTerms.SynBadEntity.hasWire, SBWire.class, contexts)
            .stream().map(e -> ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), SBWire.class, contexts))
            .collect(Collectors.toList());
    }
    
    public boolean containsWire(URI identity) {
        return getWires().stream().anyMatch(p -> p.getIdentity().equals(identity));
    }
    

    @Override
    public SBWire wireChildren(SBFlowInstance<? extends SBFlowObject> from, SBFlowInstance<? extends SBFlowObject> to, PortType type, PortState state) {

        Collection<SBPortInstance> fromPorts = from.getPorts().stream()
                .filter(p -> p.getDefinition().getPortDirection() == SBPortDirection.OUT
                            && p.getDefinition().getPortType() == type 
                            && (state == null || p.getDefinition().getPortState() == state)).collect(Collectors.toSet());


        Collection<SBPortInstance> toPorts = to.getPorts().stream()
                .filter(p -> p.getDefinition().getPortDirection() == SBPortDirection.IN
                            && p.getDefinition().getPortType() == type 
                            && (state == null || p.getDefinition().getPortState() == state)).collect(Collectors.toSet());

        if(fromPorts.isEmpty() || toPorts.isEmpty())
            return null;

        for(SBPortInstance fromPort : fromPorts) {
            for(SBPortInstance toPort : toPorts) {

                if(fromPort.isConnectable(toPort) && 
                        toPort.isConnectable(fromPort))
                             return wireChildren(fromPort, toPort);
                 }

            } 

        return null;
    }

    @Override
    public SBWire wireChildren(SBPortInstance from, SBPortInstance to) {
        if(from.isConnectable(to) && to.isConnectable(from)){
            
            
            String prefix = getPrefix();
            String prefixSeperator = SBIdentityHelper.getNamespaceSeperatorFromId(getIdentity());
            String parentDisplayId = getDisplayId();


            String wireId = prefix + prefixSeperator + parentDisplayId + "/wire_" + countWires();
            if(!getVersion().isEmpty())
                wireId = wireId.concat("/".concat(getVersion()));
            URI uri = URI.create(wireId);
            SBIdentity identity = ws.getIdentityFactory().getIdentity(uri);
                URI[] contexts = values.getContexts();
                ws.perform(FlowActions.createWire(identity, this, from, to,  getContexts()));
                return ws.getObject(identity.getIdentity(), SBWire.class, contexts);

        }
        
        return null;

    }

    private int countWires() {
        int count = 0;
        Iterator<SBWire> i = getWires().iterator();
        while(i.hasNext()) {
            count++;
            i.next();
        }
        return count;
    }

    @Override
    public boolean removeWire(SBWire wire) {
        if(!getWires().contains(wire))
            return false;

        ws.perform(FlowActions.removeWireAction(wire.getIdentity(), getIdentity(), getType(), wire.getWorkspace(), wire.getContexts()));

        return true;
    }

    @Override
    public List<SBFlowInstance<?>> getNestedObjects() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}