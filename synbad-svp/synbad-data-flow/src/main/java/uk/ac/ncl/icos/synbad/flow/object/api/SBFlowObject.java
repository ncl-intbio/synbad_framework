/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.api;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;

/**
 *
 * @author owengilfellon
 */
public interface SBFlowObject extends SBDefinition {
    
    public <T extends SBFlowObject> T getParent();
    
    public List<SBFlowObject> getChildren();
    
    public <T extends SBFlowObject> List<T> getChildren(Class<T> childClass);

    public Collection<SBPort> getPorts();
    
    public Collection<SBPort> getPorts(SBPortDirection direction);
    
    public Collection<SBPort> getPorts(PortType direction);
    
    public Collection<SBPort> getPorts(SBPortDirection direction, PortType type);

    // ======== Mutator ========
    
    public SBPort createPort(boolean isPublic, SBPortDirection direction, PortType type, PortState state, URI signalId);
    
    public void removePort(SBPort port);

}
