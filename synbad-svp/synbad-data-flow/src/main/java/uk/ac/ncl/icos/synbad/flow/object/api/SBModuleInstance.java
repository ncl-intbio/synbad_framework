/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.api;

/**
 *
 * @author owengilfellon
 */
public interface SBModuleInstance<T extends SBModule> extends SBFlowInstance<T> {
    
}
