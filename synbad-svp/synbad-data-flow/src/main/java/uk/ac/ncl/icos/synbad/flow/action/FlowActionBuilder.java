/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.action;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObject;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class FlowActionBuilder implements SBActionBuilder, SBFlowBuilder, SBDomainBuilder {

    private final DefaultSBDomainBuilder builder;
    private final SBWorkspace workspace;
    private final URI[] contexts;

    public FlowActionBuilder(FlowActionBuilder builder) {
        this.builder = new DefaultSBDomainBuilder(builder.builder);
        this.workspace = builder.workspace;
        this.contexts = builder.contexts;
    }
    
    public FlowActionBuilder(SBWorkspace workspace, URI[] contexts) {
       this.workspace = workspace;
       this.contexts  = contexts;
       builder = new DefaultSBDomainBuilder(workspace, contexts);
    }
    
    @Override
    public int getSize() {
        return builder.getSize();
    }
    
    @Override
    public boolean isEmpty() {
        return builder.isEmpty();
    }

    @Override
    public SBAction build(String label) {
        return builder.build(label);
    }

    @Override
    public SBAction build() {
        return builder.build();
    }

    @Override
    public boolean isBuilt() {
        return builder.isBuilt();
    }

    // Top Level

    @Override
    public FlowActionBuilder addAction(SBAction action) {
        if(!isBuilt())
            builder.addAction(action);
        return this;
    }
    
    @Override
    public FlowActionBuilder createWire(SBIdentity identity, SBDefinition parent, SBPortInstance from, SBPortInstance to) {
        if(!isBuilt())
            builder.addAction(FlowActions.createWire(identity, parent, from, to, getContexts()));
        return this;
    }

    @Override
    public FlowActionBuilder createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType) { 
        if(!isBuilt())
            builder.addAction(FlowActions.createPort(identity, direction, type, state, identityConstraint, owner, ownerType, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public FlowActionBuilder createPortInstance(URI identity, URI owner, URI ownerType, URI definition) {
        if(!isBuilt())
            builder.addAction(FlowActions.createPortInstance(identity, owner, ownerType, definition, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public FlowActionBuilder createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType) {
        if(!isBuilt())
            builder.addAction(FlowActions.createProxyPort(owner, proxiedPort, owner, ownerType, getWorkspace(), getContexts()));
        return this;
    }
    
    public FlowActionBuilder removeObject(SBIdentity identity, URI type, URI ownerIdentity, URI ownerType) {
        if(!isBuilt())
            builder.addAction(new RemoveObject(identity.getIdentity(), type, ownerIdentity, ownerType, getWorkspace(), getContexts()));
        return this;
    }
    
    
    @Override
    public FlowActionBuilder removeObjectAndDependants(URI identity, URI ownerId) {
        if(!isBuilt())
            builder.removeObjectAndDependants(identity, ownerId);
        return this;
    }
    
    public FlowActionBuilder createObject(URI identity, URI rdfType, URI ownerId, URI ownerType) {
        if(!isBuilt())
            builder.createObject(identity, rdfType, ownerId, ownerType);
        return this;
    }
    
    
    public FlowActionBuilder removeObject(URI identity, URI rdfType, URI ownerId, URI ownerType) {
        if(!isBuilt())
            builder.removeObject(identity, rdfType, ownerId, ownerType);
        return this;
    }

    
    public FlowActionBuilder createEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.createEdge(source, predicate, target);
        return this;
    }
    
    public FlowActionBuilder removeEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.removeEdge(source, predicate, target);
        return this;
    }
    
    public FlowActionBuilder createValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.createValue(source, predicate, value,  sourceType);
        return this;
    }
    
    public FlowActionBuilder removeValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.removeValue(source, predicate, value, sourceType);
        return this;
    }

    @Override
    public FlowActionBuilder duplicateObject(URI identity, URI[] fromContexts, boolean incrementIds) {
        if(!isBuilt())
            builder.duplicateObject(identity, fromContexts, incrementIds);
        return this;
    }


    @Override
    public SBWorkspace getWorkspace() {
        return workspace;
    }

    @Override
    public URI[] getContexts() {
        return contexts;
    }

    @Override
    public <T extends SBActionBuilder> T getAs(Class<T> clazz) {
        return builder.getAs(clazz);
    }

    
}
