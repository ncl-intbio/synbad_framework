/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.impl;

import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowInstance;

/**
 *
 * @author owengilfellon
 */

public abstract class ASBFlowInstance<T extends SBIdentified> extends ASBIdentified implements SBFlowInstance<T> {

    private static final long serialVersionUID = -4007657896534783266L;

    public ASBFlowInstance(SBIdentity identity, SBWorkspace workspace, SBValueProvider provider) {
        super(identity, workspace, provider);
    }

    @Override
    public SBDefinition getParent() {
        return ws.incomingEdges(this, getContexts()).stream()
            .filter(e -> e.getEdge().equals(UriHelper.sbol.namespacedUri("Module"))
                        || e.getEdge().equals(UriHelper.sbol.namespacedUri("Component"))
                        || e.getEdge().equals(UriHelper.sbol.namespacedUri("FunctionalComponent"))
                        || e.getEdge().equals(UriHelper.sbol.namespacedUri("Interaction")))
            .map(e -> (SBDefinition)ws.getEdgeSource(e, getContexts()))
                .findFirst().orElse(null);
    }
    
    @Override
    public Collection<SBWire> getWires(SBPortDirection direction) {
        Collection<SBWire> wires = null;
        try(SBWorkspaceGraph g = new SBWorkspaceGraph(ws, getContexts())) {
            if (direction == SBPortDirection.IN) {
                wires = g.getTraversal().v(getPorts(direction).toArray(new SBPortInstance[]{}))
                    .e(SBDirection.IN, SynBadTerms.SynBadWire.isTo)
                    .v(SBDirection.IN, SBWire.class)
                    .getResult().streamVertexes().map(v -> (SBWire) v)
                    .collect(Collectors.toSet());
            } else {
                wires = g.getTraversal().v(getPorts(direction).toArray(new SBPortInstance[]{}))
                    .e(SBDirection.IN, SynBadTerms.SynBadWire.isFrom)
                    .v(SBDirection.IN, SBWire.class)
                    .getResult().streamVertexes().map(v -> (SBWire) v)
                    .collect(Collectors.toSet());
            }
        }

        return wires;
    }

    @Override
    public Collection<SBPortInstance> getPorts() {  
        URI[] contexts = getContexts();
        return ws.outgoingEdges(this, SynBadTerms.SynBadPortInstance.hasPortInstance, 
                SBPortInstance.class, contexts).stream()
                    .filter(e -> e != null)
                    .map(e -> ws.getObject(e.getTo(), SBPortInstance.class, contexts))
                    .filter(n -> n != null).collect(Collectors.toSet());
    }

    @Override
    public Collection<SBPortInstance> getPorts(SBPortDirection direction) {
        return getPorts().stream().filter(p -> p.getPortDirection() ==  direction)
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<SBPortInstance> getPorts(PortType type) {
        return getPorts().stream().filter(p -> p.getPortType() ==  type)
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<SBPortInstance> getPorts(SBPortDirection direction, PortType type) {
        return getPorts().stream()
                .filter(p -> p.getPortDirection() ==  direction)
                .filter(p -> p.getPortType() ==  type)
                .collect(Collectors.toSet());
    }


}
