/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.rewrite;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.flow.action.FlowActionBuilder;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.SBSubGraph;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentInstance;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.rewrite.SBRewriterRule;

/**
 *
 * @author owengilfellon
 */
public class AddPortsRule implements SBRewriterRule<ComponentInstance, SynBadEdge, SBWorkspace> {

        
        private static final Logger logger = LoggerFactory.getLogger(AddPortsRule.class);
        
        public AddPortsRule() {
     
        }
        
        private SBAction performRule(SBSubGraph<SBValued, SBEdge> g, SBWorkspace workspace, SBWorkspaceGraph graph) {
             
            SBIdentified instance = g.getLabelledVertices("instance").stream()
                    .filter(v -> SBIdentified.class.isAssignableFrom(v.getClass()))
                    .map(v -> (SBIdentified)v).findFirst().orElse(null);

            if(instance == null) {
                logger.debug("No instance found");
                 return null;
            }

            logger.debug("Found [ {} ]... processing portDefintions", instance);


            Set<SBPort> portDefinitions = g.getLabelledVertices("portdefinition").stream()
                    .filter(v -> SBPort.class.isAssignableFrom(v.getClass()))
                    .map(v -> (SBPort)v).collect(Collectors.toSet());

            if(portDefinitions.isEmpty())
                logger.debug("No port definitions found");

            logger.debug("Found {}... processing portinstances", portDefinitions.size());


            Set<SBPortInstance> portInstances = graph.getTraversal().v(instance)
                    .e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasPort)
                    .v(SBDirection.OUT)
                    .getResult().streamVertexes()
                    .filter(v -> SBPortInstance.class.isAssignableFrom(v.getClass()))
                    .map(v -> (SBPortInstance) v)
                    .collect(Collectors.toSet());

            logger.debug("Found {}... building action", portInstances.size());

            boolean b = false;

            FlowActionBuilder ab = new FlowActionBuilder(workspace, instance.getContexts());


            for(SBPortInstance pi : portInstances) {
                if(!portDefinitions.contains(pi.getDefinition())) {

                        ab = ab.removeObject(workspace.getIdentityFactory().getIdentity(pi.getIdentity()),  pi.getType(), instance.getIdentity(), instance.getType() );

                    logger.debug("Removing portInstance: [ {} ]", pi);
                    b = true;
                }    
            }

            for(SBPort pd : portDefinitions) {
                if(!portInstances.stream().anyMatch(pi -> pi.getDefinition().equals(pd))) {

                        ab = ab.createPortInstance(
                                workspace.getIdentityFactory().getIdentity("http://www.synbad.org", instance.getDisplayId(), pd.getDisplayId().concat("_instance"), instance.getVersion()).getIdentity(), 
                                instance.getIdentity(),
                                instance.getType(),
                                pd.getIdentity());
                        logger.debug("Adding portInstance of: [ {} ]", pd);
                        b = true;

                }
            }

            if(b) {
                logger.debug("Performing action...");
                workspace.perform(ab.build());
            } 

           return null;
        }

        @Override
        public void apply(SBWorkspace workspace) {
            
            // get all modules, functional components, interactions,
            
            logger.debug("Applying rule to all components");
            
            SBIdentified[] instances = workspace.getObjects(SBIdentified.class, workspace.getContextIds())
                    .stream().filter(
                            i -> Module.class.isAssignableFrom(i.getClass()) ||
                                ComponentInstance.class.isAssignableFrom(i.getClass()) ||
                                Module.class.isAssignableFrom(i.getClass())).collect(Collectors.toSet()).toArray(new SBIdentified[]{});

            SBIdentified[] interactions = workspace.getObjects(Interaction.class, workspace.getContextIds())
                    .toArray(new SBIdentified[]{});

            
            try (SBWorkspaceGraph graph = new SBWorkspaceGraph(workspace, workspace.getContextIds())) {
                graph.getTraversal()
                        .v(instances).as("instance")
                        .e(SBDirection.OUT, SynBadTerms.Sbol.definedBy)
                        .v(SBDirection.OUT, SBDefinition.class).as("definition")
//                        .e(SBDirection.OUT, SynBadTerms.SynBadEntity.extensionOf)
//                        .v(SBDirection.OUT).as("svpdefinition")
                        .e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasPort)
                        .v(SBDirection.OUT).as("portdefinition").getResult().getMatches().stream().forEach(g -> performRule(g, workspace, graph));
                
                
                graph.getTraversal()
                        .v(interactions).as("instance")
                        //.e(SBEdgeDirection.OUT, SynBadTerms.Sbolh.definedBy)
                        //.v(SBEdgeDirection.OUT, SBDefinition.class).as("definition")
//                        .e(SBDirection.OUT, SynBadTerms.SynBadEntity.extensionOf)
//                        .v(SBDirection.OUT).as("svpdefinition")
                        .e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasPort)
                        .v(SBDirection.OUT).as("portdefinition").getResult().getMatches().stream().forEach(g -> performRule(g, workspace, graph));
            }
        
        }

        public  SBAction apply(SBWorkspace workspace, SBIdentified object) {
            
            logger.debug("Applying rule to single object: [ {} ]", object);
            
            if(!workspace.containsObject(object.getIdentity(), ComponentInstance.class, object.getContexts()))
                return null;
            
            try(SBWorkspaceGraph graph = new SBWorkspaceGraph(workspace, object.getContexts())) {
                graph.getTraversal()
                        .v(object).as("instance")
                        .e(SBDirection.OUT, SynBadTerms.Sbol.definedBy)
                        .v(SBDirection.OUT).as("definition")
//                    .e(SBDirection.OUT, SynBadTerms.SynBadEntity.extensionOf)
//                    .v(SBDirection.OUT).as("svpdefinition")
                        .e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasPort)
                        .v(SBDirection.OUT).as("portdefinition").getResult().getMatches().stream().forEach(g -> {


                    g.getLabelledVertices("svpdefinition").stream().map(v -> "SVP:  " + v.toString()).forEach(logger::debug);
                    g.getLabelledVertices("definition").stream().map(v -> "Def:  " + v.toString()).forEach(logger::debug);
                    g.getLabelledVertices("instance").stream().map(v -> "Ins:  " + v.toString()).forEach(logger::debug);
                    g.getLabelledVertices("portdefinition").stream().map(v -> "Port: " + v.toString()).forEach(logger::debug);



                });
            }

            return null;
            
        }

    @Override
    public Class<ComponentInstance> getRuleClass() {
        return ComponentInstance.class;
    }
    
    

    @Override
    public boolean shouldApply(SBWorkspace g, SBEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

      
    @Override
    public SBAction apply(SBWorkspace g, SBEvent e) {
        if(!SBGraphEvent.class.isAssignableFrom(e.getClass()))
            return null;
        
        SBGraphEvent dne = (SBGraphEvent)e;
        
        if(dne.getGraphEntityType() != SBGraphEntityType.NODE)
            return null;
        
        SBIdentified i = (SBIdentified)g.getObject((URI)dne.getData(), dne.getDataClass(), g.getContextIds());
        if(i == null)
            return null;
        
        return  apply(g, i);
    }
    
        
        
}  