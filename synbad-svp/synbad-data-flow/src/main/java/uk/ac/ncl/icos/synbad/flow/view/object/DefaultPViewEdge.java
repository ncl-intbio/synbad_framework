/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.view.object;

import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.view.SBPView;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;

import java.io.Serializable;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;

/**
 *
 * @author owengilfellon
 */
public class DefaultPViewEdge<N extends SBValued, 
        E extends SBEdge> extends DefaultSBViewEdge<SBViewPort<N>, N, E> implements SBViewEdge<SBViewPort<N>, N, E>, Serializable {

    private static final long serialVersionUID = -5896591177525357418L;

    public DefaultPViewEdge(E edge, SBViewPort<N> from, SBViewPort<N> to, SBPView view) {
        super(edge, from, to, view);
    }

    public DefaultPViewEdge(E edge, SBViewPort<N> from, SBViewPort<N> to, long id, SBPView view) {
        super(edge, from, to, id, view);
    }

}
