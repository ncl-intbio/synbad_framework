/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.api;

import java.util.Collection;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;

/**
 *
 * @author owengilfellon
 */
public interface SBFlowInstance<T extends SBIdentified> extends SbolInstance<T> {
    
    Collection<SBWire> getWires(SBPortDirection direction);
    
    Collection<SBPortInstance> getPorts();

    Collection<SBPortInstance> getPorts(SBPortDirection direction);

    Collection<SBPortInstance> getPorts(PortType type);

    Collection<SBPortInstance> getPorts(SBPortDirection direction, PortType type);
    
}
