/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.action;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBPort;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBProxyPort;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObject;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class FlowActions {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FlowActions.class);
    
    public static SBAction createWire(SBIdentity identity, SBDefinition parent, SBPortInstance from, SBPortInstance to, URI[] contexts) {
        return new DefaultSBDomainBuilder(parent.getWorkspace(),  contexts)
            .createObject(identity.getIdentity(), SBIdentityHelper.getURI(SBWire.TYPE), parent.getIdentity(), parent.getType())
            .createEdge(parent.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasWire), identity.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadWire.isFrom), from.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadWire.isTo), to.getIdentity())
            .build("CreateWire ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType, SBWorkspace workspace, URI[] contexts) {
        
        LOGGER.trace("Creating port: {}", identity.getIdentity());
      
        DefaultSBDomainBuilder b =new DefaultSBDomainBuilder(workspace,  contexts)
                    .createObject(identity.getIdentity(), SBIdentityHelper.getURI(DefaultSBPort.TYPE), owner, ownerType)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPort.hasPortType), SBValue.parseValue(type.getUri()), ownerType)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPort.hasDirection), SBValue.parseValue(direction.getUri()), ownerType);
        
        if(state != null)
            b = b.createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPort.hasPortState), SBValue.parseValue(state.getUri()), ownerType);
        
        if(identityConstraint != null)
            b = b.createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPort.hasIdentityConstraint), SBValue.parseValue(identityConstraint), ownerType);
                    
        return b.createEdge(owner, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasPort), identity.getIdentity())
                .build("CreatePortDef ["+ identity.getDisplayID()+ "]");
    }

    public static SBAction createPortInstance(URI identity, URI owner, URI ownerType, URI definition, SBWorkspace workspace, URI[] contexts) {
        
        return new DefaultSBDomainBuilder(workspace,  contexts)
                    .createObject(identity, SBIdentityHelper.getURI(DefaultSBPortInstance.TYPE), owner, ownerType)
                    .createEdge(owner, SBIdentityHelper.getURI(SynBadTerms.SynBadPortInstance.hasPortInstance), identity)
                    .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SynBadPortInstance.hasDefinition), definition)
                    .build("CreatePortInst ["+ identity.toASCIIString() + "]");
        }
 
    public static SBAction createProxyPort(URI identity, URI proxiedPort, URI owner, URI ownerType, SBWorkspace workspace, URI[] contexts) {
        
        LOGGER.trace("Creating proxy port: {} -> {}", identity.toASCIIString(), proxiedPort.toASCIIString());
        
        return new DefaultSBDomainBuilder(workspace, contexts)
                .createObject(identity, SBIdentityHelper.getURI(DefaultSBProxyPort.TYPE), owner,  ownerType)
                .createEdge(owner, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasPort), identity)
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SynBadPort.proxies), proxiedPort)
                .build("CreatePortProxy ["+ identity.toASCIIString() + "]");
    }
        
    /*
    public static SBAction removePort(URI identity, URI parentId, URI workspaceId, URI[] contexts) {
        return new RemoveObjectAction(identity, parentId, SBIdentityHelper.getURI(DefaultSBPort.TYPE), workspaceId, contexts);
    }*/
    
    /*
    public static SBAction removePortInstance(URI identity, URI parentId, URI workspaceId, URI[] contexts) {
        return new RemoveObjectAction(identity, parentId, SBIdentityHelper.getURI(DefaultSBPortInstance.TYPE), workspaceId, contexts);
    }
    */  

    /*
    public static SBAction removeProxyPort(URI identity, URI owner, URI workspaceId, URI[] contexts) {
        return new RemoveObjectAction(identity, owner, SBIdentityHelper.getURI(DefaultSBProxyPort.TYPE), workspaceId, contexts);
    }*/   
        
    public static SBAction removeWireAction(URI identity, URI parentId, URI parentType, SBWorkspace workspaceId, URI[] contexts) {
        return new RemoveObject(identity, SBIdentityHelper.getURI(SBWire.TYPE), parentId, parentType, workspaceId, contexts);
    }
    
   
}
