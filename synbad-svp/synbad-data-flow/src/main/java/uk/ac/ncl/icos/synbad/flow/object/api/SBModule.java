/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.api;

import java.util.Collection;
import java.util.List;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;

/**
 *
 * @author owengilfellon
 */
public interface SBModule extends SBFlowObject {

    public List<SBFlowInstance<?>> getNestedObjects();

    public Collection<SBWire> getWires();

    // Wires
    
    public SBWire wireChildren(SBFlowInstance<? extends SBFlowObject> from, SBFlowInstance<? extends SBFlowObject> to, PortType type, PortState state);
    
    public SBWire wireChildren(SBPortInstance from, SBPortInstance to);
    
    //public <T extends SBDefinition> SBInstance<T> addChild(T definition, Identity childIdentity);
    
    public boolean removeWire(SBWire wire);

}
