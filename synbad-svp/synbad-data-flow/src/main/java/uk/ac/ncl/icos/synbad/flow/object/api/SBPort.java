/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.api;

import java.net.URI;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;

/**
 *
 * @author owengilfellon
 */

public interface SBPort extends SBDefinition {
    
    public <T extends SBIdentified> T getOwner();

    public boolean isPublic();

    public SBPortDirection getPortDirection();

    public boolean isProxy();

    public PortState getPortState();

    public PortType getPortType();
    
    public URI getIdentityConstraint();
    
}