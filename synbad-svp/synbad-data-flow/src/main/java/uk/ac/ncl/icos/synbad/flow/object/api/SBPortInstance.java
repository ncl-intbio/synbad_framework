/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.flow.object.api;

import java.net.URI;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;

/**
 *
 * @author owengilfellon
 */
public interface SBPortInstance extends SBIdentified {

    public SBPortDirection getPortDirection();

    public PortState getPortState();

    public PortType getPortType();

    public URI getIdentityConstraint();

    public boolean isConnectable(SBPortInstance port);

    public boolean isProxy();

    public boolean isWired();
    
    public Set<SBWire> getWires();
    
    SBPort getDefinition();
    
    <V extends SBFlowInstance> V getOwner();

}
