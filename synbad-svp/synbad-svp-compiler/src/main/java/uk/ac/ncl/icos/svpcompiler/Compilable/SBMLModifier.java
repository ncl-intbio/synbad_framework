package uk.ac.ncl.icos.svpcompiler.Compilable;

import org.sbml.jsbml.*;
import org.sbml.jsbml.text.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by owengilfellon on 06/10/2014.
 */
public interface SBMLModifier  {

    public SBMLDocument modify(SBMLDocument document) throws ParseException;
    
    public static class RemoveInducibleBaseLevel implements SBMLModifier {
        
        private static final Logger logger = LoggerFactory.getLogger(RemoveInducibleBaseLevel.class);

        @Override
        public SBMLDocument modify(SBMLDocument document) throws ParseException {

            if(document == null)
                return null;
            Model m = document.getModel();

            //ListOf<Reaction> reactions = m.getListOfReactions();
            //ListOf<Rule> rules = ;

            for( Rule rule : m.getListOfRules() )
            {
                logger.debug("Processing rule: [ {} ]", rule.getMetaId());
                
                if(rule.getMetaId().contains("PoPSOutput")) {
                    
                    logger.debug("Processing PoPSOutput rule: [ {} ]", rule.getMetaId());
                    
                    ASTNode mathNode = rule.getMath();
                    String mathFormula = JSBML.formulaToString(mathNode);
                    if(mathFormula.startsWith("0.001+")) {
                        
                        logger.debug("Removing base level from [ {} ]", rule.getMetaId());
                        
                        mathFormula = mathFormula.substring(6, mathFormula.length());
                        ASTNode updatedMathNode = JSBML.parseFormula(mathFormula);
                        rule.setMath(updatedMathNode);
                    }   
                    //System.out.println(JSBML.formulaToString(rule.getMath()));
                }  
            }
/*
        for( Reaction reaction : reactions ) {
            
            System.out.println("------Reaction------");

            System.out.println(reaction.getName());
            
            if( reaction.getName().contains("PoPSOutput") ) {
                KineticLaw kineticLaw = reaction.getKineticLaw();
                ASTNode mathNode = kineticLaw.getMath();
                String mathFormula = JSBML.formulaToString(mathNode);
                ;
                //double increase = 5.0;
                //ASTNode updatedMathNode = JSBML.parseFormula("(" + mathFormula + ")+" + increase);
                //kineticLaw.setMath(updatedMathNode);
            }
        }*/

        return document;
    }
}
    



}
