package uk.ac.ncl.icos.svpcompiler.Compiler;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.SignalType;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.LabelledDocument;

/**
 * Created by owengilfellon on 26/09/2014.
 */
public class SBMLCompiler extends AbstractCompiler<SBMLDocument> {


    private static final Logger LOGGER = LoggerFactory.getLogger(SBMLCompiler.class);
    private final SBMLHandler   SBML_HANDLER;
    private ModelBuilder        MODEL_BUILDER;
    private SBMLDocument        SBML_CONTAINER;

    private ICompilable currPopsEnd = null;
    private List<ICompilable>   popsEnds = new ArrayList<>();
    private ICompilable currRipsEnd = null;

    private LabelledDocument        currPopsDoc = null;
    private LabelledDocument        currRipsDoc = null;
    private LabelledDocument        currMRNADoc = null;
    private List<LabelledDocument>  popsDocs = new ArrayList<>();

    private LabelledDocument       currentDocument = null;
    private LabelledDocument       previousDocument = null;

    private final String filename;


    // config

    private final boolean CHAIN_POPS = true;

    public SBMLCompiler(String filename)
    {
        this.filename = filename;
        this.SBML_HANDLER = new SBMLHandler();
        this.SBML_CONTAINER = SBML_HANDLER.GetSBMLTemplateModel(this.filename);
        this.MODEL_BUILDER = new ModelBuilder(SBML_CONTAINER);
    }

    public void addPart(ICompilable part)
    {
        parts.add(part);
    }

    public void setParts(List<ICompilable> parts)
    {
        SBML_CONTAINER = SBML_HANDLER.GetSBMLTemplateModel(this.filename);
        MODEL_BUILDER = new ModelBuilder(SBML_CONTAINER);
        this.parts.addAll(parts);
        currPopsEnd = null;
        currRipsEnd = null;
        // Need to add all other resets here
    }
    
    public boolean processPart()  {
        
        LOGGER.debug("-------- Processing [ {} ] --------", getCurrentPart().getPart().getName());
        
        if(getCurrentPart() == null)
            return true;
        
        // once all PoPS documents have been processed, store last PoPS doc
        // for connnecting to mRNA

        // if CHAIN_POPS is true, then store all PoPS docs for connecting to mRNA - with VPR SVPs (that have not PoPS input),
        // this allows multiple promoters to be chained together. All promoter (or regulatory interaction) outputs are
        // connected to the mRNA. Check SVP 2.0 - do promoters have inputs?
        
        if(currPopsDoc != null && (!hasOutput(SignalType.PoPS) || (!CHAIN_POPS || !hasInput(SignalType.PoPS)))) {
            LOGGER.trace("Storing PoPS document: [ {} ]", currPopsDoc.getLabel());
            popsDocs.add(currPopsDoc);            
        } 

        if( isTerminatorByInterface() ) {
            LOGGER.trace("Clearing documents");
            currPopsEnd = null;
            currPopsDoc = null; 
            currRipsDoc = null;
            currRipsEnd = null;
            currMRNADoc = null;
            popsEnds.clear();
            popsDocs.clear();
            return true;
        } else if(isMrnaByInterface()) {

            // Connect all stored PoPS outs to mRNA document

            for(LabelledDocument popsEnd : popsDocs) {
                LOGGER.debug("+ Linking PoPS document [ {} ] to current document: [ {} ]", popsEnd.getLabel(), currentDocument.getLabel());
                if(!link(popsEnd, currentDocument))
                    return false;
            }
            currMRNADoc = currentDocument;
        } else if (hasInput(SignalType.RiPS)) {      
            LOGGER.debug("+ Linking RiPS document [ {} ] to current document: [ {} ]", currRipsDoc.getLabel(), currentDocument.getLabel());
            if(!link(currRipsDoc, currentDocument)) {
                return false;
            }
        } else if (hasInput(SignalType.mRNA)) {
            LOGGER.debug("+ Linking mRNA document [ {} ] to current document: [ {} ]", currMRNADoc.getLabel(), currentDocument.getLabel());
            if(!link(currMRNADoc, currentDocument))
                return false;
        } else if (previousDocument != null && !hasInput(SignalType.None)) {
            LOGGER.debug("+ Linking prev document [ {} ] to curr document [ {}] ", previousDocument.getLabel(), currentDocument.getLabel());
            if(!link(previousDocument, currentDocument))
                return false;
        } else {
            LOGGER.trace("Did not link documents");
        }

        LOGGER.debug("+ Adding [ {} ]", currentDocument.getLabel() );
        if(!add(currentDocument))
            return false;
        
        addAddedPartLabel(currentDocument.getLabel());

        // Store Sticky Signal Ends, as needed
        // Need to extend these?

        currPopsDoc = hasOutput(SignalType.PoPS) ? currentDocument : null;
        currRipsDoc = hasOutput(SignalType.RiPS) ? currentDocument : null;
        currPopsEnd = hasOutput(SignalType.PoPS) ? getCurrentPart() : null;
        currRipsEnd = hasOutput(SignalType.RiPS) ? getCurrentPart() : null;

        LOGGER.trace("Set current PoPS Doc to: [ {} ]", hasOutput(SignalType.PoPS) ? currentDocument.getLabel() : "none");
        LOGGER.trace("Set current PoPS End to: [ {} ]", hasOutput(SignalType.PoPS) ? getCurrentPart().getPart().getName() : "none");
        LOGGER.trace("Set current RiPS Doc to: [ {} ]", hasOutput(SignalType.RiPS) ? currentDocument.getLabel(): "none");
        LOGGER.trace("Set current RiPS End to: [ {} ]", hasOutput(SignalType.RiPS) ? getCurrentPart().getPart().getName() : "none");

        return processInteraction();
    }
    

    
    public  boolean processInteraction()  {
        
        if (getCurrentPart().getInteractions() == null || getCurrentPart().getInteractions().isEmpty())
            return true;
        
        for(Interaction interaction:getCurrentPart().getInteractions().keySet()) {

            if(partsParsed(interaction.getParts())) {
 
                // TODO Remove assumption of PoPS Regulation?

                if (isTranscriptionalRegulation(interaction) ) {
                    
                    LOGGER.debug("-------- Processing [ {} ] --------", interaction.getName());
                    LOGGER.trace("Has input and output, assuming transcriptional regulation: [ {} ]", interaction.getName());

                    // Update contextual information, then link and add
                    
                    previousDocument = currentDocument;
                    currPopsDoc = new LabelledDocument(interaction.getName(), getInteractionDocument(interaction));
                    currentDocument = currPopsDoc;
                    
                    LOGGER.trace("Set prev document to: [ {} ]", previousDocument.getLabel());
                    LOGGER.trace("Set curr document to: [ {} ]", currentDocument.getLabel());
                    LOGGER.trace("Set curr PoPS document to: [ {} ]", currPopsDoc.getLabel());

                    LOGGER.debug("+ Linking [ {} ] to [ {} ]", previousDocument.getLabel(), currentDocument.getLabel());

                    if(!link(previousDocument, currentDocument)) {
                        LOGGER.error("Could not link documents");
                        return false;
                    }
                    
                    //printLinkDebug(previousDocument.getDocument(), currentDocument.getDocument());
              
                    LOGGER.debug("+ Adding: [ {} ]", currentDocument.getLabel());
                    addAddedPartLabel(currentDocument.getLabel());

                    if(!add(currentDocument)) {
                        LOGGER.error("Could not add document: [ {} ]", currentDocument.getLabel());
                        return false;
                    }
                }

                // Else is a protein-protein interaction, no contextual information, or linking required

                else if (isTransInteractionWithPartsInModel(interaction)) {
                    
                    LOGGER.debug("-------- Processing [ {} ] --------", interaction.getName());
                    LOGGER.trace("Assuming non-transcriptional interaction: [ {} ]", interaction.getName());
                    
                    if(partsAdded(Arrays.asList(interaction.getName()))) {
                        LOGGER.debug("Already in model: [ {} ]", interaction.getName());
                        return true;
                    }
                    
                    SBMLDocument interactionDocument = getInteractionDocument(interaction);
                    LOGGER.debug("+ Adding: [ {} ] ", interaction.getName());
                    addAddedPartLabel(interaction.getName());
                    try {
                        synchronized(SBMLDocument.class) {
                            MODEL_BUILDER.Add(interactionDocument);
                        }
                    } catch(Exception e) {
                        LOGGER.error("Could not add {}", interaction.getName());
                        return false;
                    }
                    
                } else {
                    LOGGER.trace("No parts in model for: [ {} ]", interaction.getName());
                }
            }
        }

        return true;
    }

    public boolean compileNext() {
        
        // =====================================================
        // Reset previous and current parts and documents
        // =====================================================

        if(currentDocument != null)
            LOGGER.trace("Setting previous document: [ {} ]", currentDocument.getLabel());
        
        previousDocument = currentDocument;
        
        if(getCurrentPart() != null)
            setPreviousPart(getCurrentPart());
        
        setCurrentPart(currentIndex < parts.size() ? parts.get(currentIndex++) : null);
        currentDocument = new LabelledDocument(
                getCurrentPart().getPart().getName().equals("mRNA") ?
                        getPartDocument().getModel().getSpecies(0).getName() :
                        getCurrentPart().getPart().getName(),
                getPartDocument());
        LOGGER.trace("Set current document to: [ {} ]", currentDocument.getLabel());

       if(!processPart()) {
           StringBuilder sb = new StringBuilder();
           parts.stream().forEach(p -> sb.append(p.getPart().getName()).append("; "));
           LOGGER.error("Could not process part: {} from {}", getCurrentPart().getPart().getName(), sb.toString());
           return false;
        }

        setChanged();
        notifyObservers();
        return true;
    }
    
    public SBMLDocument getPartDocument() {
        
        SBMLDocument doc = null;

        if( !hasOutput(SignalType.mRNA) &&
            !isTerminatorByInterface() &&
            !hasOutput(SignalType.Species)) {
            LOGGER.debug("Cloning document: [ {} ]", getCurrentPart().getPart().getName());
            try {
                synchronized(SBMLDocument.class) {
                    doc = MODEL_BUILDER.Clone( getCurrentPart().getPartDocument(), getCurrentPart().getPart());
                }
            } catch (Exception e) {
                LOGGER.error("Could not retrieve SBML document", e);
            }
        } else {
            doc = getCurrentPart().getPartDocument();
        }

        return doc;
    }
    
    private boolean link(LabelledDocument doc1, LabelledDocument doc2)  {
        synchronized(SBMLDocument.class) {
            try {
                MODEL_BUILDER.Link(doc1.getDocument(), doc2.getDocument());
            } catch(Exception e) {
                LOGGER.error("Could not link {} to {}", doc1.getLabel(), doc2.getLabel());
                e.printStackTrace();
                return false;
            }
        }

        return true;
    }
    
    private boolean add(LabelledDocument doc) {
        synchronized(SBMLDocument.class) {
            try {
                MODEL_BUILDER.Add(doc.getDocument());
            } catch(Exception e) {
                LOGGER.error("Could not add {}", currentDocument.getLabel());
                return false;
            }
        }

        return true;
    }
    
    private SBMLDocument getInteractionDocument(Interaction interaction)  {

        SBMLDocument interactionDocument = null;

        LOGGER.debug("Cloning document: [ {} ]", interaction.getName());
        synchronized(SBMLDocument.class) {
            try {
                interactionDocument = MODEL_BUILDER.CloneInteraction(getCurrentPart().getInteractions().get(interaction), interaction);
            } catch (Exception e) {
                LOGGER.error("Could not clone document", e);
            }
        }

        return interactionDocument;
    }
    
    public boolean hasOutput(SignalType type) {
        return getCurrentPart().getOutputSignal().contains(type);
    }
    
    public boolean hasInput(SignalType type) {
        return getCurrentPart().getInputSignal().contains(type);
    }
    
    public boolean isRegulatoryElement(){
        return hasInput(SignalType.PoPS) && hasOutput(SignalType.PoPS);
    }
    
    public boolean isTerminatorByInterface() {
        return hasInput(SignalType.None) && hasOutput(SignalType.None);
    }
    
    public boolean isMrnaByInterface() {
        return hasInput(SignalType.PoPS) && hasOutput(SignalType.mRNA);
    }
    
    public boolean isTranscriptionalRegulation(Interaction interaction) {
        return interaction.getParameters().stream().map(p -> p.getScope()).anyMatch(p -> p.equals("Input")) &&
                interaction.getParameters().stream().map(p -> p.getScope()).anyMatch(p -> p.equals("Output")) && hasOutput(SignalType.PoPS);
    }
    
    public boolean isTransInteractionWithPartsInModel(Interaction interaction) {
        return !interaction.getParameters().stream().map(p -> p.getScope()).anyMatch(p -> p.equals("Input")) &&
                !interaction.getParameters().stream().map(p -> p.getScope()).anyMatch(p -> p.equals("Output")) && partsAdded(interaction.getParts());
    }

    public boolean compileAll()  {
        boolean b = true;

        while(currentIndex < parts.size()) {
                if (!this.compileNext()) {
                    b = false;
                }
            }

        return b;
    }

    @Override
    public SBMLDocument getDocument()
    {
        synchronized(SBMLDocument.class) {
            try {
                return MODEL_BUILDER.GetModel();
            } catch (Exception e) {
                LOGGER.error(e.getLocalizedMessage());
                return null;
            }
        }
    }

    public String getModelString()
    {
        synchronized(SBMLDocument.class) {
            try {
                return MODEL_BUILDER.GetModelString();
            } catch (Exception e) {
                LOGGER.error(e.getLocalizedMessage());
                return null;
            }
        }

    }

    public ICompilable getCurrPopsEnd() {
        return currPopsEnd;
    }

    public ICompilable getCurrRipsEnd() {
        return currRipsEnd;
    }

    public List<ICompilable> getPopsEnds() {
        return popsEnds;
    }

    public LabelledDocument getCurrPopsDoc() {
        return currPopsDoc;
    }

    public LabelledDocument getCurrRipsDoc() {
        return currRipsDoc;
    }

    public List<LabelledDocument> getPopsDocs() {
        return popsDocs;
    }
    
    
    private void printLinkDebug(SBMLDocument firstDocument, SBMLDocument secondDocument)
    {
        System.out.println("=====================================================================");
        System.out.println("Name: " + firstDocument.getModel().getName() + " - " + secondDocument.getModel().getName());
        System.out.println("ID: " + firstDocument.getModel().getId() + " - " + secondDocument.getModel().getId());
        System.out.println("MetaID: " + firstDocument.getModel().getMetaId() + " - " + secondDocument.getModel().getMetaId());
        System.out.println("Element: " + firstDocument.getModel().getElementName() + " - " + secondDocument.getModel().getElementName());
        System.out.println("SBO: " + firstDocument.getModel().getSBOTermID() + " - " + secondDocument.getModel().getSBOTermID());
        System.out.println("Document 1 Species=================================================");
        List<Species> species = firstDocument.getModel().getListOfSpecies();
        for(Species s:species)
        {
             System.out.println("Species Name: " + s.getName());
             System.out.println("Species ID: " + s.getId());
             System.out.println("Species MetaID: " + s.getMetaId());
        }
        System.out.println("Document 2 Species=================================================");
        List<Species> species2 = secondDocument.getModel().getListOfSpecies();
        for(Species s:species2)
        {
             System.out.println("Species Name: " + s.getName());
             System.out.println("Species ID: " + s.getId());
             System.out.println("Species MetaID: " + s.getMetaId());
        }
        System.out.println("Document 1 Parameters===============================================");
        List<org.sbml.jsbml.Parameter> param = firstDocument.getModel().getListOfParameters();
        for(org.sbml.jsbml.Parameter p:param)
        {
             System.out.println("Parameter Name: " + p.getName());
             System.out.println("Parameter ID: " + p.getId());
             System.out.println("Parameter MetaID: " + p.getMetaId());
        }
        System.out.println("Document 2 Parameters==============================================");
        List<org.sbml.jsbml.Parameter> param2 = secondDocument.getModel().getListOfParameters();
        for(org.sbml.jsbml.Parameter p:param2)
        {
             System.out.println("Parameter Name: " + p.getName());
             System.out.println("Parameter ID: " + p.getId());
             System.out.println("Parameter MetaID: " + p.getMetaId());
        }
        System.out.println("Document 1 Parameters===============================================");
        List<Reaction> react = firstDocument.getModel().getListOfReactions();
        for(Reaction r:react)
        {
             System.out.println("Reaction Name: " + r.getName());
             System.out.println("Reaction ID: " + r.getId());
             System.out.println("Reaction MetaID: " + r.getMetaId());
        }
        System.out.println("Document 2 Parameters==============================================");
        List<Reaction> react2 = secondDocument.getModel().getListOfReactions();
        for(Reaction r:react2)
        {
             System.out.println("Reaction Name: " + r.getName());
             System.out.println("Reaction ID: " + r.getId());
             System.out.println("Reaction MetaID: " + r.getMetaId());
        }
        System.out.println("=====================================================================");
    }
    
    
}
