package uk.ac.ncl.icos.svpcompiler.Compiler;

import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by owengilfellon on 26/09/2014.
 */
public abstract class AbstractCompiler<T> extends Observable implements ICompiler<T> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCompiler.class);
    
    // Maintains the list of parts to be compiled
    protected List<ICompilable> parts = new LinkedList<>();
    
    // Parts currently added to the model to allow contextual addition of interactions
    private final ArrayList<String>   addedParts = new ArrayList<>();
    
    protected int currentIndex = 0;

    private ICompilable previousPart = null;
    private ICompilable currentPart = null;


    @Override
    public boolean add(Compilable part)
    {
        previousPart = currentPart;
        currentPart = part;
        return this.parts.add(part);
    }

    @Override
    public boolean addAll(Collection<Compilable> parts)
    {
        boolean returnValue = true;

        for(Compilable part : parts) {
            boolean b = this.add(part);
            if(!b) {
                returnValue = false;
            }
        }

        return returnValue;
    }

    @Override
    public boolean partsParsed(List<String> parts)
    {
        Iterator<String> i = parts.iterator();
        HashMap<String, Part> modelParts = new HashMap<>();
        Iterator<ICompilable> j = this.parts.listIterator();

        while(j.hasNext()) {
            Part parsedPart = j.next().getPart();
            modelParts.put(parsedPart.getName(), parsedPart);
        }

        while(i.hasNext()) {
            if( !modelParts.containsKey( i.next() ) ) {
                return false;
            }
        }

        return true;
    }
    
    protected void addAddedPartLabel(String part) {
        addedParts.add(part);
    }

    @Override
    public boolean partsAdded(List<String> part) {

        Iterator<String> j = part.listIterator();

        while (j.hasNext()) {
            if( part.equals("mRNA") || !addedParts.contains(j.next())) {
                return false;
            }
        }

        return true;
    }
    
    protected void setCurrentPart(ICompilable compilable) {
        LOGGER.trace("Set curr part: [ {} ]", compilable.getPart().getName());
        this.currentPart = compilable;
    }
    
    protected void setPreviousPart(ICompilable compilable) {
        LOGGER.trace("Set prev part: [ {} ]", compilable.getPart().getName());
        this.previousPart = compilable;
    }

    public ICompilable getCurrentPart() {
        return currentPart;
    }

    public ICompilable getPreviousPart() {
        return previousPart;
    }
}
