package uk.ac.ncl.icos.svpcompiler.Compiler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import javax.xml.stream.XMLStreamException;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Interactions;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 * Compiles Parts into an SBML model (circa feb 2014)
 * @author owengilfellon
 */
public class OldSBMLCompiler
{
    // CHANGE serverURL TO ADDRESS OF SVP REPOSITORY
     
    private static final String serverURL = "http://sbol.ncl.ac.uk:8081";
    private static final Logger logger = LoggerFactory.getLogger(OldSBMLCompiler.class);
    
    private PartsHandler parthandler;
    private ModelBuilder builder;
    private SBMLHandler sbmlhandler;
    private String filename; 
    private SBMLDocument sbmlContainer;
    private String previousPart = null;
   // private String mrna = "mRNA";  
    private Part mrnapart = null;
    private SBMLDocument mrnaDoc = null;
    private SBMLDocument previousDoc = null;
    private ArrayList<String> addedParts;
    private HashSet<String> allIDs;
    private HashMap<String, Stack<String>> promoterMRNA;
    private List<Part> parsedParts;
    
    /**
     * @throws IOException if the compiler failed to connect to the standard virtual parts database
     */    
    public OldSBMLCompiler() throws IOException
    {
        parthandler = new PartsHandler(serverURL);
        sbmlhandler = new SBMLHandler();
        promoterMRNA = new HashMap<String, Stack<String>>();
    }
    
    /**
     * 
     * @param filename The filename for the SBML output, without extension
     * @throws IOException if the compiler failed to connect to the standard virtual parts database
     */
    public OldSBMLCompiler(String filename) throws IOException
    {
        this.filename = filename;
        parthandler = new PartsHandler(serverURL);
        sbmlhandler = new SBMLHandler();
        sbmlContainer = sbmlhandler.GetSBMLTemplateModel("SVP_Model");
        promoterMRNA = new HashMap<String, Stack<String>>();
        sbmlContainer = sbmlhandler.GetSBMLTemplateModel("SVP_Model");
        builder = new ModelBuilder(sbmlContainer);
        allIDs = new HashSet<String>();
    }
    
     /**
     * Takes Parts and compiles them in the order in which they exist in the List.
     * <ul>
     * <li>Operons must begin with a Promoter</li>
     * <li>Promoters can be followed by Operators or RBS Sites</li>
     * <li>Operators can be followed by Operators or RBS Sites</li>
     * <li>RBS can be followed by CDS</li>
     * <li>CDS can be followed by RBS or Terminators</li>
     * </ul>
     * All necessary Transcription Factors for Promoters or Operators must be provided.
     * @param parsedParts Ordered parts to compile into a model (Part defined in Expression.java)
     * @return An SBML document as a String
     * @throws Exception 
     */
    
    public String compile(List<Part> parsedParts) throws Exception
    {
        sbmlContainer = sbmlhandler.GetSBMLTemplateModel("SVP_Model");
        builder = new ModelBuilder(sbmlContainer);
        allIDs = new HashSet<String>();
        this.parsedParts = parsedParts;
        previousPart = null;
        addedParts = new ArrayList<String>();
        for (Part currentID:parsedParts) {
            addPart(currentID.getName());}
        String sbmlOutput = builder.GetModelString();
        
        if(sbmlOutput == null || sbmlOutput.equals("")) {
            throw new Exception("Output from ModelBuilder is Empty");
        }
        
        writeToSBMLDocument(sbmlOutput, filename != null ? filename : "output");
        return sbmlOutput;
    }
   

    /**
     * 
     * @param currentPart The ID of the part to add to the model
     * @throws Exception 
     */
    
    private void addPart(String currentPart) throws Exception
    {
        
        SBMLDocument currentDoc = null;

        // If a part is already in the model, it must be cloned
        
        if (getPart(currentPart).getType().equals("Terminator"))
        {
            logger.debug("Found terminator, clearing documents");
            mrnaDoc = null;
            mrnapart = null;
            previousDoc = null;
            previousPart = null;
        }
        else
        {
        
            if(addedParts.contains(currentPart)) {
                logger.debug("Cloning document for: [ {} ]", currentPart);
                currentDoc = builder.Clone(getSBMLDocument(currentPart), getPart(currentPart));
                if(currentDoc == null || currentDoc.toString().equals("")){ throw new Exception("Cloned SBML Document is empty");}
            }
            else {
                logger.debug("Retrieving document for: [ {} ]", currentPart);
                currentDoc = getSBMLDocument(currentPart);
                if(currentDoc == null || currentDoc.toString().equals("")){ throw new Exception("Retrieved SBML Document is empty");}
            }

            // If the part is a promoter, add mrna

            if(getPart(currentPart).getType().equals("Promoter")) {
                
                logger.debug("Is promoter: [ {} ]", currentPart);
                
                //mrna = addedParts.contains(mrna) ? getNextId(mrna) : mrna;
                mrnapart = parthandler.GetPart("mRNA");
                if(mrnapart == null){ throw new Exception("mRNA part is null");}
                this.mrnaDoc = parthandler.GetModel(mrnapart);
                logger.debug("Adding mRNA: [ {} ]", mrnapart.getName());
                
                if(mrnaDoc == null || mrnaDoc.toString().equals("")){ throw new Exception("mRNA document is null");}
            }

            if( previousPart != null && !getPart(currentPart).getType().equals("Terminator"))
            {

                // If mRNA is not in the document, add, then link previous part to mRNA

                if(getPart(currentPart).getType().equals("RBS")) {     
                    if(mrnapart == null || !addedParts.contains(mrnapart.getName())) {
                        logger.debug("Linking previous doc to [ {} ]",  mrnapart.getName());
                        printLinkDebug(previousDoc, this.mrnaDoc);
                        builder.Link(previousDoc, this.mrnaDoc);
                        logger.debug("Adding: [ {} ]", mrnapart.getName());
                        builder.Add(this.mrnaDoc);
                        addedParts.add(mrnapart.getName());
                    }

                    previousDoc = this.mrnaDoc;
                }

                 // Then, link current part (RBS) to current Operon's mRNA, so all RBS in Operon linked to same mRNA part

                logger.debug("Linking previous doc to: [ {} ]", currentPart);
                
                 builder.Link(previousDoc, currentDoc);

                 //printLinkDebug(previousDoc, currentDoc);
            }
            
            logger.debug("Adding: [ {} ]", currentPart);

            builder.Add(currentDoc);
            addedParts.add(currentPart);

            Interactions interactions;

            // If the part has interactions, the Interaction document must be added and linked

            if((interactions = parthandler.GetInteractions(getPart(currentPart)))!=null) {

                List<Interaction> partInteractions = interactions.getInteractions();
               
                if(partInteractions!=null) {

                    // Add all interactions for which the parts are present in the model

                    for(Interaction interaction:partInteractions) {

                        if(modelContainsParts(interaction.getParts(), parsedParts)) {

                                SBMLDocument currentInteraction = null;

                                /* If Interaction is already within the model, clone */

                                //String interactionID = addedParts.contains(interaction.getName()) ? getNextId(interaction.getName()) : interaction.getName();

                                if(addedParts.contains(interaction.getName())) {
                                     logger.debug("Cloning document for: [ {} ]", interaction.getName());
                                     currentInteraction = builder.CloneInteraction(getInteractionDocument(interaction), interaction);
                                     if(currentInteraction == null){ throw new Exception("Cloned Interaction  is null: ");}
                                }
                                else {
                                    logger.debug("Retrieving document for: [ {} ]", interaction.getName());
                                     currentInteraction = getInteractionDocument(interaction);
                                     if(currentInteraction == null){ throw new Exception("Retrieved Interaction  is null: ");}
                                }

                                // If the interaction is between proteins, then it can be added without linking

                                if(     getPart(currentPart).getType().equals("FunctionalPart")
                                        && addedParts.containsAll(interaction.getParts())
                                        && !interactionContains(interaction, "Operator") &&
                                        !interactionContains(interaction, "Promoter")) {
                                    
                                    logger.debug("Not transcriptional regulation, adding: [ {} ]", interaction.getName());

                                    builder.Add(currentInteraction);
                                    addedParts.add(interaction.getName());
                                }

                                // If the interaction is with a Promoter or operator, then the interaction document must be added

                                else if( getPart(currentPart).getType().equals("Promoter")
                                        || getPart(currentPart).getType().equals("Operator")) {

                                    printLinkDebug(currentDoc, currentInteraction);
                                    
                                    logger.debug("Is transcriptional regulation adding and linking to current doc: [ {} ]", interaction.getName());

                                    builder.Link(   currentDoc,
                                                    currentInteraction);
                                    builder.Add(    currentInteraction);
                                    addedParts.add(interaction.getName());
                                    currentDoc = currentInteraction;
                                }
                        }
                    }
                }
            }
            previousDoc = currentDoc;
            previousPart = currentPart;
        }
        

        
      
        
    }
    
    /**
     * 
     * @param part The string of the part needing an ID
     * @return A unique string, including the supplied string
     */
    
    private String getNextId(String part)
    {
        Random r = new Random();
        int id;

        do {id = r.nextInt(999999999);} while (allIDs.contains(""+id));
        
        allIDs.add(""+id);
        return part+"_"+id;
    }
    
    private Part getPart(String part) throws NoSuchElementException, IOException
    {
        return parthandler.GetPart(part);
    }
    
    private SBMLDocument getSBMLDocument(String part) throws IOException, XMLStreamException, Exception
    {
        Part svp = getPart(part);
        if(svp.getDisplayName() != null) {
            return parthandler.UpdateVisualNameAnnotation(svp.getDisplayName(), parthandler.GetModel(svp));
        }
        else {
            return parthandler.GetModel(svp);
        }
    }
    
    private SBMLDocument getInteractionDocument(Interaction interaction) throws IOException, XMLStreamException
    {
        return parthandler.GetInteractionModel(interaction);
    }

    
    private Interactions getPartInteractions(String part) throws IOException, XMLStreamException
    {
        return parthandler.GetInteractions(parthandler.GetPart(part));
    }

    public void writeToSBMLDocument(String sbmlOutput, String filename)
    {
       try
       {
           FileWriter f = new FileWriter(filename + ".xml");
           f.write(sbmlOutput);
           f.flush();
           // System.out.println("\nModel written to " + filename + ".xml");
       }
       catch(Exception e)
       {
           System.out.println(e.getMessage());
       }
    }
    
    private boolean interactionContains(Interaction interaction, String partType) throws NoSuchElementException, IOException
    {
        boolean contains = false;
        
        for(String s:interaction.getParts())
        {
            if(getPart(s).getType().equals(partType))
            {
                contains = true;
            }
        }
        
        return contains;
    }
    
    private boolean modelContainsParts(List<String> stringparts, List<Part> parts)
    {
        boolean value = true;
        Iterator<String> it = stringparts.iterator();
        HashMap<String, Part> modelParts = new HashMap<String, Part>();
        Iterator<Part> lit = parts.listIterator();
        
        while (lit.hasNext()) {
            Part cpart = lit.next();
            modelParts.put(cpart.getName(), cpart);
        }
        
        while(it.hasNext())  {
            if(!modelParts.containsKey(it.next()))
                value = false;
        }  

        return value;
    }
    
    private void printLinkDebug(SBMLDocument firstDocument, SBMLDocument secondDocument)
    {
        System.out.println("=====================================================================");
        System.out.println("Name: " + firstDocument.getModel().getName() + " - " + secondDocument.getModel().getName());
        System.out.println("ID: " + firstDocument.getModel().getId() + " - " + secondDocument.getModel().getId());
        System.out.println("MetaID: " + firstDocument.getModel().getMetaId() + " - " + secondDocument.getModel().getMetaId());
        System.out.println("Element: " + firstDocument.getModel().getElementName() + " - " + secondDocument.getModel().getElementName());
        System.out.println("SBO: " + firstDocument.getModel().getSBOTermID() + " - " + secondDocument.getModel().getSBOTermID());
        System.out.println("Document 1 Species=================================================");
        List<Species> species = firstDocument.getModel().getListOfSpecies();
        for(Species s:species)
        {
             System.out.println("Species Name: " + s.getName());
             System.out.println("Species ID: " + s.getId());
             System.out.println("Species MetaID: " + s.getMetaId());
        }
        System.out.println("Document 2 Species=================================================");
        List<Species> species2 = secondDocument.getModel().getListOfSpecies();
        for(Species s:species2)
        {
             System.out.println("Species Name: " + s.getName());
             System.out.println("Species ID: " + s.getId());
             System.out.println("Species MetaID: " + s.getMetaId());
        }
        System.out.println("Document 1 Parameters===============================================");
        List<Parameter> param = firstDocument.getModel().getListOfParameters();
        for(Parameter p:param)
        {
             System.out.println("Parameter Name: " + p.getName());
             System.out.println("Parameter ID: " + p.getId());
             System.out.println("Parameter MetaID: " + p.getMetaId());
        }
        System.out.println("Document 2 Parameters==============================================");
        List<Parameter> param2 = firstDocument.getModel().getListOfParameters();
        for(Parameter p:param2)
        {
             System.out.println("Parameter Name: " + p.getName());
             System.out.println("Parameter ID: " + p.getId());
             System.out.println("Parameter MetaID: " + p.getMetaId());
        }
        System.out.println("Document 1 Parameters===============================================");
        List<Reaction> react = firstDocument.getModel().getListOfReactions();
        for(Reaction r:react)
        {
             System.out.println("Reaction Name: " + r.getName());
             System.out.println("Reaction ID: " + r.getId());
             System.out.println("Reaction MetaID: " + r.getMetaId());
        }
        System.out.println("Document 2 Parameters==============================================");
        List<Reaction> react2 = firstDocument.getModel().getListOfReactions();
        for(Reaction r:react2)
        {
             System.out.println("Reaction Name: " + r.getName());
             System.out.println("Reaction ID: " + r.getId());
             System.out.println("Reaction MetaID: " + r.getMetaId());
        }
        System.out.println("=====================================================================");
    }
}
