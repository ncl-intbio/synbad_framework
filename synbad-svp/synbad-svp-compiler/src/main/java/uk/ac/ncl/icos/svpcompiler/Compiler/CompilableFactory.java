package uk.ac.ncl.icos.svpcompiler.Compiler;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.svpcompiler.parsing.Evaluation;
import uk.ac.ncl.icos.svpcompiler.parsing.ParsedPart;
import uk.ac.ncl.icos.svpcompiler.parsing.Parsing;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Interactions;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class CompilableFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompilableFactory.class);

    private static boolean MRNA_CREATED_BY_RBS = true;

    protected static List<ICompilable> postProcess(ICompilable compilable,  PartsHandler PART_HANDLER, SBMLModifier... modifiers) {

        List<ICompilable> compilables = new ArrayList<>();


        if (MRNA_CREATED_BY_RBS && compilable.getPart().getType().equals("RBS")) {
            if (LOGGER.isDebugEnabled())
                LOGGER.debug("Adding mRNA compilable for RBS: [ {} ]", compilable.getPart().getName());
            compilables.add(getMrna(PART_HANDLER));
        }

        if (modifiers.length > 0) {
            for (SBMLModifier mod : modifiers) {
                if (LOGGER.isDebugEnabled())
                    LOGGER.debug("Adding modifier for {}: [ {} ]", compilable.getPart().getName(), mod.getClass().getSimpleName());
                compilable.addModifier(mod);
            }
        }

        if (LOGGER.isDebugEnabled())
            LOGGER.debug("Adding compilable for {}: [ {} ]", compilable.getPart().getType(), compilable.getPart().getName());
        compilables.add(compilable);

        if (!MRNA_CREATED_BY_RBS && compilable.getPart().getType().equals("Promoter")) {
            if (LOGGER.isDebugEnabled())
                LOGGER.debug("Adding mRNA compilable for Promoter: [ {} ]", compilable.getPart().getName());
            compilables.add(getMrna(PART_HANDLER));
        }

        return compilables;
    }

    public static List<ICompilable> getCompilable(Part part, List<Interaction> internalInteractions, Map<Interaction, List<Part>> externalInteractions, String repositoryURL) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Getting compilable from: [ {} ], {} internal interactions, external interaction: [ {} ]", part.getName(), internalInteractions.size(), externalInteractions.keySet().size());

        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        try {

                List<Part> parts = internalInteractions.stream()
                        .flatMap(i -> i.getPartDetails().stream())
                        .filter(pd -> pd.getMathName().equals("EnvironmentConstant"))
                        .map(pd -> {
                            Part ec = new Part();
                            ec.setDisplayName(pd.getPartName());
                            ec.setName(pd.getPartName());
                            ec.setMetaType("Environment Constant");
                            return ec;
                        }).collect(Collectors.toList());
                parts.add(part);

            SBMLDocument partDocument;

            synchronized(SBMLDocument.class) {
                 partDocument = PART_HANDLER.CreatePartModel(part, new LinkedList<>(internalInteractions), parts);
            }

            if (partDocument == null && LOGGER.isDebugEnabled())
                LOGGER.debug("SBML document is null for: [ {} ]", part.getName());

            ICompilable compilable = new Compilable(
                    part,
                    new LinkedList<>(internalInteractions),
                    partDocument,
                    createInteractionDocuments(PART_HANDLER, externalInteractions));

            return postProcess(compilable, PART_HANDLER);

        } catch (NullPointerException e) {
            LOGGER.error("Could not create part document from {} and {} interactions: {}", part.getName(), internalInteractions.size(), e.getLocalizedMessage());
            return Collections.EMPTY_LIST;
        }
    }

    public static List<ICompilable>  getCompilable(Part part, List<Interaction> internalInteractions, Map<Interaction, List<Part>> externalInteractions, String repositoryURL, SBMLModifier... modifiers) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Getting compilable from: [ {} ], {} internal interactions, {} external interactions with {} interacting parts and {} modifiers", part.getName(), internalInteractions.size(), externalInteractions.keySet().size(), externalInteractions.values().size(), modifiers.length);

        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        SBMLDocument partDocument;

        synchronized(SBMLDocument.class) {
            partDocument = PART_HANDLER.CreatePartModel(part, internalInteractions);
        }

        if (partDocument == null && LOGGER.isDebugEnabled())
            LOGGER.debug("SBML document is null for: [ {} ]", part.getName());

        ICompilable compilable = new Compilable(
                part,
                internalInteractions,
                partDocument,
                createInteractionDocuments(PART_HANDLER, externalInteractions));

        return postProcess(compilable, PART_HANDLER, modifiers);

    }

    public static List<ICompilable> getCompilable(Part part, List<Interaction> internalInteractions, String repositoryURL, SBMLModifier... modifiers) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Getting compilable from: [ {} ], {} internal interactions and {} modifiers", part.getName(), internalInteractions.size(),  modifiers.length);

        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);

        try {
            List<Part> parts = internalInteractions.stream()
                    .flatMap(i -> i.getPartDetails().stream())
                    .filter(pd -> pd.getMathName().equals("EnvironmentConstant"))
                    .map(pd -> {
                        Part ec = new Part();
                        ec.setDisplayName(pd.getPartName());
                        ec.setName(pd.getPartName());
                        ec.setMetaType("Environment Constant");
                        return ec;
                    }).collect(Collectors.toList());
            parts.add(part);

            SBMLDocument partDocument;
            synchronized(SBMLDocument.class) {
                partDocument = PART_HANDLER.CreatePartModel(part, new LinkedList<>(internalInteractions), parts);
            }

            if (partDocument == null && LOGGER.isDebugEnabled())
                LOGGER.debug("SBML document is null for: [ {} ]", part.getName());

            ICompilable compilable = new Compilable(part, new LinkedList<>(internalInteractions), partDocument);
            return postProcess(compilable, PART_HANDLER, modifiers);

        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return Collections.EMPTY_LIST;
        }
    }

    public static List<ICompilable> getCompilables(String svpwrite, String repositoryURL, SBMLModifier... modifiers) throws IOException, XMLStreamException {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Getting compilable from SvpWrite with {} modifiers: [ {} ]", modifiers.length, svpwrite);

        PartsHandler PART_HANDLER = new PartsHandler(repositoryURL);
        List<ICompilable> compilables = new ArrayList<>();
        List<String> parts = Evaluation.eval(Parsing.parse(svpwrite)).stream()
                .map(pp -> pp.getVariable()).collect(Collectors.toList());

        for(String part: parts) {

            Part svp = PART_HANDLER.GetPart(part);

            if(svp!=null) {


                SBMLDocument svpDocument;

                synchronized(SBMLDocument.class) {
                    svpDocument = PART_HANDLER.GetModel(svp);
                }

                if(svpDocument == null && LOGGER.isDebugEnabled())
                    LOGGER.debug("SBML document is null for: [ {} ]", svp.getName());

                Interactions interactions = PART_HANDLER.GetInteractions(svp);
                List<Interaction> internalInteractions = PART_HANDLER.GetInternalInteractions(svp).getInteractions();
                Map<Interaction, SBMLDocument> interactionDocuments = getInteractionDocuments(PART_HANDLER, interactions);

                Compilable c = (interactionDocuments != null ) ?
                        new Compilable(svp, internalInteractions, svpDocument, interactionDocuments) :
                        new Compilable(svp, internalInteractions, svpDocument);

                compilables.addAll(postProcess(c, PART_HANDLER, modifiers));
            }

        }

        return compilables;
    }

    public static List<ICompilable> getCompilables(List<PartBundle> partBundles, SBMLModifier... modifiers) {

        List<ICompilable> compilables = new LinkedList<>();
        PartsHandler PART_HANDLER = new PartsHandler(SVPManager.getRepositoryUrl());

        for(PartBundle bundle : partBundles) {

                Map<Interaction, SBMLDocument> docs = new HashMap<>();
                bundle.getInteractions().keySet().forEach(i -> {
                    docs.put(bundle.getInteractions().get(i), bundle.getInteractionDocs().get(i));
                });
                ICompilable c = new Compilable(bundle.getPart(), bundle.getInternalInteractions(), bundle.getPartDocument(), docs);
                LOGGER.debug("{}:{} [ {} interactions | {} internal interactions ]", c.getPart().getName(), c.getPart().getType(), c.getInteractions().keySet().size(),  c.getInternalInteractions().size());
                compilables.addAll(postProcess(c, PART_HANDLER, modifiers));
            }


        return compilables;
    }
    
    private static Map<Interaction, SBMLDocument> createInteractionDocuments(PartsHandler PART_HANDLER, Map<Interaction, List<Part>> externalInteractions) {
        
        Map<Interaction, SBMLDocument> interactionDocuments = new HashMap<>();
        
        for (Interaction interaction : externalInteractions.keySet() ) {

                try {
                    SBMLDocument interactionDoc;

                    synchronized(SBMLDocument.class) {
                        interactionDoc = PART_HANDLER.CreateInteractionModel(
                                    externalInteractions.get(interaction), interaction);
                    }

                    interactionDocuments.put(interaction, interactionDoc);
                } catch (Exception e) {
                    LOGGER.error("Could not get SBML document for: [ {} ]", interaction.getName() );
                }
            }

        return interactionDocuments;
    }
    


    private static Map<Interaction, SBMLDocument> getInteractionDocuments(PartsHandler PART_HANDLER, Interactions interactions) {

        Map<Interaction, SBMLDocument> interactionDocuments = new HashMap<>();
        
        if(interactions != null && interactions.getInteractions() != null){

            for (Interaction interaction : interactions.getInteractions() ) {

                synchronized(SBMLDocument.class) {
                    SBMLDocument interactionDoc = null;
                    try {
                        interactionDoc = PART_HANDLER.GetInteractionModel(interaction);
                        interactionDocuments.put(interaction, interactionDoc);
                    } catch (IOException | XMLStreamException ex) {
                        LOGGER.error(ex.getLocalizedMessage());
                    }

                    if(interactionDoc == null && LOGGER.isDebugEnabled())
                        LOGGER.debug("SBML document is null for: [ {} ]", interaction.getName() );
                }
            }
        }
        
        return interactionDocuments;
    }


    
    private static Compilable getMrna(PartsHandler PART_HANDLER) {
        try {
                Part mrnaPart = PART_HANDLER.GetPart("mRNA");
                SBMLDocument mrnaDocument = PART_HANDLER.GetModel(mrnaPart);
                return new Compilable(mrnaPart,
                    PART_HANDLER.GetInternalInteractions(mrnaPart).getInteractions(),
                    mrnaDocument);
        } catch (IOException | XMLStreamException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
        
        return null;
    }
}
