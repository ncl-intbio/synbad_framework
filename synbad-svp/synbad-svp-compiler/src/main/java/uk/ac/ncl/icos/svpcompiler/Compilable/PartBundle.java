/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svpcompiler.Compilable;

import org.sbml.jsbml.SBMLDocument;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 * Gathers together various objects used by SVPs - the part, it's associated
 internalInteractions and SBML sbmlDocument.
 * @author owengilfellon
 */
public class PartBundle  {

    private static final SVPManager VPR_MANAGER = SVPManager.getSVPManager();
    private static final Logger LOGGER = LoggerFactory.getLogger(PartBundle.class);
    
    private final List<Interaction> internalInteractions;
    private final Part part;
    private final SBMLDocument document;
    private final Map<String, SBMLDocument> interactionDocs = new HashMap<>();
    private final Map<String, Interaction> interactions = new HashMap<>();

    private PartBundle(Part part, SBMLDocument document) {
        this.part = part;
        this.internalInteractions = new LinkedList<>();
        this.document = document;
    }

    public PartBundle(Part part, List<Interaction> internalInteractions)
    {
        this(part, !internalInteractions.isEmpty() ?
                VPR_MANAGER.createPartDocument(part, internalInteractions) :
                null);

        LOGGER.debug("Created document from {}:{} and {} internal interactions", part.getName(), part.getType(), internalInteractions.size());
        this.internalInteractions.addAll(internalInteractions);
    }

    public PartBundle(Part part, List<Interaction> internalInteractions, List<Part> internalInteractionParts)
    {
        this(part,
            !internalInteractions.isEmpty() ?
                !internalInteractionParts.isEmpty() ?
                    VPR_MANAGER.createPartDocument(part, internalInteractions, internalInteractionParts) :
                    VPR_MANAGER.createPartDocument(part, internalInteractions) :
            null);

        LOGGER.debug("Created document from {}:{} and {} internal interactions", part.getName(), part.getType(), internalInteractions.size());
        this.internalInteractions.addAll(internalInteractions);
    }

    public PartBundle(Part part, List<Interaction> internalInteractions,  Map<Interaction, List<Part>> interactions)
    {
        this(part, internalInteractions, Collections.EMPTY_LIST, interactions);
    }

    public PartBundle(Part part, List<Interaction> internalInteractions,  List<Part> internalInteractionParts, Map<Interaction, List<Part>> interactions)
    {
        this(part, internalInteractions, internalInteractionParts);

        LOGGER.debug("Created document from {}, {} internal interactions and {} external interactions", part.getName(), internalInteractions.size(), interactions.size());
        interactions.keySet().stream().forEach(i -> {
            LOGGER.debug("\tCreating external interaction document from {} with {} parts", i.getName(), interactions.get(i).size());
            SBMLDocument sbmlDocument = VPR_MANAGER.createInteractionDocument(i, interactions.get(i));
            this.interactionDocs.put(i.getName(), sbmlDocument);
            this.interactions.put(i.getName(), i);
        });
    }

    public Part getPart() { return part; }

    public SBMLDocument getPartDocument() {
        return document;
    }

    public Map<String, SBMLDocument> getInteractionDocs() {
        return interactionDocs;
    }
    
    public Map<String, Interaction> getInteractions() {
        return interactions;
    }
    
    public List<Interaction> getInternalInteractions() {
        return internalInteractions;
    }

    @Override
    public String toString() {
       String s = "Bundle [ P: " + getPart().getName() + ", ";
       s += " M: " + getPartDocument();
       s += " ID: " + getInteractions().values().size();  
       s += " II: " + getInternalInteractions().size();
       s += " ]";
       return s;
    }
    
    private boolean isEqual(PartBundle pb1, PartBundle pb2) {
        
        boolean match = true;

        if(!isEqual(pb1.getPart(), pb2.getPart())) {
            LOGGER.error("\tParts are not equal: {} and {}", pb1.getPart().getName(), pb2.getPart().getName());
            match = false;
        }
        
        Map<String, Interaction> interactions = new HashMap<>();
        
        pb1.getInternalInteractions().stream().forEach(i -> interactions.put(i.getName(), i));
        
        if(pb1.getInternalInteractions().size() != pb2.getInternalInteractions().size()) {
            LOGGER.error("\tInternal interactions do not match for {}: {} != {}", 
                    part.getName(),
                    pb1.getInternalInteractions().size(), 
                    pb2.getInternalInteractions().size());
        }
        
        for(Interaction i : pb2.getInternalInteractions()) {
            if(!interactions.containsKey(i.getName())) {
                LOGGER.error("\tDoes not contain interaction: {}", i.getName());
                match = false;
            } else if(!isEqual(interactions.get(i.getName()), i)) {
                LOGGER.error("\tInteractions not equal: {}", i.getName());
                match = false;
            }   
        };
        
        
        for(Interaction i : pb2.getInteractions().values()) {
            if(!this.interactions.containsKey(i.getName())) {
                LOGGER.error("\tDoes not contain external interaction: {}", i.getName());
                match = false;
            } else if(!isEqual(this.interactions.get(i.getName()), i)) {
                LOGGER.error("External interactions not equal: {}", i.getName());
                match = false;
            }   
        };
        
        if(pb1.document != null && pb2.document != null && !isEqual(pb1.document, pb2.document)) {
            LOGGER.error("SBMLDocuments are not equal: {}", part.getName());
            match = false;
        }
        
       
        
        return match;
        //pb1.getSviInstances();
    }
    
    private boolean isEqual(SBMLDocument doc1, SBMLDocument doc2) {
        
        LOGGER.debug("\tisEqualSBML({},{})", doc1.getModel().getName(), doc2.getModel().getName());
        
        boolean isEqual = true;
        
        if(doc1.getModel().getSpeciesCount() != doc2.getModel().getSpeciesCount()) {
            LOGGER.error("\tSpecies are not equal: {} != {}", doc1.getModel().getSpeciesCount(), doc2.getModel().getSpeciesCount());
            doc1.getModel().getListOfSpecies().stream().forEach(s -> LOGGER.debug("\tSpecies 1: {}", s.getName()));
            doc2.getModel().getListOfSpecies().stream().forEach(s -> LOGGER.debug("\tSpecies 2: {}", s.getName()));
            isEqual = false;
        }
        
        if(doc1.getModel().getReactionCount() != doc2.getModel().getReactionCount()) {
            LOGGER.error("\tReactions are not equal: {} != {}", doc1.getModel().getReactionCount(), doc2.getModel().getReactionCount());
            isEqual = false;
        }
        
        if(doc1.getModel().getConstraintCount() != doc2.getModel().getConstraintCount()) {
            LOGGER.error("\tConstraints are not equal: {} != {}", doc1.getModel().getConstraintCount(), doc2.getModel().getConstraintCount());
            isEqual = false;
        }
        
        if(doc1.getModel().getFunctionDefinitionCount() != doc2.getModel().getFunctionDefinitionCount()) {
            LOGGER.error("\tFunction Definitions are not equal: {} != {}", doc1.getModel().getFunctionDefinitionCount(), doc2.getModel().getFunctionDefinitionCount());
            isEqual = false;
        }
        if(doc1.getModel().getChildCount() != doc2.getModel().getChildCount()) {
            LOGGER.error("\tChildren are not equal: {} != {}", doc1.getModel().getChildCount(), doc2.getModel().getChildCount());
            isEqual = false;
        }
        
        if(doc1.getModel().getInitialAssignmentCount() != doc2.getModel().getInitialAssignmentCount() ) {
            LOGGER.error("\tInitial assignments are not equal: {} != {}", doc1.getModel().getInitialAssignmentCount() , doc2.getModel().getInitialAssignmentCount() );
            isEqual = false;
        }
        
        if(doc1.getModel().getKineticLawCount() != doc2.getModel().getKineticLawCount()) {
            LOGGER.error("\tKinetic Laws are not equal: {} != {}", doc1.getModel().getKineticLawCount() , doc2.getModel().getKineticLawCount());
            isEqual = false;
        }
        
        if(doc1.getModel().getLocalParameterCount() != doc2.getModel().getLocalParameterCount()) {
            LOGGER.error("\tLocal parameters are not equal: {} != {}", doc1.getModel().getLocalParameterCount() , doc2.getModel().getLocalParameterCount());
            isEqual = false;
        }
        
         if(doc1.getModel().getVariableCount() != doc2.getModel().getVariableCount()) {
            LOGGER.error("\tVariables are not equal: {} != {}", doc1.getModel().getVariableCount() , doc2.getModel().getVariableCount());
            isEqual = false;
        }
        
        if(doc1.getModel().getParameterCount() != doc2.getModel().getParameterCount()) {
            LOGGER.error("\tParameters are not equal: {} != {}", doc1.getModel().getParameterCount() , doc2.getModel().getParameterCount());
            doc1.getModel().getListOfParameters().stream().forEach(s -> LOGGER.error("\tParameter 1: {}", s.getName()));
            doc2.getModel().getListOfParameters().stream().forEach(s -> LOGGER.error("\tParameter 2: {}", s.getName()));
            isEqual = false;
        }
      
        return  isEqual;
    }
    
    private boolean bothEmpty(String s1, String s2) {
        if(s1 == null)
            return s2 == null || s2.isEmpty();
        if(s2 == null)
            return s1.isEmpty();
        return s1.isEmpty() && s2.isEmpty();
    }
    
    private boolean isEqual(Part p1, Part p2) {
        
        LOGGER.debug("\tisEqual({},{})", p1.getName(), p2.getName());
        
        if(!bothEmpty(p1.getDescription(), p2.getDescription()) && !p1.getDescription().equals(p2.getDescription())) {
            LOGGER.error("\tDescription {} does not equal {}", p1.getDescription(), p2.getDescription());
            return false;
        }
        
        if(!bothEmpty(p1.getDesignMethod(), p2.getDesignMethod()) && !p1.getDesignMethod().equals(p2.getDesignMethod())) {
            LOGGER.error("\tDesign method {} does not equal {}", p1.getDesignMethod(), p2.getDesignMethod());
            return false;
        }
        
        if(p1.getDisplayName() != null &&!bothEmpty(p1.getDisplayName(), p2.getDisplayName()) && !p1.getDisplayName().equals(p2.getDisplayName())) {
            LOGGER.error("\tDisplay name {} does not equal {}", p1.getDisplayName(), p2.getDisplayName());
            return false;
        }
        
        if(!bothEmpty(p1.getMetaType(), p2.getMetaType()) && !p1.getMetaType().equals(p2.getMetaType())) {
            LOGGER.error("\tMeta type {} does not equal {}", p1.getMetaType(), p2.getMetaType());
            return false;
        }
        
        
        if(!bothEmpty(p1.getName(), p2.getName()) && !p1.getName().equals(p2.getName())) {
            LOGGER.error("\tName {} does not equal {}", p1.getName(), p2.getName());
            return false;
        }
    

        if(!bothEmpty(p1.getOrganism(), p2.getOrganism()) && !p1.getOrganism().equals(p2.getOrganism())) {
            LOGGER.error("\tOrganism {} does not equal {}", p1.getOrganism(), p2.getOrganism());
            return false;
        }
        
        if(!bothEmpty(p1.getSequence(), p2.getSequence()) && !p1.getSequence().replaceAll("\n", "").equals(p2.getSequence().replaceAll("\n", ""))) {
            LOGGER.error("\tSequence {} does not equal {}", p1.getSequence().replaceAll("\n", ""), p2.getSequence().replaceAll("\n", ""));
            return false;
        }
        
        if(!bothEmpty(p1.getSequenceURI(), p2.getSequenceURI()) && !p1.getSequenceURI().equals(p2.getSequenceURI())) {
            LOGGER.error("\tSequence URI {} does not equal {}", p1.getSequenceURI(), p2.getSequenceURI());
            return false;
        }
        
        if(!bothEmpty(p1.getStatus(), p2.getStatus()) && !p1.getStatus().equals(p2.getStatus())) {
            LOGGER.error("\tStatus {} does not equal {}", p1.getStatus(), p2.getStatus());
            return false;
        }
        
        if(!bothEmpty(p1.getType(), p2.getType()) && !p1.getType().equals(p2.getType())) {
            LOGGER.error("\tType {} does not equal {}", p1.getType(), p2.getType());
            return false;
        }

        if(p1.getProperties() != null) {


        Set<ImmutableTriple<String, String, String>> properties = p1.getProperties().stream()
                .map(p -> new ImmutableTriple<>(
                        p.getName() == null ? "" : p.getName(),
                        p.getValue() == null ? "" : p.getValue(),
                        p.getDescription() == null ? "" : p.getDescription()))
                .collect(Collectors.toSet());

        boolean propertiesMatch = p2.getProperties().stream()
                .map(p -> new ImmutableTriple<>(
                        p.getName() == null ? "" : p.getName(),
                        p.getValue() == null ? "" : p.getValue(),
                        p.getDescription() == null ? "" : p.getDescription()))
                .allMatch((Triple <String, String, String> p) -> {
                    boolean contains = properties.contains(p);
                    if(!contains) {
                        LOGGER.error("\tDoes not contain property: {} : {} : {}", p.getLeft(), p.getMiddle(), p.getRight());
                    } 
                    return contains;
                 });
        
        return propertiesMatch;

        } else {
            return p2.getProperties() == null || p2.getProperties().isEmpty();
        }

    }
    
    private boolean isEqual(Interaction i1, Interaction i2) {
        
        LOGGER.debug("\tisEqual({},{})", i1.getName(), i2.getName());
        
        boolean match = true;
        
        if(!bothEmpty(i1.getName(), i2.getName()) && !i1.getName().equals(i2.getName())) {
            LOGGER.error("\tName {} does not equal {}", i1.getName(), i2.getName());
            match = false;
        }
        
        /*if(!bothEmpty(i1.getDescription(), i2.getDescription()) && !i1.getDescription().equals(i2.getDescription())) {
            LOGGER.error("Description {} does not equal {}", i1.getDescription(), i2.getDescription());
            match = false;
        }*/
        
        if(!bothEmpty(i1.getFreeTextMath(), i2.getFreeTextMath()) && !i1.getFreeTextMath().equals(i2.getFreeTextMath())) {
            LOGGER.error("\tFree Text Math {} does not equal {}", i1.getFreeTextMath(), i2.getFreeTextMath());
            match = false;
        }
        
        if(!bothEmpty(i1.getInteractionType(), i2.getInteractionType()) && !i1.getInteractionType().equals(i2.getInteractionType())) {
            LOGGER.error("vInteraction type {} does not equal {}", i1.getInteractionType(), i2.getInteractionType());
            match = false;
        }
        
        if(!bothEmpty(i1.getMathName(), i2.getMathName()) && !i1.getMathName().equals(i2.getMathName())) {
            LOGGER.error("\tMath Name {} does not equal {}", i1.getMathName(), i2.getMathName());
            match = false;
        }
        
        if(!i1.getIsInternal().equals(i2.getIsInternal())) {
            LOGGER.error("\tIs Internal {} does not equal {}", i1.getIsInternal(), i2.getIsInternal());
            match = false;
        }
        
        if(!i1.getIsReaction().equals(i2.getIsReaction())) {
            LOGGER.error("\tIs Reaction {} does not equal {}", i1.getIsReaction(), i2.getIsReaction());
            match = false;
        }
        
        if(!i1.getIsReversible().equals(i2.getIsReversible())) {
            LOGGER.error("\tIs Reaction {} does not equal {}", i1.getIsReversible(), i2.getIsReversible());
            match = false;
        }
        
        if(!i1.getParts().containsAll(i2.getParts()) || !i2.getParts().containsAll(i1.getParts())) {
            LOGGER.error("\tParts {} does not equal {}", i1.getParts().size(), i2.getParts().size());
            match = false;
        }
        
        if(i1.getParameters().size() != i2.getParameters().size()) {
                LOGGER.error("\tParameters not equal in {}: {} != {}", i1.getName(), i1.getParameters().size(), i2.getParameters().size());
                match = false;
        }
            
        
        Set<Quintuple> parameters = i1.getParameters().stream()
                .map(p -> new Quintuple(
                        // don't check evidence type as not currently parsed...
                        p.getEvidenceType() == null ? "" : "",
                        p.getName() == null ? "" : p.getName(),
                        p.getParameterType() == null ? "" : p.getParameterType(),
                        p.getScope() == null ? "" : p.getScope(),
                        p.getValue() == null ? 0.0 : p.getValue()))
                .collect(Collectors.toSet());

        boolean parametersMatch = !i2.getParameters().isEmpty() && i2.getParameters().stream()
                .map(p -> new Quintuple(
                        p.getEvidenceType() == null ? "" : p.getEvidenceType(),
                        p.getName() == null ? "" : p.getName(),
                        p.getParameterType() == null ? "" : p.getParameterType(),
                        p.getScope() == null ? "" : p.getScope(),
                        p.getValue() == null ? 0.0 : p.getValue()))
                .allMatch((Quintuple p) -> {
                    boolean contains = parameters.contains(p);
                    if(!contains) {
                        LOGGER.error("\tDoes not contain parameter: {} : {} : {} : {} : {}", p.getObj1(), p.getObj2(), p.getObj3(), p.getObj4(), p.getObj5());
                         } 
                    return contains;
                 });
        
        if(!parametersMatch) {
            LOGGER.error("\tCould not find parameter in...");
            parameters.stream().forEach( c -> LOGGER.error("\t\t... {} : {} : {} : {} : {}", c.getObj1(), c.getObj2(), c.getObj3(), c.getObj4(), c.getObj5()));
            
            match = false;
        }
        
        if(i1.getConstraints() != null && i2.getConstraints() != null) {
            
            if(i1.getConstraints().size() != i2.getConstraints().size()) {
                LOGGER.error("\tConstraints not equal: {} != {}", i1.getConstraints().size(), i2.getConstraints().size());
            }
            
            Set<Quintuple> constraints = i1.getConstraints().stream()
                .map(p -> new Quintuple(
                        p.getQualifier() == null ? "" : p.getQualifier(),
                        p.getSourceID() == null ? "" : p.getSourceID(),
                        p.getSourceType() == null ? "" : p.getSourceType(),
                        p.getTargetID() == null ? "" : p.getTargetID(),
                        p.getTargetType() == null ? 0.0 : p.getTargetType()))
                .collect(Collectors.toSet());

            boolean constraintsMatch = i2.getConstraints().stream()
                    .map(p -> new Quintuple(
                            p.getQualifier() == null ? "" : p.getQualifier(),
                            p.getSourceID() == null ? "" : p.getSourceID(),
                            p.getSourceType() == null ? "" : p.getSourceType(),
                            p.getTargetID() == null ? "" : p.getTargetID(),
                            p.getTargetType() == null ? 0.0 : p.getTargetType()))
                    .allMatch((Quintuple p) -> {
                        boolean contains = constraints.contains(p);
                        if(!contains) {
                            LOGGER.error("\tDoes not contain constraint: {} : {} : {} : {} : {}", p.getObj1(), p.getObj2(), p.getObj3(), p.getObj4(), p.getObj5());
                        } 
                        return contains;
                     });

            if(!constraintsMatch) {
                 LOGGER.error("\tCould not find constraint in...");
                 constraints.stream().forEach( c -> LOGGER.error("\t\t... {} : {} : {} : {} : {}", c.getObj1(), c.getObj2(), c.getObj3(), c.getObj4(), c.getObj5()));
                        
                match = false;
            }
        } else if (i1.getConstraints() == null ^ i2.getConstraints() == null) {
            // if they're not both null, print error
            LOGGER.error("\tA constraints list is null: {}", i1.getName());
        }
        
        Set<Quintuple> partDetails = i1.getPartDetails().stream()
                .map(p -> {
                    Quintuple q = new Quintuple(
                        p.getInteractionRole() == null ? "" : p.getInteractionRole(),
                        p.getMathName() == null ? "" : p.getMathName(),
                        p.getNumberOfMolecules(),
                        p.getPartForm() == null ? "" : p.getPartForm(),
                        p.getPartName() == null ? 0.0 : p.getPartName());
                    //LOGGER.debug("Found part detail: {} : {} : {} : {} : {}", q.getObj1(), q.getObj2(), q.getObj3(), q.getObj4(), q.getObj5());
                    return q;})
                .collect(Collectors.toSet());
        
        boolean partDetailsMatch = i2.getPartDetails().stream()
                .map(p -> new Quintuple(
                        p.getInteractionRole() == null ? "" : p.getInteractionRole(),
                        p.getMathName() == null ? "" : p.getMathName(),
                        p.getNumberOfMolecules(),
                        p.getPartForm() == null ? "" : p.getPartForm(),
                        p.getPartName() == null ? 0.0 : p.getPartName()))
                .allMatch((Quintuple p) -> {
                    boolean contains = partDetails.contains(p);
                    if(!contains) {
                        LOGGER.error("\tNo part detail: {} : {} : {} : {} : {}", p.getObj1(), p.getObj2(), p.getObj3(), p.getObj4(), p.getObj5());
                        
                    } 
                    return contains;
                 });
        
        if(!partDetailsMatch) {
            LOGGER.error("\tCould not find partDetail in...");
            partDetails.stream().forEach( c -> LOGGER.error("\t\t... {} : {} : {} : {} : {}", c.getObj1(), c.getObj2(), c.getObj3(), c.getObj4(), c.getObj5()));
                       
                    
            match = false;
        }
        
        SBMLDocument doc1 = interactionDocs.get(i1.getName());
        SBMLDocument doc2 = interactionDocs.get(i2.getName());
        
        if(doc1 == null) {
            
            if(doc2 != null) {
                LOGGER.error("SBMLDocument 1 is null");
                match = false;
            }
                
        } else if(doc2 == null) {
            LOGGER.error("SBMLDocument 2 is null");
            match = false;
        } else if(!isEqual(doc1, doc2)) {
            LOGGER.error("SBMLDocuments are not equal: {}", i1.getName());
            match = false;
        }

        return match;
    }
       
    private class Quintuple {
        private final Object obj1;
        private final Object obj2;
        private final Object obj3;
        private final Object obj4;
        private final Object obj5;

        public Quintuple(Object obj1, Object obj2, Object obj3, Object obj4, Object obj5) {
            this.obj1 = obj1;
            this.obj2 = obj2;
            this.obj3 = obj3;
            this.obj4 = obj4;
            this.obj5 = obj5;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null || !obj.getClass().equals(Quintuple.class))
                return false;
            
            Quintuple other = (Quintuple) obj;
            
            if(obj1.equals(other.obj1) &&
                    obj2.equals(other.obj2) &&
                    obj3.equals(other.obj3) &&
                    obj4.equals(other.obj4) &&
                    obj5.equals(other.obj5))
                return true;
            
            return false;
        }

        public Object getObj1() {
            return obj1;
        }

        public Object getObj2() {
            return obj2;
        }

        public Object getObj3() {
            return obj3;
        }

        public Object getObj4() {
            return obj4;
        }

        public Object getObj5() {
            return obj5;
        }

        @Override
        public int hashCode() {
            return obj1.hashCode() + obj2.hashCode() + obj3.hashCode() + obj4.hashCode() + obj5.hashCode() * 19;
        } 
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof PartBundle))
            return false;
            
        PartBundle pb = (PartBundle)obj;
        
        return isEqual(this, pb);
    }
    
    

    


}
