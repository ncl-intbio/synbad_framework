package uk.ac.ncl.icos.svpcompiler.Compilable;

import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.text.parser.ParseException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 * Created by owengilfellon on 28/09/2014.
 */
public class Compilable implements ICompilable {
    
    private final Logger LOGGER = LoggerFactory.getLogger(Compilable.class);

    private final List<SignalType> inputSignal = new ArrayList<>();
    private final List<SignalType> outputSignal = new ArrayList<>();
    protected List<SBMLModifier> modifiers = new ArrayList<>();
    private List<Interaction> internalInteractions = new ArrayList<>();
    private Part svp;
    protected SBMLDocument document = null;
    protected Map<Interaction, SBMLDocument> interactions = new HashMap<>();
    //private static final String SVP_REPOSITORY_URL = "http://sbol-dev.ncl.ac.uk:8081";
    //private static final String SVP_REPOSITORY_URL = "http://sbol.ncl.ac.uk:8081";
    //protected final PartsHandler PART_HANDLER = new PartsHandler(SVP_REPOSITORY_URL);

    /**
     * Constructor for a Compilable encapsulating an SVP with no interactions with other SVPs in model
     * @param svp
     * @param internalInteractions
     * @param document SBMLDocument for the SVP
     */
    public Compilable(Part svp, List<Interaction> internalInteractions, SBMLDocument document)
    {
        this.document = document;
        this.internalInteractions = internalInteractions;
        setPart(svp);
    }

    /**
     * Constructor for a Compilable encapsulating an SVP with interactions with other SVPs in model
     * @param svp
     * @param internalInteractions
     * @param document SBMLDocument for the SVP
     * @param interactions Interactions with other SVPs, mapping Interactions to SBMLDocuments
     */
    public Compilable(Part svp, List<Interaction> internalInteractions, SBMLDocument document, Map<Interaction, SBMLDocument> interactions)
    {
        this.document = document;
        this.interactions = interactions;
        this.internalInteractions = internalInteractions;
        setPart(svp);
    }

    @Override
    public List<SignalType> getInputSignal()
    {
        return inputSignal;
    }

    @Override
    public List<SignalType> getOutputSignal()
    {
        return outputSignal;
    }

    @Override
    public List<Interaction> getInternalInteractions() {
        return internalInteractions;
    }
    
    @Override
    public void addModifier(SBMLModifier modifier) { modifiers.add(modifier); }

    @Override
    public Part getPart() { return svp; }

    protected void setPart(Part part)
    {
        boolean inputFound = false;
        boolean outputFound = false;

        if(LOGGER.isDebugEnabled())
            LOGGER.debug(part.getType());

        
        if(part.getName().contains("mRNA")) {
            inputFound = true;
            inputSignal.add(SignalType.PoPS);
            outputFound = true;
            outputSignal.add(SignalType.mRNA);
        } else if (part.getType().equalsIgnoreCase("protein complex")) {
            inputFound = true;
            inputSignal.add(SignalType.None);
            outputFound = true;
            outputSignal.add(SignalType.Species);
        } else if (!part.getType().equalsIgnoreCase("terminator")){
        
            if(internalInteractions!=null) {

                for(Interaction event : internalInteractions) {

                    for(Parameter parameter : event.getParameters()) {

                        if(parameter.getParameterType().equalsIgnoreCase("pops")) {
                            if(parameter.getName().contains("Input")) {
                                inputSignal.add(SignalType.PoPS);
                                LOGGER.trace("{}: Found I: PoPS", part.getName());
                                inputFound = true;
                            } else if (parameter.getName().contains("Output")){
                                outputSignal.add(SignalType.PoPS);
                                LOGGER.trace("{}: Found O: PoPS", part.getName());
                                outputFound = true;
                            }
                        } else if (parameter.getParameterType().equalsIgnoreCase("rips")) {
                            if(parameter.getName().contains("Input")) {
                                inputSignal.add(SignalType.RiPS);
                                LOGGER.trace("{}: Found I: RiPS", part.getName());
                                inputFound = true;
                            } else if (parameter.getName().contains("Output")){
                                outputSignal.add(SignalType.RiPS);
                                LOGGER.trace("{}: Found O: RiPS", part.getName());
                                outputFound = true;
                            }
                        } else if (parameter.getName().contains("mRNA")) {
                            if(parameter.getName().contains("Input")) {
                                inputSignal.add(SignalType.mRNA);
                                LOGGER.trace("{}: Found I: mRNA", part.getName());
                                inputFound = true;
                            } else if (parameter.getName().contains("Output")){
                                outputSignal.add(SignalType.mRNA);
                                LOGGER.trace("{}: Found O: PoPS", part.getName());
                                outputFound = true;
                            }
                        }
                    }
                }
            } 
            
        }
        
        if(!inputFound) {
            inputSignal.add(SignalType.None);
        }
        
        if(!outputFound) {
            outputSignal.add(SignalType.None);
        }

        
        this.svp = part;
       
    }

    @Override
    public SBMLDocument getPartDocument() {

        SBMLDocument filtered = document;

        for(SBMLModifier modifier : modifiers) {
            try {
                filtered = modifier.modify(filtered);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return filtered;
    }

    @Override
    public Map<Interaction, SBMLDocument> getInteractions() {

        for(Interaction i : interactions.keySet()) {
            for(SBMLModifier modifier : modifiers) {
                try {
                    modifier.modify(interactions.get(i));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return interactions;
    }



}
