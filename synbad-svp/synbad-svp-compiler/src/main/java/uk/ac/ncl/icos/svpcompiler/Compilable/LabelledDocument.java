/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svpcompiler.Compilable;

import org.sbml.jsbml.SBMLDocument;

/**
 *
 * @author owengilfellon
 */
public class LabelledDocument {
        
    private final String label;
    private final SBMLDocument document;

    public LabelledDocument(String label, SBMLDocument document) {
        this.label = label;
        this.document = document;
    }

    public SBMLDocument getDocument() {
        return document;
    }

    public String getLabel() {
        return label;
    }

}