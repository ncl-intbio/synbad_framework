package uk.ac.ncl.icos.svpcompiler.parsing;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;

/**
 * Provides a public interface for using the OLDCompiler.
 * @author Owen Gilfellon
 */
public class SVPParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(SVPParser.class);

    public static List<String> getParts(String svpstatement)
    {
        try
        {   List<String> parsedParts = new LinkedList<>();
            List<ParsedPart> parts = Evaluation.eval(Parsing.parse(svpstatement));
            for(ParsedPart part:parts)
            {
                parsedParts.add(part.getVariable());
            }
            return parsedParts;
        }
        catch(Exception e)
        {
            LOGGER.error("Could not get parts list from {}", svpstatement);
            return null;
        }
    }

    public static List<String> getParts(String svpstatement, BaseType... types)
    {
        try
        {
            Set<BaseType> typesSet = new HashSet<>(Arrays.asList(types));
            List<ParsedPart> parts = Evaluation.eval(Parsing.parse(svpstatement));
            return parts.stream().filter(p -> typesSet.contains(p.getType()))
                    .map(ParsedPart::getVariable).collect(Collectors.toList());
        }
        catch(Exception e)
        {
            LOGGER.error("Could not get parts list from {}", svpstatement);
            return null;
        }
    }
}
