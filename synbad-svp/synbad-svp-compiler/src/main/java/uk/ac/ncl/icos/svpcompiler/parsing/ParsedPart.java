package uk.ac.ncl.icos.svpcompiler.parsing;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class ParsedPart implements Expression
{
    private String variable;
    private BaseType type;
    ParsedPart(String variable, BaseType type)
    {
        this.variable = variable;
        this.type = type;
    }

    public String getVariable()
    {
        return variable;
    }

    public BaseType getType()
    {
        return type;
    }

    public String toString()
    {
        return variable + ":" + type;
    }

    public <A> A accept (ExpVisitor<A> v)
    {
        return v.visit(this);
    }
}
