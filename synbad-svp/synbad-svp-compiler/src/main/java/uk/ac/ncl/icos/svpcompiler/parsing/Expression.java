package uk.ac.ncl.icos.svpcompiler.parsing;



import java.util.ArrayList;
import java.util.List;




/**
 * Abstract syntax is represented by a family of classes - one for each syntactic construct - implementing a common interface, Expression.
 */
interface Expression
{
    /**
     * Allows abstract syntax objects to be 'visited' by visitor pattern objects
     * @param v the visitor
     * @param result of applying the visitor to the expression 
     * @return
     */
	public <A> A accept (ExpVisitor<A> v);
}

/**
 * Common interface for Expression visitors
 * @param <A> Result type returned by the visitor
 */
interface ExpVisitor<A>
{
	public A visit(ParsedPart e);
	public A visit(Def e);
	public A visit(Var e);
	public A visit(App e);
    public A visit(ExpressionList e);
}



class Def implements Expression
{
	private String functionName;
	private List<String> parameterNames;
	private List<Type> parameterTypes;
	private Type returnType;
	private Expression body;
	private Expression rest;
	
	public Def(String functionName, List<String> parameterNames, List<Type> parameterTypes, Type returnType, Expression body, Expression rest)
	{
		this.functionName = functionName;
		this.parameterNames = parameterNames;
		this.parameterTypes = parameterTypes;
		this.returnType = returnType;
		this.body = body;
		this.rest = rest;
	}

	String getFunctionName()
	{
		return functionName;
	}
	
	List<String> getParameterNames()
	{
		return parameterNames;
	}
	
	List<Type> getParameterTypes()
	{
		return parameterTypes;
	}
	
	Type getReturnType()
	{
		return returnType;
	}
	
	Expression getBody()
	{
		return body;
	}
	
	Expression getRest()
	{
		return rest;
	}
	
    public String toString()
    {
        return "fun " + functionName + "(" + parameterNames + ":" +  parameterTypes + ")" + ":" + returnType + "{" + body + "}" + rest;
    }
    
    public <A> A accept (ExpVisitor<A> v)
    {
        return v.visit(this);
    }
}

class Var implements Expression
{
    private String var;
    
    public Var(String var)
    {
        this.var = var;
    }
    
    public String getVar()
    {
        return var;
    }
    
    public String toString()
    {
        return var;
    }
    
    public <A> A accept (ExpVisitor<A> v)
    {
        return v.visit(this);
    }
}

class App implements Expression
{
    private String functionName;
    private List<Expression> arguments;
    
    public App(String functionName, List<Expression> arguments)
    {
        this.functionName = functionName;
        this.arguments = arguments;
    }
    
    public String getFunctionName()
    {
        return functionName;
    }
    
    public List<Expression> getArguments()
    {
        return arguments;
    }
    
    public String toString()
    {
        return functionName + "(" + arguments + ")";
    }

    public <A> A accept(ExpVisitor<A> v)
    {
        return v.visit(this);
    }
}

/**
 * Represents a sequence of compositions e1;e2;...;en
 */
class ExpressionList implements Expression
{
    private List<Expression> list;

    public ExpressionList(List<Expression> list)
    {
        this.list = new ArrayList<Expression>(list);
    }
    
    public List<Expression> getList()
    {
        return list;
    }
    
    public String toString()
    {
        String string = "";
        
        for (Expression e : list) string += ";" + e;
        
        return string;
    }
    
    public <A> A accept(ExpVisitor<A> v)
    {
        return v.visit(this);
    }
}