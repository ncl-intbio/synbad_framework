package uk.ac.ncl.icos.svpcompiler.Compiler;

import java.io.IOException;
import java.util.*;

import com.sun.org.apache.regexp.internal.RE;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import javax.xml.stream.XMLStreamException;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class CompilationDirector {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompilationDirector.class);
    private SBMLCompiler compiler;
    private List<ICompilable> compilable;
    private final String REPOSITORY_URL;
    private SBMLModifier[] modifiers = new SBMLModifier[]{};

    public CompilationDirector(String repositoryUrl)
    {
        this.compilable = new LinkedList<>();
        this.REPOSITORY_URL = repositoryUrl;
    }

    public void setSbmlModifiers(SBMLModifier... modifiers) {
        this.modifiers = modifiers;
    }

    public boolean setModelSvpWrite(String svpWrite) {
        try {
            this.compilable = CompilableFactory.getCompilables(svpWrite, REPOSITORY_URL, modifiers);
        } catch (Exception e) {
            LOGGER.error("Could not create compilables", e);
            return false;
        }
        return true;
    }

    public boolean setModelCompilables(List<ICompilable> compilables) {
        this.compilable = new LinkedList<>(compilables);
        this.compilable.forEach(c -> Arrays.stream(modifiers).forEach(c::addModifier));
        return true;
    }

    public boolean setModelPartBundles(List<PartBundle> partBundles) {
       return setModelCompilables(CompilableFactory.getCompilables(partBundles));
    }

    private boolean compileSBML()
    {
        this.compiler.setParts(this.compilable);
        return this.compiler.compileAll();
    }

    public SBMLDocument getSBML(String modelId)
    {
        this.compiler = new SBMLCompiler(modelId);
        this.compileSBML();
        return compiler.getDocument();
    }

    public String getSBMLString(String modelId)
    {
        this.compiler = new SBMLCompiler(modelId);
        this.compileSBML();
        return compiler.getModelString();
    }
}
