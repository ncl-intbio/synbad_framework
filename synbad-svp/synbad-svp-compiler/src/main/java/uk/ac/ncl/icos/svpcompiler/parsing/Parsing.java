package uk.ac.ncl.icos.svpcompiler.parsing;


import org.codehaus.jparsec.*;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.functors.Pair;
import org.codehaus.jparsec.functors.Tuple3;
import org.codehaus.jparsec.functors.Tuple5;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses concrete syntax to Expression abstraction syntax trees
 */
public class Parsing
{
    private static final Parser<Void> IGNORED = Scanners.WHITESPACES.skipMany();

    private static final Terminals OPERATORS = Terminals.operators(":", ";", ",", "(", ")", "{", "}", "<", ">");
    private static final Terminals KEYWORDS = Terminals.caseSensitive(new String[]{}, new String[] {"fun", "Prom", "CDS", "RBS", "Ter", "Op"});

    private static final Parser<?> TOKENIZER = Parsers.or(
            OPERATORS.tokenizer(), 
            KEYWORDS.tokenizer());

    private static final Parser<BaseType> BASETYPE = Parsers.or(
            KEYWORDS.token("Prom").retn(BaseType.PROM),
            KEYWORDS.token("CDS").retn(BaseType.CDS),
            KEYWORDS.token("RBS").retn(BaseType.RBS),
            KEYWORDS.token("Ter").retn(BaseType.TER),
            KEYWORDS.token("Op").retn(BaseType.OP));
    
    // provides a shorthand form for types with the same lhs and rhs basetype, so you can write say t:Prom instead of t:<Prom, Prom>
    private static final Parser<Type> LIFTEDBASETYPE = BASETYPE.map(new Map<BaseType, Type>() {
        public Type map(BaseType b) {
            return new Type(b, b);
        }
    });
    
    private static final Parser<Type> BASETYPEPAIR = Parsers.between(OPERATORS.token("<"),
            Parsers.tuple(BASETYPE, OPERATORS.token(","), BASETYPE),
            OPERATORS.token(">")).map(new Map<Tuple3<BaseType, Token, BaseType>, Type>() {
                public Type map(Tuple3<BaseType, Token, BaseType> baseTypes) {
                    return new Type(baseTypes.a, baseTypes.c);
                }                
            });
    
    private static final Parser<Type> TYPE = Parsers.or(
            BASETYPEPAIR,
            LIFTEDBASETYPE);

    private static final Parser<String> IDENTIFIER = Terminals.Identifier.PARSER.map(new Map<String, String>() {
        public String map(String s) {
            return s;
        }
    });

    private static Parser<Expression> expression()
    {
        Parser.Reference<Expression> ref = Parser.newReference();

        Parser<ParsedPart> part = Parsers.tuple(IDENTIFIER, OPERATORS.token(":"), BASETYPE).map(new Map<Tuple3<String, Token, BaseType>, ParsedPart>() {
            public ParsedPart map(Tuple3<String, Token, BaseType> s)
            {
                return new ParsedPart(s.a, s.c);
            }
        });

        Parser<Var> var = IDENTIFIER.map(new Map<String, Var>() {
            public Var map(String s)
            {
                return new Var(s);
            }        
        });    

        Parser<Def> def = Parsers.tuple(Parsers.sequence(KEYWORDS.token("fun"), IDENTIFIER),
                Parsers.between(OPERATORS.token("("), Parsers.pair(IDENTIFIER, Parsers.sequence(OPERATORS.token(":"), TYPE)).sepBy1(OPERATORS.token(",")), OPERATORS.token(")")),
                Parsers.sequence(OPERATORS.token(":"), TYPE),
                Parsers.between(OPERATORS.token("{"), ref.lazy(), OPERATORS.token("}")),
                ref.lazy()).map(new Map<Tuple5<String, List<Pair<String, Type>>, Type, Expression, Expression>, Def>() {
                    public Def map(Tuple5<String, List<Pair<String, Type>>, Type, Expression, Expression> t)
                    {
                        List<String> parameterNames = new ArrayList<String>();
                        List<Type> parameterTypes = new ArrayList<Type>();
                        
                        for (Pair<String, Type> pair : t.b)
                        {
                            parameterNames.add(pair.a);
                            parameterTypes.add(pair.b);
                        }
                        
                        return new Def(t.a, parameterNames, parameterTypes, t.c, t.d, t.e);
                    }
                });

        Parser<App> app = Parsers.pair(IDENTIFIER,
                Parsers.between(OPERATORS.token("("), ref.lazy().sepBy1(OPERATORS.token(",")), OPERATORS.token(")"))).map(new Map<Pair<String, List<Expression>>, App>() {
                    public App map(Pair<String, List<Expression>> app)
                    {
                        return new App(app.a, app.b);
                    }
                });
        
        Parser<ExpressionList> expressionList = Parsers.or(def, app, part, var).sepBy1(OPERATORS.token(";")).map(new Map<List<Expression>, ExpressionList>() {
                    public ExpressionList map(List<Expression> list)
                    {
                        return new ExpressionList(list);
                    }
                });

        Parser<Expression> expression = Parsers.or(expressionList, def, app, part, var);
        ref.set(expression);
        return expression;
    }
    
    private static final Parser<Expression> EXPRESSION = expression();

    /**
     * @param source Concrete syntax as a string
     * @return Abstract syntax as an Expression object
     */
    public static Expression parse(String source)
    {
        return EXPRESSION.from(TOKENIZER, IGNORED).parse(source);
    }
}
