/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svpcompiler.tests;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author owengilfellon
 */
public class CompilationDirectorTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompilationDirectorTests.class);
    private final Map<String, String> models;
    private static final String REPOSITORY_URL = "http://vm-vpr.bioswarm.net:8081";

    public CompilationDirectorTests() {

        models = new HashMap<>();

//        models.put("starting_model", "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
//                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");
//
//        models.put("oscillating_model", "BO_2887:Prom; BO_28222:RBS; BO_32227:CDS; BO_4296:Ter; BO_27718:Prom; " +
//                "BO_3793:Op; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_28157:RBS; BO_32307:CDS; BO_4296:Ter");

//        models.put("double_pspas_model", "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
//                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");
        models.put("one_pspark_model", "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter");

        models.put("three_pspark_model", "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; PspaRK:Prom; PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter");

        models.put("four_pspark_model", "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; PspaRK:Prom; PspaRK:Prom; PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter");

//        models.put("double_pspas_model2", "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
//                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; " +
//                "SpaK:CDS; BO_4296:Ter");

        // These take too long to compile, so are not routinely tested

        // models.put("experiment_131126_174025", "PspaS:Prom; RBS_SpaS:RBS; SpaR:CDS; RBS_SpaR:RBS; SpaK:CDS; BO_27957:RBS; BO_31762:CDS; BO_28152:RBS; BO_32227:CDS; BO_4296:Ter; PspaS:Prom; BO_27957:RBS; BO_31762:CDS; BO_5297:Ter; BO_3477:Prom; BO_28485:RBS; BO_28892:CDS; BO_5972:Ter; BO_27657:Prom; BO_28485:RBS; BO_28892:CDS; BO_28207:RBS; BO_31152:CDS; BO_28399:RBS; BO_28831:CDS; BO_5169:Ter; BO_3477:Prom; BO_28485:RBS; BO_28892:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_28414:RBS; BO_32077:CDS; BO_5972:Ter; BO_2914:Prom; BO_3963:Op; BO_28411:RBS; BO_31152:CDS; BO_28399:RBS; SpaK:CDS; BO_28181:RBS; BO_32227:CDS; BO_4426:Ter; BO_2872:Prom; BO_27901:RBS; BO_30695:CDS; BO_5559:Ter; BO_2709:Prom; BO_4022:Op; BO_3781:Op; BO_3692:Op; BO_28372:RBS; SpaR:CDS; BO_27981:RBS; BO_30682:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_27901:RBS; BO_30695:CDS; BO_4875:Ter; BO_3388:Prom; BO_3538:Op; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_27738:Prom; BO_28207:RBS; BO_31152:CDS; BO_5972:Ter; BO_27657:Prom; BO_28485:RBS; BO_28892:CDS; BO_27930:RBS; BO_31152:CDS; BO_28399:RBS; BO_28831:CDS; BO_27893:RBS; BO_31772:CDS; BO_5169:Ter");
        // models.put("experiment_131127_132615", "BO_2917:Prom; BO_28510:RBS; SpaK:CDS; BO_27993:RBS; BO_32147:CDS; BO_27893:RBS; BO_32743:CDS; BO_4973:Ter; PspaS:Prom; BO_4227:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; BO_2939:Prom; BO_3583:Op; BO_28333:RBS; SpaR:CDS; BO_4775:Ter; PspaS:Prom; BO_3789:Op; BO_28475:RBS; SpaK:CDS; BO_28017:RBS; BO_32147:CDS; BO_5498:Ter; BO_3143:Prom; BO_28493:RBS; BO_28950:CDS; BO_28224:RBS; BO_32997:CDS; BO_6476:Ter; BO_27718:Prom; BO_28220:RBS; BO_31762:CDS; BO_28275:RBS; BO_31001:CDS; BO_4813:Ter; BO_3403:Prom; BO_28182:RBS; BO_32601:CDS; BO_28486:RBS; BO_32601:CDS; BO_28234:RBS; BO_32227:CDS; BO_28270:RBS; BO_30746:CDS; BO_5738:Ter; PspaS:Prom; BO_3600:Op; BO_3906:Op; BO_28092:RBS; BO_32307:CDS; BO_28142:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_28317:RBS; BO_32731:CDS; BO_28152:RBS; BO_28950:CDS; BO_5427:Ter; BO_27782:Prom; BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; SpaK:CDS; BO_28518:RBS; BO_30680:CDS; BO_5205:Ter; BO_27780:Prom; BO_3596:Op; BO_27809:RBS; BO_28831:CDS; BO_28322:RBS; SpaR:CDS; BO_5139:Ter; PspaS:Prom; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27809:RBS; BO_28831:CDS; BO_27910:RBS; BO_31762:CDS; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_5427:Ter; BO_2937:Prom; BO_28152:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_3906:Op; BO_28260:RBS; BO_31762:CDS; BO_5483:Ter; PspaS:Prom; BO_3559:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28144:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_28157:RBS; BO_28950:CDS; BO_28182:RBS; BO_32601:CDS; BO_5427:Ter; BO_3388:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28185:RBS; BO_32743:CDS; BO_4296:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_28082:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; BO_28152:RBS; BO_28950:CDS; BO_27836:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_4190:Op; BO_3600:Op; BO_3906:Op; BO_27847:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_27903:RBS; BO_32307:CDS; BO_28096:RBS; BO_28950:CDS; BO_28051:RBS; BO_32731:CDS; BO_28391:RBS; BO_30753:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28134:RBS; BO_32307:CDS; BO_28326:RBS; BO_28892:CDS; BO_5427:Ter; BO_27782:Prom; BO_3711:Op; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_28326:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_3606:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28144:RBS; BO_32307:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_27782:Prom; BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; SpaK:CDS; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter; BO_3387:Prom; BO_28096:RBS; BO_32997:CDS; BO_4583:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28144:RBS; BO_32307:CDS; BO_4296:Ter; BO_27629:Prom; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter");
        // models.put("experiment_131129_130740", "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_27925:RBS; BO_31152:CDS; BO_4296:Ter; BO_27654:Prom; BO_4062:Op; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_3017:Prom; BO_27814:RBS; SpaR:CDS; BO_28246:RBS; BO_32147:CDS; BO_27875:RBS; BO_32147:CDS; BO_28522:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_5248:Ter; BO_3475:Prom; BO_28458:RBS; BO_28831:CDS; BO_6486:Ter; BO_3403:Prom; BO_27793:RBS; SpaK:CDS; BO_5388:Ter; BO_3403:Prom; BO_28140:RBS; BO_32147:CDS; BO_5418:Ter");
    }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { }
    
    @After
    public void tearDown() { }

    @Test
    public void testCompilationDirector() {

        for(String key : models.keySet()) {
                CompilationDirector director = new CompilationDirector(REPOSITORY_URL);
                director.setModelSvpWrite(models.get(key));
                String model = director.getSBMLString(key);
                SimpleCopasiSimulator simulator = new SimpleCopasiSimulator(12800, 640);
                simulator.setModel(model);
                writeSbml(key, model);
                if(!simulator.run()) {
                    Assert.fail("Could not simulate " + key);
                }
        }
    }

    private void writeSbml(String modelName, String sbmlContent) {

        File file = new File("compilertests/" + modelName + ".xml");

        try {
            if (!file.exists()) {
                if(!file.createNewFile()) {
                    LOGGER.error("Could not create file: {}", file.getName());
                }
            }

            FileWriter f = new FileWriter(file);
            f.write(sbmlContent);
            f.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
