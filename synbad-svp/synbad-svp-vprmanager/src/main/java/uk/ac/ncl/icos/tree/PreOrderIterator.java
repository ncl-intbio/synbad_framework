/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.tree;

import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;

/**
 *
 * @author owengilfellon
 */
public class PreOrderIterator<T> implements Iterator {
    
    private Tree<T> tree;
    private Node<T> currentNode = null;
    private Set<Node<T>> explored = new HashSet<Node<T>>();
    private int depth = 0;
    
    public PreOrderIterator(Tree<T> tree) {
        this.tree = tree;
    }
    
    public int getDepth()
    {
        return depth;
    }

    @Override
    public boolean hasNext() {
        
        if(currentNode == null && tree.getRootNode() != null){
            return true;
        }
        
        if(currentNode!=null)
        {
            if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                return true;
            }
            
            if(currentNode.hasSiblings())
            {
                if(!explored.containsAll(currentNode.getParent().getChildren())){
                    return true;
                }                   
            }

            Node<T> tempNode = currentNode;
            
            while(tempNode.getParent()!=null){
                
                tempNode = tempNode.getParent();
                
                if(!explored.containsAll(tempNode.getChildren())) {
                    return true;
                }
            }
        }
        
        return false;
        
    }

    @Override
    public Node<T> next() {
        
        if(currentNode == null && tree.getRootNode() != null){
            currentNode = tree.getRootNode();
            explored.add(currentNode);
            depth++;
            return currentNode;
        }
        
        if(currentNode!=null)
        {
            if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                ListIterator<Node<T>> it = currentNode.getChildren().listIterator();
                while(it.hasNext())
                {
                    Node<T> n = it.next();
                    
                    if(!explored.contains(n))
                    {
                        currentNode = n;
                        explored.add(currentNode);
                        depth++;
                        return currentNode;
                    }
                }
            }

            if(currentNode.hasSiblings())
            {
                if(!explored.containsAll(currentNode.getParent().getChildren())){
                    ListIterator<Node<T>> it = currentNode.getParent().getChildren().listIterator();
                    while(it.hasNext())
                    {
                        Node<T> n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            explored.add(currentNode);
                            return currentNode;
                        }
                    }
                }                   
            }
            
            Node<T> tempNode = currentNode;
            
            while(tempNode.getParent()!=null){
                
                tempNode = tempNode.getParent();
                depth--;
                
                if(!explored.containsAll(tempNode.getChildren())) {
                    ListIterator<Node<T>> it = tempNode.getChildren().listIterator();
                    while(it.hasNext())
                    {
                        Node<T> n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            explored.add(currentNode);
                            return currentNode;
                        }
                    }
                }
            }
        }
        
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
