package uk.ac.ncl.icos.tree;

/**
 * Created by owengilfellon on 22/05/2014.
 */
public class Tree<T> {

    public Node<T> rootNode;

    public Tree() { super(); }

    public Node<T> getRootNode()
    {
        return rootNode;
    }

    public void setRootNode(Node<T> rootNode)
    {
        this.rootNode = rootNode;
    }

    public PreOrderIterator<T> getPreOrderIterator() { return new PreOrderIterator<T>(this); }

}
