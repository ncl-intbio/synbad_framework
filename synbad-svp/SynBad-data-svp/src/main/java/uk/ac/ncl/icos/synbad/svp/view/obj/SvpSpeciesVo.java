/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SvpSpeciesVo extends DefaultSBViewIdentified<SBIdentified> implements SBViewComponent<SBIdentified, SvpViewPort>, SvVo {
    
    private final Svp svp;
    
    public SvpSpeciesVo(FunctionalComponent instance, SvpPView view) {
        super(instance, view);
        this.svp = getSvpFromWs();
    }

    public SvpSpeciesVo(FunctionalComponent instance, long id, SBView view) {
        super(instance, id, view);
        this.svp = getSvpFromWs();
    }

    @Override
    public SvpPView getView() {
        return (SvpPView) super.getView(); //To change body of generated methods, choose Tools | Templates.
    }

    public Svp getSvp() {
        return svp;
    }

    @Override
    public String getDescription() {
        return svp.getDescription();
    }

    @Override
    public URI getWasDerivedFrom() {
        return svp.getWasDerivedFrom();
    }
    
    public ComponentRole getPartTypeAsRole() {
        return svp.getPartTypeAsRole();
    }
    
    @Override
    public String getPrefix() {
        return svp.getPrefix();
    }

    @Override
    public String getDisplayId() {
        return svp.getDisplayId();
    }

    @Override
    public String getVersion() {
        return svp.getVersion();
    }

    @Override
    public String getName() {
        return svp.getName();
    }

    @Override
    public URI getPersistentId() {
        return svp.getPersistentId();
    }

    @Override
    public Set<SvpViewPort> getPorts() {
        return getView().getPorts(this);
    }
    
    @Override
    public Set<SvpViewPort> getPorts(SBPortDirection... directions) {
        Collection<SBPortDirection> portDirections = Arrays.asList(directions);
        return getView().getPorts(this).stream()
                .filter(p -> portDirections.isEmpty() || portDirections.contains(p.getDirection()))
                .collect(Collectors.toSet());
    }
    
    @Override
    public SvmVo getParent() {
        return getView().getParent(this);
    }
  
    private Svp getSvpFromWs() {

        if(!getObject().is(FunctionalComponent.TYPE))
            return null;

        Svp definition;
        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(getWorkspace(), getContexts())) {
            definition = graph.getTraversal()
                    .v(getObject())
                    .e(SBDirection.OUT, SynBadTerms.Sbol.definedBy)
                    .v(SBDirection.OUT, Svp.class)
                    .getResult().streamVertexes()
                    .filter(o -> o.is(Svp.TYPE))
                    .map(o -> (Svp)o).findFirst().orElse(null);
        }

        return definition;
    }

}
