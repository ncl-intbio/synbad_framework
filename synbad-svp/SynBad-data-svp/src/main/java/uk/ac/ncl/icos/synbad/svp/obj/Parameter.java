/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.SynBadAction;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Parameter.TYPE)
public class Parameter extends ASBIdentified {
    
    public static final String TYPE = "http://virtualparts.org/Parameter";
    private static final URI PARAMETER_TYPE = SBIdentityHelper.getURI(Parameter.TYPE);

    public Parameter(SBIdentity identity, SBWorkspace workspaceId, SBValueProvider valueProvider) {
        super(identity, workspaceId, valueProvider);
    }

    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    public String getParameterType() {
        return getValue(SynBadTerms.VPR.parameterType).asString();    
    }
    
//    public void setParameterType(String type) {
//        setValue(SynBadTerms.VPR.parameterType, SBValue.parseValue(type));
//    }
    
    public Double getParameterValue() {
        SBValue v = getValue(SynBadTerms.VPR.parameterValue);    
        return v == null ? null : v.asDouble();
    }
    
//    public void setParameterValue(double value) {
//        setValue(SynBadTerms.VPR.parameterValue, SBValue.parseValue(value));
//    }
    
    public String getParameterEvidenceCode() {
        return getValue(SynBadTerms.VPR.parameterEvidence).asString();
    }
    
//    public void setParameterEvidenceCode(String evidenceCode) {
//        setValue(SynBadTerms.VPR.parameterEvidence, SBValue.parseValue(evidenceCode));
//    } 

    public String getParameterScope() {
        return getValue(SynBadTerms.VPR.parameterScope).asString();    
    }
    
//    public void setParameterScope(String scope) {
//        setValue(SynBadTerms.VPR.parameterScope, SBValue.parseValue(scope));
//    }
    
    @SynBadAction(type = SBGraphEventType.ADDED, clazz = Parameter.class)
    public static SBAction CreateParameter(SBIdentity identity, URI parentIdentity, String name, String type, Double value, String scope, String evidenceType, SBWorkspace workspace, URI[] contexts) {

        DefaultSBDomainBuilder b =  new DefaultSBDomainBuilder(workspace,  contexts)
            .createObject(identity.getIdentity(), SBIdentityHelper.getURI(Parameter.TYPE), parentIdentity, SBIdentityHelper.getURI(SvpInteraction.TYPE))
            .createEdge(parentIdentity, SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.hasParameter),  identity.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasName), SBValue.parseValue(name), PARAMETER_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.parameterType),  SBValue.parseValue(type), PARAMETER_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.parameterEvidence),  SBValue.parseValue(evidenceType), PARAMETER_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.parameterScope),  SBValue.parseValue(scope), PARAMETER_TYPE);
        
         if(value != null)
            b.createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.parameterValue),  SBValue.parseValue(value), PARAMETER_TYPE);
        
        return b.build();
    }


}
