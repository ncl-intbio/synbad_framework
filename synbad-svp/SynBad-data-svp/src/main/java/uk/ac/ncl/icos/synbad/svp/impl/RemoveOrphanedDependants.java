/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.rewrite.SBRewriterRuleWithObj;

/**
 *
 * @author owengilfellon
 */
public class RemoveOrphanedDependants  implements SBRewriterRuleWithObj<SBIdentified, SynBadEdge, SBWorkspace> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveOrphanedDependants.class);
    private SBWorkspaceGraph g = null;

    @Override
    public void apply(SBWorkspace g) { 
    }

    @Override
    public SBAction apply(SBWorkspace g, SBIdentified object) {
        return null;
    }


    @Override
    public SBAction apply(SBWorkspace g, SBEvent e) {
        /*
        if(e.getType() != SBGraphEventType.REMOVED && e.getType() != SBGraphEventType.REMOVED)
            return null;
        
        if(!SBGraphEvent.class.isAssignableFrom(e.getClass()))
            return null;
        
        SBGraphEvent dne = (SBGraphEvent)e;
        URI identity = (URI)dne.getData();
        
        List<SBAction> toRemove = g.getRdfService().getStatements(null, SynBadTerms.SynBadSystem.dependantOn, SBValue.parseValue(identity),g.getSystemContext()).stream()
                .map(s -> s.getSubject()).map(s -> URI.create(s))
                .filter(id -> g.containsObject(id, SBIdentified.class,g.getContextIds()))
                .map(id -> g.getObject(id, SBIdentified.class, g.getContextIds()))
                .map(obj -> {  
                    URI parent = g.getRdfService()
                        .getStatements(obj.getIdentity().toASCIIString(), SynBadTerms.SynBadSystem.owns, null, g.getSystemContext())
                        .stream().map(s -> s.getObject()).map(v -> v.asURI()).findFirst().orElse(null);
                    LOGGER.debug("Removing {}", obj.getDisplayId());
                    return parent != null ? new RemoveObjectAndDependantsAction<>(obj.getIdentity(),parent, g.getIdentity(), g.getContextIds()) : null;
                })
                .filter(a -> a != null).collect(Collectors.toList());
        
       return new SBActionMacro(toRemove);
*/ return null;
    }

    @Override
    public boolean shouldApply(SBWorkspace g, SBEvent e) {
        return false;
    }

    @Override
    public boolean shouldApply(SBIdentified object) {
        return false;
    }
    
    
    
    

    @Override
    public Class<SBIdentified> getRuleClass() {
        return SBIdentified.class;
    }

}
