/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;

/**
 *
 * @author owen
 */
public enum SvpModuleType implements SBDataDef {
    
    SOURCE(URI.create("http://www.synbad.org/SvpModuleType/Source")),
    MODULATOR(URI.create("http://www.synbad.org/SvpModuleType/Modulator")),
    SINK(URI.create("http://www.synbad.org/SvpModuleType/Sink"));
    
    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private SvpModuleType(URI uri) {
        this.uri = uri;
    }  
    
}
