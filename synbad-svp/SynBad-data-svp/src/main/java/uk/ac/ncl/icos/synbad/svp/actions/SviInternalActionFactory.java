/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import java.net.URI;
import java.util.Collections;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SviInternalActionFactory {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(SviInternalActionFactory.class);
    private final static URI SVI_TYPE = SBIdentityHelper.getURI(SvpInteraction.TYPE);
    
    // ===================================================
    //              Internal Interactions
    // ===================================================
    
    public static SBAction  CreatePoPSProductionAction (SBIdentity identity, URI parentId, double ktr, SBWorkspace ws, URI[] contexts) {  
       
        SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.PoPSProduction))
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "kf"),
                    identity.getIdentity(), "ktr", "ktr", ktr, "", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "PoPS"),
                    identity.getIdentity(), "PoPSOutput", "PoPS", null, "Output", "")
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())

           // .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue("PoPS Production"), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), SVI_TYPE)

            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)  
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue("-> PoPS"), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("PoPSProduction"), SVI_TYPE)

            .build();

    }
    
    
    public static SBAction CreatePoPSModulationAction(SBIdentity identity, URI parentId, SBWorkspace ws, URI[] contexts) {

            SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);

            SBIdentity parPopsInId = SvpIdentityHelper.getParameterIdentity(identity, "PoPSInput");
            SBIdentity parPopsOutId = SvpIdentityHelper.getParameterIdentity(identity, "PoPSOutput");
            
            String math = "";
            return  new SvpActionBuilderImpl(ws, contexts)
                    .createSvi(identity, Collections.singleton(InteractionType.OperatorPoPSModulation))

                    .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
                    .createParameter(parPopsInId, identity.getIdentity(), "Input", "PoPS", null, "Input", "")
                    .createParameter(parPopsOutId, identity.getIdentity(), "Output", "PoPS", null, "Output", "")
                    
                    .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
           
                    
                   // .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue("OperatorPoPSModulation"), SVI_TYPE)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), SVI_TYPE)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue(true), SVI_TYPE)
                    
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("OperatorPoPSModulation"), SVI_TYPE)
                    .build();
       
    }

    public static SBAction CreateShimRiPSModulationAction(SBIdentity identity, URI parentId, double efficiency, SBWorkspace ws, URI[] contexts) {
 
       

            SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);

            SBIdentity parRipsInId = SvpIdentityHelper.getParameterIdentity(identity, "RiPSInput");
            SBIdentity parRipsOutId = SvpIdentityHelper.getParameterIdentity(identity, "RiPSOutput");
            SBIdentity parRipsEffId = SvpIdentityHelper.getParameterIdentity(identity, "RiPSEfficiency");
            
            String math = "RiPS -> RiPS";
            
            return new SvpActionBuilderImpl(ws, contexts)
                    .createSvi(identity, Collections.singleton(InteractionType.RiPSModulation))

                    .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
                    .createParameter(parRipsInId, identity.getIdentity(), "RiPSInput", "RiPS", null, "Input", "")
                    .createParameter(parRipsOutId, identity.getIdentity(), "RiPSOutput", "RiPS", null, "Output", "")
                    .createParameter(parRipsEffId, identity.getIdentity(), "RiPSEfficiency", "RiPSEfficiency", efficiency, "", "")
                    
                    .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
           
                    
                  //  .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue("RiPS Modulation"), SVI_TYPE)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), SVI_TYPE)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue(true), SVI_TYPE)
                    
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
                    .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("RiPSModulation"), SVI_TYPE)
                    .build();

    }
    
     public static SBAction CreateShimMrnaModulationAction(SBIdentity identity, URI parentId, double efficiency, SBWorkspace ws, URI[] contexts) {

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);
        SBIdentity parRipsInId = SvpIdentityHelper.getParameterIdentity(identity, "mRNAInput");
        SBIdentity parRipsOutId = SvpIdentityHelper.getParameterIdentity(identity, "mRNAOutput");
        SBIdentity parRipsEffId = SvpIdentityHelper.getParameterIdentity(identity, "RiPSEfficiency");
        String math = "mRNA -> mRNA";

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.mRNAModulation))
            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
            .createParameter(parRipsInId, identity.getIdentity(), "mRNAInput", "mRNA", null, "Input", "")
            .createParameter(parRipsOutId, identity.getIdentity(), "mRNAOutput", "mRNA", null, "Output", "")
            .createParameter(parRipsEffId, identity.getIdentity(), "RiPSEfficiency", "RiPSEfficiency", efficiency, "", "")
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue(true), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("mRNAModulation"), SVI_TYPE)
            .build();
    }
    
    public static SBAction CreateRiPSProductionAction (SBIdentity identity, URI parentId,double ktl, double volume, SBWorkspace ws, URI[] contexts) {

         SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.RiPSProduction))

            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "ktl"),
                    identity.getIdentity(), "ktl", "ktl", ktl, "", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "volume"),
                    identity.getIdentity(), "volume", "volume", volume, "Global", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "mrna"),
                    identity.getIdentity(), "mRNAInput", "mRNA", null, "Input", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "rips"),
                    identity.getIdentity(), "RiPSOutput", "RiPS", null, "Output", "")

            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())

            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false),SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)  
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue("-> RiPS"), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("RiPSProduction"), SVI_TYPE)

            .build();
    }
    
    public static SBAction  CreateProteinProductionAction(SBIdentity identity, URI svp, SBWorkspace ws, URI[] contexts) {

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(svp);

        SBIdentity proteinId = SvpIdentityHelper.getProteinIdentity(svpId);

        SBIdentity par_dna = Participation.getParticipationIdentity(identity, svpId);
        SBIdentity par_protein = Participation.getParticipationIdentity(identity, proteinId);

        String math = "-> " + svpId.getDisplayID();
        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.ProteinProduction))
            .addAction(ComponentDefinition.createComponentDefinition(proteinId.getIdentity(), Collections.singleton(ComponentType.Protein), Collections.emptyList(), ws, contexts))
            .addAction(new CreateEdge(proteinId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, svp,  ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "volume"), identity.getIdentity(), "volume",  "volume", 1.0, "Global", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "RiPSInput"), identity.getIdentity(), "RiPSInput",  "RiPS", null, "Input", "")
            .createEdge(svp, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
            .createParticipation(par_dna, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setInteractionRole(SbolInteractionRole.TEMPLATE)
                .setNameInMath("DNA")
                .setSvp(svpId)
                .setSbolComponentId(svpId.getIdentity()).builder()
            .createParticipation(par_protein, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setInteractionRole(SbolInteractionRole.PRODUCT)
                .setNameInMath("Species")
                .setSvp(svpId)
                .setSbolComponentId(proteinId.getIdentity()).builder()
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)                    
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("ProteinProduction"), SVI_TYPE)
            .build();

    }
    
    public static SBAction CreateDegradationAction(SBIdentity identity,URI parentId, ComponentType type, SynBadPortState state, double kd, SBWorkspace ws, URI[] contexts) {

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);
        SBIdentity componentId =  SvpIdentityHelper.getComponentIdentity(svpId, type, state);

        SBIdentity participantId = Participation.getParticipationIdentity(identity, componentId);            
        SBIdentity port_in_protein = SvpIdentityHelper.getPortIdentity(identity, componentId, SBPortDirection.IN, getType(type), SynBadPortState.Default);

        String math = state == SynBadPortState.Phosphorylated ? "Phosphorylated_" : "";
        math = math + svpId.getDisplayID() + (type == ComponentType.Complex ? "->" : " ->");

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.Degradation))
            .createParameter(SvpIdentityHelper.getParameterIdentity(svpId, "kd"), identity.getIdentity(), "kd", "kd", kd, "", "")
            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
            .createParticipation(participantId, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setMolecularForm(state)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Species")
                .setSvp(svpId)
                .setSbolComponentId(componentId.getIdentity()).builder()

            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)  
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("Degradation"), SVI_TYPE)

            .build();
    }
    
    
    
    public static SBAction CreateMrnaProductionAction(SBIdentity identity, URI parentId, SBWorkspace ws, URI[] contexts) {        

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);
        //SBIdentity mrnaId = SvpIdentityHelper.getMrnaIdentity(svpId);

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.mRNAProduction))

            //.addAction(ComponentDefinition.createComponentDefinition(mrnaId.getIdentity(), Collections.singleton(ComponentType.mRNA), Collections.emptyList(), workspace, contexts))
            .addAction(new CreateEdge(parentId, SynBadTerms.SynBadPart.hasInternalInteraction, identity.getIdentity(),  ws.getIdentity(), contexts))

            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), identity.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)  
            .build();
    }


    public static SBAction CreateComplexDisassociationAction(SBIdentity parentId, SBIdentity interactionIdentity, SBIdentity complexId, SBIdentity participant1, ComponentType type1, SynBadPortState state1,
                                                             SBIdentity participant2, ComponentType type2, SynBadPortState state2, double kb, SBWorkspace ws, URI[] contexts) {

        URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);

       SBIdentity complexDefId = SvpIdentityHelper.getComplexIdentity(complexId);

        SBIdentity participant1Component = SvpIdentityHelper.getComponentIdentity(participant1, type1, state1);
        SBIdentity participant2Component = SvpIdentityHelper.getComponentIdentity(participant2, type2, state2);

        SBIdentity parParticipant1 = Participation.getParticipationIdentity(interactionIdentity, participant1);
        SBIdentity parParticipant2 = Participation.getParticipationIdentity(interactionIdentity, participant1);
        SBIdentity parComplex = Participation.getParticipationIdentity(interactionIdentity, complexId);

        SBIdentity kbParameterId = SvpIdentityHelper.getParameterIdentity(interactionIdentity, "kb");

        SBIdentity sviParticipant1Port =  SvpIdentityHelper.getPortIdentity(interactionIdentity, participant1Component, SBPortDirection.OUT, getType(type1), state1);
        SBIdentity sviParticipant2Port =  SvpIdentityHelper.getPortIdentity(interactionIdentity, participant2Component, SBPortDirection.OUT, getType(type2), state2);
        SBIdentity sviComplexPort =  SvpIdentityHelper.getPortIdentity(interactionIdentity,complexId, SBPortDirection.IN, SynBadPortType.Complex, SynBadPortState.Default);

        String math =  complexId.getDisplayID() + " -> ";
        math = state1 == SynBadPortState.Phosphorylated ? math + "Phosphorylated_" : math;
        math = math + participant1.getDisplayID() + " + ";
        math = state2 == SynBadPortState.Phosphorylated ? math + "Phosphorylated_" : math;
        math = math + participant2.getDisplayID();

        LOGGER.debug("Creating new Complex Disassociation action: {}", interactionIdentity.getDisplayID());

        return new SvpActionBuilderImpl(ws, contexts)
                .createSvi(interactionIdentity, Collections.singleton(InteractionType.ComplexFormation))
                .createPort(sviParticipant1Port, SBPortDirection.OUT, getType(type1), state1,
                        participant1Component.getIdentity(), interactionIdentity.getIdentity(), objType)
                .createPort(sviParticipant2Port, SBPortDirection.OUT, getType(type2), state2,
                        participant2Component.getIdentity(), interactionIdentity.getIdentity(), objType)
                .createPort(sviComplexPort, SBPortDirection.IN, SynBadPortType.Complex, SynBadPortState.Default,
                        complexId.getIdentity(), interactionIdentity.getIdentity(), objType)
                .createEdge(participant1.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), interactionIdentity.getIdentity())
                .createEdge(participant2.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), interactionIdentity.getIdentity())
                .createEdge(complexId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), interactionIdentity.getIdentity())
                .createParticipation(parComplex, interactionIdentity.getIdentity())
                    .setDirection(ParticipationRole.Input)
                    .setInteractionRole(SbolInteractionRole.REACTANT)
                    .setNameInMath("SpeciesComplex")
                    .setSvp(complexId)
                    .setSbolComponentId(complexDefId.getIdentity()).builder()
                .createParticipation(parParticipant1, interactionIdentity.getIdentity())
                    .setDirection(ParticipationRole.Output)
                    .setMolecularForm(state1)
                    .setInteractionRole(SbolInteractionRole.PRODUCT)
                    .setNameInMath("Species1")
                    .setSvp(participant1)
                    .setSbolComponentId(participant1Component.getIdentity()).builder()
                .createParticipation(parParticipant2, interactionIdentity.getIdentity())
                    .setDirection(ParticipationRole.Output)
                    .setMolecularForm(state2)
                    .setInteractionRole(SbolInteractionRole.PRODUCT)
                    .setNameInMath("Species2")
                    .setSvp(participant2)
                    .setSbolComponentId(participant2Component.getIdentity()).builder()
                .createParameter(kbParameterId, interactionIdentity.getIdentity(), "kb", "kb", kb, "Local", "")
                .createEdge(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), complexId.getIdentity())
                .createEdge(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant1.getIdentity())
                .createEdge(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant2.getIdentity())


                .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.ComplexDisassociation.getUri()), objType)
                .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), objType)
                .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)
                .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
                .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("ComplexDisassociationWithTwoSpecies"), objType)
                .createEdge(parentId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), interactionIdentity.getIdentity()).build();

    }


    public static SBAction CreateSmallMoleculeComplexationAction(SBIdentity parentId, SBIdentity interactionIdentity, SBIdentity complexId, SBIdentity smallMolecule, ComponentType type1, SynBadPortState state1,
                SBIdentity participant2, ComponentType type2, SynBadPortState state2, double kf, double kd, SBWorkspace ws, URI[] contexts) {

            URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);

            SBIdentity complexDefId = SvpIdentityHelper.getComplexIdentity(complexId);
//            SBIdentity smlMolDefId = SvpIdentityHelper.getSmallMolculeIdentity(smallMolecule);

            SBIdentity participant1Component = SvpIdentityHelper.getComponentIdentity(smallMolecule, type1, state1);
            SBIdentity participant2Component = SvpIdentityHelper.getComponentIdentity(participant2, type2, state2);

            SBIdentity parParticipant1 = Participation.getParticipationIdentity(interactionIdentity, smallMolecule);
            SBIdentity parParticipant2 = Participation.getParticipationIdentity(interactionIdentity, participant2);
            SBIdentity parComplex = Participation.getParticipationIdentity(interactionIdentity, complexId);

            SBIdentity kfParameterId = SvpIdentityHelper.getParameterIdentity(interactionIdentity, "kf");

            SBIdentity sviParticipant1Port =  SvpIdentityHelper.getPortIdentity(interactionIdentity, participant1Component, SBPortDirection.IN, getType(type1), state1);
            SBIdentity sviParticipant2Port =  SvpIdentityHelper.getPortIdentity(interactionIdentity, participant2Component, SBPortDirection.IN, getType(type2), state2);
            SBIdentity sviComplexPort =  SvpIdentityHelper.getPortIdentity(interactionIdentity, complexId, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);

            String math = state1 == SynBadPortState.Phosphorylated ? "Phosphorylated_" : "";
            math = math + smallMolecule.getDisplayID() + " + ";
            math = state2 == SynBadPortState.Phosphorylated ? math + "Phosphorylated_" : math;
            math = math + participant2.getDisplayID() + " -> " + complexId.getDisplayID();


            LOGGER.debug("Creating new Complex Formation action: {}", interactionIdentity.getDisplayID());

            return new SvpActionBuilderImpl(ws, contexts)

                    .createComplex(complexId, kd)
                    .createSvp(smallMolecule, Collections.emptySet(), ComponentType.SmallMolecule)

                 //   .createDegradation(SvpIdentityHelper.getDegradationIdentity(complexId, SynBadPortState.Default), complexId.getIdentity(), ComponentType.Complex, SynBadPortState.Default, kd)
                    .createSvi(interactionIdentity, Collections.singleton(InteractionType.ComplexFormation))
                    .createPort(sviParticipant1Port, SBPortDirection.IN, getType(type1), state1,
                            participant1Component.getIdentity(), interactionIdentity.getIdentity(), objType)
                    .createPort(sviParticipant2Port, SBPortDirection.IN, getType(type2), state2,
                            participant2Component.getIdentity(), interactionIdentity.getIdentity(), objType)
                    .createPort(sviComplexPort, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default,
                            complexId.getIdentity(), interactionIdentity.getIdentity(), objType)

                    .createEdge(complexId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), interactionIdentity.getIdentity())
                    .createEdge(smallMolecule.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), interactionIdentity.getIdentity())
                    .createEdge(participant2.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), interactionIdentity.getIdentity())
                    .createParticipation(parParticipant1, interactionIdentity.getIdentity())
                        .setDirection(ParticipationRole.Input)
                        .setMolecularForm(state1)
                        .setInteractionRole(SbolInteractionRole.REACTANT)
                        .setNameInMath("Species1")
                        .setSvp(smallMolecule)
                        .setSbolComponentId(participant1Component.getIdentity()).builder()
                    .createParticipation(parParticipant2, interactionIdentity.getIdentity())
                        .setDirection(ParticipationRole.Input)
                        .setMolecularForm(state2)
                        .setInteractionRole(SbolInteractionRole.REACTANT)
                        .setNameInMath("Species2")
                        .setSvp(participant2)
                        .setSbolComponentId(participant2Component.getIdentity()).builder()
                    .createParticipation(parComplex, interactionIdentity.getIdentity())
                        .setDirection(ParticipationRole.Output)
                        .setInteractionRole(SbolInteractionRole.PRODUCT)
                        .setNameInMath("SpeciesComplex")
                        .setSvp(complexId)
                        .setSbolComponentId(complexDefId.getIdentity()).builder()
                        .createParameter(kfParameterId, interactionIdentity.getIdentity(), "kf", "kf", kf, "Local", "")
                    .createEdge(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), complexId.getIdentity())
                    .createEdge(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), smallMolecule.getIdentity())
                    .createEdge(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant2.getIdentity())
                    .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)
                    .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.ComplexFormation.getUri()), objType)
                    .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), objType)
                    .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
                    .createValue(interactionIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("ComplexFormationWithTwoSpecies"), objType)
                    .createEdge(parentId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), interactionIdentity.getIdentity()).build();

    }
    

    public static SBAction CreateSmallMoleculePhosphorylationAction(SBIdentity identity,URI parentId, URI smallMolecule, double kf, SBWorkspace ws, URI[] contexts) {

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);
        SBIdentity smlMolId = ws.getIdentityFactory().getIdentity(smallMolecule);

        SBIdentity proteinId = SvpIdentityHelper.getProteinIdentity(svpId);
        SBIdentity proteinPId = SvpIdentityHelper.getPhosphorylatedProteinIdentity(svpId);
        SBIdentity smlMolCdId = SvpIdentityHelper.getSmallMolculeIdentity(smlMolId);

        SBIdentity participantId1 = Participation.getParticipationIdentity(identity, smlMolId);
        SBIdentity participantId2 = Participation.getParticipationIdentity(identity, proteinId);
        SBIdentity participantId3 = Participation.getParticipationIdentity(identity, proteinPId);

        SBIdentity parameterId = SvpIdentityHelper.getParameterIdentity(identity, "kf");

        String math = svpId.getDisplayID() + " + " + smlMolId.getDisplayID() + " -> " + svpId.getDisplayID() + "~P + " + smlMolId.getDisplayID();

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvp(smlMolId, Collections.emptySet(), ComponentType.SmallMolecule)
            .createSvi(identity, Collections.singleton(InteractionType.Phosphorylation))

            .addAction(ComponentDefinition.createComponentDefinition(smlMolCdId.getIdentity(), Collections.singleton(ComponentType.SmallMolecule), Collections.emptyList(), ws, contexts))
            .addAction(ComponentDefinition.createComponentDefinition(proteinId.getIdentity(), Collections.singleton(ComponentType.Protein), Collections.emptySet(), ws, contexts))
            .addAction(ComponentDefinition.createComponentDefinition(proteinPId.getIdentity(), Collections.singleton(ComponentType.Protein), Collections.emptySet(), ws, contexts))

            .createParameter(parameterId, identity.getIdentity(), "kf", "kf", kf, "Local", "")

            .createParticipation(participantId1, identity.getIdentity())
                .setDirection(ParticipationRole.Modifier)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("EnvironmentConstant")
                .setSvp(smlMolId)
                .setSbolComponentId(smlMolCdId.getIdentity()).builder()
            .createParticipation(participantId2, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Protein")
                .setSvp(svpId)
                .setSbolComponentId(proteinId.getIdentity()).builder()
            .createParticipation(participantId3, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setMolecularForm(SynBadPortState.Phosphorylated)
                .setInteractionRole(SbolInteractionRole.PRODUCT)
                .setNameInMath("Protein_P")
                .setSvp(svpId)
                .setSbolComponentId(proteinPId.getIdentity()).builder()

            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), smlMolId.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true),  SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)  
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("PhosphorylationWithModifier"), SVI_TYPE)

            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
            .addAction(new CreateEdge(smlMolId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, parentId,  ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .addAction(new CreateEdge(proteinId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, parentId,  ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .addAction(new CreateEdge(proteinPId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, parentId,  ws.getIdentity(), ws.getSystemContextIds(contexts))).build();
  
    }
    
    public static SBAction CreateDephosphorylationAction(SBIdentity identity, URI parentId, double kdeP, SBWorkspace ws, URI[] contexts) {

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(parentId);

        SBIdentity protein_p_Id = SvpIdentityHelper.getPhosphorylatedProteinIdentity(svpId);
        SBIdentity protein_Id = SvpIdentityHelper.getProteinIdentity(svpId);

        SBIdentity participantId1 = Participation.getParticipationIdentity(identity, protein_p_Id);
        SBIdentity participantId2 = Participation.getParticipationIdentity(identity, protein_Id);

        String math = svpId.getDisplayID() + "~P -> " + svpId.getDisplayID();

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.Dephosphorylation))
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "kdep"),
                    identity.getIdentity(), "kdeP", "kdeP", kdeP, "", "")
            .createParticipation(participantId1, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setMolecularForm( SynBadPortState.Phosphorylated)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("SpeciesPhosphorylated")
                .setSvp(svpId)
                .setSbolComponentId(protein_p_Id.getIdentity()).builder()
            .createParticipation(participantId2, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setInteractionRole(SbolInteractionRole.PRODUCT)
                .setNameInMath("Species")
                .setSvp(svpId)
                .setSbolComponentId(protein_Id.getIdentity()).builder()
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue("true"), SVI_TYPE)  
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("Dephosphorylation"), SVI_TYPE)
            .createEdge(parentId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInternalInteraction), identity.getIdentity())
            .build();
    }
    
    private static SynBadPortType getType(ComponentType type) {
        if(type == ComponentType.Complex)
            return SynBadPortType.Complex;
        if(type == ComponentType.Protein)
            return SynBadPortType.Protein;
        if(type == ComponentType.SmallMolecule)
            return SynBadPortType.SmallMolecule;
        
        return null;
    }
}
