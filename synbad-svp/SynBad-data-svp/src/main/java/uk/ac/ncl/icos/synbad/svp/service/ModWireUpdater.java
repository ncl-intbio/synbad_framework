/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.api.SBSvpUtil;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;


public class ModWireUpdater extends AWireUpdater  {

    private final static Logger LOGGER = LoggerFactory.getLogger(ModWireUpdater.class);

    public ModWireUpdater(SBIdentityFactory f) {
        super(f);
    }

    public void applyToContext(SBWireContext ctx) {

        SBPortInstance pi = ctx.getPortInstance().get();

        ctx.getPorts().values().stream()
                .filter((SBPortInstance p) -> p.getPortDirection() != pi.getPortDirection())
                .forEach((port) -> {

                SBPortInstance[] ports = sortOutInPorts(pi, port);

                if (isTransWiring(ports[0], ports[1])) {
                    createWire(ports[0], ports[1], ctx);
                }
            }
        );
    }

    private boolean isTransWiring(SBPortInstance p1, SBPortInstance p2) {

        if(p1.getPortDirection() != SBPortDirection.OUT || p2.getPortDirection() != SBPortDirection.IN)
            return false;

        SBIdentified p1DefOwner = p1.getDefinition().getOwner();
        SBIdentified p2DefOwner = p2.getDefinition().getOwner();
        
        if(p1DefOwner.getIdentity().equals(p2DefOwner.getIdentity()))
            return false;

        // Must include an interaction

        if (!p1DefOwner.is(SvpInteraction.TYPE) && !p2DefOwner.is(SvpInteraction.TYPE))
            return false;

         // Must not be between interactions

        if(p1DefOwner.is(SvpInteraction.TYPE) && p2DefOwner.is(SvpInteraction.TYPE))
            return false;

        if(!p1.isConnectable(p2))
            return false;

        // If is output from interaction, must be to sequence components that follow the original signal source

        if (p1DefOwner.is(SvpInteraction.TYPE) && (p2DefOwner.is(Svp.TYPE) || p2DefOwner.is(SvpModule.TYPE)) ) {

            SBSvpSequenceObject toInstance = p2.getOwner();
            Optional<SvpModule> toInstanceParent = toInstance.getParent().as(SvpModule.class);

            // One of the first port owner's participants is a DNA instance that precedes the second port's owner

            boolean b = p1.getOwner().as(Interaction.class)
                .flatMap(i -> i.getParticipants().stream()
                    .map(Participation::getParticipant)
                    .filter(p -> p. getDefinition().map(def ->
                        def.getSbolTypes().contains(ComponentType.DNA)).orElse(Boolean.FALSE))
                    .findFirst())
                    .flatMap(fc -> fc.as(SvpInstance.class))
                .flatMap(svp -> toInstanceParent.map(svm -> SBSvpUtil.containsAndPrecedes(svm, svp, toInstance)))
                .orElse(Boolean.FALSE);

            if(b) {
                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Is modulating wire 1: {} -> {}", p1.getOwner().getDisplayId(), p2.getOwner().getDisplayId());
            }
            return b;
        }

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Is modulating wire 2: {} -> {}", p1.getOwner().getDisplayId(), p2.getOwner().getDisplayId());

        return true;
    }
}
