package uk.ac.ncl.icos.synbad.svp.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

public class GrnTreeParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(GrnTreeParser.class);

    private final SBWorkspace workspace;
    private final URI[] CONTEXTS;

    public GrnTreeParser(SBWorkspace workspace, URI... contexts) {
        this.workspace = workspace;
        this.CONTEXTS = contexts;
    }

    public SvpModule createModuleFromGrnTree(SBIdentity identity, GRNTree tree) {
        return createModuleFromCompilables(identity, GRNCompilableFactory.getCompilables(tree));
    }

    public SvpModule createModuleFromCompilables(SBIdentity identity, List<ICompilable> compilables) {
        LOGGER.debug("Setting up workspace...");
        Map<String, PartBundle> bundles = new HashMap<>();
        // returns a list of parsed SVPs and populates bundles
        List<Svp> svps = parseParts(compilables, bundles);
        Set<SvpInteraction> parseInteractions = parseInteractions(compilables, bundles);

        SBSvpBuilder b = new SvpActionBuilderImpl(workspace, CONTEXTS)
                .createSvm(identity);

        for(Svp svp : svps) {
            b.addSvp(identity, workspace.getIdentityFactory().getIdentity(svp.getIdentity()), null);
        }

        workspace.perform(b.build());
        return workspace.getObject(identity.getIdentity(), SvpModule.class, CONTEXTS);
    }

    public List<Svp> parseParts(List<ICompilable> compilables, Map<String, PartBundle> bundles) {

        SVPReader reader = new SVPReader(workspace, CONTEXTS);
        List<Svp> svps = new LinkedList<>();

        for(ICompilable compilable : compilables) {

            if(!compilable.getPart().getType().equals("mRNA")) {

                svps.add(reader.read(compilable.getPart(), compilable.getInternalInteractions()));

                Map<Interaction, List<Part>> interactionMap = new HashMap<>();
                for(Interaction interaction : compilable.getInteractions().keySet()) {
                    List<Part> parts = compilables.stream().filter(c -> interaction.getParts().contains(c.getPart().getName()))
                            .map(c -> c.getPart()).collect(Collectors.toList());
                    interactionMap.put(interaction, parts);
                }

                PartBundle bundle = interactionMap.isEmpty() ?
                        new PartBundle(compilable.getPart(), compilable.getInternalInteractions() == null ? Collections.EMPTY_LIST : compilable.getInternalInteractions()) :
                        new PartBundle(compilable.getPart(), compilable.getInternalInteractions() == null ? Collections.EMPTY_LIST : compilable.getInternalInteractions(), interactionMap);

                bundles.put(bundle.getPart().getName(), bundle);
            }
        }
        return svps;
    }

    public Set<SvpInteraction> parseInteractions(List<ICompilable> compilables, Map<String, PartBundle> bundles) {

        SVPInteractionReader iReader = new SVPInteractionReader(workspace, CONTEXTS);
        Set<SvpInteraction> svpInteractions = new HashSet<>();

        for(ICompilable compilable : compilables) {

            for(Interaction interaction : compilable.getInteractions().keySet()) {
                if(interaction.getParts().stream().allMatch(p -> bundles.containsKey(p))) {
                    Map<String, PartBundle> participantBundles = new HashMap<>();
                    interaction.getParts().forEach(p -> {
                        participantBundles.put(p, bundles.get(p));
                    });
                    svpInteractions.add(iReader.read(interaction, participantBundles));
                }
            }
        }
        return svpInteractions;
    }
}
