/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.datadef;

import java.net.URI;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;

/**
 *
 * @author owengilfellon
 */
public enum SynBadPortState implements PortState {
    Default(UriHelper.synbad.namespacedUri("synbadportstate/default").toASCIIString()),
    Phosphorylated(UriHelper.synbad.namespacedUri("synbadportstate/phosphorylated").toASCIIString());

    private final URI uri;
    
    public static SynBadPortState fromString(String string) {
        if(string.equals("Phosphorylated"))
            return Phosphorylated;
        else
            return Default;
    }
    
    private SynBadPortState(String uri) {
        this.uri = URI.create(uri);
    }
    
    @Override    
    public URI getUri() {
        return uri;
    }
}
