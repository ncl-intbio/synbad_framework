/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBModule;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowInstance;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.object.TopLevel;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

/**
 * SvpModule can have Svps and SvpModules as Children. SvpInteractions been SVPs
 * and SVMs are managed globally. 
 * @author owengilfellon
 */
@DomainObject(rdfType = SvpModule.TYPE)
public class SvpModule extends ASBModule  implements TopLevel, SvpEntity<ModuleDefinition>, SBDefinition {
    
    public static final String TYPE =  "http://virtualparts.org/SvmDefinition";
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpModule.class);

    public SvpModule(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    private SBFlowObject getDefFromInstance(SBWorkspace ws, SBIdentified instance) {
        if(instance.is(SvpInstance.TYPE))
            return instance.as(SvpInstance.class).flatMap(SvpInstance::getDefinition).orElse(null);
        else if(instance.is(SvmInstance.TYPE))
            return instance.as(SvmInstance.class).flatMap(SvmInstance::getDefinition).orElse(null);
        else if(instance.is(SviInstance.TYPE))
            return instance.as(SviInstance.class).flatMap(SviInstance::getDefinition).orElse(null);
        return null;
    }

    /**
     * Returns SVPs and SVMs ordered according to their ordering by 
     * SequenceConstraints and SequenceAnnotations
     * @return 
     */
    public List<SBFlowObject> getOrderedChildren() {
        
        List<SBFlowObject> children = new ArrayList<>();
        
        getOrderedInstances().stream()
                //.map(i -> i.getWrappedObject())
                .map((instance) -> getDefFromInstance(ws, instance)).forEachOrdered((obj) -> {
            children.add(obj);
        });
        
        return children;
    }
    
    //public List<SviInstance> getInteractionInstances()

     public List<SBSvpSequenceObject<? extends SvpEntity>> getOrderedInstances() {
         
        //ModuleDefinition md = getExtension();
        FunctionalComponent dnaInstance = getDnaInstance();
        List<SBSvpSequenceObject<? extends SvpEntity>> orderedInstances = new ArrayList();
        SBWorkspaceGraph g = new SBWorkspaceGraph(ws, getContexts());

        List<Component> components =  dnaInstance.getDefinition()
                .map(d -> d.getOrderedComponents())
                .orElse(Collections.EMPTY_LIST);
        
        for(Component component : components) {

            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Processing Component: {}", component.getDisplayId());
            SbolInstance outerMappedInstance = getLocalFromRemote(component, SbolInstance.class, g);
            Module m = getModuleOwnerOfMapsToFromRemote(g, component);
            
            if(m == null) {
                if(outerMappedInstance.is(Module.TYPE))
                    orderedInstances.add(ws.getObject(outerMappedInstance.getIdentity(), SvmInstance.class, getContexts()));
                else if(outerMappedInstance.is(FunctionalComponent.TYPE))
                    orderedInstances.add(ws.getObject(outerMappedInstance.getIdentity(), SvpInstance.class, getContexts()));
            } else
                orderedInstances.add(ws.getObject(m.getIdentity(), SvmInstance.class, getContexts()));
        }
        
        g.close();
        return orderedInstances;
    }
     
     
     
    public <T extends SbolInstance> T getRemotelFromLocal(SbolInstance instance, Class<T> clazz, SBWorkspaceGraph g) {
         return g.getTraversal()
            .v(instance)
            .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasLocalInstance)
            .v(SBDirection.IN, MapsTo.class)
            .e(SBDirection.OUT, SynBadTerms.SbolMapsTo.hasRemoteInstance)
            .v(SBDirection.OUT).getResult().streamVertexes()
            .filter(i -> clazz.isAssignableFrom(i.getClass()))
            .map(i -> clazz.cast(i))
            .findFirst().orElse(null);
     }
     
     public <T extends SbolInstance> T getLocalFromRemote(SbolInstance instance, Class<T> clazz, SBWorkspaceGraph g) {
         return g.getTraversal()
            .v(instance)
            .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasRemoteInstance)
            .v(SBDirection.IN, MapsTo.class)
            .e(SBDirection.OUT, SynBadTerms.SbolMapsTo.hasLocalInstance)
            .v(SBDirection.OUT).getResult().streamVertexes()
            .filter(i -> clazz.isAssignableFrom(i.getClass()))
            .map(i -> clazz.cast(i)).findFirst().orElse(null);
     }
     
      
    private Module getModuleOwnerOfMapsToFromRemote(SBWorkspaceGraph g, SbolInstance fc) {
        
        Collection<Module> collection = g.getTraversal().v(fc)
            .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasRemoteInstance)
            .v(SBDirection.IN, MapsTo.class)
            .e(SBDirection.OUT, SynBadTerms.SbolMapsTo.hasLocalInstance)
            .v(SBDirection.OUT, FunctionalComponent.class)
            .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasLocalInstance)
            .v(SBDirection.IN, MapsTo.class)
            .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasMapsTo)
            .v(SBDirection.IN, Module.class).as("MOD").getResult().getLabelledEntities(Module.class, "MOD");

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Found {} mapped instances", collection.size());

        if(LOGGER.isTraceEnabled())
            collection.stream().forEach((SBIdentified o) -> LOGGER.trace(" - {}", o.getDisplayId()));
        
        return collection.stream().filter(obj -> obj.is(Module.TYPE))
            .map(obj -> (Module)obj).findFirst().orElse(null);
    }

    /**
     * Returns either SVPs or SVMs ordered according to their ordering by 
     * SequenceConstraints and SequenceAnnotations
     * @return 
     */
    public <T extends SBFlowObject> List<T> getOrderedChildren(Class<T> childClass) {
        List<T> children = new ArrayList<>();
        getOrderedInstances().stream()
                //.map(i -> i.getWrappedObject())
                .map((instance) -> getDefFromInstance(ws, instance))
                .filter((obj) -> (childClass.isAssignableFrom(obj.getClass()))).forEachOrdered((obj) -> {
            children.add(childClass.cast(obj));
        });
        return children;
    }
    
    public boolean removeChild(int index) {
        
        List<Component> children = getDnaDefinition().getOrderedComponents();
        
        if(children.size() <= index)
            return false;
        
        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, getContexts());
        b.removeChild(getIdentity(), index);
        ws.perform(b.build());
        
        return true;
    }
    
    public Set<SvpModule> getDescendantModules() {
        return getNestedSvpModules(new HashSet<>(), this);
    }
    
    private Set<SvpModule> getNestedSvpModules(Set<SvpModule> modules, SvpModule parent) {
        parent.getChildren(SvpModule.class).forEach(m -> {
            modules.add(m);
            getNestedSvpModules(modules, m);
        });
        return modules;
    }

    private ModuleDefinition getModuleDefinition() {
        ModuleDefinition md = ws.getObject(getIdentity(), ModuleDefinition.class, getContexts());
        if(md == null) {
            LOGGER.error("Could not retrieve ModuleDefinition for {}", getDisplayId());
            return null;
        }
        
        return md;
    }

    public ComponentDefinition getDnaDefinition() {
        return getIncomingObject(SynBadTerms.SynBadModule.instancedTuOf, ComponentDefinition.class);
    }
    
    public FunctionalComponent getDnaInstance() {
        ModuleDefinition md = getModuleDefinition();
        ComponentDefinition cd = getDnaDefinition();
        return ws.incomingEdges(cd, SynBadTerms.Sbol.definedBy, FunctionalComponent.class, getContexts()).stream()
               .map(e -> ws.getObject(e.getFrom(), FunctionalComponent.class, getContexts()))
                .filter(fc-> fc.getParent().getIdentity().equals(md.getIdentity()))
                .findFirst().orElse(null);       
    }
    
    public Set<SvpInstance> getSvpInstances() {
        return as(ModuleDefinition.class)
                .map(md -> md.getFunctionalComponents().stream()
                    .map(i -> i.as(SvpInstance.class))
                    .filter(i -> i.isPresent())
                    .map(i -> i.get()).collect(Collectors.toSet()))
                .orElse(Collections.EMPTY_SET);
    }
    
    public Set<SvmInstance> getSvmInstances() {
        return as(ModuleDefinition.class)
                .map(md -> md.getModules().stream()
                    .map(i -> i.as(SvmInstance.class))
                    .filter(i -> i.isPresent())
                    .map(i -> i.get()).collect(Collectors.toSet()))
                .orElse(Collections.EMPTY_SET);
    }
    
    public Set<SviInstance> getSviInstances() {
        return as(ModuleDefinition.class)
                .map(md -> md.getInteractions().stream()
                .map(i -> i.as(SviInstance.class))
                .filter(Optional::isPresent)
                .map(i -> i.get()).collect(Collectors.toSet())).orElse(Collections.EMPTY_SET);
    }

    public Set<SviInstance> getSviInstances(InteractionType type) {
        return getSviInstances().stream()
                .filter(i -> i.getDefinition().map(d -> d.getInteractionType() == type)
                .orElse(Boolean.FALSE)).collect(Collectors.toSet());
    }

    public Set<SviInstance> getSviInstances(SvpInteraction definition) {
        return getSviInstances().stream().filter(i -> i.getDefinition().map(d ->
                        d.getIdentity().equals(definition.getIdentity())).orElse(Boolean.FALSE))
                .collect(Collectors.toSet());
    }

    public Set<SvpInteraction> getInteractionDefinitions() {
        return ws.getObject(getIdentity(), ModuleDefinition.class, getContexts()).getInteractions().stream()
            .flatMap(i -> ws.outgoingEdges(i, SynBadTerms.SynBadEntity.extensionOf, SvpInteraction.class, getContexts()).stream())
            .map(e -> ws.getObject(e.getTo(), SvpInteraction.class, getContexts()))
            .filter(svi -> svi != null)
            .collect(Collectors.toSet());
    }
    
    public SBPort createProxyPort(SBPort port) {
        SBSvpBuilder b = new SvpActionBuilderImpl(getWorkspace(), getContexts());
        SBIdentity portId = SvpIdentityHelper.getProxyPortIdentity(ws.getIdentityFactory().getIdentity(getIdentity()), ws.getIdentityFactory().getIdentity(port.getIdentity()));
        b.createProxyPort(portId,
                port.getIdentity(),
                getIdentity(),
                SBIdentityHelper.getURI(Module.TYPE));
        ws.perform(b.build());
        SBPort proxy = ws.getObject(portId.getIdentity(), SBPort.class, getContexts());
        return proxy;
    }
    
    
    public void addChild(Svp svp) {
        SBIdentity parent = ws.getIdentityFactory().getIdentity(getIdentity());
        SBIdentity child = ws.getIdentityFactory().getIdentity(svp.getIdentity());
        ws.perform(new SvpActionBuilderImpl(ws, getContexts())
            .addSvp(parent, child, null).build());
    }
    
    public void addChild(Svp svp, int index) {
        SBIdentity parent = ws.getIdentityFactory().getIdentity(getIdentity());
        SBIdentity child = ws.getIdentityFactory().getIdentity(svp.getIdentity());
        ws.perform(new SvpActionBuilderImpl(ws, getContexts())
            .addSvp( parent, child, null ).build());
    }

    public void addChild(SvpModule svm) {
        SBIdentity parent = ws.getIdentityFactory().getIdentity(getIdentity());
        SBIdentity child = ws.getIdentityFactory().getIdentity(svm.getIdentity());
        ws.perform(new SvpActionBuilderImpl(ws, getContexts())
            .addSvm( parent, child, null ).build());
    }
    
    public void addChild(SvpModule svm, int index) {
        SBIdentity parent = ws.getIdentityFactory().getIdentity(getIdentity());
        SBIdentity child = ws.getIdentityFactory().getIdentity(svm.getIdentity());
        ws.perform(new SvpActionBuilderImpl(ws, getContexts())
            .addSvm(parent, child, null ).build());
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public static SBAction CreateSvm ( SBIdentity identity,  SBWorkspace workspace, URI[] contexts) {
        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(workspace,  contexts)
            .createObject(identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE), null, null);
        return b.build();
    }
    
       
    public boolean areWired(SBFlowInstance<? extends SBIdentified> from, SBFlowInstance<? extends SBIdentified> to) {
        if(!from.getParent().getIdentity().equals(getIdentity()) || !to.getParent().getIdentity().equals(getIdentity()))
            return false;
        
        try( SBWorkspaceGraph g = new SBWorkspaceGraph(ws,getContexts())) {
        
            for(SBPortInstance f : from.getPorts(SBPortDirection.OUT)) {

                 boolean isWired = g.getTraversal()
                    .v(f)
                    .e(SBDirection.IN, SynBadTerms.SynBadWire.isFrom)
                    .v(SBDirection.IN)
                    .e(SBDirection.OUT, SynBadTerms.SynBadWire.isTo)
                    .v(SBDirection.OUT)
                    .e(SBDirection.IN, SynBadTerms.SynBadPortInstance.hasPortInstance)
                    .v(SBDirection.IN)
                    .getResult().streamVertexes().anyMatch(i -> i.getIdentity().equals(to.getIdentity()));

                 if(isWired)
                     return true;
            } 
        }
        
        return false;
    }

    @Override
    public String toString() {
        return getDisplayId().concat(" { ").concat(getOrderedChildren().stream()
                .map(SBIdentified::getDisplayId)
                .reduce("", (a, b) -> a.concat(a.isEmpty() ? b : "; ".concat(b)))).concat("}");
    }
}
