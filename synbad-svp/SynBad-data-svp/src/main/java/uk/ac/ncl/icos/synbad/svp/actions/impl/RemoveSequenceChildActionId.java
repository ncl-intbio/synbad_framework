/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceConstraint;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.actions.ADeferredAction;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;

import java.net.URI;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class RemoveSequenceChildActionId extends ADeferredAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveSequenceChildActionId.class);

    protected final SBIdentity parentId;
    private final URI childIdentity;

    public RemoveSequenceChildActionId(URI parentSvmIdentity, URI componentId, SBWorkspace workspace, URI[] contexts) {
        super(SBGraphEventType.REMOVED, workspace, contexts);
        this.parentId = workspace.getIdentityFactory().getIdentity(parentSvmIdentity);
        this.childIdentity = componentId;
    }

    @Override
    protected SBAction getAction(SBWorkspace ws) {
 
        // SVM and SVP objects
        SvpModule parentSvm = ws.getObject(parentId.getIdentity(), SvpModule.class, contexts);

        if(parentSvm == null) {
            LOGGER.error("Could not get module: {}", parentId.getDisplayID());
            return null;
        }

        List<SBFlowObject> orderedChildren = parentSvm.getOrderedChildren();
        ComponentDefinition svmDnaDef = parentSvm.getDnaDefinition();

        try {
            SBIdentity componentId = SBIdentity.getIdentity(childIdentity);

            SbolActionBuilder b = new SbolActionBuilder(ws, contexts)
                    .removeObjectAndDependants(childIdentity, svmDnaDef.getIdentity());

            LOGGER.debug("Removing {}", childIdentity.toASCIIString());

            if(orderedChildren.stream().filter(c -> c.getIdentity().equals(componentId.getIdentity())).count() == 1)
                b.removeEdge(parentId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), componentId.getIdentity());

            b = updateConstraintsFromRemoved(b, svmDnaDef, componentId);

            return b.build("RemoveSeqChild ["+ componentId + "]");
        } catch (SBIdentityException e) {
            LOGGER.error("Could not get id from component ID", e);
            return null;
        }

    }
    
    protected SbolActionBuilder updateConstraintsFromRemoved(SbolActionBuilder b, ComponentDefinition constraintOwner, SBIdentity removedComponentId) {
     
            List<Component> orderedComponents = constraintOwner.getOrderedComponents();
            
            // Get the component that is being removed

            Component toRemove = orderedComponents.stream().filter(c -> c.getIdentity().equals(removedComponentId.getIdentity())).findFirst().orElse(null);
            
            if(toRemove == null)
                return b;
            
            int toRemoveIndex = orderedComponents.indexOf(toRemove);
            
            if(toRemoveIndex == -1)
                return b;
            
            int beforeIndex = toRemoveIndex - 1;
            int afterIndex = toRemoveIndex + 1;
 
            // If there is both preceding and preceded, we must add constraint
            // constraint between them
            
            if(beforeIndex >= 0 && afterIndex < orderedComponents.size()){
                Component componentBefore = orderedComponents.get(beforeIndex);
                Component componentAfter = orderedComponents.get(afterIndex);
                SBIdentity constraintOwnerId = b.getWorkspace().getIdentityFactory().getIdentity(constraintOwner.getIdentity());
                SBIdentity precedingId = b.getWorkspace().getIdentityFactory().getIdentity(componentBefore.getIdentity());
                SBIdentity followingId = b.getWorkspace().getIdentityFactory().getIdentity(componentAfter.getIdentity());
                SBIdentity sequenceConstraintId = SequenceConstraint.getSequenceConstraintIdentity(constraintOwnerId, precedingId, followingId, ConstraintRestriction.PRECEDES);
                LOGGER.debug("Adding constraint:\t{}", sequenceConstraintId.getDisplayID());
                b = b.createSequenceConstraint(sequenceConstraintId.getIdentity(), constraintOwner.getIdentity(), precedingId.getIdentity(), ConstraintRestriction.PRECEDES, followingId.getIdentity()); 
            }
 
        return b;
    }
}

