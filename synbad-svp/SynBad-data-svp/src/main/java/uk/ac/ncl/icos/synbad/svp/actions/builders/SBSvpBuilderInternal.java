/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import java.net.URI;

import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.actions.SviExternalActionFactory;

/**
 *
 * @author owen
 */
public interface SBSvpBuilderInternal extends SBActionBuilder {

    SBSvpBuilderInternal addExtends(SBIdentity sbComponent, SBIdentity sbol);

    SBSvpBuilderInternal addExtends(SvpModule svp, ModuleDefinition sbol);

    SBSvpBuilderInternal addExtends(Svp svp, ComponentDefinition sbol);

    SBSvpBuilderInternal addExtends(SvpInteraction svp, Interaction sbol);

    SBSvpBuilderInternal addInstanceOfContentsExtends(FunctionalComponent instance, SvpModule moduleWithContents);

    SBSvpBuilderInternal createCompound(SBIdentity identity);

    SBSvpBuilderInternal createDegradation(SBIdentity identity, URI parentId, ComponentType type, SynBadPortState state, double kd);

    SBSvpBuilderInternal createDephosphorylate(SBIdentity identity, double kdep, URI parentId);

    SBSvpBuilderInternal createEdge(URI source, URI predicate, URI target);

    SBSvpBuilderInternal createMrna(SBIdentity identity, URI parentId);

    SBSvpBuilderInternal createMrnaProduction(SBIdentity identity, double ktr, double n, URI parentId);

    SBSvpBuilderInternal createParameter(SBIdentity identity, URI parentIdentity, String name, String type, Double value, String scope, String evidenceType);

    SvpParticipant.ParticipantActionBuilder createParticipation(SBIdentity identity, URI sviId);

    SBSvpBuilderInternal createPhosphorylate(SBIdentity identity, URI phosphorylator, URI phosphorylated, double kf, double kd, double kDeP);

    SBSvpBuilderInternal createPhosphorylateBySmallMolecule(SBIdentity identity, URI smallMolecule, double kf, URI parentId);

    SBSvpBuilderInternal createPoPSProduction(SBIdentity identity, double ktr, URI parentId);

    SBSvpBuilderInternal createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType);

    SBSvpBuilderInternal createPortInstance(URI identity, URI owner, URI ownerType, URI definition);

    SBSvpBuilderInternal createProperty(SBIdentity propertyIdentity, URI ownerIdentity, URI ownerType, String name, String value, String description);

    SviExternalActionFactory.ComplexDisassociationActionBuilder createComplexDisassociation(SBIdentity complexId);

    SviExternalActionFactory.ComplexFormationActionBuilder createComplexFormation(SBIdentity complexId);

    SBSvpBuilderInternal createProteinProduction(SBIdentity identity, URI parentId);

    SBSvpBuilderInternal createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType);
    
    SBSvpBuilderInternal createRiPSProduction(SBIdentity identity, double ktl, double n, URI parentId);

    SBSvpBuilderInternal createValue(URI source, URI predicate, SBValue value, URI sourceType);

    SBSvpBuilderInternal duplicateObject(URI identity, URI[] fromContexts, boolean updateIds);

    SBSvpBuilderInternal removeChild(URI parentSvm, int childIndex);

    SBSvpBuilderInternal removeEdge(URI source, URI predicate, URI target);

    SBSvpBuilderInternal removeObject(URI identity, URI ownerIdentity, URI type, URI ownerType);

    SBSvpBuilderInternal removeObjectAndDependancies(URI identity, URI ownerIdentity);

    SBSvpBuilderInternal removeValue(URI source, URI predicate, SBValue value, URI sourceType);

}
