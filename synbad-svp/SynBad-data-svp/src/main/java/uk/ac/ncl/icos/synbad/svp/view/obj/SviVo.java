/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.svp.obj.SviInstance;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SviVo extends DefaultSBViewIdentified<SBIdentified> implements SBViewComponent<SBIdentified, SBViewPort>, SvVo {
    
    private final SvpInteraction svpInteraction;
    
    public SviVo(SviInstance instance, SvpPView view) {
        super(instance, view);
        this.svpInteraction = getSvpInteractionFromWs();
    }

    public SviVo(SviInstance instance, long id, SBView view) {
        super(instance, id, view);
        this.svpInteraction = getSvpInteractionFromWs();
    }

    @Override
    public SvpPView getView() {
        return (SvpPView) super.getView(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Set<SvpVo> getParticipants() {
        Set<Svp> svpParticipants = svpInteraction.getParticipants().stream()
                .map(p -> p.getSvpParticipant()).collect(Collectors.toSet());
        
        return Collections.EMPTY_SET;
    }

    public SvpInteraction getSvpInteraction() {
        return svpInteraction;
    }
    
    @Override
    public SvmVo getParent() {
        return getView().getParent(this);
    }
    
     @Override
    public Set<SvpViewPort> getPorts(SBPortDirection... directions) {
        Collection<SBPortDirection> portDirections = Arrays.asList(directions);
        return getView().getPorts(this).stream()
                .filter(p -> portDirections.isEmpty() || portDirections.contains(p.getDirection()))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<SBViewPort> getPorts() {
       return getView().getPorts(this).stream()
                .collect(Collectors.toSet());
    }

    @Override
    public SviInstance getObject() {
        return (SviInstance) super.getObject();
    }

    private SvpInteraction getSvpInteractionFromWs() {
        return getObject().getDefinition().orElse(null);
    }

    @Override
    public String toString() {
        return getDisplayId();
    }
}
