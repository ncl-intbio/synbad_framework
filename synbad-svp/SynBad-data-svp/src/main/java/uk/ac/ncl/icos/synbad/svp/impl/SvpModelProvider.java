/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.impl;

import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.model.ASBModelFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.model.SBModelFactory;

/**
 *
 * @author owen
 */
@ServiceProvider(service = SBModelFactory.class)
public class SvpModelProvider extends ASBModelFactory<SvpModel, SvpModule> {

    public SvpModelProvider() {
        super(SvpModel.class, SvpModule.class);
    }
    
    @Override
    public SvpModel getModel(SvpModule rootNode) {
        return new SvpModel(rootNode.getWorkspace(), rootNode);
    }
}
