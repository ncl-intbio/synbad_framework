/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.flow.action.FlowActions;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 * Listens to an InteractionManager and creates or removes interactions as appropriate.
 * @author owengilfellon
 */ 
public class AWireUpdater extends ASBWorkspaceSubscriber {

    private static final Logger LOGGER = LoggerFactory.getLogger(AWireUpdater.class);
    protected SBIdentityFactory factory;

    public AWireUpdater(SBIdentityFactory f) {
        this.factory = f;
    }


    /**
     * Returns an array with the out port at index [0] and the in port at index [1]
     * @param port1
     * @param port2
     * @return
     */
    protected SBPortInstance[] sortOutInPorts(SBPortInstance port1, SBPortInstance port2) {
        SBPortInstance inPort = port1.getPortDirection() == SBPortDirection.IN ? port1 : port2;
        SBPortInstance outPort = port1.getPortDirection() == SBPortDirection.OUT ? port1 : port2;
        return new SBPortInstance[] {outPort, inPort};
    }


    protected void createWire(SBPortInstance outPort, SBPortInstance p, SBWireContext ctx) {

        SvpModule svpModule = ctx.getWireParent().get();

        SBIdentity wireIdentity = SvpWire.getWireIdentity(
                factory.getIdentity(svpModule.getIdentity()),
                factory.getIdentity(outPort.getIdentity()),
                factory.getIdentity(p.getIdentity()));

        if (!ctx.getExpectedWires().keySet().contains(wireIdentity.getIdentity())) {

            ctx.putExpectedWire(
                    wireIdentity.getIdentity(), FlowActions.createWire(wireIdentity,
                            svpModule, outPort, p,  svpModule.getContexts()));

            if(LOGGER.isDebugEnabled() && !ctx.getExistingWires().keySet().contains(wireIdentity.getIdentity())) {
                LOGGER.debug("Adding wire:\t{}:{} -> {}:{}",
                        outPort.getOwner().getParent().getDisplayId(),
                        outPort.getOwner().getDisplayId(),
                        p.getOwner().getParent().getDisplayId(),
                        p.getOwner().getDisplayId());
            }
        }
    }
}