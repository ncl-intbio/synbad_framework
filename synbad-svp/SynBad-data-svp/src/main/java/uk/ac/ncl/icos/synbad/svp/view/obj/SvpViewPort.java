/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.view.SBPView;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SvpViewPort extends DefaultSBViewIdentified<SBPortInstance> implements SBViewPort<SBPortInstance>  {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpViewPort.class);

    public SvpViewPort(SBPortInstance pi, SBPView view) {
        super(pi, view);
    }

    public SvpViewPort(SBPortInstance pi, long id, SBPView view ) {
        super(pi, id, view);
    }


    private SBPortInstance getPortInstanceFromWs() {

        if(!getObject().is(DefaultSBPortInstance.TYPE))
            return null;

        SBPortInstance definition = getWorkspace().getObject(getIdentity(),  SBPortInstance.class, getContexts());

        if(definition == null) {
            LOGGER.error("Could not retrieve {} from workspace", getDisplayId());
        }

        return definition;
    }
    @Override
    public SBPortDirection getDirection() {
        return getPortInstanceFromWs().getPortDirection();
    }

    @Override
    public SBPView getView() {
        return(SBPView)super.getView(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isProxied() {
        return getView().isProxied(this);
    }

    @Override
    public boolean isProxy() {
        return getPortInstanceFromWs().isProxy();
    }

    @Override
    public SvpViewPort getProxy() {
        return (SvpViewPort)getView().getProxy(this);
    }

    @Override
    public SvpViewPort getProxied() {
        return (SvpViewPort)getView().getProxied(this);
    }

    @Override
    public SvVo getOwner() {
        return (SvVo)getView().getPortOwner(this);
    }
    
}
