/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.parser;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.actions.ComponentFactory;


/**
 *
 * @author owengilfellon
 */
public class SVPInteractionWriter {

    private final Logger LOGGER = LoggerFactory.getLogger(SVPInteractionWriter.class);
    private final SBWorkspace workspace;
    private static final String prefix = "http://virtualparts.org";
    private final SBDataDefManager definitionManager = SBDataDefManager.getManager();
    private final URI[] svpContext;

    
    public SVPInteractionWriter(SBWorkspace workspace, URI[] contexts)
    {
        this.workspace = workspace;
        this.svpContext = contexts;
    }

    public Interaction write(SvpInteraction source) {
        LOGGER.debug("Writing interaction: {}", source.getDisplayId());
        Interaction interaction = new Interaction();
        createInteractionProperties(source, interaction);        
        return interaction;
    }

    private void createInteractionProperties(SvpInteraction svpInteraction, Interaction interaction) {

        interaction.setName(svpInteraction.getDisplayId());
        interaction.setDescription(svpInteraction.getDescription());
        
        SBValue mathName = svpInteraction.getValue(SynBadTerms.VPR.mathName);
        if(mathName != null) {
            LOGGER.debug("Writing name: {}", mathName);
            interaction.setMathName(mathName.asString());
        }
        
        SBValue freeTextMath = svpInteraction.getValue(SynBadTerms.VPR.freeTextMath);
        if(freeTextMath != null) {
            LOGGER.debug("Writing freeTextMath: {}", freeTextMath);
            interaction.setFreeTextMath(freeTextMath.asString());
        }
        
        SBValue isInternal = svpInteraction.getValue(SynBadTerms.VPR.isInternal);
        if(isInternal != null) {
            LOGGER.debug("Writing isInternal: {}", isInternal.asBoolean());
            interaction.setIsInternal(isInternal.asBoolean());
        }
            
        SBValue isReaction = svpInteraction.getValue(SynBadTerms.VPR.isReaction);
        if(isReaction != null) {
            LOGGER.debug("Writing isReaction: {}", isReaction.asBoolean());
            interaction.setIsReaction(isReaction.asBoolean());
        }
            
        SBValue isReversible = svpInteraction.getValue(SynBadTerms.VPR.isReversible);
        if(isReversible != null) {
            LOGGER.debug("Writing isReversible: {}", isReversible.asBoolean());
            interaction.setIsReversible(isReversible.asBoolean());
        }
        
        String type = svpInteraction.getInteractionType().getPrettyName();
        LOGGER.debug("Writing interactionType: {}", type);
        interaction.setInteractionType(type);

        Set<String> parts = svpInteraction.getPartParticipants();

        if(!parts.isEmpty())
            interaction.setParts(new ArrayList<>(parts));
        else
            interaction.setParts(Collections.EMPTY_LIST);
        
        List<InteractionPartDetail> ipds = svpInteraction.getParticipants().stream()
                
            // Remove the additional participations we added for DNA
                
            .filter(p -> !p.getNameInMath().equals("DNA"))
            .map(p -> {
                
                InteractionPartDetail pd = new InteractionPartDetail(
                    p.getSvpParticipant().getDisplayId(),
                    p.getState().toString(),
                    p.getDirection().toString(),
                    p.getStoichiometry(),
                    p.getNameInMath());
                
                LOGGER.debug("Writing IPD: {} : {} : {} : {} : {}",
                    pd.getPartName(),
                    pd.getPartForm(),
                    pd.getInteractionRole(),
                    pd.getNumberOfMolecules(),
                    pd.getMathName()); 
                
                
                return pd;
            }).collect(Collectors.toList());
     
        interaction.setPartDetails(ipds);
        
        /*List<String> ipd_parts = ipds.stream()
            .map(i -> i.getPartName())
            .distinct().collect(Collectors.toList());
        
        ipd_parts.stream().forEach(s -> LOGGER.debug("Adding part: {}", s));
        
        interaction.setParts(ipd_parts);
*/
        
        if(svpInteraction.getParameters().isEmpty())
            interaction.setParameters(Collections.EMPTY_LIST);
        
        svpInteraction.getParameters()
            .stream()
            .map(p ->  {
               Parameter par =  new Parameter(
                p.getParameterType(),
                p.getName(),
                p.getParameterValue(),
                p.getParameterEvidenceCode(),
                p.getParameterScope());
               LOGGER.debug("Writing parameter: {} : {} : {} : {} : {}", 
                    par.getName(),
                    par.getParameterType(),
                    par.getValue(),
                    par.getScope(),
                    par.getEvidenceType());
               return par; })
            .forEach(interaction::AddParameter);
    }
     
      private Map<String, Component> addComponents(SvpInteraction interaction, Interaction internalInteraction) {

        Map<String, Component> components = new HashMap<>();
        
        if (internalInteraction.getPartDetails() == null)
            return components;
        
        
        Map<String, Component> inputs = new HashMap<>();
        Map<String, Component> outputs = new HashMap<>();

        for(InteractionPartDetail detail : internalInteraction.getPartDetails()) {
            String id = detail.getPartForm().equals("Phosphorylated") ?
                    detail.getPartName() + "_Phosphorylated" :
                    detail.getPartName();

            Part part = SVPManager.getSVPManager().getPart(detail.getPartName());
            String form = detail.getPartForm();
            
            ComponentType partType = definitionManager.getDefinition(ComponentType.class, part.getType());
            ComponentType type = partType == null ? ComponentType.Protein : partType;
            SBAction component = null;

            if(!components.containsKey(id)) {
                switch(type) {
                    case DNA:
                        URI identity = SBIdentityHelper.getURI( prefix + part.getName() +"_dna/1.0");
                        component = ComponentFactory.createDna(
                            identity,
                            Collections.singleton(definitionManager.getDefinition(Role.class, part.getType())),
                            workspace, svpContext);
                        break;
                    case Protein:
                        URI identity2 = SBIdentityHelper.getURI( prefix + part.getName() +"_protein/1.0");
                        SynBadPortState state = form.equals("Phosphorylated") ? SynBadPortState.Phosphorylated : SynBadPortState.Default;
                        component = ComponentFactory.createProtein(
                            identity2,
                           // Collections.singleton(definitionManager.getDefinition(Role.class, part.getType())),
                            state,
                            workspace, svpContext);
                        break;
                }

                if(component == null) {
                    URI identity = SBIdentityHelper.getURI( prefix + part.getName() +"_sm/1.0");
                    component = ComponentFactory.createSmallMolecule(identity,
                           // Collections.singleton(ComponentRole.Generic),
                            workspace, svpContext);
                }
            }
            
            /*
            if(component!=null) {
                if(hasOnlyOutput(part.getName(), SynBadPortState.valueOf(form), internalInteraction.getPartDetails()) &&
                        isProduction(internalInteraction))
                    interaction.addChild(component);
                 components.put(id, component);
            }*/
               
        } 

        return components;
    }
     
    private boolean hasDirection(String speciesName, SynBadPortState state, Collection<InteractionPartDetail> ipd, SBPortDirection direction)
    {
        for(InteractionPartDetail pd : ipd) {
            if( pd.getPartName().equals(speciesName) &&
                   state == SynBadPortState.valueOf(pd.getPartForm()) && 
                   direction == SBPortDirection.fromString(pd.getInteractionRole()))
               return true;
        }

        return false;
    }   
     
    private boolean hasOnlyOutput(String speciesName, SynBadPortState state, Collection<InteractionPartDetail> ipd) {
        return hasDirection(speciesName, state, ipd, SBPortDirection.OUT) && !hasDirection(speciesName, state, ipd, SBPortDirection.IN);
    }


    private PortType getType(String string) {
        if(string.contains("PoPS"))
            return SynBadPortType.PoPS;
        if(string.contains("RiPS"))
            return SynBadPortType.RiPS;
        if(string.contains("mRNA"))
            return SynBadPortType.mRNA;
        if(string.contains("Protein") || string.contains("Species"))
            return SynBadPortType.Protein;
        if(string.contains("EnvironmentConstant"))
            return SynBadPortType.SmallMolecule;
        return null;
    }

     
     private boolean isProduction(Interaction interaction) {
        String type = interaction.getInteractionType();
        if(type.contains("Production") || type.contains("Phosphorylation") ||
                type.contains("Formation"))
            return true;
        return false;
     }

}
