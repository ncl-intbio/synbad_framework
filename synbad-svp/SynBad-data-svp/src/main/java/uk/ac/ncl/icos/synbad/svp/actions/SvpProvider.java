/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import java.net.URI;
import java.util.List;
import java.util.Random;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.parser.SVPInteractionReader;
import uk.ac.ncl.icos.synbad.svp.parser.SVPReader;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owen
 */
public class SvpProvider {
    
    private static final SVPManager SVP_MANAGER = SVPManager.getSVPManager();  
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpProvider.class);
    private final SBWorkspace WORKSPACE;
    private final URI[] CONTEXTS;
    private final String PREFIX;
    private final SBIdentityFactory ID_FACTORY;
    private final SVPReader READER;
    private final SVPInteractionReader I_READER;
    private final Random RANDOM = new Random();
    
    private boolean prototype = false;

    private SvpProvider(SBWorkspace workspace, URI[] contexts, String prefix, boolean prototype) {
        this.WORKSPACE = workspace;
        this.CONTEXTS = contexts;
        
        this.PREFIX = prefix;
        
        this.READER = new SVPReader(workspace, contexts);
        this.I_READER = new SVPInteractionReader(workspace, contexts);
        
        this.ID_FACTORY = workspace.getIdentityFactory();
        
        this.prototype = prototype;
    }
    
    public static SvpProvider getSvpProvider(SBWorkspace workspace, URI[] contexts, String prefix, boolean prototype) {
        return new SvpProvider(workspace, contexts, prefix, prototype);
    }
    
    private Svp getSvp(ComponentRole role) {
        Svp promoter = null;
        Part part = null;
        
        if(!prototype) {
            switch(role) {
                case Promoter:
                    part = SVP_MANAGER.getConstPromoter();
                    break;
                case RBS:
                    part = SVP_MANAGER.getRBS();
                    break;
                case Terminator:
                    part = SVP_MANAGER.getTerminator();
                    break;
                case Operator:
                    part = SVP_MANAGER.getNegOperator();
                    break;
                case Shim:
                    part = SVP_MANAGER.getShim();
                    break;
            }
        }
        
        SBIdentity identity = part != null ?
                    SBIdentity.getIdentity(PREFIX, part.getName(), "1.0") :  
                    ID_FACTORY.getUniqueIdentity(ID_FACTORY.getIdentity(PREFIX, role.toString(), "1.0"));


        if(part != null) {
            List<Interaction> internalInteractions = SVP_MANAGER.getInternalEvents(part);
            promoter = internalInteractions.isEmpty() ? READER.read(part) : READER.read(part, internalInteractions);
        } else {
            SvpActionBuilderImpl b = new SvpActionBuilderImpl(WORKSPACE, CONTEXTS);
            
            switch(role) {
                case Promoter:
                    b.createPromoter(identity, RANDOM.nextDouble());
                    break;
                case RBS:
                    b.createRBS(identity, RANDOM.nextDouble(), 1.0);
                    break;
                case Terminator:
                    b.createTerminator(identity);
                    break;
                case Operator:
                    b.createOperator(identity);
                    break;
            }
 
            SBAction action = b.build();
            WORKSPACE.perform(action);
            promoter = WORKSPACE.getObject(identity.getIdentity(), Svp.class, CONTEXTS);
        }

        if(promoter == null)
            LOGGER.error("Could not create svp");

        return promoter;
    }

    
    public Svp getConstitutivePromoter() {
        return getSvp(ComponentRole.Promoter);
    }
    
    public Svp getRbs() {
        return getSvp(ComponentRole.RBS);
    }
    
    public Svp getOperator() {
        return getSvp(ComponentRole.Operator);
    }
    
    public Svp getTerminator() {
        return getSvp(ComponentRole.Terminator);
    }
    

    
}
