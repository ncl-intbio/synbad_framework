/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBFlowInstance;
import uk.ac.ncl.icos.synbad.sbol.object.ChildObject;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owen
 */

@DomainObject(rdfType = SviInstance.TYPE)
public class SviInstance 
        extends ASBFlowInstance<SvpInteraction>
        implements ChildObject {
    
    public static final String TYPE = "http://virtualparts.org/SviInstance";

    public SviInstance(SBIdentity id, SBWorkspace ws, SBValueProvider provider) {
        super(id, ws, provider);
    }
    
    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    @Override
    public SvpModule getParent() {
        return as(Interaction.class).map(m -> m.getParent().getIdentity())
                .map(id -> ws.getObject(id, SvpModule.class, getContexts())).get();
    }

    public boolean isPartiallyDefined() {
        return getPorts().stream().filter(p -> p.getPortType() == SynBadPortType.Protein ||
                p.getPortType() == SynBadPortType.SmallMolecule || p.getPortType() == SynBadPortType.Complex)
                .anyMatch(p -> p.getIdentityConstraint() == null);
    }

    @Override
    public Optional<SvpInteraction> getDefinition() {
        return getOutgoingObjects(SynBadTerms.SynBadEntity.extensionOf, SvpInteraction.class).stream().findFirst();
    }

}
