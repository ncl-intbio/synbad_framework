/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.*;

/**
 *
 * @author owen
 */
public interface SBTuBuilder extends SBActionBuilder {
    
    TranscriptionUnitBuilder createTu(SBIdentity tuId);



//    SBTuBuilder repressTu(SBIdentity repressorTuId, SBIdentity repressedTuId, SBIdentity repressorSpeciesId, double binding);
//
//    SBTuBuilder activateTu(SBIdentity activatorTuId, SBIdentity activatedTuId, SBIdentity activatorSpeciesId, double binding);

    SBTuBuilder createSvm(SBIdentity identity);

    SBTuBuilder addSvm(SBIdentity parent, SBIdentity child, SBIdentity precedesComponent);

    SBTuBuilder addSvm(SBIdentity parent, SBIdentity child, int index);

    //SBCelloBuilder regulateTu(SBIdentity regulatingUnit, SBIdentity regulatedUnit);

}
