/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import java.net.URI;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = SvpParticipant.TYPE, xmlParent = SvpInteraction.class)
public class SvpParticipant extends ASBIdentified {

    public static final String TYPE = "http://www.synbad.org/SvpParticipant";
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpParticipant.class);

    public SvpParticipant(SBIdentity identity, SBWorkspace workspaceId, SBValueProvider valueProvider) {
        super(identity, workspaceId, valueProvider);
    }
    
    public SvpInteraction getInteraction() {
        return getIncomingObject(SynBadTerms.SynBadInteraction.hasParticipant, SvpInteraction.class);
    }
    
    public ComponentDefinition getSbolParticipant() {
        
        ComponentDefinition participant = getOutgoingObject(SynBadTerms.SynBadParticipant.isSbolParticipant, ComponentDefinition.class);

        if(participant == null) {
            SBValue value = getValue(SynBadTerms.SynBadParticipant.isSbolParticipant);
            URI uri = value != null && value.isURI() ? value.asURI() : null;
            SBIdentity id = uri != null ? ws.getIdentityFactory().getIdentity(uri) : null;
            
            LOGGER.error("Could not retrieve sbolParticipant [ {} ] for SvpParticipation [ {} ]", 
                    id == null ? "null" :id.getIdentity().toASCIIString(), 
                    getIdentity());
            
            return null;
        }
        
        return participant;
    }
    
    /**
     * Returns instances of the ComponentDefinition participants, from all contexts,
     * or the contexts provided.
     * @param contexts
     * @return 
     */
    public Set<FunctionalComponent> getParticipantInstances(URI... contexts) {
        
        ComponentDefinition sbolParticipant = getSbolParticipant();
        
        Set<FunctionalComponent> participantInstances = ws
            .incomingEdges(sbolParticipant, SynBadTerms.Sbol.definedBy, FunctionalComponent.class, (contexts.length > 0 ? contexts : ws.getContextIds())).stream()
            .map(e -> e.getFrom())
            .map(f -> ws.getObject(f, FunctionalComponent.class, contexts.length > 0 ? contexts : ws.getContextIds()))
            .collect(Collectors.toSet());
        
        return participantInstances;
    }
    
    public Svp getSvpParticipant() {
        return getOutgoingObject(SynBadTerms.SynBadInteraction.isSvpParticipant, Svp.class);
    }
   
    public PortState getState() {
        return dataDefinitionManager.getDefinition(PortState.class, getValue(SynBadTerms.VPR.partDetailForm).asString());
    }
    
//    public void setState(PortState state) {
//        setValue(SynBadTerms.VPR.partDetailForm, SBValue.parseValue(state.getUri()));
//    }
    
    public int getStoichiometry() {
        return getValue(SynBadTerms.VPR.partDetailStoichiometry).asInt();
    }
    
//    public void setStoichiometry(int stoichiometry) {
//        setValue(SynBadTerms.VPR.partDetailStoichiometry, SBValue.parseValue(stoichiometry));
//    }
    
    public SbolInteractionRole getInteractionRole() {
         return dataDefinitionManager.getDefinition(SbolInteractionRole.class, getValue(SynBadTerms.SynBadParticipant.interactionRole).asString());
    }
    
//    public void setInteractionRole(SbolInteractionRole role) {
//        setValue(SynBadTerms.SynBadParticipant.interactionRole, SBValue.parseValue(role.toString()));
//    }

    public SBPortDirection getPortDirection() {
        return dataDefinitionManager.getDefinition(SBPortDirection.class, getDirection().toString());
    }
    
    public ParticipationRole getDirection() {
        return dataDefinitionManager.getDefinition(ParticipationRole.class, getValue(SynBadTerms.VPR.partDetailRole).asString());
    }
    
//    public void setDirection(String role) {
//        setValue(SynBadTerms.VPR.partDetailRole, SBValue.parseValue(role));
//    }
    
    public String getNameInMath() {
        return getValue(SynBadTerms.VPR.mathName).asString();
    }
    
//    public void setNameInMath(String name) {
//        setValue(SynBadTerms.VPR.mathName, SBValue.parseValue(name));
//    }

    @Override
    public Collection<Role> getSbolRoles() {
        return getSbolParticipant().getSbolRoles();
    }

    @Override
    public Collection<Type> getSbolTypes() {
        return getSbolParticipant().getSbolTypes();
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public static ParticipantActionBuilder CreateParticipantAction( SBIdentity identity, URI parentIdentity, SBWorkspace workspace, URI[] contexts) {
        return new ParticipantActionBuilder(identity,workspace.getIdentityFactory().getIdentity(parentIdentity), workspace, contexts);
    }
    
    public static class ParticipantActionBuilder {
        
        
        private static final Logger LOGGER = LoggerFactory.getLogger(ParticipantActionBuilder.class);
        private final SvpActionBuilderImpl b;
        
        private static final URI SVP_PARTICIPANT_TYPE = SBIdentityHelper.getURI(SvpParticipant.TYPE);
        
        private final SBWorkspace workspaceId;
        private final URI[] contexts;
        private String direction;
        private PortState molecularForm = SynBadPortState.Default;
        private SbolInteractionRole role;
        private int stoichiometry = 1;
        private String nameInMath;
        private SBIdentity svp;
        private final SBIdentity sviId;
        private final SBIdentity identity;
        private URI sbolComponentId;
        
        public ParticipantActionBuilder(SvpActionBuilderImpl b, SBIdentity identity, SBIdentity sviId, SBWorkspace workspaceId, URI[] contexts) {
            this.b = b;
            this.identity = identity;
            this.sviId = sviId;
            this.workspaceId = workspaceId;
            this.contexts = contexts;
        }
                
        public ParticipantActionBuilder(SBIdentity identity, SBIdentity sviId, SBWorkspace workspaceId, URI[] contexts) {
            this.identity = identity;
            this.sviId = sviId;
            this.workspaceId = workspaceId;
            this.contexts = contexts;
            this.b = null;
        }

        public ParticipantActionBuilder setDirection(ParticipationRole direction) {
            this.direction = direction.toString();
             return this;
        }
        
        public ParticipantActionBuilder setMolecularForm(PortState state) {
            this.molecularForm = state;
             return this;
        }
        
        public ParticipantActionBuilder setInteractionRole(SbolInteractionRole role) {
            this.role = role;
             return this;
        }
        
        public ParticipantActionBuilder setStoichiometry(int stoichiometry) {
            this.stoichiometry = stoichiometry;
             return this;
        }
        
        public ParticipantActionBuilder setNameInMath(String nameInMath) {
            this.nameInMath = nameInMath;
            return this;
        }
        
        public ParticipantActionBuilder setSvp(SBIdentity svp) {
            this.svp = svp;
            return this;
        }
        
        public ParticipantActionBuilder setSbolComponentId(URI participantId) {
            this.sbolComponentId = participantId;
            return this;
        }
        
        public SBAction build() {
            
            if(sviId == null) {
                LOGGER.error("SVI ID is null");
                return null;
            }
            
            if(sbolComponentId == null) {
                LOGGER.error("Participant Component is null for {}", sviId.getDisplayID());
                return null;
            }
            
            if(direction == null) {
                LOGGER.error("Direction is null for {}", sviId.getDisplayID());
                return null;
            }
            
            if(nameInMath == null) {
                LOGGER.error("Math Name is null for {}", sviId.getDisplayID());
                return null;
            }
            
            if(svp == null) {
                LOGGER.error("SvpId is null for {}", sviId.getDisplayID());
                return null;
            }

            SBAction a =  new DefaultSBDomainBuilder(workspaceId,  contexts)
                .createObject(identity.getIdentity(), SVP_PARTICIPANT_TYPE, sviId.getIdentity(), SBIdentityHelper.getURI(SvpInteraction.TYPE))
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partDetailRole), SBValue.parseValue(direction), SVP_PARTICIPANT_TYPE)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partDetailStoichiometry), SBValue.parseValue(stoichiometry), SVP_PARTICIPANT_TYPE)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue(nameInMath), SVP_PARTICIPANT_TYPE)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partDetailForm), SBValue.parseValue(molecularForm.getUri()), SVP_PARTICIPANT_TYPE)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partDetailName), SBValue.parseValue(svp.getDisplayID()), SVP_PARTICIPANT_TYPE)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadParticipant.interactionRole), SBValue.parseValue(role.getUri()), SVP_PARTICIPANT_TYPE)
                .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadParticipant.isSbolParticipant), sbolComponentId)
                .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svp.getIdentity())    
                .createEdge(sviId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.hasParticipant), identity.getIdentity())
                .build();
          
                LOGGER.trace("Returning particicpation action for {} : {} : {} : {} : {} : {}", sviId.getDisplayID(), svp.getDisplayID(), direction, nameInMath, molecularForm, stoichiometry);
            return a;
          
        }
        
        public SvpActionBuilderImpl builder() {
            SBAction a = build();
            if(a == null) {
                LOGGER.error("Action is null");
                return null;
            }
               
            if( b == null) {
                LOGGER.trace("Builder is null, returning new builder");
                return new SvpActionBuilderImpl(workspaceId, contexts).addAction(a);
            }
                
            if (!b.isBuilt()) {
                LOGGER.trace("Adding action to builder");
                b.addAction(a);
            }
                
            LOGGER.trace("Returning builder");
            return b;
        }
    }
    
}
