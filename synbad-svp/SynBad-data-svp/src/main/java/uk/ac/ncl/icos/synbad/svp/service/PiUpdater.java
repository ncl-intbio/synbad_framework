/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import java.net.URI;
import java.util.*;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.action.FlowActions;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBFlowInstance;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.api.SBSvpUtil;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithParent;


public class PiUpdater extends ASBWorkspaceSubscriber implements SBWorkspaceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PiUpdater.class);
    private SBWorkspace ws;
    private SBIdentityFactory f;

    @Override
    public SBWorkspace getWorkspace() {
        return ws;
    }

    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.ws = workspace;
        this.f = ws.getIdentityFactory();
        this.ws.getDispatcher().subscribe(new PortInstanceUpdaterFilter(), this);
    }

    @Override
    public void close() {
        this.ws.getDispatcher().unsubscribe(this);
        this.ws = null;
        this.f = null;
    }

    @Override
    public void onNodeEvent(SBGraphEvent<URI> evt) {

        URI[] contexts =  ((SBEvtWithContext)evt).getContexts();
        URI objId = evt.getData();

        SBEvtWithParent pEvt = (SBEvtWithParent) evt;

        if(SBPort.class.isAssignableFrom(evt.getDataClass())) {
            Optional<? extends SBFlowObject> flowObj = Optional.ofNullable(ws.getObject((URI)pEvt.getParent(), SBFlowObject.class, contexts));
            if( !flowObj.isPresent()) { 
                LOGGER.error("Could not get flow object\t{} in contexts:\t{}",  f.getIdentity(objId), f.getIdentity(contexts[0]));
            }
            flowObj.ifPresent(fo ->
                SBSvpUtil.getInstances(fo, ASBFlowInstance.class, contexts)
                    .forEach(owner -> updatePortsOf(fo, owner, contexts)));
        } else if (FunctionalComponent.class.isAssignableFrom(evt.getDataClass()) || SvpInstance.class.isAssignableFrom(evt.getDataClass())) {
            FunctionalComponent fc = ws.getObject(objId, FunctionalComponent.class, contexts);
            Optional<? extends SBFlowObject> flowObj = fc.getDefinition().flatMap(cd -> cd.as(Svp.class));
            flowObj.ifPresent(fo -> updatePortsOf(fo, fc, contexts));
        } else if (Interaction.class.isAssignableFrom(evt.getDataClass())|| SviInstance.class.isAssignableFrom(evt.getDataClass())) {
            Interaction interaction = ws.getObject(objId, Interaction.class, contexts);
            Optional<? extends SBFlowObject> flowObj = interaction.as(SviInstance.class).flatMap(i -> i.getDefinition());
            flowObj.ifPresent(fo -> updatePortsOf(fo, interaction, contexts));
        } else if (Module.class.isAssignableFrom(evt.getDataClass()) || SvmInstance.class.isAssignableFrom(evt.getDataClass())) {
            Module module = ws.getObject(objId, Module.class, contexts);
            Optional<? extends SBFlowObject> flowObj = module.getDefinition().flatMap(md -> md.as(SvpModule.class));
            flowObj.ifPresent(fo -> updatePortsOf(fo, module, contexts));
        }
    }

    private void updatePortsOf(SBFlowObject portDefOwner, SBIdentified portInstanceOwner, URI[] contexts) {

        SbolActionBuilder b = new SbolActionBuilder(ws, contexts);
        Set<SBPort> addedPorts = new HashSet<>();

        Collection<SBPort> allPortDefsOfObj = portDefOwner.getPorts();

        if ((!portInstanceOwner.is(SvpInstance.TYPE)) ||
                ws.incomingEdges( portInstanceOwner, SynBadTerms.SynBadHelper.exported, SvpInstance.class, contexts).isEmpty()) {

            Collection<SBPortInstance> ownerPortInstances = SBSvpUtil.getPortInstances(portInstanceOwner);

            // remove any port instances that are not defined by their owner

            ownerPortInstances.stream()
                .filter(pi -> !allPortDefsOfObj.contains(pi.getDefinition()))
                .forEach(pi -> {
                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("Removing port:\t{}:{}", portInstanceOwner.getDisplayId(), pi.getDefinition().getDisplayId());
                    b.removeObject(pi.getIdentity(), pi.getType(), portInstanceOwner.getIdentity(), portInstanceOwner.getType());
                });

            allPortDefsOfObj.stream()
                    .filter((SBPort def) -> ownerPortInstances.stream().map(i -> i.getDefinition()).noneMatch(pidef -> pidef.equals(def)))
                    .filter((SBPort def) -> !addedPorts.contains(def)).forEach(portDefinition -> {
                SBIdentity newInstanceId = SvpIdentityHelper.getPortInstanceIdentity(
                        f.getIdentity(portInstanceOwner.getIdentity()), f.getIdentity(portDefinition.getIdentity()));
                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Adding port:\t{}:{}", portInstanceOwner.getIdentity().toASCIIString(), portDefinition.getDisplayId());
                b.addAction(FlowActions.createPortInstance(
                        newInstanceId.getIdentity(),
                        portInstanceOwner.getIdentity(),
                        getNormalisedInstanceType(portInstanceOwner),
                        portDefinition.getIdentity(),
                        ws,
                        contexts));
                addedPorts.add(portDefinition);
            });
        }

        if (!b.isEmpty()) {
            ws.perform(b.build("PiUpdate [" + portDefOwner.getDisplayId() + "]"));
        }
    }

    public URI getNormalisedInstanceType(SBIdentified obj) {
        if(obj.is(FunctionalComponent.TYPE) || obj.is(SvpInstance.TYPE))
            return URI.create(SvpInstance.TYPE);
        else if(obj.is(Interaction.TYPE) || obj.is(SviInstance.TYPE))
            return URI.create(SviInstance.TYPE);
        else if(obj.is(Module.TYPE) || obj.is(SvmInstance.TYPE))
            return URI.create(SvmInstance.TYPE);
        return null;
    }


    private class PortInstanceUpdaterFilter implements SBEventFilter {

        List<Class> classes = Arrays.asList(
                SBPort.class, 
                FunctionalComponent.class, 
                Module.class, 
                Interaction.class);

        @Override
        public boolean test(SBEvent evt) {
            return evt.getType() == SBGraphEventType.ADDED && 
                SBEvtWithContext.class.isAssignableFrom(evt.getClass()) &&
                classes.stream().anyMatch(c -> c.isAssignableFrom(evt.getDataClass()));
        }
    }
}
