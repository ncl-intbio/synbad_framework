/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.datadef;

import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;

/**
 *
 * @author owengilfellon
 */
public enum SynBadPortType implements PortType {

    DNA(UriHelper.synbad.namespacedUri("signal/dna")),
    mRNA(UriHelper.synbad.namespacedUri("signal/mrna")),
    PoPS(UriHelper.synbad.namespacedUri("signal/pops")),
    RiPS(UriHelper.synbad.namespacedUri("signal/rips")),
    Protein(UriHelper.synbad.namespacedUri("signal/protein")),
    Complex(UriHelper.synbad.namespacedUri("signal/complex")),
    SmallMolecule(UriHelper.synbad.namespacedUri("signal/smallMolecule"));


    @Override
    public URI getUri() {
        return uri;
    }
    private final URI uri;
    

    private SynBadPortType(URI uri) {
        this.uri = uri;
    }   


}

