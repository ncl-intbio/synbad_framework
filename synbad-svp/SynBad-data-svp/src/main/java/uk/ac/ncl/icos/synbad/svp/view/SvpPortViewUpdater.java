/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view;

import java.net.URI;
import java.util.*;
import java.util.Collection;
import java.util.stream.Collectors;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.DefaultSBProxyPort;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.tree.ITree;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.tree.ITreeNode;
import uk.ac.ncl.icos.synbad.view.AViewUpdater;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithParent;

/**
 *
 * @author owengilfellon
 */
public final class SvpPortViewUpdater<V extends SvpPView> extends AViewUpdater<SBWorkspaceGraph, V> implements SBSubscriber {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpPortViewUpdater.class);
    
    private static final List<String> OUT_PREDICATES = Arrays.asList(
        SynBadTerms.Sbol.definedBy,
        SynBadTerms.SbolModule.hasInteraction,
        SynBadTerms.SbolModule.hasFunctionalComponent,
        SynBadTerms.SbolModule.hasInteraction,
        SynBadTerms.SbolModule.hasModule,
        SynBadTerms.SynBadPortInstance.hasPortInstance
    );

    private final SBWorkspace ws;
    private final Multimap<URI, SvpWire> wireIdToInstance = HashMultimap.create();
    
    public SvpPortViewUpdater(V target, SBIdentified root, SBWorkspace workspace) {
        super(new SBWorkspaceGraph(workspace, root.getContexts()), target, root);
        this.ws = workspace;
        workspace.getDispatcher().subscribe(this::acceptObject, this);
        doWires(processTree(SBGraphEventType.ADDED, new UpdaterContext(getTreeFromNode(root))));
        target.nodeSet().stream().forEach(target::contract);
    }

    private Set<SBWire> processTree(SBGraphEventType type, UpdaterContext context) {

        LOGGER.debug("SvpViewPortViewUpdater: Processing tree: [{}:{}]", type.getType(), context.getTree().getRootNode().getData());

        Iterator<Object> it = context.getTree().iterator();
        Set<SBWire> wires = new HashSet<>();
        
        while(it.hasNext()) {

            Object o = it.next();

            if(SBIdentified.class.isAssignableFrom(o.getClass())) {
                SBIdentified identified = (SBIdentified) o;
                processIdentified(identified, type, wires);
           }
        }

        return wires;
    }

    private Set<SBWire> processIdentified(SBIdentified identified, SBGraphEventType type, Set<SBWire> wires) {

        if(identified.is(SvpModule.TYPE) && target.getRoot().getObject().getIdentity().equals(identified.getIdentity()))
            wires.addAll(identified.as(SvpModule.class).map(svm -> processInstance(type, svm)).orElse(Collections.EMPTY_SET));

        else if(identified.is(SvmInstance.TYPE))
            wires.addAll(identified.as(SvmInstance.class).map(svm -> processInstance(type, svm)).orElse(Collections.EMPTY_SET));

        else if(identified.is(SviInstance.TYPE))
            identified.as(SviInstance.class).ifPresent(svm -> processInstance(type, svm));

        else if(identified.is(SvpInstance.TYPE))
            identified.as(SvpInstance.class).ifPresent(svm -> processInstance(type, svm));

        else if(identified.is(FunctionalComponent.TYPE))
            identified.as(FunctionalComponent.class).ifPresent(svm -> processInstance(type, svm));

        else if(identified.is(DefaultSBPortInstance.TYPE))
            identified.as(SBPortInstance.class).ifPresent(svm -> processInstance(type, svm));

        return wires;
    }

    private void doWires(Set<SBWire> wires) {

        for(SBWire wire : wires) {

            // Get owners of the ports specified in wire
            SBPortInstance from = wire.getFrom();
            SBPortInstance to = wire.getTo();

            if(from == null) {
                LOGGER.error("Could not get source port instance from: {}", wire.getDisplayId());
            }

            if(to == null) {
                LOGGER.error("Could not get target port instance from: {}", wire.getDisplayId());
            }

            SBIdentified fromOwner = from.getOwner();
            SBIdentified toOwner = to.getOwner();

            // Find view objects of participants of wire

            target.findPortNodes(wire.getFrom()).stream()
                .map(p ->  target.getParent(target.getPortOwner(p)))
                .forEach(parentOfOwner ->  {

                // within the parents of the port owners...

                List<SvVo> siblings = target.getAllChildren(parentOfOwner);

                // ... identify the two ports ...

                Optional<SvpViewPort> fromPort = siblings.stream()
                    .filter(c -> c.getIdentity().equals(fromOwner.getIdentity()))
                    .flatMap(voFrom -> voFrom.getPorts().stream())
                    .filter( p -> p.getIdentity().equals(wire.getFrom().getIdentity()))
                    .findFirst();

                Optional<SvpViewPort> toPort =  siblings.stream()
                     .filter(c -> c.getIdentity().equals(toOwner.getIdentity()))
                     .flatMap(voTo -> voTo.getPorts().stream())
                     .filter( p -> p.getIdentity().equals(wire.getTo().getIdentity()))
                     .findFirst();

                // ... and create edge

                if(!fromPort.isPresent()) {
                    LOGGER.error("Cannot create edge, {} is null", from.getDisplayId());
                } else if(!toPort.isPresent()) {
                    LOGGER.error("Cannot create edge, {} is null", to.getDisplayId());
                } else {
                    SvpWire edge = new SvpWire(
                        new SynBadEdge(
                            SBIdentityHelper.getURI(SBWire.TYPE),
                            fromOwner.getIdentity(),
                            toOwner.getIdentity(), wire.getContexts()),
                        fromPort.get(), toPort.get(), target);

                    target.addEdgeByPort(fromPort.get(), toPort.get(), edge );
                    wireIdToInstance.put(wire.getIdentity(), edge);

                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("ADD wire {} -> {}", fromOwner.getDisplayId(), toOwner.getDisplayId());
                }
            });
        }
    }

    // ========================================================================
    //                      Object processing
    // ========================================================================



    private void processInstance(SBEventType type, SBPortInstance portInstance) {

        SBIdentified owner = portInstance.getOwner();

        if (type == SBGraphEventType.ADDED || type == SBGraphEventType.VISIBLE) {
            for (SvVo ownerNode : target.findObjects(owner.getIdentity())) {

                // Add port

                if(ownerNode.getPorts(portInstance.getPortDirection()).stream()
                        .noneMatch(p -> p.getIdentity().equals(portInstance.getIdentity()))) {

                    SvpViewPort port = new SvpViewPort(portInstance, target);
                    target.addPort(port,
                            ownerNode,
                            portInstance.getPortDirection(),
                            Collections.EMPTY_SET);

                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("ADD port {} to {} in {}", port.getIdentity().toASCIIString(),  ownerNode.getIdentity().toASCIIString(), target.getId());

                    SvVo parent = target.getParent(ownerNode);

                    // Add links to any proxied ports

                    if (port.getObject().isProxy()) {
                        Optional<SBPort> proxiedDef = port.getObject().as(DefaultSBProxyPort.class)
                                .map(DefaultSBProxyPort::getProxied);
                        proxiedDef.ifPresent(pd -> {
                            target.getAllChildren(ownerNode).stream().flatMap(on -> on.getPorts().stream())
                                    .filter(p -> p.getObject().getDefinition().getIdentity().equals(pd.getIdentity()))
                                    .forEach(p -> target.setProxy(port, p));
                        });
                    }
                }
            }
        } else if (type == SBGraphEventType.REMOVED || type == SBGraphEventType.HIDDEN) {

            for (SvVo ownerNode : target.findObjects(owner.getIdentity())) {

                Optional<SvpViewPort> port = ownerNode.getPorts().stream()
                        .filter(svp -> svp.getIdentity().equals(portInstance.getIdentity()))
                        .findFirst();

                port.ifPresent(p -> {
                    SvpViewPort parentProxy = p.getProxy();
                    if (parentProxy != null) {
                        target.removeProxy(parentProxy, p);
                    }

                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("REM port {} from {} in {}", p.getDisplayId(),  ownerNode.getDisplayId(), target.getId());
                    target.removePort(p, ownerNode);
                });
            }
        }
    }

    private Collection<SBWire> processInstance(SBEventType type, SvmInstance svmInstance) {

        doObject(type, svmInstance);

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing SvmInstance: {}", svmInstance.getIdentity().toASCIIString());

        return svmInstance.getDefinition().flatMap(svm -> {
            if(type == SBGraphEventType.ADDED || type == SBGraphEventType.VISIBLE)
                return Optional.of(svm.getWires());
            else return Optional.empty();
        }).orElse(Collections.EMPTY_LIST);


    }

    private Set<SBWire> processInstance(SBEventType type, SvpModule module) {
        return new HashSet<>(module.getWires());
    }

    private void processInstance(SBEventType type, SviInstance svpInteraction) {
        Optional<SvpInteraction> svi = svpInteraction.getDefinition();
        svi.ifPresent(definition -> {
            //if(!definition.isInternal()) {
                doObject(type, svpInteraction);
            //}
        });
    }

    private void processInstance(SBEventType type, FunctionalComponent functionalComponent) {


        if(LOGGER.isTraceEnabled())
            LOGGER.trace("SvpViewPortViewUpdater: Processing functional component: {}", functionalComponent.getDisplayId());

        if(isFunctionalComponentOfSvmTu(functionalComponent)) {
            return;
        } else if (!functionalComponent.getDefinition().map(cd -> cd.getSbolTypes().contains(ComponentType.DNA)).orElse(Boolean.FALSE)) {
            return;
        } else if (!ws.incomingEdges(functionalComponent, SynBadTerms.SynBadHelper.exported, SvpInstance.class, functionalComponent.getContexts()).isEmpty()){
            return;
        }

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing FC: {}", functionalComponent.getIdentity().toASCIIString());

        doObject(type, functionalComponent);
    }
    
    private void processInstance(SBEventType type, SvpInstance svpInstance) {

        if (!svpInstance.getDefinition().map(cd -> cd.getSbolTypes().contains(ComponentType.DNA)).orElse(Boolean.FALSE)) {
            return;
        } else if (!ws.incomingEdges(svpInstance, SynBadTerms.SynBadHelper.exported, SvpInstance.class, svpInstance.getContexts()).isEmpty()){
            return;
        }

        doObject(type, svpInstance);
    }

    private void doObject(SBEventType type, SBIdentified obj) {
        if(type == SBGraphEventType.ADDED || type == SBGraphEventType.VISIBLE)
            createObjectAndAddToOwners(obj);
        else if (type == SBGraphEventType.REMOVED || type == SBGraphEventType.HIDDEN)
            removeObjectFromOwners(obj);
    }

    // ========================================================================
    //                      Event handling
    // ========================================================================

    private boolean acceptObject(SBEvent e) {
        return SBGraphEvent.class.isAssignableFrom(e.getClass()) &&
            Arrays.equals(((SBEvtWithContext)e).getContexts(), root.getContexts()) &&
            ((SBGraphEvent)e).getGraphEntityType() == SBGraphEntityType.NODE && (
                Module.class.isAssignableFrom(e.getDataClass()) ||
                SvmInstance.class.isAssignableFrom(e.getDataClass()) ||
                Interaction.class.isAssignableFrom(e.getDataClass()) ||
                SviInstance.class.isAssignableFrom(e.getDataClass()) ||
                FunctionalComponent.class.isAssignableFrom(e.getDataClass()) ||
                SvpInstance.class.isAssignableFrom(e.getDataClass()) ||
                SBWire.class.isAssignableFrom(e.getDataClass()) ||
                SBPortInstance.class.isAssignableFrom(e.getDataClass()));
    }

    @Override
    public void onEvent(SBEvent e) {
        
        SBGraphEvent<URI> evt = (SBGraphEvent<URI>) e;
        
       // LOGGER.debug("EVT: {}", e);

        URI[] contexts = ((SBEvtWithContext)e).getContexts();
        
        if(e.getType() == SBGraphEventType.ADDED && SBWire.class.isAssignableFrom(e.getDataClass())) {

            SBWire wire = ws.getObject(evt.getData(), SBWire.class, contexts);
            if(wire == null) {
                LOGGER.error("Wire is null: {}", e.getData());
            } else {
                doWires(Collections.singleton(wire));
            }

         } else if(SBEvtWithParent.class.isAssignableFrom(e.getClass())) {
           
            SBEvtWithParent<URI> evtp = (SBEvtWithParent) e;
            URI parentId = evtp.getParent();
            
            if(parentId != null) {

                SBIdentified child = ws.getObject(evt.getData(), SBIdentified.class, contexts);

                if( child != null ) {
                    ITree<Object> tree = getTreeFromNode(child);
                    processTree((SBGraphEventType)e.getType(), new UpdaterContext(tree));
                   // processIdentified(child, SBGraphEventType.ADDED, new HashSet<>());
                } else if (e.getType() == SBGraphEventType.REMOVED ) {
                    if(SBWire.class.isAssignableFrom(e.getDataClass())) {
                        if(LOGGER.isDebugEnabled())
                            LOGGER.debug("REM wire: {}", evt.getData().toASCIIString());
                        target.removeAllEdges(wireIdToInstance.get(evt.getData()));
                        wireIdToInstance.removeAll(evt.getData());
                    } else if (SBPortInstance.class.isAssignableFrom(e.getDataClass())) {

                    } else {
                        Set<SvVo> cs = target.findObjects(evt.getData());
                        for(SvVo c : cs) {
                            if(LOGGER.isDebugEnabled())
                                LOGGER.debug("REM obj {} from {} in {}", c.getDisplayId(), c.getParent().getDisplayId(), target.getId());
                           target.removeNode(c);
                        }
                    }
                }
            }
        }
    }

    // ========================================================================
    //                      Helper methods
    // ========================================================================
    
    private void createObjectAndAddToOwners(SBIdentified object) {

        Collection<SvVo> ownerNodes = getOwnerNodes(object);

        ownerNodes.removeAll(target.findObjects(object.getIdentity()).stream().map(SvVo::getParent).collect(Collectors.toSet()));
        
        if(ownerNodes.isEmpty() && LOGGER.isTraceEnabled())
            LOGGER.trace("No owner nodes found: {}", object.getDisplayId());
        
        for(SvVo node : ownerNodes) {
            SvVo instanceNode = target.createViewObject(object);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("ADD obj {} to {} in {}", instanceNode.getIdentity().toASCIIString(),  node.getIdentity().toASCIIString(), target.getId());
            target.addNode(instanceNode, node);
        }
    }

    private void removeObjectFromOwners(SBIdentified object) {

        Collection<SvVo> ownerNodes = getOwnerNodes(object);

        for(SvVo node : ownerNodes) {
            SvVo toRemove = target.getChildren(node).stream()
                .filter(n -> n.getObject().getIdentity().equals(object.getIdentity()))
                .findFirst().orElse(null);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("REM obj {} from {} in {}", object.getDisplayId(), toRemove.getParent().getDisplayId(), target.getId());
            if(toRemove != null)
                target.removeNode((SvVo)toRemove);
        } 
    }
  
    private Set<SBIdentified> getFirstAncestor(SBIdentified instance, UpdaterContext context, String type){
        ITree<Object> tree = context.getTree();
        return tree.findAllNodes(instance)
                .stream().map(tn -> getAncestor(tn, instance, type))
                .collect(Collectors.toSet());
    }


    private SBIdentified getAncestor(ITreeNode<Object> treeInstance, SBIdentified instance, String type) {
        if(treeInstance == null)
            return null;

        while((treeInstance.getData().equals(instance) || !SBValued.class.isAssignableFrom(treeInstance.getData().getClass()) ||
                !((SBValued) treeInstance.getData()).is(type)) && treeInstance.getParent() != null) {
            treeInstance = treeInstance.getParent();
        }

        if((SBValued.class.isAssignableFrom(treeInstance.getData().getClass()) &&
                ((SBValued)treeInstance.getData()).is(type))) {
            return (SBIdentified)treeInstance.getData();
        }

        return null;
    }
    
    private Set<SBIdentified> getSbolOwnerOfInstance(SBIdentified instance) {
  
       Set<SBIdentified> owners =  getSourceGraph().getTraversal().v(instance)
            .e(SBDirection.IN,
                    SynBadTerms.SbolModule.hasFunctionalComponent,
                    SynBadTerms.SbolModule.hasInteraction,
                    SynBadTerms.SbolModule.hasModule,
                    SynBadTerms.SynBadPortInstance.hasPortInstance)
            .v(SBDirection.IN, SBIdentified.class).getResult()
               .streamVertexes().map(v -> (SBIdentified)v).collect(Collectors.toSet());

       SBIdentified owner = owners.stream().findFirst().orElse(null);
        
        // if owner is root, use ModuleDefinition
       
        if(owner != null && owner.getIdentity().equals(root.getIdentity())) {
            return Collections.singleton(owner); 
        }
        
        // if is owner of port instance, return
        
        if(SBPort.class.isAssignableFrom(instance.getClass()) && owner != null)
            return Collections.singleton(owner);

        // Check for first module ancestor
        
        owners = getFirstAncestor(instance, new UpdaterContext(getTreeFromNode(root)), Module.TYPE);
         
        if(!owners.isEmpty()) {
            return owners;
        }
           
        // otherwise, add to instances of definition in graph

        return getSourceGraph().getTraversal().v(instance)
            .e(SBDirection.IN, SynBadTerms.Sbol.definedBy)
            .v(SBDirection.IN)
            .filter(i -> {
              boolean b = !target.findObjects(i.getIdentity()).isEmpty();
              return b;
            }).getResult().streamVertexes().map(v -> (SBIdentified) v).collect(Collectors.toSet());
    }

    private boolean isFunctionalComponentOfSvmTu(FunctionalComponent instance) {
        return (getSourceGraph().getTraversal()
                    .v(instance)
                    .e(SBDirection.OUT, SynBadTerms.Sbol.definedBy)
                    .v(SBDirection.OUT)
                    .e(SBDirection.OUT, SynBadTerms.SynBadModule.instancedTuOf)
                    .v(SBDirection.OUT).getResult().stream().count() > 0);
    }

    private Collection<SvVo>  getOwnerNodes(SBIdentified instance) {

        Set<SBIdentified> ownerDefinitons = getSbolOwnerOfInstance(instance);

        if(ownerDefinitons.isEmpty()) {
            LOGGER.warn("Owner def is null: {}", instance);
            return Collections.EMPTY_LIST;
        }

        Collection<SvVo> vo = ownerDefinitons.stream()
            .flatMap(owner -> target.findObjects(owner.getIdentity(), SBIdentified.class).stream())
            .collect(Collectors.toSet());

        return vo;
    }

    @Override
    public void close() {
        getSourceGraph().close();
        wireIdToInstance.clear();
    }

    @Override
    public Collection<String> getHierarchyPredicates() {
        return OUT_PREDICATES;
    }
    
    private class UpdaterContext {
        
        private final ITree<Object> tree;

        public UpdaterContext(ITree<Object> tree) {
            this.tree = tree;
        }

        public ITree<Object> getTree() {
            return tree;
        }        
    }
}