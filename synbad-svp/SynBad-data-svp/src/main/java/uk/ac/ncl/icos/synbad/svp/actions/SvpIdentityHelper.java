/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class SvpIdentityHelper {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(SvpIdentityHelper.class);
    
    public static SBIdentity getComponentIdentity(SBIdentity identity, ComponentType type, SynBadPortState state) {
        
        if(state == SynBadPortState.Phosphorylated) {
            if(type != ComponentType.Protein)
                return null;
            return SvpIdentityHelper.getPhosphorylatedProteinIdentity(identity);
        }
          
        switch(type) {
            case DNA:
                return identity;//SvpIdentityHelper.getDnaIdentity(identity);
            case mRNA:
                return SvpIdentityHelper.getMrnaIdentity(identity);
            case Complex:
                return SvpIdentityHelper.getComplexIdentity(identity);
            case Protein:
                return SvpIdentityHelper.getProteinIdentity(identity);
            case SmallMolecule:
                return SvpIdentityHelper.getSmallMolculeIdentity(identity);
            default:
                return identity;
        }
    }

    
    
    
    public static SBIdentity createActivationIdentity(
            SBIdentity activator, 
            ComponentType type, 
            SynBadPortState state, 
            SBIdentity activated) {
        
        SBIdentity activatorid = getComponentIdentity(activator, type, state);
        return  SBIdentity.getIdentity(
                activated.getUriPrefix(), 
                activatorid.getDisplayID() + "_activates_" +  activated.getDisplayID(),
                activated.getVersion());

    }
    
     public static SBIdentity createRepressionIdentity(
            SBIdentity repressor, 
            ComponentType type, 
            SynBadPortState state, 
            SBIdentity repressed) {

            SBIdentity repressedId = getComponentIdentity(repressor, type, state);
            return  SBIdentity.getIdentity(repressed.getUriPrefix(), 
                        repressedId.getDisplayID() + "_represses_" + repressed.getDisplayID(), 
                        repressed.getVersion());

    }
     
    public static SBIdentity createExternalPhosphorylationIdentity(
            SBIdentity svp1, 
            SBIdentity svp2) {

            SBIdentity svp1Id = getComponentIdentity(svp1, ComponentType.Protein, SynBadPortState.Phosphorylated);
            SBIdentity svp2Id = getComponentIdentity(svp2, ComponentType.Protein, SynBadPortState.Default);
            return  SBIdentity.getIdentity(svp1Id.getUriPrefix(), 
                        svp1Id.getDisplayID() + "_phos_" + svp2Id.getDisplayID(), 
                        svp1Id.getVersion());

    }

    public static SBIdentity createSmlMolPhosphorylationIdentity(
            SBIdentity svp1,
            SBIdentity svp2) {

            SBIdentity svp1Id = svp1;
            SBIdentity svp2Id = getComponentIdentity(svp2, ComponentType.Protein, SynBadPortState.Default);
            return  SBIdentity.getIdentity(svp1Id.getUriPrefix(),
                    svp1Id.getDisplayID() + "_phos_" + svp2Id.getDisplayID(),
                    svp1Id.getVersion());

    }
    
    public static SBIdentity createExternalComplexationIdentity(
            SBIdentity svp1, 
            SBIdentity svp2) {

            return  SBIdentity.getIdentity(svp1.getUriPrefix(), 
                        svp1.getDisplayID() + "_binds_" + svp2.getDisplayID(), 
                        svp1.getVersion());

    }
     
     
    /*
    public static SBIdentity createActivationIdentity(SBIdentity activator, SBIdentity activated) {
        try {
            return  SBIdentity.getIdentity(
                    activated.getUriPrefix(), 
                    activator.getDisplayID() + "_act_" + activated.getDisplayID(), 
                    activated.getVersion());
        } catch (SBIdentityException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }*/
    
    public static SBIdentity getParameterIdentity(SBIdentity parentId, String parameterName) {

            return SBIdentity.getIdentity(parentId.getUriPrefix(), parentId.getDisplayID(), "param_" + parameterName, parentId.getVersion());

    }

    public static SBIdentity getMrnaIdentity(SBIdentity svpIdentity) {
        return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), "cd_" + svpIdentity.getDisplayID() + "_mrna", svpIdentity.getVersion());

    }
    
    public static SBIdentity getProteinIdentity(SBIdentity svpIdentity) {
        return SBIdentity.getIdentity(
                svpIdentity.getUriPrefix(), "cd_" + svpIdentity.getDisplayID() + "_protein", svpIdentity.getVersion());
    }
    
    public static SBIdentity getComplexIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), "cd_" + svpIdentity.getDisplayID() + "_complex", svpIdentity.getVersion());

    }
    
    public static SBIdentity getPhosphorylatedProteinIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), "cd_" + svpIdentity.getDisplayID() + "_p_protein", svpIdentity.getVersion());

    }

    public static SBIdentity getSmallMolculeIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), "cd_" + svpIdentity.getDisplayID() + "_smlMol", svpIdentity.getVersion());

    }
    
    
    public static SBIdentity getProductionIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), svpIdentity.getDisplayID() + "_Production", svpIdentity.getVersion());

    }
    
    public static SBIdentity getDegradationIdentity(SBIdentity svpIdentity, SynBadPortState state) {
        
        // Needs to be in this format to write correctly to VPR parts
        
        String s = state == null ? "" : 
                state == SynBadPortState.Phosphorylated ? "Phosphorylated_" : "";
            

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(),  s+ svpIdentity.getDisplayID()  + "_Degradation", svpIdentity.getVersion());

    }
    
    public static SBIdentity getComplexFormationIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), svpIdentity.getDisplayID() + "_Formation", svpIdentity.getVersion());

    }
    
    public static SBIdentity getComplexDisassociationIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), svpIdentity.getDisplayID() + "_Disassociation", svpIdentity.getVersion());

    }
    
    public static SBIdentity getPhosphorylationInteractionIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), svpIdentity.getDisplayID() + "_Phosphorylation", svpIdentity.getVersion());

    }
    
    public static SBIdentity getDePhosphorylationInteractionIdentity(SBIdentity svpIdentity) {

            return SBIdentity.getIdentity(svpIdentity.getUriPrefix(), svpIdentity.getDisplayID() + "_P_Dephosphorylation", svpIdentity.getVersion());

    }

    public static SBIdentity getPortIdentity(SBIdentity owner, SBIdentity participant, SBPortDirection direction, PortType type, PortState state) {

            String portState = state == null || state == SynBadPortState.Default ? "" : "_" + state.toString().charAt(0);
           
            String portParticipant = participant == null ? "" : "_" + 
                (SBIdentityHelper.isValidChildPersistentId(participant.getIdentity()) ? 
                    SBIdentityHelper.getChildDisplayIdFromId(participant.getIdentity()) :
                    participant.getDisplayID());
           
            String childDisplayId = SBIdentityHelper.isValidChildPersistentId(owner.getIdentity()) ?
                    SBIdentityHelper.getChildDisplayIdFromId(owner.getIdentity()) :
                    owner.getDisplayID();
            
            SBIdentity identity =  SBIdentity.getIdentity(
                    owner.getUriPrefix(), 
                    childDisplayId,
                    direction + "_" + type + portParticipant + portState,
                    owner.getVersion());
            return identity;

    }
    /*
     public static SBIdentity getProxyPortIdentity(
             SBIdentity owner, 
             URI identityConstraint, 
             SBPortDirection direction, PortType type, PortState state) {
        

            String portState = state  == null || state == SynBadPortState.Default ? "" : "_" + state.toString().charAt(0);

         String portParticipant = null;
         try {
             portParticipant = identityConstraint == null ? "" :
                 (SBIdentityHelper.isValidChildPersistentId(identityConstraint) ?
                     SBIdentityHelper.getChildDisplayIdFromId(identityConstraint) + "_" :
                     SBIdentity.getIdentity(identityConstraint).getDisplayID()) + "_";
         } catch (SBIdentityException e) {
             e.printStackTrace();
         }

         String childDisplayId = SBIdentityHelper.isValidChildPersistentId(owner.getIdentity()) ?
                    SBIdentityHelper.getChildDisplayIdFromId(owner.getIdentity()) :
                    owner.getDisplayID();
            
            SBIdentity identity =  SBIdentity.getIdentity(
                    owner.getUriPrefix(), 
                    childDisplayId,
                    "p_" + childDisplayId + "_" + portParticipant   + portState,
                    owner.getVersion());
            return identity;

    }*/
    
    public static SBIdentity getProxyPortIdentity(SBIdentity owner, SBIdentity port) {

            SBIdentity identity =  SBIdentity.getIdentity(
                    owner.getUriPrefix(),
                    owner.getDisplayID(),
                    port.getDisplayID().replaceAll("/", "_"),
                    owner.getVersion());

            return identity;

    }
    
    public static SBIdentity getPortInstanceIdentity(SBIdentity owner, SBIdentity portDef) {

            
            String parentDisplayId = //SBIdentityHelper.isValidChildPersistentId(owner.getIdentity()) ?
                //SBIdentityHelper.getChildDisplayIdFromId(owner.getIdentity()) :
            //    owner.getDisplayID().replaceAll("/", "_");
                    SBIdentityHelper.getParentChildDisplayId(owner).replaceAll("/", "_");
            
            String childDisplayId = SBIdentityHelper.isValidChildPersistentId(portDef.getIdentity()) ? 
                SBIdentityHelper.getChildDisplayIdFromId(portDef.getIdentity()) :
                portDef.getDisplayID();
            
            if(parentDisplayId == null || parentDisplayId.equals("null")) {
                LOGGER.error("Could not retrieve parentDisplayId from {}", owner.getDisplayID());
                return null;
            }
                    
            if(childDisplayId == null) {
                 LOGGER.error("Could not retrieve childDisplayId from {}", portDef.getDisplayID());
                 return null;
            }
               
            SBIdentity portIdentity = SBIdentity.getIdentity(
                    owner.getUriPrefix(),
                    parentDisplayId,
                    childDisplayId,
                    owner.getVersion());
                    
                 
                    return portIdentity;

    }

}
