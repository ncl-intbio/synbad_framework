package uk.ac.ncl.icos.synbad.svp.service;

import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBFlowInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;

import java.net.URI;
import java.util.*;

public class SBWireContext {

    // from event

    private SvpModule wireParent = null;
    private SBPortInstance portInstance = null;
    private ASBFlowInstance portOwner = null;
    private SBWire wire = null;
    private Map<URI, SBPortInstance> ports = new HashMap<>();

    // filled in by updaters

    private Map<URI, SBWire> existingWires = new HashMap<>();
    private Map<URI, SBAction> expectedWires = new HashMap<>();

    public void setWire(SBWire wire) { this.wire = wire; }

    public Optional<SBWire> getWire() { return Optional.ofNullable(wire); }

    public Optional<SBPortInstance> getPortInstance() { return Optional.ofNullable(portInstance); }

    public void setPortInstance(SBPortInstance portInstance) { this.portInstance = portInstance; }

    public Optional<ASBFlowInstance> getPortOwner() { return Optional.ofNullable(portOwner); }

    public void setPortOwner(ASBFlowInstance owner) { this.portOwner = owner; }

    public Optional<SvpModule> getWireParent() {
        return Optional.ofNullable(wireParent);
    }

    public Map<URI, SBAction> getExpectedWires() {
        return expectedWires;
    }

    public Map<URI, SBPortInstance> getPorts() {
        return ports;
    }

    public Map<URI, SBWire> getExistingWires() {
        return existingWires;
    }

    public void putExistingWire(SBWire wire) {
        this.existingWires.put(wire.getIdentity(), wire);
    }

    public void putExpectedWire(URI uri, SBAction action) {
        this.expectedWires.put(uri, action);
    }

    public void putPort(SBPortInstance port) {
        this.ports.put(port.getIdentity(), port);
    }

    public void setWireParent(SvpModule module) {
        this.wireParent = module;
    }

}
