/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.parser;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;

/**
 *
 * @author owengilfellon
 */
public class SbmlWriter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SbmlWriter.class);
    
    private final SVPWriter partWriter;
    private final SVPInteractionWriter interactionParser;
    private final SBWorkspace workspace;

    public SbmlWriter(SBWorkspace workspace, URI[] context)
    {
        partWriter = new SVPWriter(workspace, context);
        interactionParser = new SVPInteractionWriter(workspace, context);
        this.workspace = workspace;
    }
    
    public SBMLDocument getSbmlDocument(SvpModel model) throws Exception {
        SvpPView view = model.getMainView();
        view.nodeSet().stream().forEach(view::expand);
        List<PartBundle> bundles = processSvm(
                view.getRoot(),
                new LinkedList<>(),
                view.getModel().getSvis().stream()
                        .map(svi -> svi.getSvpInteraction().getIdentity())
                        .collect(Collectors.toSet()));
        CompilationDirector director = new CompilationDirector(SVPManager.getRepositoryUrl());
        director.setModelPartBundles(bundles);
        SBMLDocument sbmlOutput = director.getSBML(model.getRootNode().getDisplayId());
        return sbmlOutput;
    }
    
    private List<PartBundle> processSvm(SvmVo svm, List<PartBundle> bundles, Set<URI> permittedInteractions) {
        List<SvVo> components = svm.getOrderedSequenceComponents();
        for(SvVo component : components) {
            if(SvpVo.class.isAssignableFrom(component.getClass())) {
                SvpVo svpVo = (SvpVo)component;
                Svp definition = svpVo.getObject().getDefinition().get();
                PartBundle bundle = partWriter.write((Svp)svpVo.getSvp(), definition.getExternalInteractions().stream()
                        .filter(svi ->  permittedInteractions.contains(svi.getIdentity()))
                        .collect(Collectors.toSet()));
                bundles.add(bundle);
            } else if(SvmVo.class.isAssignableFrom(component.getClass())) {
                processSvm((SvmVo)component, bundles, permittedInteractions);
            }
        }

        // NB: This was causing some issues... think it was added for Complexation interactions?

//        Set<PartBundle> productsToAdd = svm.getSvis().stream().map(svi -> svi.getSvpInteraction()).flatMap(svi -> svi.getParticipants().stream())
//            .filter(p -> p.getInteractionRole().equals(SbolInteractionRole.PRODUCT))
//            .map(p -> p.getSvpParticipant())s
//            .filter(p -> bundles.stream().noneMatch(pb -> pb.getPart().getDisplayName().equals(p.getDisplayId())))
//            .map(p -> partWriter.write(p))
//            .filter(Objects::nonNull)
//            .collect(Collectors.toSet());
//
//        productsToAdd.stream().forEach(pb -> LOGGER.info("Adding from interaction: {}", pb.getPart().getDisplayName()));
//
//        bundles.addAll(productsToAdd);
        return bundles;
    }
}
