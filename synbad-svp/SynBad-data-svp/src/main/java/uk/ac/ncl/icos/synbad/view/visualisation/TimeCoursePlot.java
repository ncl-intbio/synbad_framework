package uk.ac.ncl.icos.synbad.view.visualisation;

import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.markers.SeriesMarkers;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

import javax.swing.*;
import java.sql.Time;

public class TimeCoursePlot {

    private final TimeCourseTraces traces;
    private XYChart chart = null;

    public TimeCoursePlot(TimeCourseTraces traces) {
        this.traces = traces;
    }

    public XYChart getChart() {
        if(chart != null)
            return chart;

        this.chart = new XYChart(1280, 720);
        for(String metabolite : traces.getSpecies()) {
            XYSeries series = chart.addSeries(metabolite, null, traces.getTimeCourse(metabolite));
            series.setMarker(SeriesMarkers.NONE);
        }

        return chart;
    }

    public JPanel getPanel() {

        if(chart == null) {
            getChart();
        }

//        SwingWrapper w = new SwingWrapper<>(chart);
//        w.displayChart();
//        JPanel p = w.getXChartPanel();
//        return p;
        return new XChartPanel<>(chart);

    }
}
