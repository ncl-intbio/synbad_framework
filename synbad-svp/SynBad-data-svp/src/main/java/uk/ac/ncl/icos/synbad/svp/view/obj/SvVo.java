/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;

import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;

/**
 *
 * @author owen
 */
public interface SvVo extends SBViewIdentified<SBIdentified> {
 
    public Set<SvpViewPort> getPorts(SBPortDirection... directions);
    
    public SvmVo getParent();
    
}
