/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.example;

import java.net.URI;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.model.SBModelManager;

/**
 *
 * @author owen
 */
public class ExampleModuleFactory {
        
    private final static Logger LOGGER = LoggerFactory.getLogger(ExampleModuleFactory.class);
    
    public static SvpModel createAutoRegulation(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();
        
        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proX = f.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        SBIdentity cdsB = f.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsB, ComponentType.Protein, SynBadPortState.Default, proX);
        
        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
            .createSink(sink)
                .setCdsIdentity(cdsB).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, sink, null)
            .createActivateUnit(actId)
                .setModulatorOwner(sink)
                .setModulator(cdsB)
                .setModulated(proX).builder();

        ws.perform(b.build());
        
        // Get model and view

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
        return model;
    }

    public static SvpModel createAutoRegulationUsingComplex(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proX = f.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        SBIdentity proY = f.getIdentity(tu1Id.getUriPrefix(), "pY", tu1Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
        SBIdentity cdsC = f.getIdentity(sink.getUriPrefix(), "C", sink.getVersion());
        SBIdentity complexId = f.getIdentity(sink.getUriPrefix(), "AB", sink.getVersion());
        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(complexId, ComponentType.Complex, SynBadPortState.Default, proX);

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
            .createComplexationUnit(tu1Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proY)
                .setCdsIdentity(cdsA)
                .setCds2Identity(cdsB)
                .setComplexIdentity(complexId).builder()
            .createSink(sink)
                .setCdsIdentity(cdsC).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, sink, null);

        ws.perform(b.build());
        SBIdentity innerPortOwner = SvpIdentityHelper.createExternalComplexationIdentity(cdsA, cdsB);

        b = new CelloActionBuilder(ws, CONTEXTS)
            .createActivateUnit(actId)
            .setModulatorOwner(tu1Id)
            .setPortOwner(innerPortOwner)
            .setModulator(complexId)
            .setModulatorType(SynBadPortType.Complex)
            .setModulated(proX).builder();

        ws.perform(b.build());

        // Get model and view

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
    }

    public static SvpModel createAutoRegulationUsingPhosphorylated(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proX = f.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        SBIdentity proY = f.getIdentity(tu1Id.getUriPrefix(), "pY", tu1Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
        SBIdentity cdsC = f.getIdentity(sink.getUriPrefix(), "C", sink.getVersion());
        SBIdentity smlMolId = f.getIdentity(sink.getUriPrefix(), "smlMol", sink.getVersion());
        SBIdentity innerPortOwner = SvpIdentityHelper.createExternalPhosphorylationIdentity(cdsA, cdsB);
        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsB, ComponentType.Protein, SynBadPortState.Phosphorylated, proX);

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
            .createSmlMolPhosUnit(tu1Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proY)
                .setCdsIdentity(cdsA)
                .setCds2Identity(cdsB)
                .setSmlMolIdentity(smlMolId).builder()
            .createSink(sink)
                .setCdsIdentity(cdsC).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, sink, null)
            .createActivateUnit(actId)
                .setModulatorOwner(tu1Id)
                .setPortOwner(innerPortOwner)
                .setModulator(cdsB)
                .setModulatorState(SynBadPortState.Phosphorylated)
                .setModulated(proX).builder();

        ws.perform(b.build());

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
    }

    public static SvpModel createAutoRegulationWithModulator(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proX = f.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        SBIdentity proY = f.getIdentity(tu1Id.getUriPrefix(), "pY", tu1Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(sink.getUriPrefix(), "B", sink.getVersion());
        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsA, ComponentType.Protein, SynBadPortState.Default, proX);

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
            .createUnit(tu1Id, TuActionsFactory.InteractionMode.NEGATIVE)
                .setPromoterIdentity(proY)
                .setCdsIdentity(cdsA).builder()
            .createSink(sink)
                .setCdsIdentity(cdsB).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, sink, null)
            .createActivateUnit(actId)
                .setModulatorOwner(tu1Id)
                .setModulator(cdsA)
                .setModulated(proX).builder();

        ws.perform(b.build());

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
    }

    public static SvpModel createRepressilator(SBWorkspace ws, URI[] CONTEXTS) {
        
        SBIdentityFactory f = ws.getIdentityFactory();
        
        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity tu2Id = f.getIdentity("http://www.synbad.org", "tu2", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proX = f.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        SBIdentity opX = f.getIdentity(generator.getUriPrefix(), "oX", generator.getVersion());
        SBIdentity proY = f.getIdentity(tu1Id.getUriPrefix(), "pY", tu1Id.getVersion());
        SBIdentity proZ = f.getIdentity(tu1Id.getUriPrefix(), "pZ", tu1Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
        SBIdentity cdsC = f.getIdentity(sink.getUriPrefix(), "C", sink.getVersion());
        SBIdentity repressId = SvpIdentityHelper.createRepressionIdentity(cdsC, ComponentType.Protein, SynBadPortState.Default, proX);

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
            .createUnit(tu1Id, TuActionsFactory.InteractionMode.NEGATIVE)
                .setCdsIdentity(cdsA)
                .setPromoterIdentity(proY).builder()
            .createUnit(tu2Id, TuActionsFactory.InteractionMode.NEGATIVE)
                .setCdsIdentity(cdsB)
                .setPromoterIdentity(proZ).builder()
            .createSink(sink)
                .setCdsIdentity(cdsC).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, tu2Id, null)
            .addSvm(rootIdentity, sink, null)
            .createRepressUnit(repressId)
                .setCommonParent(rootIdentity)
                .setModulator(cdsC)
                .setModulatorOwner(sink)
                .setModulated(opX)
                .setModulatedIndex(0)
                .builder();

        ws.perform(b.build());
        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
    }

    public static SvpModel createGeneratorModulatorSink(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity tu2Id = f.getIdentity("http://www.synbad.org", "tu2", "1.0");
        SBIdentity tu3Id = f.getIdentity("http://www.synbad.org", "tu3", "1.0");
        SBIdentity tu4Id = f.getIdentity("http://www.synbad.org", "tu4", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proW = f.getIdentity(generator.getUriPrefix(), "pW", generator.getVersion());
        SBIdentity proX = f.getIdentity(tu1Id.getUriPrefix(), "pX", tu1Id.getVersion());
        SBIdentity proY = f.getIdentity(tu2Id.getUriPrefix(), "pY", tu2Id.getVersion());
        SBIdentity opY = f.getIdentity(tu2Id.getUriPrefix(), "oY", tu2Id.getVersion());
        SBIdentity proZ = f.getIdentity(tu3Id.getUriPrefix(), "pZ", tu3Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(tu2Id.getUriPrefix(), "B", tu2Id.getVersion());
        SBIdentity cdsC = f.getIdentity(tu2Id.getUriPrefix(), "C", tu2Id.getVersion());
        SBIdentity cdsD = f.getIdentity(tu3Id.getUriPrefix(), "D", tu3Id.getVersion());
        SBIdentity cdsE = f.getIdentity(tu3Id.getUriPrefix(), "E", tu3Id.getVersion());
        SBIdentity cdsF = f.getIdentity(tu3Id.getUriPrefix(), "F", tu3Id.getVersion());
        SBIdentity cdsG = f.getIdentity(tu3Id.getUriPrefix(), "G", tu3Id.getVersion());
        SBIdentity smlMolId = f.getIdentity(tu2Id.getUriPrefix(), "sm", tu2Id.getVersion());
        SBIdentity smlMol2Id = f.getIdentity(tu2Id.getUriPrefix(), "sm2", tu2Id.getVersion());
        SBIdentity complexId = f.getIdentity(tu3Id.getUriPrefix(), "DE", tu3Id.getVersion());

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator).builder()
            .createUnit(tu1Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proW)
                .setCdsIdentity(cdsA).builder()
            .createSmlMolPhosUnit(tu2Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proX)
                .setCdsIdentity(cdsB)
                .setCds2Identity(cdsC)
                .setSmlMolIdentity(smlMolId).builder()
            .createComplexationUnit(tu3Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proY)
                .setCdsIdentity(cdsD)
                .setCds2Identity(cdsE)
                .setComplexIdentity(complexId).builder()
            .createSmlMolPhosUnit(tu4Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proZ)
                .setCdsIdentity(cdsF)
                .setCds2Identity(cdsG)
                .setSmlMolIdentity(smlMol2Id).builder()
            .createSink(sink).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, tu2Id, null)
            .addSvm(rootIdentity, tu3Id, null)
            .addSvm(rootIdentity, tu4Id, null)
            .addSvm(rootIdentity, sink, null);

        ws.perform(b.build());

        // Get model and view

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
    }

    public static SvpModel createNegativeAutoRegulationWithModulator(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proX = f.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        SBIdentity opX = f.getIdentity(generator.getUriPrefix(), "oX", generator.getVersion());
        SBIdentity proY = f.getIdentity(tu1Id.getUriPrefix(), "pY", tu1Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(sink.getUriPrefix(), "B", sink.getVersion());
        SBIdentity actId = SvpIdentityHelper.createRepressionIdentity(cdsA, ComponentType.Protein, SynBadPortState.Default, proX);

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
            .createUnit(tu1Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proY)
                .setCdsIdentity(cdsA).builder()
            .createSink(sink)
                .setCdsIdentity(cdsB).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, sink, null)
            .createRepressUnit(actId)
                .setModulatorOwner(tu1Id)
                .setModulator(cdsA)
                .setCommonParent(rootIdentity)
                .setModulated(opX)
                .setModulatedIndex(0).builder();

        ws.perform(b.build());
        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return  Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);

    }

    public static SvpModel createUnitSmlMolComplex(SBWorkspace ws, URI[] CONTEXTS) {

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity rootIdentity = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity generator = f.getIdentity("http://www.synbad.org", "generator", "1.0");
        SBIdentity tu1Id = f.getIdentity("http://www.synbad.org", "tu1", "1.0");
        SBIdentity tu2Id = f.getIdentity("http://www.synbad.org", "tu2", "1.0");
        SBIdentity tu3Id = f.getIdentity("http://www.synbad.org", "tu3", "1.0");
        SBIdentity sink = f.getIdentity("http://www.synbad.org", "sink", "1.0");
        SBIdentity proW = f.getIdentity(generator.getUriPrefix(), "pW", generator.getVersion());
        SBIdentity proX = f.getIdentity(tu1Id.getUriPrefix(), "pX", tu1Id.getVersion());
        SBIdentity proY = f.getIdentity(tu2Id.getUriPrefix(), "pY", tu2Id.getVersion());
        SBIdentity opY = f.getIdentity(tu2Id.getUriPrefix(), "oY", tu2Id.getVersion());
        SBIdentity proZ = f.getIdentity(tu3Id.getUriPrefix(), "pZ", tu3Id.getVersion());
        SBIdentity cdsA = f.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = f.getIdentity(tu2Id.getUriPrefix(), "B", tu2Id.getVersion());
        SBIdentity cdsC = f.getIdentity(tu2Id.getUriPrefix(), "C", tu2Id.getVersion());
        SBIdentity cdsD = f.getIdentity(tu3Id.getUriPrefix(), "D", tu3Id.getVersion());
        SBIdentity cdsE = f.getIdentity(tu3Id.getUriPrefix(), "E", tu3Id.getVersion());
        SBIdentity smlMolId = f.getIdentity(tu2Id.getUriPrefix(), "sm", tu2Id.getVersion());
        SBIdentity complexId = f.getIdentity(tu3Id.getUriPrefix(), "DE", tu3Id.getVersion());

        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsB, ComponentType.Protein, SynBadPortState.Default, proZ);
        SBIdentity innerPortOwner = SvpIdentityHelper.createExternalPhosphorylationIdentity(cdsB, cdsC);
        SBIdentity repressId = SvpIdentityHelper.createRepressionIdentity(complexId, ComponentType.Complex, SynBadPortState.Default, proX);
        SBIdentity innerPortOwner2 = SvpIdentityHelper.createExternalComplexationIdentity(cdsD, cdsE);

        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
            .createSvm(rootIdentity)
            .createGenerator(generator).builder()
            .createUnit(tu1Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proX)
                .setCdsIdentity(cdsA).builder()
            .createSmlMolPhosUnit(tu2Id, TuActionsFactory.InteractionMode.POSITIVE)
                .setPromoterIdentity(proY)
                .setCdsIdentity(cdsB)
                .setCds2Identity(cdsC)
                .setSmlMolIdentity(smlMolId).builder()
            .createComplexationUnit(tu3Id, TuActionsFactory.InteractionMode.NEGATIVE)
                .setPromoterIdentity(proZ)
                .setCdsIdentity(cdsD)
                .setCds2Identity(cdsE)
                .setComplexIdentity(complexId).builder()
            .createSink(sink).builder()
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, tu2Id, null)
            .addSvm(rootIdentity, tu3Id, null)
            .addSvm(rootIdentity, sink, null)
//            .createActivateUnit(actId)
//                .setModulatorOwner(tu2Id)
//                .setPortOwner(innerPortOwner)
//                .setModulator(cdsC)
//                .setModulatorState(SynBadPortState.Phosphorylated)
//                .setModulated(proZ).builder()
//            .createRepressUnit(repressId)
//                .setModulatorOwner(tu2Id)
//                .setModulator(complexId)
//                .setPortOwner(innerPortOwner2)
//                .setModulatorType(SynBadPortType.Complex)
//                .setCommonParent(rootIdentity)
//                .setModulated(opY)
//                .setModulatedIndex(2)
//                .builder()
                ;

        ws.perform(b.build());

        // Get model and view

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        return Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
    }
}
