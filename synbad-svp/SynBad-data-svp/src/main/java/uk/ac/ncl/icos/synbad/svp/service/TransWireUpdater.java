/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import java.util.Arrays;
import java.util.Collection;

import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 * 
 * @author owengilfellon
 */ 
public class TransWireUpdater extends AWireUpdater {
   
    private final Collection<PortType> transTypes = Arrays.asList(
            SynBadPortType.Complex, 
            SynBadPortType.Protein, 
            SynBadPortType.SmallMolecule,
            SynBadPortType.mRNA);

    public TransWireUpdater(SBIdentityFactory f) {
        super(f);
    }

    public void applyToContext(SBWireContext ctx) {

        SBPortInstance pi = ctx.getPortInstance().get();

        if(!transTypes.contains(pi.getPortType()))
            return;

        if(!pi.getOwner().is(Interaction.TYPE))
            return;


        ctx.getPorts().values().stream()
            .filter((SBPortInstance p) -> p.getPortDirection() != pi.getPortDirection())
            .forEach((port) -> testPorts(sortOutInPorts(pi, port), ctx));
    }

    private void testPorts(SBPortInstance[] sortedPorts, SBWireContext ctx) {
        if (sortedPorts[1].getOwner().is(Interaction.TYPE) && sortedPorts[0].isConnectable(sortedPorts[1])) {
            createWire(sortedPorts[0], sortedPorts[1], ctx);
        }
    }
}
