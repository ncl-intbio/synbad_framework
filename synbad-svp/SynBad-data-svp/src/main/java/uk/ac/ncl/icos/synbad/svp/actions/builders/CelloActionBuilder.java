/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.impl.RemoveSequenceChildActionId;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.*;

import java.net.URI;
import java.util.Collections;
import java.util.Set;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.action.FlowActions;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.ASBActionBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.actions.impl.AddSvmAction;
import uk.ac.ncl.icos.synbad.svp.actions.impl.AddSvpAction;
import uk.ac.ncl.icos.synbad.svp.actions.impl.RemoveSequenceChildAction;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateValue;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveEdge;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObject;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.actions.SviExternalActionFactory;
import uk.ac.ncl.icos.synbad.svp.actions.SvpActions;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;

/**
 *
 * @author owengilfellon
 */
public class CelloActionBuilder extends ASBActionBuilder implements SBSvpBuilder, SBSviBuilder, SBCelloBuilder, SBDomainBuilder, SBTuBuilder {
    
    private final DefaultSBDomainBuilder builder;
    
    public CelloActionBuilder(SBWorkspace workspaceId, URI[] contexts) {
        super(workspaceId, contexts);
        this.builder = new DefaultSBDomainBuilder(workspaceId, contexts);
    }
    
    // --------------------------------------------------------


    @Override
    public TranscriptionUnitBuilder createTu(SBIdentity tuId) {
        return new TranscriptionUnitBuilder(this, tuId, getWorkspace(), contexts);
    }


    @Override
    public UnitBuilder createUnit(SBIdentity identity, InteractionMode mode) {
        return new UnitBuilder(this, identity, mode, getWorkspace(), contexts);
    }

    @Override
    public GeneratorUnitBuilder createGenerator(SBIdentity identity) {
        return new GeneratorUnitBuilder(this, identity,  contexts);
    }

    @Override
    public SinkUnitBuilder createSink(SBIdentity identity) {
        return new SinkUnitBuilder(this, identity,  contexts);
    }
    
    @Override
    public SmlMolPhosUnitBuilder createSmlMolPhosUnit(SBIdentity identity, InteractionMode mode) {
        return new SmlMolPhosUnitBuilder(this, identity, mode, contexts);
    }
    
    @Override
    public ComplexationUnitBuilder createComplexationUnit(SBIdentity identity, InteractionMode mode) {
        return new ComplexationUnitBuilder(this, identity, mode, contexts);
    }
    
    @Override
    public ActivationBuilder createActivateUnit(SBIdentity identity) {
        return new ActivationBuilder(this, identity, contexts);
    }

    @Override
    public RepressionBuilder createRepressUnit(SBIdentity identity) {
        return new RepressionBuilder(this, identity, contexts);
    }


    // -------------------------------------------------------
    
    
    
    @Override
    public int getSize() {
        return builder.getSize();
    }
    
    @Override
    public boolean isEmpty() {
        return builder.isEmpty();
    }


    @Override
    public SBAction build(String label) {
        return builder.build(label);
    }

    @Override
    public SBAction build() {
        return builder.build();
    }

    @Override
    public boolean isBuilt() {
        return builder.isBuilt();
    }

    // Top Level
    
    
    public CelloActionBuilder addAction(SBAction action) {
        if(!isBuilt())
            builder.addAction(action);
        return this;
    }
    
  
    @Override
    public CelloActionBuilder createSvp (SBIdentity identity, Set<Role> roles, ComponentType type) {
        
        if(!isBuilt()) {
            SBDataDefManager m  = SBDataDefManager.getManager();
            builder.addAction(Svp.CreateSvp(identity, roles, type, getWorkspace(), getContexts()));
            roles.stream().forEach(r -> {
                ComponentRole role = m.getDefinition(ComponentRole.class, r.getUri().toASCIIString());
                builder.createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partTypeAsRole), SBValue.parseValue(role.getUri()), SBIdentityHelper.getURI(Svp.TYPE));
            });
        }
        
        return this;
    }
    
    @Override
    public CelloActionBuilder createSvi(SBIdentity identity, Set<InteractionType> roles) {
        if(!isBuilt())
            builder.addAction(SvpInteraction.CreateSvi(identity, roles, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createSvm (SBIdentity identity) {

    
            // Create module definition
            
            SBIdentity mdId = identityFactory.getIdentity(identity.getUriPrefix(), "md_" + identity.getDisplayID(), identity.getVersion());
           
            // Create component definition for module's DNA
            
            SBIdentity cdId = identityFactory.getIdentity(identity.getUriPrefix(), "cd_" + identity.getDisplayID() + "_dna", identity.getVersion());
            
            // Create functional component to instance DNA
            
            SBIdentity fcId = FunctionalComponent.getFunctionalComponentIdentity(mdId, cdId);

            if(!isBuilt()) {
                builder.addAction(SvpModule.CreateSvm(identity, getWorkspace(), getContexts()));
                builder.addAction(ModuleDefinition.createModuleDefinition(identity.getIdentity(), getWorkspace(), getContexts()));
                builder.addAction(ComponentDefinition.createComponentDefinition(cdId.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.Generic),  getWorkspace(), getContexts()));
                builder.addAction(FunctionalComponent.createFuncComponent(fcId.getIdentity(), cdId.getIdentity(), identity.getIdentity(), SbolAccess.PUBLIC, SbolDirection.INOUT, getWorkspace(), getContexts()));
                //builder.addAction(SvpActions.AddExtends(identity.getIdentity(), mdId.getIdentity(), getWorkspace(), getContexts()));
                builder.addAction(SvpActions.AddInstanceOfContents(cdId.getIdentity(), identity.getIdentity(), getWorkspace(), getContexts()));
            }
            return this;
  
    }

    
    @Override
    public CelloActionBuilder addSvm (SBIdentity parent, SBIdentity child, SBIdentity precedesComponent) {
        if(!isBuilt()) {
            builder.addAction(new AddSvmAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), precedesComponent == null ? null :precedesComponent.getIdentity(), getWorkspace(), getContexts()));
           // builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        
        return this;
    }
    
    @Override
    public CelloActionBuilder addSvp (SBIdentity parent, SBIdentity child, SBIdentity precedesComponent) {
        if(!isBuilt())  {
            builder.addAction(new AddSvpAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), precedesComponent == null ? null : precedesComponent.getIdentity(), getWorkspace(), getContexts()));
    //        builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        return this;
    }
    
    @Override
    public CelloActionBuilder addSvm (SBIdentity parent, SBIdentity child, int index) {
        if(!isBuilt()) {
            builder.addAction(new AddSvmAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), index, getWorkspace(), getContexts()));
  //          builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        
        return this;
    }
    
    @Override
    public CelloActionBuilder addSvp (SBIdentity parent, SBIdentity child, int index) {
        if(!isBuilt())  {
            builder.addAction(new AddSvpAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), index, getWorkspace(), getContexts()));
 //           builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        return this;
    }
 
    
    
    @Override
    public CelloActionBuilder createWire(SBIdentity identity, SBDefinition parent, SBPortInstance from, SBPortInstance to) {
        if(!isBuilt())
            builder.addAction(FlowActions.createWire(identity, parent, from, to, getContexts()));
        return this;
    }
    
    
    
    @Override
    public CelloActionBuilder createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType) {
        if(!isBuilt())
            builder.addAction(FlowActions.createPort(identity, direction, type, state, identityConstraint, owner, ownerType, getWorkspace(), getContexts()));
        return this;
    }
    



    @Override
    public CelloActionBuilder createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType) {
        if(!isBuilt()) {
            builder.addAction(FlowActions.createProxyPort(identity.getIdentity(), proxiedPort, owner, ownerType, getWorkspace(), getContexts()));
 //           builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, owner, getWorkspace(), getContexts()));
        }
            
        return this;
    }
    

    @Override
    public CelloActionBuilder createActivateByPromoter(SBIdentity identity, ComponentType type, SynBadPortState state, URI modulator, URI promoterId, double km, double n) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreateActivateByPromoterAction(identity, promoterId, type, state, modulator, km, n,  getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createActivateANDNByPromoter(SBIdentity identity, URI promoterId, ComponentType type1, SynBadPortState state1, URI activator,
                                                           ComponentType type2, SynBadPortState state2, URI repressor,
                                                           double kmActivator, double nActivator,
                                                           double kmRepressor, double nRepressor) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreateANDNRegulation(identity, promoterId, type1, state1, activator, type2, state2, repressor, kmActivator, nActivator, kmRepressor, nRepressor,  getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createRepressByOperator(SBIdentity identity, ComponentType type, SynBadPortState state, URI modulator, URI operatorId, double km, double n) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreateRepressByOperatorAction(identity, operatorId, type, state, modulator, km, n, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createCDSWithComplexationSmallMolecule(SBIdentity identity, SBIdentity smallMolecule, SBIdentity complexId, double degradationRate, double complexation, double disassociationRate, double complexDegradationRate) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSWithComplexationSmallMoleculeDefinitionAction(identity, smallMolecule, complexId, getWorkspace(), degradationRate,  complexation, disassociationRate, complexDegradationRate, getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createPhosphorylate(SBIdentity identity, URI phosphorylator, URI phosphorylated, double kf, double kd, double kDeP) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreatePhosphorylationAction(identity,  phosphorylator, phosphorylated, kf, kd, kDeP, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SviExternalActionFactory.ComplexFormationActionBuilder createComplexFormation(SBIdentity complexId) {
        return new SviExternalActionFactory.ComplexFormationActionBuilder(this, complexId, getWorkspace(), getContexts());
    }

    @Override
    public SviExternalActionFactory.ComplexDisassociationActionBuilder createComplexDisassociation(SBIdentity complexId) {
        return new SviExternalActionFactory.ComplexDisassociationActionBuilder(this, complexId, getWorkspace(), getContexts());
    }

    @Override
    public CelloActionBuilder removeObject(URI identity, URI ownerIdentity, URI type, URI ownerType) {
        if(!isBuilt())
            builder.addAction(new RemoveObject(identity, type, ownerIdentity, ownerType, getWorkspace(), getContexts()));
        return this;
    }
    
    
    
    @Override
     public CelloActionBuilder duplicateObject(URI identity, URI[] fromContexts, boolean incrementIds) {
        if(!isBuilt())
            builder.duplicateObject(identity, fromContexts, incrementIds);
        return this;
    }
    
    @Override
    public CelloActionBuilder removeChild(URI parentSvm, int childIndex) {
        if(!isBuilt())
            builder.addAction(new RemoveSequenceChildAction(parentSvm, childIndex, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder removeChild(URI parentSvm, URI childComponentId) {
        if(!isBuilt())
            builder.addAction(new RemoveSequenceChildActionId(parentSvm, childComponentId, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.addAction(new CreateEdge(source, predicate.toASCIIString(), target, getWorkspace().getIdentity(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder removeEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.addAction(new RemoveEdge(source, predicate.toASCIIString(), target, getWorkspace().getIdentity(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.addAction(new CreateValue(source, predicate.toASCIIString(), value, sourceType, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder removeValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.addAction(new RemoveValue(predicate, predicate.toASCIIString(), value, sourceType, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createPromoter(SBIdentity identity, double ktr ) {
        if(!isBuilt())
            builder.addAction(Svp.createPromoter(identity, getWorkspace(), ktr, getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createOperator(SBIdentity identity
                                             //PortType type, PortState state,
                                             //URI identityConstraint
            ) {
        if(!isBuilt())
            builder.addAction(Svp.CreateOperatorDefinitionAction(identity,  getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createUpstreamShim(SBIdentity identity, double efficiency) {
        if(!isBuilt())
            builder.addAction(Svp.CreateUpstreamShimAction(identity, efficiency, getWorkspace(), getContexts()));
        return this;
    }
        
    @Override
    public CelloActionBuilder createDownstreamShim(SBIdentity identity, double efficiency) {
        if(!isBuilt())
            builder.addAction(Svp.CreateDownstreamShimAction(identity, efficiency, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createRBS(SBIdentity identity, double ktl, double volume) {
        if(!isBuilt())
            builder.addAction(Svp.CreateRBSDefinitionAction(identity, getWorkspace(), ktl, volume, getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createComplex(SBIdentity identity, double kd) {
        if(!isBuilt())
            builder.addAction(Svp.CreateComplexDefinitionAction(identity, getWorkspace(), kd, getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createCDSWithPhosphorylatingSmallMolecule(SBIdentity identity, SBIdentity smallMolecule, double degradationRate, double phosphorylationRate, double phosDegradationRate, double dephosphorylationRate) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSWithPhosphorylatingSmallMoleculeDefinitionAction(identity, smallMolecule, getWorkspace(), degradationRate,  phosDegradationRate, phosDegradationRate, dephosphorylationRate, getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createCDSWthPhosphorylated(SBIdentity identity, double degradationRate, double pDegradationRate, double dePhosRate) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSDefinitionWithPhosphorylatedAction(identity, ws, degradationRate, pDegradationRate, dePhosRate, contexts));
        return this;
    }

    
    @Override
    public CelloActionBuilder createCDS(SBIdentity identity, double Kd) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSDefinitionAction(identity, getWorkspace(),Kd, getContexts()));
        return this;
    }
    
    @Override
    public CelloActionBuilder createCompound(SBIdentity identity) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCompoundDefinitionAction(identity, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public CelloActionBuilder createTerminator(SBIdentity identity) {
        if(!isBuilt())
            builder.addAction(Svp.CreateTerminatorDefinitionAction(identity, getWorkspace(), getContexts()));
        return this;
    }
    
     @Override
    public CelloActionBuilder createObject(URI identity, URI rdfType, URI parentIdentity, URI parentRdfType) {
        if(!isBuilt())
            builder.createObject(identity, rdfType, parentIdentity, parentRdfType);
        return this;
    }


    @Override
    public CelloActionBuilder removeObjectAndDependants(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            builder.removeObjectAndDependants(identity, ownerIdentity);
        return this;
    }

    @Override
    public SBSviBuilder asInteractionBuilder() {
        return (SBSviBuilder) this;
    }

    @Override
    public SBSvpBuilder asPartBuilder() {
        return (SBSvpBuilder) this;
    }

}
