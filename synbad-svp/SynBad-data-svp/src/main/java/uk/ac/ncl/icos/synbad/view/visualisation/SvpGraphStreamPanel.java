/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.visualisation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.view.SBPView;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.view.SBHView;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBPTraversal;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.visualisation.GraphStreamPanel;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithParent;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithSrc;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;

/**
 *
 * @author owengilfellon
 */
public class SvpGraphStreamPanel extends GraphStreamPanel<SvVo,  DefaultSBViewEdge<? extends SBViewIdentified<? extends SBIdentified>, ? extends SBIdentified, ? extends SynBadEdge >, SvpPView> {

    static final Logger logger = LoggerFactory.getLogger(SvpGraphStreamPanel.class);
    private String css ="graph {            fill-color: white; } " +
                        "node {             text-color: rgb(85, 81, 123);   text-color:#444; text-style:bold;} "+
                        "node.sbobject {    fill-color: rgb(134, 131, 164);  } " +
                        "node:clicked {     fill-color: rgb(255, 255, 255);} "+
                        "node.instance {    size: 20px; }" +
                        "node.module {      fill-color: rgb(0, 122, 135); }" +
                        "node.component {   fill-color: rgb(174, 164, 68);}" +
                        "node.interaction { fill-color: rgb(255, 0, 255);  }" +
                        "edge {             text-color: rgb(0, 80, 0); text-color:#ddd; arrow-shape: arrow; shape: angle; }";

    public SvpGraphStreamPanel() {
        super();
    }

    @Override
    public void setUpStyles(Graph gsGraph) {
        gsGraph.addAttribute("ui.stylesheet", css);
    }

    @Override
    public void addNodeStyle(SvVo instance, Node node) {
        Set<String> instanceTypes = instance.getValues(SynBadTerms.Rdf.hasType).stream()
                .filter(v -> v.isURI())
                .map(v -> v.asURI().toASCIIString())
                .collect(Collectors.toSet());

         node.addAttribute("ui.label", !instance.getName().isEmpty() ? instance.getName() : instance.getDisplayId());

        if(instanceTypes.contains(ModuleDefinition.TYPE)) {
            node.addAttribute("ui.label", instance.getDisplayId());
            node.addAttribute("ui.class", "moduledefinition, module, definition, sbobject");
        } else if (instanceTypes.contains(Module.TYPE)) {
            node.addAttribute("ui.label", instance.getDisplayId());
            node.addAttribute("ui.class", "module, instance, sbobject");
        } else if (instanceTypes.contains(ComponentDefinition.TYPE)) {
            node.addAttribute("ui.class", "component, componentdefinition, definition, sbobject");
        } else if (instanceTypes.contains(Component.TYPE)) {
            node.addAttribute("ui.class", "component, instance, sbobject");
        } else if (instanceTypes.contains(FunctionalComponent.TYPE)) {
         //   if()
            node.addAttribute("ui.label", instance.getDisplayId());
            node.addAttribute("ui.class", "component, instance, functionalcomponent, sbobject");
                
        } else if(instanceTypes.contains(Interaction.TYPE) ) {
            node.addAttribute("ui.label", instance.getDisplayId());
            node.addAttribute("ui.class", "interaction, sbobject");
        } else {
            node.addAttribute("ui.class", "sbobject ");
        }
 
    }

    @Override
    public void addEdgeStyle(DefaultSBViewEdge<? extends SBViewIdentified<? extends SBIdentified>, ? extends SBIdentified, ? extends SynBadEdge > instance, Edge edge) {
//        super.addEdgeStyle(instance, edge);
//
//        if(instance.getEdge().getEdge().equals(SBIdentityHelper.getURI(SBWire.TYPE))) {
//             edge.addAttribute("ui.style", "size:2px; arrow-shape: arrow; shape:angle;");
//        } else if(instance.getEdge().getEdge().equals(SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild))) {
//             edge.addAttribute("ui.style", "color: #ccc; ");
//        }
//
        
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    
    @Override
    public String getEdgeLabel(DefaultSBViewEdge<? extends SBViewIdentified<? extends SBIdentified>, ? extends SBIdentified, ? extends SynBadEdge > edge) {
        return  edge.getEdge().getEdge().toASCIIString();
    }

    @Override
    public SvVo getEdgeFrom(DefaultSBViewEdge<? extends SBViewIdentified<? extends SBIdentified>, ? extends SBIdentified, ? extends SynBadEdge > edge) {
        if(edge.getEdge().getEdge().equals(SBIdentityHelper.getURI("http://www.synbad.org/view/child")))
            return ((SvVo)edge.getFrom());
        SvVo o = (SvVo)((SBPView)edge.getView()).getPortOwner(edge.getFrom());
        logger.debug("EdgeFrom: {}", o.getIdentity().toASCIIString());
        return o;
    }

    @Override
    public SvVo getEdgeTo(DefaultSBViewEdge<? extends SBViewIdentified<? extends SBIdentified>, ? extends SBIdentified, ? extends SynBadEdge > edge) {
        if(edge.getEdge().getEdge().equals(SBIdentityHelper.getURI("http://www.synbad.org/view/child")))
            return ((SvVo)edge.getTo());
        SvVo o = (SvVo)((SBPView)edge.getView()).getPortOwner(edge.getTo());
        logger.debug("EdgeTo: {}", o.getIdentity().toASCIIString());
        return o;
    }

    public void onNodeEvent(SBGraphEvent<DefaultSBViewIdentified> evt) {
        
        logger.debug("Node evt: {}", evt);
        
        if(!SBEvtWithParent.class.isAssignableFrom(evt.getClass()) || !SBEvtWithSrc.class.isAssignableFrom(evt.getClass()))
            return;
        
        if(SBHView.class.isAssignableFrom(((SBEvtWithSrc)evt).getSourceClass()))
        {
     
            logger.debug("HNode evt: {}", evt);
            if(evt.getType() == SBGraphEventType.ADDED || evt.getType() == SBGraphEventType.VISIBLE || evt.getType() == SBGraphEventType.ADDED) {
                String id = super.addEntity((SvVo)evt.getData());
               
                
                
                DefaultSBViewIdentified<SBIdentified> child = (DefaultSBViewIdentified<SBIdentified>)evt.getData();
                DefaultSBViewIdentified<SBIdentified> parent = (DefaultSBViewIdentified<SBIdentified>)((SBEvtWithParent)evt).getParent();
                if(!child.is(Interaction.TYPE)) {
                    logger.debug("Adding edge: {} -> {}", getPrettyName(parent), getPrettyName(child));
                    super.addEdge(new DefaultSBViewEdge(
                            new SynBadEdge(SBIdentityHelper.getURI("http://www.synbad.org/view/child"), 
                                    parent.getIdentity(), child.getIdentity(), parent.getContexts()), parent, child, getView()));
                }
            }

              else if(evt.getType() == SBGraphEventType.REMOVED || evt.getType() == SBGraphEventType.HIDDEN)
                super.removeEntity((SvVo)evt.getData());
        }
    }

    @Override
    public void onEvent(SBEvent e) {
        logger.debug("Received event: " + e);
        
        if(!SBGraphEvent.class.isAssignableFrom(e.getClass()))
            return;
        
        SBGraphEvent evt = (SBGraphEvent) e;
        
        if(evt.getGraphEntityType() == SBGraphEntityType.NODE) 
        {
            onNodeEvent(evt);
        } 
        else if (evt.getGraphEntityType() == SBGraphEntityType.EDGE)
        {
            onEdgeEvent(evt);
        } 
    }

    public void onEdgeEvent(SBGraphEvent<SBViewEdge> evt) {

        logger.debug("Edge evt: {}", evt);
        if(evt.getType() == SBGraphEventType.ADDED || evt.getType() == SBGraphEventType.ADDED || evt.getType() == SBGraphEventType.VISIBLE) {

            SvVo from = getEdgeFrom((SvpWire)evt.getData());
            SvVo to = getEdgeTo((SvpWire)evt.getData());
            addEntity(from);
            addEntity(to);

            // add edge from resolved from to resolved to

            addEdge((SvpWire)evt.getData());

        } else if(evt.getType() == SBGraphEventType.REMOVED || evt.getType() == SBGraphEventType.REMOVED || evt.getType() == SBGraphEventType.HIDDEN)
            removeEdge((SvpWire)evt.getData());
    }

    @Override
    public void setView(SvVo startNode, SvpPView view) {
        super.setView(startNode, view); 
        gsGraph.clear();
        setUpStyles(gsGraph);
       // setUpPanel();
        //renderPanel();
        intialiseGraph();
        
    }
 
    public void intialiseGraph() {
        
        logger.debug("Initialise graph");
        
        List<SBEvent> events = new ArrayList<>();
        SvpPView view = getView();
        
        List<SvVo> vs = view.getTraversal()
                .v(view.getRoot()).loop(o -> true,
            new DefaultSBPTraversal<>(view).children(), true, false, true)
                .getResult().streamVertexes().map(o -> (SvVo)o).collect(Collectors.toList());

        for(SvVo v :vs) {
            if(!v.equals(view.getRoot())) {
                logger.debug("Constructing node: {}", v);
                SvmVo parent = view.getParent(v);
                events.add(new DefaultSBGraphEvent(
                    SBGraphEventType.ADDED, SBGraphEntityType.NODE,
                    v, v.getClass(),
                    view, view.getClass(),
                    parent, parent.getClass(),
                    parent.getContexts()));
            }
        }
        
       
        events.stream().forEachOrdered(e -> onEvent(e));
        
         view.getAllEdges().stream().map(e ->  {
             logger.debug("Constructing edge: {}", e);
            return new DefaultSBGraphEvent(
                        SBGraphEventType.ADDED, SBGraphEntityType.EDGE, 
                        e, e.getClass(), 
                        view, view.getClass(),
                        null, null,
                        e.getEdge().getContexts());
        }).forEach(e -> onEvent(e));
        
    }
}
