/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SvpVo extends DefaultSBViewIdentified<SBIdentified> implements SBViewComponent<SBIdentified, SvpViewPort>, SvVo {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpVo.class);

    public SvpVo(SvpInstance instance, SvpPView view) {
        super(instance, view);
    }

    public SvpVo(SvpInstance instance, long id, SBView view) {
        super(instance, id, view);
    }

    @Override
    public SvpPView getView() {
        return (SvpPView) super.getView();
    }

    /**
     * All Svp instances should be mapped to a component that defines its physical position
     * @return
     */
    public Component getComponent() {
        return null;
    }

    @Override
    public SvpInstance getObject() {
        return (SvpInstance) super.getObject();
    }

    /**
     * All uses of the SVP wrapped by this SvpVo should be through this method, to ensure fresh data
     * @return
     */
    public Svp getSvp() {

        Optional<Svp> svp = getObject().getDefinition();

        if(!svp.isPresent()) {
            LOGGER.error("Could not retrieve SVP from {}", getObject().getDisplayId());
        }

        return svp.orElse(null);
    }

    @Override
    public String getDescription() { return getSvp().getDescription(); }

    @Override
    public URI getWasDerivedFrom() {
        return getSvp().getWasDerivedFrom();
    }
    
    public ComponentRole getPartTypeAsRole() {
        return getSvp().as(Svp.class).get().getPartTypeAsRole();
    }
    
    @Override
    public String getPrefix() {
        return getSvp().getPrefix();
    }

    @Override
    public String getDisplayId() {
        return getObject().getDisplayId();
    }

    @Override
    public String getVersion() {
        return getSvp().getVersion();
    }

    @Override
    public String getName() {
        return getSvp().getName();
    }

    @Override
    public URI getPersistentId() { return getSvp().getPersistentId(); }

    @Override
    public Set<SvpViewPort> getPorts() {
        return getView().getPorts(this);
    }
    
    @Override
    public Set<SvpViewPort> getPorts(SBPortDirection... directions) {
        Collection<SBPortDirection> portDirections = Arrays.asList(directions);
        return getView().getPorts(this).stream()
                .filter(p -> portDirections.isEmpty() || portDirections.contains(p.getDirection()))
                .collect(Collectors.toSet());
    }
    
    @Override
    public SvmVo getParent() {
        return getView().getParent(this);
    }

    @Override
    public String toString() {
        return getDisplayId();
    }
}
