/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import java.net.URI;
import java.util.Set;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public interface SBSvpBuilder extends SBActionBuilder {

    SBSvpBuilder addSvm(SBIdentity parent, SBIdentity child, SBIdentity precedesComponent);
    
    SBSvpBuilder addSvm(SBIdentity parent, SBIdentity child, int index);

    SBSvpBuilder addSvp(SBIdentity parent, SBIdentity child, SBIdentity precedesComponent);

    SBSvpBuilder addSvp(SBIdentity parent, SBIdentity child, int index);

    //SBSvpBuilder addSvi(SBIdentity parent, SBIdentity child);
    
    SBSvpBuilder createCDS(SBIdentity identity, double Kd);

    SBSvpBuilder createCDSWithPhosphorylatingSmallMolecule(SBIdentity identity, SBIdentity smallMolecule, double degradationRate, double phosphorylationRate, double phosDegradationRate, double dephosphorylationRate);

    SBSvpBuilder createCDSWithComplexationSmallMolecule(SBIdentity identity, SBIdentity smallMolecule, SBIdentity complexId, double degradationRate, double phosphorylationRate, double phosDegradationRate, double dephosphorylationRate);

    SBSvpBuilder createCDSWthPhosphorylated(SBIdentity identity,  double degradationRate, double pDegradationRate, double dePhosRate);
    
    SBSvpBuilder createCompound(SBIdentity identity);

    SBSvpBuilder createDownstreamShim(SBIdentity identity, double efficiency);

    SBSvpBuilder createOperator(SBIdentity identity);

    SBSvpBuilder createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType);

    SBSvpBuilder createPromoter(SBIdentity identity, double ktr);

    SBSvpBuilder createComplex(SBIdentity identity, double kd);

    SBSvpBuilder createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType);

    SBSvpBuilder createRBS(SBIdentity identity, double ktl, double volume);

    SBSvpBuilder createSvi(SBIdentity identity, Set<InteractionType> roles);

    SBSvpBuilder createSvm(SBIdentity identity);

    SBSvpBuilder createSvp(SBIdentity identity, Set<Role> roles, ComponentType type);

    SBSvpBuilder createTerminator(SBIdentity identity);
    
    SBSvpBuilder createUpstreamShim(SBIdentity identity, double efficiency);

    SBSvpBuilder createWire(SBIdentity identity, SBDefinition parent, SBPortInstance from, SBPortInstance to);

    SBSvpBuilder removeChild(URI parentSvm, int childIndex);

    SBSvpBuilder removeChild(URI parentSvm, URI childComponentId);
    
    SBSviBuilder asInteractionBuilder();
    
}
