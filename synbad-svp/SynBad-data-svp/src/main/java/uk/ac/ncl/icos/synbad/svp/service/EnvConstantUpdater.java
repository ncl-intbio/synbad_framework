/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.*;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.SBSubGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;

import java.net.URI;

/**
 *
 * @author owengilfellon
 */
public class EnvConstantUpdater extends ASBWorkspaceSubscriber implements SBWorkspaceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvConstantUpdater.class);
    private SBWorkspace ws;
    private SBIdentityFactory identityFactory;

    public EnvConstantUpdater() {
    }

    private static boolean accept(SBEvent event) {

        if(!SBGraphEvent.class.isAssignableFrom(event.getClass()))
            return false;
        
        SBGraphEvent gEvt = (SBGraphEvent) event;
     
        if(gEvt.getGraphEntityType() != SBGraphEntityType.EDGE)
            return false;

        SynBadEdge edge = (SynBadEdge) gEvt.getData();

        return edge.getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasFunctionalComponent) ||
                edge.getEdge().toASCIIString().equals(SynBadTerms.SynBadPart.hasInternalSpecies);

    }

    @Override
    public void close() {
        this.ws = null;
        this.identityFactory = null;
    }

    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.ws = workspace;
        this.identityFactory = ws.getIdentityFactory();
        this.ws.getDispatcher().subscribe(EnvConstantUpdater::accept, this);
    }

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> evt) {
        processFc(evt.getType(), evt.getData(), ((SBEvtWithContext)evt).getContexts());
        
    }
 
    private void processFc(
            SBGraphEventType type, 
            SynBadEdge edge,
            URI[] contexts) {
        
        URI fcId = edge.getTo();
        
        if(fcId == null)
            return;

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("processFc(): {} {}", type, fcId.toASCIIString());

        SbolActionBuilder b = new SbolActionBuilder(ws, contexts);

        if(type == SBGraphEventType.ADDED && edge.getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasFunctionalComponent)) {

            SvpInstance envConstContainingFc = ws.getObject(fcId, SvpInstance.class, contexts);
            
            if(envConstContainingFc == null)
                return;
            
            Svp svpDef = envConstContainingFc.getDefinition()
                    .map(def -> ws.getObject(def.getIdentity(), Svp.class, contexts)).orElse(null);
            
            if(svpDef == null)
                return;
        
            try (SBWorkspaceGraph g = new SBWorkspaceGraph(ws, contexts)) {
            
                SBTraversalResult<SBIdentified, SBEdge> r = g.getTraversal().v(svpDef)
                    .e(SBDirection.OUT, SynBadTerms.SynBadPart.hasInternalInteraction).v(SBDirection.OUT, SvpInteraction.class)
                    .e(SBDirection.OUT, SynBadTerms.SynBadInteraction.hasParticipant).v(SBDirection.OUT, SvpParticipant.class)
                    .e(SBDirection.OUT, SynBadTerms.SynBadParticipant.isSbolParticipant).v(SBDirection.OUT, ComponentDefinition.class)
                    .filter(n -> n.getSbolTypes().contains(ComponentType.SmallMolecule))
                    .as("PARTICIPANT")
                    .getResult();

                if(r.getTraverserCount() == 0)
                    return;

                SBIdentity parentId = identityFactory.getIdentity(envConstContainingFc.getParent().getIdentity());

                r.getMatches().stream().flatMap(sg -> sg.getLabelledVertices("PARTICIPANT").stream())
                        .map(obj -> (ComponentDefinition) obj)
                        .distinct()
                        .forEach(participant -> {
                            SBIdentity ecDefId = identityFactory.getIdentity(participant.getIdentity());
                            SBIdentity instanceId = identityFactory.getIdentity(parentId.getUriPrefix(), parentId.getDisplayID(), ecDefId.getDisplayID(), parentId.getVersion());
                            if(!ws.containsIdentity(instanceId.getIdentity(), contexts)) {
                                b.createFuncComponent(instanceId.getIdentity(), participant, envConstContainingFc.getParent().as(ModuleDefinition.class).get(), SbolDirection.INOUT, SbolAccess.PUBLIC);
                                b.createEdge(envConstContainingFc.getIdentity(), URI.create(SynBadTerms.SynBadPart.hasInternalSpecies), instanceId.getIdentity());
                                if(LOGGER.isDebugEnabled())
                                    LOGGER.debug("Added EC:\t\t{}", instanceId.getDisplayID());
                            } });
            }
        }

        else if (type == SBGraphEventType.REMOVED) {

            if(edge.getEdge().toASCIIString().equals(SynBadTerms.SynBadPart.hasInternalSpecies) && 
                    ws.containsObject(edge.getTo(), FunctionalComponent.class, contexts)) {
                
                 FunctionalComponent ec = ws.getObject(edge.getTo(), FunctionalComponent.class, contexts);
                 if(ws.incomingEdges(ec, SynBadTerms.SynBadPart.hasInternalSpecies, FunctionalComponent.class, contexts).isEmpty()) {
                    b.removeObjectAndDependants(ec.getIdentity(), edge.getFrom());
                     if(LOGGER.isDebugEnabled())
                         LOGGER.debug("Removed EC:\t\t{}", ec.getDisplayId());
                 }
            }
        }

        if(!b.isEmpty()) {
            ws.perform(b.build("EnvConstUpdater [" + identityFactory.getIdentity(fcId).getDisplayID() + "]"));
        }
    }

    @Override
    public SBWorkspace getWorkspace() {
     return ws;
    }
}
