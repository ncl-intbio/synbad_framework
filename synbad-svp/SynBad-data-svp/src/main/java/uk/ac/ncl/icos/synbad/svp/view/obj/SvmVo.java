/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SvmVo extends DefaultSBViewIdentified<SBIdentified> implements SvVo, SBViewComponent<SBIdentified, SBViewPort> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvmVo.class);

    public SvmVo(SvpModule definition, SvpPView view) {
        super(definition, view);
    }

    public SvmVo(SvmInstance instance, SvpPView view) {
        super(instance, view);
    }

    public SvmVo(SvpModule definition, long id, SBView view) {
        super(definition, id, view);
    }

    public SvmVo(SvmInstance instance, long id, SBView view) {
        super(instance, id, view);
    }

    @Override
    public SBIdentified getObject() {
        return super.getObject();
    }

    public boolean isRoot() {
        return getView().getRoot().equals(this);
    }
    
    @Override
    public SvmVo getParent() {
        return getView().getParent(this);
    }

    @Override
    public SvpPView getView() {
        return (SvpPView)super.getView();
    }

    @Override
    public Set<SvpViewPort> getPorts(SBPortDirection... directions) {
        Collection<SBPortDirection> portDirections = Arrays.asList(directions);
        return getView().getPorts(this).stream()
                .filter(p -> portDirections.isEmpty() || portDirections.contains(p.getDirection()))
                .collect(Collectors.toSet());
    }
    

    public Set<SvpViewPort> getPorts(boolean wired, SBPortDirection... directions) {
        Collection<SBPortDirection> portDirections = Arrays.asList(directions);
        return getView().getPorts(this).stream()
                .filter(p -> portDirections.isEmpty() || portDirections.contains(p.getDirection()))
                .filter(p -> wired ? 
                    !getView().getAllEdgesByPort(p).isEmpty() || p.getProxy() != null : 
                    getView().getAllEdgesByPort(p).isEmpty() && p.getProxy() == null)
                .collect(Collectors.toSet());
    }
    
    
    @Override
    public Set<SBViewPort> getPorts() {
        return getView().getPorts(this).stream()
                .collect(Collectors.toSet());
    }
    
    public Set<SvpWire> getNestedWires() {
        return getView().getChildren(this).stream()
            .map(c -> (SvVo)c)
            .flatMap(c -> c.getPorts(SBPortDirection.OUT).stream())
            .flatMap(p -> getView().getAllEdgesByPort(p).stream())
            .collect(Collectors.toSet());
    }
    
    public Set<SvpViewPort> getAllNestedPorts(SBPortDirection... directions) {
        return getView().getAllChildren(this).stream()
            .map(c -> (SvVo)c)
            .flatMap(c -> c.getPorts(directions).stream())
            .collect(Collectors.toSet());
    }
    
    public Set<SvpViewPort> getNestedPorts(boolean wired, SBPortDirection... directions) {
        return getView().getChildren(this).stream()
            .map(c -> (SvVo)c)
            .flatMap(c -> c.getPorts(directions).stream())
                .filter(p -> wired ? 
                    !getView().getAllEdgesByPort(p).isEmpty() || p.getProxy() != null : 
                    getView().getAllEdgesByPort(p).isEmpty() && p.getProxy() == null)
                .collect(Collectors.toSet());
    }
    
    public List<SvVo> getOrderedSequenceComponents() {

        Map<URI, SvVo> components = getView().getChildren(this).stream().distinct()
                .collect(Collectors.toMap(
                        (t) -> { return t.getObject().getIdentity();},
                        (t) -> { return t;}));

        ModuleDefinition md = getSvpModuleFromWs().as(ModuleDefinition.class).get();

        List<SvVo> orderedComponents = new ArrayList<>();
        getSvpModule().getOrderedInstances().stream().map((instance) -> {
            // If fc maps to a Module
            
            SbolInstance mod = md.getModules().stream()
                    .flatMap(m -> m.getMapsTo().stream())
                    .filter(m -> m.getLocalInstance().equals(instance))
                    .filter(m -> m.getOwner().is(Module.TYPE))
                    .map(m-> m.getOwner()).findFirst().orElse(null);

            if(mod == null && instance == null) {
                LOGGER.warn("");
            }

            return components.get(mod != null ? mod.getIdentity() : instance.getIdentity());
        }).filter((vc) -> (vc != null)).forEachOrdered(orderedComponents::add);

        return orderedComponents;
    }
    
    public <T extends SvVo> List<T> getOrderedChildren(Class<T> clazz) {
        return getOrderedSequenceComponents().stream()
                .filter(c -> clazz.isAssignableFrom(c.getClass()))
                .map(c -> clazz.cast(c)).collect(Collectors.toList());
    }
    
    public SvpModule getSvpModule() {
        return getSvpModuleFromWs();
    }
 
    
    public Collection<SvpVo> getSvps() {
        return getView().getChildren(this).stream()
                .filter(o -> SvpVo.class.isAssignableFrom(o.getClass()))
                .map(o -> (SvpVo)o).collect(Collectors.toSet());
    }
    
    public Collection<SviVo> getSvis() {
        return getView().getChildren(this).stream()
                .filter(o -> SviVo.class.isAssignableFrom(o.getClass()))
                .map(o -> (SviVo)o).collect(Collectors.toSet());
    }
    
    public Collection<SvmVo> getSvms() {
        return getView().getChildren(this).stream()
                .filter(o -> SvmVo.class.isAssignableFrom(o.getClass()))
                .map(o -> (SvmVo)o).collect(Collectors.toSet());
    }
        
    private SvpModule getSvpModuleFromWs() {

        return getObject().is(SvmInstance.TYPE) ?
                getWorkspace().getObject(getIdentity(), SvmInstance.class, getContexts()).getDefinition().orElse(null) :
                getWorkspace().getObject(getIdentity(), SvpModule.class, getContexts());
    }

    @Override
    public String toString() {
        return getDisplayId();
    }

    /* public String toString() {
        return getOrderedSequenceComponents().stream().map(Object::toString)
                .reduce("{ ", (a, b) -> a.equals("{ ")
                        ? a.concat(b)
                        : a.concat("; ".concat(b))).concat(" }");
    }*/

}
