/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.jena.ext.com.google.common.collect.Streams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithParent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBFlowInstance;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObject;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 * 
 * @author owengilfellon
 */

public class SBWireUpdater extends ASBWorkspaceSubscriber implements SBWorkspaceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SBWireUpdater.class);
    private SBWorkspace ws;
    private SBIdentityFactory factory;
    private ModWireUpdater modWireUpdater;
    private TransWireUpdater transWireUpdater;
    private CisWireUpdater cisWireUpdater;

    @Override
    public void onNodeEvent(SBGraphEvent<URI> evt) {

        if(evt.getType() != SBGraphEventType.ADDED)
            return;

        URI[] contexts = ((SBEvtWithContext)evt).getContexts();
        SBWireContext ctx = initialiseContext(evt, contexts);

        // If it was a port instance event, run two interaction wire updaters

        if(ctx.getPortInstance().isPresent()) {

            transWireUpdater.applyToContext(ctx);
            modWireUpdater.applyToContext(ctx);
        }

        // Run cis wire updater to identify cis wires

        cisWireUpdater.applyToContext(ctx);

        performAction(ctx, contexts);
    }

    private SBWireContext initialiseContext(SBGraphEvent<URI> evt, URI[] contexts) {

        SBEvtWithParent<URI> e = (SBEvtWithParent<URI>) evt;
        URI instanceId = evt.getData();
        SBWireContext ctx = new SBWireContext();

        // If event is a port instance, add port instance, owner and owner parent

        SBPortInstance instanceObj = ws.getObject(instanceId, SBPortInstance.class, contexts);

        if(instanceObj == null) {
            LOGGER.error("Could not retrieve port instance {} in context: {}", factory.getIdentity(instanceId), factory.getIdentity(contexts[0]));
            return ctx;
        }

        ASBFlowInstance parentObj = ws.getObject(e.getParent(), ASBFlowInstance.class, contexts);
        if(parentObj == null) {
            LOGGER.error("Could not get Interaction\t{} in context:\t{}", factory.getIdentity(e.getParent()), factory.getIdentity(contexts[0]));
            return ctx;
        }

        ctx.setWireParent(parentObj.getParent().as(SvpModule.class).get());
        ctx.setPortInstance(instanceObj);
        ctx.setPortOwner(parentObj);

        // If we've specified wire parent (we should in all events), get wires and *all* port instances

        if(ctx.getWireParent().isPresent()) {
            SvpModule svpModule = ctx.getWireParent().get();
            svpModule.getWires().forEach(ctx::putExistingWire);
            if(ctx.getPortInstance().isPresent())
                Streams.concat(
                    svpModule.getSvpInstances().stream(),
                    svpModule.getSviInstances().stream(),
                    svpModule.getSvmInstances().stream())
                .flatMap(i -> i.getPorts().stream()).forEach(ctx::putPort);
        }

        return ctx;
    }

    private boolean isCisWire(SBWire wire) {
        SBPortInstance from = wire.getFrom();
        SBPortInstance to = wire.getTo();
        return !from.getOwner().is(Interaction.TYPE) && !to.getOwner().is(Interaction.TYPE);
    }

    private void performAction(SBWireContext ctx, URI[] contexts) {

        SbolActionBuilder b = new SbolActionBuilder(ws, contexts);

        if(ctx.getPortInstance().isPresent()) {
            for (URI id : ctx.getExistingWires().keySet()) {

                // Remove cis wires that have not been added to expected

                if(isCisWire(ctx.getExistingWires().get(id)) && !ctx.getExpectedWires().containsKey(id)) {

                    if(LOGGER.isDebugEnabled()) {
                        SBWire wire = ctx.getExistingWires().get(id);

                        LOGGER.debug("Remove wire: {}:{} -> {}:{}",
                                wire.getFrom().getOwner().getParent().getDisplayId(),
                                wire.getFrom().getOwner().getDisplayId(),
                                wire.getTo().getOwner().getParent().getDisplayId(),
                                wire.getTo().getOwner().getDisplayId());
                    }

                    SBAction a = new RemoveObject<>(
                            id, URI.create(SBWire.TYPE),
                            ctx.getWireParent().get().getIdentity(),
                            URI.create(SvpModule.TYPE), ws, contexts);
                    b = b.addAction(a);
                }
            }
        }

        // Add all wires that are expected but not found

        for (URI id : ctx.getExpectedWires().keySet()) {
            if (!ctx.getExistingWires().keySet().contains(id)) {
                SBAction a = ctx.getExpectedWires().get(id);
                if (a != null) {
                    b = b.addAction(a);
                }
            }
        }

        // if any modifications have been identified, perform

        SBAction action = b.isEmpty() ? null : b.build("SBWireUpdater[" + ctx.getWireParent().get().getDisplayId() + "]");
        if (action != null) {
            ws.perform(action);
        }
    }

    @Override
    public void setWorkspace(SBWorkspace ws) {
        this.factory = ws.getIdentityFactory();
        this.ws = ws;

        this.ws.getDispatcher().subscribe(event -> //SBWire.class.isAssignableFrom(event.getDataClass()) ||
                SBPortInstance.class.isAssignableFrom(event.getDataClass()), this);

        // Initialise the wire updaters

        this.modWireUpdater = new ModWireUpdater(factory);
        this.transWireUpdater =  new TransWireUpdater(factory);
        this.cisWireUpdater = new CisWireUpdater(factory);
    }

    @Override
    public void close() {
        this.ws.getDispatcher().unsubscribe(this);
        this.ws = null;
        this.factory = null;
    }

    @Override
    public SBWorkspace getWorkspace() {
        return ws;
    }
}
