/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.impl;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.util.Collection;

import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SvpDebugger {
    
    public static void printFlowObject(SBFlowObject identified, OutputStream os) {
        
        
        PrintWriter writer = new PrintWriter(os);

        SBWorkspace ws = identified.getWorkspace();
        
        writer.println();
        writer.println("============================================");
        writer.println(identified.getDisplayId());
        writer.println("============================================");
        
        for(String predicate : identified.getPredicates() ) {
            Collection<SBValue> values = identified.getValues(predicate);
            for(SBValue v : values) {
                if(!(v.isURI() && ws.containsIdentity(v.asURI(), ws.getContextIds()))) {
                    URI p = URI.create(predicate);
                    writer.printf("%-25s: %s\n", p.getPath() + (p.getFragment() != null ? "#" + p.getFragment() : ""), v);
                }
                
                    
            }
        }
        writer.println("------------------  Ports  ------------------");
        
        identified.getPorts(SBPortDirection.IN).stream().forEach(p -> {
            SBIdentified i = p.getIdentityConstraint() != null ? ws.getObject(p.getIdentityConstraint(), SBIdentified.class, p.getContexts()) : null;
            String state = p.getPortState() == null ? "" : p.getPortState().toString();
            String idconstraint = i == null ? "" : i.getDisplayId();
            writer.printf("%-25s %-25s %-25s %-25s\n", p.getPortDirection().toString(), p.getPortType(), state, idconstraint);
        });
        
        identified.getPorts(SBPortDirection.OUT).stream().forEach(p -> {
            
            SBIdentified i = p.getIdentityConstraint() != null ? ws.getObject(p.getIdentityConstraint(), SBIdentified.class, p.getContexts()) : null;
            String state = p.getPortState() == null ? "" : p.getPortState().toString();
            String idconstraint = i == null ? "" : i.getDisplayId();
            writer.printf("%-25s %-25s %-25s %-25s\n", p.getPortDirection().toString(), p.getPortType(), state, idconstraint);
            
        
        });
        
        if(identified.is(SvpModule.TYPE)) {
            writer.println("------------------  Wires  ------------------");
        
            ((SvpModule)identified).getWires().stream().forEach(p -> {
                writer.printf("%-25s -> %-25s\n",  p.getFrom().getDisplayId(), p.getTo().getDisplayId());
            });
            
        } else if (identified.is(Svp.TYPE)) {
            Svp svp = (Svp) identified;
            writer.flush();
            svp.getInternalInteractions().stream().forEach(i -> {
                printFlowObject(i, os);
            });
        } else if (identified.is(SvpInteraction.TYPE)) {
            SvpInteraction svi = (SvpInteraction) identified;
            writer.println("------------------ Parameters ------------------");
            
            writer.printf("%-10s %-10s %-10s %-10s %-10s\n", 
                        "",
                       "Type",
                        "Value", 
                        "Scope", 
                        "EvidenceCode");
            
            svi.getParameters().stream().forEach(par -> {
                writer.printf("%-10s %-10s %-10s %-10s %-10s\n", 
                        "Parameter:",
                        par.getParameterType(),
                        par.getParameterValue(), 
                        par.getParameterScope(), 
                        par.getParameterEvidenceCode());
            });
            
            
            writer.println("------------------  Participants  ------------------");
            
            writer.printf("%-10s %-10s %-10s %-10s %-10s\n", 
                        "",
                       "Direction",
                        "PortDirection", 
                        "Role", 
                        "MathName");
            
            svi.getParticipants().stream().forEach(par -> {
                writer.printf("%-10s %-10s %-10s %-10s %-10s\n", 
                        "Participant:",
                       par.getDirection(),
                        par.getPortDirection(), 
                        par.getInteractionRole(), 
                        par.getNameInMath());
                
            });
        }
        
        

        writer.flush();
    } 
    
}
