/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import java.util.Optional;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBFlowInstance;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.sbol.object.*;

import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owen
 */
@DomainObject(rdfType = SvmInstance.TYPE)
public class SvmInstance extends ASBFlowInstance<SvpModule> implements ChildObject,  SBSvpSequenceObject<SvpModule> {

    public static final String TYPE = "http://virtualparts.org/SvmInstance";
    
    public SvmInstance(SBIdentity identity, SBWorkspace workspace, SBValueProvider provider) {
        super(identity, workspace, provider);
    }
    
    @Override
    public boolean isMappedToComponent() {
        return getMappedToComponent() != null;
    }
    
    @Override
    public Component getMappedToComponent() {
       try (SBWorkspaceGraph g = new SBWorkspaceGraph(ws, getContexts())) {
           return g.getTraversal().v(this)
                   .e(SBDirection.OUT, SynBadTerms.SbolMapsTo.hasMapsTo)
                   .v(SBDirection.OUT, MapsTo.class)
                   .e(SBDirection.OUT, SynBadTerms.SbolMapsTo.hasLocalInstance)
                   .v(SBDirection.OUT, FunctionalComponent.class)
                   .e(SBDirection.IN, SynBadTerms.SbolMapsTo.hasLocalInstance)
                   .v(SBDirection.IN, MapsTo.class)
                   .e(SBDirection.OUT, SynBadTerms.SbolMapsTo.hasRemoteInstance)
                   .v(SBDirection.OUT, Component.class).getResult().streamVertexes().map(o -> (Component)o).findFirst().orElse(null);
       }
    }
    
    @Override
    public SvpModule getParent() {
        return as(Module.class).map(m -> m.getParent().getIdentity())
                .map(id -> ws.getObject(id, SvpModule.class, getContexts())).get();
    }

    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    @Override
    public Optional<SvpModule> getDefinition() {
        return getOutgoingObjects(SynBadTerms.Sbol.definedBy, SvpModule.class).stream().findFirst();
                
    }
    
}
