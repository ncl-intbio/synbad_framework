/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSviBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.SvpModuleType;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class TuActionsFactory {

    private final static Logger LOGGER = LoggerFactory.getLogger(TuActionsFactory.class);

    public enum InteractionMode {
        POSITIVE,
        NEGATIVE
    }

    public enum ModuleType {
        POPS_SOURCE,
        POPS_MODULATOR,
        POPS_SINK
    }

    public static SBAction createPopsGenerator(SBIdentity identity, SBIdentity promoterId, double ktr, SBWorkspace ws, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();
        SBIdentity uniquePromoter = promoterId != null ? promoterId : f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), "prom_" + identity.getDisplayID(), identity.getVersion()));
        SBIdentity popsOut = SvpIdentityHelper.getPortIdentity(uniquePromoter, uniquePromoter, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity,  popsOut);

        b.createSvm(identity)
                .createPromoter(uniquePromoter, ktr)
                .addSvp(identity, uniquePromoter, null)
                .createProxyPort(popsOutProxy, popsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .getAs(SBDomainBuilder.class)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.SOURCE.getUri()), SBIdentityHelper.getURI(SvpModule.TYPE));

        // TO-DO: Proxy POPS IN (RBS)
        // TO-DO: Proxy POPS OUT (Activation / repression)
        return b.build();
    }

    public static SBAction createPopsSinkWithComplexation(SBIdentity identity, SBIdentity cds, SBIdentity smallMolecule, SBIdentity complexId, double ktl, double kd, double cForm, double cDeg, double cDiss, SBWorkspace ws, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity uniqueCds = cds != null ? cds : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueRbs = cds != null ?
                f.getIdentity(identity.getUriPrefix(), "rbs_" + cds.getDisplayID(), identity.getVersion()) :
                f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());

        SBIdentity uniqueTerminator = f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion());

        SBIdentity popsIn = SvpIdentityHelper.getPortIdentity(uniqueRbs, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy = SvpIdentityHelper.getProxyPortIdentity(identity, popsIn);

        b.createSvm(identity)
            .createRBS(uniqueRbs, ktl, 1.0)
            .createCDSWithComplexationSmallMolecule(uniqueCds, smallMolecule, complexId, kd, cForm, cDeg, cDiss)
            .createTerminator(uniqueTerminator)
            .addSvp(identity, uniqueRbs, null)
            .addSvp(identity, uniqueCds, null)
            .addSvp(identity, uniqueTerminator, null)
            .createProxyPort(popsInProxy, popsIn.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .getAs(SBDomainBuilder.class)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.SINK.getUri()), SBIdentityHelper.getURI(SvpModule.TYPE));


        SBIdentity complexOut = SvpIdentityHelper.getPortIdentity(uniqueCds,
                complexId,
                SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);

        SBIdentity proteinOut = SvpIdentityHelper.getPortIdentity(uniqueCds,
                SvpIdentityHelper.getProteinIdentity(uniqueCds),
                SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
        // SBIdentity proteinOut = SvpIdentityHelper.getPortIdentity(uniqueCds, uniqueCds, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);

        SBIdentity complexOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, complexOut);
        SBIdentity proteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, proteinOut);

//        b.createProxyPort(complexOutProxy, complexOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
//        b.createProxyPort(proteinOutProxy, proteinOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));

        return b.build();
    }

    public static SBAction createPopsSink(SBIdentity identity, SBIdentity cds, double ktl, double kd, SBWorkspace ws, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity uniqueCds = cds != null ? cds : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueRbs = cds != null ? 
                f.getIdentity(identity.getUriPrefix(), "rbs_" + cds.getDisplayID(), identity.getVersion()) :
                f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());
       
         SBIdentity uniqueTerminator = f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion());

        SBIdentity popsIn = SvpIdentityHelper.getPortIdentity(uniqueRbs, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy = SvpIdentityHelper.getProxyPortIdentity(identity, popsIn);

        b.createSvm(identity)
            .createRBS(uniqueRbs, ktl, 1.0)
            .createCDS(uniqueCds, kd)
            .createTerminator(uniqueTerminator)
            .addSvp(identity, uniqueRbs, null)
            .addSvp(identity, uniqueCds, null)
            .addSvp(identity, uniqueTerminator, null)
            .createProxyPort(popsInProxy, popsIn.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .getAs(SBDomainBuilder.class)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.SINK.getUri()), SBIdentityHelper.getURI(SvpModule.TYPE));

        
        SBIdentity proteinOut = SvpIdentityHelper.getPortIdentity(uniqueCds,
                SvpIdentityHelper.getProteinIdentity(uniqueCds), 
                SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
        // SBIdentity proteinOut = SvpIdentityHelper.getPortIdentity(uniqueCds, uniqueCds, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
        
        SBIdentity proteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, proteinOut);

        // b.createProxyPort(proteinOutProxy, proteinOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));

        return b.build();
    }

    public static SBAction createPopsModulator(SBIdentity identity, SBIdentity cdsId, SBIdentity promoterId, InteractionMode mode, double ktr, double ktl, double kd, double km, double n, SBWorkspace ws, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity uniqueCds = cdsId != null ? cdsId : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());//);
        SBIdentity uniqueRbs =  cdsId != null
                        ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cdsId.getDisplayID(), identity.getVersion())
                        : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());//);
        SBIdentity uniqueTerminator = f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion());//);
        SBIdentity uniquePromoter = promoterId != null ? promoterId : f.getIdentity(identity.getUriPrefix(), "prom_" + identity.getDisplayID(), identity.getVersion());//);
        
        
        SBIdentity popsInPort = SvpIdentityHelper.getPortIdentity(uniqueRbs, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy = SvpIdentityHelper.getProxyPortIdentity(identity, popsInPort);
       
        b.createSvm(identity)
                .createPromoter(uniquePromoter, ktr)
                .createRBS(uniqueRbs, ktl, 1.0)
                .createCDS(uniqueCds, kd)
                .createTerminator(uniqueTerminator)
                .addSvp(identity, uniqueRbs, null)
                .addSvp(identity, uniqueCds, null)
                .addSvp(identity, uniqueTerminator, null)
                .addSvp(identity, uniquePromoter, null)
                .createProxyPort(popsInProxy, popsInPort.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .getAs(SBDomainBuilder.class)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.MODULATOR.getUri()), SBIdentityHelper.getURI(SvpModule.TYPE));

        if (mode == InteractionMode.NEGATIVE) {
            SBIdentity uniqueOperator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_operator", identity.getVersion()));
            SBIdentity cdsRepressesOperator = SvpIdentityHelper.createRepressionIdentity(uniqueCds, ComponentType.Protein, SynBadPortState.Default, uniquePromoter);
            
            SBIdentity interactionPopsOut = SvpIdentityHelper.getPortIdentity(cdsRepressesOperator,
                    uniqueOperator, 
                    SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
            
//            SBIdentity proteinOut = SvpIdentityHelper.getPortIdentity(uniqueCds,
//                    SvpIdentityHelper.getProteinIdentity(uniqueCds),
//                    SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
            
            SBIdentity popsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOut);
//            SBIdentity proteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, proteinOut);

            b.createOperator(uniqueOperator)
                .addSvp(identity, uniqueOperator, null).asInteractionBuilder()
                .createRepressByOperator(cdsRepressesOperator, ComponentType.Protein, SynBadPortState.Default,
                            uniqueCds.getIdentity(), 
                        uniqueOperator.getIdentity(), km, n).asPartBuilder()
                .createProxyPort(popsOutProxy, interactionPopsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
//                .createProxyPort(proteinOutProxy, proteinOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
            //    .getAs(SBDomainBuilder.class)
    //            .addAction(new UpdatePortInstances(DefaultEventType.ADDED, identity.getIdentity(), workspaceId, contexts));
        } else {

            SBIdentity cdsActivatesPromoter = SvpIdentityHelper.createActivationIdentity(uniqueCds, ComponentType.Protein, SynBadPortState.Default, uniquePromoter);

            SBIdentity interactionPopsOut = SvpIdentityHelper.getPortIdentity(cdsActivatesPromoter,
                    uniquePromoter,
                    SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);

//            SBIdentity proteinOut = SvpIdentityHelper.getPortIdentity(uniqueCds,
//                    SvpIdentityHelper.getProteinIdentity(uniqueCds),
//                    SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);

            SBIdentity popsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOut);
//            SBIdentity proteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, proteinOut);

            b.asInteractionBuilder()
                .createActivateByPromoter(cdsActivatesPromoter, ComponentType.Protein, SynBadPortState.Default, uniqueCds.getIdentity(), uniquePromoter.getIdentity(), km, n)
                .createProxyPort(popsOutProxy, interactionPopsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
//                    .createProxyPort(proteinOutProxy, proteinOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
        }

        return b.build();
    }

    public static SBAction createTranscriptionUnit(SBIdentity identity, SBIdentity cdsId, SBIdentity promoterId, double ktr, double ktl, double kd, SBWorkspace ws, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity uniqueCds = cdsId != null ? cdsId : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());//);
        SBIdentity uniqueRbs =  cdsId != null
                ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cdsId.getDisplayID(), identity.getVersion())
                : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());//);
        SBIdentity uniqueTerminator = f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion());//);
        SBIdentity uniquePromoter = promoterId != null ? promoterId : f.getIdentity(identity.getUriPrefix(), "prom_" + identity.getDisplayID(), identity.getVersion());//);

        SBIdentity proteinOutPort = SvpIdentityHelper.getPortIdentity(uniqueCds, SvpIdentityHelper.getProteinIdentity(uniqueCds), SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
        SBIdentity proteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, proteinOutPort);

        b.createSvm(identity)
                .createPromoter(uniquePromoter, ktr)
                .createRBS(uniqueRbs, ktl, 1.0)
                .createCDS(uniqueCds, kd)
                .createTerminator(uniqueTerminator)
                .addSvp(identity, uniquePromoter, null)
                .addSvp(identity, uniqueRbs, null)
                .addSvp(identity, uniqueCds, null)
                .addSvp(identity, uniqueTerminator, null)
                .createProxyPort(proteinOutProxy, proteinOutPort.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .getAs(SBDomainBuilder.class);
                //.createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.MODULATOR.getUri()), SBIdentityHelper.getURI(SvpModule.TYPE));

        return b.build();
    }

    public static SBAction createSmlMolPhosPopsModulator(SBIdentity identity,
            SBIdentity cds1, SBIdentity cds2, SBIdentity smallMolecule, SBIdentity promoterId, InteractionMode mode,
            double ktr,
            double ktl1, double kd1, double phosRate, double phosDegRate, double dePhosRate,
            double ktl2, double kd2,
            double kf, double kd, double kDeP,
            double km, double n,
            SBWorkspace ws,
            URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity smlMol = smallMolecule != null 
                ? smallMolecule 
                : f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), "smlMol_" + identity.getDisplayID(), identity.getVersion()));

        SBIdentity uniqueCds1 = cds1 != null
                ? cds1
                : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueCds2 = cds2 != null
                ? cds2
                : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueRbs1
                = cds1 != null
                        ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cds1.getDisplayID(), identity.getVersion())
                        : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID() + "_1", identity.getVersion());
        SBIdentity uniqueRbs2
                = cds2 != null
                        ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cds2.getDisplayID(), identity.getVersion())
                        : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueTerminator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion()));
        SBIdentity uniquePromoter = promoterId != null ? promoterId : f.getIdentity(identity.getUriPrefix(), "prom_" + identity.getDisplayID(), identity.getVersion());

        SBIdentity cdsPhosCds = SvpIdentityHelper.createExternalPhosphorylationIdentity(uniqueCds1, uniqueCds2);

        SBIdentity popsIn1 = SvpIdentityHelper.getPortIdentity(uniqueRbs1, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy1 = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, popsIn1));
        SBIdentity popsIn2 = SvpIdentityHelper.getPortIdentity(uniqueRbs2, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy2 = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, popsIn2));

        b.createSvm(identity)
                .createPromoter(uniquePromoter, ktr)
                .createRBS(uniqueRbs1, ktl1, 1.0)
                .createCDSWithPhosphorylatingSmallMolecule(uniqueCds1, smlMol, kd1, phosRate, phosDegRate, dePhosRate)
                .createRBS(uniqueRbs2, ktl2, 1.0)
                .createCDS(uniqueCds2, kd2)
                .createTerminator(uniqueTerminator)
                
                .addSvp(identity, uniqueRbs1, null)
                .addSvp(identity, uniqueCds1, null)
                .addSvp(identity, uniqueRbs2, null)
                .addSvp(identity, uniqueCds2, null)
                .addSvp(identity, uniqueTerminator, null)
                .addSvp(identity, uniquePromoter, null).asInteractionBuilder()
                .createPhosphorylate(cdsPhosCds, uniqueCds1.getIdentity(), uniqueCds2.getIdentity(), kf, kd, kDeP).asPartBuilder()
                .createProxyPort(popsInProxy1, popsIn1.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .createProxyPort(popsInProxy2, popsIn2.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));

        if (mode == InteractionMode.NEGATIVE) {
            SBIdentity uniqueOperator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_operator", identity.getVersion()));
            SBIdentity cdsRepressesOperator = SvpIdentityHelper.createRepressionIdentity(uniqueCds2, ComponentType.Protein, SynBadPortState.Phosphorylated, uniqueOperator);
            
            SBIdentity interactionPopsOut = SvpIdentityHelper.getPortIdentity(cdsRepressesOperator, uniqueOperator, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
            SBIdentity interactionPopsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOut);
           
            SBIdentity interactionProteinOut = SvpIdentityHelper.getPortIdentity(cdsPhosCds, SvpIdentityHelper.getPhosphorylatedProteinIdentity(uniqueCds2), SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
            SBIdentity interactionProteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionProteinOut);
    
            b.createOperator(uniqueOperator)
                .addSvp(identity, uniqueOperator, null)
                .createProxyPort(interactionPopsOutProxy, interactionPopsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .createProxyPort(interactionProteinOutProxy, interactionProteinOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE)).asInteractionBuilder()
                .createRepressByOperator(cdsRepressesOperator, ComponentType.Protein, SynBadPortState.Phosphorylated, uniqueCds2.getIdentity(), uniqueOperator.getIdentity(), km, n);

        } else {
            SBIdentity cdsActivatesPromoter = SvpIdentityHelper.createActivationIdentity(uniqueCds2, ComponentType.Protein, SynBadPortState.Phosphorylated, uniquePromoter);

            SBIdentity interactionPopsOut = SvpIdentityHelper.getPortIdentity(cdsActivatesPromoter, uniquePromoter, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
            SBIdentity interactionPopsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOut);

            SBIdentity interactionProteinOut = SvpIdentityHelper.getPortIdentity(cdsPhosCds, SvpIdentityHelper.getPhosphorylatedProteinIdentity(uniqueCds2), SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
            SBIdentity interactionProteinOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOutProxy);

            b .createProxyPort(interactionPopsOutProxy, interactionPopsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                    .createProxyPort(interactionProteinOutProxy, interactionProteinOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE)).asInteractionBuilder()
                    .createActivateByPromoter(cdsActivatesPromoter, ComponentType.Protein, SynBadPortState.Phosphorylated, uniqueCds2.getIdentity(), uniquePromoter.getIdentity(), km, n);
        }

        b.getAs(SBDomainBuilder.class)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.MODULATOR.getUri()), SBIdentityHelper.getURI(SvpModule.TYPE));

        // TO-DO: Proxy POPS IN (RBS)
        // TO-DO: Proxy POPS OUT (Activation / repression)
        return b.build();
    }
    
    
    
//     public static SBAction createInutPhosphorylatesProtein(SBIdentity identity,
//            SBIdentity cds1, SBIdentity cds2, SBIdentity smallMolecule, SBIdentity promoterId, InteractionMode mode,
//            double ktr,
//            double ktl1, double kd1, double phosRate, double phosDegRate, double dePhosRate,
//            double ktl2, double kd2,
//            double kf, double kd, double kDeP,
//            double km, double n,
//            SBWorkspace ws,
//            URI[] contexts) {
//
//
//         if(cds1 == null) {
//             LOGGER.error("CDS1 cannot be null");
//             return null;
//         }
//
//        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
//        SBIdentityFactory f = ws.getIdentityFactory();
//
//        SBIdentity uniqueCds2 = cds2 != null
//                ? cds2
//                : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
//
//        SBIdentity uniqueRbs2
//                = cds2 != null
//                        ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cds2.getDisplayID(), identity.getVersion())
//                        : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());
//        SBIdentity uniqueTerminator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion()));
//        SBIdentity uniquePromoter = promoterId != null ? promoterId : f.getIdentity(identity.getUriPrefix(), "prom_" + identity.getDisplayID(), identity.getVersion());
//
//        SBIdentity cdsPhosCds = SvpIdentityHelper.createExternalPhosphorylationIdentity(cds1, uniqueCds2);
//        SBIdentity inPort2Id = SvpIdentityHelper.getPortIdentity(uniqueRbs2, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
//        SBIdentity inProxy2Id = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, inPort2Id));
//
//        SBIdentity inPortId = SvpIdentityHelper.getPortIdentity(cdsPhosCds,
//                SvpIdentityHelper.getComponentIdentity(cds1, ComponentType.Protein, SynBadPortState.Phosphorylated)
//                , SBPortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
//        SBIdentity inProxyId = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, inPortId));
//
//        b.createSvm(identity)
//                .createPromoter(uniquePromoter, ktr)
//
//                .createRBS(uniqueRbs2, ktl2, 1.0)
//                .createCDS(uniqueCds2, kd2)
//                .createTerminator(uniqueTerminator)
//
//                .addSvp(identity, uniqueRbs2, null)
//                .addSvp(identity, uniqueCds2, null)
//                .addSvp(identity, uniqueTerminator, null)
//                .addSvp(identity, uniquePromoter, null).asInteractionBuilder()
//                .createPhosphorylate(cdsPhosCds, cds1.getIdentity(), uniqueCds2.getIdentity(), kf, kd, kDeP)
//
//                .createProxyPort(inProxyId, inPortId.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
//                .createProxyPort(inProxy2Id, inPort2Id.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
//
//        if (mode == InteractionMode.NEGATIVE) {
//            SBIdentity uniqueOperator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_operator", identity.getVersion()));
//            SBIdentity cdsRepressesOperator = SvpIdentityHelper.createRepressionIdentity(uniqueCds2, ComponentType.Protein, SynBadPortState.Phosphorylated, uniqueOperator);
//
//            SBIdentity portId = SvpIdentityHelper.getPortIdentity(cdsRepressesOperator, uniqueOperator, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
//            SBIdentity proxyId = SvpIdentityHelper.getProxyPortIdentity(identity, portId);
//
//            SBIdentity port2Id = SvpIdentityHelper.getPortIdentity(cdsPhosCds, SvpIdentityHelper.getPhosphorylatedProteinIdentity(uniqueCds2), SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
//            SBIdentity proxy2Id = SvpIdentityHelper.getProxyPortIdentity(identity, port2Id);
//
//            b.createOperator(uniqueOperator)
//                .addSvp(identity, uniqueOperator, null)
//                .createProxyPort(proxyId, portId.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
//                .createProxyPort(proxy2Id, port2Id.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE)).asInteractionBuilder()
//                .createRepressByOperator(cdsRepressesOperator, ComponentType.Protein, SynBadPortState.Phosphorylated, uniqueCds2.getIdentity(), uniqueOperator.getIdentity(), km, n);
//        } else {
//            SBIdentity cdsActivatesPromoter = SvpIdentityHelper.createActivationIdentity(uniqueCds2, ComponentType.Protein, SynBadPortState.Phosphorylated, uniquePromoter);
//            SBIdentity portId = SvpIdentityHelper.getPortIdentity(cdsActivatesPromoter, uniquePromoter, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
//            SBIdentity proxyId = SvpIdentityHelper.getProxyPortIdentity(identity, portId);
//            SBIdentity port2Id = SvpIdentityHelper.getPortIdentity(cdsPhosCds, SvpIdentityHelper.getPhosphorylatedProteinIdentity(uniqueCds2), SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
//            SBIdentity proxy2Id = SvpIdentityHelper.getProxyPortIdentity(identity, port2Id);
//            b .createProxyPort(proxyId, portId.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
//                    .createProxyPort(proxy2Id, port2Id.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE)).asInteractionBuilder()
//                    .createActivateByPromoter(cdsActivatesPromoter, ComponentType.Protein, SynBadPortState.Phosphorylated, uniqueCds2.getIdentity(), uniquePromoter.getIdentity(), km, n);
//
//        }
//
//        b.getAs(SBDomainBuilder.class)
//                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.MODULATOR.getUri()), ws.getIdentity());
//
//        return b.build();
//    }

    /**
     *
     * Creates a unit that takes a PoPS input and returns a PoPS output.
     * Contains two CDS, the first protein creates a complex with the second
     * which then regulates a promoter producing PoPS out. If a small molecule
     * ID is specified, the first protein in phosphorylated by the small
     * molecule, and creates a complex with the second in it's phosphorylated
     * form.
     *
     * @param complexId Provides a specific ID for the complex. Generated
     * automatically if null.
     * @param smlMolId Provides a specific ID for the small molecule. If
     * provided, the small molecule phosphorylates the first protein, which is
     * then used as a participant in the complexation.
     * @return
     */
    public static SBAction createComplexPopsModulator(SBIdentity identity, InteractionMode mode,
            SBIdentity cds1,
            SBIdentity cds2,
            SBIdentity smlMolId,
            SBIdentity complexId,
            SBIdentity promoterId,
            // promoter
            double ktr,
            // cds 1
            double ktl1, double kd1, double phosRate, double phosDegRate, double dePhosRate,
            // cds 2
            double ktl2, double kd2,
            // phos
            double kf, double kd, double kDe,
            // act
            double km, double n,
            SBWorkspace ws, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity uniqueCds1 = cds1 != null
                ? cds1
                : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueCds2 = cds2 != null
                ? cds2
                : f.getIdentity(identity.getUriPrefix(), "cds_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniqueRbs1
                = cds1 != null
                        ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cds1.getDisplayID(), identity.getVersion())
                        : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID() + "_1", identity.getVersion());
        SBIdentity uniqueRbs2
                = cds2 != null
                        ? f.getIdentity(identity.getUriPrefix(), "rbs_" + cds2.getDisplayID(), identity.getVersion())
                        : f.getIdentity(identity.getUriPrefix(), "rbs_" + identity.getDisplayID(), identity.getVersion());

        SBIdentity uniqueTerminator
                = f.getIdentity(identity.getUriPrefix(), "ter_" + identity.getDisplayID(), identity.getVersion());
        SBIdentity uniquePromoter = promoterId != null
                ? promoterId
                : f.getIdentity(identity.getUriPrefix(), "prom_" + identity.getDisplayID(), identity.getVersion());

        SBIdentity complexId1 = complexId != null
                ? complexId
                : f.getIdentity(identity.getUriPrefix(), uniqueCds1.getDisplayID() + "_" + uniqueCds2.getDisplayID(), identity.getVersion());

        b.createSvm(identity)
                .createPromoter(uniquePromoter, ktr)
                .createRBS(uniqueRbs1, ktl1, 1.0);

        if (smlMolId != null) {
            b.createCDSWithPhosphorylatingSmallMolecule(uniqueCds1, smlMolId, kd1, phosRate, phosDegRate, dePhosRate);
        } else {
            b.createCDS(uniqueCds1, kd1);
        }

        b.createRBS(uniqueRbs2, ktl2, 1.0)
                .createCDS(uniqueCds2, kd2)
                .createTerminator(uniqueTerminator)
                .addSvp(identity, uniqueRbs1, null)
                .addSvp(identity, uniqueCds1, null)
                .addSvp(identity, uniqueRbs2, null)
                .addSvp(identity, uniqueCds2, null)
                .addSvp(identity, uniqueTerminator, null)
                .addSvp(identity, uniquePromoter, null).asInteractionBuilder()
                .createComplexFormation(complexId1)
                    .setParticipant1(uniqueCds1)
                    .setType1(ComponentType.Protein)
                    .setState1(smlMolId == null ? SynBadPortState.Default : SynBadPortState.Phosphorylated)
                    .setParticipant2(uniqueCds2)
                    .setType2(ComponentType.Protein)
                    .setState2(SynBadPortState.Default)
                    .setKd(kd)
                    .setKf(kf).builder().asInteractionBuilder()
                .createComplexDisassociation(complexId1)
                    .setParticipant1(uniqueCds1)
                    .setType1(ComponentType.Protein)
                    .setState1(smlMolId == null ? SynBadPortState.Default : SynBadPortState.Phosphorylated)
                    .setParticipant2(uniqueCds2)
                    .setType2(ComponentType.Protein)
                    .setState2(SynBadPortState.Default)
                    .setKb(kDe).builder();

        SBIdentity popsIn1 = SvpIdentityHelper.getPortIdentity(uniqueRbs1, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy1 = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, popsIn1));
        SBIdentity popsIn2 = SvpIdentityHelper.getPortIdentity(uniqueRbs2, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsInProxy2 = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, popsIn2));

        b.createProxyPort(popsInProxy1, popsIn1.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
        b.createProxyPort(popsInProxy2, popsIn2.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));

        SBIdentity complexInteraction = SvpIdentityHelper.createExternalComplexationIdentity(uniqueCds1, uniqueCds2);
        SBIdentity complexCdId = SvpIdentityHelper.getComponentIdentity(complexId1, ComponentType.Complex, SynBadPortState.Default);

        if (mode == InteractionMode.NEGATIVE) {

            SBIdentity uniqueOperator = f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_operator", identity.getVersion());
            SBIdentity cdsRepressesOperator = SvpIdentityHelper.createRepressionIdentity(complexId1, ComponentType.Complex, SynBadPortState.Default, uniqueOperator);

            b.createOperator(uniqueOperator);
            b.addSvp(identity, uniqueOperator, null);
            b.asInteractionBuilder().createRepressByOperator(cdsRepressesOperator, ComponentType.Complex, SynBadPortState.Default, complexId1.getIdentity(), uniqueOperator.getIdentity(), km, n);

            SBIdentity interactionPopsOut = SvpIdentityHelper.getPortIdentity(cdsRepressesOperator, uniqueOperator, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
            SBIdentity interactionPopsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOut);

            b.createProxyPort(interactionPopsOutProxy, interactionPopsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
            
            SBIdentity interactionComplexOut = SvpIdentityHelper.getPortIdentity(complexInteraction, complexCdId, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);
            SBIdentity interactionComplexOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionComplexOut);

            b.createProxyPort(interactionComplexOutProxy, interactionComplexOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE));
        
        } else {

            SBIdentity cdsActivatesPromoter = SvpIdentityHelper.createActivationIdentity(complexId1, ComponentType.Complex, SynBadPortState.Default, uniquePromoter);

            SBIdentity interactionPopsOut = SvpIdentityHelper.getPortIdentity(cdsActivatesPromoter, uniquePromoter, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);
            SBIdentity interactionPopsOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionPopsOut);

            SBIdentity interactionComplexOut = SvpIdentityHelper.getPortIdentity(complexInteraction, complexCdId, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);
            SBIdentity interactionComplexOutProxy = SvpIdentityHelper.getProxyPortIdentity(identity, interactionComplexOut);

            b.createProxyPort(interactionPopsOutProxy, interactionPopsOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .createProxyPort(interactionComplexOutProxy, interactionComplexOut.getIdentity(), identity.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
                .asInteractionBuilder().createActivateByPromoter(cdsActivatesPromoter, ComponentType.Complex, SynBadPortState.Default, complexId1.getIdentity(), uniquePromoter.getIdentity(), km, n);
        }

        b.getAs(SBDomainBuilder.class)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadTu.moduleType), SBValue.parseValue(SvpModuleType.MODULATOR.getUri()), ws.getIdentity());

        // TO-DO: Proxy POPS IN (RBS)
        // TO-DO: Proxy POPS OUT (Activation)

        return b.build();
    }

    // ==========================================================================
    public static SBAction createTranscriptionUnit(SBIdentity identity, SBWorkspace ws, double ktr, double ktl, double kd, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity uniquePromoter = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_promoter", identity.getVersion()));
        SBIdentity uniqueRbs = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_rbs", identity.getVersion()));
        SBIdentity uniqueCds = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_cds", identity.getVersion()));
        SBIdentity uniqueTerminator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_terminator", identity.getVersion()));

        SBIdentity portOut1 = SvpIdentityHelper.getPortIdentity(
                uniqueCds, SvpIdentityHelper.getProteinIdentity(uniqueCds), SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);

        SBIdentity popsOutProxy = f.getUniqueIdentity(SvpIdentityHelper.getProxyPortIdentity(identity, portOut1));


        b.createSvm(identity)
            .createPromoter(uniquePromoter, ktr)
            .createRBS(uniqueRbs, ktl, 1.0)
            .createCDS(uniqueCds, kd)
            .createTerminator(uniqueTerminator)
            .addSvp(identity, uniquePromoter, null)
            .addSvp(identity, uniqueRbs, null)
            .addSvp(identity, uniqueCds, null)
            .addSvp(identity, uniqueTerminator, null)
            .createProxyPort(popsOutProxy, portOut1.getIdentity(), identity.getIdentity(), URI.create(SvpModule.TYPE))
            .getAs(SBDomainBuilder.class)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.Sbol.role), SBValue.parseValue(ComponentRole.Gene.getUri()), ws.getIdentity());

        // TO-DO: Create proxy port of CDS
        return b.build();
    }

    public static ComponentType getComponentTypeFromPortType(SynBadPortType type) {
        switch (type) {
            case Complex:
                return ComponentType.Complex;
            case DNA:
                return ComponentType.DNA;
            case Protein:
                return ComponentType.Protein;
            case SmallMolecule:
                return ComponentType.SmallMolecule;
            case mRNA:
                return ComponentType.mRNA;
            default:
                return null;
        }
    }

    public static SBAction createActivateUnit(
            SBIdentity identity, URI modulatorOwner, URI portOwner,
            URI modulator, SynBadPortType modulatorType, SynBadPortState modulatorState,
            URI modulated, double km, double n,
            SBWorkspace ws, URI[] contexts) {

        URI svmType = SBIdentityHelper.getURI(SvpModule.TYPE);
        SBSviBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity modulatorId = f.getIdentity(modulator);
        SBIdentity modulatorOwnerId = f.getIdentity(modulatorOwner);
        SBIdentity portOwnerId = f.getIdentity(portOwner);
        SBIdentity modulatorSbolId = SvpIdentityHelper.getComponentIdentity(modulatorId, getComponentTypeFromPortType(modulatorType), modulatorState);
        SBIdentity modulatorPort = SvpIdentityHelper.getPortIdentity(portOwnerId, modulatorSbolId, SBPortDirection.OUT, modulatorType, modulatorState);
        SBIdentity modulatorProxyPort = SvpIdentityHelper.getProxyPortIdentity(modulatorOwnerId, modulatorPort);

        b = //b.createProxyPort(modulatorProxyPort, modulatorPort.getIdentity(), modulatorOwner, svmType)
            b.createActivateByPromoter(identity, getComponentTypeFromPortType(modulatorType), modulatorState, modulator, modulated, km, n);

        return b.build();
    } 
    
    

    public static SBAction createRepressUnit(
            SBIdentity identity, 
            URI modulatorOwner, URI portowner, URI modulator, SynBadPortType modulatorType, SynBadPortState modulatorState,
            URI commonParent,
            URI modulated, int modulatedIndex, double km, double n,
            SBWorkspace ws, URI[] contexts) {

        URI svmType = SBIdentityHelper.getURI(SvpModule.TYPE);
        SBSviBuilder b = new SvpActionBuilderImpl(ws, contexts);
        SBIdentityFactory f = ws.getIdentityFactory();

        SBIdentity commonParentId = f.getIdentity(commonParent);
        SBIdentity modulatorOwnerId = f.getIdentity(modulatorOwner);
        SBIdentity modulatorId = f.getIdentity(modulator);
        SBIdentity modulatedId = f.getIdentity(modulated);
        SBIdentity portownerid = f.getIdentity(portowner);
        
        SBIdentity modulatorSbolId = SvpIdentityHelper.getComponentIdentity(modulatorId, getComponentTypeFromPortType(modulatorType), modulatorState);
        SBIdentity modulatorPort = SvpIdentityHelper.getPortIdentity(portownerid, modulatorSbolId, SBPortDirection.OUT, modulatorType, modulatorState);
        SBIdentity modulatorProxyPort = SvpIdentityHelper.getProxyPortIdentity(modulatorOwnerId, modulatorPort);

        // Add operator in main TU outside of Units (slot between modules...)
        
        b = b.asPartBuilder().createOperator(modulatedId)
            .addSvp(commonParentId, modulatedId, modulatedIndex + 1 ).asInteractionBuilder()
            .createRepressByOperator(identity, getComponentTypeFromPortType(modulatorType), modulatorState, modulator, modulated, km, n)
            .createProxyPort(modulatorProxyPort, modulatorPort.getIdentity(), modulatorOwner, svmType);

        return b.build();
    }

    public static SBAction createRepressedTranscriptionUnit(SBIdentity identity, SBWorkspace ws, double ktr, double ktl, double kd, URI[] contexts) {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        SBIdentityFactory f = ws.getIdentityFactory();
        SBIdentity uniqueIdentity = f.getUniqueIdentity(identity);
        SBIdentity uniquePromoter = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_promoter", identity.getVersion()));
        SBIdentity uniqueOperator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_operator", identity.getVersion()));

        SBIdentity uniqueRbs = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_rbs", identity.getVersion()));
        SBIdentity uniqueCds = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_cds", identity.getVersion()));
        SBIdentity uniqueTerminator = f.getUniqueIdentity(f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_terminator", identity.getVersion()));

        b = b.createSvm(uniqueIdentity);
        b = b.createPromoter(uniquePromoter, ktr);
        b = b.createOperator(uniqueOperator);
        b = b.createRBS(uniqueRbs, ktl, 1.0);
        b = b.createCDS(uniqueCds, kd);
        b = b.createTerminator(uniqueTerminator);

        // TO-DO: Create proxy port of Operator
        // TO-DO: Create proxy port of CDS
        return b.build();
    }
}
