/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.actions.ADeferredAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

/**
 *
 * @author owengilfellon
 */
public class AddUnitProxyPorts extends ADeferredAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddUnitProxyPorts.class);
    private final URI tuId;

    public AddUnitProxyPorts(SBGraphEventType type, URI tuId, SynBadPortType portType, SynBadPortState portState, SBWorkspace workspace, URI[] contexts) {
        super(type, workspace, contexts);
        this.tuId = tuId;
    }

    @Override
    protected SBAction getAction(SBWorkspace ws) {

        SBIdentityFactory f = ws.getIdentityFactory();
       
        SvpModule tu = ws.getObject(tuId, SvpModule.class, contexts);
        List<Svp> svps = tu.getOrderedChildren(Svp.class);
        /*svps.stream().
        
        
        if (!b.isEmpty()) {
            return b.build();
                    // If port is proxy
                    // If component constraint != null
        }
*/
        return null;
    }

    private Set<SBPortInstance> getPortInstances(SBIdentified component, SBWorkspaceGraph graph) {
        return graph.getTraversal().v(component)
                .e(SBDirection.OUT, SynBadTerms.SynBadPortInstance.hasPortInstance)
                .v(SBDirection.OUT, SBPortInstance.class)
                .getResult().streamVertexes().map(obj -> (SBPortInstance) obj)
                .collect(Collectors.toSet());
    }

}
