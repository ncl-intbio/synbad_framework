/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import java.net.URI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;

/**
 *
 * @author owengilfellon
 */
public class AddSvmAction extends AAddChildAction {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSvmAction.class);
    private URI preceded = null;
    
    public AddSvmAction(SBGraphEventType type, URI parentSvmIdentity, URI childSvmIdentity, int index, SBWorkspace workspace, URI[] contexts) {
        super(type, parentSvmIdentity, childSvmIdentity, index,  workspace, contexts);
    }
    
    public AddSvmAction(SBGraphEventType type, URI parentSvmIdentity, URI childSvmIdentity, URI preceded, SBWorkspace workspace, URI[] contexts) {
        super(type, parentSvmIdentity, childSvmIdentity, -1,  workspace, contexts);
        this.preceded = preceded;
    }

    @Override
    public URI getPrecededIdentity() {
        if(preceded == null) {
           // LOGGER.error("Preceded has not been calculate yet.");
        }
        return preceded;
    }
    
    
    
    protected SBAction getAction(SBWorkspace ws) {

        SvpModule parentSvm = ws.getObject(parentIdentity, SvpModule.class, contexts);
        SvpModule childSvm = ws.getObject(childIdentity, SvpModule.class, contexts);

        if(parentSvm == null) {
            LOGGER.error("Could not retrieve parent {} for child {}", parentIdentity.toASCIIString(), childIdentity.toASCIIString());
            return null;
        }

        if(childSvm == null) {
            LOGGER.error("Could not retrieve child {} of parent {}", childIdentity.toASCIIString(), parentIdentity.toASCIIString());
            return null;
        }

        ComponentDefinition parentDnaDef = parentSvm.getDnaDefinition();
        ComponentDefinition childDnaDef = childSvm.getDnaDefinition();

        ModuleDefinition parentModuleDef = parentSvm.as(ModuleDefinition.class).get();
        ModuleDefinition childModuleDef = childSvm.as(ModuleDefinition.class).get();

        if(parentDnaDef == null || childDnaDef == null) {
            LOGGER.error("Parent or child has no defined DNA ComponentDefinition!");
            return null;
        }

        if(parentModuleDef == null || childModuleDef == null) {
            LOGGER.error("Parent or child has no associated ModuleDefinition1");
            return null;
        }

         LOGGER.debug("--- {} ---", childSvm.getDisplayId());
            
        // Component Definitions associated with each SVM, and Identities

        List<Component> components = parentDnaDef.getOrderedComponents();
            
        if(preceded == null && 0 <= index && index < components.size()) {
            preceded = components.get(index).getIdentity();
        }

        // Get FunctionalComponent instances of each SVM's ComponentDefinition
        // from their ModuleDefinitions, and Identities

        FunctionalComponent childDnaFc = childModuleDef.getFunctionalComponents().stream()
                .filter(fc -> fc.getDefinition().get().getIdentity().equals(childDnaDef.getIdentity()))
                .findFirst().orElse(null);

        FunctionalComponent parentDnaFc = parentModuleDef.getFunctionalComponents().stream()
                .filter(fc -> fc.getDefinition().get().getIdentity().equals(parentDnaDef.getIdentity()))
                .findFirst().orElse(null);

        if(childDnaFc == null || parentDnaFc == null) {
            LOGGER.error("Parent or child has no instance of DNA ComponentDefinition!");
            return null;
        }


        // Get identities for objects we've retrieved
            
        SBIdentity parentModuleDefId = ws.getIdentityFactory().getIdentity(parentModuleDef.getIdentity());
        SBIdentity childModuleDefId = ws.getIdentityFactory().getIdentity(childModuleDef.getIdentity());

        SBIdentity parentSvmDnaId = ws.getIdentityFactory().getIdentity(parentDnaDef.getIdentity());
        SBIdentity childSvmDnaId = ws.getIdentityFactory().getIdentity(childDnaDef.getIdentity());

        SBIdentity parentDnaFcId = ws.getIdentityFactory().getIdentity(parentDnaFc.getIdentity());
        SBIdentity childDnaFcId = ws.getIdentityFactory().getIdentity(childDnaFc.getIdentity());

        // Create Identities for objects that we will create

        SBIdentity childComponentId = getUniqueIdentity(ws, Component.getComponentIdentity(parentSvmDnaId, childSvmDnaId));
        SBIdentity childModId = getUniqueIdentity(ws, Module.getModuleIdentity(parentModuleDefId, childModuleDefId));
        SBIdentity childFcId = getUniqueIdentity(ws, FunctionalComponent.getFunctionalComponentIdentity(parentModuleDefId, childSvmDnaId));

        SBIdentity mapChildFcToChildDna = MapsTo.getMapsToIdentity(childModId, childFcId, childDnaFcId, MapsToRefinement.verifyIdentical);
        SBIdentity mapChildFcToParentDna = MapsTo.getMapsToIdentity(parentDnaFcId, childFcId, childComponentId, MapsToRefinement.verifyIdentical);

        //List<Component> orderedComponents = parentDnaDef.getOrderedComponents();

        // Create Module to parent ModuleDefinition, and Component to parent
        // SVM's ComponentDefinition, and create FunctionalComponent to map
        // parent SVM's FC to child SVM's FC
       
        SbolActionBuilder b = new SbolActionBuilder(ws, contexts)
            .createModule(childModId.getIdentity(), childModuleDef, parentModuleDef)
            .createValue(childModId.getIdentity(),URI.create(SynBadTerms.Rdf.hasType), SBValue.parseValue(URI.create(SvmInstance.TYPE)), URI.create(SvpModule.TYPE))
            .createComponent(childComponentId.getIdentity(), parentDnaDef, childDnaDef, SbolAccess.PUBLIC)
            .createFuncComponent(childFcId.getIdentity(), childDnaDef, parentModuleDef, SbolDirection.INOUT, SbolAccess.PUBLIC)
            .createMapsTo(mapChildFcToChildDna.getIdentity(),
                childModId.getIdentity(),
                SBIdentityHelper.getURI(Module.TYPE),
                MapsToRefinement.verifyIdentical,
                childDnaFcId.getIdentity(),
                childFcId.getIdentity())
            .createMapsTo(mapChildFcToParentDna.getIdentity(),
                parentDnaFc.getIdentity(),
                SBIdentityHelper.getURI(FunctionalComponent.TYPE),
                MapsToRefinement.verifyIdentical,
                childComponentId.getIdentity(),
                childFcId.getIdentity())
            .createEdge(parentIdentity, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), childIdentity)
            .addAction(new CreateEdge(childModId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, childComponentId.getIdentity(), ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .addAction(new CreateEdge(childComponentId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, childModId.getIdentity(),  ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .addAction(new CreateEdge(childFcId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, childComponentId.getIdentity(), ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .addAction(new CreateEdge(childComponentId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, childFcId.getIdentity(),  ws.getIdentity(),  ws.getSystemContextIds(contexts)));
     
        LOGGER.trace("Adding module:\t\t{}", childModId.getDisplayID());
        LOGGER.trace("Adding component:\t\t{}", childComponentId.getDisplayID());
        LOGGER.trace("Adding funcComp:\t\t{}", childFcId.getDisplayID());
        LOGGER.trace("Adding mapsTo:\t\t{} to {}", mapChildFcToChildDna.getDisplayID(), childModId.getDisplayID());
        LOGGER.trace("Adding mapsTo:\t\t{} to {}", mapChildFcToParentDna.getDisplayID(), parentDnaFc.getDisplayId());
        LOGGER.trace("Adding child:\t\t{} has child {}", parentSvm.getDisplayId(), childSvm.getDisplayId());

        b = updateConstraintsFromInserted(b, parentDnaDef, childComponentId);

        //b.addAction(new UpdateTransWires(parentSvm.getIdentity(), workspace, contexts));

        return b.build("AddSvm ["+ childSvm.getDisplayId() + "]");

    }
}
