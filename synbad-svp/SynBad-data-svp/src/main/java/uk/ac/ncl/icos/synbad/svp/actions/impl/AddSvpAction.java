/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.SBActionMacro;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.svp.actions.SvpActions;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;

/**
 *
 * @author owengilfellon
 */
public class AddSvpAction extends AAddChildAction {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSvpAction.class);
    protected final Map<URI, URI> foundObjects = new HashMap<>();
    protected final Map<URI, SBAction> expectedObjects = new HashMap<>();
    private URI preceded = null;

    
    public AddSvpAction(SBGraphEventType type, URI parentSvmIdentity, URI childSvpIdentity, int index, SBWorkspace workspace, URI[] contexts) {
        super(type, parentSvmIdentity, childSvpIdentity, index, workspace, contexts);
    }
    
    public AddSvpAction(SBGraphEventType type, URI parentSvmIdentity, URI childSvpIdentity, URI precededComponentIdentity, SBWorkspace workspace, URI[] contexts) {
        super(type, parentSvmIdentity, childSvpIdentity, -1, workspace, contexts);
        this.preceded = precededComponentIdentity;
    }
    
    @Override
    public URI getPrecededIdentity() {
        if(preceded == null) {
           // LOGGER.error("Preceded has not been calculate yet.");
        }
        return preceded;
    }
    

    @Override
    protected SBAction getAction(SBWorkspace workspace) {

        // SVM and SVP objects

        SvpModule parentSvm = workspace.getObject(parentIdentity, SvpModule.class, contexts);
        Svp childSvp = workspace.getObject(childIdentity, Svp.class, contexts);

        if(parentSvm == null) {
            LOGGER.error("Parent is null");
            return null;
        }

        if(childSvp == null) {
            LOGGER.error("Child is null");
            return null;
        }

        LOGGER.debug("Adding {} to {}", childSvp.getDisplayId(), parentSvm.getDisplayId());

        // ComponentDefinitions associated with SVM and SVP, and
        // ModuleDefinition from SVM

        ModuleDefinition svmModDef = parentSvm.as(ModuleDefinition.class).get();
        ComponentDefinition svmDnaDef = parentSvm.getDnaDefinition();
        ComponentDefinition svpDnaDef = workspace.getObject(childSvp.getIdentity(), ComponentDefinition.class, contexts);

        if(svmDnaDef == null || svpDnaDef == null || svmModDef == null) {
            LOGGER.error("Parent or child CD or MD is null!");
            return null;
        }
        
        List<Component> components = svmDnaDef.getOrderedComponents();
            
        if(preceded == null && 0 <= index && index < components.size()) {
            preceded = components.get(index).getIdentity();
        }

        // Get identities for objects we've retrieved

        SBIdentity svmModDefId = workspace.getIdentityFactory().getIdentity(svmModDef.getIdentity());
        SBIdentity svmDnaDefId = workspace.getIdentityFactory().getIdentity(svmDnaDef.getIdentity());
        SBIdentity svpDnaDefId = workspace.getIdentityFactory().getIdentity(svpDnaDef.getIdentity());

        FunctionalComponent svpFcTu = parentSvm.getDnaInstance();
        SBIdentity svpFcTuId = workspace.getIdentityFactory().getIdentity(svpFcTu.getIdentity());

        // Create Identities for objects that we will create

        SBIdentity svpDnaComponentId = getUniqueIdentity(workspace, Component.getComponentIdentity(svmDnaDefId, svpDnaDefId));
        SBIdentity svpDnaFunctionalComponentId = getUniqueIdentity(workspace, FunctionalComponent.getFunctionalComponentIdentity(svmModDefId, svpDnaDefId));

        // Create Component within parent's DNA ComponentDefinition,
        // FunctionalComponent within parent's ModuleDefinition, and a
        // mapping between the two

        SbolActionBuilder b = new SbolActionBuilder(workspace, contexts);

            // Component within ComponentDefintion describing module's DNA

            

        SBIdentity mtId = createMapsTo(svpFcTuId, svpDnaFunctionalComponentId, svpDnaComponentId, workspace);

        LOGGER.trace("Adding component:\t\t{}", svpDnaComponentId.getDisplayID());
        LOGGER.trace("Adding funcComp:\t\t{}", svpDnaFunctionalComponentId.getDisplayID());

        b = updateConstraintsFromInserted(b, svmDnaDef, svpDnaComponentId);

        svmModDef.getFunctionalComponents().stream().map(fc -> fc.getIdentity()).forEach(uri -> {
            foundObjects.put(uri, SBIdentityHelper.getURI(FunctionalComponent.TYPE));
        });

        svmModDef.getInteractions().stream().flatMap(i -> i.getParticipants().stream()).map(p -> p.getIdentity()).forEach(uri -> {
            foundObjects.put(uri, SBIdentityHelper.getURI(Participation.TYPE));
        });

        svmModDef.getFunctionalComponents().stream().flatMap(fc -> fc.getMapsTo().stream()).map(mt -> mt.getIdentity()).forEach(uri -> {
            foundObjects.put(uri, SBIdentityHelper.getURI(MapsTo.TYPE));
        });

        svmModDef.getModules().stream().flatMap(m -> m.getMapsTo().stream()).map(mt -> mt.getIdentity()).forEach(uri -> {
            foundObjects.put(uri, SBIdentityHelper.getURI(MapsTo.TYPE));
        });

        updateInteractions(svmModDef, childSvp, svpDnaFunctionalComponentId.getIdentity());
        
        b.createComponent(svpDnaComponentId.getIdentity(), svmDnaDef, svpDnaDef, SbolAccess.PUBLIC)

            // FunctionalComponent within moduleDefinition

            .createFuncComponent(svpDnaFunctionalComponentId.getIdentity(), svpDnaDef, svmModDef, SbolDirection.IN, SbolAccess.PUBLIC)
            .createValue(svpDnaFunctionalComponentId.getIdentity(),URI.create(SynBadTerms.Rdf.hasType), SBValue.parseValue(URI.create(SvpInstance.TYPE)), URI.create(SvpModule.TYPE))
            
            .createEdge(parentIdentity, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), childIdentity)

            // addAction rather than createEdge to use system context

            .addAction(
                new CreateEdge(
                    svpDnaFunctionalComponentId.getIdentity(), 
                    SynBadTerms.SynBadSystem.dependantOn, 
                    svpDnaComponentId.getIdentity(),
                        workspace.getIdentity(), workspace.getSystemContextIds(contexts)));
        
        // b.addAction(new UpdateTransWires(parentSvm.getIdentity(), workspace, contexts));

        createActions(b);
        foundObjects.clear();
        expectedObjects.clear();
        
        if(preceded != null)
            LOGGER.trace("Adding {} preceding {}", childSvp.getDisplayId(), preceded.toASCIIString());
        
        return b.build();

    }
    
    private void createActions(SbolActionBuilder b) {

        for(URI id : expectedObjects.keySet()) {
            //LOGGER.debug("Expected: {}", id.toASCIIString());
            if(!foundObjects.keySet().contains(id)) {
                SBAction a = expectedObjects.get(id);
                if(a != null) {
                    b = b.addAction(a);
                    //LOGGER.debug("Adding: {}", id);
                }
            }
        }
    }
    
    /**
     * Process internal interactions and add required interactions and functional components

     */
    private void updateInteractions(
            ModuleDefinition svmModDef, 
            Svp childSvp,
            URI svpDnaFc) {
        
        childSvp.getInternalInteractions().stream()
            .flatMap(svi -> svi.getParticipants().stream())
            .filter(p -> p.getNameInMath().equals("EnvironmentalConstant"))
            .forEach(svpInteractionParticipant -> {

                ComponentDefinition participantDefinition = (ComponentDefinition)svpInteractionParticipant.getSbolParticipant();

                if(participantDefinition == null) {
                    LOGGER.warn("Couldn't find SVP for {}", svpInteractionParticipant.getDisplayId());
                } else {

                    SBIdentity fcId = createFunctionalComponent(
                                svpInteractionParticipant.getInteraction(), 
                                svpInteractionParticipant, 
                                svmModDef,
                                svpDnaFc);

                    LOGGER.trace("Adding FC: {}",  fcId.getDisplayID());
                }
            });
    }
    
    
     private SBIdentity createMapsTo(SBIdentity owner, SBIdentity localId, SBIdentity remoteId, SBWorkspace workspace) {
        //try {

            SBIdentity mtId = MapsTo.getMapsToIdentity(owner, localId, remoteId, MapsToRefinement.verifyIdentical);
            /*SBIdentity mtId = ws.getIdentityFactory().getIdentity(
                    owner.getUriPrefix(),
                    owner.getDisplayID(),
                    "mt_" + localId.getDisplayID() + "_to_" + remoteId.getDisplayID(),
                    owner.getVersion());
            */
            // create mapsTo to map owner to dna
            
            if(expectedObjects.containsKey(mtId.getIdentity()))
                return mtId;
                
            LOGGER.trace("Adding mapsTo:\t\t{}", mtId.getDisplayID());
                
            expectedObjects.put(mtId.getIdentity(), new SBActionMacro(Arrays.asList(
                    MapsTo.createMapsTo(
                            mtId.getIdentity(),
                            owner.getIdentity(),
                            SBIdentityHelper.getURI(FunctionalComponent.TYPE),
                            MapsToRefinement.verifyIdentical,
                            remoteId.getIdentity(),
                            localId.getIdentity(),
                            workspace,
                            contexts)
                    )
                )
            );

            return mtId;
//        } catch (SBIdentityException ex) {
//            LOGGER.error(ex.getLocalizedMessage());
//            return null;
//        }
    }
    
    private SBIdentity createInteractionAndExtends(ModuleDefinition parentMd, SvpInteraction definition) {
        
        SBIdentity interactionIdentity = getUniqueIdentity(
            parentMd.getWorkspace(),
            parentMd.getWorkspace().getIdentityFactory().getIdentity(
                definition.getPrefix(),
                parentMd.getDisplayId(),
                "i_" + definition.getDisplayId(),
                definition.getVersion()
            )
        );
 
        
        if(expectedObjects.containsKey(interactionIdentity.getIdentity()))
            return interactionIdentity;
                    
        LOGGER.debug("Adding interaction:\t{}", interactionIdentity.getDisplayID());

        expectedObjects.put(interactionIdentity.getIdentity(),
            new SBActionMacro(
                Arrays.asList(Interaction.createInteraction(interactionIdentity.getIdentity(),
                        Collections.singleton(definition.getSbolInteractionType()),
                        parentMd,
                        contexts),
                    SvpActions.AddExtends(definition.getIdentity(),
                        interactionIdentity.getIdentity(),
                        parentMd.getWorkspace(),
                        contexts)//,
                 /*   new CreateEdgeAction(interactionIdentity.getIdentity(),
                        SynBadTerms.SynBadSystem.dependantOn,
                        definition.getIdentity(), 
                        ws.getIdentity(), new URI[]{ws.getSystemContext()})))*/)));

        return interactionIdentity;
    }

    private SBIdentity createParticipation(SBIdentity interactionIdentity, SBIdentity fcId, SvpParticipant svpInteractionParticipant) {
        
        SBIdentity participationId = Participation.getParticipationIdentity(interactionIdentity, fcId);

        if(expectedObjects.containsKey(participationId.getIdentity()))
            return participationId;
            
        LOGGER.trace("Adding participation:\t{}", participationId.getDisplayID());

        expectedObjects.put(participationId.getIdentity(),         
            new SbolActionBuilder(svpInteractionParticipant.getWorkspace(), contexts)
                .createParticipation(
                    participationId.getIdentity(),
                    interactionIdentity.getIdentity(),
                    fcId.getIdentity(),
                    svpInteractionParticipant.getInteractionRole())
                .addParticipation(interactionIdentity.getIdentity(), participationId.getIdentity())
                .createEdge(participationId.getIdentity(), 
                    SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.extensionOf), 
                    svpInteractionParticipant.getIdentity())
                .createEdge(participationId.getIdentity(), 
                    SBIdentityHelper.getURI(SynBadTerms.SynBadSystem.dependantOn), 
                    fcId.getIdentity()).build());
     
        
        return participationId;
    }
    
    private SBIdentity createFunctionalComponent(SvpInteraction svi, SvpParticipant participantDefinition, ModuleDefinition md_svpModuleDefinition, URI dnaFc)  {
 
        ComponentDefinition definition = participantDefinition.getSbolParticipant();
   
        SBWorkspace ws = svi.getWorkspace();
       
        SBIdentity fcId = FunctionalComponent.getFunctionalComponentIdentity(
                ws.getIdentityFactory().getIdentity(md_svpModuleDefinition.getIdentity()), 
                ws.getIdentityFactory().getIdentity(definition.getIdentity()));
        
        if(expectedObjects.containsKey(fcId.getIdentity()))
            return fcId;
            
        LOGGER.trace("Adding funcComp:\t\t{}", fcId.getDisplayID());

        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(ws, contexts)
            .addAction(FunctionalComponent.createFuncComponent(
                fcId.getIdentity(),
                participantDefinition.getSbolParticipant().getIdentity(),
                md_svpModuleDefinition.getIdentity(),
                SbolAccess.PUBLIC,
                SbolDirection.OUT,
                ws, contexts))
            .addAction(new CreateEdge(
                fcId.getIdentity(),
                SynBadTerms.SynBadSystem.dependantOn,
                svi.getIdentity(), ws.getIdentity(),
                ws.getSystemContextIds(contexts)))
            .addAction(new CreateEdge(
                fcId.getIdentity(),
                SynBadTerms.SynBadSystem.dependantOn,
                dnaFc, ws.getIdentity(),
                ws.getSystemContextIds(contexts)));
        
        
        /*if(participantDefinition.getDirection() == ParticipationRole.Output) {
            b.addAction(new CreateEdgeAction(fcId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, interactionId.getIdentity(), workspace, ws.getSystemContextIds(contexts)));
        }*/

        expectedObjects.put(fcId.getIdentity(), b.build());
        return fcId;
    }

    
}
