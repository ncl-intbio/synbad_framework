/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view.obj;

import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.view.SBPView;
import uk.ac.ncl.icos.synbad.flow.view.object.DefaultViewWire;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * Represents a point of connection in the model. MapsTo and SVP
 * ports are examples of explicit ports, edges between components
 * and interactions are between implicit ports.
 * 
 * Implicit ports could be derived from and enforce the data model.
 * For example, a component definition provides 0-* component ports.
 * Provides 1 empty port, and if that is taken generates another empty
 * port. If there is an upper limit (i.e. 0-1), then it limits the number of
 * generated ports. IF removed, leave at least 1 empty port.
 * 
 * 
 * 
 * @author owengilfellon
 */

public class SvpWire extends DefaultViewWire<SvpViewPort, SBPortInstance, SynBadEdge> {

    public SvpWire(SynBadEdge edge, SvpViewPort from, SvpViewPort to, SBPView view) {
        super(edge, from, to, view);
    }

    public SvpWire(SynBadEdge edge, SvpViewPort from, SvpViewPort to,  long id, SBPView view) {
        super(edge, from, to, id, view);
    }

    @Override
    public String toString() {
        if(getFrom().getOwner() != null && getTo().getOwner() != null)
            return getFrom().getOwner().getDisplayId() + " -> " + getTo().getOwner().getDisplayId();
        else {
            return getEdge().toString();
        }
    }

    public static SBIdentity getWireIdentity(SBIdentity parentsvm, SBIdentity from, SBIdentity to) {

        String fromId =  SBIdentityHelper.getParentChildDisplayId(from).replaceAll("/", "_");
        String toId = SBIdentityHelper.getParentChildDisplayId(to).replaceAll("/", "_");

        SBIdentity identity = SBIdentity.getIdentity(
                parentsvm.getUriPrefix(),
                parentsvm.getDisplayID(),
                fromId + "_to_" + toId,
                parentsvm.getVersion());

        return identity;
    }
    
    
}
