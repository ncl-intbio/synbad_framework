/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.impl;

import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.view.SBViewFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = SBViewFactory.class)
public class DefaultSvpViewFactory implements SBViewFactory<SvpModel, SvpModule, SvpPView>{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSvpViewFactory.class);
    
    @Override
    public SvpPView getView( SvpModule rootNode, SvpModel model ) {  
        SvpPView view = new SvpPView( 
                rootNode,
                model );
       // view.addUpdater( new SvpPortViewUpdater( view, view.getRoot() ) );
        if(LOGGER.isDebugEnabled())
            LOGGER.debug( "Constructed view: {}", view.getClass().getSimpleName() );
        return view;
    }

    @Override
    public Class<SvpModel> getModelClass() {
        return SvpModel.class;
    }

    @Override
    public Class<SvpModule> getRootNodeClass() {
        return SvpModule.class;
    }

    @Override
    public Class<SvpPView> getViewClass() {
        return SvpPView.class;
    }
    
}
