/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class SinkUnitBuilder {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(SinkUnitBuilder.class);

    private final SBWorkspace workspaceId;
    private final URI[] contexts;
    private final SBIdentity identity;
    private SBIdentity cdsId;
    private Double translationRate = null;
    private Double degradationRate = null;

    private SBIdentity smlMolId = null;
    private SBIdentity complexId = null;
    private Double complexationRate = null;
    private Double disassociationRate = null;
    private Double complexDegradationRate = null;
    
    protected final Random RANDOM = new Random();
    private final CelloActionBuilder b;
   
    
    //private final InteractionMode mode;

    public SinkUnitBuilder(CelloActionBuilder b, SBIdentity identity, URI[] contexts) {
        this.b = b;
        this.identity = identity;
        this.workspaceId = b.getWorkspace();
        this.contexts = contexts;
    }

    public SinkUnitBuilder(SBIdentity identity, SBWorkspace workspaceId, URI[] contexts) {
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.b = null;
    }


    public SinkUnitBuilder setTranslationRate(double translationRate) {
        this.translationRate = translationRate;
        return this;
    }
    
     public SinkUnitBuilder setDegradationRate(double degradationRate) {
        this.degradationRate = degradationRate;
        return this;
    }

    public SinkUnitBuilder setCdsIdentity(SBIdentity cdsIdentity) {
        this.cdsId = cdsIdentity;
        return this;
    }

    public SinkUnitBuilder addComplexationSmallMolecule(SBIdentity smlMolId, SBIdentity complexId, double complexationRate, double disassociationRate, double complexDegradationRate) {
        this.smlMolId = smlMolId;
        this.complexationRate = complexationRate;
        this.complexId = complexId;
        this.disassociationRate = disassociationRate;
        this.complexDegradationRate = complexDegradationRate;
        return  this;
    }
    
    public double getRandomParameter() {
        return RANDOM.nextDouble();
    }
   
  
    public SBAction build() {
     
        if(translationRate == null)
            translationRate = getRandomParameter();
        
        if(degradationRate == null)
            degradationRate = getRandomParameter();

        if(complexId != null) {

            if(complexationRate == null)
                complexationRate = getRandomParameter();

            if(complexDegradationRate == null)
                complexDegradationRate = getRandomParameter();

            if(disassociationRate == null)
                disassociationRate = getRandomParameter();

            SBAction a =  TuActionsFactory.createPopsSinkWithComplexation(identity, cdsId, smlMolId, complexId, translationRate, degradationRate, complexationRate, complexDegradationRate, disassociationRate, workspaceId, contexts);
            LOGGER.trace("Returning generator: {}", identity.getDisplayID());
            return a;
        } else {
            SBAction a =  TuActionsFactory.createPopsSink(identity, cdsId, translationRate, degradationRate, workspaceId, contexts);
            LOGGER.trace("Returning sink: {}", identity.getDisplayID());
            return a;
        }
        

 
    }
    
    public SBCelloBuilder builder() {
        SBAction a = build();
        if(a == null) {
            LOGGER.error("Action is null");
            return null;
        }

        if( b == null) {
            LOGGER.trace("Builder is null, returning new builder");
            return new CelloActionBuilder(workspaceId, contexts).addAction(a);
        }

        if (!b.isBuilt()) {
            LOGGER.trace("Adding action to builder");
            b.addAction(a);
        }

        LOGGER.trace("Returning builder");
        return b;
    }
 
}
