/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.model.ASBModel;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SviVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * @author owengilfellon
 */
public class SvpModel extends ASBModel<SBIdentified, SvpPView > implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpModel.class);
    private static final long serialVersionUID = -1322571019565004836L;

    public SvpModel(SBWorkspace workspace, SvpModule module) {
        super(workspace, module);}

    public SvpModel(SBWorkspace workspace, SvpModule module, NamespaceBinding namespace) {
        super(workspace, module, namespace);}

    @Override
    public SvpModule getRootNode() {
        return (SvpModule)super.getRootNode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected SvpPView constructView() {
        return new DefaultSvpViewFactory().getView(getRootNode(), this);
    }
    
    public SvmVo getViewRoot() {
        SvmVo viewRoot = getCurrentView().getRoot();
        return viewRoot;
    }
    
    public List<SvmVo> getSvms() {
        List<SvmVo> allSvms = new ArrayList<>();
        SvpPView currentView = getCurrentView();
        currentView.hierarchyIterator(currentView.getRoot()).forEachRemaining(vc -> {
            if((vc.isClass(SvmInstance.class) || vc.isClass(SvpModule.class)) && SvmVo.class.isAssignableFrom(vc.getClass())) {
                SvmVo svm = vc.getAsSubclass(SvmVo.class);
                if(svm != null) {
                    allSvms.add(svm);
                }
            }
        });
        
        return allSvms;
    }

    public List<SviVo> getSvis() {
        return new LinkedList<>(getSvms().stream().flatMap(svmVo -> svmVo.getSvis().stream())
                .collect(Collectors.toSet()));
    }

    public List<SvpVo> getSvps() {
        List<SvpVo> allSvps = new ArrayList<>();
        SvpPView currentView = getCurrentView();
        currentView.hierarchyIterator(currentView.getRoot()).forEachRemaining(vc -> {
            if(vc.isClass(SvpInstance.class) && SvpVo.class.isAssignableFrom(vc.getClass())) {
                SvpVo svp = vc.getAsSubclass(SvpVo.class);
                if(svp != null) {
                    allSvps.add(svp);
                }
            }
        });
        return allSvps;
    }
    
    public List<SvpVo> getSvps(ComponentRole role) {
        List<SvpVo> allSvps = new ArrayList<>();
        SvpPView currentView = getCurrentView();
        currentView.hierarchyIterator(currentView.getRoot()).forEachRemaining(vc -> {
            if(vc.isClass(SvpInstance.class) && SvpVo.class.isAssignableFrom(vc.getClass())) {
                SvpVo svp = vc.getAsSubclass(SvpVo.class);
                if(svp != null && svp.getPartTypeAsRole() == role) {
                    allSvps.add(svp);
                }
            }
        });
        
        return allSvps;
    }

//    @Override
//    public String toString() {
//        return Streams.stream(getCurrentView().hierarchyIterator(getViewRoot()))
//                .filter(o -> o.is(SvpInstance.TYPE))
//                .map(o -> o.getAsSubclass(SvpVo.class))
//                .map(o -> o.getSvp().getDisplayId())
//                .reduce("", (a, b) -> a.concat(a.isEmpty() ? b : "; ".concat(b)));
//    }
}