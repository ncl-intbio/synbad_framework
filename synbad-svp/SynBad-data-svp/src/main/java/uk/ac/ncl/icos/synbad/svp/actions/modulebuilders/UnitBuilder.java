/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class UnitBuilder {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(UnitBuilder.class);
    protected final CelloActionBuilder b;
    protected final SBWorkspace workspaceId;
    protected final URI[] contexts;
    protected final Random RANDOM = new Random();
   
    protected Double transcriptionRate = null;
    protected Double translationRate1 = null;
    protected Double degradationRate1 = null;
    protected Double dnaBinding = null;
    protected Double n = null;
    
    protected final SBIdentity identity;
    protected SBIdentity cdsId = null;
    protected SBIdentity promoterId = null; 
    
    protected final InteractionMode mode;

    public UnitBuilder(CelloActionBuilder b, SBIdentity identity, InteractionMode mode, SBWorkspace workspaceId, URI[] contexts) {
        this.b = b;
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.mode = mode;
    }

    public UnitBuilder(SBIdentity identity, InteractionMode mode, SBWorkspace workspaceId, URI[] contexts) {
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.mode = mode;
        this.b = null;
    }

    public UnitBuilder setTranscriptionRate(double transcriptionRate) {
        this.transcriptionRate = transcriptionRate;
        return this;
    }
    
    public UnitBuilder setTranslationRate(double translationRate1) {
        this.translationRate1 = translationRate1;
        return this;
    }

    public UnitBuilder setDegradationRate(double degradationRate1) {
        this.degradationRate1 = degradationRate1;
        return this;
    }
    
    public UnitBuilder setDnaBindingRate(double dnaBindingRate) {
        this.dnaBinding = dnaBindingRate;
        return this;
    }
    
    public UnitBuilder setN(double n) {
        this.n = n;
        return this;
    }
    
    public UnitBuilder setCdsIdentity(SBIdentity cdsIdentity) {
        this.cdsId = cdsIdentity;
        return this;
    }
    
    public UnitBuilder setPromoterIdentity(SBIdentity promoterIdentity) {
        this.promoterId = promoterIdentity;
        return this;
    }
    
    public double getRandomParameter() {
        return RANDOM.nextDouble();
    }
  
    public SBAction build() {
        
        if(degradationRate1 == null)
            degradationRate1 = getRandomParameter();
    
        if(dnaBinding == null)
            dnaBinding = getRandomParameter();
        
        if(n == null)
            n = 1.0;
     
        if(transcriptionRate == null)
            transcriptionRate = getRandomParameter();
        
        if(translationRate1 == null)
           translationRate1 = getRandomParameter();
    
        SBAction a =  TuActionsFactory.createPopsModulator( 
                identity, cdsId, promoterId, mode, 
                transcriptionRate, translationRate1, 
                degradationRate1, dnaBinding, n, 
                workspaceId, contexts);

            LOGGER.trace("Returning complexationUnit: {}", identity.getDisplayID());
 
        return a;
    }

    public SBCelloBuilder builder() {
        SBAction a = build();
        if(a == null) {
            LOGGER.error("Action is null");
            return null;
        }

        if( b == null) {
            LOGGER.trace("Builder is null, returning new builder");
            return new CelloActionBuilder(workspaceId, contexts).addAction(a);
        }

        if (!b.isBuilt()) {
            LOGGER.trace("Adding action to builder");
            b.addAction(a);
        }

        LOGGER.trace("Returning builder");
        return b;
    }
 
}
