/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import java.net.URI;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;


/**
 *
 * @author owengilfellon
 */
public class ComponentFactory {

    public static SBAction createProtein(URI identity,  SynBadPortState state, SBWorkspace workspace, URI[] contexts) {
        return new SbolActionBuilder(workspace, contexts)
            .createComponentDefinition(identity, new Type[] { ComponentType.Protein },  new Role[] {})
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SynBadPort.hasPortState), SBValue.parseValue(state.getUri()), SBIdentityHelper.getURI(ComponentDefinition.TYPE)).build();
    }
    
    public static SBAction createDna(URI identity, Set<Role> roles, SBWorkspace workspace, URI[] contexts) {
        return new SbolActionBuilder(workspace, contexts)
            .createComponentDefinition(identity, new Type[] { ComponentType.DNA }, roles.toArray(new Role[] {})).build();
    }
    
    public static SBAction createSmallMolecule(URI identity, SBWorkspace workspace, URI[] contexts) {
        return new SbolActionBuilder(workspace, contexts)
            .createComponentDefinition(identity, new Type[] { ComponentType.SmallMolecule }, new Role[] {}).build();
    }
    
    public static SBAction createMrna(URI identity, SBWorkspace workspace, URI[] contexts) {
        return new SbolActionBuilder(workspace, contexts)
            .createComponentDefinition(identity, new Type[] { ComponentType.mRNA },  new Role[] {}).build();
    }
}
