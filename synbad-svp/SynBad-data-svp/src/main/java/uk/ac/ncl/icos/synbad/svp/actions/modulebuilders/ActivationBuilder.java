/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class ActivationBuilder {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(ActivationBuilder.class);

    private final SBWorkspace workspaceId;
    private final URI[] contexts;
    private final SBIdentity identity;
    private SBIdentity modulator;
    private SynBadPortType modulatorType;
    private SynBadPortState modulatorState;
    private SBIdentity modulatorOwner;
    private SBIdentity portOwner;
    private SBIdentity modulated;
    private Double km = null;
    private Double n = null;
    
    protected final Random RANDOM = new Random();

    private final CelloActionBuilder b;
   
    
    //private final InteractionMode mode;

    public ActivationBuilder(CelloActionBuilder b, SBIdentity identity, URI[] contexts) {
        this.b = b;
        this.identity = identity;
        this.workspaceId = b.getWorkspace();
        this.contexts = contexts;
    }

    public ActivationBuilder(SBIdentity identity, SBWorkspace workspaceId, URI[] contexts) {
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.b = null;
    }


    public ActivationBuilder setKm(double km) {
        this.km = km;
        return this;
    }
    
     public ActivationBuilder setN(double n) {
        this.n = n;
        return this;
    }

    /**
     * @param modulator - the CDS SVP that contains the modulator
     * @return
     */
    public ActivationBuilder setModulator(SBIdentity modulator) {
        this.modulator = modulator;
        return this;
    }

    /**
     *
     * @param modulated - the promoter SVP that is being regulated
     * @return
     */
    public ActivationBuilder setModulated(SBIdentity modulated) {
        this.modulated = modulated;
        return this;
    }

    /**
     *
     * @param modulatorOwner - the SVM that contains the modulator SVP
     * @return
     */
    public ActivationBuilder setModulatorOwner(SBIdentity modulatorOwner) {
        this.modulatorOwner = modulatorOwner;
        return this;
    }
    
    public ActivationBuilder setModulatorType(SynBadPortType type) {
        this.modulatorType = type;
        return this;
    }
    
     public ActivationBuilder setModulatorState(SynBadPortState state) {
        this.modulatorState = state;
        return this;
    }

    /**
     * @return
     */
    public ActivationBuilder setPortOwner(SBIdentity portOwner) {
        this.portOwner = portOwner;
        return this;
    }
    
    
    
    public double getRandomParameter() {
        return RANDOM.nextDouble();
    }
   
  
    public SBAction build() {
        
        if(modulatorOwner == null) {
            LOGGER.error("Modulator owner cannot be null");
            return null;
        }
        
        if(modulator == null) {
            LOGGER.error("Modulator cannot be null");
            return null;
        }
        
        if(modulated == null) {
            LOGGER.error("Modulated cannot be null");
            return null;
        }
     
        if(portOwner == null)
            portOwner = modulator;
        
        if(km == null)
            km = getRandomParameter();
        
        if(n == null)
            n = getRandomParameter();
        
        if(modulatorState == null)
            modulatorState = SynBadPortState.Default;
        
        if(modulatorType == null)
            modulatorType = SynBadPortType.Protein;
        
        SBAction a =  TuActionsFactory.createActivateUnit(
                identity, modulatorOwner.getIdentity(), portOwner.getIdentity(), 
                modulator.getIdentity(), modulatorType, modulatorState, modulated.getIdentity(), 
                km, n, workspaceId, contexts);
        LOGGER.trace("Returning activation: {}", identity.getDisplayID());
 
        return a;
    }
    
    public SBCelloBuilder builder() {
        SBAction a = build();
        if(a == null) {
            LOGGER.error("Action is null");
            return null;
        }

        if( b == null) {
            LOGGER.trace("Builder is null, returning new builder");
            return new CelloActionBuilder(workspaceId, contexts).addAction(a);
        }

        if (!b.isBuilt()) {
            LOGGER.trace("Adding action to builder");
            b.addAction(a);
        }

        LOGGER.trace("Returning builder");
        return b;
    }
 
}
