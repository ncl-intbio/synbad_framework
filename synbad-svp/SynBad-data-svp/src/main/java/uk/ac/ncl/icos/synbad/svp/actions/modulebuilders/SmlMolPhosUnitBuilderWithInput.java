/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class SmlMolPhosUnitBuilderWithInput extends UnitBuilder {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(SmlMolPhosUnitBuilderWithInput.class);
  
    private SBIdentity smlMolId = null;
    private SBIdentity cds2Id = null; 
 
    private Double phosphorylationRate1 = null;
    private Double phosphorylatedDegradationRate1 = null;
    private Double dephosphorylationRate1 = null;
    
    private Double translationRate2 = null;
    private Double degradationRate2 = null;
    private Double phosphorylationRate2 = null;
    private Double phosphorylatedDegradationRate2 = null;
    private Double dephosphorylationRate2 = null;
    
    public SmlMolPhosUnitBuilderWithInput(CelloActionBuilder b, SBIdentity identity, InteractionMode mode, SBWorkspace workspaceId, URI[] contexts) {
        super(b, identity, mode, workspaceId, contexts);
    }

    public SmlMolPhosUnitBuilderWithInput(SBIdentity identity, InteractionMode mode, SBWorkspace workspaceId, URI[] contexts) {
        super(identity, mode, workspaceId, contexts);
    }

    @Override
    public SmlMolPhosUnitBuilderWithInput setTranscriptionRate(double transcriptionRate) {
        this.transcriptionRate = transcriptionRate;
        return this;
    }
    
    @Override
    public SmlMolPhosUnitBuilderWithInput setTranslationRate(double translationRate1) {
        this.translationRate1 = translationRate1;
        return this;
    }
    
    @Override
     public SmlMolPhosUnitBuilderWithInput setDegradationRate(double degradationRate1) {
        this.degradationRate1 = degradationRate1;
        return this;
    }
     
    @Override
    public SmlMolPhosUnitBuilderWithInput setDnaBindingRate(double dnaBindingRate) {
        this.dnaBinding = dnaBindingRate;
        return this;
    }

    @Override
    public SmlMolPhosUnitBuilderWithInput setN(double n) {
        this.n = n;
        return this;
    }

    @Override
    public SmlMolPhosUnitBuilderWithInput setPromoterIdentity(SBIdentity promoterIdentity) {
        return (SmlMolPhosUnitBuilderWithInput) super.setPromoterIdentity(promoterIdentity);
    }

    @Override
    public SmlMolPhosUnitBuilderWithInput setCdsIdentity(SBIdentity cdsId) {
        this.cdsId = cdsId;
        return this;
    }

    public SmlMolPhosUnitBuilderWithInput setTranslationRate2(double translationRate2) {
        this.translationRate2 = translationRate2;
        return this;
    }

    public SmlMolPhosUnitBuilderWithInput setDegradationRate2(double degradationRate2) {
        this.degradationRate2 = degradationRate2;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setPhosphorylationRate1(double phosphorylationRate1) {
        this.phosphorylationRate1 = phosphorylationRate1;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setPhosphorylationRate2(double phosphorylationRate2) {
        this.phosphorylationRate2 = phosphorylationRate2;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setPhosphorylatedDegradationRate1(double phosphorylatedDegradationRate1) {
        this.phosphorylatedDegradationRate1 = phosphorylatedDegradationRate1;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setPhosphorylatedDegradationRate2(double phosphorylatedDegradationRate2) {
        this.phosphorylatedDegradationRate2 = phosphorylatedDegradationRate2;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setDephosphorylationRate1(double dephosphorylationRate1) {
        this.dephosphorylationRate1 = dephosphorylationRate1;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setDephosphorylationRate2(double dephosphorylationRate2) {
        this.dephosphorylationRate2 = dephosphorylationRate2;
        return this;
    }
    
    public SmlMolPhosUnitBuilderWithInput setCds2Identity(SBIdentity cds2Id) {
        this.cds2Id = cds2Id;
        return this;
    }

    public SmlMolPhosUnitBuilderWithInput setSmlMolIdentity(SBIdentity smlMolId) {
        this.smlMolId = smlMolId;
        return this;
    }
    
   
    @Override
    public SBAction build() {
        
        if(degradationRate1 == null)
            degradationRate1 = getRandomParameter();
        
        if(degradationRate2 == null)
            degradationRate2 = getRandomParameter();
        
        if(dephosphorylationRate1 == null)
            dephosphorylationRate1 = getRandomParameter();
        
        if(dephosphorylationRate2 == null)
            dephosphorylationRate2 = getRandomParameter();
        
        if(dnaBinding == null)
            dnaBinding = getRandomParameter();
        
        if(n == null)
            n = 1.0;
        
        if(phosphorylatedDegradationRate1 == null)
            phosphorylatedDegradationRate1 = getRandomParameter();
        
        if(phosphorylatedDegradationRate2 == null)
            phosphorylatedDegradationRate2 = getRandomParameter();
        
        if(phosphorylationRate1 == null)
            phosphorylationRate1 = getRandomParameter();
        
        if(phosphorylationRate2== null)
            phosphorylationRate2 = getRandomParameter();
        
        if(transcriptionRate == null)
            transcriptionRate = getRandomParameter();
        
        if(translationRate1 == null)
           translationRate1 = getRandomParameter();
        
        if(translationRate2 == null)
           translationRate2 = getRandomParameter();

        SBAction a =  TuActionsFactory.createSmlMolPhosPopsModulator(identity, 
                cdsId, cds2Id, smlMolId, promoterId,
                this.mode,
                transcriptionRate, translationRate1, degradationRate1,
                phosphorylationRate1, phosphorylatedDegradationRate1, dephosphorylationRate1, 
                translationRate2, degradationRate2, phosphorylationRate2, phosphorylatedDegradationRate2, 
                dephosphorylationRate2, dnaBinding, n, workspaceId, contexts);

            LOGGER.trace("Returning SmlMolPhosModulator: {}", identity.getDisplayID());
 
        return a;
    }

        
    
}
