/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.parser;

import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

/**
 * Imports objects from the JParts API into a {@link SBWorkspace}.
 * @author owengilfellon
 */
public class SVPReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(SVPReader.class);    
    private static final SBDataDefManager DEFINITION_MANAGER = SBDataDefManager.getManager();
    
    private static final URI SVP_TYPE = SBIdentityHelper.getURI(Svp.TYPE);
    
    private final String prefix;
    private final URI[] contexts;
    private final String version;

    private final SBWorkspace workspace;
    private final SVPInteractionReader interactionReader;
 
    public SVPReader(SBWorkspace workspace, URI[] contexts)
    {
        this.interactionReader = new SVPInteractionReader(workspace, contexts);
        this.workspace = workspace;
        this.contexts = contexts;
        this.prefix = "http://virtualparts.org";
        this.version = "1.0";
    }

    public SVPReader(SBWorkspace workspace, URI[] contexts, String prefix, String version)
    {
        this.workspace = workspace;
        this.interactionReader = new SVPInteractionReader(workspace, contexts);
        this.contexts = contexts;
        this.prefix = prefix;
        this.version = version;
    }
    
    
    public Svp read(Part part, List<Interaction> internalInteractions)
    {
        LOGGER.debug("Reading part: {}", part.getName());

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(workspace,  contexts);
        SBIdentity svpIdentity = workspace.getIdentityFactory().getIdentity(prefix, part.getName(), version);


        if(workspace.containsObject(svpIdentity.getIdentity(), Svp.class, contexts)) {
            LOGGER.debug("Returing existing object: {}", svpIdentity.getIdentity().toASCIIString());
            return workspace.getObject(svpIdentity.getIdentity(), Svp.class, contexts);
        }

        b = read(b, part, internalInteractions);

        if(!b.isEmpty())
            workspace.perform(b.build("Read SVP [" + part.getName() + "]"));

        Svp svp = workspace.getObject(svpIdentity.getIdentity(), Svp.class, contexts);    
        LOGGER.debug("Created Svp. Returning {}", svp.getDisplayId());
        return svp;
    }
    
    public SvpActionBuilderImpl read(SvpActionBuilderImpl b, Part part, List<Interaction> internalInteractions)
    {
        LOGGER.debug("Reading part: {}", part.getName());
    

        SBIdentity svpIdentity = workspace.getIdentityFactory().getIdentity(prefix, part.getName(), version);

        ComponentRole componentRole = DEFINITION_MANAGER.getDefinition(ComponentRole.class, part.getType());

        if(componentRole == ComponentRole.Promoter) {
            Parameter parameter = internalInteractions.stream().flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("ktr")).findFirst().orElse(null);
            if(parameter != null)
                b = b.createPromoter(svpIdentity, parameter.getValue());
        }

        if(componentRole == ComponentRole.RBS) {
            Parameter parameter = internalInteractions.stream().flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("ktl")).findFirst().orElse(null);
            if(parameter != null)
                b = b.createRBS(svpIdentity, parameter.getValue(), 1.0);
        }

        if(componentRole == ComponentRole.Shim) {
            Parameter parameter = internalInteractions.stream().flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("RiPSEfficiency")).findFirst().orElse(null);

            boolean upstream = internalInteractions.stream()
                    .anyMatch(i -> i.getInteractionType().equals("mRNA Modulation"));

            if(parameter != null) {
                b = upstream ?
                        b.createUpstreamShim(svpIdentity, parameter.getValue()) :
                        b.createDownstreamShim(svpIdentity, parameter.getValue());
            }

        }

        if(componentRole == ComponentRole.Terminator) {
            b = b.createTerminator(svpIdentity);
        }

        if(componentRole == ComponentRole.Operator) {
            b = b.createOperator(svpIdentity);
        }

        if(componentRole == ComponentRole.CDS) {
            
            if(internalInteractions.stream().anyMatch(i -> i.getInteractionType().equals("Phosphorylation") || i.getInteractionType().equals("Dephosphorylation"))) {
                Parameter kd = internalInteractions.stream()
                    .filter(i -> i.getPartDetails().stream().allMatch(p -> !p.getPartForm().equals("Phosphorylated")))
                    .flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("kd")).findFirst().orElse(null);
                Parameter pkd = internalInteractions.stream()
                    .filter(i -> i.getPartDetails().stream().allMatch(p -> p.getPartForm().equals("Phosphorylated")))
                    .flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("kd")).findFirst().orElse(null);
                Parameter kdeP = internalInteractions.stream().flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("kdeP")).findFirst().orElse(null);
                Parameter kf = internalInteractions.stream().flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("kf")).findFirst().orElse(null);

                InteractionPartDetail ec = internalInteractions.stream().flatMap(i -> i.getPartDetails().stream()).filter(i -> i.getMathName().equals("EnvironmentConstant")).findFirst().orElse(null);

                if(ec != null) {
                    SBIdentity smlMolId = workspace.getIdentityFactory().getIdentity(prefix, ec.getPartName(), version);
                      LOGGER.debug("Creating CDS:{} with Small Molecule:{}", svpIdentity.getDisplayID(), smlMolId.getDisplayID());
                       b = b.createCDSWithPhosphorylatingSmallMolecule(svpIdentity, smlMolId, kd.getValue(), kf.getValue(), pkd.getValue(),   kdeP.getValue());
                } else {
                    LOGGER.debug("Creating CDS with phosphorylated:{}", svpIdentity.getDisplayID());
                       b = b.createCDSWthPhosphorylated(svpIdentity, kd.getValue(), pkd.getValue(), kdeP.getValue());
                }
              } else {
                Parameter kd = internalInteractions.stream()
                    .flatMap(i -> i.getParameters().stream())
                    .filter(p -> p.getParameterType().equals("kd")).findFirst().orElse(null);
                
                LOGGER.debug("Creating CDS:{}", svpIdentity.getDisplayID());
                
                b = b.createCDS(svpIdentity, kd.getValue());
            }
        }

        if(part.getType().equalsIgnoreCase("protein complex")) {
            if(internalInteractions.stream().anyMatch(i -> i.getInteractionType().equals("Degradation"))) {

                LOGGER.debug("Creating Protein Complex:{}", svpIdentity.getDisplayID());

                Parameter kd = internalInteractions.stream().flatMap(i -> i.getParameters().stream())
                        .filter(p -> p.getParameterType().equals("kd")).findFirst().orElse(null);
                b = b.createComplex(svpIdentity, kd.getValue());
            }
        }
        // Create SVP (or retrieve....)

        else {
            b = b.createSvp(svpIdentity, Collections.singleton(componentRole), ComponentType.DNA);
            LOGGER.debug("CreateSvpAction: {}:{}", svpIdentity.getDisplayID(), componentRole);
        }



        // Add SVP's properties

        b = readPartProperties(b, svpIdentity.getIdentity(), part);

        return b;
    }
  
    public SvpActionBuilderImpl read(SvpActionBuilderImpl b, Part part)
    {   
        List<Interaction> interactions = SVPManager.getSVPManager().getInternalEvents(part);
        LOGGER.debug("Reading part with {} VPR internal interactions: {}", interactions.size(), part.getName());
        return read(b, part, interactions);
    }
    
    public Svp read(Part part)
    {   
        List<Interaction> interactions = SVPManager.getSVPManager().getInternalEvents(part);
        LOGGER.debug("Reading part with {} VPR internal interactions: {}", interactions.size(), part.getName());
        return read(part, interactions);
    }
    
    private SvpActionBuilderImpl readPartProperties(SvpActionBuilderImpl b, URI svp, Part part) {
       
        SBIdentity identity = workspace.getIdentityFactory().getIdentity(svp);
        
        // Set SVP values

        SBValue type = SBValue.parseValue(part.getType()!= null ? part.getType() : "");
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.partType), type, SVP_TYPE);

        SBValue metaType = SBValue.parseValue(part.getMetaType()!= null ? part.getMetaType() : "");
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.metaType), metaType, SVP_TYPE);

        SBValue name = SBValue.parseValue(part.getDisplayName() != null ? part.getDisplayName() : part.getName());
        LOGGER.debug("Reading name: {}", name);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasName), name, SVP_TYPE);

        SBValue description = SBValue.parseValue(part.getDescription() != null ? part.getDescription() : "");
        LOGGER.debug("Reading description: {}", description);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasDescription), description, SVP_TYPE);

        SBValue sequence = SBValue.parseValue(SBValue.parseValue(part.getSequence() != null ? part.getSequence().replace("![CDATA[", "").replace("]]", "").replace("\n", "") : ""));
        LOGGER.debug("Reading sequence: {}", sequence);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.sequence), sequence, SVP_TYPE);

        SBValue sequenceUri = SBValue.parseValue(part.getSequenceURI() != null ? part.getSequenceURI() : "");
        LOGGER.debug("Reading sequence URI: {}", sequenceUri);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.sequenceUri), sequenceUri, SVP_TYPE);

        SBValue organism = SBValue.parseValue(part.getOrganism() != null ? part.getOrganism() : "");
        LOGGER.debug("Reading organism: {}", organism);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.organism), organism, SVP_TYPE);

        SBValue designMethod = SBValue.parseValue(part.getDesignMethod() != null ? part.getDesignMethod() : "");
        LOGGER.debug("Reading design method: {}", designMethod);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.designMethod), designMethod, SVP_TYPE);

        SBValue status = SBValue.parseValue(part.getStatus() != null ? part.getStatus() : "");
        LOGGER.debug("Reading status: {}", status);
        b = b.createValue(svp, SBIdentityHelper.getURI(SynBadTerms.VPR.status), status, SVP_TYPE);
        
        //svp.setValue(SynBadTerms.VPR.metaType,      SBValue.parseValue(part.getMetaType()     != null ? part.getMetaType() : ""));

        if(part.getProperties()==null)
            return b;

        Set<URI> propertyIds = new HashSet<>();

        for(Property property : part.getProperties()) {
            
            int index = 0;

                SBIdentity pidentity = workspace.getIdentityFactory().getIdentity(
                        identity.getUriPrefix(),
                        identity.getDisplayID() + "/Property_" + property.getName().replaceAll(" ", "") + "_" + index,
                        identity.getVersion());
                
                while(propertyIds.contains(pidentity.getIdentity()) || workspace.containsIdentity(pidentity.getIdentity(), contexts))
                    pidentity = workspace.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "/Property_" + property.getName().replaceAll(" ", "") + "_" + ++index, identity.getVersion());

                propertyIds.add(pidentity.getIdentity());
                String propertyName = property.getName() != null ? property.getName() : "";
                String propertyValue = property.getValue() != null ? property.getValue() : "";
                String propertyDescription = property.getDescription() != null ? property.getDescription() : "";
 
                LOGGER.debug("Reading property: {} : {} : {}", propertyName, propertyValue, propertyDescription);
                b.createProperty(pidentity, svp, SVP_TYPE, propertyName, propertyValue, propertyDescription);
        }

        return b;
    }
    
    private boolean isProduction(SvpInteraction interaction) {
        InteractionType type = interaction.getInteractionType();
        if(type.toString().contains("Production") || type.toString().contains("Phosphorylation") ||
                type.toString().contains("Formation"))
            return true;
        return false;
     }
}
