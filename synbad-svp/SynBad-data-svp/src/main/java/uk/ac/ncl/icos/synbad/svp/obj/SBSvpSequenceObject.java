/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowInstance;
import uk.ac.ncl.icos.synbad.sbol.object.Component;

/**
 *
 * @author owen
 * @param <T> Instance Class
 * @param <V> Svp Definition Class
 */
public interface SBSvpSequenceObject<D extends SBIdentified>  extends SBSvpObj, SBFlowInstance<D> {
    
    
    public Component getMappedToComponent();
    
    public boolean isMappedToComponent();
}
