 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBComponent;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.SviInternalActionFactory;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.sbol.object.TopLevel;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;


 /**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Svp.TYPE)
public class Svp extends ASBComponent implements TopLevel, SvpEntity<ComponentDefinition> {
    
    public static final String TYPE = "http://www.synbad.org/Svp";
    public static final URI SVP_TYPE = SBIdentityHelper.getURI(TYPE);

    // Extend an abstract class that defines hierarchical entities with ports?
    // Can then use same class for defining algorithms or other flow-based models?

    public Svp(SBIdentity identity, SBWorkspace workspace, SBValueProvider valueProvider) {
        super(identity, workspace, valueProvider);
    }
    
    
//    @Override
//    public ComponentDefinition getExtension() {
//        return ws.incomingEdges(this, SynBadTerms.SynBadEntity.extensionOf, ComponentDefinition.class , getContexts()).stream()
//                .map(fc -> ws.getObject(fc.getFrom(), ComponentDefinition.class, getContexts())).findFirst().orElse(null);
//    }
    
    public SvpInteraction createInternalInteraction( SBIdentity identity, Set<InteractionType> types) {
        SvpInteraction i = ws.getObject(identity.getIdentity(), SvpInteraction.class, getContexts());
        if(i!=null)
            return i;
        else if (ws.containsIdentity(identity.getIdentity(), getContexts()))
            return null;
        ws.perform(SvpInteraction.CreateSvi(identity, types, getWorkspace(), getContexts()));
        return ws.getObject(identity.getIdentity(), SvpInteraction.class, getContexts());
    }
    
    public Collection<SvpInteraction> getExternalInteractions() { 
        return getOutgoingObjects(SynBadTerms.SynBadPart.hasInteraction, SvpInteraction.class);
    }
    
    @OwnedEdge(id = SynBadTerms.SynBadPart.hasInternalInteraction, clazz = SvpInteraction.class)
    public Collection<SvpInteraction> getInternalInteractions() {
        return getOutgoingObjects(SynBadTerms.SynBadPart.hasInternalInteraction, SvpInteraction.class);
    } 
    
    @OwnedEdge(id = SynBadTerms.SynBadPart.hasProperty, clazz = Property.class)
    public Collection<Property> getProperties() {
        return getOutgoingObjects(SynBadTerms.SynBadPart.hasProperty, Property.class);
    }
    
    public Property createProperty(String name, String value, String description) {
        SBIdentityFactory f = ws.getIdentityFactory();
        SBIdentity propertyIdentity = f.getUniqueIdentity(f.getIdentity(getPrefix(), getDisplayId(), name, getVersion()));
        ws.perform(Property.CreatePropertyAction(propertyIdentity, getIdentity(), SBIdentityHelper.getURI(Svp.TYPE), name, value, description, getWorkspace(), getContexts()));
        return ws.getObject(propertyIdentity.getIdentity(), Property.class, getContexts());
    }  
    
    public String getSequence() {
        SBValue s = getValue(SynBadTerms.VPR.sequence);
        return s != null ? s.asString() : "";
    }
    
    public ComponentRole getPartTypeAsRole() {
        SBValue s = getValue(SynBadTerms.VPR.partTypeAsRole);
        if(s == null)
            return null;
        ComponentRole role = dataDefinitionManager.getDefinition(ComponentRole.class, s.asString());
        return role;
    }

    public String getPartType() {
        SBValue s = getValue(SynBadTerms.VPR.partType);
        return s != null ? s.asString() : "";
    }

    public String getMetaType() {
        SBValue s = getValue(SynBadTerms.VPR.metaType);
        return s != null ? s.asString() : "";
    }
    
//    public void setMetaType(String metaType) {
//        setValue(SynBadTerms.VPR.metaType, SBValue.parseValue(metaType));
//    }
    
    public String getStatus() {
        SBValue s = getValue(SynBadTerms.VPR.status);
        return s != null ? s.asString() : "";
    }
    
//    public void setStatus(String status) {
//        setValue(SynBadTerms.VPR.status, SBValue.parseValue(status));
//    }
    
    public String getDesignMethod() {
        SBValue s = getValue(SynBadTerms.VPR.designMethod);
        return s != null ? s.asString() : "";
    }
    
//    public void setDesignMethod(String designMethod) {
//        setValue(SynBadTerms.VPR.designMethod, SBValue.parseValue(designMethod));
//    }
 
    public String getOrganism() {
        SBValue s = getValue(SynBadTerms.VPR.organism);
        return s != null ? s.asString() : "";
    }
    
//    public void setOrganism(String organism) {
//        setValue(SynBadTerms.VPR.organism, SBValue.parseValue(this));
//    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    @Override
    public Collection<Role> getSbolRoles() {
        return ws.getObject(getIdentity(), ComponentDefinition.class, getContexts()).getSbolRoles();
    }

    @Override
    public Collection<Type> getSbolTypes() {
        return ws.getObject(getIdentity(), ComponentDefinition.class, getContexts()).getSbolTypes();
    }
    
    public static SvpActionBuilderImpl getOrCreateComponentDefinition(SvpActionBuilderImpl builder, SBIdentity identity, SBIdentity parentId,  Set<Type> types, Set<Role> roles, URI[] contexts) {
        ComponentDefinition cd = builder.getWorkspace().getObject(identity.getIdentity(), ComponentDefinition.class, contexts);
        if(cd != null)
            return builder;

        return builder.addAction(ComponentDefinition.createComponentDefinition(identity.getIdentity(), types, roles, builder.getWorkspace(), contexts))
                .addExtends(parentId, identity);
    }

    public static SBAction CreateSvp( SBIdentity identity, Set<Role> roles, ComponentType type, SBWorkspace workspace, URI[] contexts) {
        SbolActionBuilder b = new  SbolActionBuilder(workspace,  contexts)
            .createObject(identity.getIdentity(), SBIdentityHelper.getURI(Svp.TYPE), null, null);
            URI cdId = SvpIdentityHelper.getComponentIdentity(identity, type, SynBadPortState.Default).getIdentity();
            b.createComponentDefinition(cdId, new Type[] { type }, roles.toArray(new Role[] {}));
            b.createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), cdId);
        return b.build("CreateSvp ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction createPromoter(SBIdentity identity, SBWorkspace ws, double ktr, URI[] contexts) {

        SBIdentityFactory f = ws.getIdentityFactory();
        
        SBIdentity popsProduction = f.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_PoPSProduction", identity.getVersion());
        SBIdentity port_popsIn = SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity port_out_pops = SvpIdentityHelper.getPortIdentity(identity, identity, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);

        //Identity componentDefinition = SvpIdentityHelper.getDnaIdentity(identity);

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);
        b = b.createSvp(identity, Collections.singleton(ComponentRole.Promoter), ComponentType.DNA)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Promoter"), SVP_TYPE)
            .createPoPSProduction(popsProduction, ktr,identity.getIdentity())
            .createPort(port_out_pops, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default, identity.getIdentity(), identity.getIdentity(), SVP_TYPE)
            .createPort(port_popsIn, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE);
        return b.build("CreateSvp(Promoter) ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction CreateUpstreamShimAction(SBIdentity identity, double efficiency, // PortType type, PortState state, URI modulator,
        SBWorkspace ws, URI[] contexts) {

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);
        SBIdentity ripsModulation = ws.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Modulation", identity.getVersion());

        SBIdentity port_in_rips = SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.mRNA, SynBadPortState.Default);
        SBIdentity port_out_rips = SvpIdentityHelper.getPortIdentity(identity, identity, SBPortDirection.OUT, SynBadPortType.mRNA, SynBadPortState.Default);

        b = b.createSvp(identity, Collections.singleton(ComponentRole.Shim), ComponentType.DNA)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Shim"), SVP_TYPE)
            .addAction(SviInternalActionFactory.CreateShimMrnaModulationAction(ripsModulation, identity.getIdentity(), efficiency, ws, contexts))
            .createPort(port_out_rips, SBPortDirection.OUT, 
                        SynBadPortType.mRNA, SynBadPortState.Default, identity.getIdentity(), identity.getIdentity(), SVP_TYPE)
            .createPort(port_in_rips, SBPortDirection.IN, 
                        SynBadPortType.mRNA, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE);

        return b.build("CreateSvp(UpShim) ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction CreateDownstreamShimAction(SBIdentity identity, double efficiency, // PortType type, PortState state, URI modulator,
        SBWorkspace ws, URI[] contexts) {


        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);
        SBIdentity ripsModulation = ws.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Modulation", identity.getVersion());

        SBIdentity port_in_rips = SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default);
        SBIdentity port_out_rips = SvpIdentityHelper.getPortIdentity(identity, identity, SBPortDirection.OUT, SynBadPortType.RiPS, SynBadPortState.Default);

        b = b.createSvp(identity, Collections.singleton(ComponentRole.Shim), ComponentType.DNA)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Shim"), SVP_TYPE)
            .addAction(SviInternalActionFactory.CreateShimRiPSModulationAction(ripsModulation, identity.getIdentity(), efficiency, ws, contexts))
            .createPort(port_out_rips, SBPortDirection.OUT, 
                        SynBadPortType.RiPS, SynBadPortState.Default, identity.getIdentity(), identity.getIdentity(), SVP_TYPE)
            .createPort(port_in_rips, SBPortDirection.IN, 
                        SynBadPortType.RiPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE);

        return b.build("CreateSvp(DownShim) ["+ identity.getDisplayID() + "]");
    }

    public static SBAction CreateOperatorDefinitionAction(SBIdentity identity,// PortType type, PortState state, URI modulator,
        SBWorkspace ws, URI[] contexts) {

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);
        SBIdentity popsModulation = ws.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_PoPSModulation", identity.getVersion());

        SBIdentity port_in_pops = SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity port_out_pops = SvpIdentityHelper.getPortIdentity(identity, identity, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);

        b = b.createSvp(identity, Collections.singleton(ComponentRole.Operator), ComponentType.DNA)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Operator"), SVP_TYPE)
            .addAction(SviInternalActionFactory.CreatePoPSModulationAction(popsModulation, identity.getIdentity(), ws, contexts))
            .createPort(port_out_pops, SBPortDirection.OUT, 
                        SynBadPortType.PoPS, SynBadPortState.Default, identity.getIdentity(), identity.getIdentity(), SVP_TYPE)
            .createPort(port_in_pops, SBPortDirection.IN, 
                        SynBadPortType.PoPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE);

        return b.build("CreateSvp(Operator) ["+ identity.getDisplayID() + "]");

    }
        
    public static SBAction CreateRBSDefinitionAction(SBIdentity identity, SBWorkspace ws, double ktl, double volume, URI contexts[]) {
       
        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);

        SBIdentity mrnaId = ws.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_mRNA", identity.getVersion());
        SBIdentity rips_production = ws.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_RiPSProduction", identity.getVersion());

        b = b.createSvp(identity, Collections.singleton(ComponentRole.RBS), ComponentType.DNA)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("RBS"), SVP_TYPE)
            .createRiPSProduction(rips_production,  ktl, volume, identity.getIdentity())
            .createMrna(mrnaId, identity.getIdentity())
            .createPort(SvpIdentityHelper.getPortIdentity(identity, identity, SBPortDirection.OUT, SynBadPortType.RiPS, SynBadPortState.Default),
                 SBPortDirection.OUT, SynBadPortType.RiPS, SynBadPortState.Default, identity.getIdentity(), identity.getIdentity(), SVP_TYPE)
            .createPort(SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default),
                 SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE)

            .addAction(new CreateEdge(mrnaId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, identity.getIdentity(),  ws.getIdentity(), ws.getSystemContextIds(contexts)));

            return b.build("CreateSvp(RBS) ["+ identity.getDisplayID() + "]");
    }

     public static SBAction CreateCDSWithComplexationSmallMoleculeDefinitionAction(SBIdentity identity, SBIdentity smallMoleculeId, SBIdentity complexIdentity, SBWorkspace ws,
                                                                                      double degradationRate, double complexationRate, double disassociationRate, double complexDegradationRate, URI[] contexts) {

         SBIdentity cd_svp_protein = SvpIdentityHelper.getProteinIdentity(identity);

         SBIdentity svi_proteinProductionId = SvpIdentityHelper.getProductionIdentity(identity);
         SBIdentity svi_proteinDegradationId = SvpIdentityHelper.getDegradationIdentity(identity, SynBadPortState.Default);
         SBIdentity svi_complexationId = SvpIdentityHelper.getComplexFormationIdentity(complexIdentity);
         SBIdentity svi_disassociationId = SvpIdentityHelper.getComplexDisassociationIdentity(complexIdentity);

         SBIdentity port_in_rips = SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default);
         SBIdentity port_out_protein = SvpIdentityHelper.getPortIdentity(identity, cd_svp_protein, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
         SBIdentity port_out_protein_p = SvpIdentityHelper.getPortIdentity(identity, complexIdentity, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);

         SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);

         b = b.createSvp(identity, Collections.singleton(ComponentRole.CDS), ComponentType.DNA)
                 .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("FunctionalPart"), SVP_TYPE)
                 .addAction(SviInternalActionFactory.CreateProteinProductionAction (
                         svi_proteinProductionId, identity.getIdentity(), ws, contexts))
                 .addAction(SviInternalActionFactory.CreateDegradationAction (
                         svi_proteinDegradationId, identity.getIdentity(), ComponentType.Protein, SynBadPortState.Default, degradationRate, ws, contexts))
//                 .addAction(SviInternalActionFactory.CreateDegradationAction (
//                         cd_svp_complex, identity.getIdentity(), ComponentType.Complex, SynBadPortState.Default, degradationRate, ws, contexts))
                 .createPort(port_in_rips, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE)
                 .createPort(port_out_protein, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, cd_svp_protein.getIdentity(), identity.getIdentity(), SVP_TYPE)
                 .addAction(SviInternalActionFactory.CreateSmallMoleculeComplexationAction(
                         identity,
                         svi_complexationId,
                         complexIdentity, smallMoleculeId, ComponentType.SmallMolecule, SynBadPortState.Default,
                         identity, ComponentType.Protein, SynBadPortState.Default, complexationRate, complexDegradationRate, ws, contexts))
                 .addAction(SviInternalActionFactory.CreateComplexDisassociationAction(
                         identity,
                         svi_disassociationId,
                         complexIdentity, smallMoleculeId, ComponentType.SmallMolecule, SynBadPortState.Default,
                         identity, ComponentType.Protein, SynBadPortState.Default,disassociationRate,  ws, contexts))
                 .createPort(port_out_protein_p, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default, SvpIdentityHelper.getComplexIdentity(complexIdentity).getIdentity(), identity.getIdentity(), SVP_TYPE);

         return b.build("CreateSvp(CDSSmlMol) ["+ identity.getDisplayID() + "]");

     }
    
    public static SBAction CreateCDSWithPhosphorylatingSmallMoleculeDefinitionAction(SBIdentity identity, SBIdentity smallMoleculeId, SBWorkspace ws,
                                                                                     double degradationRate, double phosphorylationRate, double phosDegradationRate, double dephosphorylationRate, URI[] contexts) {

        SBIdentity cd_svp_protein = SvpIdentityHelper.getProteinIdentity(identity);
        SBIdentity cd_svp_protein_p = SvpIdentityHelper.getPhosphorylatedProteinIdentity(identity);

        SBIdentity svi_proteinProductionId = SvpIdentityHelper.getProductionIdentity(identity);
        SBIdentity svi_proteinDegradationId = SvpIdentityHelper.getDegradationIdentity(identity, SynBadPortState.Default);
        SBIdentity svi_proteinPhosId = SvpIdentityHelper.createSmlMolPhosphorylationIdentity(smallMoleculeId, identity);
        SBIdentity svi_proteinDephosId = SvpIdentityHelper.getDePhosphorylationInteractionIdentity(identity);
        SBIdentity svi_proteinPDegId = SvpIdentityHelper.getDegradationIdentity(identity, SynBadPortState.Phosphorylated);

        SBIdentity port_in_rips = SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default);
        SBIdentity port_out_protein = SvpIdentityHelper.getPortIdentity(identity, cd_svp_protein, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
        SBIdentity port_out_protein_p = SvpIdentityHelper.getPortIdentity(identity, cd_svp_protein_p, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated);

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(ws, contexts);

        b = b.createSvp(identity, Collections.singleton(ComponentRole.CDS), ComponentType.DNA)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("FunctionalPart"), SVP_TYPE)
            .addAction(SviInternalActionFactory.CreateProteinProductionAction (
                    svi_proteinProductionId, identity.getIdentity(), ws, contexts))
            .addAction(SviInternalActionFactory.CreateSmallMoleculePhosphorylationAction (
                    svi_proteinPhosId, identity.getIdentity(), smallMoleculeId.getIdentity(), phosphorylationRate, ws, contexts))
            .addAction(SviInternalActionFactory.CreateDephosphorylationAction (
                    svi_proteinDephosId, identity.getIdentity(), dephosphorylationRate, ws, contexts))
            .addAction(SviInternalActionFactory.CreateDegradationAction (
                    svi_proteinDegradationId, identity.getIdentity(), ComponentType.Protein, SynBadPortState.Default, degradationRate, ws, contexts))
            .addAction(SviInternalActionFactory.CreateDegradationAction (
                    svi_proteinPDegId, identity.getIdentity(), ComponentType.Protein, SynBadPortState.Phosphorylated, phosDegradationRate, ws, contexts))
            .createPort(port_in_rips, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE)
            .createPort(port_out_protein, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, cd_svp_protein.getIdentity(), identity.getIdentity(), SVP_TYPE)
            .createPort(port_out_protein_p, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated, cd_svp_protein_p.getIdentity(), identity.getIdentity(), SVP_TYPE);

            return b.build("CreateSvp(CDSSmlMol) ["+ identity.getDisplayID() + "]");
  
    }
 
    public static SBAction CreateCDSDefinitionAction(SBIdentity identity, SBWorkspace workspace, double Kd, URI[] contexts) {

        SBIdentity proteinDefinition = SvpIdentityHelper.getProteinIdentity(identity);

        SBIdentity productionId = workspace.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Production", identity.getVersion());
        SBIdentity degradationId = workspace.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Degradation", identity.getVersion());

        SBIdentity inRipsPort =  SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default);
        SBIdentity outProteinPort =  SvpIdentityHelper.getPortIdentity(identity, proteinDefinition, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(workspace, contexts);
        b = b.createSvp(identity, Collections.singleton(ComponentRole.CDS), ComponentType.DNA)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("FunctionalPart"), SVP_TYPE)
            .createProteinProduction(productionId, identity.getIdentity())
            .createDegradation(degradationId, identity.getIdentity(), ComponentType.Protein,  SynBadPortState.Default, Kd)
            .createPort(inRipsPort, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE)
            .createPort(outProteinPort, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, proteinDefinition.getIdentity(), identity.getIdentity(), SVP_TYPE);

        return b.build("CreateSvp(CDS) ["+ identity.getDisplayID() + "]");
    }

    public static SBAction CreateComplexDefinitionAction(SBIdentity identity, SBWorkspace workspace, double Kd, URI[] contexts) {

        //Identity sbolDefinition = SvpIdentityHelper.getComplexIdentity(identity);
        SBIdentity degradationId = workspace.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Degradation", identity.getVersion());
        SBIdentity outComplexPort =  SvpIdentityHelper.getPortIdentity(identity, identity, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);
        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(workspace, contexts);
        
        b = b.createSvp(identity, Collections.emptySet(), ComponentType.Complex)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Protein Complex"), SVP_TYPE)
            .createDegradation(degradationId, identity.getIdentity(), ComponentType.Complex,  SynBadPortState.Default, Kd)
            .createPort(outComplexPort, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, identity.getIdentity(), identity.getIdentity(), SVP_TYPE);
        return b.build("CreateSvp(Complex) ["+ identity.getDisplayID() + "]");
    }
    
     public static SBAction CreateCDSDefinitionWithPhosphorylatedAction(SBIdentity identity, SBWorkspace workspace, double degradationRate,  double phosDegradationRate, double dephosphorylationRate, URI[] contexts) {
        

        SBIdentity proteinDefinition = SvpIdentityHelper.getProteinIdentity(identity);
        SBIdentity proteinPDefinition = SvpIdentityHelper.getPhosphorylatedProteinIdentity(identity);
        SBIdentity productionId = workspace.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Production", identity.getVersion());
        SBIdentity degradationId = workspace.getIdentityFactory().getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_Degradation", identity.getVersion());
        SBIdentity svi_proteinDephosId = SvpIdentityHelper.getDePhosphorylationInteractionIdentity(identity);
        SBIdentity svi_proteinPDegId = SvpIdentityHelper.getDegradationIdentity(identity, SynBadPortState.Phosphorylated);
        SBIdentity inRipsPort =  SvpIdentityHelper.getPortIdentity(identity, null, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default);
        SBIdentity outProteinPort =  SvpIdentityHelper.getPortIdentity(identity, proteinDefinition, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(workspace, contexts);
        b = b.createSvp(identity, Collections.singleton(ComponentRole.CDS), ComponentType.DNA)
            .addAction(ComponentDefinition.createComponentDefinition(proteinPDefinition.getIdentity(),  Collections.singleton(ComponentType.Protein), Collections.singleton(ComponentRole.Generic),workspace, contexts))
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("FunctionalPart"), SVP_TYPE)
            .createProteinProduction(productionId, identity.getIdentity())
            .createDegradation(degradationId, identity.getIdentity(), ComponentType.Protein,  SynBadPortState.Default, degradationRate)
            .createDegradation(svi_proteinPDegId, identity.getIdentity(), ComponentType.Protein,  SynBadPortState.Phosphorylated, phosDegradationRate)
            .createDephosphorylate(svi_proteinDephosId, dephosphorylationRate, identity.getIdentity())
            .createPort(inRipsPort, SBPortDirection.IN, SynBadPortType.RiPS, SynBadPortState.Default, null, identity.getIdentity(), SVP_TYPE)
            .createPort(outProteinPort, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, proteinDefinition.getIdentity(), identity.getIdentity(), SVP_TYPE);

        return b.build("CreateSvp(CDSWithPhosP) ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction  CreateTerminatorDefinitionAction(SBIdentity identity, SBWorkspace workspace, URI[] contexts) {
        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(workspace, contexts)
            .createSvp(identity, Collections.singleton(ComponentRole.Terminator), ComponentType.DNA)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Terminator"), SVP_TYPE);
        return b.build("CreateSvp(Ter) ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction  CreateCompoundDefinitionAction(SBIdentity identity, SBWorkspace workspace, URI[] contexts) {

        // BioPax does not describe compounds, add as generic small molecule
        
        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(workspace, contexts)
            .createSvp(identity, Collections.singleton(ComponentRole.Generic), ComponentType.SmallMolecule)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partType), SBValue.parseValue("Compound"), SVP_TYPE);
         return b.build("CreateSvp(Compound) ["+ identity.getDisplayID() + "]");
    }
    
    public static SBAction CreateMrna(SBIdentity identity, URI svpId, SBWorkspace workspace, URI[] contexts) {
    
        SBIdentityFactory f = workspace.getIdentityFactory();
        SBIdentity svp = f.getIdentity(svpId);
        SBIdentity mrnaProduction = f.getIdentity(identity.getUriPrefix(),
            svp.getDisplayID() + "_mRNAProduction", identity.getVersion());

        SvpActionBuilderImpl b =  new SvpActionBuilderImpl(workspace, contexts);
        b = b.createSvp(identity, Collections.singleton(ComponentRole.mRNA), ComponentType.mRNA)
            .createEdge(svpId, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), identity.getIdentity())
            .addAction(SviInternalActionFactory.CreateMrnaProductionAction(mrnaProduction, identity.getIdentity(), workspace, contexts));
        
        return b.build("CreateSvp(mRNA) ["+ identity.getDisplayID() + "]");
    }
}
