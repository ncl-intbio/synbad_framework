/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.net.URI;
import java.util.*;
import java.util.Collection;
import java.util.stream.Collectors;
import org.apache.jena.ext.com.google.common.collect.Streams;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.actions.SvpActions;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.ServiceDependency;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.svp.obj.SviInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;

/**
 * Listens to an InteractionManager and creates or removes interactions as appropriate.
 * @author owengilfellon
 */ 
public class SviUpdater extends ASBWorkspaceSubscriber implements SBWorkspaceService {
   
    private static final Logger LOGGER = LoggerFactory.getLogger(SviUpdater.class);
    private SBWorkspace ws;
    private SvpInteractionManager manager;
    private SBIdentityFactory factory;
    private Map<URI, Multimap<URI, URI>> interactionToOutput;
    private Map<URI, Multimap<URI, URI>> outputToInteraction;

    public SviUpdater() { }

    @Override
    public void close() {
        this.ws.getDispatcher().unsubscribe(this);
        this.ws = null;
        this.factory = null;
        this.manager = null;
        this.interactionToOutput.clear();
        this.outputToInteraction.clear();
    }

    @Override
    public void setWorkspace(SBWorkspace ws) {
        this.ws = ws;
        this.factory = ws.getIdentityFactory();
        this.manager = new DefaultInteractionManager(ws);
        this.ws.getDispatcher().subscribe(this::accept, this);
        this.interactionToOutput = new HashMap<>();
        this.outputToInteraction = new HashMap<>();
        initialiseInteractions();
    }
    
    private boolean accept(SBEvent e) {
        
        if(!SBGraphEvent.class.isAssignableFrom(e.getClass()))
            return false;
        
        if(((SBGraphEvent)e).getGraphEntityType() != SBGraphEntityType.EDGE && !SvpInteraction.class.isAssignableFrom(e.getDataClass()))
            return false;
        
        if(SvpInteraction.class.isAssignableFrom(e.getDataClass()))
            return true;
        
        SBGraphEvent<SynBadEdge> evt = (SBGraphEvent<SynBadEdge>) e;
        
        return evt.getData().getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasFunctionalComponent) ||
                evt.getData().getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasInteraction);
    }
    
    private void initialiseInteractions() {
        if(ws.getContextIds().length > 0) {
            for(SvpInteraction svi : ws.getObjects(SvpInteraction.class, ws.getContextIds())) {
                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("Processing on construction {}", svi.getDisplayId());
                processSvi(SBGraphEventType.ADDED, svi.getIdentity(), svi.getContexts());
            }
        }
    }

    @Override
    public void onNodeEvent(SBGraphEvent<URI> evt) {
        processSvi(evt.getType(), evt.getData(), ((SBEvtWithContext)evt).getContexts());
    }
    
    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> evt) {
        if(evt.getData().getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasFunctionalComponent))  {
            processFc(evt.getType(), evt.getData().getTo(),  evt.getData().getFrom(), ((SBEvtWithContext)evt).getContexts());
        } else if (evt.getData().getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasInteraction)) {
            processInteraction(evt.getType(), evt.getData().getTo(), ((SBEvtWithContext)evt).getContexts());
        }
    }
    
    /**
     * If interaction is added...
     * 1. Find all MDs with FCs of all INPUT participants
     * 2. Add interaction to all MDs (singleton if no sequence element)
     * 3. Add output FCs if required (singleton)
     * 3. Add participations from SVIs to FCs
     * 
     * If interaction is removed...
     * 1. Find all interaction instances and remove
     * 2. Find all interaction outputs and remove (if not produced by another interaction)
     * 
     * @param type
     * @param sviId
     * @param contexts 
     */
    private void processSvi(
            SBGraphEventType type,
            URI sviId,
            URI[] contexts) {

        Set<ModuleDefinition> moduleDefinitions = new HashSet<>();
        SvpInteraction svi = ws.getObject(sviId, SvpInteraction.class, contexts);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("processSvi(): {}: {}", type, svi == null ? sviId.toASCIIString() : svi.getDisplayId());

        // get moduleDefinitions containing participant functionalComponents for modified interaction

        if(type == SBGraphEventType.ADDED) {
            Set<ModuleDefinition> parentsRequiringInteraction = manager.getParticipantInstances(sviId, contexts).stream()
                    .map(fc -> ws.getObject(fc, FunctionalComponent.class, contexts))
                    .map(FunctionalComponent::getParent)
                    .collect(Collectors.toSet());
            moduleDefinitions.addAll(parentsRequiringInteraction);
        } else if (type == SBGraphEventType.REMOVED) {
            Collection<URI> allExistingInteractions = manager.getInteractionInstances(sviId, contexts);
            Set<ModuleDefinition> parentsRequiringInteractionRemoval = allExistingInteractions.stream()
                    .map(i -> ws.getObject(i, Interaction.class, contexts))
                    .map(Interaction::getParent)
                    .collect(Collectors.toSet());
            moduleDefinitions.addAll(parentsRequiringInteractionRemoval);
        }
        
        if(moduleDefinitions.isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("{}: No module definitions found with participant",  type);
            return;
        }

        SbolActionBuilder b = new SbolActionBuilder(ws, contexts);

        // for each parent, add or remove instances of the SVI

        for(ModuleDefinition parent : moduleDefinitions) {
            if(type == SBGraphEventType.ADDED)
                addInstances(svi, parent, b);
            else
                removeInstances(sviId, parent, b);
        }
        
        // if any modifications have been identified, perform

        SBAction action = b.isEmpty() ?  null : b.build("SviUpdate SVI[" + svi.getDisplayId() + "]");
        if(action != null)
            ws.perform(action);

    }

    private void processInteraction(
            SBGraphEventType type,
            URI iId,
            URI[] contexts) {

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("processInteraction(): {}: {}", type, iId.toASCIIString());

       if( type == SBGraphEventType.REMOVED ) {
           SBActionBuilder b = new DefaultSBDomainBuilder(ws, contexts);
           for(URI context : contexts) {
               if(interactionToOutput.containsKey(context)) {
                   Collection<URI> outputs = interactionToOutput.get(context).removeAll(iId);
                   outputs.forEach(o -> {
                       FunctionalComponent fc = ws.getObject(o, FunctionalComponent.class, contexts);
                       if(fc != null) {
                           if(LOGGER.isDebugEnabled())
                               LOGGER.debug("Removing {} produced by {}", fc.getDisplayId(), factory.getIdentity(iId).getDisplayID());
                           ((DefaultSBDomainBuilder) b).removeObjectAndDependants(fc.getIdentity(), context);
                       }
                       removeOutput(new URI[]{context}, iId, o);
                   });
               }
           }
           if(!b.isEmpty()) {
               ws.perform(b.build("Removing outputs of " + iId.toASCIIString()));
           }
       }
    }
  
    /**
     * If FunctionalComponent is added...
     * 1. Identify participation in any SVI definitions
     * 2. If any of those SVIs now have all inputs...
     * 3. Instantiate SVI, and link to FCs with Participations
     * 
     * If FunctionalComponent is removed...
     * 1. Identify participation in any SVI definitions
     * 2. If any of those SVIs now do not have all inputs...
     * 3. Remove SVI instance and related Participations
     * 4. Remove SVI outputs
     * @param type
     * @param fcId
     * @param contexts 
     */
    private void processFc(
            SBGraphEventType type, 
            URI fcId, 
            URI mdId,
            URI[] contexts) {
        
        if(fcId == null)
            return;

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("processFc(): {} {}", type, fcId.toASCIIString());
        
        SbolActionBuilder b = new SbolActionBuilder(ws, contexts);

        if(type == SBGraphEventType.ADDED) {

            // From FunctionalComponent, identify SVP definition and participation in SVIs

            FunctionalComponent fc = ws.getObject(fcId, FunctionalComponent.class, contexts);
            ModuleDefinition parent = fc.getParent();
            //ComponentDefinition def = fc.getDefinition();

            manager.getSviFromParticipantInstance(fcId, contexts).forEach(svi -> {
                 
                SvpInteraction interaction = ws.getObject(svi, SvpInteraction.class, contexts);

                // if is participant of an SVI, add records

                if(interaction != null ) {
                    addInstances(interaction, parent, b);
                } else {
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("{}: Could not find SVI  for {}",  type, fc.getDisplayId());
                    return;
                }
            });
        }

        else if (type == SBGraphEventType.REMOVED) {

            Collection<URI> interactionsWithFunctionalComponents = manager.getIncompleteSvisWithInstancesInParent(mdId, contexts);

            for(URI interaction : interactionsWithFunctionalComponents) {

                // If the SVI still exists, process it

                SvpInteraction svi = ws.getObject(interaction, SvpInteraction.class, contexts);

                if(svi != null) {
                    //removeInstances(fcId, parent, b);
                    processSvi(type, svi.getIdentity(), contexts);
                } 
            }
        }

        if(!b.isEmpty()) {
            ws.perform(b.build("SviUpdate FC[" + factory.getIdentity(fcId).getDisplayID() + "]"));
        }
    }

    private void addInstances(  
            SvpInteraction svi,
            ModuleDefinition parent,
            SbolActionBuilder b) {

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("addInstances(): {}", svi.getDisplayId());

        Set<URI> expectedObjects = new HashSet<>();
        
        if(!manager.containsSvi(svi.getIdentity(), svi.getContexts())) {
            LOGGER.warn("Interaction manager does not have record for: {}", svi.getDisplayId());
            return;
        }

        // if parent has child instances of all svpparticipants 

        if(manager.shouldBeInstanced(svi.getIdentity(), parent.getIdentity(), svi.getContexts()) && 
                !manager.isInstanced(svi.getIdentity(), parent.getIdentity(), Collections.EMPTY_SET, svi.getContexts())) {

            Collection<SvpParticipant> svpParticipants = svi.getParticipants();
            Collection<InteractionMatch> matches = getMatches(svi, svpParticipants, parent);
           
            for(InteractionMatch match : matches) {

                if(!svi.isInternal() || !isAnyMappedToModuleMember(match.getInputFcs().values(), parent.getContexts())) {

                    SBIdentity interactionIdentity = createInteraction(parent, svi, expectedObjects, b);

                    // do inputs
                    for(SvpParticipant svp : match.getInputFcs().keySet()) {
                        SBIdentity fcId = ws.getIdentityFactory().getIdentity(match.getInputFcs().get(svp));
                        createParticipation(interactionIdentity, fcId, svp, expectedObjects, b);
                    }

                    // do outputs
                    svpParticipants.stream()
                        .filter(svp -> svp.getDirection() == ParticipationRole.Output)
                        .forEach(svp -> {
                            try {
                                URI fc = match.getOutputFcs().get(svp);
                                SBIdentity fcId = fc != null ? 
                                    SBIdentity.getIdentity(fc) :
                                    createFunctionalComponentOfOutputParticipant(svi, svp, parent, interactionIdentity, expectedObjects, b);
                                SBIdentity parId = createParticipation(interactionIdentity, fcId, svp, expectedObjects, b);
                            } catch (SBIdentityException ex) {
                                Exceptions.printStackTrace(ex);
                            }
                    });
                }
            }
        } else if(LOGGER.isTraceEnabled()){
            LOGGER.trace("{} does not have all participants for: {}", parent.getDisplayId(), svi.getDisplayId());
        }
    }
    
    private void removeInstances( 
        URI svi,
        ModuleDefinition parent,
        SbolActionBuilder b) {

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("removeInstances: {}", svi.toASCIIString());

        SvpInteraction svpInteraction = ws.getObject(svi, SvpInteraction.class, parent.getContexts());
        
        if(svpInteraction != null) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("SVI still exists: {}", svpInteraction.getDisplayId());
        }
            
        boolean hasAllParticipants = manager.shouldBeInstanced(svi, parent.getIdentity(), parent.getContexts());

        if(hasAllParticipants) {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Not removing, has all participants: {} in {}", svi.toASCIIString(), parent.getDisplayId());
            return;
        } else {
            
            // TODO: Should only remove outputs that are no longer being produced
            
             Collection<URI> outputInstances = manager
                    .getParticipantInstances(svi, ParticipationRole.Output, svpInteraction.getContexts());


             Collection<URI> processedObjects = new HashSet<>();

            parent.getFunctionalComponents().stream()
                    .filter(fc -> outputInstances.contains(fc.getIdentity()))
                    .filter(fc -> !processedObjects.contains(fc.getIdentity()))
                    .forEach(fc -> {

                        // if there are any FCs with the output participant's ComponentDefinition
                        if(LOGGER.isTraceEnabled())
                            LOGGER.trace("-> {}", fc.getIdentity().toASCIIString());

                        Collection<URI> producerInteraction = Arrays.stream(parent.getContexts())
                                .filter(outputToInteraction::containsKey)
                                .flatMap(c -> outputToInteraction.get(c).get(fc.getIdentity()).stream())
                                .collect(Collectors.toSet());

                        if(producerInteraction.size() <= 1)  {
                            if(LOGGER.isDebugEnabled())
                                LOGGER.debug("\tRemoving output FC by participation: {}", fc.getIdentity().toASCIIString());
                            b.removeObjectAndDependants(fc.getIdentity(), SBIdentityHelper.getURI(FunctionalComponent.TYPE));
                            producerInteraction.forEach(i -> removeOutput(parent.getContexts(), i, fc.getIdentity()));
                        }

                        processedObjects.add(fc.getIdentity());
                    });
        }
        
        // instances created as a result of interaction (presence of all inputs)

       
        // for all output instances in parent
    }
 
    private boolean isAnyMappedToModuleMember(Collection<URI> fcs, URI[] contexts) {
        return fcs.stream()
            .map(fc -> {
                FunctionalComponent func = ws.getObject(fc, FunctionalComponent.class, contexts);
                if(func == null) {
                    LOGGER.error("Could not get FunctionalComponent {} in contexts: [{}]",
                            factory.getIdentity(fc).getDisplayID(),
                            factory.getIdentity(contexts[0]).getDisplayID());
                }
                return func;
            })
            .anyMatch(this::isMappedToModuleMember);
    }

    private boolean isMappedToModuleMember(FunctionalComponent fc) {
        
        if(fc == null) {
            LOGGER.error("FunctionalComponent is null");
            return false;
        }
        
        URI[] contexts = fc.getContexts();
        return  ws.incomingEdges(fc, SynBadTerms.SbolMapsTo.hasLocalInstance, MapsTo.class, contexts).stream()
                .map(e -> ws.getObject(e.getFrom(), MapsTo.class, contexts).getOwner())
                .anyMatch(i -> {
                    return i.is(Module.TYPE);
                });
    }

    private Collection<InteractionMatch> getMatches(SvpInteraction svi, Collection<SvpParticipant> svpParticipants, ModuleDefinition parent) {
                    
        Map<URI, Stack<URI>> definitionToFc = new HashMap<>();

        svpParticipants.stream().map(p -> p.getSbolParticipant()).forEach(cd -> {
            if(!definitionToFc.containsKey(cd.getIdentity())) {
                definitionToFc.put(cd.getIdentity(), new Stack<>());
            }

            manager.getParticipationInstancesFromDefinitionInMd(cd.getIdentity(), parent.getIdentity(), cd.getContexts())
                .forEach(i -> definitionToFc.get(cd.getIdentity()).push(i));
        });

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Has {} participants requiring interaction in {}", definitionToFc.values()
                .stream().flatMap(s -> s.stream()).count(),  parent.getDisplayId());
            
        Collection<InteractionMatch> matches = new HashSet<>();
        boolean hasMatch = true;

        while(hasMatch) {

            Map<SvpParticipant, URI> inputs = new HashMap<>();
            Map<SvpParticipant, URI> outputs = new HashMap<>();

            int expectedInputs = 0;

            for(SvpParticipant svp : svpParticipants) {

                if(svp.getDirection() != ParticipationRole.Output)
                    expectedInputs++;

                ComponentDefinition participant = svp.getSbolParticipant();
                URI fc = null;

                if(participant != null && definitionToFc.containsKey(participant.getIdentity()) 
                        && !definitionToFc.get(participant.getIdentity()).isEmpty()) {
                    Stack<URI> fcInstances = definitionToFc.get(participant.getIdentity());
                    fc = participant.getSbolTypes().contains(ComponentType.DNA) ?
                            fcInstances.pop() :
                            fcInstances.peek();
                }

                if(fc == null) {
                    hasMatch = false;
                } else if(svp.getDirection() == ParticipationRole.Output) {
                    outputs.put(svp, fc);
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("\tAdding output: {}", fc.toASCIIString());
                } else {
                    inputs.put(svp, fc);
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("\tAdding input: {}", fc.toASCIIString());
                }
            }

            InteractionMatch m = new InteractionMatch(inputs, outputs);
            boolean isInstanced = manager.isInstanced(svi.getIdentity(), parent.getIdentity(), m.getInputFcs().values(), svi.getContexts());
            boolean transInteraction = m.getInputFcs().keySet().stream()
                    .noneMatch(svp -> svp.getSbolTypes().contains(ComponentType.DNA));

            if( !matches.contains(m) && !isInstanced && m.getInputFcs().size() == expectedInputs) {
                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("\tFound match!");
                matches.add(m);
            }

            if (definitionToFc.values().stream().anyMatch(s -> s.isEmpty())) {
                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("\tRan out of instances");
                hasMatch = false;
            } else if (m.getInputFcs().size() < expectedInputs) {
                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("\tMatch does not have all participants");
                hasMatch = false;
            } else if (transInteraction && (!matches.isEmpty() || isInstanced)) {
                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("\tHas a single trans interaction");
                hasMatch = false;
            }
        }

        return matches;
    }

     private SBIdentity createInteraction(
            ModuleDefinition md_svpModuleDefinition, 
            SvpInteraction svi,
            Set<URI> expectedObjects,
            SbolActionBuilder b) {

        // If has DNA participant, create unique interaction
        
        SBIdentity interactionIdentity = svi.hasDnaParticipant() ? 
            factory.getUniqueIdentity(factory.getIdentity(svi.getPrefix(), md_svpModuleDefinition.getDisplayId(), "i_" + svi.getDisplayId(), svi.getVersion())) :
            factory.getIdentity(svi.getPrefix(), md_svpModuleDefinition.getDisplayId(), "i_" + svi.getDisplayId(), svi.getVersion());

        if(!expectedObjects.contains(interactionIdentity.getIdentity()) &&
                !ws.containsIdentity(interactionIdentity.getIdentity(), md_svpModuleDefinition.getContexts())) {

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Adding instance:\t{}:{}", md_svpModuleDefinition.getDisplayId(), interactionIdentity.getDisplayID());

            b.createInteraction(
                        interactionIdentity.getIdentity(),
                        new SbolInteractionType[] { svi.getSbolInteractionType() },
                        md_svpModuleDefinition,
                        md_svpModuleDefinition.getContexts())
                .addAction(
                    SvpActions.AddExtends(
                        svi.getIdentity(),
                        interactionIdentity.getIdentity(),
                        ws,
                        md_svpModuleDefinition.getContexts()))
                .createValue(interactionIdentity.getIdentity(),URI.create(SynBadTerms.Rdf.hasType), SBValue.parseValue(URI.create(SviInstance.TYPE)), URI.create(SvmInstance.TYPE))
                .addAction(new CreateEdge(interactionIdentity.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, svi.getIdentity(),
                        ws.getIdentity(), ws.getSystemContextIds(md_svpModuleDefinition.getContexts())));
                /*.addAction(
                        new UpdatePortInstances(SBGraphEventType.ADDED, svi.getIdentity(),  md_svpModuleDefinition.getWorkspace(), md_svpModuleDefinition.getContexts()));*/

            expectedObjects.add(interactionIdentity.getIdentity());

        } else if(LOGGER.isTraceEnabled()){
             LOGGER.trace("\tInteraction already exists:\t{}", interactionIdentity.getDisplayID());
        }

        return interactionIdentity;
    }

    private SBIdentity createParticipation(
            SBIdentity interactionIdentity, 
            SBIdentity fcId, 
            SvpParticipant svpInteractionParticipant,
            Set<URI> expectedObjects,
            SbolActionBuilder b) {

        SBIdentity participationId = factory.getUniqueIdentity(Participation.getParticipationIdentity(interactionIdentity, fcId));

        // get unique ID...

        if(!expectedObjects.contains(participationId.getIdentity())) {

            if(LOGGER.isTraceEnabled())
                LOGGER.trace("\tAdding participation: {}", participationId.getDisplayID());

            expectedObjects.add(participationId.getIdentity());
            b.createParticipation(
                    participationId.getIdentity(),
                    interactionIdentity.getIdentity(),
                    fcId.getIdentity(),
                    svpInteractionParticipant.getInteractionRole())
                .createEdge(participationId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.extensionOf), svpInteractionParticipant.getIdentity());
            if(svpInteractionParticipant.getDirection() != ParticipationRole.Output) {
                b.addAction(new CreateEdge(interactionIdentity.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, fcId.getIdentity(), ws.getIdentity(), ws.getSystemContextIds(svpInteractionParticipant.getContexts())));
            }

        } else if(LOGGER.isTraceEnabled()){
            LOGGER.trace("\tParticipation already exists: {}", participationId.getDisplayID());
        }

        return participationId;
    }

    private SBIdentity createFunctionalComponentOfOutputParticipant(SvpInteraction svi, SvpParticipant participantDefinition, ModuleDefinition md_svpModuleDefinition, SBIdentity interactionIdentity,
                                Set<URI> expectedObjects,
                                SbolActionBuilder b) {

        SBIdentity fcId = FunctionalComponent.getFunctionalComponentIdentity(
                factory.getIdentity(md_svpModuleDefinition.getIdentity()), 
                factory.getIdentity(participantDefinition.getSbolParticipant().getIdentity()));

        if(!expectedObjects.contains(fcId.getIdentity()) && !ws.containsIdentity(fcId.getIdentity(), svi.getContexts())) {
            b.createFuncComponent(
                fcId.getIdentity(),
                participantDefinition.getSbolParticipant(),
                md_svpModuleDefinition,
                SbolDirection.OUT,
                SbolAccess.PUBLIC);
            b.createValue(fcId.getIdentity(),URI.create(SynBadTerms.Rdf.hasType), SBValue.parseValue(URI.create(SvpInstance.TYPE)), URI.create(SvpModule.TYPE));

            InteractionType t = svi.getInteractionType();

            if(participantDefinition.getDirection() == ParticipationRole.Output
                    && (t == InteractionType.ProteinProduction  ||
                    t == InteractionType.ComplexFormation ||
                    t == InteractionType.MetabolicReaction ||
                    t == InteractionType.Phosphorylation )) {
                addOutput(svi.getContexts(), interactionIdentity.getIdentity(), fcId.getIdentity());
            }
            //addOutput(svi.getContexts(), interactionIdentity.getIdentity(), fcId.getIdentity());
            expectedObjects.add(fcId.getIdentity());
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Adding output:\t{}:{}", md_svpModuleDefinition.getDisplayId(), fcId.getDisplayID());
        }

        return fcId;
    }

    private void addOutput(URI[] contexts, URI interaction, URI output) {
        for(URI context: contexts) {
            if(!interactionToOutput.containsKey(context))
                interactionToOutput.put(context, HashMultimap.create());

            if(!outputToInteraction.containsKey(context))
                outputToInteraction.put(context, HashMultimap.create());

            interactionToOutput.get(context).put(interaction, output);
            outputToInteraction.get(context).put(output, interaction);
        }
    }

    private void removeInteraction(URI[] contexts, URI interaction) {
        for(URI context: contexts) {

            if(interactionToOutput.containsKey(context))
                interactionToOutput.get(context).removeAll(interaction);

            if(interactionToOutput.get(context).isEmpty())
                interactionToOutput.remove(context);
        }
    }

    private void removeOutput(URI[] contexts, URI interaction, URI output) {
        for(URI context: contexts) {
            if (outputToInteraction.containsKey(context)) {
                outputToInteraction.get(context).remove(output, interaction);

                if (interactionToOutput.containsKey(context))
                    interactionToOutput.get(context).remove(interaction, output);

                if (outputToInteraction.get(context).isEmpty())
                    outputToInteraction.remove(context);

                if (interactionToOutput.get(context).isEmpty())
                    interactionToOutput.remove(context);
            }
        }
    }


    @Override
    public SBWorkspace getWorkspace() {
     return ws;
    }

    private class InteractionMatch {
        
        private Map<SvpParticipant, URI> inputFcs;
        private Map<SvpParticipant, URI> outputFcs;

        public InteractionMatch(Map<SvpParticipant, URI> inputFcs, Map<SvpParticipant, URI> outputFc) {
            this.inputFcs = inputFcs;
            this.outputFcs = outputFc;
        }

        public Map<SvpParticipant, URI> getInputFcs() {
            return inputFcs;
        }

        public Map<SvpParticipant, URI> getOutputFcs() {
            return outputFcs;
        }
        
        public Set<URI> getAllFcs() {
            return Streams.concat(getInputFcs().values().stream(), getOutputFcs().values().stream()).collect(Collectors.toSet());
        }

        @Override
        public boolean equals(Object o) {
            
            if(o == null)
                return false;
            if(o == this)
                return true;
            if(!InteractionMatch.class.isAssignableFrom(o.getClass()))
                return false;
            
            InteractionMatch m = (InteractionMatch) o;
            
            return m.getAllFcs().containsAll(getAllFcs());
        }

        @Override
        public int hashCode() {
            
            int hash = 7;
            
            for(URI fc : getAllFcs()) {
                hash *= fc.hashCode();
            }
            
            return hash;
        }
    }
}
