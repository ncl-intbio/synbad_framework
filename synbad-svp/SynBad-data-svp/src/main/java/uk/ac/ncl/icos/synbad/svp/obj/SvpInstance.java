/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owen
 */
@DomainObject(rdfType = SvpInstance.TYPE)
public class SvpInstance extends ASvpInstance<Svp, FunctionalComponent> {
    
    public static final String TYPE = "http://virtualparts.org/SvpInstance";
    
    public SvpInstance(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    @Override
    public boolean isMappedToComponent() {
        return getMappedToComponent() != null;
    }
    
    @Override
    public SvpModule getParent() {
        return as(FunctionalComponent.class).map(m -> m.getParent().getIdentity())
                .map(id -> ws.getObject(id, SvpModule.class, getContexts())).get();
    }
    
    @Override
    public Optional<Svp> getDefinition() {
        return getOutgoingObjects(SynBadTerms.Sbol.definedBy, Svp.class).stream().findFirst();
    }
    
    @Override
    public Component getMappedToComponent() {
        SbolInstance remoteComponent;
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(ws, getContexts())) {
            SvpModule md = getParent();
            remoteComponent = md.getRemotelFromLocal(this, Component.class, g);
        }
        if(remoteComponent != null && remoteComponent.is(Component.TYPE))
            return (Component)remoteComponent;
        
        return null;
    }

    @Override
    public URI getType() {
        return URI.create(TYPE);
    }

    
}
