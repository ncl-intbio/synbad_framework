/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class GeneratorUnitBuilder {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(GeneratorUnitBuilder.class);

    private final SBWorkspace workspaceId;
    private final URI[] contexts;
    private final SBIdentity identity;
    private SBIdentity promoterId;
    private Double transcriptionRate = null;
    
    protected final Random RANDOM = new Random();

    private final CelloActionBuilder b;
   
    
    //private final InteractionMode mode;

    public GeneratorUnitBuilder(CelloActionBuilder b, SBIdentity identity, URI[] contexts) {
        this.b = b;
        this.identity = identity;
        this.workspaceId = b.getWorkspace();
        this.contexts = contexts;
    }

    public GeneratorUnitBuilder(SBIdentity identity, SBWorkspace workspaceId, URI[] contexts) {
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.b = null;
    }


    public GeneratorUnitBuilder setTranscriptionRate(double transcriptionRate) {
        this.transcriptionRate = transcriptionRate;
        return this;
    }

    public GeneratorUnitBuilder setPromoterIdentity(SBIdentity promoterIdentity) {
        this.promoterId = promoterIdentity;
        return this;
    }
    
    public double getRandomParameter() {
        return RANDOM.nextDouble();
    }
   
  
    public SBAction build() {
     
        if(transcriptionRate == null)
            transcriptionRate = getRandomParameter();
        
        SBAction a =  TuActionsFactory.createPopsGenerator(identity, promoterId, transcriptionRate, workspaceId, contexts);
        LOGGER.trace("Returning generator: {}", identity.getDisplayID());
 
        return a;
    }
    
    public SBCelloBuilder builder() {
        SBAction a = build();
        if(a == null) {
            LOGGER.error("Action is null");
            return null;
        }

        if( b == null) {
            LOGGER.trace("Builder is null, returning new builder");
            return new CelloActionBuilder(workspaceId, contexts).addAction(a);
        }

        if (!b.isBuilt()) {
            LOGGER.trace("Adding action to builder");
            b.addAction(a);
        }

        LOGGER.trace("Returning builder");
        return b;
    }
 
}
