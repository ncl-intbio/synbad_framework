/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SviInstance;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 * Listens to a provided workspace, and updates records of interactions appropriately.
 * @author owen
 */
public class DefaultInteractionManager implements SvpInteractionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultInteractionManager.class);
    private  SBWorkspace workspace;
    private SBIdentityFactory f;

    public DefaultInteractionManager(SBWorkspace workspace) {
        this.workspace = workspace;
        this.f = workspace.getIdentityFactory();
    }


    /*
     * =================================================================
     *                          Helper methods
     * =================================================================
     */

    @Override
    public boolean containsSvi(URI uri, URI[] contexts) {
        return workspace.containsObject(uri, SvpInteraction.class, contexts);
    }

    @Override
    public boolean containsSvpParticipation(URI uri, URI[] contexts) {
        return workspace.containsObject(uri, SvpParticipant.class, contexts);
    }

    @Override
    public boolean containsFunctionalComponent(URI uri, URI[] contexts) {
        return workspace.containsObject(uri, FunctionalComponent.class, contexts);
    }

    /**Returns the identities of all FunctionalComponents that
     * are participants of the provided SVI.
     *
     * @param svi
     * @return
     */
    @Override
    public Collection<URI> getParticipantInstances(URI svi, URI[] contexts) {

        if(svi == null || !containsSvi(svi, contexts)) {
            LOGGER.error("Does not contain SVI {} in contexts: [{}]", 
                    f.getIdentity(svi).getDisplayID(),
                    f.getIdentity(contexts[0]).getDisplayID());
            return Collections.EMPTY_SET;
        }
            
        SvpParticipant[] participants = (SvpParticipant[])
                workspace.getObject(svi, SvpInteraction.class, contexts)
                    .getParticipants().toArray(new SvpParticipant[] {});
        
         try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(participants)
                    .e(SBDirection.OUT, SynBadTerms.SynBadParticipant.isSbolParticipant)
                    .v(SBDirection.OUT, ComponentDefinition.class)
                    .e(SBDirection.IN, SynBadTerms.Sbol.definedBy)
                    .v(SBDirection.IN, FunctionalComponent.class)
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 
    }

    /**
     * Returns the identities of all FunctionalComponents that
     * are participants of the provided SVI, that have the
     * provided ParticipationRole.
     * @param svi
     * @return
     */
    @Override
    public Collection<URI> getParticipantInstances(URI svi, ParticipationRole rol, URI[] contexts) {
        if(svi == null || !containsSvi(svi, contexts))
            return Collections.EMPTY_SET;

        SvpParticipant[] participants = (SvpParticipant[])
                workspace.getObject(svi, SvpInteraction.class, contexts)
                    .getParticipants().stream().filter(p -> p.getDirection() == rol).collect(Collectors.toSet()).toArray(new SvpParticipant[] {});
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(participants)
                    .e(SBDirection.OUT, SynBadTerms.SynBadParticipant.isSbolParticipant)
                    .v(SBDirection.OUT, ComponentDefinition.class)
                    .e(SBDirection.IN, SynBadTerms.Sbol.definedBy)
                    .v(SBDirection.IN, FunctionalComponent.class)
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 
    }

    @Override
    public Collection<URI> getInteractionInstances(URI svi, URI[] contexts) {
        SvpInteraction interaction = workspace.getObject(svi, SvpInteraction.class, contexts);
        return workspace.incomingEdges(interaction, SynBadTerms.SynBadEntity.extensionOf, Interaction.class, contexts)
                .stream().map(e -> e.getFrom()).collect(Collectors.toSet());
    }

    @Override
    public Collection<URI> getSvisWithConstraint(URI constraint, URI[] contexts) {
        
        if(constraint == null) {
            LOGGER.warn("Constraint URI is null");
            return Collections.EMPTY_SET;
        }
        
        FunctionalComponent sbolParticipant = workspace.getObject(constraint, FunctionalComponent.class, contexts);
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(sbolParticipant)
                    .e(SBDirection.IN, SynBadTerms.SynBadParticipant.isSbolParticipant)
                    .v(SBDirection.IN, SvpParticipant.class)
                    .e(SBDirection.IN, SynBadTerms.SynBadInteraction.hasParticipant)
                    .v(SBDirection.IN, SvpInteraction.class)
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 

    }

    @Override
    public Collection<URI> getInteractionsWithConstraintInParent(URI constraint, URI parent, URI[] contexts) {
        
        if(constraint == null) {
            LOGGER.warn("Functional Comonent URI is null");
            return Collections.EMPTY_SET;
        }
        
        if(parent == null) {
            LOGGER.warn("Module Definition URI is null");
            return Collections.EMPTY_SET;
        }
        
        FunctionalComponent sbolParticipant = workspace.getObject(constraint, FunctionalComponent.class, contexts);
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(sbolParticipant)
                    .e(SBDirection.IN, SynBadTerms.SynBadParticipant.isSbolParticipant)
                    .v(SBDirection.IN, SvpParticipant.class)
                    .e(SBDirection.IN, SynBadTerms.SynBadInteraction.hasParticipant)
                    .v(SBDirection.IN, SvpInteraction.class)
                    .e(SBDirection.IN, SynBadTerms.SynBadEntity.extensionOf)
                    .v(SBDirection.IN, Interaction.class)
                    .filter(i -> parent.equals(i.getParent().getIdentity()))
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 
    }

    @Override
    public Collection<URI> getSviFromParticipantInstance(URI fc, URI[] contexts) {

        if(fc == null) {
            LOGGER.warn("Functional Component URI is null");
            return Collections.EMPTY_SET;
        }

        FunctionalComponent sbolParticipant = workspace.getObject(fc, FunctionalComponent.class, contexts);
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(sbolParticipant.getDefinition().get())
                    .e(SBDirection.IN, SynBadTerms.SynBadParticipant.isSbolParticipant)
                    .v(SBDirection.IN, SvpParticipant.class)
                    .e(SBDirection.IN, SynBadTerms.SynBadInteraction.hasParticipant)
                    .v(SBDirection.IN, SvpInteraction.class)
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 
    }

    @Override
    public Collection<URI> getSviFromParticipantSvp(URI svp, URI[] contexts) {
        if(svp == null) {
            LOGGER.warn("SVP URI is null");
            return Collections.EMPTY_SET;
        }

        Svp svpParticipant = workspace.getObject(svp, Svp.class, contexts);
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(svpParticipant)
                    .e(SBDirection.IN, SynBadTerms.SynBadInteraction.isSvpParticipant)
                    .v(SBDirection.IN, SvpInteraction.class)
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 
    }

    @Override
    public Collection<URI> getIncompleteSvisWithInstancesInParent(URI md, URI[] contexts) {

        ModuleDefinition moduleDefinition = workspace.getObject(md, ModuleDefinition.class, contexts);
        
        if(moduleDefinition  == null) {
            LOGGER.warn("ModuleDefinition could not be retrieved: {}", f.getIdentity(md).getDisplayID());
            return Collections.EMPTY_SET;
        }
        
        Set<SvpInteraction> interactions = moduleDefinition.getInteractions().stream()
                .map(i -> i.as(SviInstance.class))
                .map(i -> i.flatMap(svi -> svi.getDefinition()))
                .filter(i -> i.isPresent())
                .map(i -> i.get())
                .collect(Collectors.toSet());
        
        Set<URI> toReturn = new HashSet<>();
        
        for(SvpInteraction svi : interactions) {
            
           //LOGGER.info("Testing {}", svi.getDisplayId());
           
           Set<SvpParticipant> svpParticipants = svi.getParticipants().stream()
                .filter(p -> p.getDirection() == ParticipationRole.Input 
                        || p.getDirection() == ParticipationRole.Modifier)
                .collect(Collectors.toSet());
        
            try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)){
                Set<ComponentDefinition> participations = g.getTraversal()
                        .v(svpParticipants.toArray(new SvpParticipant[]{}))
                        .e(SBDirection.IN, SynBadTerms.SynBadEntity.extensionOf)
                        .v(SBDirection.IN, Participation.class)
                        .filter(p -> p.getInteraction().getParent().equals(moduleDefinition))
                        .filter(p -> p.getParticipant() != null)
                        .getResult().streamVertexes().map(v -> (Participation)v)
                        .map(p -> p.getParticipant().getDefinition().get())
                        .collect(Collectors.toSet());
                
                if(!svpParticipants.stream().allMatch(ip -> participations.contains(ip.getSbolParticipant()))) {
                   // LOGGER.trace("Is incomplete: {}", svi.getDisplayId());
                    toReturn.add(svi.getIdentity());
                }
            }
        }
        
       return toReturn;
    }

    @Override
    public Collection<URI> getParticipationInstancesFromDefinitionInMd(URI cd, URI md, URI[] contexts) {
        
        if(cd == null) {
            LOGGER.warn("Component Definition URI is null");
            return Collections.EMPTY_SET;
        }
        
        ComponentDefinition def = workspace.getObject(cd, ComponentDefinition.class, contexts);
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            return g.getTraversal().v(def)
                    .e(SBDirection.IN, SynBadTerms.Sbol.definedBy)
                    .v(SBDirection.IN, FunctionalComponent.class)
                    .filter(fc -> fc.getParent().getIdentity().equals(md))
                    .getResult().streamVertexes().map(v -> v.getIdentity()).collect(Collectors.toSet());
        } 
    }

    @Override
    public boolean containsAllParticipants(ModuleDefinition parent, Collection<URI> participants, URI[] contexts) {

        if(parent == null) {
            LOGGER.error("Parent cannot be null");
            return false;
        }

        Collection<SvpParticipant> participantObjs = participants.stream()
                .map(uri -> workspace.getObject(uri, SvpParticipant.class, parent.getContexts()))
                .filter(p -> p != null).collect(Collectors.toSet());

        if(participants.size() != participantObjs.size()) {
            LOGGER.error("Could not retrieve all participants (missing {} of {})", participants.size() - participantObjs.size(), participants.size());
            return false;
        }

        Set<URI> componentDefs = parent.getFunctionalComponents().stream()
                .map(fc -> fc.getDefinition().get().getIdentity()).collect(Collectors.toSet());

        return participantObjs.stream()
                .filter(p -> p.getDirection() == ParticipationRole.Input || p.getDirection() == ParticipationRole.Modifier)
                .allMatch((SvpParticipant p) -> {
                    boolean b = componentDefs.contains(p.getSbolParticipant().getIdentity());
                    return b;
                });
    }
    
    @Override
    public boolean shouldBeInstanced(URI svi, URI moduleDefinition, URI[] contexts) {
        
        SvpInteraction interaction = workspace.getObject(svi, SvpInteraction.class, contexts);
        
        if(interaction == null) {
            LOGGER.warn("svp Interaction {} does not exist in [{}]", 
                    f.getIdentity(svi).getDisplayID(), 
                    f.getIdentity(contexts[0]).getDisplayID());
            return false;
        }
        
        ModuleDefinition md = workspace.getObject(moduleDefinition, ModuleDefinition.class, contexts);
        
        if(md == null) {
            LOGGER.warn("Module Definition {} does not exist in [{}]", 
                    f.getIdentity(moduleDefinition).getDisplayID(), 
                    f.getIdentity(contexts[0]).getDisplayID());
            return false;
        }
        
        Collection<URI> participants = interaction.getParticipants().stream()
                .filter(p -> p.getDirection() == ParticipationRole.Input || p.getDirection() == ParticipationRole.Modifier)
                .map(p -> p.getSbolParticipant().getIdentity())
                .collect(Collectors.toSet());
        
        Collection<URI> instanceDefinitions = md.getFunctionalComponents().stream()
                .map(fc -> fc.getDefinition().get().getIdentity())
                .collect(Collectors.toSet());
        
        return instanceDefinitions.containsAll(participants);
    }

    
    @Override
    public boolean isInstanced(URI svi, URI moduleDefinition, Collection<URI> participantFcs, URI[] contexts) {
        SvpInteraction interaction = workspace.getObject(svi, SvpInteraction.class, contexts);
        
        if(interaction == null) {
            LOGGER.warn("SVP Interaction {} does not exist in [{}]", 
                    f.getIdentity(svi).getDisplayID(), 
                    f.getIdentity(contexts[0]).getDisplayID());
            return false;
        }
        
        Collection<ComponentDefinition> sbolParticipants = interaction.getParticipants()
                .stream().filter(p -> p.getDirection() != ParticipationRole.Output)
                .map(p -> p.getSbolParticipant())
                .collect(Collectors.toSet());
        
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(workspace, contexts)) {
            Set<ComponentDefinition> cds = g.getTraversal().v(interaction)
                    .e(SBDirection.IN, SynBadTerms.SynBadEntity.extensionOf)
                    .v(SBDirection.IN, Interaction.class)
                    .filter(i -> moduleDefinition.equals(i.getParent().getIdentity()))
                    .e(SBDirection.OUT, SynBadTerms.SbolInteraction.hasParticipation)
                    .v(SBDirection.OUT, Participation.class)
                    .e(SBDirection.OUT, SynBadTerms.SbolInteraction.participant)
                    .v(SBDirection.OUT, FunctionalComponent.class)
                    .getResult().stream().map(fc -> ((FunctionalComponent)fc).getDefinition().get())
                    .collect(Collectors.toSet());
            
            return cds.containsAll(sbolParticipants);
        }
    }

}
