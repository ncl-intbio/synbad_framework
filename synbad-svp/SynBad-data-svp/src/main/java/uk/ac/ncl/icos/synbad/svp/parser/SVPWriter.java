 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.parser;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;

/**
 * Writes {@link Svp}s to JParts API objects, collected in {@link PartBundle}s.
 * @author owengilfellon
 */
public class SVPWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SVPWriter.class);
    private final SVPInteractionWriter interactionParser;

    public SVPWriter(SBWorkspace workspace, URI[] context)
    {
        interactionParser = new SVPInteractionWriter(workspace, context);
    }
    
    private Part parsePart(Svp svp) {
        
        Part parsed = new Part();
        createPartProperties(parsed, svp);
        return parsed;
    }
    
    private Map<Interaction, List<Part>> parseInternalInteractions(Svp svp) {
        Map<Interaction, List<Part>> interactions = new HashMap<>();
  
        for(SvpInteraction e : svp.getInternalInteractions()) {
            Interaction interaction = interactionParser.write(e);
            List<Part> parts = e.getParticipants().stream()
                    .map(p -> p.getSvpParticipant())
                    .map(part -> parsePart(part)).collect(Collectors.toList());
            interactions.put(interaction, parts);
        }
        
        return interactions;
    }

    private Map<Interaction, List<Part>> parseExternalInteractions(Set<SvpInteraction> interactions) {
        Map<Interaction, List<Part>> parsed = new HashMap<>();

        for(SvpInteraction e : interactions) {
            Interaction interaction = interactionParser.write(e);
            List<Part> parts = e.getParticipants().stream()
                    .map(SvpParticipant::getSvpParticipant)
                    .map(this::parsePart).collect(Collectors.toList());
            parsed.put(interaction, parts);
        }

        return parsed;
    }


    private Map<Interaction, List<Part>> parseExternalInteractions(Svp svp) {
        Map<Interaction, List<Part>> interactions = new HashMap<>();
  
        for(SvpInteraction e : svp.getExternalInteractions()) {
            Interaction interaction = interactionParser.write(e);
            List<Part> parts = e.getParticipants().stream()
                    .map(p -> p.getSvpParticipant())
                    .map(part -> parsePart(part)).collect(Collectors.toList());
            interactions.put(interaction, parts);
        }
        
        return interactions;
    }
    

    public PartBundle write(Svp svp)
    {
       LOGGER.debug("Writing part: {}", svp.getDisplayId());
        Part parsed = parsePart(svp);
        Map<Interaction, List<Part>> interactions = parseInternalInteractions(svp);
        Map<Interaction, List<Part>> interactionDocuments = parseExternalInteractions(svp);
        return interactionDocuments.isEmpty() ?
                new PartBundle(parsed, new ArrayList<>(interactions.keySet()), interactions.values().stream().flatMap(Collection::stream).collect(Collectors.toList())) :
                new PartBundle(parsed, new ArrayList<>(interactions.keySet()), interactions.values().stream().flatMap(Collection::stream).collect(Collectors.toList()),  interactionDocuments);
    }

    public PartBundle write(Svp svp, Set<SvpInteraction> externalInteractions)
    {
        LOGGER.debug("Writing part: {}", svp.getDisplayId());
        Part parsed = parsePart(svp);
        Map<Interaction, List<Part>> interactions = parseInternalInteractions(svp);
        Map<Interaction, List<Part>> interactionDocuments = parseExternalInteractions(externalInteractions);
        return interactionDocuments.isEmpty() ?
                new PartBundle(parsed, new ArrayList<>(interactions.keySet()), interactions.values().stream().flatMap(Collection::stream).collect(Collectors.toList())) :
                new PartBundle(parsed, new ArrayList<>(interactions.keySet()), interactions.values().stream().flatMap(Collection::stream).collect(Collectors.toList()),  interactionDocuments);
    }
    
    

    private void createPartProperties(Part part, Svp svp) {

        String name = svp.getDisplayId();
        LOGGER.debug("Writing name: {}", name);
        part.setName(name);
        
        String displayName = svp.getName();
        LOGGER.debug("Writing display name: {}", displayName);
        part.setDisplayName(displayName);
   
        String description = svp.getDescription();
        LOGGER.debug("Writing description: {}", description);
        part.setDescription(description == null ? "" : description);
        
        String sequence = svp.getSequence();
        LOGGER.debug("Writing sequence: {}", sequence);
        part.setSequence(sequence == null ? "" : "![CDATA["+sequence + "]]");

        String organism = svp.getOrganism();
        LOGGER.debug("Writing organism: {}", organism);
        part.setOrganism(organism == null ? "" : organism);
        
        String designMethod = svp.getDesignMethod();
        LOGGER.debug("Writing design method: {}", designMethod);
        part.setDesignMethod(designMethod == null ? "" : designMethod);
        
        String metaType = svp.getMetaType();
        LOGGER.debug("Writing metaType: {}", metaType);
        part.setMetaType((metaType == null || metaType.isEmpty()) ? "Part" : metaType);
        
        String status = svp.getStatus();
        LOGGER.debug("Writing status: {}", status);
        part.setStatus(status == null ? "" : status);
        
        // CDS referred to as FunctionalPart by VPR
        
        String partType = svp.getPartType();
        LOGGER.debug("Writing type: {}", partType);
        part.setType(partType);
        
        part.setProperties(new ArrayList<>());
        
        svp.getProperties().stream()
            .map(p ->  { 
                Property property = new Property(); 
                property.setName(p.getName());
                property.setValue(p.getPropertyValue());
                property.setDescription(p.getDescription());
                LOGGER.debug("Writing property: {} : {} : {}", 
                        property.getName(), property.getValue(), property.getDescription());
                
                return property;
            }).forEach(part::AddProperty);
  
    }
    
    private boolean isProduction(SvpInteraction interaction) {
        InteractionType type = interaction.getInteractionType();
        if(type.toString().contains("Production") || type.toString().contains("Phosphorylation") ||
                type.toString().contains("Formation"))
            return true;
        return false;
     }
}
