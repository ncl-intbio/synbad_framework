/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBComponent;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.TopLevel;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = SvpInteraction.TYPE)
public class SvpInteraction extends ASBComponent implements TopLevel  {

    public static final String TYPE = "http://www.synbad.org/Svi";
    private static final Logger logger = LoggerFactory.getLogger(SvpInteraction.class);
    
    // cache value on first get
    
    private Boolean isDnaBased = null;

    // Interactions are where the parameters are stored - for parts, in internal
    // interactions.
    
    // Interactions need parts as per SVPs.
    
    // Interactions also define participants, which can be used to identify
    // when interactions should be instantiated, or to select other entities
    // for instantiation based on desired functionality.
    
    private static final SBDataDefManager m = SBDataDefManager.getManager();

    public SvpInteraction(SBIdentity identity, SBWorkspace workspaceId, SBValueProvider valueProvider) {
        super(identity, workspaceId, valueProvider);
    }
    
    public Set<Interaction> getExtensions() {
        return getIncomingObjects(SynBadTerms.SynBadEntity.extensionOf, Interaction.class);
    }
    
    @Override
    public SBFlowObject getParent() {
        List<SynBadEdge> edges = ws.incomingEdges(this, SynBadTerms.SynBadPart.hasInternalInteraction, SBFlowObject.class, getContexts());
        if(edges.isEmpty()) {
            edges = ws.incomingEdges(this, SynBadTerms.SynBadEntity.hasChild, SBFlowObject.class, getContexts());;
        }
        return edges.stream().map(e -> ws.getEdgeSource(e, getContexts())).map(s -> (SBFlowObject) s).findFirst().orElse(null);
    }
    
    public SbolInteractionType getSbolInteractionType() {
        InteractionType t = getInteractionType();
        if(t == null)
            return null;
        switch(t) {
            case Degradation:
                return SbolInteractionType.DEGRADATION;
            case ProteinProduction:
                return SbolInteractionType.GENETIC_PRODUCTION;
            case PoPSModulation:
                return SbolInteractionType.CONTROL;
            default:
                return SbolInteractionType.BIOCHEMICAL_REACTION;
        }
    }
    
    public InteractionType getInteractionType() {
        URI role = getValue(SynBadTerms.VPR.interactionType).asURI();
        if(!m.containsDefinition(InteractionType.class, role.toASCIIString()))
            return null;
        
        return m.getDefinition(InteractionType.class, role.toASCIIString());
    }
    
    public Set<String> getPartParticipants() {
        return getOutgoingObjects(SynBadTerms.SynBadInteraction.isSvpParticipant, Svp.class)
                .stream().map(ASBIdentified::getDisplayId)
                .collect(Collectors.toSet());
    }

    private String getStringFromValue(String predicate) {
        SBValue v = getValue(predicate);
        
        if(v == null || !v.isString())
            return "";
        
        else return v.asString();
    }
    
    private Boolean getBooleanFromValue(String predicate) {
        SBValue v = getValue(predicate);
        
        if(v == null || !v.isBoolean())
            return null;
        
        else return v.asBoolean();
    }
    
//    public void setInteractionType(InteractionType interactionType) {
//        setValue(SynBadTerms.VPR.interactionType, SBValue.parseValue(interactionType.getUri()));
//    }

    public String getMathName() {
        return getStringFromValue(SynBadTerms.VPR.mathName);
    }
    
//    public void setMathName(String mathName) {
//        setValue( SynBadTerms.VPR.mathName, SBValue.parseValue(mathName));
//    } 
    
    public String getFreeTextMath() {
        return getStringFromValue(SynBadTerms.VPR.freeTextMath);
    }
    
//    public void setFreeTextMath(String freeTextMath) {
//        setValue(SynBadTerms.VPR.freeTextMath, SBValue.parseValue(freeTextMath));
//    }
    
    public boolean hasDnaParticipant() {
        if(this.isDnaBased != null)
            return this.isDnaBased;
        
        // store value and return
        
        this.isDnaBased = getParticipants().stream()
                .anyMatch(p -> p.getSbolTypes().contains(ComponentType.DNA));
        return this.isDnaBased;
    }
    
    public boolean isInternal() {
        return !ws.incomingEdges(this,
                SynBadTerms.SynBadPart.hasInternalInteraction,
                SBIdentified.class,
                getContexts()).isEmpty();
    }
    
//    public void setIsInternal(boolean isInternal) {
//        setValue(SynBadTerms.VPR.isInternal, SBValue.parseValue(isInternal));
//    }
    
    public boolean isReaction() {
        Boolean b = getBooleanFromValue(SynBadTerms.VPR.isReaction);
        return b == null ? false : b;
    }
    
//    public void setIsReaction(boolean isReaction) {
//        setValue(SynBadTerms.VPR.isReaction, SBValue.parseValue(isReaction));
//    }
    
    public boolean isReversible() {
        Boolean b = getBooleanFromValue(SynBadTerms.VPR.isReversible);
        return b == null ? false : b;
    }
    
//    public void setIsReversible(boolean isReversible) {
//        setValue(SynBadTerms.VPR.isReversible, SBValue.parseValue(isReversible));
//    }
    
    
    public boolean hasParticipant(URI svpIdentity) {
        return getOutgoingObjects(SynBadTerms.SynBadInteraction.hasParticipant, SvpParticipant.class)
                .stream().anyMatch(p -> p.getSvpParticipant().getIdentity().equals(svpIdentity));
    }
    
    @OwnedEdge(id = SynBadTerms.SynBadInteraction.hasParticipant, clazz = SvpParticipant.class)
    public Collection<SvpParticipant> getParticipants() {
        return getOutgoingObjects( SynBadTerms.SynBadInteraction.hasParticipant, SvpParticipant.class);
    }

    @OwnedEdge(id =  SynBadTerms.SynBadInteraction.hasParameter, clazz = Parameter.class)
    public Collection<Parameter> getParameters() {
        return getOutgoingObjects(SynBadTerms.SynBadInteraction.hasParameter, Parameter.class);
    }

    public SvpParticipant createParticipant(SBIdentity identity, ParticipationRole direction, PortState molecularForm, SbolInteractionRole role, int stoichiometry, String nameInMath, SBIdentity svpName, URI participantId) {
        
        SvpParticipant p = ws.getObject(identity.getIdentity(), SvpParticipant.class, getContexts());
        if(p!=null)
            return p;
        
        ws.perform(SvpParticipant.CreateParticipantAction(identity, getIdentity(), ws, getContexts())
                .setDirection(direction)
                .setMolecularForm(molecularForm)
                .setInteractionRole(role)
                .setStoichiometry(stoichiometry)
                .setNameInMath(nameInMath)
                .setSvp(svpName)
                .setSbolComponentId(participantId).build());
         p = ws.getObject(identity.getIdentity(), SvpParticipant.class, getContexts());
         return p;
    }

    public Parameter createParameter( SBIdentity identity, URI parentIdentity, String name, String type, double value, String scope, String evidenceType) {
        Parameter p = ws.getObject(identity.getIdentity(),Parameter.class, getContexts());
        if(p!=null)
            return p;
        else if (ws.containsIdentity(identity.getIdentity(), getContexts()))
            return null;
        ws.perform(Parameter.CreateParameter(identity, parentIdentity, name, type, value, scope, evidenceType, getWorkspace(), getContexts()));
        return ws.getObject(identity.getIdentity(), Parameter.class, getContexts());

    }

    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    public static SBAction CreateSvi(SBIdentity identity, Set<InteractionType> types, SBWorkspace workspace, URI[] contexts) {
   
        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(workspace,  contexts)
            .createObject(identity.getIdentity(), SBIdentityHelper.getURI(SvpInteraction.TYPE), null, null);
        types.stream().forEach(r -> b.createValue(identity.getIdentity(), 
                SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType),
                SBValue.parseValue(r.getUri()), 
                SBIdentityHelper.getURI(SvpInteraction.TYPE)));
        return b.build("CreateSvi ["+ identity.getDisplayID() + "]");
    }

}
