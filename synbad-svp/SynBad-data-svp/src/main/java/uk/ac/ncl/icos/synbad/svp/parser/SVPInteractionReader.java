/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.parser;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSviBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilderInternal;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;

/**
 *
 * @author owengilfellon
 */
public class SVPInteractionReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(SVPInteractionReader.class.getName());
    private static final SBDataDefManager DEFINITION_MANAGER = SBDataDefManager.getManager();
    private static final URI objtype = SBIdentityHelper.getURI(SvpInteraction.TYPE);
    
    private final SBWorkspace workspace;
    private final String prefix;
    private final String version;
    private final URI[] contexts;
    

    public SVPInteractionReader(SBWorkspace workspace, URI[] contexts)
    {
        this.workspace = workspace;
        this.contexts = contexts;
        this.prefix = "http://virtualparts.org";
        this.version = "1.0";
    }
    
    public SVPInteractionReader(SBWorkspace workspace, URI[] contexts, String prefix, String version)
    {
        this.workspace = workspace;
        this.contexts = contexts;
        this.prefix = prefix;
        this.version = version;  
    }
    
    public SvpInteraction read(Interaction interaction, Map<String, PartBundle> parts)
    {
        SBSviBuilder b = new SvpActionBuilderImpl(workspace, contexts);
        b = readToBuilder(b, interaction, parts);
        if( b == null || b.isEmpty())
            return null;
        workspace.perform(b.build("ReadInteraction [" + interaction.getName() + "]"));
   
            SBIdentity interactionId = workspace.getIdentityFactory().getIdentity(prefix, interaction.getName(), version);
            SvpInteraction svi = workspace.getObject(interactionId.getIdentity(), SvpInteraction.class, contexts);
            return svi;

    }
    
    public SBSviBuilder readToBuilder(SBSviBuilder b, Interaction interaction, Map<String, PartBundle> parts)
    {
            LOGGER.debug("Reading interaction to SvpActionBuilder: {}", interaction.getName());
            SBIdentity identity = workspace.getIdentityFactory().getIdentity(prefix, interaction.getName(), version);

            if(workspace.containsObject(identity.getIdentity(), SvpInteraction.class, contexts)) {
                LOGGER.debug("Found existing object, returning: {}", identity.getIdentity().toASCIIString());
                return b;
            }
            
            // Create SvpInteraction if non exists
            
            InteractionType type = DEFINITION_MANAGER.getDefinition(InteractionType.class, interaction.getInteractionType());
            
            if(type == null) {
                LOGGER.error("Cannot get type from: {}", interaction.getInteractionType());
            }
            
            if (null != type) switch (type) {
                case TranscriptionActivation:
                    LOGGER.debug("Parsing {}", type);
                    
                    InteractionPartDetail activator = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("Activator")).findFirst().orElse(null);
                    
                    if(activator == null) {
                        LOGGER.error("Could not get activator for {}", identity.getDisplayID());
                        return null;
                    }
                        
                    String promoterName = interaction.getParts().stream().filter(s -> !s.equals(activator.getPartName())).findFirst().orElse(null);
                    SBIdentity promoterId = workspace.getIdentityFactory().getIdentity(prefix, promoterName, version);
                    SynBadPortState state = DEFINITION_MANAGER.getDefinition(SynBadPortState.class, activator.getPartForm());
                    SBIdentity activatorId = workspace.getIdentityFactory().getIdentity(prefix, activator.getPartName(), version);
                    
                    if(promoterName == null) {
                        LOGGER.error("Could not get promotor for {}", identity.getDisplayID());
                        return null;
                    }
                    
                    if(state == null) {
                        LOGGER.error("Could not get activator state for {}", activatorId.getDisplayID());
                        return null;
                    }                       

                    double Km = getParameter("Km", interaction);
                    double n = getParameter("n", interaction);
                    
                    b = b.createActivateByPromoter(
                            identity, 
                            ComponentType.Protein, 
                            state, 
                            activatorId.getIdentity(), 
                            promoterId.getIdentity(), 
                            Km, n);
                    
                    break;
                case TranscriptionalRepression:
                    LOGGER.warn("Ignoring {}", type);
                    break;
                case TranscriptionalRepressionUsingOperator:
                    LOGGER.debug("Parsing {}", type);
                    
                    InteractionPartDetail repressor = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("Repressor")).findFirst().orElse(null);
                    
                    if(repressor == null) {
                        LOGGER.error("Could not get repressor for {}", identity.getDisplayID());
                        return null;
                    }
                    
                    String operatorName = interaction.getParts().stream().filter(s -> !s.equals(repressor.getPartName())).findFirst().orElse(null);
                    SBIdentity operatorId = workspace.getIdentityFactory().getIdentity(prefix, operatorName, version);
                    SynBadPortState repressorstate = DEFINITION_MANAGER.getDefinition(SynBadPortState.class, repressor.getPartForm());
                    SBIdentity repressorId = workspace.getIdentityFactory().getIdentity(prefix, repressor.getPartName(), version);
                    
                    if(operatorName == null) {
                        LOGGER.error("Could not get promotor for {}", identity.getDisplayID());
                        return null;
                    }
                    
                    if(repressorstate == null) {
                        LOGGER.error("Could not get repressor state for {}", repressorId.getDisplayID());
                        return null;
                    } 
   
                    double opKm = getParameter("Km", interaction);
                    double opn = getParameter("n", interaction);
                    
                    b = b.createRepressByOperator(
                            identity, 
                            ComponentType.Protein, 
                            repressorstate, 
                            repressorId.getIdentity(), 
                            operatorId.getIdentity(), 
                            opKm, 
                            opn);
                    break;
                case TranscriptionActivationUsingOperator:
                    LOGGER.warn("Ignoring {}", type);
                    break;
                case Phosphorylation:
                    LOGGER.debug("Parsing {}", type);
          
//                    InteractionPartDetail pPar1 = interaction.getPartDetails().stream()
//                            .filter(pd -> pd.getMathName().equals("Protein1_P")).findFirst().orElse(null);
                    InteractionPartDetail pPar2 = interaction.getPartDetails().stream()
                            .filter(pd -> pd.getMathName().equals("Protein1")).findFirst().orElse(null);
                    InteractionPartDetail pPar3 = interaction.getPartDetails().stream()
                            .filter(pd -> pd.getMathName().equals("Protein2")).findFirst().orElse(null);
//                    InteractionPartDetail pPar4 = interaction.getPartDetails().stream()
//                            .filter(pd -> pd.getMathName().equals("Protein2_P")).findFirst().orElse(null);
    
                    SBIdentity pPar2Id = workspace.getIdentityFactory().getIdentity(prefix, pPar2.getPartName(), version);
                    SBIdentity pPar3Id = workspace.getIdentityFactory().getIdentity(prefix, pPar3.getPartName(), version);
                    
                    PartBundle pPb2 = parts.get(pPar2.getPartName());
                    
                    Interaction dephosphorylation = pPb2.getInternalInteractions().stream().filter(i -> i.getInteractionType().equals("Dephosphorylation")).findFirst().orElse(null);
                    Interaction pDegradation = pPb2.getInternalInteractions().stream()
                            .filter(i -> i.getInteractionType().equals("Degradation"))
                            .filter(i -> i.getPartDetails().stream().anyMatch(pd -> pd.getPartForm().equals("Phosphorylated")))
                            .findFirst().orElse(null);
                    
                    double pKf = getParameter("kf", interaction);
                    double pKd = getParameter("kd", pDegradation);
                    double pKdeP = getParameter("kdeP", dephosphorylation);                    
                    
                    LOGGER.debug("Parsing {}", type);
                    
                    b = b.createPhosphorylate(
                            SvpIdentityHelper.getPhosphorylationInteractionIdentity(identity),
                            pPar2Id.getIdentity(),
                            pPar3Id.getIdentity(),
                            pKf, pKd, pKdeP);
                    
                    break;
                case ComplexFormation:
                    LOGGER.debug("Parsing {}", type);
          
                    InteractionPartDetail cfPar1 = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("Species1")).findFirst().orElse(null);
                    InteractionPartDetail cfPar2 = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("Species2")).findFirst().orElse(null);
                    InteractionPartDetail cfPar3 = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("SpeciesComplex")).findFirst().orElse(null);
    
                    SBIdentity cfPar1Id = workspace.getIdentityFactory().getIdentity(prefix, cfPar1.getPartName(), version);
                    SBIdentity cfPar2Id = workspace.getIdentityFactory().getIdentity(prefix, cfPar2.getPartName(), version);
                    SBIdentity cfPar3Id = workspace.getIdentityFactory().getIdentity(prefix, cfPar3.getPartName(), version);
  
                    PartBundle cfPb3 = parts.get(cfPar3.getPartName());
                    
                    SynBadPortState cfState1 = DEFINITION_MANAGER.getDefinition(SynBadPortState.class, cfPar1.getPartForm());
                    SynBadPortState cfState2 = DEFINITION_MANAGER.getDefinition(SynBadPortState.class, cfPar2.getPartForm());

                    Interaction complexDegradation = cfPb3.getInternalInteractions().stream()
                            .filter(i -> i.getInteractionType().equals("Degradation")).findFirst().orElse(null);
                    
                    double cfKf = getParameter("kf", interaction);
                    double cfKd = getParameter("kd", complexDegradation);

                    LOGGER.debug("Parsing {}", type);
                    b = b.createComplexFormation(cfPar3Id)
                            .setKf(cfKf)
                            .setKd(cfKd)
                            .setParticipant1(cfPar1Id)
                            .setType1(ComponentType.Protein)
                            .setState1(cfState1)
                            .setParticipant2(cfPar2Id)
                            .setType2(ComponentType.Protein)
                            .setState2(cfState2)
                            .builder().asInteractionBuilder();
                    break;
                case ComplexDisassociation: 
                    
                    LOGGER.debug("Parsing {}", type);
                    
                    InteractionPartDetail cdPar1 = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("Species1")).findFirst().orElse(null);
                    InteractionPartDetail cdPar2 = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("Species2")).findFirst().orElse(null);
                    InteractionPartDetail cdPar3 = interaction.getPartDetails().stream().filter(pd -> pd.getMathName().equals("SpeciesComplex")).findFirst().orElse(null);
    
                    SBIdentity cdPar1Id = workspace.getIdentityFactory().getIdentity(prefix, cdPar1.getPartName(), version);
                    SBIdentity cdPar2Id = workspace.getIdentityFactory().getIdentity(prefix, cdPar2.getPartName(), version);
                    SBIdentity cdPar3Id = workspace.getIdentityFactory().getIdentity(prefix, cdPar3.getPartName(), version);

                    SynBadPortState cdState1 = DEFINITION_MANAGER.getDefinition(SynBadPortState.class, cdPar1.getPartForm());
                    SynBadPortState cdState2 = DEFINITION_MANAGER.getDefinition(SynBadPortState.class, cdPar2.getPartForm());

                    double cdKb = getParameter("kb", interaction);

                    LOGGER.debug("Parsing {}", type);
                    b = b.getAs(SBSvpBuilderInternal.class).createComplexDisassociation(cdPar3Id)
                            .setKb(cdKb)
                            .setParticipant1(cdPar1Id)
                            .setType1(ComponentType.Protein)
                            .setState1(cdState1)
                            .setParticipant2(cdPar2Id)
                            .setType2(ComponentType.Protein)
                            .setState2(cdState2)
                            .builder().asInteractionBuilder();
                    break;
                case MetabolicReaction:
                    LOGGER.debug("Ignoring {}", type);
                    break;
                default:
                    LOGGER.debug("{}:{} was not an external interaction", interaction.getName(), type);
                    break;
            }
            
            b = addObjProperties(b, identity.getIdentity(), interaction);
            
            return b;

    }

   
    private SBSviBuilder addObjProperties(SBSviBuilder b, URI svpInteraction, Interaction interaction) {
        if(interaction.getInteractionType() != null && !interaction.getInteractionType().isEmpty()) {
                InteractionType t = DEFINITION_MANAGER.getDefinition(InteractionType.class, interaction.getInteractionType());
                if(t != null) {
                    LOGGER.debug("Reading type: {}", t);
                    b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(t.getUri()), objtype);
                } else {
                    LOGGER.error("Cannot get type from: {}", interaction.getInteractionType());
                }
            }
            
            if(interaction.getName() != null && !interaction.getName().isEmpty()) {
                SBValue v = SBValue.parseValue(interaction.getName());
                LOGGER.debug("Reading name: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasName), v, objtype);
            }
                
            if(interaction.getDescription() != null && !interaction.getDescription().isEmpty()) {
                SBValue v = SBValue.parseValue(interaction.getDescription());
                LOGGER.debug("Reading description: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasDescription), v, objtype);
            }
                
            if(interaction.getMathName() != null && !interaction.getMathName().isEmpty()) {
                SBValue v = SBValue.parseValue(interaction.getMathName());
                LOGGER.debug("Reading mathName: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), v, objtype);
            }
            
            if(interaction.getFreeTextMath() != null && !interaction.getFreeTextMath().isEmpty()) {
                SBValue v = SBValue.parseValue(interaction.getFreeTextMath());
                LOGGER.debug("Reading freeTextMath: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), v, objtype);  
            }
                
            if(interaction.getIsInternal() != null) {
                SBValue v = SBValue.parseValue(interaction.getIsInternal());
                LOGGER.debug("Reading isInternal: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), v, objtype);
            }
            
            if(interaction.getIsReaction() != null) {
                SBValue v = SBValue.parseValue(interaction.getIsReaction());
                LOGGER.debug("Reading isReaction: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), v, objtype);
            }
                
            
            if(interaction.getIsReversible() != null) {
                SBValue v = SBValue.parseValue(interaction.getIsReversible());
                LOGGER.debug("Reading isReversible: {}", v);
                b.getAs(SBDomainBuilder.class).createValue(svpInteraction, SBIdentityHelper.getURI(SynBadTerms.VPR.isReversible), v, objtype);
            }
            
             
            
        return b;
    }
    
    

    private SbolInteractionRole getRole(InteractionType type)
    {
        if(type == InteractionType.PoPSProduction)
            return SbolInteractionRole.PROMOTER;
        if(type == InteractionType.ProteinProduction)
            return SbolInteractionRole.PRODUCT;
        
        return SbolInteractionRole.REACTANT; 
    }

    private PortType getType(String string) {
        if(string.contains("PoPS"))
            return SynBadPortType.PoPS;
        if(string.contains("RiPS"))
            return SynBadPortType.RiPS;
        if(string.contains("mRNA"))
            return SynBadPortType.mRNA;
        if(string.contains("Protein") || string.contains("Species"))
            return SynBadPortType.Protein;
        if(string.contains("EnvironmentConstant"))
            return SynBadPortType.SmallMolecule;
        return null;
    }
    

    private boolean hasOnlyOutput(String speciesName, SynBadPortState state, Collection<InteractionPartDetail> ipd) {
        return hasDirection(speciesName, state, ipd, SBPortDirection.OUT) && !hasDirection(speciesName, state, ipd, SBPortDirection.IN);
    }
    
    private boolean hasDirection(String speciesName, SynBadPortState state, Collection<InteractionPartDetail> ipd, SBPortDirection direction)
    {
        for(InteractionPartDetail pd : ipd) {
            if( pd.getPartName().equals(speciesName) &&
                   state == SynBadPortState.valueOf(pd.getPartForm()) && 
                   direction == SBPortDirection.fromString(pd.getInteractionRole()))
               return true;
        }

        return false;
    }   
     
    private boolean isProduction(Interaction interaction) {
        String type = interaction.getInteractionType();
        if(type.contains("Production") || type.contains("Phosphorylation") ||
                type.contains("Formation"))
            return true;
        return false;
    }

    
    private double getParameter(String parameter, Interaction interaction) {
        Parameter par = interaction.getParameters().stream()
                        .filter(p -> p.getParameterType().equals(parameter))
                        .findFirst().orElse(null);
        
        if(par == null) {
            LOGGER.debug("Parameter could not be found: {}", parameter);
            return 0;
        }

        return par.getValue();
    }
}
