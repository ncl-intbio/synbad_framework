/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.example;

import java.net.URI;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.workspace.objects.DefaultSBIdentityFactory;

/**
 *
 * 
 * 
 * @author owengilfellon
 */
public class ExampleFactory {
    
    private static final Logger logger = LoggerFactory.getLogger(ExampleFactory.class);
    private static final SBIdentityFactory f = new DefaultSBIdentityFactory(null);
    private static final String PREFIX = "http://www.synbad.org";
    private static final String VERSION = "1.0";
    
    public static final SBIdentity M_SUBTILIN_RECEIVER = f.getIdentity(PREFIX, "SubtilinReceiver_SVP", VERSION);
    
    public static final SBIdentity M_DETECTOR = f.getIdentity(PREFIX, "Receiver_SVP", VERSION);
    public static final SBIdentity M_REPORTER = f.getIdentity(PREFIX, "Reporter_SVP", VERSION);

    public static final SBIdentity P_DETECTOR_POPS_OUT = f.getIdentity(PREFIX, M_DETECTOR.getDisplayID() + "_proxy_signal_pops_out", VERSION);
    public static final SBIdentity P_REPORTER_POPS_IN = f.getIdentity(PREFIX, M_REPORTER.getDisplayID() + "_proxy_signal_pops_in", VERSION);

    public static final SBIdentity C_SUBTILIN = f.getIdentity(PREFIX, "Subtilin", VERSION);
 
    public static final SBIdentity P_PSPARK = f.getIdentity(PREFIX, "PspaRK_SVP", VERSION);
    public static final SBIdentity P_RBS_SPAK = f.getIdentity(PREFIX, "RBS_SpaK_SVP", VERSION);
    public static final SBIdentity P_SPAK = f.getIdentity(PREFIX, "SpaK_SVP", VERSION);
    public static final SBIdentity P_RBS_SPAR = f.getIdentity(PREFIX, "RBS_SpaR_SVP", VERSION);
    public static final SBIdentity P_SPAR = f.getIdentity(PREFIX, "SpaR_SVP", VERSION);
    public static final SBIdentity P_TER_1 = f.getIdentity(PREFIX, "Ter1_SVP", VERSION);

    public static final SBIdentity P_PSPAS = f.getIdentity(PREFIX, "PspaS_SVP", VERSION);
    public static final SBIdentity P_RBS_SPAS = f.getIdentity(PREFIX, "RBS_SpaS_SVP", VERSION);
    public static final SBIdentity P_GFP = f.getIdentity(PREFIX, "GFP_SVP", VERSION);
    public static final SBIdentity P_TER_2 = f.getIdentity(PREFIX, "Ter2_SVP", VERSION);

    public static final SBIdentity I_SPAK_P_PHOS_SPAR = f.getIdentity(PREFIX, "SpaK_P_Phosphorylates_SpaR", VERSION);
    public static final SBIdentity I_SPAR_P_ACT_PSPAS = f.getIdentity(PREFIX, "SpaR_P_Activates_PspaS", VERSION);

    public static SvpModule setupWorkspace(SBWorkspace ws, URI[] contextURI) {
       
        URI workspaceURI = ws.getIdentity();
        ws.createContext(contextURI[0]);

        logger.info("Creating example model {} in {}", M_SUBTILIN_RECEIVER.getDisplayID(), workspaceURI.toASCIIString());

        // Create commands for constructs parts

        SBAction actions = new SvpActionBuilderImpl(ws,  contextURI)
            .createPromoter(P_PSPARK, 0.04)
            .createRBS(P_RBS_SPAK, 0.1818, 1.0)
            .createCDSWithPhosphorylatingSmallMolecule(P_SPAK, C_SUBTILIN,
                    0.0012,
                    1.0e-4,
                    0.0012,
                    0.1)
            .createRBS(P_RBS_SPAR, 0.1, 1.0)
            .createCDS(P_SPAR,  0.0012)
            .createTerminator(P_TER_1)
            .createPromoter(P_PSPAS, 0.04)
            .createRBS(P_RBS_SPAS, 0.145, 1.0)
            .createCDS(P_GFP, 0.0012)
            .createTerminator(P_TER_2)
            .createSvm(M_DETECTOR)
            .createSvm(M_REPORTER)
            .createSvm(M_SUBTILIN_RECEIVER)
            .addSvp(M_DETECTOR, P_PSPARK, null)
            .addSvp(M_DETECTOR, P_RBS_SPAK, null)

            // add spak and internal interactions

            .addSvp(M_DETECTOR, P_SPAK, null)
            .addSvp(M_DETECTOR, P_RBS_SPAR, null)

            // add spar and internal itneractions

            .addSvp(M_DETECTOR, P_SPAR, null)
            .addSvp(M_DETECTOR, P_TER_1, null)
            .addSvp(M_DETECTOR, P_PSPAS, null)
            .addSvp(M_REPORTER, P_RBS_SPAS, null)
            .addSvp(M_REPORTER, P_GFP, null)
            .addSvp(M_REPORTER, P_TER_2, null)
            .addSvm(M_SUBTILIN_RECEIVER, M_DETECTOR, null)
            .addSvm(M_SUBTILIN_RECEIVER, M_REPORTER, null)
            .createPhosphorylate(I_SPAK_P_PHOS_SPAR, P_SPAK.getIdentity(), P_SPAR.getIdentity(),
                    1.0e-4,
                    0.0012,
                    0.1)
            .createActivateByPromoter(I_SPAR_P_ACT_PSPAS, ComponentType.Protein, SynBadPortState.Phosphorylated, P_SPAR.getIdentity(), P_PSPAS.getIdentity(),
                    320.0,
                    2.0)
            .build();

        // Note, no DNA or whatnot associated at this point?

        ws.perform(actions);

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, contextURI);

        SvpInteraction activation = ws.getObject(I_SPAR_P_ACT_PSPAS.getIdentity(), SvpInteraction.class, contextURI);
        Svp rbsspas = ws.getObject(P_RBS_SPAS.getIdentity(), Svp.class, contextURI);

        URI popsIn = rbsspas.getPorts(SBPortDirection.IN, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);
        URI popsOut = activation.getPorts(SBPortDirection.OUT, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);

        // create hierarchy

        SBAction actions2 = new SvpActionBuilderImpl(ws, contextURI)
            .createProxyPort(P_DETECTOR_POPS_OUT, popsOut, M_DETECTOR.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .createProxyPort(P_REPORTER_POPS_IN, popsIn, M_REPORTER.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .build();

        ws.perform(actions2);
        return subtilinReceiverSvp;
    }
        
    public static SvpModule setupWorkspace() {
        URI workspaceURI = SBIdentityHelper.getURI("http://www.synbad.org/test_workspace/1.0");
        SBWorkspace ws = new SBWorkspaceBuilder(workspaceURI)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        URI[] contextURI = new URI[] { SBIdentityHelper.getURI("http://www.synbad.org/subtilin_reporter_context") };
        return setupWorkspace(ws, contextURI);
    }
    
    public static SvpModule setupWorkspaceOneTu() {
        
        URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
        URI[] contextURI = new URI[] { SBIdentityHelper.getURI("http://www.synbad.org/subtilin_reporter_context") };
        SBWorkspace ws = new SBWorkspaceBuilder(workspaceURI)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        logger.debug("Constructing identities");

        Random r = new Random();

        // Create commands for constructs parts

        SBAction actions = new SvpActionBuilderImpl(ws, contextURI)
            .createPromoter(P_PSPARK, r.nextDouble())
            .createRBS(P_RBS_SPAK, r.nextDouble(), 1.0)
            .createCDSWithPhosphorylatingSmallMolecule(P_SPAK, C_SUBTILIN, r.nextDouble(),  r.nextDouble(), r.nextDouble(), r.nextDouble())
            .createRBS(P_RBS_SPAR, r.nextDouble(), 1.0)
            .createCDS(P_SPAR,  r.nextDouble())
            .createTerminator(P_TER_1)
            .createPromoter(P_PSPAS, 1.0)
            .createRBS(P_RBS_SPAS, r.nextDouble(), 1.0)
            .createCDS(P_GFP, r.nextDouble())
            .createTerminator(P_TER_2)
            .createSvm(M_SUBTILIN_RECEIVER)
            .createPhosphorylate(I_SPAK_P_PHOS_SPAR, P_SPAK.getIdentity(), P_SPAR.getIdentity(), 0.5, 0.5, 0.5)
                .createActivateByPromoter(I_SPAR_P_ACT_PSPAS, ComponentType.Protein, SynBadPortState.Phosphorylated, P_SPAR.getIdentity(), P_PSPAS.getIdentity(), 0.1, 1)

            .addSvp(M_SUBTILIN_RECEIVER, P_PSPARK, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAK, null)

            // add spak and internal interactions

            .addSvp(M_SUBTILIN_RECEIVER, P_SPAK, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAR, null)

            // add spar and internal itneractions

            .addSvp(M_SUBTILIN_RECEIVER, P_SPAR, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_TER_1, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_PSPAS, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAS, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_GFP, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_TER_2, null)
            .build();

        // Note, no DNA or whatnot associated at this point?

        ws.perform(actions);

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, contextURI);

        return subtilinReceiverSvp;

    }
    
    public static SvpModule setupWorkspaceModuleTu() {
      
        URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
        URI[] contextURI = new URI[] { SBIdentityHelper.getURI("http://www.synbad.org/subtilin_reporter_context") };
        SBWorkspace ws = new SBWorkspaceBuilder(workspaceURI)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        logger.debug("Constructing identities");

        Random r = new Random();

        // Create commands for constructs parts

        SBAction actions = new SvpActionBuilderImpl(ws, contextURI)
            .createPromoter(P_PSPARK, r.nextDouble())
            .createRBS(P_RBS_SPAK, r.nextDouble(), 1.0)
            .createCDSWithPhosphorylatingSmallMolecule(P_SPAK, C_SUBTILIN, r.nextDouble(),  r.nextDouble(), r.nextDouble(), r.nextDouble())
            .createRBS(P_RBS_SPAR, r.nextDouble(), 1.0)
            .createCDS(P_SPAR,  r.nextDouble())
            .createTerminator(P_TER_1)
            .createPromoter(P_PSPAS, 1.0)
            .createRBS(P_RBS_SPAS, r.nextDouble(), 1.0)
            .createCDS(P_GFP, r.nextDouble())
            .createTerminator(P_TER_2)
            .createSvm(M_DETECTOR)
            .createSvm(M_SUBTILIN_RECEIVER)
            .createPhosphorylate(I_SPAK_P_PHOS_SPAR, P_SPAK.getIdentity(), P_SPAR.getIdentity(), 0.5, 0.5, 0.5)
            .createActivateByPromoter(I_SPAR_P_ACT_PSPAS, ComponentType.Protein, SynBadPortState.Phosphorylated, P_SPAR.getIdentity(), P_PSPAS.getIdentity(), 0.1, 1)

            .addSvm(M_SUBTILIN_RECEIVER, M_DETECTOR, null)
            .addSvp(M_DETECTOR, P_PSPARK, null)
            .addSvp(M_DETECTOR, P_RBS_SPAK, null)

            // add spak and internal interactions

            .addSvp(M_DETECTOR, P_SPAK, null)
            .addSvp(M_DETECTOR, P_RBS_SPAR, null)

            // add spar and internal itneractions

            .addSvp(M_DETECTOR, P_SPAR, null)
            .addSvp(M_DETECTOR, P_TER_1, null)
            .addSvp(M_DETECTOR, P_PSPAS, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAS, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_GFP, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_TER_2, null)
            .build();

        ws.perform(actions);

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, contextURI);

         SvpInteraction activation = ws.getObject(I_SPAR_P_ACT_PSPAS.getIdentity(), SvpInteraction.class, contextURI);
        Svp rbsspas = ws.getObject(P_RBS_SPAS.getIdentity(), Svp.class, contextURI);

        URI popsIn = rbsspas.getPorts(SBPortDirection.IN, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);
        URI popsOut = activation.getPorts(SBPortDirection.OUT, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);

        // create hierarchy

        SBAction actions2 = new SvpActionBuilderImpl(ws,  contextURI)
            .createProxyPort(P_DETECTOR_POPS_OUT, popsOut, M_DETECTOR.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .build();

        ws.perform(actions2);

        return subtilinReceiverSvp;
            
      
    }
    
    public static SvpModule setupWorkspaceMixedModules() {
        
        URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
        URI[] contextURI = new URI[] { SBIdentityHelper.getURI("http://www.synbad.org/subtilin_reporter_context") };
        SBWorkspace ws = new SBWorkspaceBuilder(workspaceURI)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        logger.debug("Constructing identities");

        Random r = new Random();

        SBIdentity codingSequencesId = ws.getIdentityFactory().getIdentity(PREFIX, "CodingSequences_SVP", VERSION);

        SBIdentity p_codingSequences_ProteinOut = ws.getIdentityFactory().getIdentity(PREFIX, codingSequencesId.getDisplayID() + "_proxy_signal_protein_p_out", VERSION);
        SBIdentity p_codingSequences_ProteinIn = ws.getIdentityFactory().getIdentity(PREFIX, codingSequencesId.getDisplayID() + "_proxy_signal_protein_p_in", VERSION);

        SBIdentity p_reporter_popsIn2 = ws.getIdentityFactory().getIdentity(PREFIX, codingSequencesId.getDisplayID() + "_proxy_signal_pops_in_2", VERSION);

        // Create commands for constructs parts

        SBAction actions = new SvpActionBuilderImpl(ws, contextURI)
            .createPromoter(P_PSPARK, r.nextDouble())
            .createRBS(P_RBS_SPAK, r.nextDouble(), 1.0)
            .createCDSWithPhosphorylatingSmallMolecule(P_SPAK, C_SUBTILIN, r.nextDouble(),  r.nextDouble(), r.nextDouble(), r.nextDouble())
            .createRBS(P_RBS_SPAR, r.nextDouble(), 1.0)
            .createCDS(P_SPAR,  r.nextDouble())
            .createTerminator(P_TER_1)
            .createPromoter(P_PSPAS, 1.0)
            .createRBS(P_RBS_SPAS, r.nextDouble(), 1.0)
            .createCDS(P_GFP, r.nextDouble())
            .createTerminator(P_TER_2)
            .createSvm(M_REPORTER)
            .createSvm(codingSequencesId)
            .createSvm(M_SUBTILIN_RECEIVER)
            .createPhosphorylate(I_SPAK_P_PHOS_SPAR, P_SPAK.getIdentity(), P_SPAR.getIdentity(), 0.5, 0.5, 0.5)
            .createActivateByPromoter(I_SPAR_P_ACT_PSPAS, ComponentType.Protein, SynBadPortState.Phosphorylated, P_SPAR.getIdentity(), P_PSPAS.getIdentity(), 0.1, 1)

            .addSvp(M_SUBTILIN_RECEIVER, P_PSPARK, null)
            .addSvm(M_SUBTILIN_RECEIVER, codingSequencesId, null)

            .addSvp(codingSequencesId, P_RBS_SPAK, null)

            // add spak and internal interactions

            .addSvp(codingSequencesId, P_SPAK, null)
            .addSvp(codingSequencesId, P_RBS_SPAR, null)

            // add spar and internal itneractions

            .addSvp(codingSequencesId, P_SPAR, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_TER_1, null)
            .addSvp(M_REPORTER, P_PSPAS, null)
            .addSvp(M_REPORTER, P_RBS_SPAS, null)
            .addSvp(M_REPORTER, P_GFP, null)
            .addSvp(M_REPORTER, P_TER_2, null)
            .addSvm(M_SUBTILIN_RECEIVER, M_REPORTER, null)
            .build();

        // Note, no DNA or whatnot associated at this point?

        ws.perform(actions);

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, contextURI);

        SvpInteraction phos = ws.getObject(I_SPAK_P_PHOS_SPAR.getIdentity(), SvpInteraction.class, contextURI);
        SvpInteraction act = ws.getObject(I_SPAR_P_ACT_PSPAS.getIdentity(), SvpInteraction.class, contextURI);
        Svp rbsspar = ws.getObject(P_RBS_SPAR.getIdentity(), Svp.class, contextURI);
        Svp rbsspak = ws.getObject(P_RBS_SPAK.getIdentity(), Svp.class, contextURI);

        URI popsIn = rbsspak.getPorts(SBPortDirection.IN, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);
        URI popsIn2 = rbsspar.getPorts(SBPortDirection.IN, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);
        URI pproteinOut = phos.getPorts(SBPortDirection.OUT, SynBadPortType.Protein).stream()
                .filter(p -> p.getPortState() == SynBadPortState.Phosphorylated)
                .map(p -> p.getIdentity()).findFirst().orElse(null);
        URI pproteinIn = act.getPorts(SBPortDirection.IN, SynBadPortType.Protein).stream()
                .filter(p -> p.getPortState() == SynBadPortState.Phosphorylated)
                .map(p -> p.getIdentity()).findFirst().orElse(null);

        // create hierarchy

        SBAction actions2 = new SvpActionBuilderImpl(ws, contextURI)
            .createProxyPort(p_codingSequences_ProteinOut, pproteinOut, codingSequencesId.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .createProxyPort(P_REPORTER_POPS_IN, popsIn, codingSequencesId.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .createProxyPort(p_reporter_popsIn2, popsIn2, codingSequencesId.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .createProxyPort(p_codingSequences_ProteinIn, pproteinIn, M_REPORTER.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .build();

        ws.perform(actions2);

        return subtilinReceiverSvp;
    }
    
     public static SvpModule setupWorkspaceMixedModulesSml() {
        
        URI workspaceURI = UriHelper.synbad.namespacedUri("testWorkspace");
        URI[] contextURI = new URI[] { SBIdentityHelper.getURI("http://www.synbad.org/subtilin_reporter_context") };
         SBWorkspace ws = new SBWorkspaceBuilder(workspaceURI)
                 .addService(new PiUpdater())
                 .addService(new EnvConstantUpdater())
                 .addService(new SviUpdater())
                 .addService(new ExportedFcUpdater())
                 .addService(new SBWireUpdater()).build();

        logger.debug("Constructing identities");

        Random r = new Random();

        SBIdentity promoterModId = ws.getIdentityFactory().getIdentity(PREFIX, "Promoter_SVM", VERSION);
        SBIdentity cdsModId = ws.getIdentityFactory().getIdentity(PREFIX, "CDS_SVM", VERSION);

        SBIdentity p_popsOut = ws.getIdentityFactory().getIdentity(PREFIX, promoterModId.getDisplayID() + "_proxy_signal_protein_p_out", VERSION);
        SBIdentity p_ripsIn  = ws.getIdentityFactory().getIdentity(PREFIX, cdsModId.getDisplayID() + "_proxy_signal_protein_p_in", VERSION);

        // Create commands for constructs parts

        SBAction actions = new SvpActionBuilderImpl(ws, contextURI)
            .createPromoter(P_PSPARK, r.nextDouble())
            .createRBS(P_RBS_SPAK, r.nextDouble(), 1.0)
            .createCDSWithPhosphorylatingSmallMolecule(P_SPAK, C_SUBTILIN, r.nextDouble(),  r.nextDouble(), r.nextDouble(), r.nextDouble())
            .createSvm(M_SUBTILIN_RECEIVER)

            .createSvm(promoterModId)
            .createSvm(cdsModId)   
            .addSvp(promoterModId, P_PSPARK, null)
            .addSvp(cdsModId, P_SPAK, null)
            .addSvm(M_SUBTILIN_RECEIVER, promoterModId, cdsModId)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAK, null)
            .addSvm(M_SUBTILIN_RECEIVER, cdsModId, null)

            .build();

        // Note, no DNA or whatnot associated at this point?

        ws.perform(actions);

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, contextURI);

        Svp prom = ws.getObject(P_PSPARK.getIdentity(), Svp.class, contextURI);
        Svp cds = ws.getObject(P_SPAK.getIdentity(), Svp.class, contextURI);

        URI popsOut = prom.getPorts(SBPortDirection.OUT, SynBadPortType.PoPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);
        URI ripsIn = cds.getPorts(SBPortDirection.IN, SynBadPortType.RiPS).stream().map(p -> p.getIdentity()).findFirst().orElse(null);

        // create hierarchy

        SBAction actions2 = new SvpActionBuilderImpl(ws,  contextURI)
            .createProxyPort(p_popsOut, popsOut, promoterModId.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .createProxyPort(p_ripsIn, ripsIn, cdsModId.getIdentity(), SBIdentityHelper.getURI(SvpModule.TYPE))
            .build();

        ws.perform(actions2);

        return subtilinReceiverSvp;
    }
}
