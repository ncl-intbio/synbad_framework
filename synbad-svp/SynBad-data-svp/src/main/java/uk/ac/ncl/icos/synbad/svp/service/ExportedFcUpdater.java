/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.api.SBSvpUtil;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

import java.net.URI;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.workspace.events.SBFilterEdgeByPredicates;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;

public class ExportedFcUpdater extends ASBWorkspaceSubscriber implements SBWorkspaceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExportedFcUpdater.class);
    private SBWorkspace ws;
    private SBIdentityFactory f;

    @Override
    public SBWorkspace getWorkspace() {
        return ws;
    }

    @Override
    public void setWorkspace(SBWorkspace workspace) {
        this.ws = workspace;
        this.f = workspace.getIdentityFactory();
        workspace.getDispatcher().subscribe(new SBFilterEdgeByPredicates(
                SynBadTerms.SbolModule.hasFunctionalComponent,
                SynBadTerms.SbolModule.hasModule,
                SynBadTerms.SynBadEntity.hasPort,
                SynBadTerms.SynBadHelper.exported,
                SynBadTerms.SynBadEntity.hasWire), this);
    }

    @Override
    public void close() {
        this.ws.getDispatcher().unsubscribe(this);
        this.ws = null;
        this.f = null;
    }

    private boolean evtEdgeIs(SBGraphEvent<SynBadEdge> evt, String edge) {
        return evt.getData().getEdge().toASCIIString().equals(edge);
    }

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> evt) {
   
        URI[] contexts =  ((SBEvtWithContext)evt).getContexts();

        if(evt.getType() == SBGraphEventType.ADDED) {

            if(evtEdgeIs(evt, SynBadTerms.SynBadEntity.hasWire)) {
                    SBWire wire = ws.getObject(evt.getData().getTo(), SBWire.class, contexts);
                    if(wire != null) {
                        wire.getTo().getOwner().as(SvmInstance.class)
                                .ifPresent(svm -> updateDefinitionExports(svm.getDefinition().get(), contexts));
                    }

            } else if (evtEdgeIs(evt, SynBadTerms.SbolModule.hasFunctionalComponent) ||
                     evtEdgeIs(evt, SynBadTerms.SynBadEntity.hasPort)) {
                if (ws.isObject(evt.getData().getFrom(), SBFlowObject.class, contexts) ) {
                    SBFlowObject svm = ws.getObject(evt.getData().getFrom(), SBFlowObject.class, contexts);
                    updateDefinitionExports(svm, contexts);
                }
            }
        } else if ( evt.getType() == SBGraphEventType.REMOVED) {
            if(evtEdgeIs(evt, SynBadTerms.SynBadHelper.exported)) {
                URI outerExported = evt.getData().getTo();
                if(ws.containsObject(outerExported, FunctionalComponent.class, contexts)) {
                    SbolActionBuilder b = new SbolActionBuilder(ws, contexts);
                    FunctionalComponent outer = ws.getObject(outerExported, FunctionalComponent.class, contexts);
                    b.removeObjectAndDependants(outerExported, outer.getParent().getIdentity());
                    ws.perform(b.build("ExportedFcUpdate [" + outer.getDisplayId() + "]"));
                }
            }
        }
    }

    private void updateDefinitionExports( SBFlowObject portDefOwner, URI[] contexts) {

        if(portDefOwner == null)
            return;
        
        SbolActionBuilder b = new SbolActionBuilder(ws, contexts);

        // From SvmInstance owner of FC or Port Instances

        for (SvmInstance portInstanceOwner : SBSvpUtil.getInstances(portDefOwner, SvmInstance.class, contexts)) {

            if(portInstanceOwner == null) {
                LOGGER.error("Owner of port is null");
                return;
            }

           // if (portInstanceOwner.is(Module.TYPE) ) {

                // get any proxy instances of the Module / SvmInstance where IdentityConstraint is not null (i.e. trans
                // signals), export FCs


                portInstanceOwner.getPorts().stream()
                    .filter(SBPortInstance::isProxy)
                    .filter(pi -> pi.getIdentityConstraint() != null)

                    // check by direction and add / export as appropriate

                    .forEach(pi ->  {
                        if(pi.getPortDirection() == SBPortDirection.IN) {
                            addImportedFunctionalComponents(b, pi, contexts);
                        } else {
                            addExportedFunctionalComponents(b, pi, contexts);
                        }
                    });
          //  }
        }

        if (!b.isEmpty()) {
           ws.perform(b.build("ExportedFcUpdate [" + portDefOwner.getDisplayId() + "]"));
        }
    }

    private void addExportedFunctionalComponents(
            SbolActionBuilder b,
            SBPortInstance pi,
            URI[] contexts) {

        Module internalFcOwner = pi.getOwner().as(Module.class).get();
        ModuleDefinition externalFcOwner = internalFcOwner.getParent();
        ComponentDefinition constraintDef = ws.getObject(pi.getDefinition().getIdentityConstraint(), ComponentDefinition.class, contexts);

        if(constraintDef == null){
            LOGGER.error("Constraint Definition for {} is null: {}", pi.getDefinition().getDisplayId(), pi.getDefinition().getIdentityConstraint().toASCIIString());
            return;
        }
        
        SBIdentity funcComponentId = FunctionalComponent.getFunctionalComponentIdentity(
                f.getIdentity(externalFcOwner.getIdentity()),
                f.getIdentity(constraintDef.getIdentity()));
        
        if(ws.containsObject(funcComponentId.getIdentity(), FunctionalComponent.class, contexts))
            return;

        Optional<FunctionalComponent> toMap = ((Module)internalFcOwner).getDefinition()
                .flatMap(def -> def.getFunctionalComponents().stream()
                    .filter(fc -> fc.getDefinition()
                        .map(fcdef -> fcdef.getIdentity().equals(constraintDef.getIdentity()))
                        .orElse(Boolean.FALSE))
                    .findFirst());

        if(!toMap.isPresent()) {
           // LOGGER.error("Can't find instance of {} in {}", constraintDef.getDisplayId(), internalFcOwner.getDisplayId());
            return;
        }

        toMap.ifPresent(mt -> {
            if(!ws.outgoingEdges(mt, SynBadTerms.SynBadHelper.exported, FunctionalComponent.class, contexts).isEmpty())
             return;

            SBIdentity toMapId = f.getIdentity(mt.getIdentity());
            SBIdentity mapsToIdentity = MapsTo.getMapsToIdentity(f.getIdentity(internalFcOwner.getIdentity()), funcComponentId, toMapId, MapsToRefinement.useRemote);

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Exporting FC:\t{}:{}", externalFcOwner.getDisplayId(), funcComponentId.getDisplayID());

            b.createFuncComponent(funcComponentId.getIdentity(), constraintDef, externalFcOwner, SbolDirection.OUT, SbolAccess.PUBLIC);
            b.createValue(funcComponentId.getIdentity(),URI.create(SynBadTerms.Rdf.hasType), SBValue.parseValue(URI.create(SvpInstance.TYPE)), URI.create(SvpModule.TYPE));
            b.createMapsTo(mapsToIdentity.getIdentity(), internalFcOwner.getIdentity(), SBIdentityHelper.getURI(Module.TYPE), MapsToRefinement.useRemote, mt.getIdentity(), funcComponentId.getIdentity());
            b.createEdge(mt.getIdentity(), URI.create(SynBadTerms.SynBadHelper.exported), funcComponentId.getIdentity());
            b.addAction(new CreateEdge(funcComponentId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, pi.getIdentity(), ws.getIdentity(), ws.getSystemContextIds(contexts)));
        });
    }

    private void addImportedFunctionalComponents(
            SbolActionBuilder b,
            SBPortInstance pi,
            URI[] contexts) {

        ModuleDefinition externalFcOwner = pi.getOwner().as(Module.class).get().getParent();
        Module internalFcOwner = pi.getOwner().as(Module.class).get();
        ComponentDefinition constraintDef = ws.getObject(pi.getDefinition().getIdentityConstraint(), ComponentDefinition.class, contexts);

        if(constraintDef == null){
            LOGGER.error("Constraint Definition for {} is null: {}", pi.getDefinition().getDisplayId(), pi.getDefinition().getIdentityConstraint().toASCIIString());
            return;
        }

        SBIdentity funcComponentId = FunctionalComponent.getFunctionalComponentIdentity(
                f.getIdentity(internalFcOwner.getDefinition().get().getIdentity()),
                f.getIdentity(constraintDef.getIdentity()));

        if(ws.containsObject(funcComponentId.getIdentity(), FunctionalComponent.class, contexts))
            return;

        Optional<FunctionalComponent> toMap = externalFcOwner.getFunctionalComponents().stream()
                        .filter(fc -> fc.getDefinition()
                                .map(fcdef -> fcdef.getIdentity().equals(constraintDef.getIdentity()))
                                .orElse(Boolean.FALSE))
                        .findFirst();

        if(!toMap.isPresent()) {
            // LOGGER.error("Can't find instance of {} in {}", constraintDef.getDisplayId(), externalFcOwner.getDisplayId());
            return;
        }

        toMap.ifPresent(fcToMap -> {
            if(!ws.outgoingEdges(fcToMap, SynBadTerms.SynBadHelper.imported, FunctionalComponent.class, contexts).isEmpty())
                return;

            SBIdentity toMapId = f.getIdentity(fcToMap.getIdentity());
            SBIdentity mapsToIdentity = MapsTo.getMapsToIdentity(f.getIdentity(externalFcOwner.getIdentity()), funcComponentId, toMapId, MapsToRefinement.useRemote);

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Importing FC:\t{}:{}", internalFcOwner.getDisplayId(), funcComponentId.getDisplayID());

            b.createFuncComponent(funcComponentId.getIdentity(), constraintDef, internalFcOwner.getDefinition().get(), SbolDirection.OUT, SbolAccess.PUBLIC);
            b.createValue(funcComponentId.getIdentity(),URI.create(SynBadTerms.Rdf.hasType), SBValue.parseValue(URI.create(SvpInstance.TYPE)), URI.create(SvpModule.TYPE));
            b.createMapsTo(mapsToIdentity.getIdentity(), internalFcOwner.getIdentity(), SBIdentityHelper.getURI(Module.TYPE), MapsToRefinement.useRemote, fcToMap.getIdentity(), funcComponentId.getIdentity());
            b.createEdge(fcToMap.getIdentity(), URI.create(SynBadTerms.SynBadHelper.imported), funcComponentId.getIdentity());
            b.addAction(new CreateEdge(funcComponentId.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, pi.getIdentity(), ws.getIdentity(), ws.getSystemContextIds(contexts)));
        });
    }
}
