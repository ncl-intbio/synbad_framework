/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.vpr;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.synbad.svp.parser.SVPInteractionReader;
import uk.ac.ncl.icos.synbad.svp.parser.SVPReader;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class SBVprAdapter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SBVprAdapter.class);
    private final SVPManager m = SVPManager.getSVPManager();
    private final SBWorkspace workspace;
    private final Map<URI, PartBundle> partBundles = new HashMap<>();
    
    public SBVprAdapter(SBWorkspace workspace) {
        this.workspace = workspace;
        m.getAllConstPromoter();
        m.getAllInducPromoters();
        m.getAllNegOperators();
        m.getAllPromoters();
        m.getAllRBS();
        m.getAllTerminators();
    }
    
    public URI getVprIdentity(String name) {
        return UriHelper.vpr.namespacedUri(name);
    }
    
    public Svp resolve(Part part, URI[] contexts) {
        SVPReader reader = new SVPReader(workspace, contexts);
        Svp svp = reader.read(part);
        return svp;
    }
    
    public SvpInteraction resolve(Interaction interaction, URI[] contexts) {
        SVPInteractionReader reader = new SVPInteractionReader(workspace, contexts);
        Map<String, PartBundle> participants = new HashMap<>();
        interaction.getParts().stream().forEach(p -> {
            Part pb_part = m.getPart(p);
            List<Interaction> pb_interactions = m.getInternalEvents(pb_part);
            PartBundle pb = new PartBundle(pb_part, pb_interactions);
            participants.put(p, pb);
        });
        
        SvpInteraction i = reader.read(interaction, participants);
        return i;
    }
}
