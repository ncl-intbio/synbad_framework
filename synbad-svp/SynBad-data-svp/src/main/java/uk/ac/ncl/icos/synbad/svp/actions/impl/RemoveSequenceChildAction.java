/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import java.net.URI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceConstraint;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.actions.ADeferredAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class RemoveSequenceChildAction extends ADeferredAction {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveSequenceChildAction.class);
    
    protected final SBIdentity parentId;
    protected final Integer index;
    
    public RemoveSequenceChildAction(URI parentSvmIdentity, int index, SBWorkspace workspace, URI[] contexts) {
        super(SBGraphEventType.REMOVED, workspace, contexts);
        this.parentId = workspace.getIdentityFactory().getIdentity(parentSvmIdentity);
        this.index = index;
    }


    @Override
    protected SBAction getAction(SBWorkspace ws) {
 
        // SVM and SVP objects
        SvpModule parentSvm = ws.getObject(parentId.getIdentity(), SvpModule.class, contexts);

        if(parentSvm == null) {
            LOGGER.error("Could not get module: {}", parentId.getDisplayID());
            return null;
        }

        List<SBFlowObject> orderedChildren = parentSvm.getOrderedChildren();
        SBFlowObject child = parentSvm.getOrderedChildren().get(index);

        if(child == null) {
            LOGGER.error("Could not get child {} of module: {}", index, parentId.getDisplayID());
            return null;
        }

        LOGGER.debug("--- {} ---", child.getDisplayId());

        ComponentDefinition svmDnaDef = parentSvm.getDnaDefinition();
        Component component = parentSvm.getDnaDefinition().getOrderedComponents().get(index);
        SBIdentity componentId = ws.getIdentityFactory().getIdentity(component.getIdentity());

        SbolActionBuilder b = new SbolActionBuilder(ws, contexts)
                .removeObjectAndDependants(component.getIdentity(), svmDnaDef.getIdentity());

        if(orderedChildren.stream().filter(c -> c.getIdentity().equals(child.getIdentity())).count() == 1)
            b.removeEdge(parentId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), child.getIdentity());

        b = updateConstraintsFromRemoved(b, svmDnaDef, componentId);

        return b.build("RemoveSeqChild ["+ child.getDisplayId() + "]");
    }
    
    protected SbolActionBuilder updateConstraintsFromRemoved(SbolActionBuilder b, ComponentDefinition constraintOwner, SBIdentity removedComponentId) {
     
            List<Component> orderedComponents = constraintOwner.getOrderedComponents();
            
            // Get the component that is being removed

            Component toRemove = orderedComponents.stream()
                .filter(c -> c.getIdentity().equals(removedComponentId.getIdentity()))
                .findFirst().orElse(null);
            
            if(toRemove == null)
                return b;
            
            int toRemoveIndex = orderedComponents.indexOf(toRemove);
            
            if(toRemoveIndex == -1)
                return b;
            
            int beforeIndex = toRemoveIndex - 1;
            int afterIndex = toRemoveIndex + 1;
 
            // If there is both preceding and preceded, we must add constraint
            // constraint between them
            
            if(beforeIndex >= 0 && afterIndex < orderedComponents.size()){
                Component componentBefore = orderedComponents.get(beforeIndex);
                Component componentAfter = orderedComponents.get(afterIndex);
                SBIdentity constraintOwnerId = b.getWorkspace().getIdentityFactory().getIdentity(constraintOwner.getIdentity());
                SBIdentity precedingId = b.getWorkspace().getIdentityFactory().getIdentity(componentBefore.getIdentity());
                SBIdentity followingId = b.getWorkspace().getIdentityFactory().getIdentity(componentAfter.getIdentity());
                SBIdentity sequenceConstraintId = SequenceConstraint.getSequenceConstraintIdentity(constraintOwnerId, precedingId, followingId, ConstraintRestriction.PRECEDES);
                LOGGER.debug("Adding constraint:\t{}", sequenceConstraintId.getDisplayID());
                b = b.createSequenceConstraint(sequenceConstraintId.getIdentity(), constraintOwner.getIdentity(), precedingId.getIdentity(), ConstraintRestriction.PRECEDES, followingId.getIdentity());
            }
 
        return b;
    }
}

