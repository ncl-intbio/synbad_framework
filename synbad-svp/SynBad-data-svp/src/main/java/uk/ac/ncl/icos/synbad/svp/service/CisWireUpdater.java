/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import com.google.common.collect.HashMultimap;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 * Listens to an InteractionManager and creates or removes interactions as appropriate.
 * @author owengilfellon
 */ 
public class CisWireUpdater extends AWireUpdater  {

    private static final Logger LOGGER = LoggerFactory.getLogger(CisWireUpdater.class);

    public CisWireUpdater(SBIdentityFactory f) {
        super(f);
    }

    /**
     * Adds wires between sequence components.
     */
    public void applyToContext(SBWireContext ctx) {

        SvpModule svpModule = ctx.getWireParent().get();

        List<SBSvpSequenceObject<? extends SvpEntity>> orderedChildren = svpModule.getOrderedInstances();

        if(orderedChildren.isEmpty())
            return;

        HashMultimap<PortType, SBPortInstance> openPorts = HashMultimap.create();

        // iterate through ordered children, maintaining a map of open ports

        for(SBSvpSequenceObject<? extends SvpEntity> fc : orderedChildren) {

            // if terminator, clear open ports

            if(isFunctionalComponentWithType(fc, ComponentRole.Terminator)) {
                openPorts.clear();
            } else {

                Collection<SBPortInstance> portInstances = fc.getPorts();

                for(SBPortInstance p : portInstances) {
                    if(p.getPortDirection() ==  SBPortDirection.IN) {
                        openPorts.get(p.getPortType()).stream()
                            // if port is wired to an interaction, it cannot be wired to a cis-part
                            .filter(outPort -> outPort.getWires().stream().noneMatch(w -> w.getTo().getOwner().is(Interaction.TYPE)))
                            .findFirst().ifPresent(outPort -> {
                                // create wire if in port has a matching out port in map
                                createWire(outPort, p, ctx);
                            });
                    }
                }

                // if a module containing a terminator, clear open ports

                if(isModuleContainingType(fc, ComponentRole.Terminator))
                    openPorts.clear();

                // Add all out ports of current part to map

                for(SBPortInstance p : portInstances) {
                    if (p.getPortDirection() == SBPortDirection.OUT) {
                        if (openPorts.containsKey(p.getPortType())) {
                            openPorts.removeAll(p.getPortType());
                        }
                        openPorts.put(p.getPortType(), p);
                    }
                }
            }
        }
    }

    private boolean isFunctionalComponentWithType(SBIdentified fc, ComponentRole role) {
        return fc.is(SvpInstance.TYPE) && fc.as(SvpInstance.class)
            .flatMap(SvpInstance::getDefinition)
            .map(d -> d.getSbolRoles().contains(role)).orElse(Boolean.FALSE);
    }
    
    private boolean isModuleContainingType(SBIdentified obj, ComponentRole role) {
        if(!obj.is(Module.TYPE))
            return false;
        
        return obj.as(SvmInstance.class)
            .flatMap(SvmInstance::getDefinition)
            .map(e -> e.getChildren(Svp.class).stream().anyMatch(c -> c.getPartTypeAsRole() == role))
            .orElse(Boolean.FALSE);
    }
}
