/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.view;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHIterator;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.obj.*;
import uk.ac.ncl.icos.synbad.svp.obj.SviInstance;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;
import uk.ac.ncl.icos.synbad.view.DefaultSBPView;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class SvpPView extends DefaultSBPView<SvVo, SvpWire, SvpViewPort> implements Serializable {

    private final SvpModel model;
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpPView.class);


    
    public SvpPView(SvpModule rootNode, SvpModel model) {
        super(rootNode);
        this.model = model;
        addUpdater(new SvpPortViewUpdater(this, rootNode, model.getWorkspace()));
    }

    @Override
    public SvpModel getModel() {
        return model;
    }

    @Override
    public SBHIterator<SvVo> hierarchyIterator(SvVo instance) {
        return super.hierarchyIterator(instance); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SvmVo getRoot() {
        return (SvmVo)super.getRoot(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SvmVo getParent(SvVo node) {
        return (SvmVo)super.getParent(node); //To change body of generated methods, choose Tools | Templates.
    }
    
    private boolean isCisBased(SBValued fc) {
        return fc.as(FunctionalComponent.class).map(svp -> svp.getSbolTypes().contains(ComponentType.DNA)).orElse(Boolean.FALSE);
    }

    @Override
    protected SvVo createViewObject(SBValued object) {
        if( object.is(SvpModule.TYPE)) {
            return new SvmVo((SvpModule)object,this);
        } if(object.is(SvmInstance.TYPE)) {
            return new SvmVo((SvmInstance) object, this);
        } else if (object.is(SvpInstance.TYPE)) {
            SvpInstance svp = (SvpInstance) object;
            return new SvpVo(svp, this);
        } else if (object.is(SviInstance.TYPE)) {
            return new SviVo((SviInstance)object, this);
        } else if (object.is(FunctionalComponent.TYPE) && !isCisBased(object)) {
            LOGGER.error("Creating species: {}", object.getIdentity().toASCIIString());
            return new SvpSpeciesVo(object.as(FunctionalComponent.class).get(), this);
        }else {
            return null;
        }
    }

    @Override
    protected SvVo createViewObject(SvVo object) {
        if( object.is(SvpModule.TYPE)) {
            return new SvmVo((SvpModule) ((SvmVo)object).getObject(),this);
        } if(object.is(SvmInstance.TYPE)) {
            return new SvmVo((SvmInstance) ((SvmVo) object).getObject(), this);
        } else if (object.is(SvpInstance.TYPE)) {
            return new SvpVo(object.getObject().as(SvpInstance.class).get(), this);
        } else if (object.is(SviInstance.TYPE)) {
            return new SviVo(((SviVo)object).getObject(), this);
        } else if (object.is(FunctionalComponent.TYPE) && !isCisBased(object)) {
            LOGGER.error("Creating species: {}", object.getIdentity().toASCIIString());
            return new SvpSpeciesVo(object.getObject().as(FunctionalComponent.class).get(), this);
        } else {
            return null;
        }
    }

    @Override
    protected SvpViewPort createViewPort(SvpViewPort object) {
        return new SvpViewPort(object.getObject(), object.getId(),  this);
    }

    @Override
    protected SvpViewPort createViewPort(SBValued instance, SBValued definition) {
        return new SvpViewPort((SBPortInstance)instance, this);
    }

    
    @Override
    protected SvpWire createViewEdge(SvpWire edge) {
        return new SvpWire(edge.getEdge(), edge.getFrom(), edge.getTo(), this);
    }

    @Override
    protected SvpWire createViewEdge(SBEdge edge, SvpViewPort from, SvpViewPort to) {
        return new SvpWire((SynBadEdge)edge, from, to, this);
    }
    
    @Override
    public String toString() {
        return "SvpPView[ ".concat(getRoot().toString()).concat(" ]");
    }

    public <T extends SvVo> List<T> getChildren(SvmVo node, Class<T> clazz) {
        return super.getChildren(node).stream()
                .filter(c -> clazz.isAssignableFrom(c.getClass()))
                .map(c -> clazz.cast(c)).collect(Collectors.toList());
    }
    
    private String getPrettyName(Object node) {
        if (SBIdentified.class.isAssignableFrom(node.getClass()))
            return ((SBIdentified)node).getDisplayId();
        else if (DefaultSBViewEdge.class.isAssignableFrom(node.getClass())) {
           DefaultSBViewEdge edge = ((DefaultSBViewEdge)node);
           return getPrettyName(edge.getEdge().getEdge()) + ": " + getPrettyName(edge.getFrom()) + " -> " + getPrettyName(edge.getTo());
        } else if (URI.class.isAssignableFrom(node.getClass()))
            return ((URI)node).getPath().replace("/", "");
        else
            return node.toString();
    }

    boolean addEdgeByPort(SvpViewPort from, SvpViewPort to, SvpWire edge) {
        
        if(graph.addEdgeByPort(from, to, edge)) {
            registerEdge(edge);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Added {}", getPrettyName(edge));
            dispatchEdgeEvent(SBGraphEventType.ADDED, edge);
            //dispatchEdgeEvent(SBGraphEventType.VISIBLE, e);
            return true;
        }

        //LOGGER.error("Could not add edge: {} -> {}", from.getDisplayId(), to.getDisplayId());
        
        return false;
    }

    boolean addEdge(SvVo from, SvVo to, SvpWire edge) {

        SvVo f = from.getView().equals(this) ? from : createViewObject(from);
        SvVo t = to.getView().equals(this) ? to : createViewObject(to);

        if(!graph.isVisible(f)) {
            LOGGER.warn("Cannot add edge {} - source not visible: [ {} ]", edge, f);
            return false;
        } else if ( !graph.isVisible(t)) {
            LOGGER.warn("Cannot add edge {} - target not visible: [ {} ]", edge, t);
            return false;
        }

        SvpWire e = createViewEdge(edge);

        if(graph.addEdge(f, t, e)) {
            registerEdge(e);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Added {}", getPrettyName(e));
            dispatchEdgeEvent(SBGraphEventType.ADDED, e);
            //dispatchEdgeEvent(SBGraphEventType.VISIBLE, e);
            return true;
        }
        return false;
    }
    
    boolean removeAllEdges(Collection<? extends SvpWire> edges) {
        
        boolean b = true;
        
        for(SvpWire edge :  new HashSet<>(edges)) {
            if(!removeEdge(edge))
                b = false;
        }
        
        return b;
    }
    
    Set<SvpWire> removeAllEdges(SvVo sourceNode, SvVo targetNode) {
        Set<SvpWire> edges = new HashSet<>() ;
        for(SvpWire e: getAllEdges(sourceNode, targetNode)) {
            if(removeEdge(e))
                edges.add(e);
        }
        
        return edges;
    }
    
    boolean removeAllNodes(Collection<? extends SvVo> nodes) {
        
        boolean b = true;
        
        for(SvVo o : nodes) {
            if(!removeNode(o))
                b = false;
        }
        
        return b;
    }
    
    boolean addNode(SvVo instance, SvVo parent, int index) {
        
        if(parent == null)
            return false;

        SvVo c = createViewObject(instance);
        
        if(graph.containsNode(c)) {
            LOGGER.warn("Already in graph: [ {} ]", c);
            return false;
        }
 
        // is the node expanded or not?
        SvVo p = parent.getView().equals(this) ? parent : createViewObject(parent);
   
        if(graph.addNode(c, p, index)) {
            registerNode(c);
            dispatchNodeEvent(SBGraphEventType.ADDED, c, p);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Added {} to {} at index {}", getPrettyName(c), getPrettyName(p), index);
            if(isVisible(c))
                dispatchNodeEvent(SBGraphEventType.VISIBLE, c, p);
            return true;
        } else {
            LOGGER.warn("Could not add: [ {} ] to [ {} ]", c, p);
            return false;
        }
        
    }
    
    boolean addNode(SvVo child, SvVo parent) {
        
        if(parent == null)
            return false;
        
        return addNode(child, parent, getChildren(parent).size());
    }
    
    boolean removeNode(SvVo object) {
        
        SvVo o = object.getView().equals(this) ? object : (SvVo)createViewObject(object);
        boolean removed = true;

        if(!graph.containsNode(o))
            removed = false;

        new HashSet<>(getPorts(o)).stream().forEach(p -> {
            removePort(p, o);
        });

        // is the node expanded or not?
        if(!graph.removeNode(o)) 
            removed = false;
        
        deregisterNode(o);
        
        if(removed) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Removed {}", getPrettyName(o));
            dispatchNodeEvent(SBGraphEventType.REMOVED,  object, getParent(o));
        }

        return removed;
    }
    
    boolean addPort(SvpViewPort portNode, SvVo ownerNode, SBPortDirection direction, Set<String> constraints) {
        
        if(!graph.addPort(portNode, ownerNode, direction, constraints))
            return false;
        
        instancePorts.put(portNode.getObject(), portNode);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added {} port to {}", getPrettyName(portNode), getPrettyName(ownerNode));
        dispatchPortEvent(SBGraphEventType.ADDED, portNode);
        
        return true;
    }
    

    
    boolean removeEdge(SvpWire edge) {

        //ViewEdge<SvpViewPort, SynBadEdge> e = createViewEdge(edge);
         
//        if(e == null || (!isEdgeVisible(e)))
//            return false;

        boolean removed = true;

        if(!graph.removeEdge(edge)) {
            //LOGGER.error("Could not remove edge");
            removed = false;
        }

        deregisterEdge(edge);

        if(removed) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Removed {}", getPrettyName(edge));
            dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
        }

        return removed;
    }

    private boolean graphHasBothPorts(SvpWire edge) {
        return graph.containsPort(edge.getFrom()) && graph.containsPort(edge.getTo());
    }
    
    SvpWire removeEdge(SvVo sourceNode, SvVo targetNode) {
        SvpWire e =  graph.getEdge(sourceNode, targetNode);
        
        if(e == null)
            return null;

        removeEdge(e);
        return e;
    }
    
    boolean removeProxy(SvpViewPort proxy, SvpViewPort proxied) {
        return graph.removeProxy(proxy, proxied);
    }
    
    boolean setProxy(SvpViewPort proxy, SvpViewPort proxied) {
        return graph.setProxy(proxy, proxied);
    }
    
    boolean removePort(SvpViewPort portNode, SvVo ownerNode) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("REM port: {}", portNode.getIdentity().toASCIIString());

        removeAllEdges(getAllEdgesByPort(portNode));

        boolean removed = true;

        if (!graph.removePort(portNode, ownerNode))
            removed = false;
        
        instancePorts.remove(portNode.getObject(), portNode);
        return removed;
    }

    private void writeObject(ObjectOutputStream out) throws IOException
    {
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException
    {
        try {
            in.defaultReadObject();
            //addUpdater(new SvpPortViewUpdater(this, getRoot(), model.getWorkspace()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
