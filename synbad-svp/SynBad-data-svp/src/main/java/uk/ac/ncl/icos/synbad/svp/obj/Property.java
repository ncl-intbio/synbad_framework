/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Property.TYPE)
public class Property extends ASBIdentified {
    
    public static final String TYPE = "http://virtualparts.org/Property";
    private static final URI PROPERTY_TYPE = SBIdentityHelper.getURI(Property.TYPE);

    public Property(SBIdentity identity, SBWorkspace workspaceId, SBValueProvider valueProvider) {
        super(identity, workspaceId, valueProvider);
    }
    
    public String getPropertyValue() {
        return getValue(SynBadTerms.VPR.propertyValue).asString();    
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public static SBAction CreatePropertyAction(SBIdentity propertyIdentity, URI ownerIdentity, URI ownerType, String name, String value, String description, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace,  contexts)
            .createObject(propertyIdentity.getIdentity(), PROPERTY_TYPE, ownerIdentity, ownerType)
            .createEdge(ownerIdentity, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasProperty), propertyIdentity.getIdentity())
            .createValue(propertyIdentity.getIdentity(),SBIdentityHelper.getURI(SynBadTerms.VPR.propertyValue), SBValue.parseValue(value), PROPERTY_TYPE)
            .createValue(propertyIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasName), SBValue.parseValue(name), PROPERTY_TYPE)
            .createValue(propertyIdentity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolIdentified.hasDescription), SBValue.parseValue(description), PROPERTY_TYPE).build();
    }

}
