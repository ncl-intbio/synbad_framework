/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import java.net.URI;
import java.util.Collections;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SviExternalActionFactory {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(SviExternalActionFactory.class);
    private final static URI SVI_TYPE = SBIdentityHelper.getURI(SvpInteraction.TYPE);

    public static SBAction CreatePhosphorylationAction(SBIdentity identity, URI phosphorylator, URI phosphorylated, double kf, double kd, double kDeP, SBWorkspace ws, URI[] contexts) {

        SBIdentity phosOr = ws.getIdentityFactory().getIdentity(phosphorylator);
        SBIdentity phosEd = ws.getIdentityFactory().getIdentity(phosphorylated);

        SBIdentity phosEdIn_Id = SvpIdentityHelper.getProteinIdentity(phosEd);
        SBIdentity phosEdOut_Id = SvpIdentityHelper.getPhosphorylatedProteinIdentity(phosEd);

        SBIdentity phosOrIn_Id = SvpIdentityHelper.getPhosphorylatedProteinIdentity(phosOr);
        SBIdentity phosOrOut_Id = SvpIdentityHelper.getProteinIdentity(phosOr);

        SBIdentity portPhosEdIn = SvpIdentityHelper.getPortIdentity(identity, phosEdIn_Id, SBPortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default);
        SBIdentity portPhosEdOut = SvpIdentityHelper.getPortIdentity(identity, phosEdOut_Id, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated);

        SBIdentity portPhosOrIn = SvpIdentityHelper.getPortIdentity(identity, phosOrIn_Id, SBPortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated);
        SBIdentity portPhosOrOut = SvpIdentityHelper.getPortIdentity(identity, phosOrOut_Id, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);

        SBIdentity parPhosEdIn = Participation.getParticipationIdentity(identity, phosEdIn_Id);
        SBIdentity parPhosEdOut = Participation.getParticipationIdentity(identity, phosEdOut_Id);
        SBIdentity parPhosOrIn = Participation.getParticipationIdentity(identity, phosOrIn_Id);
        SBIdentity parPhosOrOut = Participation.getParticipationIdentity(identity, phosOrOut_Id);

        SBIdentity degradationId = SvpIdentityHelper.getDegradationIdentity(phosEd, SynBadPortState.Phosphorylated);
        SBIdentity dephosphorylationId = SvpIdentityHelper.getDePhosphorylationInteractionIdentity(phosEd);

        String math =   phosOr.getDisplayID() + "~P + " + phosEd.getDisplayID() + " -> " +
                        phosOr.getDisplayID() + " + " + phosEd.getDisplayID() + "~P";

        LOGGER.debug("Creating new Phosphorylation action: {}", identity.getDisplayID());

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.Phosphorylation))
            .addAction(ComponentDefinition.createComponentDefinition(phosEdOut_Id.getIdentity(), Collections.singleton(ComponentType.Protein), Collections.emptyList(), ws, contexts))
            //.addAction(SBSbolActions.createComponentDefinition(phosOrIn_Id.getIdentity(), Collections.singleton(ComponentType.Protein), Collections.singleton(ComponentRole.Generic), workspace, contexts))
            .addAction(new CreateEdge(phosEdOut_Id.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, identity.getIdentity(),  ws.getIdentity(), ws.getSystemContextIds(contexts)))
            .createPort(portPhosEdIn, SBPortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default, phosEdIn_Id.getIdentity(), identity.getIdentity(), SVI_TYPE)
            .createPort(portPhosEdOut, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Phosphorylated, phosEdOut_Id.getIdentity(), identity.getIdentity(),SVI_TYPE)
            .createPort(portPhosOrIn, SBPortDirection.IN, SynBadPortType.Protein, SynBadPortState.Phosphorylated, phosOrIn_Id.getIdentity(), identity.getIdentity(), SVI_TYPE)
            .createPort(portPhosOrOut, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default, phosOrOut_Id.getIdentity(), identity.getIdentity(), SVI_TYPE)
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "kf"),
                identity.getIdentity(), "kf", "kf", kf, "Local", "")
            .createParticipation(parPhosEdIn, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Protein2")
                .setSvp(phosEd)
                .setSbolComponentId(phosEdIn_Id.getIdentity()).builder()
            .createParticipation(parPhosOrIn, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setMolecularForm( SynBadPortState.Phosphorylated)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Protein1_P")
                .setSvp(phosOr)
                .setSbolComponentId(phosOrIn_Id.getIdentity()).builder()
            .createParticipation(parPhosEdOut, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setMolecularForm(SynBadPortState.Phosphorylated)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Protein2_P")
                .setSvp(phosEd)
                .setSbolComponentId(phosEdOut_Id.getIdentity()).builder()
            .createParticipation(parPhosOrOut, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Protein1")
                .setSvp(phosOr)
                .setSbolComponentId(phosOrOut_Id.getIdentity()).builder()
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), phosOr.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), phosEd.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.Phosphorylation.getUri()), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math),SVI_TYPE)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("TwoComponentPhosphorelay"), SVI_TYPE)

            // create internal interactions for degradation and dephosphorylation in the phosphorylated svp 
            // phosphorylator should already have them from another phos interaction, or from
            // an internal phosphorylatedbysmallmolecule interaction

            .createDegradation(degradationId, phosEd.getIdentity(), ComponentType.Protein, SynBadPortState.Phosphorylated, kd)
            .createDephosphorylate(dephosphorylationId, kDeP, phosEd.getIdentity())
            .createEdge(phosphorylated, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(phosphorylator, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .build();
    }
    
    /**
     * 
     * @param identity
     * @param promoterId The identity of the SVP promoter
     * @param type
     * @param state
     * @param modulator The identity of the SVP activator
     * @param km
     * @param n
     * @param contexts
     * @return 
     */
     public static SBAction CreateActivateByPromoterAction(SBIdentity identity, URI promoterId, ComponentType type, SynBadPortState state,  URI modulator, double km, double n, SBWorkspace ws, URI[] contexts) {

        URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(promoterId);
        SBIdentity modulatorId = ws.getIdentityFactory().getIdentity(modulator);
        SBIdentity constraintId = SvpIdentityHelper.getComponentIdentity(modulatorId, type, state);
        SBIdentity participantId1 = Participation.getParticipationIdentity(identity, constraintId);
        SBIdentity participantId2 = Participation.getParticipationIdentity(identity, svpId);
        SBIdentity modulatorIn = SvpIdentityHelper.getPortIdentity(identity, constraintId, SBPortDirection.IN, getType(type), state);
        SBIdentity popsIn = SvpIdentityHelper.getPortIdentity(identity, svpId, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsOut = SvpIdentityHelper.getPortIdentity(identity, svpId, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);

        String mod = //state == SynBadPortState.Phosphorylated
               // ? "Phosphorylated_" + modulatorId.getDisplayID()
                //:
                modulatorId.getDisplayID();
        String math = svpId.getDisplayID() + "_PoPS * " + mod + " / (" + mod + " + Km)";

        LOGGER.debug("Creating new Promoter Activation action: {}", identity.getDisplayID());

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.TranscriptionActivation))
            .createPort(modulatorIn, SBPortDirection.IN, getType(type), state,
                constraintId.getIdentity(), identity.getIdentity(), objType)
            .createPort(popsOut, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default,
                svpId.getIdentity(), identity.getIdentity(), objType)
            .createPort(popsIn, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default,
                svpId.getIdentity(), identity.getIdentity(), objType)

            // create ports on modulator and promoter

            .createEdge(promoterId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(modulator, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createParticipation(participantId1, identity.getIdentity())
                .setDirection(ParticipationRole.Modifier)
                .setMolecularForm(state)
                .setInteractionRole(SbolInteractionRole.MODIFIER)
                .setNameInMath("Activator")
                .setSvp( modulatorId)
                .setSbolComponentId(constraintId.getIdentity()).builder()
            .createParticipation(participantId2, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("DNA")
                .setSvp(svpId)
                .setSbolComponentId(svpId.getIdentity()).builder()
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "km"), identity.getIdentity(), "Km", "Km", km, "", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "n"), identity.getIdentity(), "n", "n", n, "", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "PoPSInput"), identity.getIdentity(), "PoPSInput", "PoPS", null, "Input", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "PopsOutput"), identity.getIdentity(), "PoPSOutput", "PoPS", null, "Output", "")
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), modulatorId.getIdentity())

            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.TranscriptionActivation.getUri()), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("PromoterInduction"), objType);

        if(state == SynBadPortState.Phosphorylated)
            b = b.addAction(ComponentDefinition.createComponentDefinition(constraintId.getIdentity(),
                    Collections.singleton(ComponentType.Protein), Collections.emptyList(), ws, contexts));

        return b.build();

    }

    public static SBAction CreateANDNRegulation(SBIdentity identity, URI promoterId,
                                                ComponentType type1, SynBadPortState state1,  URI activator,
                                                ComponentType type2, SynBadPortState state2,  URI repressor,
                                                double kmActivator, double nActivator,
                                                double kmRepressor, double nRepressor,  SBWorkspace ws, URI[] contexts) {

        URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);

        SBIdentity svpId = ws.getIdentityFactory().getIdentity(promoterId);

        SBIdentity modulatorId1 = ws.getIdentityFactory().getIdentity(activator);
        SBIdentity modulatorId2 = ws.getIdentityFactory().getIdentity(repressor);

        SBIdentity constraintId1 = SvpIdentityHelper.getComponentIdentity(modulatorId1, type1, state1);
        SBIdentity constraintId2 = SvpIdentityHelper.getComponentIdentity(modulatorId2, type2, state2);

        SBIdentity participantId1 = Participation.getParticipationIdentity(identity, constraintId1);
        SBIdentity participantId2 = Participation.getParticipationIdentity(identity, constraintId2);
        SBIdentity participantId3 = Participation.getParticipationIdentity(identity, svpId);


        SBIdentity modulatorIn1 = SvpIdentityHelper.getPortIdentity(identity, constraintId1, SBPortDirection.IN, getType(type1), state1);
        SBIdentity modulatorIn2 = SvpIdentityHelper.getPortIdentity(identity, constraintId2, SBPortDirection.IN, getType(type2), state2);

        SBIdentity popsIn = SvpIdentityHelper.getPortIdentity(identity, svpId, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsOut = SvpIdentityHelper.getPortIdentity(identity, svpId, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);

       // String math = svpId.getDisplayID() + "_PoPS * " + modulatorId1.getDisplayID() + " / (" + modulatorId1.getDisplayID() + " + Km)";
        String math = "PoPS * (" + modulatorId1.getDisplayID() + " / (" + modulatorId1.getDisplayID() + " + Km_" + modulatorId1.getDisplayID() + ")) * " +
                "( Km_" + modulatorId2.getDisplayID() +" / (" + modulatorId2.getDisplayID() + " + Km_" + modulatorId2.getDisplayID() +"))";

        LOGGER.debug("Creating new Promoter ANDN regulation action: {}", identity.getDisplayID());

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, contexts)
                .createSvi(identity, Collections.singleton(InteractionType.TranscriptionActivation))
                .createPort(modulatorIn1, SBPortDirection.IN, getType(type1), state1, constraintId1.getIdentity(), identity.getIdentity(), objType)
                .createPort(modulatorIn2, SBPortDirection.IN, getType(type2), state2, constraintId2.getIdentity(), identity.getIdentity(), objType)
                .createPort(popsOut, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default, svpId.getIdentity(), identity.getIdentity(), objType)
                .createPort(popsIn, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default, svpId.getIdentity(), identity.getIdentity(), objType)

                // create ports on regulator1 and promoter

                .createEdge(promoterId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
                .createEdge(activator, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
                .createEdge(repressor, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
                .createParticipation(participantId1, identity.getIdentity())
                    .setDirection(ParticipationRole.Modifier)
                    .setMolecularForm(state1)
                    .setInteractionRole(SbolInteractionRole.MODIFIER)
                    .setNameInMath("Activator")
                    .setSvp( modulatorId1)
                    .setSbolComponentId(constraintId1.getIdentity()).builder()
                .createParticipation(participantId2, identity.getIdentity())
                    .setDirection(ParticipationRole.Modifier)
                    .setMolecularForm(state2)
                    .setInteractionRole(SbolInteractionRole.MODIFIER)
                    .setNameInMath("Repressor")
                    .setSvp( modulatorId2)
                    .setSbolComponentId(constraintId2.getIdentity()).builder()
                .createParticipation(participantId3, identity.getIdentity())
                    .setDirection(ParticipationRole.Input)
                    .setInteractionRole(SbolInteractionRole.REACTANT)
                    .setNameInMath("DNA")
                    .setSvp(svpId)
                    .setSbolComponentId(svpId.getIdentity()).builder()
                .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "KA"), identity.getIdentity(), "KA", "Km", kmActivator, "", "")
                .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "nA"), identity.getIdentity(), "nA", "n", nActivator, "", "")
                .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "KR"), identity.getIdentity(), "KR", "Km", kmRepressor, "", "")
                .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "nR"), identity.getIdentity(), "nR", "n", nRepressor, "", "")
                .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "PoPSInput"), identity.getIdentity(), "PoPSInput", "PoPS", null, "Input", "")
                .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "PopsOutput"), identity.getIdentity(), "PoPSOutput", "PoPS", null, "Output", "")
                .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
                .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), modulatorId1.getIdentity())

                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.TranscriptionActivation.getUri()), objType)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), objType)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
                .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("ANDN_Promoter"), objType);

        if(state1 == SynBadPortState.Phosphorylated)
            b = b.addAction(ComponentDefinition.createComponentDefinition(constraintId1.getIdentity(),
                    Collections.singleton(ComponentType.Protein), Collections.emptyList(), ws, contexts));

        return b.build();

    }

    public static SBAction CreateRepressByOperatorAction(SBIdentity identity, URI promoterId, ComponentType type, SynBadPortState state,  URI modulator, double km, double n, SBWorkspace ws, URI[] contexts) {
 
        URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);
        
        SBIdentity svpId = ws.getIdentityFactory().getIdentity(promoterId);
        SBIdentity modulatorId = ws.getIdentityFactory().getIdentity(modulator);
        SBIdentity constraintId =  SvpIdentityHelper.getComponentIdentity(modulatorId, type, state);
        SBIdentity participantId1 = Participation.getParticipationIdentity(identity, constraintId);
        SBIdentity participantId2 = Participation.getParticipationIdentity(identity, identity);
        SBIdentity modulatorIn = SvpIdentityHelper.getPortIdentity(identity, constraintId, SBPortDirection.IN, getType(type), state);
        SBIdentity popsIn = SvpIdentityHelper.getPortIdentity(identity, svpId, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default);
        SBIdentity popsOut = SvpIdentityHelper.getPortIdentity(identity, svpId, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default);

//        String mod = state == SynBadPortState.Phosphorylated
//                ? "Phosphorylated_" + modulatorId.getDisplayID()
//                : modulatorId.getDisplayID();
        String math = "Km / (" + modulatorId.getDisplayID() + " + Km)";

        LOGGER.debug("Creating new Operator Repression action: {}", identity.getDisplayID());

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.TranscriptionalRepressionUsingOperator))
            .createPort(modulatorIn, SBPortDirection.IN, getType(type), state, constraintId.getIdentity(), identity.getIdentity(), objType)
            .createPort(popsOut, SBPortDirection.OUT, SynBadPortType.PoPS, SynBadPortState.Default, svpId.getIdentity(), identity.getIdentity(), objType)
            .createPort(popsIn, SBPortDirection.IN, SynBadPortType.PoPS, SynBadPortState.Default,svpId.getIdentity(), identity.getIdentity(), objType)

            // create ports on modulator and promoter

            .createEdge(promoterId, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(modulator, SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())

            .createParticipation(participantId1, identity.getIdentity())
                .setDirection(ParticipationRole.Modifier)
                .setMolecularForm(state)
                .setInteractionRole(SbolInteractionRole.MODIFIER)
                .setNameInMath("Repressor")
                .setSvp( modulatorId)
                .setSbolComponentId(constraintId.getIdentity()).builder()
            .createParticipation(participantId2, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("DNA")
                .setSvp(svpId)
                .setSbolComponentId(svpId.getIdentity()).builder()
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "km"), identity.getIdentity(), "Km", "Km", km, "", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "n"), identity.getIdentity(), "n", "n", n, "", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "Input"), identity.getIdentity(), "Input", "PoPS", null, "Input", "")
            .createParameter(SvpIdentityHelper.getParameterIdentity(identity, "Output"), identity.getIdentity(), "Output", "PoPS", null, "Output", "")
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), svpId.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), modulatorId.getIdentity())
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.TranscriptionalRepressionUsingOperator.getUri()), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(false), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isInternal), SBValue.parseValue(false), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("OperatorRepression"), objType)
            .build();
    }
    
    public static SBAction CreateComplexFormationAction(SBIdentity identity, SBIdentity complexId, SBIdentity participant1, ComponentType type1, SynBadPortState state1,
                                                            SBIdentity participant2, ComponentType type2, SynBadPortState state2, double kf, double kd, SBWorkspace ws, URI[] contexts) {
        
        URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);
       
        SBIdentity complexDefId = SvpIdentityHelper.getComplexIdentity(complexId);
        
        SBIdentity participant1Component = SvpIdentityHelper.getComponentIdentity(participant1, type1, state1);
        SBIdentity participant2Component = SvpIdentityHelper.getComponentIdentity(participant2, type2, state2);

        SBIdentity parParticipant1 = Participation.getParticipationIdentity(identity, participant1);
        SBIdentity parParticipant2 = Participation.getParticipationIdentity(identity, participant2);
        SBIdentity parComplex = Participation.getParticipationIdentity(identity, complexId);

        SBIdentity kfParameterId = SvpIdentityHelper.getParameterIdentity(identity, "kf");
        
        SBIdentity sviParticipant1Port =  SvpIdentityHelper.getPortIdentity(identity, participant1Component, SBPortDirection.IN, getType(type1), state1);
        SBIdentity sviParticipant2Port =  SvpIdentityHelper.getPortIdentity(identity, participant2Component, SBPortDirection.IN, getType(type2), state2);
        SBIdentity sviComplexPort =  SvpIdentityHelper.getPortIdentity(identity, complexDefId, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default);
        
        String math = state1 == SynBadPortState.Phosphorylated ? "Phosphorylated_" : "";
        math = math + participant1.getDisplayID() + " + ";
        math = state2 == SynBadPortState.Phosphorylated ? math + "Phosphorylated_" : math;
        math = math + participant2.getDisplayID() + " -> " + complexId.getDisplayID();


        LOGGER.debug("Creating new Complex Formation action: {}", identity.getDisplayID());

        return new SvpActionBuilderImpl(ws, contexts)
            .createComplex(complexId, kd)
            .createDegradation(SvpIdentityHelper.getDegradationIdentity(complexId, SynBadPortState.Default), complexId.getIdentity(), ComponentType.Complex, SynBadPortState.Default, kd)
            .createSvi(identity, Collections.singleton(InteractionType.ComplexFormation))
            .createPort(sviParticipant1Port, SBPortDirection.IN, getType(type1), state1, 
                    participant1Component.getIdentity(), identity.getIdentity(), objType)
            .createPort(sviParticipant2Port, SBPortDirection.IN, getType(type2), state2, 
                    participant2Component.getIdentity(), identity.getIdentity(), objType)
            .createPort(sviComplexPort, SBPortDirection.OUT, SynBadPortType.Complex, SynBadPortState.Default, 
                    complexDefId.getIdentity(), identity.getIdentity(), objType)

            .createEdge(complexId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(participant1.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(participant2.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createParticipation(parParticipant1, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setMolecularForm(state1)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Species1")
                .setSvp(participant1)
                .setSbolComponentId(participant1Component.getIdentity()).builder()
            .createParticipation(parParticipant2, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setMolecularForm(state2)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("Species2")
                .setSvp(participant2)
                .setSbolComponentId(participant2Component.getIdentity()).builder()
            .createParticipation(parComplex, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setInteractionRole(SbolInteractionRole.PRODUCT)
                .setNameInMath("SpeciesComplex")
                .setSvp(complexId)
                .setSbolComponentId(complexDefId.getIdentity()).builder()
            .createParameter(kfParameterId, identity.getIdentity(), "kf", "kf", kf, "Local", "")

            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), complexId.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant1.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant2.getIdentity())

            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.ComplexFormation.getUri()), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("ComplexFormationWithTwoSpecies"), objType)
            .build();

    }
    
    public static SBAction CreateComplexDisassociationAction(SBIdentity identity, SBIdentity complexId, SBIdentity participant1, ComponentType type1, SynBadPortState state1,
                                                            SBIdentity participant2, ComponentType type2, SynBadPortState state2, double kb, SBWorkspace ws, URI[] contexts) {

        URI objType = SBIdentityHelper.getURI(SvpInteraction.TYPE);
        
        SBIdentity complexDefId = SvpIdentityHelper.getComplexIdentity(complexId);

        SBIdentity participant1Component = SvpIdentityHelper.getComponentIdentity(participant1, type1, state1);
        SBIdentity participant2Component = SvpIdentityHelper.getComponentIdentity(participant2, type2, state2);

        SBIdentity parParticipant1 = Participation.getParticipationIdentity(identity, participant1);
        SBIdentity parParticipant2 = Participation.getParticipationIdentity(identity, participant1);
        SBIdentity parComplex = Participation.getParticipationIdentity(identity, complexId);
        
        SBIdentity kbParameterId = SvpIdentityHelper.getParameterIdentity(identity, "kb");

        SBIdentity sviParticipant1Port =  SvpIdentityHelper.getPortIdentity(identity, participant1Component, SBPortDirection.OUT, getType(type1), state1);
        SBIdentity sviParticipant2Port =  SvpIdentityHelper.getPortIdentity(identity, participant2Component, SBPortDirection.OUT, getType(type2), state2);
        SBIdentity sviComplexPort =  SvpIdentityHelper.getPortIdentity(identity, complexDefId, SBPortDirection.IN, SynBadPortType.Complex, SynBadPortState.Default);

        String math =  complexId.getDisplayID() + " -> ";
        math = state1 == SynBadPortState.Phosphorylated ? math + "Phosphorylated_" : math;
        math = math + participant1.getDisplayID() + " + ";
        math = state2 == SynBadPortState.Phosphorylated ? math + "Phosphorylated_" : math;
        math = math + participant2.getDisplayID();

        LOGGER.debug("Creating new Complex Disassociation action: {}", identity.getDisplayID());

        return new SvpActionBuilderImpl(ws, contexts)
            .createSvi(identity, Collections.singleton(InteractionType.ComplexFormation))
            .createPort(sviParticipant1Port, SBPortDirection.OUT, getType(type1), state1, 
                    participant1Component.getIdentity(), identity.getIdentity(), objType)
            .createPort(sviParticipant2Port, SBPortDirection.OUT, getType(type2), state2, 
                    participant2Component.getIdentity(), identity.getIdentity(), objType)
            .createPort(sviComplexPort, SBPortDirection.IN, SynBadPortType.Complex, SynBadPortState.Default, 
                    complexDefId.getIdentity(), identity.getIdentity(), objType)
            .createEdge(participant1.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(participant2.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
            .createEdge(complexId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadPart.hasInteraction), identity.getIdentity())
             .createParticipation(parComplex, identity.getIdentity())
                .setDirection(ParticipationRole.Input)
                .setInteractionRole(SbolInteractionRole.REACTANT)
                .setNameInMath("SpeciesComplex")
                .setSvp(complexId)
                .setSbolComponentId(complexDefId.getIdentity()).builder()
            .createParticipation(parParticipant1, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setMolecularForm(state1)
                .setInteractionRole(SbolInteractionRole.PRODUCT)
                .setNameInMath("Species1")
                .setSvp(participant1)
                .setSbolComponentId(participant1Component.getIdentity()).builder()
            .createParticipation(parParticipant2, identity.getIdentity())
                .setDirection(ParticipationRole.Output)
                .setMolecularForm(state2)
                .setInteractionRole(SbolInteractionRole.PRODUCT)
                .setNameInMath("Species2")
                .setSvp(participant2)
                .setSbolComponentId(participant2Component.getIdentity()).builder()
            .createParameter(kbParameterId, identity.getIdentity(), "kb", "kb", kb, "Local", "")

            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), complexId.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant1.getIdentity())
            .createEdge(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadInteraction.isSvpParticipant), participant2.getIdentity())


            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.interactionType), SBValue.parseValue(InteractionType.ComplexDisassociation.getUri()), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.isReaction), SBValue.parseValue(true), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.freeTextMath), SBValue.parseValue(math), objType)
            .createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.mathName), SBValue.parseValue("ComplexDisassociationWithTwoSpecies"), objType)
            .build();

    }

    /*
    public static SBAction CreateMetabolicReactionAction(SBIdentity identity, URI parentId, PortType type, PortState state, URI modulator, SBWorkspace ws, URI[] contexts) {
    
    }*/
    
    public static abstract class ComplexActionBuilder {
        
        private static final Logger LOGGER = LoggerFactory.getLogger(ComplexActionBuilder.class);
        protected final SBSvpBuilder b;
        protected final SBIdentity complexId;
        protected final SBWorkspace workspaceId;
        protected final URI[] contexts;
        protected SBIdentity participant1;
        protected ComponentType type1 = ComponentType.Protein;
        protected SynBadPortState state1 = SynBadPortState.Default;
        protected SBIdentity participant2;
        protected ComponentType type2 = ComponentType.Protein;
        protected SynBadPortState state2 = SynBadPortState.Default;

        public ComplexActionBuilder(SBSvpBuilder b, SBIdentity complexId, SBWorkspace workspaceId, URI[] contexts) {
            this.workspaceId = workspaceId;
            this.contexts = contexts;
            this.complexId = complexId;
            this.b = b;
        }

        public ComplexActionBuilder(SBIdentity complexId, SBWorkspace workspaceId, URI[] contexts) {
            this.workspaceId = workspaceId;
            this.contexts = contexts;
            this.complexId = complexId;
            this.b = null;
        }
        
        public ComplexActionBuilder setParticipant1(SBIdentity svpIdentity) {
            this.participant1 = svpIdentity;
            return this;
        }
        
        public ComplexActionBuilder setType1(ComponentType type) {
            this.type1 = type;
            return this;        
        }
    
        public ComplexActionBuilder setState1(SynBadPortState state) {
            this.state1 = state;
            return this;        
        }
        
        public ComplexActionBuilder setParticipant2(SBIdentity svpIdentity) {
            this.participant2 = svpIdentity;
            return this;        
        }
        
        public ComplexActionBuilder setType2(ComponentType type) {
            this.type2 = type;
            return this;        
        }
        
        public ComplexActionBuilder setState2(SynBadPortState state) {
            this.state1 = state;
            return this;       
        }

        protected abstract SBAction getAction();
 
        public SBSvpBuilder builder() {
            SBAction a = build();
            
            if(a == null)
                return null;
            if( b == null)
                new SvpActionBuilderImpl(workspaceId, contexts).addAction(a);
            if (!b.isBuilt())
                b.getAs(SBDomainBuilder.class).addAction(a);
            return b;
        }
        
        public SBAction build() {
            
            
            if(participant1 == null) {
                LOGGER.error("Participant1 is null" );
                return null;
            }
            
            if(participant2 == null) {
                LOGGER.error("Participant2 is null" );
                return null;
            }
            
            if(complexId == null) {
                LOGGER.error("ComplexId is null" );
                return null;
            }

            SBAction a = getAction();
        
            return a;
            
        }
    }
    
    public static class ComplexFormationActionBuilder extends ComplexActionBuilder {
        
        protected double kf = 0;
        protected double kd = 0;
        
        
        public ComplexFormationActionBuilder(SBSvpBuilder b, SBIdentity complexId, SBWorkspace workspaceId, URI[] contexts) {
            super(b, complexId, workspaceId, contexts);
        }

        public ComplexFormationActionBuilder(SBIdentity complexId, SBWorkspace workspaceId, URI[] contexts) {
            super(complexId, workspaceId, contexts);
        }
        
        public ComplexFormationActionBuilder setKf(double kf) {
            this.kf = kf;
            return this;       
        }
        
        public ComplexFormationActionBuilder setKd(double kd) {
            this.kd = kd;
            return this;       
        }

        @Override
        public ComplexFormationActionBuilder setParticipant1(SBIdentity svpIdentity) {
            return (ComplexFormationActionBuilder)super.setParticipant1(svpIdentity);
        }

        @Override
        public ComplexFormationActionBuilder setParticipant2(SBIdentity svpIdentity) {
            return (ComplexFormationActionBuilder)super.setParticipant2(svpIdentity);
        }

        @Override
        public ComplexFormationActionBuilder setState1(SynBadPortState state) {
            return (ComplexFormationActionBuilder) super.setState1(state);
        }

        @Override
        public ComplexFormationActionBuilder setState2(SynBadPortState state) {
            return (ComplexFormationActionBuilder) super.setState2(state);
        }

        @Override
        public ComplexFormationActionBuilder setType1(ComponentType type) {
            return (ComplexFormationActionBuilder) super.setType1(type);
        }

        @Override
        public ComplexFormationActionBuilder setType2(ComponentType type) {
            return (ComplexFormationActionBuilder) super.setType2(type);
        }

        @Override
        protected SBAction getAction() {
            SBIdentity formationId = SvpIdentityHelper.createExternalComplexationIdentity(participant1, participant2);

            if(complexId == null)
                LOGGER.warn("Complex ID is null in complex formation builder: {}", formationId.getDisplayID());
            
            if(participant1 == null)
                LOGGER.warn("Participant 1 is null in complex formation builder: {}", formationId.getDisplayID());
            
            if(participant2 == null)
                LOGGER.warn("Participant 2 is null in complex formation builder: {}", formationId.getDisplayID());
            
            
            SBAction a = new SvpActionBuilderImpl(workspaceId, contexts)
                    .addAction(CreateComplexFormationAction(formationId, complexId, 
                            participant1, type1, state1, 
                            participant2, type2, state2, kf, kd, workspaceId, contexts)).build();
            
            return a;
        }

    }
    
    public static class ComplexDisassociationActionBuilder extends ComplexActionBuilder {
        
        protected double kb = 0;
        
        public ComplexDisassociationActionBuilder(SBSvpBuilder b, SBIdentity complexId, SBWorkspace workspaceId, URI[] contexts) {
            super(b, complexId, workspaceId, contexts);
        }

        public ComplexDisassociationActionBuilder(SBIdentity complexId, SBWorkspace workspaceId, URI[] contexts) {
            super(complexId, workspaceId, contexts);
        }
        
        public ComplexDisassociationActionBuilder setKb(double kb) {
            this.kb = kb;
            return this;       
        }

        @Override
        public ComplexDisassociationActionBuilder setType1(ComponentType type) {
            return (ComplexDisassociationActionBuilder) super.setType1(type);
        }

        @Override
        public ComplexDisassociationActionBuilder setState2(SynBadPortState state) {
            return (ComplexDisassociationActionBuilder) super.setState2(state);
        }

        @Override
        public ComplexDisassociationActionBuilder setState1(SynBadPortState state) {
            return (ComplexDisassociationActionBuilder) super.setState1(state);
        }

        @Override
        public ComplexDisassociationActionBuilder setParticipant1(SBIdentity svpIdentity) {
            return (ComplexDisassociationActionBuilder) super.setParticipant1(svpIdentity);
        }

        @Override
        public ComplexDisassociationActionBuilder setParticipant2(SBIdentity svpIdentity) {
            return (ComplexDisassociationActionBuilder) super.setParticipant2(svpIdentity);
        }

        @Override
        public ComplexDisassociationActionBuilder setType2(ComponentType type) {
            return (ComplexDisassociationActionBuilder) super.setType2(type);
        }

        @Override
        protected SBAction getAction() {
            SBIdentity disassociationId = SvpIdentityHelper.getComplexDisassociationIdentity(complexId);

            if(participant1 == null)
                LOGGER.warn("Participant 1 is null in complex formation builder: {}", disassociationId.getDisplayID());
            
            if(participant2 == null)
                LOGGER.warn("Participant 2 is null in complex formation builder: {}", disassociationId.getDisplayID());
            
            SBAction a = new SvpActionBuilderImpl(workspaceId, contexts)
                    .addAction(CreateComplexDisassociationAction(disassociationId,  
                            complexId, 
                            participant1, type1, state1, 
                            participant2, type2, state2, kb, workspaceId, contexts))
                    .build();
            
            return a;
        }
    }
    
    private static SynBadPortType getType(ComponentType type) {
        if(type == ComponentType.Complex)
            return SynBadPortType.Complex;
        if(type == ComponentType.Protein)
            return SynBadPortType.Protein;
        if(type == ComponentType.SmallMolecule)
            return SynBadPortType.SmallMolecule;
        
        return null;
    }

}
