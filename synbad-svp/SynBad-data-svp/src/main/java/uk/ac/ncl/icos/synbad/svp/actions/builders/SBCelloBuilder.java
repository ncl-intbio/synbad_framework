/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.UnitBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.SinkUnitBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.ComplexationUnitBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.GeneratorUnitBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.SmlMolPhosUnitBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.ActivationBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.RepressionBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public interface SBCelloBuilder extends SBActionBuilder {

    GeneratorUnitBuilder createGenerator(SBIdentity identity);
    
    SinkUnitBuilder createSink(SBIdentity identity);

    UnitBuilder createUnit(SBIdentity identity, InteractionMode mode);

    SmlMolPhosUnitBuilder createSmlMolPhosUnit(SBIdentity identity, InteractionMode mode);
    
    ComplexationUnitBuilder createComplexationUnit(SBIdentity identity, InteractionMode mode);
    
    ActivationBuilder createActivateUnit(SBIdentity identity);

    RepressionBuilder createRepressUnit(SBIdentity identity);
    
    SBCelloBuilder createSvm(SBIdentity identity);
    
    SBCelloBuilder addSvm(SBIdentity parent, SBIdentity child, SBIdentity precedesComponent);
    
    SBCelloBuilder addSvm(SBIdentity parent, SBIdentity child, int index);
    
    //SBCelloBuilder regulateTu(SBIdentity regulatingUnit, SBIdentity regulatedUnit);

}
