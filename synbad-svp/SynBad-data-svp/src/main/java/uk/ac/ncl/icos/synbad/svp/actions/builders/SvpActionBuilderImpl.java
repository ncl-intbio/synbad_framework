/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.flow.action.FlowActions;
import uk.ac.ncl.icos.synbad.flow.definition.PortState;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.svp.actions.impl.*;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.flow.object.api.SBDefinition;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.ASBActionBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.obj.Parameter;
import uk.ac.ncl.icos.synbad.svp.obj.Property;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateValue;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveEdge;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObject;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObjectAndDependants;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.actions.SviExternalActionFactory;
import uk.ac.ncl.icos.synbad.svp.actions.SviInternalActionFactory;
import uk.ac.ncl.icos.synbad.svp.actions.SvpActions;

/**
 *
 * @author owengilfellon
 */
public class SvpActionBuilderImpl extends ASBActionBuilder implements SBSvpBuilder, SBSviBuilder, SBSvpBuilderInternal, SBDomainBuilder {
    
    private final DefaultSBDomainBuilder builder;
    
    public SvpActionBuilderImpl(SBWorkspace workspaceId, URI[] contexts) {
        super(workspaceId, contexts);
        this.builder = new DefaultSBDomainBuilder(workspaceId, contexts);
    }
    
    @Override
    public int getSize() {
        return builder.getSize();
    }
    
    
    @Override
    public boolean isEmpty() {
        return builder.isEmpty();
    }

    @Override
    public SBAction build(String label) {
        return builder.build(label);
    }

    @Override
    public SBAction build() {
        return builder.build();
    }

    @Override
    public boolean isBuilt() {
        return builder.isBuilt();
    }

    // Top Level

    public SvpActionBuilderImpl addAction(SBAction action) {
        if(!isBuilt())
            builder.addAction(action);
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createParameter( SBIdentity identity, URI parentIdentity, String name, String type, Double value, String scope, String evidenceType) {
        if(!isBuilt())
            builder.addAction(Parameter.CreateParameter(identity, parentIdentity, name, type, value, scope, evidenceType, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpParticipant.ParticipantActionBuilder createParticipation( SBIdentity identity, URI sviId) {
            //if(!isBuilt())
        return new SvpParticipant.ParticipantActionBuilder(this, identity, identityFactory.getIdentity(sviId), getWorkspace(), getContexts());
    }
    
    @Override
    public SvpActionBuilderImpl createProperty( SBIdentity propertyIdentity, URI ownerIdentity, URI ownerType, String name, String value, String description) {
        if(!isBuilt())
            builder.addAction(Property.CreatePropertyAction(propertyIdentity, ownerIdentity, ownerType, name, value, description, getWorkspace(), getContexts()));
        return this;
    }
     
    @Override
    public SvpActionBuilderImpl createSvp (SBIdentity identity, Set<Role> roles, ComponentType type) {
        
        if(!isBuilt()) {
            SBDataDefManager m  = SBDataDefManager.getManager();
            builder.addAction(Svp.CreateSvp(identity, roles, type, getWorkspace(), getContexts()));
            roles.stream().forEach(r -> {
                ComponentRole role = m.getDefinition(ComponentRole.class, r.getUri().toASCIIString());
                builder.createValue(identity.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.VPR.partTypeAsRole), SBValue.parseValue(role.getUri()), SBIdentityHelper.getURI(Svp.TYPE));
            });
        }
        
        return this;
    }



    @Override
    public SvpActionBuilderImpl createSvi( SBIdentity identity, Set<InteractionType> roles) {
        if(!isBuilt())
            builder.addAction(SvpInteraction.CreateSvi(identity, roles, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createSvm ( SBIdentity identity) {

    
            // Create module definitionx
            
           // SBIdentity mdId = identityFactory.getIdentity(identity.getUriPrefix(), "md_" + identity.getDisplayID(), identity.getVersion());
           
            // Create component definition for module's DNA
            
            SBIdentity cdId = identityFactory.getIdentity(identity.getUriPrefix(), "cd_" + identity.getDisplayID() + "_dna", identity.getVersion());
            
            // Create functional component to instance DNA
            
            SBIdentity fcId = FunctionalComponent.getFunctionalComponentIdentity(identity, cdId);

            if(!isBuilt()) {
                builder.addAction(SvpModule.CreateSvm(identity, getWorkspace(), getContexts()));
                builder.addAction(ModuleDefinition.createModuleDefinition(identity.getIdentity(), getWorkspace(), getContexts()));
                builder.addAction(ComponentDefinition.createComponentDefinition(cdId.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.Generic),  getWorkspace(), getContexts()));
                builder.addAction(FunctionalComponent.createFuncComponent(fcId.getIdentity(), cdId.getIdentity(), identity.getIdentity(), SbolAccess.PUBLIC, SbolDirection.INOUT, getWorkspace(), getContexts()));
                //builder.addAction(SvpActions.AddExtends(identity.getIdentity(), mdId.getIdentity(), getWorkspace(), getContexts()));
                builder.addAction(SvpActions.AddInstanceOfContents(cdId.getIdentity(), identity.getIdentity(), getWorkspace(), getContexts()));
            }
            return this;
  
    }

    
    @Override
    public SvpActionBuilderImpl addSvm ( SBIdentity parent, SBIdentity child, SBIdentity precedesComponent) {
        if(!isBuilt()) {
            builder.addAction(new AddSvmAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), precedesComponent == null ? null :precedesComponent.getIdentity(), getWorkspace(), getContexts()));
   //         builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl addSvp (  SBIdentity parent, SBIdentity child, int index) {
        if(!isBuilt())  {
            builder.addAction(new AddSvpAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), index, getWorkspace(), getContexts()));
   //         builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        return this;
    }


    
    @Override
    public SvpActionBuilderImpl addSvm ( SBIdentity parent, SBIdentity child, int index) {
        if(!isBuilt()) {
            builder.addAction(new AddSvmAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), index, getWorkspace(), getContexts()));
   //         builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl addSvp (  SBIdentity parent, SBIdentity child, SBIdentity precedesComponent) {
        if(!isBuilt())  {
            builder.addAction(
                    new AddSvpAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), precedesComponent == null ? null : precedesComponent.getIdentity(), getWorkspace(), getContexts()));
  //          builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        return this;
    }

   // @Override
    public SvpActionBuilderImpl addSvi (  SBIdentity parent, SBIdentity child) {
        if(!isBuilt())  {
            //builder.addAction(new AddSviAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), index, getWorkspace(), getContexts()));
            //         builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
//          builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
        return this;
    }
 
    @Override
    public SvpActionBuilderImpl createMrna(SBIdentity identity, URI parentId) {
        if(!isBuilt()) {
            builder.addAction(Svp.CreateMrna(identity, parentId, getWorkspace(), getContexts()));
        }
            
        return this;
    }
  
    @Override
    public SvpActionBuilderImpl addExtends (  SBIdentity sbComponent, SBIdentity sbol) {
        if(!isBuilt())
            builder.addAction(SvpActions.AddExtends(sbComponent.getIdentity(), sbol.getIdentity(), getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl addExtends (  SvpModule svp, ModuleDefinition sbol) {
        if(!isBuilt())
            builder.addAction(SvpActions.AddExtends(svp.getIdentity(), sbol.getIdentity(), getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl addExtends (  Svp svp, ComponentDefinition sbol) {
        if(!isBuilt())
            builder.addAction(SvpActions.AddExtends(svp.getIdentity(), sbol.getIdentity(), getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl addExtends (  SvpInteraction svp, Interaction sbol) {
        if(!isBuilt())
            builder.addAction(SvpActions.AddExtends(svp.getIdentity(), sbol.getIdentity(), getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl addInstanceOfContentsExtends (  FunctionalComponent instance, SvpModule moduleWithContents) {
        if(!isBuilt())
            builder.addAction(SvpActions.AddInstanceOfContents(instance.getIdentity(), moduleWithContents.getIdentity(), getWorkspace(),getContexts()));
        return this;
    }
    
    
    @Override
    public SvpActionBuilderImpl createWire(SBIdentity identity, SBDefinition parent, SBPortInstance from, SBPortInstance to) {
        if(!isBuilt())
            builder.addAction(FlowActions.createWire(identity, parent, from, to,  getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createPort(SBIdentity identity, SBPortDirection direction, PortType type, PortState state, URI identityConstraint, URI owner, URI ownerType) { 
        if(!isBuilt())
            builder.addAction(FlowActions.createPort(identity, direction, type, state, identityConstraint, owner, ownerType, getWorkspace(), getContexts()));
        return this;
    }
    


    @Override
    public SvpActionBuilderImpl createPortInstance(URI identity, URI owner, URI ownerType, URI definition) {
        if(!isBuilt())
            builder.addAction(FlowActions.createPortInstance(identity, owner, ownerType, definition, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType) {
        if(!isBuilt()) {
            builder.addAction(FlowActions.createProxyPort(identity.getIdentity(), proxiedPort, owner, ownerType, getWorkspace(), getContexts()));
   //         builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, owner, getWorkspace(), getContexts()));
        }
            
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createProteinProduction(SBIdentity identity, URI parentId) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreateProteinProductionAction(identity, parentId, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createDegradation(SBIdentity identity, URI parentId, ComponentType type, SynBadPortState state, double kd) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreateDegradationAction(identity, parentId, type, state, kd, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createMrnaProduction(SBIdentity identity, double ktr, double n, URI parentId) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreateMrnaProductionAction(identity, parentId, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createPoPSProduction(SBIdentity identity, double ktr, URI parentId) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreatePoPSProductionAction(identity, parentId, ktr, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createRiPSProduction(SBIdentity identity, double ktl, double n, URI parentId) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreateRiPSProductionAction(identity, parentId, ktl, n, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createPhosphorylateBySmallMolecule(SBIdentity identity, URI smallMolecule, double kf,  URI parentId) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreateSmallMoleculePhosphorylationAction(identity, parentId, smallMolecule, kf, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createDephosphorylate(SBIdentity identity, double kdep,  URI parentId) {
        if(!isBuilt())
            builder.addAction(SviInternalActionFactory.CreateDephosphorylationAction(identity, parentId, kdep, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createActivateByPromoter(SBIdentity identity, ComponentType type, SynBadPortState state, URI modulator, URI promoterId, double km, double n) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreateActivateByPromoterAction(identity, promoterId, type, state, modulator, km, n,  getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createActivateANDNByPromoter(SBIdentity identity, URI promoterId, ComponentType type1, SynBadPortState state1,  URI activator,
                                                         ComponentType type2, SynBadPortState state2,  URI repressor,
                                                         double kmActivator, double nActivator,
                                                         double kmRepressor, double nRepressor) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreateANDNRegulation(identity, promoterId, type1, state1, activator, type2, state2, repressor, kmActivator, nActivator, kmRepressor, nRepressor,  getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createRepressByOperator(SBIdentity identity, ComponentType type, SynBadPortState state, URI modulator, URI operatorId, double km, double n) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreateRepressByOperatorAction(identity, operatorId, type, state, modulator, km, n, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createPhosphorylate(SBIdentity identity, URI phosphorylator, URI phosphorylated, double kf, double kd, double kDeP) {
        if(!isBuilt())
            builder.addAction(SviExternalActionFactory.CreatePhosphorylationAction(identity,  phosphorylator, phosphorylated, kf, kd, kDeP, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SviExternalActionFactory.ComplexFormationActionBuilder createComplexFormation(SBIdentity complexId) {
        return new SviExternalActionFactory.ComplexFormationActionBuilder(this, complexId, getWorkspace(), getContexts());
    }
    
    @Override
    public SviExternalActionFactory.ComplexDisassociationActionBuilder createComplexDisassociation(SBIdentity complexId) {
        return new SviExternalActionFactory.ComplexDisassociationActionBuilder(this, complexId, getWorkspace(), getContexts());
    }

    @Override
    public SvpActionBuilderImpl removeObject(URI identity, URI ownerIdentity, URI type, URI ownerType) {
        if(!isBuilt())
            builder.addAction(new RemoveObject(identity, type, ownerIdentity, ownerType, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl removeObjectAndDependancies(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            builder.addAction(new RemoveObjectAndDependants(identity, ownerIdentity, getWorkspace(), getContexts()));
        return this;
    }
    
    
    @Override
     public SvpActionBuilderImpl duplicateObject(URI identity, URI[] fromContexts, boolean incrementIds) {
        if(!isBuilt())
            builder.duplicateObject(identity, fromContexts, incrementIds);
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl removeChild(URI parentSvm, int childIndex) {
        if(!isBuilt())
            builder.addAction(new RemoveSequenceChildAction(parentSvm, childIndex, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl removeChild(URI parentSvm, URI childComponentId) {
        if(!isBuilt())
            builder.addAction(new RemoveSequenceChildActionId(parentSvm, childComponentId, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.addAction(new CreateEdge(source, predicate.toASCIIString(), target, getWorkspace().getIdentity(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl removeEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.addAction(new RemoveEdge(source, predicate.toASCIIString(), target, getWorkspace().getIdentity(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.addAction(new CreateValue(source, predicate.toASCIIString(), value, sourceType, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl removeValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.addAction(new RemoveValue(predicate, predicate.toASCIIString(), value, sourceType, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createPromoter(SBIdentity identity, double ktr ) {
        if(!isBuilt())
            builder.addAction(Svp.createPromoter(identity, getWorkspace(), ktr, getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createOperator(SBIdentity identity
            //PortType type, PortState state, 
            //URI identityConstraint
            ) {
        if(!isBuilt())
            builder.addAction(Svp.CreateOperatorDefinitionAction(identity,  getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createUpstreamShim(SBIdentity identity, double efficiency) {
        if(!isBuilt())
            builder.addAction(Svp.CreateUpstreamShimAction(identity, efficiency, getWorkspace(), getContexts()));
        return this;
    }
        
    @Override
    public SvpActionBuilderImpl createDownstreamShim(SBIdentity identity, double efficiency) {
        if(!isBuilt())
            builder.addAction(Svp.CreateDownstreamShimAction(identity, efficiency, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createRBS(SBIdentity identity, double ktl, double volume) {
        if(!isBuilt())
            builder.addAction(Svp.CreateRBSDefinitionAction(identity, getWorkspace(), ktl, volume, getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createComplex(SBIdentity identity, double kd) {
        if(!isBuilt())
            builder.addAction(Svp.CreateComplexDefinitionAction(identity, getWorkspace(), kd, getContexts()));
        return this;
    }
    @Override
    public SvpActionBuilderImpl createCDSWithPhosphorylatingSmallMolecule(SBIdentity identity, SBIdentity smallMolecule, double degradationRate, double phosphorylationRate, double phosDegradationRate, double dephosphorylationRate) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSWithPhosphorylatingSmallMoleculeDefinitionAction(identity, smallMolecule, getWorkspace(), degradationRate, phosphorylationRate, phosDegradationRate, dephosphorylationRate, getContexts()));
        return this;
    }

    @Override
    public SBSvpBuilder createCDSWithComplexationSmallMolecule(SBIdentity identity, SBIdentity smallMolecule, SBIdentity complexIdentity, double degradationRate, double complexation, double disassociationRate, double complexDegradationRate) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSWithComplexationSmallMoleculeDefinitionAction(identity, smallMolecule, complexIdentity, getWorkspace(), degradationRate,  complexation, disassociationRate, complexDegradationRate, getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl createCDS(SBIdentity identity,  double Kd) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSDefinitionAction(identity, getWorkspace(),Kd, getContexts()));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createCDSWthPhosphorylated(SBIdentity identity,  double degradationRate, double pDegradationRate, double dePhosRate) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCDSDefinitionWithPhosphorylatedAction(identity, getWorkspace(), degradationRate, pDegradationRate, dePhosRate, contexts));
        return this;
    }
    
    @Override
    public SvpActionBuilderImpl createCompound(SBIdentity identity) {
        if(!isBuilt())
            builder.addAction(Svp.CreateCompoundDefinitionAction(identity, getWorkspace(), getContexts()));
        return this;
    }

    @Override
    public SvpActionBuilderImpl  createTerminator(SBIdentity identity) {
        if(!isBuilt())
            builder.addAction(Svp.CreateTerminatorDefinitionAction(identity, getWorkspace(), getContexts()));
        return this;
    }
    
     @Override
    public SvpActionBuilderImpl createObject(URI identity, URI rdfType, URI parentIdentity, URI parentRdfType) {
        if(!isBuilt())
            builder.createObject(identity, rdfType, parentIdentity, parentRdfType);
        return this;
    }


    @Override
    public SvpActionBuilderImpl removeObjectAndDependants(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            builder.removeObjectAndDependants(identity, ownerIdentity);
        return this;
    }
    /* public SvpActionBuilder updateWires(URI svmIdentity, URI workspaceId, URI[] contexts) {
        if(!isBuilt())
            builder.addAction(new UpdateTransWires(svmIdentity, workspaceId, contexts));
        return this;
    } */
    
    /*
    public SvpActionBuilder addSvi (  SBIdentity parent, SBIdentity child) {
        if(!isBuilt())  {
            builder.addAction(new AddSviAction(SBGraphEventType.ADDED, parent.getIdentity(), child.getIdentity(), getWorkspace(), getContexts()));
            builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, child.getIdentity(), getWorkspace(), getContexts()));
        //    builder.addAction(new UpdateWires(parent.getIdentity(),getWorkspace(), getContexts()));
        }
            
        return this;
    }*/
    
    /*
    
    public SvpActionBuilder removeWire(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            builder.addAction(FlowActions.removeWireAction(identity, ownerIdentity, getWorkspace(), getContexts()));
        return this;
    }*/
    
        /*

    public SvpActionBuilder removePort(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            builder.addAction(FlowActions.removePortInstance(identity, ownerIdentity, getWorkspace(), getContexts()));
        return this;
    }*/
    
    /*
    public SvpActionBuilder removePortInstance(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            builder.addAction(FlowActions.removePortInstance(identity, ownerIdentity, getWorkspace(), getContexts()));
        return this;
    }*/
    
        
    /*
    public SvpActionBuilder removeProxyPort(URI identity, URI ownerIdentity) {
        if(!isBuilt()) {
            builder.addAction(FlowActions.removeProxyPort(identity, ownerIdentity, getWorkspace(), getContexts()));
            builder.addAction(new UpdatePortInstances(SBGraphEventType.ADDED, ownerIdentity, getWorkspace(), getContexts()));
        }
        return this;
    }
    */
    
    /*
    public SvpActionBuilder createInduciblePromoter(SBIdentity identity, PortType type, PortState state, URI identityConstraint,double ktr, double n) {
        if(!isBuilt())
            builder.addAction(SvpActionFactory.CreateInduciblePromoterDefinitionAction(identity, type, state, identityConstraint, ktr, n, getWorkspace(), getContexts()));
        return this;
     }*/

    @Override
    public SBSviBuilder asInteractionBuilder() {
        return (SBSviBuilder)this;
    }

    @Override
    public SBSvpBuilder asPartBuilder() {
        return (SBSvpBuilder)this;
    }


}
