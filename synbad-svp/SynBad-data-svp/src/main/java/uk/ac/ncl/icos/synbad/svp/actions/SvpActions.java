/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions;

import java.net.URI;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SvpActions {
    
    public static SBAction AddExtends( URI svpidentity, URI sbolidentity, SBWorkspace workspace, URI[] contexts) {
        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(workspace,  contexts)
                .createEdge(sbolidentity, SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.extensionOf), svpidentity)
                .addAction(new CreateEdge(sbolidentity, SynBadTerms.SynBadSystem.dependantOn, svpidentity, workspace.getIdentity(), workspace.getSystemContextIds(contexts)));
        return b.build();
    }
    
    public static SBAction AddInstanceOfContents( URI componentDefinition, URI svpModule, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace,  contexts)
             .createEdge(componentDefinition, SBIdentityHelper.getURI(SynBadTerms.SynBadModule.instancedTuOf), svpModule).build();
    }  
}
