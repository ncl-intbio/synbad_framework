/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;

/**
 *
 * @author owen
 * @param <T> Instance Class
 * @param <V> Svp Definition Class
 */
public interface SBSvpObj<V extends SBFlowObject> extends SBIdentified {
    
    
}
