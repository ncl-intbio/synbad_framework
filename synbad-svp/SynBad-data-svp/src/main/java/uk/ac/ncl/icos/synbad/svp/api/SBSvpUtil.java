package uk.ac.ncl.icos.synbad.svp.api;

import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.flow.object.api.SBFlowObject;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.SBSubGraph;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

import java.net.URI;
import java.util.*;
import java.util.Collection;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.flow.definition.PortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;

public class SBSvpUtil {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SBSvpUtil.class);

    /**
     * Returns the instance of the provided port definition owned by the provided
     * component.
     * @param component A functional component, module or interaction.
     * @param definition The port definition of the port instance.
     * @return 
     */
    static public SBPortInstance getPortInstance(SBValued component, SBPort definition) {
        return getPortInstances(component).stream()
            .filter(sbPortInstance -> sbPortInstance.getDefinition().equals(definition))
            .findFirst().orElse(null);
    }

    /**
     * Checks if a local functional component is mapped to a remote Component.
     * @param fc
     * @return 
     */
    static public boolean isMappedToDna(FunctionalComponent fc) {
        
        SBWorkspace ws = fc.getWorkspace();
        URI[] contexts = fc.getContexts();
        return  ws.incomingEdges(fc, SynBadTerms.SbolMapsTo.hasLocalInstance, MapsTo.class, contexts).stream()
                .map(e -> ws.getObject(e.getFrom(), MapsTo.class, contexts).getRemoteInstance())
                .anyMatch(i -> i.is(Component.TYPE));
    }

    /**
     * Returns the port instances owned by a FunctionalComponent, Module or
     * Interaction.
     * @param component
     * @return 
     */
    static public Collection<SBPortInstance> getPortInstances(SBValued component) {
        if(component.is(FunctionalComponent.TYPE))
            return component.as(SvpInstance.class).map(i -> i.getPorts()).orElse(Collections.EMPTY_LIST);
        else if(component.is(Module.TYPE))
            return component.as(SvmInstance.class).map(i -> i.getPorts()).orElse(Collections.EMPTY_LIST);
        else if (component.is(Interaction.TYPE))
            return component.as(SviInstance.class).map(i -> i.getPorts()).orElse(Collections.EMPTY_LIST);
        else return Collections.EMPTY_LIST;
    }
    
    public static Collection<SBPortInstance> getPorts(SBValued component) {  
        
        URI[] contexts = component.getContexts();

        SBWorkspace ws = component.getWorkspace();

        return ws.outgoingEdges(component,
                SynBadTerms.SynBadPortInstance.hasPortInstance, 
                SBPortInstance.class, 
                contexts).stream()
                    .filter(e -> e != null)
                    .map(e -> ws.getObject(e.getTo(), SBPortInstance.class, contexts))
                    .filter(n -> n != null).collect(Collectors.toSet());
    }

    
    public static Collection<SBPortInstance> getPorts(SBValued component, SBPortDirection direction) {
        return getPorts(component).stream().filter(p -> p.getPortDirection() ==  direction)
                .collect(Collectors.toSet());
    }

    public static Collection<SBPortInstance> getPorts(SBValued component, PortType type) {
        return getPorts(component).stream().filter(p -> p.getPortType() ==  type)
                .collect(Collectors.toSet());
    }

    public static Collection<SBPortInstance> getPorts(SBValued component, SBPortDirection direction, PortType type) {
        return getPorts(component).stream()
                .filter(p -> p.getPortDirection() ==  direction)
                .filter(p -> p.getPortType() ==  type)
                .collect(Collectors.toSet());
    }

    /**
     * Returns any instances of a provided SVM, SVP or SVI.
     * @param object
     * @param contexts
     * @return Any Modules, FunctionalComponents, or Interactions that instance
     * the provided SVM, SVP or SVI respectively.
     */
    static public <T extends SBValued> Set<T> getInstances(SBFlowObject object, Class<T> clazz, URI[] contexts) {

        SBWorkspace ws = object.getWorkspace();

        Set<SBSubGraph<SBValued, SBEdge>> definitionInstanceParentSubgraphs = null;

        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(ws, contexts)) {
            definitionInstanceParentSubgraphs = SvpInteraction.class.isAssignableFrom(object.getClass()) ? 
                graph.getTraversal()
                    .v(object)
                    .e(SBDirection.IN, SynBadTerms.SynBadEntity.extensionOf)
                    .v(SBDirection.IN)
                    .filter(v -> v.is(SviInstance.TYPE)).as("I").getResult().getMatches():
                graph.getTraversal()
                    .v(object)
                    .e(SBDirection.IN, SynBadTerms.Sbol.definedBy)
                    .v(SBDirection.IN)
                    .filter(v -> v.is(SvpInstance.TYPE) || v.is(SvmInstance.TYPE)).as("I").getResult().getMatches();
        }

        return definitionInstanceParentSubgraphs.stream()
                .flatMap(sg -> sg.getLabelledVertices("I").stream())
                .map(m -> m.as(clazz))
                .filter(m -> m.isPresent())
                .map(m -> m.get())
                .collect(Collectors.toSet());
    }
    
    /**
     * Checks that the upstream object exists before the downstream object and
     * within the same transcriptional unit (there exists no terminator between
     * them).
     * @param parent The wrapped module definition that contains both upstream and downstream objects.
     * @param upstreamObject The wrapped module or functionalComponent that should be upstream.
     * @param downstreamObject The wrapped module or functionalComponent that should be downstream.
     * @return 
     */
    public static boolean containsAndPrecedes(SvpModule parent, SBSvpSequenceObject upstreamObject, SBSvpSequenceObject downstreamObject) {

        List<SBSvpSequenceObject<? extends SvpEntity>> orderedInstances = parent.getOrderedInstances();
        
        int downstreamIndex = indexOfOrOfParent(orderedInstances, downstreamObject);
        int upstreamIndex = indexOfOrOfParent(orderedInstances, upstreamObject);

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Does {} precede {}?", upstreamObject.getDisplayId(), downstreamObject.getDisplayId());

        if (downstreamIndex == -1 || upstreamIndex == -1) {
            return false;
        }

        boolean b = upstreamIndex < downstreamIndex;
            
        if(b) {
            for(int i = upstreamIndex + 1; i < downstreamIndex; i++) {
                if(isOrContains(orderedInstances.get(i), ComponentRole.Terminator)) {
                    return false;
                }
            }
        }
        
        return b;
    }
    
    /**
     * Checks that the provided wrapped object is or contains a functional 
     * component with the provided ComponentRole. Modules are searched 
     * recursively.
     * @param regulatedSvp
     * @param type
     * @return 
     */
    public static boolean isOrContains(SBSvpSequenceObject regulatedSvp, ComponentRole type) {
        if(regulatedSvp.is(FunctionalComponent.TYPE)) {
            Optional<SvpInstance> fc = regulatedSvp.as(SvpInstance.class);
            boolean b = fc.flatMap(SvpInstance::getDefinition)
                    .map(d -> d.getSbolRoles().contains(type)).orElse(Boolean.FALSE);
            if(b)
//                LOGGER.debug("Is or contains {}! {}", type, regulatedSvp.getDisplayId());
                return b;
        } else if (regulatedSvp.is(Module.TYPE)) {
            Optional<SvmInstance> fc = regulatedSvp.as(SvmInstance.class);

            boolean b = fc.flatMap(SvmInstance::getDefinition)
                    .map(SvpModule::getSvpInstances)
                    .map(d -> d.stream().anyMatch(i ->
                            i.getDefinition().map(svp ->
                                svp.getSbolRoles().contains(type)).orElse(Boolean.FALSE)))
                    .orElse(Boolean.FALSE);

            if(b) {
//                LOGGER.debug("Is or contains {}! {}", type, regulatedSvp.getDisplayId());
                 return true;
            }
               
            if(fc.flatMap(i -> i.getDefinition())
                .map(i -> i.getSvmInstances())
                .map(m -> m.stream().anyMatch(i -> i.as(SvmInstance.class)
                    .map(svm -> isOrContains(svm, type)).orElse(Boolean.FALSE)))
                .orElse(Boolean.FALSE)) {
//                LOGGER.debug("Is or contains {}! {}", type, regulatedSvp.getDisplayId());
                 return true;
            }
        }
        return false;
    }

    private static int indexOfOrOfParent(List<SBSvpSequenceObject<? extends SvpEntity>> instances, SBSvpSequenceObject<? extends SvpEntity> obj) {
        int index = instances.indexOf(obj);
        if (index != -1) {
            return index;
        }

        int parentIndex = 0;

        for (SBSvpSequenceObject sequenceObject : instances) {
            if (sequenceObject.is(Module.TYPE)) {
                boolean b = false;
                if (obj.is(Module.TYPE)) {
                    Set<Module> modules = sequenceObject.as(Module.class).flatMap(m -> m.getDefinition()).map(m -> m.getModules()).orElse(Collections.EMPTY_SET);
                    b = modules.stream().map(m -> m.getDefinition())
                            .anyMatch(d -> d.map(x -> x.getIdentity().equals(
                                    obj.getDefinition().map(def -> def.getIdentity()).orElse(null)))
                                    .orElse(Boolean.FALSE));
                } else if (obj.is(FunctionalComponent.TYPE)) {
                    Set<FunctionalComponent> fcs = sequenceObject.as(Module.class).get().getDefinition().get().getFunctionalComponents();
                    b = fcs.stream().map(m -> m.getDefinition())
                            .anyMatch(d -> d.map(x -> x.getIdentity().equals(
                                    obj.getDefinition().map(def -> def.getIdentity()).orElse(null)))
                                    .orElse(Boolean.FALSE));
                }
                if (b)
                    index = parentIndex;
            }

            parentIndex++;
        }
        
        return index;
    }
}
