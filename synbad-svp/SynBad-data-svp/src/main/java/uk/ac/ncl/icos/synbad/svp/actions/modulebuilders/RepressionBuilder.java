/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import java.net.URI;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class RepressionBuilder {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(RepressionBuilder.class);

    private final SBWorkspace workspaceId;
    private final URI[] contexts;
    private final SBIdentity identity;
    private SBIdentity modulator;
    private SynBadPortType modulatorType;
    private SynBadPortState modulatorState;
    private SBIdentity modulatorOwner;
    private SBIdentity commonParent;
    private Integer modulatedIndex;
    private SBIdentity portOwner;
    private SBIdentity modulated;
    private Double km = null;
    private Double n = null;
    
    protected final Random RANDOM = new Random();
    private final CelloActionBuilder b;

    public RepressionBuilder(CelloActionBuilder b, SBIdentity identity, URI[] contexts) {
        this.b = b;
        this.identity = identity;
        this.workspaceId = b.getWorkspace();
        this.contexts = contexts;
    }

    public RepressionBuilder(SBIdentity identity, SBWorkspace workspaceId, URI[] contexts) {
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.b = null;
    }


    public RepressionBuilder setKm(double km) {
        this.km = km;
        return this;
    }
    
     public RepressionBuilder setN(double n) {
        this.n = n;
        return this;
    }

    public RepressionBuilder setModulator(SBIdentity modulator) {
        this.modulator = modulator;
        return this;
    }
    
    public RepressionBuilder setModulated(SBIdentity modulated) {
        this.modulated = modulated;
        return this;
    }
    
    public RepressionBuilder setModulatorOwner(SBIdentity modulatorOwner) {
        this.modulatorOwner = modulatorOwner;
        return this;
    }
    
    public RepressionBuilder setModulatorType(SynBadPortType type) {
        this.modulatorType = type;
        return this;
    }
    
     public RepressionBuilder setModulatorState(SynBadPortState state) {
        this.modulatorState = state;
        return this;
    }
     
    public RepressionBuilder setCommonParent(SBIdentity commonParent) {
        this.commonParent = commonParent;
        return this;
    }
    
    public RepressionBuilder setModulatedIndex(int index) {
        this.modulatedIndex = index;
        return this;
    }
    
    public RepressionBuilder setPortOwner(SBIdentity portOwner) {
        this.portOwner = portOwner;
        return this;
    }
    
    public double getRandomParameter() {
        return RANDOM.nextDouble();
    }
   
    public SBAction build() {
        
        if(modulatorOwner == null) {
            LOGGER.error("Modulator owner cannot be null");
            return null;
        }
        
        if(modulator == null) {
            LOGGER.error("Modulator cannot be null");
            return null;
        }
        
        if(modulated == null) {
            LOGGER.error("Modulated cannot be null");
            return null;
        }
         
        if(commonParent == null) {
            LOGGER.error("Common parent cannot be null");
            return null;
        }
        
        if(modulatedIndex == null) {
            LOGGER.error("Modulated Index cannot be null");
            return null;
        }
     
        if(portOwner == null)
            portOwner = modulator;
        
        if(km == null)
            km = getRandomParameter();
        
        if(n == null)
            n = getRandomParameter();
        
        if(modulatorState == null)
            modulatorState = SynBadPortState.Default;
        
        if(modulatorType == null)
            modulatorType = SynBadPortType.Protein;
        
        SBAction a =  TuActionsFactory.createRepressUnit(
                identity, modulatorOwner.getIdentity(), portOwner.getIdentity(), 
                modulator.getIdentity(), modulatorType, modulatorState, 
                commonParent.getIdentity(), modulated.getIdentity(), 
                modulatedIndex,
                km, n,workspaceId, contexts);
        LOGGER.trace("Returning repression: {}", identity.getDisplayID());
 
        return a;
    }
    
    public SBCelloBuilder builder() {
        SBAction a = build();
        if(a == null) {
            LOGGER.error("Action is null");
            return null;
        }

        if( b == null) {
            LOGGER.trace("Builder is null, returning new builder");
            return new CelloActionBuilder(workspaceId, contexts).addAction(a);
        }

        if (!b.isBuilt()) {
            LOGGER.trace("Adding action to builder");
            b.addAction(a);
        }

        LOGGER.trace("Returning builder");
        return b;
    }
 
}
