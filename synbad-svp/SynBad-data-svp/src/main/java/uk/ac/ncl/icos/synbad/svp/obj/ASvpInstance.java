/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.obj;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.flow.object.impl.ASBFlowInstance;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

/**
 *
 * @author owen
 */
public abstract class ASvpInstance<D extends SvpEntity, I extends SBIdentified> extends ASBFlowInstance<D> implements SbolInstance<D>, SBSvpSequenceObject<D> {
    
    
    public ASvpInstance(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    @Override
    public boolean isMappedToComponent() {
        return getMappedToComponent() != null;
    }

    
    @Override
    public Component getMappedToComponent() {
        SbolInstance remoteComponent;
        try (SBWorkspaceGraph g = new SBWorkspaceGraph(ws, getContexts())) {
            SvpModule md = getParent().as(SvpModule.class).get();
            remoteComponent = md.getRemotelFromLocal(this, Component.class, g);
        }
        if(remoteComponent != null && remoteComponent.is(Component.TYPE))
            return (Component)remoteComponent;
        
        return null;
    }

}
