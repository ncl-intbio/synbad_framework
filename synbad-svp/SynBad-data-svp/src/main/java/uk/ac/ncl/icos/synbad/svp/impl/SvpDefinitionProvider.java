/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.impl;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefProvider;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.svp.actions.modulebuilders.SvpModuleType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider( service = SBDataDefProvider.class)
public class SvpDefinitionProvider implements SBDataDefProvider {

    @Override
    public void addDefinitions(SBDataDefManager manager) {

        // Add defined terms

        for(SvpInteractionType type : SvpInteractionType.values()) {
            manager.addDefinition(type);
        }
        
        for(SvpModuleType type : SvpModuleType.values()) {
            manager.addDefinition(type);
        }
        
        for(SynBadPortState state : SynBadPortState.values()) {
            manager.addDefinition(state);
        }

        for(SynBadPortType type : SynBadPortType.values()) {
            manager.addDefinition(type);
        }
    
        manager.addSynonym("PoPS", SynBadPortType.PoPS);
        manager.addSynonym("RiPS", SynBadPortType.RiPS);
        manager.addSynonym("mRNA", SynBadPortType.mRNA);
        manager.addSynonym("Protein", SynBadPortType.Protein);
        
        // Should be "contains" species, but don't have that functionality yet
        
        manager.addSynonym("Species", SynBadPortType.Protein);
        manager.addSynonym("SpeciesPhosphorylated", SynBadPortType.Protein);
        manager.addSynonym("EnvironmentConstant", SynBadPortType.SmallMolecule);
        manager.addSynonym("Default", SynBadPortState.Default);
        manager.addSynonym("Phosphorylated", SynBadPortState.Phosphorylated);

        
     }
}

