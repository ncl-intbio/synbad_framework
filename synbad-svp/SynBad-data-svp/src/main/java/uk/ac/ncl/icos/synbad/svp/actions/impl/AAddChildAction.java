/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import java.net.URI;
import java.util.List;
import java.util.Set;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceConstraint;
import uk.ac.ncl.icos.synbad.workspace.actions.ADeferredAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public abstract class AAddChildAction extends ADeferredAction {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AAddChildAction.class);
    
    protected final URI parentIdentity;
    protected final URI childIdentity;
    protected int index;

    public AAddChildAction(SBGraphEventType type, URI parentSvmIdentity, URI childSvpIdentity, int index, SBWorkspace workspace, URI[] contexts) {
        super(type, workspace, contexts);
        this.parentIdentity = parentSvmIdentity;
        this.childIdentity = childSvpIdentity;
        this.index = index;
    }

    protected SBIdentity getUniqueIdentity(SBWorkspace ws, SBIdentity identity) {
        SBIdentity uniqueIdentity = identity;
        int increment = 1;
        while(uniqueIdentity != null && ws.containsIdentity(uniqueIdentity.getIdentity(), ws.getContextIds())) {
            uniqueIdentity = SBIdentity.getIdentity(identity.getUriPrefix(), identity.getDisplayID() + "_" + increment++, identity.getVersion());
        }
        return uniqueIdentity;
    }

    protected abstract URI getPrecededIdentity();
    
    protected SbolActionBuilder updateConstraintsFromInserted(SbolActionBuilder b, ComponentDefinition constraintOwner, SBIdentity addedComponentId) {
        
        
        URI precededComponentIdentity = getPrecededIdentity();
        
        try {

            List<Component> orderedComponents = constraintOwner.getOrderedComponents();
            
            // Get the component, if any, that the child SVP component precedes
            
            Component precededComponent = precededComponentIdentity != null ?  
                    orderedComponents.stream()
                            .filter(c -> c.getIdentity().equals(precededComponentIdentity)).findFirst().orElse(null) :
                    null;
            
            Component precedingComponent = null;
            
            // Get the component, if any, that precedes the child SVP
     
            if(precededComponent != null && orderedComponents.contains(precededComponent)) {
               
                int index = orderedComponents.indexOf(precededComponent);
                
                if(index > 0) {
                    precedingComponent = orderedComponents.get(index - 1);
                }
            } else if (!orderedComponents.isEmpty()) {
                precedingComponent = orderedComponents.get(orderedComponents.size() - 1);
        }
        // The Component representing the DNA of the child SVP is ordered
        // using sequence constraints.

        Set<SequenceConstraint> constraints = constraintOwner.getSequenceConstraints();

        // If there is both preceding and preceded, we must remove existing
        // constraint between them

        if(precedingComponent != null && precededComponent != null){
            
            LOGGER.debug("Removing constraint between {} and {}", precedingComponent.getDisplayId(), precededComponent.getDisplayId());
            
            SequenceConstraint c1 = constraints.stream()
                .filter(constraint -> {
                    return constraint.getObject().equals(precededComponent); })
                .findFirst().orElse(null);

            if(c1 != null) {
                LOGGER.debug("Removing constraint:\t\t{}", c1.getDisplayId());

                b = b.removeObject(c1.getIdentity(), SBIdentityHelper.getURI(SequenceConstraint.TYPE), constraintOwner.getIdentity(), SBIdentityHelper.getURI(ComponentDefinition.TYPE));
            }
        }

        // If there is a preceding component, add constraint from 
        // preceding -> new component

        SBIdentity constraintOwnerId = SBIdentity.getIdentity(constraintOwner.getIdentity());

        if(precedingComponent != null){

                SBIdentity precedingId = SBIdentity.getIdentity(precedingComponent.getIdentity());
                SBIdentity sequenceConstraintId = SequenceConstraint.getSequenceConstraintIdentity(constraintOwnerId, precedingId, addedComponentId, ConstraintRestriction.PRECEDES);
                LOGGER.debug("Adding constraint:\t{}", sequenceConstraintId.getDisplayID());
                b = b.createSequenceConstraint(sequenceConstraintId.getIdentity(), constraintOwner.getIdentity(), precedingComponent.getIdentity(), ConstraintRestriction.PRECEDES, addedComponentId.getIdentity());
        }

        // If there is a preceded component, add constraint from 
        // new component -> preceded

        if(precededComponent != null) {
            SBIdentity precededId = SBIdentity.getIdentity(precededComponent.getIdentity());
            SBIdentity sequenceConstraintId = SequenceConstraint.getSequenceConstraintIdentity(constraintOwnerId, addedComponentId, precededId, ConstraintRestriction.PRECEDES);
            LOGGER.debug("Adding constraint:\t{}", sequenceConstraintId.getDisplayID());
            b = b.createSequenceConstraint(sequenceConstraintId.getIdentity(), constraintOwner.getIdentity(), addedComponentId.getIdentity(), ConstraintRestriction.PRECEDES, precededComponent.getIdentity());
        }

         } catch (SBIdentityException ex) {
                Exceptions.printStackTrace(ex);
        }
        return b;
    }
}

