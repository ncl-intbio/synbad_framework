 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.impl;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.svp.actions.SvpActions;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpParticipant;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class AddSviAction extends AAddChildAction {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSviAction.class);
    protected final Map<URI, URI> foundObjects = new HashMap<>();
    protected final Map<URI, SBAction> expectedObjects = new HashMap<>();

    public AddSviAction(SBGraphEventType type, URI parentSvmIdentity, URI childSvpIdentity, SBWorkspace workspace, URI[] contexts) {
        super(type, parentSvmIdentity, childSvpIdentity, 0, workspace, contexts);
    }

    @Override
    protected URI getPrecededIdentity() {
        throw new UnsupportedOperationException("Not supported."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected SBAction getAction(SBWorkspace ws) {

            // SVM and SVP objects
            
            ModuleDefinition parentSvm = ws.getObject(parentIdentity, ModuleDefinition.class, contexts);
            SvpInteraction childSvi = ws.getObject(childIdentity, SvpInteraction.class, contexts);
            
            LOGGER.debug("--- {} ---", childSvi.getDisplayId());
            
            SbolActionBuilder b = new SbolActionBuilder(ws, contexts);
            b.createEdge(parentSvm.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.hasChild), childSvi.getIdentity());
            
            
            parentSvm.getFunctionalComponents().stream().map(fc -> fc.getIdentity()).forEach(uri -> {
                foundObjects.put(uri, SBIdentityHelper.getURI(FunctionalComponent.TYPE));
            });
            
            parentSvm.getInteractions().stream().flatMap(i -> i.getParticipants().stream()).map(p -> p.getIdentity()).forEach(uri -> {
                foundObjects.put(uri, SBIdentityHelper.getURI(Participation.TYPE));
            });
            
            
            try {
                // Create interaction

                SBIdentity interactionIdentity = createInteractionAndExtends(parentSvm, childSvi);

                for(SvpParticipant svpInteractionParticipant : childSvi.getParticipants()) {

                    ComponentDefinition participantDefinition = (ComponentDefinition)svpInteractionParticipant.getSbolParticipant();
                    
                    if(participantDefinition == null) {
                        LOGGER.debug("Couldn't find SVP for {}", svpInteractionParticipant.getDisplayId());
                    } else {
                        
                        // Create functional component
                        
                        SBIdentity fcId = createFunctionalComponent(childSvi, svpInteractionParticipant, parentSvm);
                        
                        // Create participation
                        
                        SBIdentity parId = createParticipation(interactionIdentity, fcId, svpInteractionParticipant);
                    }
                }
            } catch (SBIdentityException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
            
            

            //b.addAction(new UpdateTransWires(parentIdentity, workspace, contexts));
            
            createActions(b);
            
            foundObjects.clear();
            expectedObjects.clear();
           
            return b.build("AddSvi ["+ childSvi.getDisplayId() + "]");

    }
    
    public void createActions(SbolActionBuilder b) {
        
        /*
for(URI found : foundObjects.keySet()) {
            if(!expectedObjects.keySet().contains(found)) {
                LOGGER.debug("Removing: {}", found);
                b = b.removeObjectAndDependants(found, foundObjects.get(found));
            }
        }*/
        
        for(URI id : expectedObjects.keySet()) {
            //LOGGER.debug("Expected: {}", id.toASCIIString());
            if(!foundObjects.keySet().contains(id)) {
                SBAction a = expectedObjects.get(id);
                if(a != null) {
                    b = b.addAction(a);
                    //LOGGER.debug("Adding: {}", id);
                }
            }
        }
    }
    
    /*
    
     public SBIdentity createMapsTo(SBIdentity fc_tuInstance, SBIdentity fcId, SBIdentity c_svpDna) {
        try {
            SBIdentity mtId = ws.getIdentityFactory().getIdentity(fc_tuInstance.getUriPrefix(),
                    fc_tuInstance.getDisplayID(),
                    "mt_" + fcId.getDisplayID() + "_to_" + c_svpDna.getDisplayID(),
                    fc_tuInstance.getVersion());
            
            // create mapsTo to map fc_tuInstance to dna
            
            if(!expectedObjects.containsKey(mtId.getIdentity())) {
                
                LOGGER.debug("Adding mapsTo:\t\t{}", mtId.getDisplayID());
                
                expectedObjects.put(mtId.getIdentity(), SBSbolActions.createMapsTo(
                        mtId.getIdentity(),
                        fc_tuInstance.getIdentity(),
                        MapsToRefinement.verifyIdentical,
                        c_svpDna.getIdentity(),
                        fcId.getIdentity(),
                        g.getWorkspace().getIdentity(),
                        g.getContexts()));
            }
            
            return mtId;
        } catch (SBIdentityException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
    }
    */
    
    public SBIdentity createInteractionAndExtends(ModuleDefinition md_svpModuleDefinition, SvpInteraction svi) {

        SBWorkspace ws = md_svpModuleDefinition.getWorkspace();
        
        SBIdentity interactionIdentity;

            interactionIdentity = ws.getIdentityFactory().getIdentity(
                    svi.getPrefix(),
                    md_svpModuleDefinition.getDisplayId(),
                    "i_" + svi.getDisplayId(),
                    svi.getVersion());
    
        
        if(!expectedObjects.containsKey(interactionIdentity.getIdentity())) {
            
            LOGGER.debug("Adding interaction:\t{}", interactionIdentity.getDisplayID());
                
            
            DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(ws, contexts)
                    .addAction(Interaction.createInteraction(
                            interactionIdentity.getIdentity(),
                            Collections.singleton(svi.getSbolInteractionType()),
                            md_svpModuleDefinition,
                            contexts))
                    .addAction(
                        SvpActions.AddExtends(
                            svi.getIdentity(),
                            interactionIdentity.getIdentity(),
                            md_svpModuleDefinition.getWorkspace(),
                            contexts))
                    .addAction(new CreateEdge(interactionIdentity.getIdentity(), SynBadTerms.SynBadSystem.dependantOn, svi.getIdentity(), 
                                ws.getIdentity(), ws.getSystemContextIds(contexts)));
            /*
            Set<SBPort> ports = new HashSet<>();
                if(svi != null)
                    ports.addAll(svi.getPorts());

                    for(SBPort p : ports) {
                        try {
                            SBIdentity portIdentity = getUniqueIdentity(SBIdentity.getIdentity(interactionIdentity.getUriPrefix(), interactionIdentity.getDisplayID(), p.getDisplayId() + "_instance", interactionIdentity.getVersion()));
                            LOGGER.debug("Adding port:\t\t{}", portIdentity.getDisplayID());
                            b.addAction(FlowActions.createPortInstance(portIdentity.getIdentity(), interactionIdentity.getIdentity(), p.getIdentity(), workspace, contexts));
                        } catch (SBIdentityException ex) {
                            LOGGER.error(ex.getLocalizedMessage());
                        }
                    };*/
                    
                    expectedObjects.put(interactionIdentity.getIdentity(), b.build());
            
        }
        
        return interactionIdentity;
    }
    
    
    
    public SBIdentity createParticipation(SBIdentity interactionIdentity, SBIdentity fcId, SvpParticipant svpInteractionParticipant) throws SBIdentityException {
        
        SBIdentity participationId = Participation.getParticipationIdentity(interactionIdentity, fcId);

        if(!expectedObjects.containsKey(participationId.getIdentity())) {
            
            expectedObjects.put(participationId.getIdentity(),         
                new SbolActionBuilder(svpInteractionParticipant.getWorkspace(),  contexts)
                    .createParticipation(
                        participationId.getIdentity(),
                        interactionIdentity.getIdentity(),
                        fcId.getIdentity(),
                        svpInteractionParticipant.getInteractionRole())
                    .createEdge(participationId.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SynBadEntity.extensionOf), svpInteractionParticipant.getIdentity()).build());
            
            LOGGER.debug("Adding participation:\t{}", participationId.getDisplayID());
        }
            
        
        return participationId;
    }
    
    public SBIdentity createFunctionalComponent(SvpInteraction svi, SvpParticipant participantDefinition, ModuleDefinition md_svpModuleDefinition) throws SBIdentityException {
 
        SBWorkspace ws = svi.getWorkspace();

        SBIdentity fcId = FunctionalComponent.getFunctionalComponentIdentity(
                ws.getIdentityFactory().getIdentity(md_svpModuleDefinition.getIdentity()), 
                ws.getIdentityFactory().getIdentity(participantDefinition.getSbolParticipant().getIdentity()));
        
        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(ws,  contexts)
                .addAction(FunctionalComponent.createFuncComponent(
                    fcId.getIdentity(),
                    participantDefinition.getSbolParticipant().getIdentity(),
                    md_svpModuleDefinition.getIdentity(),
                    SbolAccess.PUBLIC,
                    SbolDirection.OUT,
                    ws,
                    contexts))
                
                .addAction(new CreateEdge(
                    fcId.getIdentity(),
                    SynBadTerms.SynBadSystem.dependantOn,
                    svi.getIdentity(), ws.getIdentity(),
                    ws.getSystemContextIds(contexts)));
        
        if(!expectedObjects.containsKey(fcId.getIdentity())) {
            
            expectedObjects.put(fcId.getIdentity(), b.build());
            
            LOGGER.debug("Adding funcComp:\t\t{}", fcId.getDisplayID());
            
            
            
            /*
            Svp svp = g.getTraversal()
                    .v(participantDefinition.getSbolParticipant())
                    .e(SBDirection.OUT, SynBadTerms.SynBadEntity.extensionOf)
                    .v(SBDirection.OUT).getResult().streamVertexes()
                    .map(obj -> (Svp)obj).findFirst().orElse(null);
            Set<SBPort> ports = new HashSet<>();
            if(svp != null)
                ports.addAll(svp.getPorts());

            
            // TODO!!!!!!!!!!!! Need to add ports to the DNA functional compoennt, not the protein fc
            
            for(SBPort p : ports) {
                try {
                    SBIdentity portIdentity = getUniqueIdentity(SBIdentity.getIdentity(fcId.getUriPrefix(), fcId.getDisplayID(), p.getDisplayId() + "_instance", fcId.getVersion()));
                    LOGGER.debug("Adding port:\t\t{}", portIdentity.getDisplayID());
                    b.addAction(FlowActions.createPortInstance(portIdentity.getIdentity(), fcId.getIdentity(),p.getIdentity(), workspace, contexts));
                } catch (SBIdentityException ex) {
                    LOGGER.error(ex.getLocalizedMessage());
                }
            };*/
        }
        
        return fcId;

    }
    
    
    
}
