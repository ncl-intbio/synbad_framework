/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.modulebuilders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBTuBuilder;

import java.net.URI;
import java.util.Random;

/**
 *
 * @author owen
 */
public class TranscriptionUnitBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(TranscriptionUnitBuilder.class);
    protected final CelloActionBuilder b;
    protected final SBWorkspace workspaceId;
    protected final URI[] contexts;
    protected final Random RANDOM = new Random();

    protected Double transcriptionRate = null;
    protected Double translationRate1 = null;
    protected Double degradationRate1 = null;

    protected final SBIdentity identity;
    protected SBIdentity cdsId = null;
    protected SBIdentity promoterId = null;


    public TranscriptionUnitBuilder(CelloActionBuilder b, SBIdentity identity,  SBWorkspace workspaceId, URI[] contexts) {
        this.b = b;
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
    }

    public TranscriptionUnitBuilder(SBIdentity identity,  SBWorkspace workspaceId, URI[] contexts) {
        this.identity = identity;
        this.workspaceId = workspaceId;
        this.contexts = contexts;
        this.b = null;
    }

    public TranscriptionUnitBuilder setTranscriptionRate(double transcriptionRate) {
        this.transcriptionRate = transcriptionRate;
        return this;
    }
    
    public TranscriptionUnitBuilder setTranslationRate(double translationRate1) {
        this.translationRate1 = translationRate1;
        return this;
    }

    public TranscriptionUnitBuilder setDegradationRate(double degradationRate1) {
        this.degradationRate1 = degradationRate1;
        return this;
    }

    public TranscriptionUnitBuilder setCdsIdentity(SBIdentity cdsIdentity) {
        this.cdsId = cdsIdentity;
        return this;
    }
    
    public TranscriptionUnitBuilder setPromoterIdentity(SBIdentity promoterIdentity) {
        this.promoterId = promoterIdentity;
        return this;
    }
    
    public double getRandomParameter() {
        return RANDOM.nextDouble();
    }
  
    public SBAction build() {
        
        if(degradationRate1 == null)
            degradationRate1 = getRandomParameter();

        if(transcriptionRate == null)
            transcriptionRate = getRandomParameter();
        
        if(translationRate1 == null)
           translationRate1 = getRandomParameter();
    
        SBAction a =  TuActionsFactory.createTranscriptionUnit(
                identity, cdsId, promoterId,
                transcriptionRate, translationRate1, 
                degradationRate1, workspaceId, contexts);

            LOGGER.trace("Returning TranscriptionUnit: {}", identity.getDisplayID());
 
        return a;
    }

    public SBTuBuilder builder() {
        SBAction a = build();
        if(a == null) {
            LOGGER.error("Action is null");
            return null;
        }

        if( b == null) {
            LOGGER.trace("Builder is null, returning new builder");
            return new CelloActionBuilder(workspaceId, contexts).addAction(a);
        }

        if (!b.isBuilt()) {
            LOGGER.trace("Adding action to builder");
            b.addAction(a);
        }

        LOGGER.trace("Returning builder");
        return b;
    }
 
}
