/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.api;

import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.SBSynBadContext;
import uk.ac.ncl.icos.synbad.model.SBModelManager;

/**
 *
 * @author owen
 */
public class SynBadSvp extends SBSynBadContext {
    
    private SBModelManager MODEL_FACTORY = null;

    public SynBadSvp(WorkspaceType type) {
        super(type);
    }
    
    public SBModelManager getModelFactory() {
        if(MODEL_FACTORY == null) {
            MODEL_FACTORY = Lookup.getDefault().lookup(SBModelManager.class);
        }
        
        return MODEL_FACTORY;
    }
}
