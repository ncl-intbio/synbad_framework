/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.actions.builders;

import java.net.URI;
import java.util.Set;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.actions.SviExternalActionFactory;

/**
 *
 * @author owen
 */
public interface SBSviBuilder extends SBActionBuilder {

    SBSviBuilder createActivateByPromoter(SBIdentity identity, ComponentType type, SynBadPortState state, URI modulator, URI promoterId, double km, double n);

    SBSviBuilder createActivateANDNByPromoter(SBIdentity identity, URI promoterId, ComponentType type1, SynBadPortState state1,  URI activator,
                                                         ComponentType type2, SynBadPortState state2,  URI repressor,
                                                         double kmActivator, double nActivator,
                                                         double kmRepressor, double nRepressor);
   
    SBSviBuilder createPhosphorylate(SBIdentity identity, URI phosphorylator, URI phosphorylated, double kf, double kd, double kDeP);
  
    SviExternalActionFactory.ComplexFormationActionBuilder createComplexFormation(SBIdentity complexId);

    SviExternalActionFactory.ComplexDisassociationActionBuilder createComplexDisassociation(SBIdentity complexId);

    SBSviBuilder createRepressByOperator(SBIdentity identity, ComponentType type, SynBadPortState state, URI modulator, URI operatorId, double km, double n);

    SBSviBuilder createSvi(SBIdentity identity, Set<InteractionType> roles);
    
    SBSviBuilder createProxyPort(SBIdentity identity, URI proxiedPort, URI owner, URI ownerType);
    
    SBSvpBuilder asPartBuilder();

}
