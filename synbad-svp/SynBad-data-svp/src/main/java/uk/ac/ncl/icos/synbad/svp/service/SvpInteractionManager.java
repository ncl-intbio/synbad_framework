/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.svp.service;

import java.net.URI;
import java.util.Collection;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;

/**
 *
 * @author owen
 */
public interface SvpInteractionManager  {

    boolean containsSvi(URI uri, URI[] contexts);
    
    boolean containsSvpParticipation(URI uri, URI[] contexts);
    
    boolean containsFunctionalComponent(URI uri, URI[] contexts);

    Collection<URI> getSvisWithConstraint(URI constraint, URI[] contexts);
    
    Collection<URI> getInteractionsWithConstraintInParent(URI constraint, URI parent, URI[] contexts );
    
    Collection<URI> getSviFromParticipantSvp(URI svp, URI[] contexts);
    
    Collection<URI> getSviFromParticipantInstance(URI fc, URI[] contexts);
    
    Collection<URI> getIncompleteSvisWithInstancesInParent(URI md, URI[] contexts);

    Collection<URI> getInteractionInstances(URI svi, URI[] contexts);
    
    Collection<URI> getParticipationInstancesFromDefinitionInMd(URI cd, URI md, URI[] contexts);

    /**Returns the identities of all FunctionalComponents that
     * are participants of the provided SVI.
     *
     * @param svi
     * @return
     */
    Collection<URI> getParticipantInstances(URI svi, URI[] contexts);

    /**
     * Returns the identities of all FunctionalComponents that
     * are participants of the provided SVI, that have the
     * provided ParticipationRole.
     * @param svi
     * @param role
     * @return
     */
    Collection<URI> getParticipantInstances(URI svi, ParticipationRole role, URI[] contexts);

    boolean containsAllParticipants(ModuleDefinition parent, Collection<URI> participants, URI[] contexts);
    
    boolean isInstanced(URI svi, URI moduleDefinition, Collection<URI> participants, URI[] contexts);
    
    boolean shouldBeInstanced(URI svi, URI moduleDefinition, URI[] contexts);

}
