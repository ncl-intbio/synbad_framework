/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SviVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.workspace.*;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WorkspaceTraversal;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;

/**
 *
 * @author owengilfellon
 */
public class DuplicationTests {
    
    private final static Random RANDOM =  new Random();
    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] FROM_CONTEXTS = new URI[] { URI.create("http://www.synbad.org/from1/1.0") };
    private final static URI[] TO_CONTEXTS = new URI[] { URI.create("http://www.synbad.org/to1/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(DuplicationTests.class);

    private SBIdentityFactory f;

    private final SBIdentity rootIdentity;
    private final SBIdentity generator;
    private final SBIdentity tu1Id;
    private final SBIdentity tu2Id;
    private final SBIdentity tu3Id;
    private final SBIdentity tu4Id;
    private final SBIdentity sink;

    private final SBIdentity proX;
    private final SBIdentity cdsB;
    
    private SBWorkspace ws;

    public DuplicationTests() throws SBIdentityException {
        rootIdentity = SBIdentity.getIdentity("http://www.synbad.org", "root", "1.0");
        generator = SBIdentity.getIdentity("http://www.synbad.org", "generator", "1.0");
        tu1Id = SBIdentity.getIdentity("http://www.synbad.org", "tu1", "1.0");
        tu2Id = SBIdentity.getIdentity("http://www.synbad.org", "tu2", "1.0");
        tu3Id = SBIdentity.getIdentity("http://www.synbad.org", "tu3", "1.0");
        tu4Id = SBIdentity.getIdentity("http://www.synbad.org", "tu4", "1.0");
        sink = SBIdentity.getIdentity("http://www.synbad.org", "sink", "1.0");
        proX = SBIdentity.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        cdsB = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
    }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );
        
        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        this.ws.createContext(FROM_CONTEXTS[0]);
        this.f =  ws.getIdentityFactory();
    }
    
    @After
    public void tearDown() {   
        ws.shutdown();
        this.ws = null;
    }



    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testDuplicateWithNoIncrement() throws SBIdentityException {


        Assert.assertEquals("FROM context should be empty",
                0, ws.getRdfService().getStatements(null, null, null, FROM_CONTEXTS).stream().count());

        Assert.assertEquals("TO context should be empty",
                0, ws.getRdfService().getStatements(null, null, null, TO_CONTEXTS).stream().count());

        Assert.assertEquals("FROM system context should be empty",
                0, ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(FROM_CONTEXTS)).stream().count());

        Assert.assertEquals("TO system context should be empty",
                0, ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(TO_CONTEXTS)).stream().count());


        SBSvpBuilder b = new SvpActionBuilderImpl(ws,  FROM_CONTEXTS);
        
        // Create identities
        
        SBIdentity svmIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svm", "1.0");
        SBIdentity promoterIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "promoter", "1.0");
        SBIdentity rbs1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_1", "1.0");
        SBIdentity rbs2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_2", "1.0");
        SBIdentity smlMolIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "smlMol", "1.0");
        SBIdentity terIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "terminator", "1.0");

        // Build and perform actions
        
        b = b.createSvm(svmIdentity)
                .createPromoter(promoterIdentity, 1.0)
                .createRBS(rbs1Identity, 1.0, 1.0)
                .createCDSWithPhosphorylatingSmallMolecule(cds1Identity, smlMolIdentity, 0.31, 0.1, 0.123, 0.213)
                .createRBS(rbs2Identity, 1.0, 1.0)
                .createCDS(cds2Identity, 1.0)
                .createTerminator(terIdentity)
                .addSvp(svmIdentity, promoterIdentity, null)
                .addSvp(svmIdentity, rbs1Identity, null)
                .addSvp(svmIdentity, cds1Identity, null)
                .addSvp(svmIdentity, rbs2Identity, null)
                .addSvp(svmIdentity, cds2Identity, null)
                .addSvp(svmIdentity, terIdentity, null);
 
        // logger.debug("--- Creating model ---");
        
        SBAction a = b.build();
        ws.perform(a);



        //Set<String> objects1 = ws.getIdentities(FROM_CONTEXTS).stream().map(o -> o.getPersistentId().toASCIIString()).collect(Collectors.toSet());
        Collection<SBIdentified> objects1 = ws.getObjects(SBIdentified.class, FROM_CONTEXTS);

        ws.perform(new SvpActionBuilderImpl(ws,  TO_CONTEXTS)
                .duplicateObject(svmIdentity.getIdentity(), FROM_CONTEXTS, false)
                .build());

        //Set<String> objects2 = ws.getIdentities(TO_CONTEXTS).stream().map(o -> o.getPersistentId().toASCIIString()).collect(Collectors.toSet());
        Collection<SBIdentified> objects2 = ws.getObjects(SBIdentified.class, TO_CONTEXTS);

        //objects1.stream().filter(id -> !objects2.contains(id)).forEach(System.out::println);

        try (SBWorkspaceGraph g = new SBWorkspaceGraph(ws, FROM_CONTEXTS)) {

            Set<String> traversed = g.getTraversal()
                .v(svmIdentity.getIdentity(), SBIdentified.class)
                .loop((fComponent) -> true, new WorkspaceTraversal<>(g)
                    .e(SBDirection.OUT)
                    .v(SBDirection.OUT, SBIdentified.class), true, false)
                .getResult().streamVertexes().map(SBValued::getIdentity)
                    .map(URI::toASCIIString).collect(Collectors.toSet());

            Set<String> allObjs = ws.getObjects(SBIdentified.class, FROM_CONTEXTS).stream()
                    .map(SBValued::getIdentity).map(URI::toASCIIString).collect(Collectors.toSet());

           // traversed.forEach(System.out::println);

          //  Sets.difference(traversed, allObjs).stream().forEach(System.out::println);
        }

        // now get ones that haven't been visited


        Assert.assertEquals("All objects should be in new context", objects1.size(), objects2.size());

        Assert.assertEquals("Should be same statements in duplicate context",
                ws.getRdfService().getStatements(null, null, null, FROM_CONTEXTS).stream().count(),
                ws.getRdfService().getStatements(null, null, null, TO_CONTEXTS).stream().count());
        Assert.assertEquals("Should be same statements in duplicate systems context",
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(FROM_CONTEXTS)).stream().count(),
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(TO_CONTEXTS)).stream().count());



    }

    @Test
    public void testDuplicateWithIncrement() throws SBIdentityException {

        SBSvpBuilder b = new SvpActionBuilderImpl(ws,  FROM_CONTEXTS);

        // Create identities

        SBIdentity svmIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svm", "1.0");
        SBIdentity promoterIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "promoter", "1.0");
        SBIdentity rbs1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_1", "1.0");
        SBIdentity rbs2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_2", "1.0");
        SBIdentity smlMolIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "smlMol", "1.0");
        SBIdentity terIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "terminator", "1.0");

        // Build and perform actions

        b = b.createSvm(svmIdentity)
                .createPromoter(promoterIdentity, 1.0)
                .createRBS(rbs1Identity, 1.0, 1.0)
                .createCDSWithPhosphorylatingSmallMolecule(cds1Identity, smlMolIdentity, 0.31, 0.1, 0.123, 0.213)
                .createRBS(rbs2Identity, 1.0, 1.0)
                .createCDS(cds2Identity, 1.0)
                .createTerminator(terIdentity)
                .addSvp(svmIdentity, promoterIdentity, null)
                .addSvp(svmIdentity, rbs1Identity, null)
                .addSvp(svmIdentity, cds1Identity, null)
                .addSvp(svmIdentity, rbs2Identity, null)
                .addSvp(svmIdentity, cds2Identity, null)
                .addSvp(svmIdentity, terIdentity, null);

        // logger.debug("--- Creating model ---");

        SBAction a = b.build();
        ws.perform(a);

        //Set<String> objects1 = ws.getIdentities(FROM_CONTEXTS).stream().map(o -> o.getPersistentId().toASCIIString()).collect(Collectors.toSet());
        Collection<SBIdentified> objects1 = ws.getObjects(SBIdentified.class, FROM_CONTEXTS);

        ws.perform(new SvpActionBuilderImpl(ws,  TO_CONTEXTS)
                .duplicateObject(svmIdentity.getIdentity(), FROM_CONTEXTS, true)
                .build());

        //Set<String> objects2 = ws.getIdentities(TO_CONTEXTS).stream().map(o -> o.getPersistentId().toASCIIString()).collect(Collectors.toSet());
        Collection<SBIdentified> objects2 = ws.getObjects(SBIdentified.class, TO_CONTEXTS);

        //objects1.stream().filter(id -> !objects2.contains(id)).forEach(System.out::println);

        Assert.assertEquals("All objects should be in new context", objects1.size(), objects2.size());

        Assert.assertEquals("Should be same statements in duplicate context",
                ws.getRdfService().getStatements(null, null, null, FROM_CONTEXTS).stream().count(),
                ws.getRdfService().getStatements(null, null, null, TO_CONTEXTS).stream().count());
        Assert.assertEquals("Should be same statements in duplicate systems context",
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(FROM_CONTEXTS)).stream().count(),
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(TO_CONTEXTS)).stream().count());


    }

    @Test
    public void testDuplicateWithModulesWithIncrement() throws SBIdentityException {

        SvpModel model = ExampleModuleFactory.createRepressilator(ws, FROM_CONTEXTS);

        Collection<SBIdentified> objects1 = ws.getObjects(SBIdentified.class, FROM_CONTEXTS);

        ws.perform(new SvpActionBuilderImpl(ws,  TO_CONTEXTS)
                .duplicateObject(model.getRootNode().getIdentity(), FROM_CONTEXTS, true)
                .build());

        //Set<String> objects2 = ws.getIdentities(TO_CONTEXTS).stream().map(o -> o.getPersistentId().toASCIIString()).collect(Collectors.toSet());
        Collection<SBIdentified> objects2 = ws.getObjects(SBIdentified.class, TO_CONTEXTS);

        //objects1.stream().filter(id -> !objects2.contains(id)).forEach(System.out::println);

        Set<String> obj1 = objects1.stream().map(s -> s.getPersistentId().toASCIIString()).collect(Collectors.toSet());
        Set<String> obj2 = objects2.stream().map(s -> s.getPersistentId().toASCIIString()).collect(Collectors.toSet());

        obj1.stream().filter(id -> !obj2.contains(id)).forEach(System.out::println);
        obj2.stream().filter(id -> !obj1.contains(id)).forEach(System.out::println);

        Assert.assertEquals("All objects should be in new context", objects1.size(), objects2.size());

        Assert.assertEquals("Should be same statements in duplicate context",
                ws.getRdfService().getStatements(null, null, null, FROM_CONTEXTS).stream().count(),
                ws.getRdfService().getStatements(null, null, null, TO_CONTEXTS).stream().count());
        Assert.assertEquals("Should be same statements in duplicate systems context",
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(FROM_CONTEXTS)).stream().count(),
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(TO_CONTEXTS)).stream().count());


    }
}
