/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPortInstance;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SviInstance;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SviVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.workspace.actions.DuplicateObject;

/**
 *
 * @author owengilfellon
 */
public class SvpModelAndViewTests {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpModelAndViewTests.class);
    private SvpModule module;
    private SBWorkspace workspace;
    private SBModelManager m;

    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/duplicate-model/1.0") };

    //private SBWorkspaceGraph g;

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class);

        LOGGER.debug("Setting up workspace...");
        module = ExampleFactory.setupWorkspace();
        workspace = module.getWorkspace();
        m = Lookup.getDefault().lookup(SBModelManager.class);
    }
    
    @After
    public void tearDown() {
        workspace.shutdown();
        workspace = null;
    }
    
    /**
     * Test of getRootNode method, of class SvpModel.
     */
    @Test
    public void testCreateSvpModelAndView() {
        
        LOGGER.debug("Getting model...");

        SvpModel model = m.createModel(SvpModel.class, module);
        
        LOGGER.debug("Got model {}:{} from {}", model.getViewRoot().getId(), model.getViewRoot().getDisplayId(), module.getDisplayId());
        LOGGER.debug("Getting view...");
        
        SvpPView view = model.createView();
        model.setCurrentView(view);

        SvpPView view2 = model.createView();
        assert(model.getCurrentView().equals(view));
        assert(!model.getCurrentView().equals(view2));
        
        model.getSvms().stream().forEach(svmVo -> {
        
            LOGGER.debug("============= {} ==============", svmVo.getDisplayId());

            SvpModule svm = svmVo.getSvpModule();
            
            svm.getSvmInstances().forEach(svp -> LOGGER.debug("SVM:  I: {} O: {} | {}",
                    svp.getPorts(SBPortDirection.IN).size(),
                    svp.getPorts(SBPortDirection.OUT).size(),
                    svp.getDisplayId()));
            
            svm.getSvpInstances().forEach(svp -> LOGGER.debug("SVP:  I: {} O: {} | {}",
                    svp.getPorts(SBPortDirection.IN).size(),
                    svp.getPorts(SBPortDirection.OUT).size(),
                    svp.getDisplayId()));

            svm.getSviInstances().stream()
                .filter(i -> !i.getDefinition().map(d -> d.isInternal()).orElse(Boolean.FALSE))
                .forEach(svp -> LOGGER.debug("eSVI: I: {} O: {} | {}",
                        svp.getPorts(SBPortDirection.IN).size(),
                        svp.getPorts(SBPortDirection.OUT).size(),
                        svp.getDisplayId()));
            
            svm.getSviInstances().stream()
                .filter(i -> !i.getDefinition().map(d -> d.isInternal()).orElse(Boolean.FALSE))
                .forEach(svp -> LOGGER.debug("iSVI: I: {} O: {} | {}",
                        svp.getPorts(SBPortDirection.IN).size(),
                        svp.getPorts(SBPortDirection.OUT).size(),
                        svp.getDisplayId()));
        });
        
        model.close();
    }

    @Test
    public void testModules() {

        String PREFIX = "http://www.synbad.org";
        String VERSION = "1.0";

        SBIdentity rootId = SBIdentity.getIdentity(PREFIX, "root", VERSION);
        SBIdentity system1Id = SBIdentity.getIdentity(PREFIX, "system1", VERSION);
        SBIdentity system2Id = SBIdentity.getIdentity(PREFIX, "system2", VERSION);

        SBIdentity generatorId = SBIdentity.getIdentity(PREFIX, "generator", VERSION);
        SBIdentity modulator1Id = SBIdentity.getIdentity(PREFIX, "modulator1", VERSION);
        SBIdentity modulator2Id = SBIdentity.getIdentity(PREFIX, "modulator2", VERSION);
        SBIdentity sink1Id = SBIdentity.getIdentity(PREFIX, "sink1", VERSION);
        SBIdentity sink2Id = SBIdentity.getIdentity(PREFIX, "sink2", VERSION);

        SBCelloBuilder b = new CelloActionBuilder(workspace, CONTEXTS)
            .createSvm(rootId)
            .createSvm(system1Id)
            .createSvm(system2Id)
            .createGenerator(generatorId)
                .builder()
            .createUnit(modulator1Id, TuActionsFactory.InteractionMode.NEGATIVE)
                .builder()
            .createUnit(modulator2Id, TuActionsFactory.InteractionMode.NEGATIVE)
                .builder()
            .createSink(sink1Id)
                .builder()
            .createSink(sink2Id)
                .builder();

        workspace.perform(b.build());

        SvpModule module = workspace.getObject(rootId.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, module);
        SvpPView view = model.getMainView();

        b = new CelloActionBuilder(workspace, CONTEXTS)
                .addSvm(rootId, system1Id, null);

        workspace.perform(b.build());

        view.expand(view.getRoot());

        Assert.assertEquals("Root should have one child module", 1, view.getChildren(view.getRoot(), SvmVo.class).size());

        SvmVo system1 = view.getChildren(view.getRoot(), SvmVo.class).iterator().next();

        Assert.assertEquals("System 1 should have no children", 0,  view.getChildren(system1).size());

        b = new CelloActionBuilder(workspace, CONTEXTS)
            .addSvm(system1Id, generatorId, null)
            .addSvm(system1Id, modulator1Id, null)
            .addSvm(system1Id, sink1Id, null);

        workspace.perform(b.build());

        Assert.assertTrue("System1 should be contracted", view.isContracted(system1));
        Assert.assertEquals("getChildren shouldn't return children if parent is contracted", 0, view.getChildren(system1).size());

        view.expand(system1);

        List<SvVo> systemChildren = view.getChildren(system1);

        Assert.assertEquals("System 1 should have 3 children", 3,  view.getChildren(system1).size());

        b = new CelloActionBuilder(workspace, CONTEXTS)
            .addSvm(system2Id, generatorId, null)
            .addSvm(system2Id, modulator2Id, null)
            .addSvm(system2Id, sink2Id, null)
            .addSvm(rootId, system2Id, null);

        workspace.perform(b.build());



    }
    
     /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testExpandAndContract()  {

        SvpModel model = m.createModel(SvpModel.class, module);
        SvpPView view = model.getMainView();
        SvmVo root = view.getRoot();
     
        Assert.assertEquals("Root should be Subtilin Receiver module", ExampleFactory.M_SUBTILIN_RECEIVER.getIdentity(), root.getSvpModule().getIdentity());
        
        List<SvVo> children = view.getChildren(root);
        List<SvVo> orderedChildren  = root.getOrderedChildren(SvVo.class);
        List<SvmVo> orderedSvms  = root.getOrderedChildren(SvmVo.class);
        List<SvVo> allChildren = view.getAllChildren(root);
        Set<SvpViewPort> rootNestedPorts  = root.getNestedPorts(true, SBPortDirection.OUT);
        Set<SvpViewPort> allRootNestedPorts  = root.getAllNestedPorts(SBPortDirection.OUT);

        Assert.assertEquals("Before expansion, should be no ordered SVMs", 0, orderedSvms.size());
        Assert.assertEquals("Before expansion, should be two all children", 2, allChildren.size());
        Assert.assertEquals("Before expansion, children should equal ordered children", children.size(), orderedChildren.size());
        Assert.assertEquals("Before expansion, ordered children should equal ordered svms", orderedChildren.size(), orderedSvms.size());
        Assert.assertEquals("Before expansion, should be no nested out ports", 0, rootNestedPorts.size());
        Assert.assertEquals("Before expansion, should be one all nested out port", 1, allRootNestedPorts.size());
        Assert.assertFalse("Before expansion, owner of port should not be visible", view.isVisible(allRootNestedPorts.iterator().next().getOwner()));
        
        view.expand(root);
        
        children = view.getChildren(root);
        orderedChildren  = root.getOrderedChildren(SvVo.class);
        orderedSvms  = root.getOrderedChildren(SvmVo.class);
        
        Assert.assertEquals("After root expansion, should be two children", 2, orderedSvms.size());
        Assert.assertEquals("After root expansion, children should equal ordered children", children.size(), orderedChildren.size());
        Assert.assertEquals("After root  expansion, ordered children should equal ordered svms", orderedChildren.size(), orderedSvms.size());
        
        rootNestedPorts  = root.getNestedPorts(true, SBPortDirection.OUT);
        
        Assert.assertEquals("After root expansion, should be one nested out ports", 1, rootNestedPorts.size());
        Assert.assertTrue("After root expansion, owner of port should be visible", view.isVisible(allRootNestedPorts.iterator().next().getOwner()));
        
        view.nodeSet().stream().forEach(view::expand);

        Assert.assertEquals("After expansion, root shoould have two children", 2, orderedSvms.size());
        Assert.assertEquals("After expansion, root's first child should be detector", ExampleFactory.M_DETECTOR.getIdentity(), orderedSvms.get(0).getSvpModule().getIdentity());
        Assert.assertEquals("After expansion, root's second child should be reporter",ExampleFactory.M_REPORTER.getIdentity(), orderedSvms.get(1).getSvpModule().getIdentity());
        Assert.assertEquals("After expansion, detector should have one port", 1, orderedSvms.get(0).getPorts().size());
        Assert.assertEquals("After expansion, reporter should have one port", 1, orderedSvms.get(1).getPorts().size());
        Assert.assertTrue("After expansion, detector port should be proxy", orderedSvms.get(0).getPorts().iterator().next().isProxy());
        Assert.assertTrue("After expansion, Reporter port should be proxy", orderedSvms.get(1).getPorts().iterator().next().isProxy());
        Assert.assertEquals("After expansion, Root should have one wire", 1, root.getNestedWires().size());
        Assert.assertEquals("After expansion, Detector port should be PoPS out", ExampleFactory.P_DETECTOR_POPS_OUT.getIdentity(), 
                ((SBPortInstance)orderedSvms.get(0).getPorts().iterator().next().getObject()).getDefinition().getIdentity());
        Assert.assertEquals("After expansion, Reporter port should be PoPS in", ExampleFactory.P_REPORTER_POPS_IN.getIdentity(), 
                ((SBPortInstance)orderedSvms.get(1).getPorts().iterator().next().getObject()).getDefinition().getIdentity());

        model.close();
    }



    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testSvpViewOrdering()  {

        SvpModel model = m.createModel(SvpModel.class, module);
        SvpPView view = model.getMainView();
        SvmVo root = view.getRoot();

        view.expand(root);
        List<SvmVo> orderedSvms  = root.getOrderedChildren(SvmVo.class);
        orderedSvms.forEach(view::expand);

        List<SvpVo> detectorChildren = orderedSvms.get(0).getOrderedChildren(SvpVo.class);
        Assert.assertEquals(ExampleFactory.P_PSPARK.getIdentity(), detectorChildren.get(0).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_RBS_SPAK.getIdentity(), detectorChildren.get(1).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_SPAK.getIdentity(), detectorChildren.get(2).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_RBS_SPAR.getIdentity(), detectorChildren.get(3).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_SPAR.getIdentity(), detectorChildren.get(4).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_TER_1.getIdentity(), detectorChildren.get(5).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_PSPAS.getIdentity(), detectorChildren.get(6).getSvp().getIdentity());

        List<SvpVo> reporterChildren  = orderedSvms.get(1).getOrderedChildren(SvpVo.class);
        Assert.assertEquals(ExampleFactory.P_RBS_SPAS.getIdentity(), reporterChildren.get(0).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_GFP.getIdentity(), reporterChildren.get(1).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_TER_2.getIdentity(), reporterChildren.get(2).getSvp().getIdentity());

        model.close();
    }

    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testSvpViewInteractions()  {

        SvpModel model = m.createModel(SvpModel.class, module);
        SvpPView view = model.getMainView();
        SvmVo root = view.getRoot();

        // Expand all modules

        view.expand(root);
        List<SvmVo> orderedSvms = root.getOrderedChildren(SvmVo.class);
        orderedSvms.forEach(view::expand);

        // collect interactions

        List<SviVo> rootInteractions = view.getChildren(root, SviVo.class);
        List<SviVo> detectorInteractions = view.getChildren(orderedSvms.get(0), SviVo.class);
        List<SviVo> reporterInteractions = view.getChildren(orderedSvms.get(1), SviVo.class);

        Collection<SvVo> allInteractions = view.findObjects(SviInstance.class);

        Assert.assertEquals(0, rootInteractions.size());
        Assert.assertEquals(11, detectorInteractions.size());
        Assert.assertEquals(2, reporterInteractions.size());
        Assert.assertEquals(13, allInteractions.size());

        model.close();
    }
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testSwapRbs()  {

        SvpModel model = m.createModel(SvpModel.class, module);
        SvpPView view = model.getMainView();
        SvmVo root = view.getRoot();
        
        view.nodeSet().stream().forEach(view::expand);
        
        // Swap RBS_SpaK and RBS_SpaR
        
        List<SvmVo> rootChildren  = root.getOrderedChildren(SvmVo.class);
        Assert.assertEquals("Detector should have 7 children", 7, rootChildren.get(0).getOrderedChildren(SvpVo.class).size());
        
        Set<String> allObjects = workspace.getObjects(SBValued.class, model.getRootNode().getContexts())
                .stream().map(obj -> obj.getIdentity().toASCIIString().replaceAll("_[0-9]", "")).collect(Collectors.toSet());

        // TODO: Neaten up the floating indexes caused by using DeferredAction

        ModuleDefinition detector1 = workspace.getObject(ExampleFactory.M_DETECTOR.getIdentity(), ModuleDefinition.class, model.getViewRoot().getContexts());
        int fcs1 = detector1.getFunctionalComponents().size();

        SBSvpBuilder b = new SvpActionBuilderImpl(workspace, root.getContexts())
            .removeChild(ExampleFactory.M_DETECTOR.getIdentity(), 1)
            // When the second removeChild is run, RBS at index 3 is at index 2 due to the first RBS' removal
            .removeChild(ExampleFactory.M_DETECTOR.getIdentity(), 2)
            .addSvp(ExampleFactory.M_DETECTOR, ExampleFactory.P_RBS_SPAR, 1)
            // When the second addChild is run, the insertion point is at index 3 again, due to the first RBS' insertion
            .addSvp(ExampleFactory.M_DETECTOR, ExampleFactory.P_RBS_SPAK, 3);
        workspace.perform(b.build());

        ModuleDefinition detector2 = workspace.getObject(ExampleFactory.M_DETECTOR.getIdentity(), ModuleDefinition.class, model.getViewRoot().getContexts());
        int fcs2 = detector2.getFunctionalComponents().size();

        Assert.assertEquals("Should be same number of functional components", fcs1, fcs2);

        Set<String> allObjects2 = workspace.getObjects(SBValued.class, model.getRootNode().getContexts())
                .stream().map(obj -> obj.getIdentity().toASCIIString().replaceAll("_[0-9]", "")).collect(Collectors.toSet());
        
        Assert.assertEquals("There should be an identical number of objects after moving", allObjects.size(), allObjects2.size());

        List<SvpVo> detectorChildren = rootChildren.get(0).getOrderedChildren(SvpVo.class);
        Assert.assertEquals("Detector should still have 7 children", 7, detectorChildren.size());
        Assert.assertEquals(ExampleFactory.P_RBS_SPAR.getIdentity(), detectorChildren.get(1).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_RBS_SPAK.getIdentity(), detectorChildren.get(3).getSvp().getIdentity());
    
        // SvpView wraps instances with Svp-based extensions in view objects
        
        List<SviVo> rootInteractions = view.getChildren(root, SviVo.class);
        List<SviVo> detectorInteractions = view.getChildren(rootChildren.get(0), SviVo.class);
        List<SviVo> reporterInteractions = view.getChildren(rootChildren.get(1), SviVo.class);
        
        Assert.assertEquals(0, rootInteractions.size());
        Assert.assertEquals(11, detectorInteractions.size());
        Assert.assertEquals(2, reporterInteractions.size());
        
        model.close();
    }

    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testSwapCds()  {

        workspace.perform(new DuplicateObject(module.getIdentity(), workspace, false, module.getContexts(), CONTEXTS));
        SvpModel model2 = m.createModel(SvpModel.class, workspace.getObject(module.getIdentity(), SvpModule.class, CONTEXTS));
        SvpPView view = model2.getMainView();
        SvmVo root = view.getRoot();

        view.nodeSet().stream().forEach(view::expand);

        // Swap RBS_SpaK and RBS_SpaR

        List<SvmVo> rootChildren  = root.getOrderedChildren(SvmVo.class);
        Assert.assertEquals("Detector should have 7 children", 7, rootChildren.get(0).getOrderedChildren(SvpVo.class).size());

        Set<String> allObjects = workspace.getObjects(SBValued.class, model2.getRootNode().getContexts())
                .stream().map(obj -> obj.getIdentity().toASCIIString().replaceAll("_[0-9]", "")).collect(Collectors.toSet());

        // TODO: Neaten up the floating indexes caused by using DeferredAction

        ModuleDefinition detectorContext1 = workspace.getObject(ExampleFactory.M_DETECTOR.getIdentity(), ModuleDefinition.class, module.getContexts());
        int fcs1 = detectorContext1.getFunctionalComponents().size();
        int mtFc1 = (int) detectorContext1.getFunctionalComponents().stream().flatMap(fc -> fc.getMapsTo().stream()).count();
        int i1 = detectorContext1.getInteractions().size();

        SvpModule detectorSvmContext1 = workspace.getObject(ExampleFactory.M_DETECTOR.getIdentity(), SvpModule.class, module.getContexts());
        int cs1 = detectorSvmContext1.getDnaDefinition().getComponents().size();
        int scs1 = detectorSvmContext1.getDnaDefinition().getSequenceConstraints().size();

       // view.hierarchyIterator(view.getRoot()).forEachRemaining(System.out::println);
       // detectorSvmContext1.getDnaDefinition().getSequenceConstraints().forEach(c -> System.out.println("C2: " + c.getIdentity().toASCIIString()));

        SBSvpBuilder b = new SvpActionBuilderImpl(workspace, CONTEXTS)
                .removeChild(ExampleFactory.M_DETECTOR.getIdentity(), 2)
                // When the second removeChild is run, RBS at index 3 is at index 2 due to the first RBS' removal
                .removeChild(ExampleFactory.M_DETECTOR.getIdentity(), 3)
                .addSvp(ExampleFactory.M_DETECTOR, ExampleFactory.P_SPAR, 2)
                // When the second addChild is run, the insertion point is at index 3 again, due to the first RBS' insertion
                .addSvp(ExampleFactory.M_DETECTOR, ExampleFactory.P_SPAK, 4);
        //        SBSvpBuilder b2 = new SvpActionBuilderImpl(workspace.getIdentity(), CONTEXTS)

        SBSvpBuilder b2 = new SvpActionBuilderImpl(workspace, CONTEXTS)
                .removeChild(ExampleFactory.M_DETECTOR.getIdentity(), 1)
                // When the second removeChild is run, RBS at index 3 is at index 2 due to the first RBS' removal
                .removeChild(ExampleFactory.M_DETECTOR.getIdentity(), 2)//
                .addSvp(ExampleFactory.M_DETECTOR, ExampleFactory.P_RBS_SPAR, 1)
                // When the second addChild is run, the insertion point is at index 3 again, due to the first RBS' insertion
                .addSvp(ExampleFactory.M_DETECTOR, ExampleFactory.P_RBS_SPAK, 3);


        workspace.perform(b.build());
        workspace.perform(b2.build());

        // System.out.println("------------");
        ModuleDefinition detectorModifiedContext2 = workspace.getObject(ExampleFactory.M_DETECTOR.getIdentity(), ModuleDefinition.class, CONTEXTS);
        int fcs2 = detectorModifiedContext2.getFunctionalComponents().size();
        int mtFc2 = (int) detectorModifiedContext2.getFunctionalComponents().stream().flatMap(fc -> fc.getMapsTo().stream()).count();
        int i2 = detectorModifiedContext2.getInteractions().size();

        SvpModule detectorSvmContext2 = workspace.getObject(ExampleFactory.M_DETECTOR.getIdentity(), SvpModule.class, CONTEXTS);
        int cs2 = detectorSvmContext2.getDnaDefinition().getComponents().size();
        int scs2 = detectorSvmContext2.getDnaDefinition().getSequenceConstraints().size();

        // detectorSvmContext2.getDnaDefinition().getSequenceConstraints().forEach(c -> System.out.println("C2: " + c.getIdentity().toASCIIString()));

        Assert.assertEquals("Should be same number of functional components", fcs1, fcs2);
        Assert.assertEquals("Should be same number of mapped functional components", mtFc1, mtFc2);
        Assert.assertEquals("Should be same number of functional components", i1, i2);
        Assert.assertEquals("Should be same number of components", cs1, cs2);
        Assert.assertEquals("Should be same number of sequence constraints", scs1, scs2);

        Set<String> allObjects2 = workspace.getObjects(SBValued.class, CONTEXTS)
                .stream().map(obj -> obj.getIdentity().toASCIIString().replaceAll("_[0-9]", "")).collect(Collectors.toSet());

        Assert.assertEquals("There should be an identical number of objects after moving", allObjects.size(), allObjects2.size());

        List<SvpVo> detectorChildren = rootChildren.get(0).getOrderedChildren(SvpVo.class);
        Assert.assertEquals("Detector should still have 7 children", 7, detectorChildren.size());
        Assert.assertEquals(ExampleFactory.P_SPAR.getIdentity(), detectorChildren.get(2).getSvp().getIdentity());
        Assert.assertEquals(ExampleFactory.P_SPAK.getIdentity(), detectorChildren.get(4).getSvp().getIdentity());

        // SvpView wraps instances with Svp-based extensions in view objects

        List<SviVo> rootInteractions = view.getChildren(root, SviVo.class);
        List<SviVo> detectorInteractions = view.getChildren(rootChildren.get(0), SviVo.class);
        List<SviVo> reporterInteractions = view.getChildren(rootChildren.get(1), SviVo.class);

        Assert.assertEquals(0, rootInteractions.size());
        Assert.assertEquals(11, detectorInteractions.size());
        Assert.assertEquals(2, reporterInteractions.size());

        model2.close();
    }
}
