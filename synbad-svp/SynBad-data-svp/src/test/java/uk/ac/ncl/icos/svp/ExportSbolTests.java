/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidate;
import org.sbolstandard.core2.SBOLValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonFullExporter;
import uk.ac.ncl.icos.synbad.sbol.io.SBSbolExporter;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class ExportSbolTests {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportSbolTests.class);
    private static final URI WORKSPACE_URI = URI.create("http://www.synbad.org/exportTests/1.0");
    private static final URI CONTEXT_URI = URI.create("http://www.synbad.org/exportTestsContext/1.0");
    private SBWorkspace ws;

    public ExportSbolTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );

        this.ws = new SBWorkspaceBuilder(WORKSPACE_URI)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        ws.createContext(CONTEXT_URI);
    }
    
    @After
    public void tearDown() {
        ws.shutdown();
        ws = null; 
    }

    //@Test
    public void importCelloModel()  {

        LOGGER.debug("Creating simple model...");

        File f2 = new File("imports/cello-export.xml");

        ws.getRdfService().addRdf(f2, ws.getContextIds());

        ws.getObjects(ModuleDefinition.class, ws.getContextIds()).stream()
                .forEach(obj -> {
                    LOGGER.debug(obj.getDisplayId());
                });
    }
    /**
     * Test of createEntity method, of class Workspace.
     */
    //@Test
    public void exportPromoter() {

        try {
            
            LOGGER.debug("Creating simple model...");

            SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, new URI[] {CONTEXT_URI});
            SBIdentity svpId = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svp", "1.0") ;
            b.createPromoter(svpId, 1.0);
            ws.perform(b.build());

            LOGGER.debug("Writing SBOL document of Promoter to .xml file...");

            File f2 = new File("exports/sbol-promoter.xml");
            if(!f2.exists())
                f2.delete();
            f2.createNewFile();

            
            FileOutputStream os = new FileOutputStream(f2);
            SBSbolExporter exporter = new SBSbolExporter();
            exporter.export(os, ws, ws.getContextIds());
            os.close();
            
            SBOLDocument d = null;
            try {
                d = SBOLReader.read(f2);
            } catch (SBOLValidationException | SBOLConversionException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }

            LOGGER.debug("Validating export...");

            SBOLValidate.clearErrors();

            if(d != null) {
                SBOLValidate.validateSBOL(d, true, true, true);
                SBOLValidate.getErrors().stream().forEach(LOGGER::warn);
            }
            
        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }
    
    /**
     * Test of createEntity method, of class Workspace.
     */
    //@Test
    public void exportSubtilinReceiver()  {

        try {
            
            LOGGER.debug("Setting up example model...");
            
            SvpModule def = ExampleFactory.setupWorkspace(ws, new URI[] {CONTEXT_URI});
            
            LOGGER.debug("Writing SBOL document of Example Model to .xml file...");
            
            File f2 = new File("exports/sbol-subtilinreceiver.xml");
            if(!f2.exists())
                f2.delete();
            f2.createNewFile();
            
            FileOutputStream os = new FileOutputStream(f2);
            SBSbolExporter exporter = new SBSbolExporter();
            exporter.export(os, ws, ws.getContextIds());
            os.close();
            
            SBOLDocument d = null;
            try {
                d = SBOLReader.read(f2);
            }  catch (SBOLValidationException | SBOLConversionException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
            
            LOGGER.debug("Validating exported XML file...");
            
            if(d != null) {
               SBOLValidate.clearErrors();
                SBOLValidate.validateSBOL(d, true, true, true);
                SBOLValidate.getErrors().stream().forEach(LOGGER::warn); 
            }
            
        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }

    }

    /**
     * Test of createEntity method, of class Workspace.
     */
     //@Test
    public void exportRepressilator()  {

        try {

            LOGGER.debug("Setting up example model...");

            SvpModel def = ExampleModuleFactory.createRepressilator(ws, new URI[] { CONTEXT_URI });

            LOGGER.debug("Writing SBOL document of Example Model to .xml file...");

            File f2 = new File("exports/sbol-repressilator.xml");
            if(!f2.exists())
                f2.delete();
            f2.createNewFile();

            FileOutputStream os = new FileOutputStream(f2);
            SBSbolExporter exporter = new SBSbolExporter();
            exporter.export(os, ws, ws.getContextIds());
            os.close();
        
            SBOLDocument d = null;
            try {
                d = SBOLReader.read(f2);
            }  catch (SBOLValidationException | SBOLConversionException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }

            LOGGER.debug("Validating exported XML file...");

            if(d != null) {
                SBOLValidate.clearErrors();
                SBOLValidate.validateSBOL(d, true, true, true);
                SBOLValidate.getErrors().stream().forEach(LOGGER::warn);
            }

        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }

    }

    //@Test
    public void exportGenModSink()  {

        try {

            LOGGER.debug("Setting up example model...");

            SvpModel def = ExampleModuleFactory.createGeneratorModulatorSink(ws, new URI[] { CONTEXT_URI });

            LOGGER.debug("Writing SBOL document of Example Model to .xml file...");

            File f2 = new File("exports/sbol-genModSink.xml");
            if(!f2.exists())
                f2.delete();
            f2.createNewFile();

            FileOutputStream os = new FileOutputStream(f2);
            SBSbolExporter exporter = new SBSbolExporter();
            exporter.export(os, ws, ws.getContextIds());
            os.close();
        
            SBOLDocument d = null;
            try {
                d = SBOLReader.read(f2);
            }  catch (SBOLValidationException | SBOLConversionException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }

            LOGGER.debug("Validating exported XML file...");

            if(d != null) {
                SBOLValidate.clearErrors();
                SBOLValidate.validateSBOL(d, true, true, true);
                SBOLValidate.getErrors().stream().forEach(LOGGER::warn);
            }

        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }

    }
    
    //@Test
    public void exportWorkspaceJson() {
        
        try {
            
            LOGGER.debug("Setting up example model...");
            
            SvpModule def = ExampleFactory.setupWorkspace(ws, new URI[] {CONTEXT_URI});
            
            LOGGER.debug("Exporting subtilin receiver to .synbad file...");
            
            File f2 = new File("exports/json-subtilinreceiver.json");
            if(!f2.exists())
                f2.delete();
            f2.createNewFile();
            
            SBJsonFullExporter exporter = new SBJsonFullExporter();
            FileOutputStream stream = new FileOutputStream(f2);
            Set<SBWorkspace> workspaces = new HashSet<>();
            workspaces.add(ws);
            exporter.export(stream, workspaces);
            stream.close();
        } catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }
}
