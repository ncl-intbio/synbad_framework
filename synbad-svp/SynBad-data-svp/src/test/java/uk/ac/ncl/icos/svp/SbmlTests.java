/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.parser.SbmlWriter;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.view.visualisation.SvpGraphStreamPanel;
import uk.ac.ncl.icos.synbad.view.visualisation.TimeCoursePlot;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.objects.DefaultSBIdentityFactory;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.net.URI;
import java.util.Arrays;

/**
 *
 * @author owengilfellon
 */
public class SbmlTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(SbmlTests.class);
    private static final SBIdentityFactory f = new DefaultSBIdentityFactory(null);

    private static final URI WORKSPACE_ID = URI.create("http://synbad.org/sbmltests/1.0");
    private static final URI[] CONTEXTS = new URI[] { URI.create("http://synbad.org/sbmltests/1.0") };

    private static final String PREFIX = "http://www.synbad.org";
    private static final String VERSION = "1.0";

    public static final SBIdentity M_SUBTILIN_RECEIVER = f.getIdentity(PREFIX, "SubtilinReceiver_SVP", VERSION);

    public static final SBIdentity M_DETECTOR = f.getIdentity(PREFIX, "Receiver_SVP", VERSION);
    public static final SBIdentity M_REPORTER = f.getIdentity(PREFIX, "Reporter_SVP", VERSION);

    public static final SBIdentity PROXY_DETECTOR_PROTEIN_OUT = f.getIdentity(PREFIX, M_DETECTOR.getDisplayID() + "_proxy_signal_protein_out", VERSION);
    public static final SBIdentity PROXY_REPORTER_PROTEIN_IN = f.getIdentity(PREFIX, M_REPORTER.getDisplayID() + "_proxy_signal_protein_in", VERSION);

    public static final SBIdentity C_SUBTILIN = f.getIdentity(PREFIX, "Subtilin", VERSION);

    public static final SBIdentity P_PSPARK = f.getIdentity(PREFIX, "PspaRK_SVP", VERSION);
    public static final SBIdentity P_RBS_SPAK = f.getIdentity(PREFIX, "RBS_SpaK_SVP", VERSION);
    public static final SBIdentity P_SPAR = f.getIdentity(PREFIX, "SpaR_SVP", VERSION);
    public static final SBIdentity P_TER_1 = f.getIdentity(PREFIX, "Ter1_SVP", VERSION);

    public static final SBIdentity P_PSPAS = f.getIdentity(PREFIX, "PspaS_SVP", VERSION);
    public static final SBIdentity P_OP = f.getIdentity(PREFIX, "PspaS_Op_SVP", VERSION);
    public static final SBIdentity P_RBS_SPAS = f.getIdentity(PREFIX, "RBS_SpaS_SVP", VERSION);
    public static final SBIdentity P_GFP = f.getIdentity(PREFIX, "GFP_SVP", VERSION);
    public static final SBIdentity P_TER_2 = f.getIdentity(PREFIX, "Ter2_SVP", VERSION);

    public static final SBIdentity I_SPAK_P_PHOS_SPAR = f.getIdentity(PREFIX, "SpaK_P_Phosphorylates_SpaR", VERSION);
    public static final SBIdentity I_SPAR_REPRESS_PSPAS = f.getIdentity(PREFIX, "SpaR_Represses_PspaS", VERSION);

    public static final SBIdentity proteinDefinition = SvpIdentityHelper.getProteinIdentity(P_SPAR);
    public static final SBIdentity outProteinPort =  SvpIdentityHelper.getPortIdentity(P_SPAR, proteinDefinition, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
    public static final SBIdentity inProteinPort =  SvpIdentityHelper.getPortIdentity(I_SPAR_REPRESS_PSPAS, proteinDefinition, SBPortDirection.IN, SynBadPortType.Protein, SynBadPortState.Default);
    public static final SBIdentity constraintId =  SvpIdentityHelper.getComponentIdentity(P_SPAR, ComponentType.Protein, SynBadPortState.Default);


    private SBWorkspace ws;

    public SbmlTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );

        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        ws.createContext(CONTEXTS[0]);
    }

    @Test
    public void testNegativeRegulationInModules() {

        SBAction actions = new SvpActionBuilderImpl(ws,  CONTEXTS)
                .createPromoter(P_PSPARK, 0.04)
                .createRBS(P_RBS_SPAK, 0.1818, 1.0)
                .createCDS(P_SPAR,  0.0012)
                .createTerminator(P_TER_1)
                .createPromoter(P_PSPAS, 0.04)
                .createOperator(P_OP)
                .createRBS(P_RBS_SPAS, 0.145, 1.0)
                .createCDS(P_GFP, 0.0012)
                .createTerminator(P_TER_2)
                .createSvm(M_SUBTILIN_RECEIVER)
                .createSvm(M_DETECTOR)
                .createSvm(M_REPORTER)
                .addSvp(M_DETECTOR, P_PSPARK, null)
                .addSvp(M_DETECTOR, P_OP, null)
                .addSvp(M_DETECTOR, P_RBS_SPAK, null)
                .addSvp(M_DETECTOR, P_SPAR, null)
                .addSvp(M_DETECTOR, P_TER_1, null)
                .addSvp(M_REPORTER, P_PSPAS, null)
                .addSvp(M_REPORTER, P_OP, null)
                .addSvp(M_REPORTER, P_RBS_SPAS, null)
                .addSvp(M_REPORTER, P_GFP, null)
                .addSvp(M_REPORTER, P_TER_2, null)
                .addSvm(M_SUBTILIN_RECEIVER, M_DETECTOR, null)
                .addSvm(M_SUBTILIN_RECEIVER, M_REPORTER, null)
                .createRepressByOperator(I_SPAR_REPRESS_PSPAS, ComponentType.Protein, SynBadPortState.Default, P_SPAR.getIdentity(), P_OP.getIdentity(), 0.42, 2.0)
                .build();

        ws.perform(actions);

        // Must export / import signals to instance interactions in reporter

        SBIdentity P_DETECTOR_PROTEIN_OUT = ws.getObject(P_SPAR.getIdentity(), Svp.class, CONTEXTS)
                .getPorts(SBPortDirection.OUT, SynBadPortType.Protein).stream()
                .map(portOut -> f.getIdentity(portOut.getIdentity())).findFirst().orElse(null);

        SBIdentity P_REPORTER_PROTEIN_IN = ws.getObject(I_SPAR_REPRESS_PSPAS.getIdentity(), SvpInteraction.class, CONTEXTS)
                .getPorts(SBPortDirection.IN, SynBadPortType.Protein).stream()
                .map(portIn -> f.getIdentity(portIn.getIdentity())).findFirst().orElse(null);

        ws.perform(new SvpActionBuilderImpl(ws,  CONTEXTS)
                .createProxyPort(PROXY_REPORTER_PROTEIN_IN, P_REPORTER_PROTEIN_IN.getIdentity(), M_REPORTER.getIdentity(), URI.create(SvpInteraction.TYPE))
                .createProxyPort(PROXY_DETECTOR_PROTEIN_OUT, P_DETECTOR_PROTEIN_OUT.getIdentity(), M_DETECTOR.getIdentity(), URI.create(Svp.TYPE))
                .build());

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, subtilinReceiverSvp);
        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);


        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-negative-regulation-in-modules");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }


    @Test
    public void testNegativeRegulation() {

        SBAction actions = new SvpActionBuilderImpl(ws,  CONTEXTS)
            .createPromoter(P_PSPARK, 0.04)
            .createRBS(P_RBS_SPAK, 0.1818, 1.0)
            .createCDS(P_SPAR,  0.0012)
            .createTerminator(P_TER_1)
            .createPromoter(P_PSPAS, 0.04)
            .createOperator(P_OP)
            .createRBS(P_RBS_SPAS, 0.145, 1.0)
            .createCDS(P_GFP, 0.0012)
            .createTerminator(P_TER_2)
            .createSvm(M_SUBTILIN_RECEIVER)
            .addSvp(M_SUBTILIN_RECEIVER, P_PSPARK, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_OP, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAK, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_SPAR, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_TER_1, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_PSPAS, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_OP, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_RBS_SPAS, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_GFP, null)
            .addSvp(M_SUBTILIN_RECEIVER, P_TER_2, null)
            .createRepressByOperator(I_SPAR_REPRESS_PSPAS, ComponentType.Protein, SynBadPortState.Default, P_SPAR.getIdentity(), P_OP.getIdentity(), 0.42, 2.0)
            .build();

        // Note, no DNA or whatnot associated at this point?

        ws.perform(actions);

        SvpModule subtilinReceiverSvp = ws.getObject(M_SUBTILIN_RECEIVER.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, subtilinReceiverSvp);
        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-negative-regulation");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }


    private void simulateModel(String sbml) throws Exception{
        SBSbmlSimulator s = new SimpleCopasiSimulator(1000, 1000);
        s.setModel(sbml);
        s.run();
        s.getResults().getSpecies().stream().forEach(
                m -> LOGGER.debug("{}: {}", m, Arrays.toString(s.getResults().getTimeCourse(m).toArray(new Double[] {})))
        );
    }


    public static void writeToXml(String sbml, String filename) {
        //LOGGER.debug(sbml);

        try
        {
            FileWriter writer = new FileWriter("exports/" + filename + ".xml");
            writer.write(sbml);
            writer.flush();
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }

    
    @After
    public void tearDown() {
        ws.shutdown();
        ws = null; 
    }

}
