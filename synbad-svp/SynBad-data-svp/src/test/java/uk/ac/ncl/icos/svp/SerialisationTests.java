/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

import java.io.*;
import java.net.URI;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
public class SerialisationTests {

    private final static Random random =  new Random();
    private final static URI workspace = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] contexts = new URI[] { URI.create("http://www.synbad.org/updatertests/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(SerialisationTests.class);
    private SBIdentity rootIdentity;
    private SBIdentity uberRootIdentity;
    private SBIdentity promoter;
    private SBIdentity cds;
    private SBIdentity small_molecule;
    private SBIdentity phosphorylator;
    private SBIdentity cds_activates_promoter;
    private SBIdentity phosphorylator_phosphorylates_cds;

    private SBWorkspace ws;
   // private SviUpdater updater;

    public SerialisationTests() {

    }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class);
        
        this.ws = new DefaultSBWorkspace(workspace);
       // this.ws.createContext(contexts[0]);
      //  this.updater = m.getService(workspace, SviUpdater.class);

    }
    
    @After
    public void tearDown() {   
        this.ws.shutdown();
        // m.close();
        this.ws = null;
    }
    
    
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
  //  @Test
    public void testSerialiseSbIdentified() throws IOException, ClassNotFoundException {
        SvpModule m  = ExampleFactory.setupWorkspace();

        String filename = "testSvm.sb";

        FileOutputStream file = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(file);

        // Method for serialization of object
        out.writeObject(m);
        out.close();
        file.close();

        FileInputStream file2 = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file2);

        // Method for deserialization of object
        SvpModule m2 = (SvpModule) in.readObject();

        in.close();
        file.close();

        System.out.println(m2);
        
    }

  //  @Test
    public void testSerialiseModel() throws IOException, ClassNotFoundException {
        SvpModel m  = ExampleModuleFactory.createAutoRegulation(ws, contexts);

        String filename = "testModel.sb";

        FileOutputStream file = new FileOutputStream(filename);
        ObjectOutputStream out = new ObjectOutputStream(file);

        // Method for serialization of object
        out.writeObject(m);
        out.close();
        file.close();

        FileInputStream file2 = new FileInputStream(filename);
        ObjectInputStream in = new ObjectInputStream(file2);

        // Method for deserialization of object
        SvpModel m2 = (SvpModel) in.readObject();

        in.close();
        file.close();

        System.out.println(m2);
        
        m.close();
        m2.close();
    }
}
