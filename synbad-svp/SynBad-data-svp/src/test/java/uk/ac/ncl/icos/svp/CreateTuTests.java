/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.actions.builders.*;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.svp.parser.SbmlWriter;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.*;
import uk.ac.ncl.icos.synbad.view.visualisation.SvpGraphStreamPanel;
import uk.ac.ncl.icos.synbad.view.visualisation.TimeCoursePlot;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObjectAndDependants;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.net.URI;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
public class CreateTuTests {

    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/updatertests/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(CreateTuTests.class);

    private SBIdentityFactory f;
    private SBWorkspace ws;

    private final SBIdentity rootIdentity;
    private final SBIdentity tu1Id;
    private final SBIdentity tu2Id;
    private final SBIdentity tu3Id;

    public CreateTuTests() throws SBIdentityException {
        rootIdentity = SBIdentity.getIdentity("http://www.synbad.org", "root", "1.0");
        tu1Id = SBIdentity.getIdentity("http://www.synbad.org", "tu1", "1.0");
        tu2Id = SBIdentity.getIdentity("http://www.synbad.org", "tu2", "1.0");
        tu3Id = SBIdentity.getIdentity("http://www.synbad.org", "tu3", "1.0");
    }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBModelFactory.class,
            SvpModelProvider.class);
        
        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
            .addService(new EnvConstantUpdater())
            .addService(new PiUpdater())
            .addService(new SviUpdater())
            .addService(new ExportedFcUpdater())
            .addService(new SBWireUpdater()).build();

        this.ws.createContext(CONTEXTS[0]);
        this.f =  ws.getIdentityFactory();
    }
    
    @After
    public void tearDown() {   
        this.ws.shutdown();
        this.ws = null;
    }


    public static void main(String[] args) {
        try {
           CreateTuTests test = new CreateTuTests();
            test.setUp();
            test.testCreateTu();
            test.tearDown();
        } catch (SBIdentityException ex) {
            Exceptions.printStackTrace(ex);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testCreateTu() {

        SBIdentity cdsA = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "A", tu1Id.getVersion());
        SBIdentity cdsB = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
        SBIdentity cdsC = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "C", tu1Id.getVersion());

        SBIdentity promA = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "promA", tu1Id.getVersion());
        SBIdentity promB = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "promB", tu1Id.getVersion());
        SBIdentity promC = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "promC", tu1Id.getVersion());

        SBIdentity aActB = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "A_B", tu1Id.getVersion());
        SBIdentity bActC = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "B_C", tu1Id.getVersion());
        SBIdentity cActA = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "C_A", tu1Id.getVersion());

        SBTuBuilder b = new CelloActionBuilder(ws,  CONTEXTS);

        // Build and perform actions

        b = b.createSvm(rootIdentity)
            .createTu(tu1Id)
                .setPromoterIdentity(promA)
                .setCdsIdentity(cdsA)
                .builder()
            .createTu(tu2Id)
                .setPromoterIdentity(promB)
                .setCdsIdentity(cdsB)
                .builder()
            .createTu(tu3Id)
                .setPromoterIdentity(promC)
                .setCdsIdentity(cdsC)
                .builder()
            .addSvm(rootIdentity, tu1Id, null)
            .addSvm(rootIdentity, tu2Id, null)
            .addSvm(rootIdentity, tu3Id, null)
            .getAs(SBSviBuilder.class)
            .createActivateByPromoter(aActB, ComponentType.Protein, SynBadPortState.Default, cdsA.getIdentity(), promB.getIdentity(), 0.3, 1.0)
            .createActivateByPromoter(bActC, ComponentType.Protein, SynBadPortState.Default, cdsB.getIdentity(), promC.getIdentity(), 0.3, 1.0)
            .createActivateByPromoter(cActA, ComponentType.Protein, SynBadPortState.Default, cdsC.getIdentity(), promA.getIdentity(), 0.3, 0.0)
            .getAs(SBTuBuilder.class);

        ws.perform(b.build());

        SvpModule root = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);

        Assert.assertEquals("Root ModuleDefinition should have DNA, DNA instances of each TU, and three exported proteins, ", (Integer) 7, root.as(ModuleDefinition.class).map(md -> md.getFunctionalComponents().size()).orElse(-1));

        // Can't export protein in of activation interaction before activation has been instanced. which it can't be ....

        Assert.assertEquals("Should be no interactions in root", (Integer) 0, root.as(ModuleDefinition.class).map(md -> md.getInteractions().size()).orElse(-1));
        Assert.assertEquals("Should be three SVPs in root SVM (proteins count as SVPs...)", 3, root.getSvpInstances().size());
        Assert.assertEquals("Should be three SVMs in root SVM", 3, root.getSvmInstances().size());

        // Test creation and removal of interactions between TUs.

//        root.getSvpInstances().size();
    }
}
