/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.event.pipes.*;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.actions.SBRdfCommandManager;

import java.net.URI;
import java.util.*;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceExperimentTests  {

    private final static Logger LOGGER = LoggerFactory.getLogger(WorkspaceExperimentTests.class);

    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBModelFactory.class,
            SvpModelProvider.class);
    }

    /**
         * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testModifyContexts()  {

        LOGGER.debug("================== Test Example Factory ==================");

        URI WORKSPACE_ID_1 = URI.create("http://www.synbad.org/workspace-one/1.0");
        URI WORKSPACE_ID_2 = URI.create("http://www.synbad.org/workspace-two/1.0");

        // Create first workspace with two contexts

        SBWorkspace ws_1 = new SBWorkspaceBuilder(WORKSPACE_ID_1)
                .addService(new EnvConstantUpdater())
                .addService(new PiUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater())
                .build();

        URI[] CONTEXTS_1 = new URI[] { URI.create("http://www.synbad.org/context-one/1.0") };
        ws_1.createContext(CONTEXTS_1[0]);
        URI[] SYS_CONTEXTS_1 = ws_1.getSystemContextIds(CONTEXTS_1);

        // Create the second workspace with contexts, and no services

        SBWorkspace ws_2 = new SBWorkspaceBuilder(WORKSPACE_ID_2)
                .build();

        URI[] CONTEXTS_2 = new URI[] { URI.create("http://www.synbad.org/context-two/1.0") };
        ws_2.createContext(CONTEXTS_2[0]);
        URI[] SYS_CONTEXTS_2 = ws_1.getSystemContextIds(CONTEXTS_2);

        // Create a pipeline to update context from workspace 1 events, and a command manager to
        // apply the new events to workspace 2

        DefaultSBEventPipelineSubscriberSource source = new DefaultSBEventPipelineSubscriberSource();
        SBRdfCommandManager commandManager = new SBRdfCommandManager(ws_2.getRdfService(), ws_2.getDispatcher());

        source.push()
                .map(new SBEventFilter.DefaultFilter(), e -> {
                    SBGraphEvent evt = (SBGraphEvent) e;
                    SBEvtWithSrc es = (SBEvtWithSrc) e;
                    SBEvtWithParent ep = (SBEvtWithParent) e;
                    return new DefaultSBGraphEvent<>(
                            evt.getType(),
                            evt.getGraphEntityType(),
                            evt.getData(), evt.getDataClass(),
                            es.getSource(),es.getSourceClass(),
                            ep.getParent(), ep.getParentClass(),
                            ((SBEvtWithContext)evt).getContexts()[0].toASCIIString().equals(SYS_CONTEXTS_1[0].toASCIIString()) ?
                                SYS_CONTEXTS_2 :
                                CONTEXTS_2);
                })
                .doSideEffect(new SBEventFilter.DefaultFilter(), e -> {
                    commandManager.perform(Collections.singleton(e));
                    ws_2.getDispatcher().publish(e);
                });

        // connect the pipe to workspace 1 by passing events to the push method.

        ws_1.getDispatcher().subscribe(new SBEventFilter.DefaultFilter(), source::push);

        // create model in workspace 1

        SvpModule subtilinReceiver = ExampleFactory.setupWorkspace(ws_1, CONTEXTS_1);

        // verify that workspace 1 and workspace 2 have the same number of statements

        int statements_1 = ws_1.getRdfService().getStatements(null, null, null, CONTEXTS_1).size();
        int statements_2 = ws_2.getRdfService().getStatements(null, null, null, CONTEXTS_2).size();

        LOGGER.info("Statements: {} | {}", statements_1, statements_2);
        Assert.assertEquals("Workspaces should have the same number of statements", statements_1, statements_2);
    }


}
