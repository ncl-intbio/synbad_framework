/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.SBSynBadContext;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.svp.api.SynBadSvp;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class ModelPerformanceTests {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelPerformanceTests.class);
    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/updatertests/1.0") };

    private SBWorkspace ws;
    private SBIdentityFactory f;

    private int NO_OF_MODELS = 50;
    private int NO_OF_CONCURRENT_MODELS = 50;

    private URI rootIdentity;
    //private SBWorkspaceGraph g;

    SynBadSvp context = new SynBadSvp(SBSynBadContext.WorkspaceType.MEMORY);

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );

        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        this.ws.createContext(CONTEXTS[0]);
        this.f =  ws.getIdentityFactory();
        SvpModel model = ExampleModuleFactory.createAutoRegulation(ws, CONTEXTS);
        this.rootIdentity = model.getRootNode().getIdentity();
    }
    
    @After
    public void tearDown() {
        this.ws.shutdown();
    }
    
    /**
     * Test of getRootNode method, of class SvpModel.
     */
    @Test
    public void testSequentialModelCreation() {

        long runningCount = 0;
        for(int i = 0; i < NO_OF_MODELS; i++) {
            long timeBefore = System.currentTimeMillis();
            SvpModule module = ws.getObject(rootIdentity, SvpModule.class, CONTEXTS);
            SvpModel model = context.getModelFactory().createModel(SvpModel.class, module);
            SvpPView view = model.getMainView();
            model.close();
            long timeAfter = System.currentTimeMillis();
            if(i % 10 == 0)
                LOGGER.debug("Creation of model and view: {} ms", timeAfter - timeBefore);

            runningCount += (timeAfter - timeBefore);
        }

        LOGGER.debug("Average: {} ms", runningCount / NO_OF_MODELS);
    }

    @Test
    public void testConcurrentModelCreation() {

        List<SvpModel> models = new ArrayList<>();

        long runningCount = 0;

        for(int i = 0; i < NO_OF_CONCURRENT_MODELS; i++) {
            long timeBefore = System.currentTimeMillis();
            SvpModule module = ws.getObject(rootIdentity, SvpModule.class, CONTEXTS);
            SvpModel model = context.getModelFactory().createModel(SvpModel.class, module);
            SvpPView view = model.getMainView();
            long timeAfter = System.currentTimeMillis();
            models.add(model);
            if(i % 10 == 0)
                LOGGER.debug("Creation of model and view: {} ms", timeAfter - timeBefore);

            runningCount += (timeAfter - timeBefore);
        }

        LOGGER.debug("Average: {} ms", runningCount / NO_OF_CONCURRENT_MODELS);

        models.stream().forEach(SvpModel::close);
    }
}
