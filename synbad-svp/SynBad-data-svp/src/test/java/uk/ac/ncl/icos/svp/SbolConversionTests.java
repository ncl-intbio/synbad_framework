/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.sbolstandard.core2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.sbol.io.SBSbolExporter;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonFullExporter;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author owengilfellon
 */
public class SbolConversionTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(SbolConversionTests.class);
    private static final URI WORKSPACE_URI = URI.create("http://www.synbad.org/exportTests/1.0");
    private static final URI CONTEXT_URI = URI.create("http://www.synbad.org/exportTestsContext/1.0");
    private SBWorkspace ws;

    public SbolConversionTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );

        this.ws = new SBWorkspaceBuilder(WORKSPACE_URI)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
        ws.createContext(CONTEXT_URI);
    }
    
    @After
    public void tearDown() {
        ws.shutdown();
        ws = null; 
    }

   @Test
    public void importCelloModel()  {

        LOGGER.debug("Creating simple model...");

        File f2 = new File("imports/cello-export.xml");

        ws.getRdfService().addRdf(f2, ws.getContextIds());

      //SBObjectFactoryManager m = SBObjectFactoryManager.get();

       SBDataDefManager m = SBDataDefManager.getManager();

        ws.getObjects(ComponentDefinition.class, new URI[] { CONTEXT_URI }).forEach(cd -> {
            LOGGER.debug("{} [ {} ] : [ {} ] : [ {} ]",
                cd.getDisplayId(),
                cd.getSbolRoles().stream().map(role -> role.toString()).reduce("", (a, b) -> a.isEmpty() ? b : " | ".concat(b)),
                cd.getSbolTypes().stream().map(role -> role.toString()) .reduce("", (a, b) -> a.isEmpty() ? b : " | ".concat(b)),
                cd.getOrderedComponents().stream().map(role -> role.toString()) .reduce("", (a, b) -> a.isEmpty() ? b : " | ".concat(b)));
        });

        ws.getObjects(ModuleDefinition.class, ws.getContextIds()).stream()
                .forEach(obj -> {
                    LOGGER.debug(obj.getDisplayId());
                    obj.getFunctionalComponents().stream().forEach(fc -> {LOGGER.debug("\t{}", fc.getDisplayId());});

                });
    }


}
