/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;

import java.net.URI;

/**
 *
 * @author owengilfellon
 */
public class SvpIdTests {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpIdTests.class);
    private SBWorkspace workspace;

    private final static URI WS_ID = URI.create("http://www.synbad.org/ws/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/id-tests/1.0") };

    //private SBWorkspaceGraph g;

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

        LOGGER.debug("Setting up workspace...");
        workspace = new DefaultSBWorkspace(WS_ID);
    }
    
    @After
    public void tearDown() {
        workspace.shutdown();
        workspace = null;
    }

    @Test
    public void testSvpIdentities() {

        SBIdentity svmId = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "X", "1.0");
        SBIdentity svmCdId = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "cd_" + svmId.getDisplayID() + "_dna", svmId.getVersion());

        SBIdentity svpId = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "A", "1.0");
        SBIdentity svp2Id = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "B", "1.0");

        SBIdentity componentId = Component.getComponentIdentity(svmCdId, svpId);
        SBIdentity component2Id = Component.getComponentIdentity(svmCdId, svp2Id);

        Assert.assertEquals(svmCdId.getDisplayID(), SBIdentityHelper.getParentDisplayIdFromId(componentId.getIdentity()));
        Assert.assertEquals("c_" + svpId.getDisplayID(), SBIdentityHelper.getChildDisplayIdFromId(componentId.getIdentity()));
        Assert.assertEquals("c_" + svp2Id.getDisplayID(), SBIdentityHelper.getChildDisplayIdFromId(component2Id.getIdentity()));

        SBIdentity fcId = FunctionalComponent.getFunctionalComponentIdentity(svmId, svpId);

        Assert.assertEquals(svmId.getDisplayID(), SBIdentityHelper.getParentDisplayIdFromId(fcId.getIdentity()));
        Assert.assertEquals("fc_" + svpId.getDisplayID(), SBIdentityHelper.getChildDisplayIdFromId(fcId.getIdentity()));

        SBIdentity scId = SequenceConstraint.getSequenceConstraintIdentity(svmCdId, componentId, component2Id, ConstraintRestriction.PRECEDES);
        SBIdentity mtId = MapsTo.getMapsToIdentity(svmId, componentId, component2Id, MapsToRefinement.verifyIdentical);

        SBIdentity proteinAId = SvpIdentityHelper.getComponentIdentity(svpId, ComponentType.Protein, SynBadPortState.Default);
        SBIdentity proteinBId = SvpIdentityHelper.getComponentIdentity(svp2Id, ComponentType.Protein, SynBadPortState.Default);

        SBIdentity portId = SvpIdentityHelper.getPortIdentity(svpId, proteinAId, SBPortDirection.OUT, SynBadPortType.Protein, SynBadPortState.Default);
        SBIdentity portInstanceId = SvpIdentityHelper.getPortInstanceIdentity(fcId, portId);
        SBIdentity proxyPort1Id = SvpIdentityHelper.getProxyPortIdentity(svmId, portId);
    }
}
