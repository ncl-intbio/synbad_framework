/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 *
 * @author owengilfellon
 */
public class ServiceUpdaterTests {
    
    private final URI workspaceId = URI.create("http://www.synbad.org/addRemove");
    private final URI[] contexts = new URI[] { URI.create("http://www.synbad.org/addRemoveContext") };
    private Logger LOGGER = LoggerFactory.getLogger(ServiceUpdaterTests.class);
    private SBWorkspace workspace;
    private SBModelManager m;
    private SBIdentityFactory f;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );

        LOGGER.debug("Setting up workspace...");
        workspace = new SBWorkspaceBuilder(workspaceId)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        m = Lookup.getDefault().lookup(SBModelManager.class);
        f = workspace.getIdentityFactory();
    }
    
    @After
    public void tearDown() {
        workspace.shutdown();
        workspace = null;
    }
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testAddParts()  {
        
        SBSvpBuilder b = new SvpActionBuilderImpl(workspace, contexts);
        
        SBIdentity root = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity smlA = f.getIdentity("http://www.synbad.org", "smlA", "1.0");
        SBIdentity cdsA = f.getIdentity("http://www.synbad.org", "A", "1.0");
        SBIdentity cdsB = f.getIdentity("http://www.synbad.org", "B", "1.0");       
        SBIdentity phos = f.getIdentity("http://www.synbad.org", "A_phos_B", "1.0");       
        
        SBAction action = b.createSvm(root)
            .createCDSWithPhosphorylatingSmallMolecule(cdsA, smlA, 0, 0, 0, 0)
            .createCDS(cdsB, 0)
            .asInteractionBuilder()
                .createPhosphorylate(phos, cdsA.getIdentity(), cdsB.getIdentity(), 0, 0, 0)
                .asPartBuilder()
            .addSvp(root, cdsA, null)
            .addSvp(root, cdsB, null)
            .build();
             
        workspace.perform(action);
        
        SvpModule module = workspace.getObject(root.getIdentity(), SvpModule.class, contexts);
        Assert.assertEquals("Module should contain A and B", 2, module.getChildren().size());
        
        ComponentDefinition dna = module.getDnaDefinition();
        Assert.assertEquals("Module DNA should contain A and B DNA", 2, dna.getComponents().size());
        
        ModuleDefinition parent = module.getDnaInstance().getParent();
        Assert.assertEquals("ModuleDefinition should be Module and contain DNA instance", parent.getIdentity(), module.getIdentity());
        
        ComponentDefinition svpA = workspace.getObject(cdsA.getIdentity(), ComponentDefinition.class, contexts);
        ComponentDefinition svpB = workspace.getObject(cdsB.getIdentity(), ComponentDefinition.class, contexts);
        
        Set<FunctionalComponent> functionalComponents = parent.getFunctionalComponents();
        
        Assert.assertTrue("Module should contain instance of svpA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_A") && fc.getDefinition().get().equals(svpA)));
        Assert.assertTrue("Module should contain instance of svpB", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_B") && fc.getDefinition().get().equals(svpB)));
        Assert.assertTrue("Module should contain instance of module DNA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_root_dna")));
        Assert.assertTrue("Module should contain instance of small molecule", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("cd_smlA_smlMol")));
        Assert.assertTrue("Module should contain instance of unphosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_protein")));
        Assert.assertTrue("Module should contain instance of unphosphorylated B", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_B_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_p_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated B", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_B_p_protein")));

        Set<Interaction> interactions = parent.getInteractions();
        
        Assert.assertTrue("Module should contain A production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Production_1")));
        Assert.assertTrue("Module should contain A degradation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Degradation")));
        Assert.assertTrue("Module should contain A~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain A~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_A_Degradation")));
        
        Assert.assertTrue("Module should contain B production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_B_Production_1")));
        Assert.assertTrue("Module should contain B degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_B_Degradation")));
        Assert.assertTrue("Module should contain B~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_B_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain B~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_B_Degradation")));
        
        Assert.assertTrue("Module should contain sml mol phosphorylates A interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_smlA_phos_cd_A_protein")));
        Assert.assertTrue("Module should contain A phosphorylates B interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_A_phos_B")));

    }
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testRemoveParts()  {
        
        SBIdentity root = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity smlA = f.getIdentity("http://www.synbad.org", "smlA", "1.0");
        SBIdentity cdsA = f.getIdentity("http://www.synbad.org", "A", "1.0");
        SBIdentity cdsB = f.getIdentity("http://www.synbad.org", "B", "1.0");       
        SBIdentity phos = f.getIdentity("http://www.synbad.org", "A_phos_B", "1.0");       
        
        SBAction action = new SvpActionBuilderImpl(workspace, contexts).createSvm(root)
            .createCDSWithPhosphorylatingSmallMolecule(cdsA, smlA, 0, 0, 0, 0)
            .createCDS(cdsB, 0)
            .asInteractionBuilder()
                .createPhosphorylate(phos, cdsA.getIdentity(), cdsB.getIdentity(), 0, 0, 0)
                .asPartBuilder()
            .addSvp(root, cdsA, null)
            .addSvp(root, cdsB, null)
            .build();
             
        workspace.perform(action);
        
        SBAction action2 = new SvpActionBuilderImpl(workspace, contexts)
            .removeChild(root.getIdentity(), 1).build();
        
        workspace.perform(action2);

        SvpModule module = workspace.getObject(root.getIdentity(), SvpModule.class, contexts);
        Assert.assertEquals("Module should contain only A", 1, module.getChildren().size());
        
        ComponentDefinition dna = module.getDnaDefinition();
        Assert.assertEquals("Module DNA should contain only A DNA", 1, dna.getComponents().size());
        
        ModuleDefinition parent = module.getDnaInstance().getParent();
        Assert.assertEquals("ModuleDefinition should be Module and contain DNA instance", parent.getIdentity(), module.getIdentity());
        
        ComponentDefinition svpA = workspace.getObject(cdsA.getIdentity(), ComponentDefinition.class, contexts);
        ComponentDefinition svpB = workspace.getObject(cdsB.getIdentity(), ComponentDefinition.class, contexts);
        
        Set<FunctionalComponent> functionalComponents = parent.getFunctionalComponents();

        Assert.assertTrue("Module should contain instance of svpA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_A") && fc.getDefinition().get().equals(svpA)));
        Assert.assertTrue("Module should not contain instance of svpB", functionalComponents.stream()
                .noneMatch(fc -> fc.getDisplayId().equals("fc_B") && fc.getDefinition().get().equals(svpB)));
        
        Assert.assertTrue("Module should contain instance of module DNA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_root_dna")));
        
        Assert.assertTrue("Module should contain instance of small molecule", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("cd_smlA_smlMol")));
        Assert.assertTrue("Module should contain instance of unphosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_protein")));
        Assert.assertTrue("Module should not contain instance of unphosphorylated B", functionalComponents.stream()
                .noneMatch(fc -> fc.getDisplayId().equals("fc_cd_B_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_p_protein")));
        Assert.assertTrue("Module should not contain instance of phosphorylated B", functionalComponents.stream()
                .noneMatch(fc -> fc.getDisplayId().equals("fc_cd_B_p_protein")));

        Set<Interaction> interactions = parent.getInteractions();
        
        Assert.assertTrue("Module should contain A production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Production_1")));
        Assert.assertTrue("Module should contain A degradation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Degradation")));
        Assert.assertTrue("Module should contain A~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain A~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_A_Degradation")));
        
        Assert.assertTrue("Module should not contain B production interaction", interactions.stream()
                .noneMatch(i -> i.getDisplayId().equals("i_B_Production_1")));
        Assert.assertTrue("Module should not contain B degradation interaction", interactions.stream()
                .noneMatch(i -> i.getDisplayId().equals("i_B_Degradation")));
        Assert.assertTrue("Module should not contain B~P dephosphorylation interaction", interactions.stream()
                .noneMatch(i -> i.getDisplayId().equals("i_B_P_Dephosphorylation")));
        Assert.assertTrue("Module should not contain B~P degradation interaction", interactions.stream()
                .noneMatch(i -> i.getDisplayId().equals("i_Phosphorylated_B_Degradation")));
        
        Assert.assertTrue("Module should contain sml mol phosphorylates A interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_smlA_phos_cd_A_protein")));
        Assert.assertTrue("Module should not contain A phosphorylates B interaction", interactions.stream()
               .noneMatch(i -> i.getDisplayId().equals("i_A_phos_B")));
    }
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testRemoveThenReaddParts()  {
        
        SBIdentity root = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity smlA = f.getIdentity("http://www.synbad.org", "smlA", "1.0");
        SBIdentity cdsA = f.getIdentity("http://www.synbad.org", "A", "1.0");
        SBIdentity cdsB = f.getIdentity("http://www.synbad.org", "B", "1.0");       
        SBIdentity phos = f.getIdentity("http://www.synbad.org", "A_phos_B", "1.0");       
        
        SBAction action = new SvpActionBuilderImpl(workspace, contexts).createSvm(root)
            .createCDSWithPhosphorylatingSmallMolecule(cdsA, smlA, 0, 0, 0, 0)
            .createCDS(cdsB, 0)
            .asInteractionBuilder()
                .createPhosphorylate(phos, cdsA.getIdentity(), cdsB.getIdentity(), 0, 0, 0)
                .asPartBuilder()
            .addSvp(root, cdsA, null)
            .addSvp(root, cdsB, null)
            .build();
             
        workspace.perform(action);
        
        SBAction action2 = new SvpActionBuilderImpl(workspace, contexts)
            .removeChild(root.getIdentity(), 1).build();
        
        workspace.perform(action2);

        SBAction action3 = new SvpActionBuilderImpl(workspace, contexts)
            .addSvp(root, cdsB, null).build();
        
        workspace.perform(action3);
        
        SvpModule module = workspace.getObject(root.getIdentity(), SvpModule.class, contexts);
        Assert.assertEquals("Module should contain A and B", 2, module.getChildren().size());
        
        ComponentDefinition dna = module.getDnaDefinition();
        Assert.assertEquals("Module DNA should contain A and B DNA", 2, dna.getComponents().size());
        
        ModuleDefinition parent = module.getDnaInstance().getParent();
        Assert.assertEquals("ModuleDefinition should be Module and contain DNA instance", parent.getIdentity(), module.getIdentity());
        
        ComponentDefinition svpA = workspace.getObject(cdsA.getIdentity(), ComponentDefinition.class, contexts);
        ComponentDefinition svpB = workspace.getObject(cdsB.getIdentity(), ComponentDefinition.class, contexts);
                
        Assert.assertEquals("First ordered component should be A", svpA, dna.getOrderedComponents().get(0).getDefinition().get());
        Assert.assertEquals("Second ordered component should be B", svpB, dna.getOrderedComponents().get(1).getDefinition().get());
        
        Set<FunctionalComponent> functionalComponents = parent.getFunctionalComponents();
        
        Assert.assertTrue("Module should contain instance of svpA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_A") && fc.getDefinition().get().equals(svpA)));
        Assert.assertTrue("Module should contain instance of svpB", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_B") && fc.getDefinition().get().equals(svpB)));
        Assert.assertTrue("Module should contain instance of module DNA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_root_dna")));
        Assert.assertTrue("Module should contain instance of small molecule", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("cd_smlA_smlMol")));
        Assert.assertTrue("Module should contain instance of unphosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_protein")));
        Assert.assertTrue("Module should contain instance of unphosphorylated B", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_B_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_p_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated B", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_B_p_protein")));

        Set<Interaction> interactions = parent.getInteractions();
        
        Assert.assertTrue("Module should contain A production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Production_1")));
        Assert.assertTrue("Module should contain A degradation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Degradation")));
        Assert.assertTrue("Module should contain A~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain A~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_A_Degradation")));
        
        // TO-DO: Don't increment IDs if re-adding?
       
        Assert.assertTrue("Module should contain B production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_B_Production_2")));
        Assert.assertTrue("Module should contain B degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_B_Degradation")));
        Assert.assertTrue("Module should contain B~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_B_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain B~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_B_Degradation")));
        
        Assert.assertTrue("Module should contain sml mol phosphorylates A interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_smlA_phos_cd_A_protein")));
        Assert.assertTrue("Module should contain A phosphorylates B interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_A_phos_B")));
    }
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testSwapParts()  {
        
        SBIdentity root = f.getIdentity("http://www.synbad.org", "root", "1.0");
        SBIdentity smlA = f.getIdentity("http://www.synbad.org", "smlA", "1.0");
        SBIdentity cdsA = f.getIdentity("http://www.synbad.org", "A", "1.0");
        SBIdentity cdsB = f.getIdentity("http://www.synbad.org", "B", "1.0");       
        SBIdentity phos = f.getIdentity("http://www.synbad.org", "A_phos_B", "1.0");       
        
        SBAction action = new SvpActionBuilderImpl(workspace, contexts).createSvm(root)
            .createCDSWithPhosphorylatingSmallMolecule(cdsA, smlA, 0, 0, 0, 0)
            .createCDS(cdsB, 0)
            .asInteractionBuilder()
                .createPhosphorylate(phos, cdsA.getIdentity(), cdsB.getIdentity(), 0, 0, 0)
                .asPartBuilder()
            .addSvp(root, cdsA, null)
            .addSvp(root, cdsB, null)
            .build();
             
        workspace.perform(action);
        
        SvpModule module = workspace.getObject(root.getIdentity(), SvpModule.class, contexts);
        ModuleDefinition parent = module.getDnaInstance().getParent();
        ComponentDefinition svpA = workspace.getObject(cdsA.getIdentity(), ComponentDefinition.class, contexts);
        ComponentDefinition svpB = workspace.getObject(cdsB.getIdentity(), ComponentDefinition.class, contexts);
        
        Set<URI> allObjects = workspace.getObjects(SBValued.class, contexts)
            .stream().map(obj -> obj.getIdentity()).collect(Collectors.toSet());
      

        SBAction action2 = new SvpActionBuilderImpl(workspace, contexts)
            .removeChild(root.getIdentity(), 0)
            .removeChild(root.getIdentity(), 0)
            .addSvp(root, cdsB, null)
            .addSvp(root, cdsA, null).build();
        
        workspace.perform(action2);
        
        Set<URI> allObjects2 = workspace.getObjects(SBValued.class, contexts)
            .stream().map(obj -> obj.getIdentity()).collect(Collectors.toSet());

        Assert.assertEquals("There should be an identical number of objects after moving", allObjects.size(), allObjects2.size());

        ComponentDefinition dna = module.getDnaDefinition();
        
        Assert.assertEquals("First ordered component should be B", svpB, dna.getOrderedComponents().get(0).getDefinition().get());
        Assert.assertEquals("Second ordered component should be A", svpA, dna.getOrderedComponents().get(1).getDefinition().get());
            
        Set<FunctionalComponent> functionalComponents = parent.getFunctionalComponents();
        
        Assert.assertTrue("Module should contain instance of svpA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_A") && fc.getDefinition().get().equals(svpA)));
        Assert.assertTrue("Module should contain instance of svpB", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_B") && fc.getDefinition().get().equals(svpB)));
        Assert.assertTrue("Module should contain instance of module DNA", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_root_dna")));
        Assert.assertTrue("Module should contain instance of small molecule", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("cd_smlA_smlMol")));
        Assert.assertTrue("Module should contain instance of unphosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_protein")));
        Assert.assertTrue("Module should contain instance of unphosphorylated B", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_B_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated A", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_A_p_protein")));
        Assert.assertTrue("Module should contain instance of phosphorylated B", functionalComponents.stream()
                .anyMatch(fc -> fc.getDisplayId().equals("fc_cd_B_p_protein")));

        Set<Interaction> interactions = parent.getInteractions();
        
        Assert.assertTrue("Module should contain A production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Production_2")));
        Assert.assertTrue("Module should contain A degradation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_Degradation")));
        Assert.assertTrue("Module should contain A~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_A_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain A~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_A_Degradation")));
        
        // TO-DO: Don't increment IDs if re-adding?
       
        Assert.assertTrue("Module should contain B production interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_B_Production_2")));
        Assert.assertTrue("Module should contain B degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_B_Degradation")));
        Assert.assertTrue("Module should contain B~P dephosphorylation interaction", interactions.stream()
                .anyMatch(i -> i.getDisplayId().equals("i_B_P_Dephosphorylation")));
        Assert.assertTrue("Module should contain B~P degradation interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_Phosphorylated_B_Degradation")));
        
        Assert.assertTrue("Module should contain sml mol phosphorylates A interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_smlA_phos_cd_A_protein")));
        Assert.assertTrue("Module should contain A phosphorylates B interaction", interactions.stream()
               .anyMatch(i -> i.getDisplayId().equals("i_A_phos_B")));
    }
    
}
