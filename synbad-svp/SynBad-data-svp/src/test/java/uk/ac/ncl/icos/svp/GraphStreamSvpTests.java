/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.net.URI;
import javax.swing.JFrame;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.netbeans.junit.MockServices;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.workspace.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.view.visualisation.SvpGraphStreamPanel;

/**
 *
 * @author owengilfellon
 */
public class GraphStreamSvpTests {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphStreamSvpTests.class);
    private final URI WORKSPACE_ID = URI.create("http://www.synbad.org/graphStreamTests");
    private final URI[] CONTEXT_URI = new URI[] { URI.create("http://www.synbad.org/graphStreamTestsContext") };

    SvpModule module;
    SBWorkspace result;
    SBWorkspaceGraph g;
    
    public GraphStreamSvpTests() { }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class);

        result =  new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        module = ExampleModuleFactory.createGeneratorModulatorSink(result, CONTEXT_URI).getRootNode();
        g = new SBWorkspaceGraph(result, module.getContexts());
    }
    
    @After
    public void tearDown() throws Exception { 


        module = null;
        g.close();
        result.shutdown();
        g = null;
        result = null;
    }
    
    public static void main(String[] args) {
        try {
            GraphStreamSvpTests test = new GraphStreamSvpTests();
            test.setUp();
            test.testSetupWorkspace();
            test.tearDown();
        } catch (SBIdentityException ex) {
            Exceptions.printStackTrace(ex);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    //@Test
    public void testSetupWorkspace() throws SBIdentityException {

        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        
        SvpModel model = new SvpModel(result, module);
        SvpPView view = model.getMainView();
        view.nodeSet().stream().forEach(n -> view.expand(n));

        SvpGraphStreamPanel panel = new SvpGraphStreamPanel();
        panel.setView(view.getRoot(), view);
        
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        panel.run();
        
    }
}
