/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.awt.*;
import java.io.FileWriter;
import java.net.URI;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.flow.object.api.SBPort;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.svp.parser.SbmlWriter;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.view.visualisation.TimeCoursePlot;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSviBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

import javax.swing.*;

/**
 *
 * @author owengilfellon
 */
public class CreateInteractionsTests implements SBSubscriber {
    
    private final static Random random =  new Random();
    private final static URI workspace = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] contexts = new URI[] { URI.create("http://www.synbad.org/updatertests/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(CreateInteractionsTests.class);
    private SBIdentity rootIdentity;
    private SBIdentity uberRootIdentity;
    private SBIdentity promoter;
    private SBIdentity cds;
    private SBIdentity small_molecule;
    private SBIdentity phosphorylator;
    private SBIdentity cds_activates_promoter;
    private SBIdentity phosphorylator_phosphorylates_cds;
    
    private SBWorkspace ws;
   // private SviUpdater updater;

    public CreateInteractionsTests() {
            uberRootIdentity = SBIdentity.getIdentity("http://www.synbad.org", "uberRoot", "1.0");;
            rootIdentity = SBIdentity.getIdentity("http://www.synbad.org", "root", "1.0");
            promoter = SBIdentity.getIdentity("http://www.synbad.org", "promoterP", "1.0");
            cds = SBIdentity.getIdentity("http://www.synbad.org", "proteinB", "1.0");
            cds_activates_promoter = SBIdentity.getIdentity("http://www.synbad.org", "B_act_P", "1.0");
            phosphorylator_phosphorylates_cds = SBIdentity.getIdentity("http://www.synbad.org", "A_binds_B", "1.0");
            phosphorylator = SBIdentity.getIdentity("http://www.synbad.org", "proteinA", "1.0");
            small_molecule = SBIdentity.getIdentity("http://www.synbad.org", "smlMol", "1.0");
    }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBModelFactory.class,
            SvpModelProvider.class);

        this.ws = new SBWorkspaceBuilder(workspace)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        this.ws.createContext(contexts[0]);
    }
    
    @After
    public void tearDown() {   
        this.ws.shutdown();
        this.ws = null;
    }


    @Test
    public void testActivateByPromoter() {

        LOGGER.debug("================== Test Regulation ==================");

        SBIdentityFactory f = ws.getIdentityFactory();

        // Create Identities

        SBIdentity svmId = f.getIdentity("http://www.synbad.org", "svm1", "1.0");
        SBIdentity promoter1Id = f.getIdentity("http://www.synbad.org", "promoter1", "1.0");
        SBIdentity rbs1Id = f.getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Id = f.getIdentity("http://www.synbad.org", "cds1", "1.0");
        SBIdentity terminator1Id = f.getIdentity("http://www.synbad.org", "terminator1", "1.0");

        SBIdentity promoter2Id = f.getIdentity("http://www.synbad.org", "promoter2", "1.0");
        SBIdentity rbs2Id = f.getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Id = f.getIdentity("http://www.synbad.org", "cds2", "1.0");
        SBIdentity terminator2Id = f.getIdentity("http://www.synbad.org", "terminator2", "1.0");

        SBIdentity cds_activates_promotoer = f.getIdentity("http://www.synbad.org", "cds_activate_promoter", "1.0");

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        b.createPromoter(promoter1Id, 1.0);
        b.createRBS(rbs1Id, 0.5, 1.0);
        b.createCDS(cds1Id, 1.0);
        b.createTerminator(terminator1Id);
        b.createPromoter(promoter2Id, 1.0);
        b.createRBS(rbs2Id, 0.5, 1.0);
        b.createCDS(cds2Id, 1.0);
        b.createTerminator(terminator2Id);
        b.createSvm(svmId);

        b.addSvp(svmId, promoter1Id, null);
        b.addSvp(svmId, rbs1Id, null);
        b.addSvp(svmId, cds1Id, null);
        b.addSvp(svmId, terminator1Id, null);
        b.addSvp(svmId, promoter2Id, null);
        b.addSvp(svmId, rbs2Id, null);
        b.addSvp(svmId, cds2Id, null);
        b.addSvp(svmId, terminator2Id, null);
        b.asInteractionBuilder().createActivateByPromoter(cds_activates_promotoer,
                ComponentType.Protein, SynBadPortState.Default, cds1Id.getIdentity(), promoter2Id.getIdentity(), 0.23, 1.0);

        ws.perform(b.build());

        SvpModule svm = ws.getObject(svmId.getIdentity(), SvpModule.class, contexts);

      //  ModuleDefinition md = svm.as(ModuleDefinition.class).get();

        java.util.List<SBSvpSequenceObject<? extends SvpEntity>> instances = svm.getOrderedInstances();

        SviInstance activation = svm.getSviInstances(InteractionType.TranscriptionActivation).iterator().next();

        Assert.assertNotNull("Activation should not be null", activation);
        Assert.assertEquals(8, instances.size());
        Assert.assertTrue(instances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(3).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(4).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(5).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(6).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(7).is(FunctionalComponent.TYPE));

        Assert.assertTrue("Promoter 1 and RBS 1 should be wired", svm.areWired(instances.get(0), instances.get(1)));
        Assert.assertTrue("RBS 1 and CDS 1 should be wired", svm.areWired(instances.get(1), instances.get(2)));
        Assert.assertTrue("CDS 1 and Terminator 1 should not be wired", !svm.areWired(instances.get(2), instances.get(3)));
        Assert.assertTrue("CDS 1 and Activation should be wired", svm.areWired(instances.get(2), activation));
        Assert.assertTrue("Promoter 2 and Activation should be wired", svm.areWired(instances.get(4), activation));
        Assert.assertTrue("Activation and RBS 2 should be wired", svm.areWired(activation, instances.get(5)));
        Assert.assertTrue("RBS 2 and CDS 2 should be wired", svm.areWired(instances.get(5), instances.get(6)));
        Assert.assertTrue("CDS 2 and Terminator 2 should not be wired", !svm.areWired(instances.get(6), instances.get(7)));
        Assert.assertTrue("Promoter 2 and RBS 2 should not be wired", !svm.areWired(instances.get(4), instances.get(5)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, contexts);

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, svm);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-manual-activate-promoter");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }

    @Test
    public void testRepressByOperator() {

        LOGGER.debug("================== Test Regulation ==================");

        SBIdentityFactory f = ws.getIdentityFactory();

        // Create Identities

        SBIdentity svmId = f.getIdentity("http://www.synbad.org", "svm1", "1.0");
        SBIdentity promoter1Id = f.getIdentity("http://www.synbad.org", "promoter1", "1.0");
        SBIdentity rbs1Id = f.getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Id = f.getIdentity("http://www.synbad.org", "cds1", "1.0");
        SBIdentity terminator1Id = f.getIdentity("http://www.synbad.org", "terminator1", "1.0");

        SBIdentity promoter2Id = f.getIdentity("http://www.synbad.org", "promoter2", "1.0");
        SBIdentity operator2Id = f.getIdentity("http://www.synbad.org", "operator2", "1.0");
        SBIdentity rbs2Id = f.getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Id = f.getIdentity("http://www.synbad.org", "cds2", "1.0");
        SBIdentity terminator2Id = f.getIdentity("http://www.synbad.org", "terminator2", "1.0");

        SBIdentity cds_represses_operator = f.getIdentity("http://www.synbad.org", "cds_represses_operator", "1.0");

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        b.createPromoter(promoter1Id, 1.0);
        b.createRBS(rbs1Id, 0.5, 1.0);
        b.createCDS(cds1Id, 1.0);
        b.createTerminator(terminator1Id);
        b.createPromoter(promoter2Id, 1.0);
        b.createOperator(operator2Id);
        b.createRBS(rbs2Id, 0.5, 1.0);
        b.createCDS(cds2Id, 1.0);
        b.createTerminator(terminator2Id);
        b.createSvm(svmId);

        b.addSvp(svmId, promoter1Id, null);
        b.addSvp(svmId, rbs1Id, null);
        b.addSvp(svmId, cds1Id, null);
        b.addSvp(svmId, terminator1Id, null);
        b.addSvp(svmId, promoter2Id, null);
        b.addSvp(svmId, operator2Id, null);
        b.addSvp(svmId, rbs2Id, null);
        b.addSvp(svmId, cds2Id, null);
        b.addSvp(svmId, terminator2Id, null);
        b.asInteractionBuilder().createRepressByOperator(cds_represses_operator,
                ComponentType.Protein, SynBadPortState.Default, cds1Id.getIdentity(), operator2Id.getIdentity(), 0.23, 1.0);

        ws.perform(b.build());

        SvpModule svm = ws.getObject(svmId.getIdentity(), SvpModule.class, contexts);

//        ModuleDefinition md = svm.as(ModuleDefinition.class).get();

        java.util.List<SBSvpSequenceObject<? extends SvpEntity>> instances = svm.getOrderedInstances();

        SviInstance repression = svm.getSviInstances(InteractionType.TranscriptionalRepressionUsingOperator).iterator().next();

        Assert.assertNotNull("Repression should not be null", repression);
        Assert.assertEquals(9, instances.size());
        Assert.assertTrue(instances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(3).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(4).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(5).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(6).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(7).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(8).is(FunctionalComponent.TYPE));

        Assert.assertTrue("Promoter 1 and RBS 1 should be wired", svm.areWired(instances.get(0), instances.get(1)));
        Assert.assertTrue("RBS 1 and CDS 1 should be wired", svm.areWired(instances.get(1), instances.get(2)));
        Assert.assertTrue("CDS 1 and Terminator 1 should not be wired", !svm.areWired(instances.get(2), instances.get(3)));
        Assert.assertTrue("CDS 1 and Repression should be wired", svm.areWired(instances.get(2), repression));
        Assert.assertTrue("Promoter 2 and Operator 2 should be wired", svm.areWired(instances.get(4), instances.get(5)));
        Assert.assertTrue("Operator 2 and Repression should be wired", svm.areWired(instances.get(5), repression));
        Assert.assertTrue("Repression and RBS 2 should be wired", svm.areWired(repression, instances.get(6)));
        Assert.assertTrue("RBS 2 and CDS 2 should be wired", svm.areWired(instances.get(6), instances.get(7)));
        Assert.assertTrue("CDS 2 and Terminator 2 should not be wired", !svm.areWired(instances.get(7), instances.get(8)));
        Assert.assertTrue("Promoter 2 and RBS 2 should not be wired", !svm.areWired(instances.get(4), instances.get(6)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, contexts);

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, svm);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-manual-repress-by-operator");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }

    @Test
    public void testRegulationWithDuplicatePromoter() {

        LOGGER.debug("================== Test Regulation with Duplicate Promoters ==================");

        SBIdentityFactory f = ws.getIdentityFactory();

        // Create Identities

        SBIdentity svmId = f.getIdentity("http://www.synbad.org", "svm1", "1.0");
        SBIdentity promoter1Id = f.getIdentity("http://www.synbad.org", "promoter1", "1.0");
        SBIdentity rbs1Id = f.getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Id = f.getIdentity("http://www.synbad.org", "cds1", "1.0");
        SBIdentity terminator1Id = f.getIdentity("http://www.synbad.org", "terminator1", "1.0");
        SBIdentity rbs2Id = f.getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Id = f.getIdentity("http://www.synbad.org", "cds2", "1.0");
        SBIdentity terminator2Id = f.getIdentity("http://www.synbad.org", "terminator2", "1.0");
        SBIdentity cds_activates_promotoer = f.getIdentity("http://www.synbad.org", "cds_activate_promoter", "1.0");

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        b.createPromoter(promoter1Id, 1.0);
        b.createRBS(rbs1Id, 0.5, 1.0);
        b.createCDS(cds1Id, 1.0);
        b.createTerminator(terminator1Id);
        b.createRBS(rbs2Id, 0.5, 1.0);
        b.createCDS(cds2Id, 1.0);
        b.createTerminator(terminator2Id);
        b.createSvm(svmId);
        b.addSvp(svmId, promoter1Id, null);
        b.addSvp(svmId, rbs1Id, null);
        b.addSvp(svmId, cds1Id, null);
        b.addSvp(svmId, terminator1Id, null);
        b.addSvp(svmId, promoter1Id, null);
        b.addSvp(svmId, rbs2Id, null);
        b.addSvp(svmId, cds2Id, null);
        b.addSvp(svmId, terminator2Id, null);
        b.asInteractionBuilder().createActivateByPromoter(cds_activates_promotoer,
                ComponentType.Protein, SynBadPortState.Default, cds1Id.getIdentity(), promoter1Id.getIdentity(), 0.23, 1.0);

        ws.perform(b.build());

        SvpModule svm = ws.getObject(svmId.getIdentity(), SvpModule.class, contexts);

        List<SBSvpSequenceObject<? extends SvpEntity>> instances = svm.getOrderedInstances();
        Set<SviInstance> activation = svm.getSviInstances().stream()
                .filter(i -> i.getDefinition().map(d -> !d.isInternal()).orElse(Boolean.FALSE))
                .collect(Collectors.toSet());

        Assert.assertEquals("Should be two activations", 2, activation.size());
        Assert.assertEquals(8, instances.size());
        Assert.assertTrue(instances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(3).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(4).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(5).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(6).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(7).is(FunctionalComponent.TYPE));

        Assert.assertTrue("RBS 1 and CDS 1 should be wired", svm.areWired(instances.get(1), instances.get(2)));
        Assert.assertTrue("CDS 1 and Terminator 1 should not be wired", !svm.areWired(instances.get(2), instances.get(3)));
        Assert.assertTrue("CDS 1 and Activation should be wired", activation.stream().anyMatch(a -> svm.areWired(instances.get(2), a)));
        Assert.assertTrue("Promoter 1 and Activation should be wired", activation.stream().anyMatch(a -> svm.areWired(instances.get(0), a)));
        Assert.assertTrue("Promoter 2 and Activation should be wired",  activation.stream().anyMatch(a -> svm.areWired(instances.get(4), a)));
        Assert.assertTrue("Activation and RBS 1 should be wired",  activation.stream().anyMatch(a -> svm.areWired(a, instances.get(1))));
        Assert.assertTrue("Activation and RBS 2 should be wired",  activation.stream().anyMatch(a -> svm.areWired( a, instances.get(5))));
        Assert.assertTrue("RBS 2 and CDS 2 should be wired", svm.areWired(instances.get(5), instances.get(6)));
        Assert.assertTrue("CDS 2 and Activation should not be wired",  !activation.stream().anyMatch(a -> svm.areWired(instances.get(6), a)));
        Assert.assertTrue("CDS 2 and Terminator 2 should not be wired", !svm.areWired(instances.get(6), instances.get(7)));

        // TO DO - FIX

        Assert.assertTrue("Promoter 1 and RBS 1 should not be wired", !svm.areWired(instances.get(0), instances.get(1)));
        Assert.assertTrue("Promoter 2 and RBS 2 should not be wired", !svm.areWired(instances.get(4), instances.get(5)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, contexts);

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, svm);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-manual-double-promoter-regulation");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }

    @Test
    public void testRegulationWithDuplicateCds() {

        LOGGER.debug("================== Test Regulation with Duplicate CDS ==================");


        SBIdentityFactory f = ws.getIdentityFactory();

        // Create Identities

        SBIdentity svmId = f.getIdentity("http://www.synbad.org", "svm1", "1.0");
        SBIdentity promoter1Id = f.getIdentity("http://www.synbad.org", "promoter1", "1.0");
        SBIdentity rbs1aId = f.getIdentity("http://www.synbad.org", "rbs1a", "1.0");
        SBIdentity rbs1bId = f.getIdentity("http://www.synbad.org", "rbs1b", "1.0");
        SBIdentity cds1Id = f.getIdentity("http://www.synbad.org", "cds1", "1.0");
        SBIdentity terminator1Id = f.getIdentity("http://www.synbad.org", "terminator1", "1.0");

        SBIdentity promoter2Id = f.getIdentity("http://www.synbad.org", "promoter2", "1.0");
        SBIdentity rbs2Id = f.getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Id = f.getIdentity("http://www.synbad.org", "cds2", "1.0");
        SBIdentity terminator2Id = f.getIdentity("http://www.synbad.org", "terminator2", "1.0");

        SBIdentity cds_activates_promoter = f.getIdentity("http://www.synbad.org", "cds_activate_promoter", "1.0");

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        b.createPromoter(promoter1Id, 1.0);
        b.createRBS(rbs1aId, 0.5, 1.0);
        b.createCDS(cds1Id, 1.0);
        b.createRBS(rbs1bId, 0.5, 1.0);
        b.createTerminator(terminator1Id);
        b.createPromoter(promoter2Id, 1.0);
        b.createRBS(rbs2Id, 0.5, 1.0);
        b.createCDS(cds2Id, 1.0);
        b.createTerminator(terminator2Id);
        b.createSvm(svmId);

        b.addSvp(svmId, promoter1Id, null);
        b.addSvp(svmId, rbs1aId, null);
        b.addSvp(svmId, cds1Id, null);
        b.addSvp(svmId, rbs1bId, null);
        b.addSvp(svmId, cds1Id, null);
        b.addSvp(svmId, terminator1Id, null);
        b.addSvp(svmId, promoter2Id, null);
        b.addSvp(svmId, rbs2Id, null);
        b.addSvp(svmId, cds2Id, null);
        b.addSvp(svmId, terminator2Id, null);
        b.asInteractionBuilder().createActivateByPromoter(cds_activates_promoter,
                ComponentType.Protein, SynBadPortState.Default, cds1Id.getIdentity(), promoter2Id.getIdentity(), 0.23, 1.0);

        ws.perform(b.build());
        SvpModule svm = ws.getObject(svmId.getIdentity(), SvpModule.class, contexts);

        List<SBSvpSequenceObject<? extends SvpEntity>> instances = svm.getOrderedInstances();
        SviInstance activation = svm.as(ModuleDefinition.class).flatMap(md ->
                md.getInteractions().stream()
                        .map(i -> i.as(SviInstance.class))
                        .filter(i -> i.isPresent()).map(i -> i.get())
                        .filter(i -> !i.getDefinition().map(d -> d.isInternal()).orElse(Boolean.FALSE))
                        .findFirst()).orElse(null);

        Assert.assertNotNull("Activation should not be null", activation);
        Assert.assertEquals("There should be ten instances", 10, instances.size());
        Assert.assertTrue(instances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(3).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(4).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(5).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(6).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(7).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(8).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(9).is(FunctionalComponent.TYPE));

        Assert.assertTrue("Promoter 1 and RBS 1 should be wired", svm.areWired(instances.get(0), instances.get(1)));
        Assert.assertTrue("Promoter 1 and RBS 2 should be wired", svm.areWired(instances.get(0), instances.get(3)));
        //Assert.assertTrue("Promoter 1 and Activation should be wired", svm.areWired(instances.get(0), activation));

        Assert.assertTrue("RBS 1 and CDS 1 should be wired", svm.areWired(instances.get(1), instances.get(2)));
        Assert.assertTrue("RBS 2 and CDS 2 should be wired", svm.areWired(instances.get(3), instances.get(4)));

        Assert.assertTrue("CDS 1 and Activation should be wired", svm.areWired(instances.get(2), activation));
        Assert.assertTrue("CDS 2 and Activation should be wired", svm.areWired(instances.get(4), activation));

        Assert.assertTrue("RBS 1 and CDS 2 should not be wired", !svm.areWired(instances.get(1), instances.get(4)));
        Assert.assertTrue("CDS 2 and Terminator 1 should not be wired", !svm.areWired(instances.get(4), instances.get(5)));
        Assert.assertTrue("Promoter 2 and Activation should be wired", svm.areWired(instances.get(6), activation));

        Assert.assertTrue("Activation and RBS 3 should be wired", svm.areWired(activation, instances.get(7)));
        Assert.assertTrue("RBS 3 and CDS 3 should be wired", svm.areWired(instances.get(7), instances.get(8)));
        Assert.assertTrue("CDS 3 and Activation should not be wired", !svm.areWired(instances.get(8), activation));
        Assert.assertTrue("CDS 3 and Terminator 2 should not be wired", !svm.areWired(instances.get(8), instances.get(9)));

        // TO DO - FIX

        Assert.assertTrue("Promoter 2 and RBS 2 should not be wired", !svm.areWired(instances.get(6), instances.get(7)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, contexts);

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, svm);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-manual-double-cds-regulation");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }

    }

    @Test
    public void testComplexation() {

        LOGGER.debug("================== Test Complexation  ==================");

        SBIdentityFactory f = ws.getIdentityFactory();

        // Create Identities

        SBIdentity svmId = f.getIdentity("http://www.synbad.org", "svm1", "1.0");
        SBIdentity promoter1Id = f.getIdentity("http://www.synbad.org", "promoter1", "1.0");
        SBIdentity rbs1Id = f.getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Id = f.getIdentity("http://www.synbad.org", "cds1", "1.0");
        SBIdentity terminator1Id = f.getIdentity("http://www.synbad.org", "terminator1", "1.0");

        SBIdentity promoter2Id = f.getIdentity("http://www.synbad.org", "promoter2", "1.0");
        SBIdentity rbs2Id = f.getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Id = f.getIdentity("http://www.synbad.org", "cds2", "1.0");
        SBIdentity terminator2Id = f.getIdentity("http://www.synbad.org", "terminator2", "1.0");

        SBIdentity complexId = f.getIdentity("http://www.synbad.org", "cds1_binds_cds2", "1.0");
        SBIdentity cds_activates_promotoer = f.getIdentity("http://www.synbad.org", "complex_activate_promoter", "1.0");

        SBSvpBuilder b = new SvpActionBuilderImpl(ws, contexts);

        b.createPromoter(promoter1Id, 1.0);
        b.createRBS(rbs1Id, 0.5, 1.0);
        b.createCDS(cds1Id, 1.0);
        b.createTerminator(terminator1Id);
        b.createPromoter(promoter2Id, 1.0);
        b.createRBS(rbs2Id, 0.5, 1.0);
        b.createCDS(cds2Id, 1.0);
        b.createTerminator(terminator2Id);
        b.createSvm(svmId);

        b.addSvp(svmId, promoter1Id, null);
        b.addSvp(svmId, rbs1Id, null);
        b.addSvp(svmId, cds1Id, null);
        b.addSvp(svmId, terminator1Id, null);
        b.addSvp(svmId, promoter2Id, null);
        b.addSvp(svmId, rbs2Id, null);
        b.addSvp(svmId, cds2Id, null);
        b.addSvp(svmId, terminator2Id, null);

        b = b.asInteractionBuilder().createComplexFormation(complexId)
                .setKd(0.2132)
                .setKf(0.2342)
                .setParticipant1(cds1Id)
                .setState1(SynBadPortState.Default)
                .setType1(ComponentType.Protein)
                .setParticipant2(cds2Id)
                .setState2(SynBadPortState.Default)
                .setType2(ComponentType.Protein).builder();

        b = b.asInteractionBuilder().createActivateByPromoter(cds_activates_promotoer,
                ComponentType.Complex, SynBadPortState.Default, complexId.getIdentity(), promoter2Id.getIdentity(), 0.23, 1.0)
                .asPartBuilder();

        ws.perform(b.build());

        ComponentDefinition cd = ws.getObject(URI.create("http://www.synbad.org/cd_cds1_binds_cds2_complex/1.0"), ComponentDefinition.class, contexts);

        SvpModule svm = ws.getObject(svmId.getIdentity(), SvpModule.class, contexts);

        List<SBSvpSequenceObject<? extends SvpEntity>> instances = svm.getOrderedInstances();

        SviInstance activation = svm.getSviInstances().stream()
                .filter(i -> i.getDefinition().map(d -> d.getIdentity().equals(cds_activates_promotoer.getIdentity())).orElse(Boolean.FALSE))
                .findFirst().orElse(null);

        SviInstance binds = svm.getSviInstances().stream()
                .filter(i -> i.getDefinition().map(d -> d.getIdentity().equals(complexId.getIdentity())).orElse(Boolean.FALSE))
                .findFirst().orElse(null);

        Assert.assertNotNull("Activation should not be null", activation);
        Assert.assertEquals(8, instances.size());
        Assert.assertTrue(instances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(3).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(4).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(5).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(6).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(7).is(FunctionalComponent.TYPE));

        Assert.assertTrue("Promoter 1 and RBS 1 should be wired", svm.areWired(instances.get(0), instances.get(1)));
        Assert.assertTrue("RBS 1 and CDS 1 should be wired", svm.areWired(instances.get(1), instances.get(2)));
        Assert.assertTrue("CDS 1 and Terminator 1 should not be wired", !svm.areWired(instances.get(2), instances.get(3)));
        Assert.assertTrue("CDS 1 and Binding Interaction should be wired", svm.areWired(instances.get(2), binds));
        Assert.assertTrue("Promoter 2 and Activation should be wired", svm.areWired(instances.get(4), activation));
        Assert.assertTrue("Activation and RBS 2 should be wired", svm.areWired(activation, instances.get(5)));
        Assert.assertTrue("RBS 2 and CDS 2 should be wired", svm.areWired(instances.get(5), instances.get(6)));
        Assert.assertTrue("CDS 2 and Terminator 2 should not be wired", !svm.areWired(instances.get(6), instances.get(7)));
        Assert.assertTrue("CDS 2 and Binding Interaction should be wired", svm.areWired(instances.get(6), binds));
        Assert.assertTrue("Promoter 2 and RBS 2 should not be wired", !svm.areWired(instances.get(4), instances.get(5)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, contexts);

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, svm);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-manual-complexation");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }

    @Test
    public void testSmlMolBinding() throws SBIdentityException {

        SBIdentity complexId = SBIdentity.getIdentity(URI.create("http://synbad.org/activated_proteinB/1.0"));
        SBIdentity generatorId = SBIdentity.getIdentity(URI.create("http://synbad.org/gen/1.0"));
        SBIdentity sinkId = SBIdentity.getIdentity(URI.create("http://synbad.org/sink/1.0"));
        SBSvpBuilder b = new CelloActionBuilder(ws,  contexts)
         .createGenerator(generatorId)
            .setPromoterIdentity(promoter)
            .builder()
        .createSink(sinkId)
            .setCdsIdentity(cds)
            .addComplexationSmallMolecule(small_molecule, complexId, 0.4, 0.4, 0.4)
            .builder()
        .getAs(SBSvpBuilder.class)
        .asInteractionBuilder()
            .createActivateByPromoter(cds_activates_promoter, ComponentType.Complex, SynBadPortState.Default,
                    complexId.getIdentity(), promoter.getIdentity(), 0.5, 1.0)
        .asPartBuilder()
        .createSvm(rootIdentity)
        .addSvm(rootIdentity, generatorId, null)
        .addSvm(rootIdentity, sinkId, null);

//        ws.getDispatcher().subscribe(event ->
//                SBGraphEvent.class.isAssignableFrom(event.getClass()) &&
//                ((SBGraphEvent)event).getGraphEntityType() == SBGraphEntityType.NODE,
//            System.out::println);
        ws.perform(b.build());

        SvpModule module = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, contexts);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, module);

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, contexts);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);

            ListOf<Species> species = doc.getModel().getListOfSpecies();

            Assert.assertEquals(
                    "SBML model should contain " + cds.getDisplayID(),
                    1,
                    species.stream().filter(s -> s.getId().equals(cds.getDisplayID())).count());

            Assert.assertEquals(
                    "SBML model should contain " + complexId.getDisplayID(),
                    1,
                    species.stream().filter(s -> s.getId().equals(complexId.getDisplayID())).count());

            Assert.assertEquals(
                    "SBML model should contain " + small_molecule.getDisplayID(),
                    1,
                    species.stream().filter(s -> s.getId().equals(small_molecule.getDisplayID())).count());

            //System.out.println(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-inducer-test");
            simulateModel(new SBMLHandler().GetSBML(doc));
        } catch (Exception e) {
            //LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }

    }

    
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testAddandRemoveActivationProxies() throws SBIdentityException {

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, contexts)
            .createSvm(uberRootIdentity)
            .createSvm(rootIdentity)
            .createPromoter(promoter, 0.2)
            .createCDS(cds, 0.2)
            .addSvm(uberRootIdentity, rootIdentity, null)
            .addSvp(rootIdentity, promoter, null)
            .addSvp(rootIdentity, cds, null);
        ws.perform(b.build());

        
        SvpModule uberRootSvm = ws.getObject(uberRootIdentity.getIdentity(), SvpModule.class, contexts);
        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, contexts);

        ModuleDefinition rootMd = ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, contexts);
        
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        Svp promoterSvp = ws.getObject(promoter.getIdentity(), Svp.class, contexts);
        Svp cdsSvp = ws.getObject(cds.getIdentity(), Svp.class, contexts);

        LOGGER.trace("--- Adding proxies  ---");
        
        SvpActionBuilderImpl b2 = new SvpActionBuilderImpl(ws, contexts);
        
        for(SBPort port : promoterSvp.getPorts(SBPortDirection.OUT)) {
            b2.createProxyPort(
                    SvpIdentityHelper.getProxyPortIdentity(rootIdentity, ws.getIdentityFactory().getIdentity(port.getIdentity())),
                    port.getIdentity(), 
                    rootSvm.getIdentity(), 
                    URI.create(SvpModule.TYPE));
        }
        
        for(SBPort port : cdsSvp.getPorts(SBPortDirection.OUT)) {
            b2.createProxyPort(
                    SvpIdentityHelper.getProxyPortIdentity(rootIdentity, ws.getIdentityFactory().getIdentity(port.getIdentity())),
                    port.getIdentity(), 
                    rootSvm.getIdentity(), 
                    URI.create(SvpModule.TYPE));
        }
        
        ws.perform(b2.build());
        
        //assert(rootMd.getSviInstances().isEmpty());
       
        int functionalComponentsBefore =  rootMd.getFunctionalComponents().size();

        LOGGER.trace("--- Adding interaction  ---");
        
        b = new SvpActionBuilderImpl(ws, contexts)
            .createActivateByPromoter(cds_activates_promoter, ComponentType.Protein, SynBadPortState.Default, cds.getIdentity(), promoter.getIdentity(), 0.42, 1.0);
        ws.perform(b.build());
        
        SvpInteraction svi = ws.getObject(cds_activates_promoter.getIdentity(), SvpInteraction.class, contexts);
        int functionalComponentsAfter =  rootMd.getFunctionalComponents().size();

        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.debug(" I: {}",  i.getName()));
        
        Assert.assertEquals("No functional components should have been added", functionalComponentsBefore, functionalComponentsAfter);
        Assert.assertTrue("Module definition should contain generated interaction", 
                rootMd.getInteractions().stream()
                        .anyMatch(i -> svi.getExtensions().stream()
                                .filter(in -> in.getParent().getIdentity().equals(rootMd.getIdentity()))
                                .allMatch(e -> i.getIdentity().equals(e.getIdentity()))));

        LOGGER.trace("-------- Removing CDS ---------");
        
        b2 = new SvpActionBuilderImpl(ws, contexts)
                .removeChild(rootSvm.getIdentity(), 1);

        ws.perform(b2.build());
        
        int functionalComponentsAfterRemove =  rootMd.getFunctionalComponents().size();

        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        Assert.assertEquals("CDS DNA and protein should have been removed", functionalComponentsAfter - 2, functionalComponentsAfterRemove);
        Assert.assertFalse("Interaction should have been removed", rootMd.getInteractions().stream().anyMatch(i -> svi.getExtensions().stream().allMatch(e -> i.getIdentity().equals(e.getIdentity()))));

    }

    @Test
    public void testAddandRemovePromoterActivation() {

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, contexts)
            .createSvm(rootIdentity)
            .createPromoter(promoter, 0.2)
            .createCDS(cds, 0.2)
            .addSvp(rootIdentity, promoter, null)
            .addSvp(rootIdentity, cds, null);
        ws.perform(b.build());

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, contexts);
        ModuleDefinition rootMd = ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, contexts);
        
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        Svp promoterSvp = ws.getObject(promoter.getIdentity(), Svp.class, contexts);
        Svp cdsSvp = ws.getObject(cds.getIdentity(), Svp.class, contexts);

        int functionalComponentsBefore =  rootMd.getFunctionalComponents().size();

        LOGGER.trace("--- Adding interaction  ---");
        
        b = new SvpActionBuilderImpl(ws, contexts)
            .createActivateByPromoter(cds_activates_promoter, ComponentType.Protein, SynBadPortState.Default, cds.getIdentity(), promoter.getIdentity(), 0.42, 1.0);
        ws.perform(b.build());
        
        rootMd = ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, contexts);
        
        
        SvpInteraction svi = ws.getObject(cds_activates_promoter.getIdentity(), SvpInteraction.class, contexts);
        int functionalComponentsAfter =  rootMd.getFunctionalComponents().size();

        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        Assert.assertEquals("No functional components should have been added", functionalComponentsBefore, functionalComponentsAfter);
        Assert.assertTrue("Module definition should contain generated interaction", rootMd.getInteractions().stream().anyMatch(i -> svi.getExtensions().stream().allMatch(e -> i.getIdentity().equals(e.getIdentity()))));

        LOGGER.trace("-------- Removing CDS ---------");
        
        SvpActionBuilderImpl b2 = new SvpActionBuilderImpl(ws, contexts)
                .removeChild(rootSvm.getIdentity(), 1);

        ws.perform(b2.build());
        
        rootMd = ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, contexts);
        
        int functionalComponentsAfterRemove =  rootMd.getFunctionalComponents().size();
        
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        Assert.assertEquals("CDS DNA and protein should have been removed", functionalComponentsAfter - 2, functionalComponentsAfterRemove);
        Assert.assertFalse("Interaction should have been removed", rootMd.getInteractions().stream().anyMatch(i -> svi.getExtensions().stream().allMatch(e -> i.getIdentity().equals(e.getIdentity()))));
    }
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testAddTwoandRemoveOneCDS() throws SBIdentityException {

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, contexts)
            .createSvm(rootIdentity)
            .createPromoter(promoter, 0.2)
            .createCDS(cds, 0.2)
            .addSvp(rootIdentity, promoter, null)
            .addSvp(rootIdentity, cds, null)
            .addSvp(rootIdentity, cds, null);
        ws.perform(b.build());

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, contexts);
        Svp promoterSvp = ws.getObject(promoter.getIdentity(), Svp.class, contexts);
        Svp cdsSvp = ws.getObject(cds.getIdentity(), Svp.class, contexts);
        ModuleDefinition rootMd = ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, contexts);
        
        int functionalComponentsBeforeInteraction =  rootMd.getFunctionalComponents().size();
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        
        LOGGER.trace("--- Adding interaction  ---");
        
        b = new SvpActionBuilderImpl(ws, contexts)
            .createActivateByPromoter(cds_activates_promoter, ComponentType.Protein, SynBadPortState.Default, cds.getIdentity(), promoter.getIdentity(), 0.42, 1.0);
        ws.perform(b.build());
        
        SvpInteraction svi = ws.getObject(cds_activates_promoter.getIdentity(), SvpInteraction.class, contexts);
        int functionalComponentsAfterInteraction =  rootMd.getFunctionalComponents().size();
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        // Should already have a proptein FC added due to the protein production interactions
        
        Assert.assertEquals("No functional components should have been added", functionalComponentsBeforeInteraction, functionalComponentsAfterInteraction);

        // Interaction should have been added for the activation SVI
        
        boolean hasActivation = !svi.getExtensions().isEmpty();
        
        Assert.assertTrue("Module definition should contain generated interaction", hasActivation);

        //rootMd.getSvpInstances().stream().forEach(fc -> LOGGER.trace("1FC: {}",  fc.getName()));
        
        LOGGER.trace("--- Removing first CDS ---");
         
        SvpActionBuilderImpl b2 = new SvpActionBuilderImpl(ws, contexts)
                .removeChild(rootSvm.getIdentity(), 1);

        ws.perform(b2.build());
   
        int functionalComponentsAfterFirstRemoval =  rootMd.getFunctionalComponents().size();
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        //rootMd.getSvpInstances().stream().forEach(fc -> LOGGER.trace("2FC: {}",  fc.getName()));
        
        // The DNA FC should have been removed, but not the protein, as there 
        // is another protein production interaction
        
        Assert.assertEquals("CDS DNA but not protein should have been removed", functionalComponentsAfterInteraction - 1, functionalComponentsAfterFirstRemoval);
        
        // As there is still a protein, the activation interaction should still be present
        
        hasActivation = !svi.getExtensions().isEmpty();
        
        Assert.assertTrue("Interaction should not have been removed", hasActivation);

        LOGGER.trace("--- Removing second CDS ---");
        
         b2 = new SvpActionBuilderImpl(ws, contexts)
                .removeChild(rootSvm.getIdentity(), 1);

        ws.perform(b2.build());
        
        int functionalComponentsAfterSecondRemove =  rootMd.getFunctionalComponents().size();
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(i -> LOGGER.trace(" I: {}",  i.getName()));
        
        //rootMd.getSvpInstances().stream().forEach(fc -> LOGGER.trace("3FC: {}",  fc.getName()));
        
        Assert.assertEquals("CDS DNA and protein should have been removed", functionalComponentsAfterFirstRemoval - 2, functionalComponentsAfterSecondRemove);
        
        hasActivation = !svi.getExtensions().isEmpty();
        
        Assert.assertFalse("Interaction should have been removed", hasActivation );
    }
    
    
    /**
     * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testAddPhosphorylation() throws SBIdentityException {
  
        SvpActionBuilderImpl b = new SvpActionBuilderImpl(ws, contexts)
            .createSvm(rootIdentity)
            .createCDSWithPhosphorylatingSmallMolecule(phosphorylator, small_molecule, 0.2, 0.2, 0.2, 0.2)
            .createCDS(cds, 0.3)
            .addSvp(rootIdentity, phosphorylator, null)
            .addSvp(rootIdentity, cds, null);
        ws.perform(b.build());
       
        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, contexts);
        Svp phosphorylatorSvp = ws.getObject(phosphorylator.getIdentity(), Svp.class, contexts);
        Svp cdsSvp = ws.getObject(cds.getIdentity(), Svp.class, contexts);
        ModuleDefinition rootMd = ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, contexts);
        
        int functionalComponentsBeforePhosphorylation =  rootMd.getFunctionalComponents().size();

        LOGGER.trace("--- Adding phosphorylation ---");
        
        b = new SvpActionBuilderImpl(ws, contexts)
            .createPhosphorylate(phosphorylator_phosphorylates_cds, phosphorylator.getIdentity(), cds.getIdentity(), 0.5, 0.5, 0.5);
        ws.perform(b.build());
        
        SvpInteraction svi = ws.getObject(phosphorylator_phosphorylates_cds.getIdentity(), SvpInteraction.class, contexts);
        
        int functionalComponentsAfterPhosphorylation =  rootMd.getFunctionalComponents().size();
 
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        
        boolean hasPhosphorylation = !svi.getExtensions().isEmpty();
        
        Assert.assertTrue("Module definition should contain generated interaction", hasPhosphorylation);      
        Assert.assertEquals("Functional component for phosphorylated protein should have been added", functionalComponentsBeforePhosphorylation + 1, functionalComponentsAfterPhosphorylation);
        
        LOGGER.trace("--- Removing phosphorylated CDS ---");
        
        SvpActionBuilderImpl b2 = new SvpActionBuilderImpl(ws, contexts)
                .removeChild(rootSvm.getIdentity(), 1);
        ws.perform(b2.build());

        int functionalComponentsAfterRemove =  rootMd.getFunctionalComponents().size();
 
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
      
        Assert.assertEquals("Functional components for cds, protein and protein_p should have been removed", functionalComponentsAfterPhosphorylation - 3, functionalComponentsAfterRemove);
        Assert.assertFalse("Interaction should have been removed", rootMd.getInteractions().stream().anyMatch(i -> svi.getExtensions().contains(i)));

        LOGGER.trace("--- Removing phosphorylator CDS ---");
        
        b2 = new SvpActionBuilderImpl(ws, contexts)
                .removeChild(rootSvm.getIdentity(), 0);
        ws.perform(b2.build());
        
        int functionalComponentsAfterSecondRemove =  rootMd.getFunctionalComponents().size();
 
        rootMd.getFunctionalComponents().stream().forEach(fc -> LOGGER.trace("FC: {}",  fc.getName()));
        rootMd.getInteractions().stream().forEach(fc -> LOGGER.trace(" I: {}",  fc.getName()));
      
        Assert.assertEquals("Functional components for smlMol, DNA, protein and protein_p should have been removed", functionalComponentsAfterRemove - 4, functionalComponentsAfterSecondRemove);
    }


    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testANDNRegulation() throws SBIdentityException {

        SBSviBuilder b = new SvpActionBuilderImpl(ws,  contexts);

        // Create identities

        SBIdentity svmIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svm", "1.0");
        SBIdentity promoterIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "promoter", "1.0");
        SBIdentity rbs1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "codingSequence1", "1.0");
        SBIdentity rbs2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "codingSequence2", "1.0");
        SBIdentity smlMolIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "smlMol", "1.0");
        SBIdentity actIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "andn_activation", "1.0");

        // Build and perform actions

        b = b.asPartBuilder()
                .createSvm(svmIdentity)
                .createPromoter(promoterIdentity,  1.0)
                .createRBS(rbs1Identity, 0.123, 1.0)
                .createCDS(cds1Identity, 0.0012)
                .createRBS(rbs2Identity, 0.142, 1.0)
                .createCDS(cds2Identity, 0.0012)
                .addSvp(svmIdentity, promoterIdentity, null)
                .addSvp(svmIdentity, rbs1Identity, null)
                .addSvp(svmIdentity, cds1Identity, null)
                .addSvp(svmIdentity, rbs2Identity, null)
                .addSvp(svmIdentity, cds2Identity, null)
                .asInteractionBuilder()

                // Produces POPS if 1, 1+2, not none or 2 alone (pSinI - activated by Spo0A and repressed by SinR Tetramer)

                .createActivateANDNByPromoter(actIdentity, promoterIdentity.getIdentity(),
                        ComponentType.Protein, SynBadPortState.Default, cds1Identity.getIdentity(),
                        ComponentType.Protein, SynBadPortState.Default, cds2Identity.getIdentity(), 0.213, 0.32, 0.231, 0.23);

        SBAction a = b.build();
        ws.perform(a);

        SbmlWriter writer = new SbmlWriter(ws, contexts);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, ws.getObject(svmIdentity.getIdentity(), SvpModule.class, contexts));

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-andn-regulation");
            simulateModel(new SBMLHandler().GetSBML(doc));
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }
    
    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testPromoterActivationPhosphorylated() throws SBIdentityException {
        
        SBSviBuilder b = new SvpActionBuilderImpl(ws,  contexts);

        SBIdentity svmIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svm", "1.0");
        SBIdentity promoterIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "promoter", "1.0");
        SBIdentity cdsIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "codingSequence", "1.0");
        SBIdentity smlMolIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "smlMol", "1.0");
        SBIdentity actIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "activation", "1.0");

        b = b.asPartBuilder().createSvm(svmIdentity)
                .createPromoter(promoterIdentity,  1.0)
                .addSvp(svmIdentity, promoterIdentity, null)
                .createCDSWithPhosphorylatingSmallMolecule(cdsIdentity, smlMolIdentity, 0.3, 0.34, 0.2, 0.5)
                .addSvp(svmIdentity, cdsIdentity, null).asInteractionBuilder()
                .createActivateByPromoter(actIdentity, 
                        ComponentType.Protein, SynBadPortState.Phosphorylated, 
                        cdsIdentity.getIdentity(), promoterIdentity.getIdentity(), 0.213, 0.32);

        SBAction a = b.build();
        ws.perform(a);
    }
    
    
    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testCreatePhosphorylation() throws SBIdentityException {
        
         SBSvpBuilder b = new SvpActionBuilderImpl(ws,  contexts);

        // Create identities
        
        SBIdentity svmIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svm", "1.0"); 
        SBIdentity cds1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_1", "1.0");
        SBIdentity cds2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_2", "1.0");
        SBIdentity smlMolIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "smlMol", "1.0");
        SBIdentity phosIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "phosphorylation", "1.0");
    
        // Build and perform actions
        
        b = b.createSvm(svmIdentity)         
                .createCDSWithPhosphorylatingSmallMolecule(cds1Identity, smlMolIdentity, 0.412, 0.231, 0.42, 0.32)
                .addSvp(svmIdentity, cds1Identity, null)              
                .createCDS(cds2Identity, 0.123)
                .addSvp(svmIdentity, cds2Identity, null).asInteractionBuilder()
                .createPhosphorylate(phosIdentity, cds1Identity.getIdentity(), cds2Identity.getIdentity(), 1.0, 0.231, 0.23)
                .asPartBuilder();
                
 
        //        logger.debug("--- Creating model ---");
        
        SBAction a = b.build();
        ws.perform(a);
    }


    public static void writeToXml(String sbml, String filename) {
        LOGGER.debug(sbml);

        try
        {
            FileWriter writer = new FileWriter("exports/" + filename + ".xml");
            writer.write(sbml);
            writer.flush();
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }

    public void renderChart(TimeCourseTraces result) {

        JPanel panel = new TimeCoursePlot(result).getPanel();

        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void simulateModel(String sbml) throws Exception{
        SBSbmlSimulator s = new SimpleCopasiSimulator(1000, 1000);
        s.setModel(sbml);
        if(s.run()) {

           // renderChart(s.getResults());
//            s.getResults().getSpecies().stream().forEach(
//                    m -> LOGGER.debug("{}: {}", m, Arrays.toString(s.getResults().getTimeCourse(m).toArray(new Double[] {})))
//            );
        } else {
            throw new Exception("Simulation of model failed");
        }
    }


    @Override
    public void onEvent(SBEvent e) {
//        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {
//            SBGraphEvent evt = (SBGraphEvent)e;
//            if(evt.getGraphEntityType() == SBGraphEntityType.EDGE) {
//                SynBadEdge edge = (SynBadEdge) evt.getData();
//                if(edge.getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasFunctionalComponent)) {
//                    System.out.println(evt.getType() + ": " + edge);
//                }
//                if(edge.getEdge().toASCIIString().equals(SynBadTerms.SynBadPart.hasInternalSpecies)) {
//                    System.out.println(evt.getType() + ": " + edge);
//                }
//            }
//        }
    }
}
