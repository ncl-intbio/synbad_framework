/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.io.FileWriter;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.parser.*;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.synbad.model.SBModelManager;

/**
 *
 * @author owengilfellon
 */
public class PartsParsingTests {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PartsParsingTests.class);

    private final URI WORKSPACE_ID = URI.create("http://www.synbad.org/sbmltest/1.0");
    private final String VPR_URL = "http://vm-vpr.bioswarm.net:8081";
    private static final String PREFIX = "http://virtualparts.org";
    private static final URI[] CONTEXTS = new URI[] {URI.create(PREFIX + "/imported")};

    private static final SVPManager VPR_MANAGER = SVPManager.getSVPManager();

    private SVPReader reader;
    private SVPInteractionReader interactionReader;
    private SVPWriter writer;
    private SVPInteractionWriter interactionWriter;


    private SBWorkspace workspace;
    private SBModelManager factory = Lookup.getDefault().lookup(SBModelManager.class);


    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(
            DefaultSBWorkspaceManagerMem.class,
            DefaultSBModelFactory.class,
            SvpModelProvider.class
        );

        workspace = new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new PiUpdater())
                .addService(new EnvConstantUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        workspace.createContext(CONTEXTS[0]);

        reader = new SVPReader(workspace, CONTEXTS);
        writer = new SVPWriter(workspace, CONTEXTS);
        interactionReader = new SVPInteractionReader(workspace, CONTEXTS);
        interactionWriter = new SVPInteractionWriter(workspace, CONTEXTS);
    }
    
    @After
    public void tearDown() {
        workspace.shutdown();
        workspace = null;
    }



    @Test
    public void testParsePromoterFromSvpWrite() throws Exception {
        LOGGER.debug("--- testParsePromoterFromSvpWrite() ---");
        String svpWrite =  "PspaRK:Prom";
        List<ICompilable> compilables =  CompilableFactory.getCompilables(svpWrite,  VPR_URL);
        Map<String, PartBundle> bundles = new HashMap<>();
        GrnTreeParser parser = new GrnTreeParser(workspace, CONTEXTS);
        List<Svp> svps = parser.parseParts(compilables, bundles);
        SBMLDocument doc = bundles.get("PspaRK").getPartDocument();
        try {
            simulateModel(new SBMLHandler().GetSBML(doc));
        } catch (Exception e) {
            LOGGER.error("Could not simulate model");
            Assert.fail("Could not simulate model");
        }
    }

    /**
     * Test of getRootNode method, of class SvpModel.
     */
    @Test
    public void testParseRbsFromSvpWrite() throws Exception {
        LOGGER.debug("--- testParseRbsFromSvpWrite() ---");
        String svpWrite =  "RBS_SpaR:RBS";
        List<ICompilable> compilables =  CompilableFactory.getCompilables(svpWrite,  VPR_URL);
        Map<String, PartBundle> bundles = new HashMap<>();
        GrnTreeParser parser = new GrnTreeParser(workspace, CONTEXTS);
        List<Svp> svps = parser.parseParts(compilables, bundles);
        SBMLDocument doc = bundles.get("RBS_SpaR").getPartDocument();
        try {
            simulateModel(new SBMLHandler().GetSBML(doc));
        } catch (Exception e) {
            LOGGER.error("Could not simulate model");
            Assert.fail("Could not simulate model");
        }
    }

    @Test
    public void testParsePromoterRbsFromSvpWrite() throws Exception {
        LOGGER.debug("--- testParsePromoterRbsFromSvpWrite() ---");
        String svpWrite =  "PspaRK:Prom; RBS_SpaR:RBS";
        List<ICompilable> compilables =  CompilableFactory.getCompilables(svpWrite,  VPR_URL);
        Map<String, PartBundle> bundles = new HashMap<>();
        GrnTreeParser parser = new GrnTreeParser(workspace, CONTEXTS);
        List<Svp> svps = parser.parseParts(compilables, bundles);
        SBIdentity id = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "root", "1.0");
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, parser.createModuleFromCompilables(id, compilables));
        SbmlWriter writer = new SbmlWriter(workspace, CONTEXTS);
        SBMLDocument doc = writer.getSbmlDocument(model);
        model.close();
         writeToXml(new SBMLHandler().GetSBML(doc), "sbml-pspark-rbs-via-synbad");
        try {
            simulateModel(new SBMLHandler().GetSBML(doc));
        } catch (Exception e) {
            LOGGER.error("Could not simulate model");
            Assert.fail("Could not simulate model");
        }
    }

    /**
     * Test of getRootNode method, of class SvpModel.
     */
    @Test
    public void testParseSubtilinReceiverFromSvpWrite() throws Exception {

        LOGGER.debug("--- testParseSubtilinReceiverFromSvpWrite() ---");

        String starting_model =  "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter";
        SBIdentity id = SBIdentity.getIdentity("http://www.synbad.org", "root", "1.0");

        // test simulate from VPR

        CompilationDirector director = new CompilationDirector(VPR_URL);
        List<ICompilable> compilables1 = CompilableFactory.getCompilables(starting_model,  VPR_URL);
        director.setModelCompilables(compilables1);

        SBMLDocument sbmlOutput1 = director.getSBML("subtilinreceiver");
        writeToXml(new SBMLHandler().GetSBML(sbmlOutput1), "sbml-subtilinreceiver-from-vpr");
        String sbmlAsString1 = new SBMLHandler().GetSBML(sbmlOutput1);
        try {
            simulateModel(sbmlAsString1);
        } catch (Exception e) {
            LOGGER.error("Could not simulate model");
            Assert.fail("Could not simulate model");
        }

        // test simulate from VPR -> SynBad

        GrnTreeParser parser = new GrnTreeParser(workspace, CONTEXTS);
        SvpModule module = parser.createModuleFromCompilables(id, compilables1);
        SvpModel model = factory.createModel(SvpModel.class, module);
        SBMLDocument sbmlOutput2  = new SbmlWriter(workspace, CONTEXTS).getSbmlDocument(model);
        String sbmlAsString2 = new SBMLHandler().GetSBML(sbmlOutput2);
        model.close();
        writeToXml(sbmlAsString2, "sbml-subtilinreceiver-via-synbad");
        try {
            simulateModel(sbmlAsString2);
        } catch (Exception e) {
            LOGGER.error("Could not simulate model");
            Assert.fail("Could not simulate model");
        }
    }
    @Test
    public void testParseVprPromoter() throws Exception {

        LOGGER.debug("--- Testing Promoter ---");

        // Retrieve part from VPR

        Part p1 = VPR_MANAGER.getConstPromoter();
        PartBundle b = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1));

        // Create Svp from VPR Part

        Svp promoter = reader.read(p1);

        // Create VPR Part from SVP

        LOGGER.debug("--- Writing converted part bundle ---");

        PartBundle pb_p1 = writer.write(promoter);

        assert (b.equals(pb_p1));

        SBIdentity promoterId = SBIdentity.getIdentity(PREFIX, "prototypePromoter", "1.0");

        workspace.perform(new SvpActionBuilderImpl(workspace, CONTEXTS)
                .createPromoter(promoterId, 0.213).build());

        Svp abstractPromoter = workspace.getObject(promoterId.getIdentity(), Svp.class, CONTEXTS);

//        SvpDebugger.printFlowObject(promoter, System.out);
//        SvpDebugger.printFlowObject(abstractPromoter, System.out);
    }

    @Test
    public void testParseVprInducPromoter() throws Exception {

        LOGGER.debug("--- Testing Inducible Promoter ---");

        // Retrieve part and interaction from VPR

        Part p1 = VPR_MANAGER.getInducPromoter();
        Part p2 = VPR_MANAGER.getInteraction(p1);
        Interaction p1_p2 = VPR_MANAGER.getInteractions(p1, p2).stream().findFirst().orElse(null);

        Map<Interaction, List<Part>> exIntMap1 = new HashMap<>();
        exIntMap1.put(p1_p2, Arrays.asList(p1, p2));

        PartBundle p1Bundle = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1), exIntMap1);
        PartBundle p2Bundle = new PartBundle(p2, VPR_MANAGER.getInternalEvents(p2), exIntMap1);

        // Create Svp and SvpInteraction from VPR Part and Interaction

        Map<String, PartBundle> interactionParts = new HashMap<>();
        interactionParts.put(p1.getName(), p1Bundle);
        interactionParts.put(p2.getName(), p2Bundle);

        Svp svp1 = reader.read(p1);
        Svp svp2 = reader.read(p2);

        SvpInteraction act = interactionReader.read(p1_p2, interactionParts);

        // Create VPR Part from SVP

        PartBundle tmpBundle1 = writer.write(svp1);
        PartBundle tmpBundle2 = writer.write(svp2);

        // Create VPR Interaction from SvpInteraction and create combined bundle

        Map<Interaction, List<Part>> extIntMap2 = new HashMap<>();
        Interaction parsedInteraction = interactionWriter.write(act);


        extIntMap2.put(parsedInteraction, Arrays.asList(tmpBundle1.getPart(), tmpBundle2.getPart()));

        LOGGER.debug("--- Writing converted part bundles ---");

        PartBundle parsedBundle1 = new PartBundle(tmpBundle1.getPart(), tmpBundle1.getInternalInteractions(), extIntMap2);
        PartBundle parsedBundle2 = new PartBundle(tmpBundle2.getPart(), tmpBundle2.getInternalInteractions(), extIntMap2);

        assert (p1Bundle.equals(parsedBundle1));
        assert (p2Bundle.equals(parsedBundle2));

//        SvpDebugger.printFlowObject(svp1, System.out);
    }

    @Test
    public void testParseVprRbs() throws Exception {

        LOGGER.debug("--- Testing RBS ---");

        // Retrieve part from VPR

        Part p1 = VPR_MANAGER.getRBS();
        PartBundle b = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1));

        // Create Svp from VPR Part

        Svp rbs = reader.read(p1);

        // Create VPR Part from SVP

        LOGGER.debug("--- Writing converted part bundle ---");

        PartBundle pb_p1 = writer.write(rbs);
        assert (b.equals(pb_p1));

        SBIdentity rbsId = SBIdentity.getIdentity(PREFIX, "prototypeRbs", "1.0");

        workspace.perform(new SvpActionBuilderImpl(workspace, CONTEXTS)
                .createRBS(rbsId, 0.213, 1.0).build());

        Svp abstractRbs = workspace.getObject(rbsId.getIdentity(), Svp.class, CONTEXTS);


//        SvpDebugger.printFlowObject(rbs, System.out);
//        SvpDebugger.printFlowObject(abstractRbs, System.out);
    }

    @Test
    public void testParseVprCds() throws Exception {

        // Retrieve part from VPR
        LOGGER.debug("--- Testing CDS ---");

        Part p1 = VPR_MANAGER.getInducPromoter();
        Part p2 = VPR_MANAGER.getInteraction(p1);
        Interaction p1_p2 = VPR_MANAGER.getInteractions(p1, p2).stream().findFirst().orElse(null);
        Map<Interaction, List<Part>> exIntMap1 = new HashMap<>();
        exIntMap1.put(p1_p2, Arrays.asList(p1, p2));
        PartBundle p1Bundle = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1), exIntMap1);
        PartBundle p2Bundle = new PartBundle(p2, VPR_MANAGER.getInternalEvents(p2), exIntMap1);

        // Create Svp from VPR Part

        Map<String, PartBundle> interactionParts = new HashMap<>();
        interactionParts.put(p1.getName(), p1Bundle);
        interactionParts.put(p2.getName(), p2Bundle);

        Svp svp1 = reader.read(p1);
        Svp svp2 = reader.read(p2);
        SvpInteraction act = interactionReader.read(p1_p2, interactionParts);

        // Create VPR Part from SVP

        PartBundle tmpBundle1 = writer.write(svp1);
        PartBundle tmpBundle2 = writer.write(svp2);

        // Create VPR Interaction from SvpInteraction and create combined bundle

        Map<Interaction, List<Part>> extIntMap2 = new HashMap<>();
        Interaction parsedInteraction = interactionWriter.write(act);
        extIntMap2.put(parsedInteraction, Arrays.asList(tmpBundle1.getPart(), tmpBundle2.getPart()));

        LOGGER.debug("--- Writing converted part bundles ---");

        PartBundle parsedBundle1 = new PartBundle(tmpBundle1.getPart(), tmpBundle1.getInternalInteractions(), extIntMap2);
        PartBundle parsedBundle2 = new PartBundle(tmpBundle2.getPart(), tmpBundle2.getInternalInteractions(), extIntMap2);

        assert (p1Bundle.equals(parsedBundle1));
        assert (p2Bundle.equals(parsedBundle2));
    }

    @Test
    public void testParseVprSinIR() throws Exception {

        // Retrieve part from VPR
        LOGGER.debug("--- Testing Protein Complex ---");

        Part p1 = VPR_MANAGER.getPart("SinIR");
        PartBundle p1Bundle = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1));

        // Create Svp from VPR Part

        Map<String, PartBundle> interactionParts = new HashMap<>();
        interactionParts.put(p1.getName(), p1Bundle);

        Svp svp1 = reader.read(p1);
        // Create VPR Part from SVP

        PartBundle tmpBundle1 = writer.write(svp1);

        // Create VPR Interaction from SvpInteraction and create combined bundle

        LOGGER.debug("--- Writing converted part bundles ---");

        PartBundle parsedBundle1 = new PartBundle(tmpBundle1.getPart(), tmpBundle1.getInternalInteractions());

        assert (p1Bundle.equals(tmpBundle1));
    }

    @Test
    public void testParseVprTerminator() throws Exception {

        // Retrieve part from VPR

        LOGGER.debug("--- Testing Terminator ---");

        Part p1 = VPR_MANAGER.getTerminator();
        PartBundle b = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1));

        // Create Svp from VPR Part

        Svp terminator = reader.read(p1);

        // Create VPR Part from SVP

        LOGGER.debug("--- Writing converted part bundle ---");

        PartBundle pb_p1 = writer.write(terminator);
        assert (b.equals( pb_p1));
    }

    @Test
    public void testParseVprOperator() throws Exception {

        LOGGER.debug("--- Testing Operator ---");

        // Retrieve part from VPR

        Part p1 = VPR_MANAGER.getNegOperator();
        Part p2 = VPR_MANAGER.getInteraction(p1);
        Interaction p1_p2 = VPR_MANAGER.getInteractions(p1, p2).stream().findFirst().orElse(null);
        Map<Interaction, List<Part>> exIntMap1 = new HashMap<>();
        exIntMap1.put(p1_p2, Arrays.asList(p1, p2));

        PartBundle p1Bundle = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1), exIntMap1);
        PartBundle p2Bundle = new PartBundle(p2, VPR_MANAGER.getInternalEvents(p2), exIntMap1);

        // Create Svp from VPR Part

        Map<String, PartBundle> interactionParts = new HashMap<>();
        interactionParts.put(p1.getName(), p1Bundle);
        interactionParts.put(p2.getName(), p2Bundle);
        Svp svp1 = reader.read(p1);
        Svp svp2 = reader.read(p2);
        SvpInteraction act = interactionReader.read(p1_p2, interactionParts);

        // Create VPR Part from SVP

        PartBundle tmpBundle1 = writer.write(svp1);
        PartBundle tmpBundle2 = writer.write(svp2);

        // Create VPR Interaction from SvpInteraction and create combined bundle

        Map<Interaction, List<Part>> extIntMap2 = new HashMap<>();
        Interaction parsedInteraction = interactionWriter.write(act);
        extIntMap2.put(parsedInteraction, Arrays.asList(tmpBundle1.getPart(), tmpBundle2.getPart()));

        LOGGER.debug("--- Writing converted part bundles ---");

        PartBundle parsedBundle1 = new PartBundle(tmpBundle1.getPart(), tmpBundle1.getInternalInteractions(), extIntMap2);
        PartBundle parsedBundle2 = new PartBundle(tmpBundle2.getPart(), tmpBundle2.getInternalInteractions(), extIntMap2);

        assert (p1Bundle.equals(parsedBundle1));
        assert (p2Bundle.equals(parsedBundle2));
    }

    @Test
    public void testParseVprShim() throws Exception {

        LOGGER.debug("--- Testing Shim ---");

        // Retrieve part from VPR

        Part p1 = VPR_MANAGER.getShim();
        PartBundle b = new PartBundle(p1, VPR_MANAGER.getInternalEvents(p1));

        // Create Svp from VPR Part

        Svp shim = reader.read(p1);

        // Create VPR Part from SVP

        LOGGER.debug("--- Writing converted part bundle ---");

        PartBundle pb_p1 = writer.write(shim);
        assert (b.equals(pb_p1));
    }

    @Test
    public void testConvert_Part() throws Exception {

        // create an SBIdentity for the SvpModule representing the TU

        SBIdentity tuId = workspace.getIdentityFactory().getIdentity(URI.create("http://www.synbad.org/tu/1.0"));
        URI mdId = URI.create("http://www.synbad.org/moduleDefinition/1.0");

        // Retrieve SVPs from repository using JParts API

        Part p1 = VPR_MANAGER.getConstPromoter();
        Part r1 = VPR_MANAGER.getRBS();
        Part c1 = VPR_MANAGER.getPart("SpaK");
        Part r2 = VPR_MANAGER.getRBS();
        Part c2 = VPR_MANAGER.getPart("SpaR");
        Part t = VPR_MANAGER.getTerminator();

        // Parse them into workspace

        Svp inducible = reader.read(p1);
        Svp rbs1 = reader.read(r1);
        Svp spak = reader.read(c1);
        Svp rbs2 = reader.read(r2);
        Svp spar = reader.read(c2);
        Svp terminator = reader.read(t);

        SBAction addChildrenAction = new SvpActionBuilderImpl(workspace, CONTEXTS)
                .createSvm(tuId)
                .addSvp(tuId, workspace.getIdentityFactory().getIdentity(inducible.getIdentity()), null)
                .addSvp(tuId, workspace.getIdentityFactory().getIdentity(rbs1.getIdentity()), null)
                .addSvp(tuId, workspace.getIdentityFactory().getIdentity(spak.getIdentity()), null)
                .addSvp(tuId, workspace.getIdentityFactory().getIdentity(rbs2.getIdentity()), null)
                .addSvp(tuId, workspace.getIdentityFactory().getIdentity(spar.getIdentity()), null)
                .addSvp(tuId, workspace.getIdentityFactory().getIdentity(terminator.getIdentity()), null)
                .build();

        workspace.perform(addChildrenAction);

        SVPWriter writer = new SVPWriter(workspace, CONTEXTS);

        PartBundle pb_p1 = writer.write(inducible);
        PartBundle pb_r1 = writer.write(rbs1);
        PartBundle pb_c1 = writer.write(spak);
        PartBundle pb_r2 = writer.write(rbs2);
        PartBundle pb_c2 = writer.write(spar);

        Map<String, PartBundle> spar_spak_bundles = new HashMap<>();
        spar_spak_bundles.put(c1.getName(), pb_c1);
        spar_spak_bundles.put(c2.getName(), pb_c2);
        interactionReader.read(VPR_MANAGER.getInteractions(c1, c2).get(0), spar_spak_bundles);

    }
 
    public static void writeToXml(String sbml, String filename) {
        try
        {
            FileWriter writer = new FileWriter("exports/" + filename + ".xml");
            writer.write(sbml);
            writer.flush();
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.getLocalizedMessage());
        }   
    }

    private void simulateModel(String sbml) throws Exception{
        SBSbmlSimulator s = new SimpleCopasiSimulator(1000, 1000);
        s.setModel(sbml);
        s.run();
        s.getResults().getSpecies().stream().forEach(
                    m -> LOGGER.debug("{}: {}", m, Arrays.toString(s.getResults().getTimeCourse(m).toArray(new Double[] {})))
            );
    }


     private void compareDocuments(SBMLDocument firstDocument, SBMLDocument secondDocument)
    {
        System.out.println("=====================================================================");
        System.out.println("Name: " + firstDocument.getModel().getName() + " - " + secondDocument.getModel().getName());
        System.out.println("ID: " + firstDocument.getModel().getId() + " - " + secondDocument.getModel().getId());
        System.out.println("MetaID: " + firstDocument.getModel().getMetaId() + " - " + secondDocument.getModel().getMetaId());
        System.out.println("Element: " + firstDocument.getModel().getElementName() + " - " + secondDocument.getModel().getElementName());
        System.out.println("SBO: " + firstDocument.getModel().getSBOTermID() + " - " + secondDocument.getModel().getSBOTermID());
        System.out.println("Document 1 Species=================================================");
        List<Species> species = firstDocument.getModel().getListOfSpecies();
        for(Species s:species)
        {
             System.out.println("Species Name: " + s.getName());
             System.out.println("Species ID: " + s.getId());
             System.out.println("Species MetaID: " + s.getMetaId());
        }
        System.out.println("Document 2 Species=================================================");
        List<Species> species2 = secondDocument.getModel().getListOfSpecies();
        for(Species s:species2)
        {
             System.out.println("Species Name: " + s.getName());
             System.out.println("Species ID: " + s.getId());
             System.out.println("Species MetaID: " + s.getMetaId());
        }
        System.out.println("Document 1 Parameters===============================================");
        List<org.sbml.jsbml.Parameter> param = firstDocument.getModel().getListOfParameters();
        for(org.sbml.jsbml.Parameter p:param)
        {
             System.out.println("Parameter Name: " + p.getName());
             System.out.println("Parameter ID: " + p.getId());
             System.out.println("Parameter MetaID: " + p.getMetaId());
        }
        System.out.println("Document 2 Parameters==============================================");
        List<org.sbml.jsbml.Parameter> param2 = secondDocument.getModel().getListOfParameters();
        for(org.sbml.jsbml.Parameter p:param2)
        {
             System.out.println("Parameter Name: " + p.getName());
             System.out.println("Parameter ID: " + p.getId());
             System.out.println("Parameter MetaID: " + p.getMetaId());
        }
        System.out.println("Document 1 Parameters===============================================");
        List<Reaction> react = firstDocument.getModel().getListOfReactions();
        for(Reaction r:react)
        {
             System.out.println("Reaction Name: " + r.getName());
             System.out.println("Reaction ID: " + r.getId());
             System.out.println("Reaction MetaID: " + r.getMetaId());
        }
        System.out.println("Document 2 Parameters==============================================");
        List<Reaction> react2 = secondDocument.getModel().getListOfReactions();
        for(Reaction r:react2)
        {
             System.out.println("Reaction Name: " + r.getName());
             System.out.println("Reaction ID: " + r.getId());
             System.out.println("Reaction MetaID: " + r.getMetaId());
        }
        System.out.println("=====================================================================");
    }
    
}
