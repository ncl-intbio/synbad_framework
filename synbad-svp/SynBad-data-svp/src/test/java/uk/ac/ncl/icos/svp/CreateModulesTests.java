/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.FileWriter;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.swing.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.Species;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.svp.actions.builders.*;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.*;
import uk.ac.ncl.icos.synbad.svp.parser.SbmlWriter;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.view.visualisation.TimeCoursePlot;
import uk.ac.ncl.icos.synbad.workspace.*;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObjectAndDependants;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.view.obj.SviVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.view.visualisation.SvpGraphStreamPanel;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpWire;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

/**
 *
 * @author owengilfellon
 */
public class CreateModulesTests implements SBSubscriber {
    
    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/updatertests/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(CreateModulesTests.class);

    private SBIdentityFactory f;
    private SBWorkspace ws;

    private final SBIdentity rootIdentity;
    private final SBIdentity generator;
    private final SBIdentity tu1Id;
    private final SBIdentity tu2Id;
    private final SBIdentity tu3Id;
    private final SBIdentity tu4Id;
    private final SBIdentity sink;
    private final SBIdentity proX;
    private final SBIdentity cdsB;

    public CreateModulesTests() throws SBIdentityException {
        rootIdentity = SBIdentity.getIdentity("http://www.synbad.org", "root", "1.0");
        generator = SBIdentity.getIdentity("http://www.synbad.org", "generator", "1.0");
        tu1Id = SBIdentity.getIdentity("http://www.synbad.org", "tu1", "1.0");
        tu2Id = SBIdentity.getIdentity("http://www.synbad.org", "tu2", "1.0");
        tu3Id = SBIdentity.getIdentity("http://www.synbad.org", "tu3", "1.0");
        tu4Id = SBIdentity.getIdentity("http://www.synbad.org", "tu4", "1.0");
        sink = SBIdentity.getIdentity("http://www.synbad.org", "sink", "1.0");
        proX = SBIdentity.getIdentity(generator.getUriPrefix(), "pX", generator.getVersion());
        cdsB = SBIdentity.getIdentity(tu1Id.getUriPrefix(), "B", tu1Id.getVersion());
    }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        MockServices.setServices(
            DefaultSBModelFactory.class,
            SvpModelProvider.class);
        
        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
            .addService(new EnvConstantUpdater())
            .addService(new PiUpdater())
            .addService(new SviUpdater())
            .addService(new ExportedFcUpdater())
            .addService(new SBWireUpdater()).build();

        this.ws.createContext(CONTEXTS[0]);
        this.f =  ws.getIdentityFactory();
    }
    
    @After
    public void tearDown() {   
        this.ws.shutdown();
        this.ws = null;
    }


    public static void main(String[] args) {
        try {
           CreateModulesTests test = new CreateModulesTests();
            test.setUp();
            test.testAutoRegulation();
            test.tearDown();
        } catch (SBIdentityException ex) {
            Exceptions.printStackTrace(ex);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testCreateTu() throws SBIdentityException {
        
        SBSvpBuilder b = new SvpActionBuilderImpl(ws,  CONTEXTS);

        // Create identities
        
        SBIdentity svmIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "svm", "1.0");
        SBIdentity promoterIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "promoter", "1.0");
        SBIdentity rbs1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cds1Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_1", "1.0");
        SBIdentity rbs2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "rbs2", "1.0");
        SBIdentity cds2Identity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "coding_sequence_2", "1.0");
        SBIdentity smlMolIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "smlMol", "1.0");
        SBIdentity terIdentity = ws.getIdentityFactory().getIdentity("http://www.synbad.org", "terminator", "1.0");

        // Build and perform actions
        
        b = b.createSvm(svmIdentity)
            .createPromoter(promoterIdentity, 1.0)
            .createRBS(rbs1Identity, 1.0, 1.0)
            .createCDSWithPhosphorylatingSmallMolecule(cds1Identity, smlMolIdentity, 0.31, 0.1, 0.123, 0.213)
            .createRBS(rbs2Identity, 1.0, 1.0)
            .createCDS(cds2Identity, 1.0)
            .createTerminator(terIdentity)
            .addSvp(svmIdentity, promoterIdentity, null)
            .addSvp(svmIdentity, rbs1Identity, null)
            .addSvp(svmIdentity, cds1Identity, null)
            .addSvp(svmIdentity, rbs2Identity, null)
            .addSvp(svmIdentity, cds2Identity, null)
            .addSvp(svmIdentity, terIdentity, null);

        ws.perform(b.build());
    }

    @Test
    public void testSVPCommands() {
      
        // retrieve workspace manager and create workspace

        String prefix = "http://www.synbad.org";
        String version = "1.0";

        Random r = new Random();

        SBIdentity psparkId = ws.getIdentityFactory().getIdentity(prefix, "PspaRK", version);
        SBIdentity rbsspakId = ws.getIdentityFactory().getIdentity(prefix, "RBS_SpaK", version);
        SBIdentity spakId = ws.getIdentityFactory().getIdentity(prefix, "SpaK", version);
        SBIdentity rbsparId = ws.getIdentityFactory().getIdentity(prefix, "RBS_SpaR", version);
        SBIdentity sparId = ws.getIdentityFactory().getIdentity(prefix, "SpaR", version);
        SBIdentity ter1Id = ws.getIdentityFactory().getIdentity(prefix, "Ter1", version);
        SBIdentity spakSparId = ws.getIdentityFactory().getIdentity(prefix, "SpakPhosSpar", version);
        SBIdentity subtilinId = ws.getIdentityFactory().getIdentity(prefix, "Subtilin", version);

        // Create commands for constructs parts

        SBAction actions = new SvpActionBuilderImpl(ws,  CONTEXTS)
                .createPromoter(psparkId, r.nextDouble())
                .createRBS(rbsspakId, r.nextDouble(), 1.0)
                .createCDSWithPhosphorylatingSmallMolecule(spakId, subtilinId, r.nextDouble(), r.nextDouble(), r.nextDouble(), r.nextDouble())
                .createRBS(rbsparId, r.nextDouble(), 1.0)
                .createCDS(sparId, r.nextDouble())
                .createTerminator(ter1Id)
                .createPhosphorylate(spakSparId, spakId.getIdentity(), sparId.getIdentity(), r.nextDouble(), r.nextDouble(), r.nextDouble())
                .build();

        // Note, no DNA or whatnot associated at this point?

        ws.perform(actions);

        Svp pspark = ws.getObject(psparkId.getIdentity(), Svp.class, CONTEXTS);
        Svp rbsspak = ws.getObject(rbsspakId.getIdentity(), Svp.class, CONTEXTS);
        Svp spak = ws.getObject(spakId.getIdentity(), Svp.class, CONTEXTS);
        Svp rbsspar = ws.getObject(rbsparId.getIdentity(), Svp.class, CONTEXTS);
        Svp spar = ws.getObject(sparId.getIdentity(), Svp.class, CONTEXTS);
        Svp ter1 = ws.getObject(ter1Id.getIdentity(), Svp.class, CONTEXTS);

        try (SBWorkspaceGraph g = new SBWorkspaceGraph(ws, CONTEXTS)) {

            List<Svp> svps = Arrays.asList(pspark, rbsspak, spak, rbsspar, spar, ter1);
            svps.stream().forEach(svp -> {

                if(!svp.getPorts().isEmpty()) {

                    // SVPs are not owned by ports

                    assert(ws.incomingEdges(svp, svp.getContexts()).stream().filter(e -> e.getEdge().toASCIIString()
                            .equals(SynBadTerms.SynBadEntity.hasPort)).collect(Collectors.toSet()).isEmpty());

                    // Ports match filtered edges

                    assert(ws.outgoingEdges(svp, svp.getContexts()).stream().filter(e -> e.getEdge().toASCIIString()
                            .equals(SynBadTerms.SynBadEntity.hasPort)).count() == svp.getPorts().size());

                    // Ports match traversed edges

                    assert(g.getTraversal().v(svp).e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasPort).v(SBDirection.OUT)
                            .getResult().streamVertexes().count() == svp.getPorts().size());
                }
            });
        }
    }

    @Test
    public void testAutoRegulationModule()  {

        SvpModel model = ExampleModuleFactory.createAutoRegulation(ws, CONTEXTS);
        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);
        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();

        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsB, ComponentType.Protein, SynBadPortState.Default, proX);
        SvpInteraction actDef = ws.getObject(actId.getIdentity(), SvpInteraction.class, CONTEXTS);
        //.SviInstance activation = rootSvm.getSviInstances(actDef).iterator().next();

        debugSvmWiring(0, view.getRoot());

        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());

        SvmVo sinkVo = sequenceChildren.get(1).getAsSubclass(SvmVo.class);
        Assert.assertEquals("Sink should have no unconnected ports", 0, sinkVo.getPorts(false).size());
        Assert.assertEquals("Sink should have one wired in ports", 1, sinkVo.getPorts(true, SBPortDirection.IN).size());
        Assert.assertEquals("Sink should have no wired out ports", 0, sinkVo.getPorts(true, SBPortDirection.OUT).size());

        // Sink_Protein -> activation, Generator_PoPS -> activation, activation -> sink

        Assert.assertEquals("Root VO should have 1 nested wires", 1, rootVo.getNestedWires().size());
        Assert.assertEquals("Root Svm should have 1 nested wires", 1, rootVo.getSvpModule().getWires().size());

        // Currently, protein degradation is added as an SVI to Root SVM. Should not instance internal SVIs when FC is exported from module...

        Assert.assertEquals("Root VO should have no interactions", 0, rootVo.getSvis().size());
        Assert.assertEquals("Root Svm should have no interactions", 0, rootSvm.getInteractionDefinitions().size());
        Assert.assertEquals("Root ModuleDefinition should have no interactions", 0,
                ws.getObject(rootSvm.getIdentity(), ModuleDefinition.class, CONTEXTS).getInteractions().size());

        List<SBSvpSequenceObject<? extends SvpEntity>> instances = rootSvm.getOrderedInstances();

//        Assert.assertTrue("Generator and Activation should be wired", rootSvm.areWired(instances.get(0), activation));
//        Assert.assertTrue("Sink and Activation  should be wired", rootSvm.areWired(instances.get(1), activation));
//        Assert.assertTrue("Activation and sink should be wired", rootSvm.areWired(activation, instances.get(1)));


        debugSvmWiring(0, rootVo);
        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-autoregulation");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }

    @Test
    public void testAutoRegulationRemoveSink()  {

        SvpModel model = ExampleModuleFactory.createAutoRegulation(ws, CONTEXTS);

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);

        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        ModuleDefinition md = rootSvm.as(ModuleDefinition.class).get();
        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsB, ComponentType.Protein, SynBadPortState.Default, proX);
        SvmVo sinkVo = sequenceChildren.get(1).getAsSubclass(SvmVo.class);

        ws.getDispatcher().subscribe(e -> SBGraphEvent.class.isAssignableFrom(e.getClass()) &&
                ((SBGraphEvent)e).getGraphEntityType() == SBGraphEntityType.NODE, e -> LOGGER.debug("{}", e.toString()));
        ws.perform(new RemoveObjectAndDependants<>(sinkVo.getIdentity(), rootVo.getIdentity(), ws, CONTEXTS));

        debugSvmWiring(0, rootVo);

        List<SvVo> sequenceChildrenAfterRemoval = view.getRoot().getOrderedSequenceComponents();

        Assert.assertEquals("Sink VO should have been removed", 1, sequenceChildrenAfterRemoval.size());

        // Exported FCs are probably not being removed, therefore interactions are not being removed either....

        Assert.assertEquals("Activation should have been removed", 0, rootSvm.getInteractionDefinitions().size());
        Assert.assertEquals("Activation VO should have been removed", 0, rootVo.getSvis().size());

        Assert.assertEquals("Wires should have been removed", 0, rootSvm.getWires().size());
        Assert.assertEquals("View Wires should have been removed", 0, rootVo.getNestedWires().size());

        model.close();
    }

    @Test
    public void testAutoRegulationUsingPhosphorylated() {

        SvpModel model = ExampleModuleFactory.createAutoRegulationUsingPhosphorylated(ws, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);

        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        Set<SvpWire> wires = rootVo.getNestedWires();rootVo.getSvis().size();
        Collection<SviVo> interactions = rootVo.getSvis();

        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        SvmVo tu1vo = sequenceChildren.get(1).getAsSubclass(SvmVo.class);
        SvmVo sinkVo = sequenceChildren.get(2).getAsSubclass(SvmVo.class);

        debugSvmWiring(0, view.getRoot());

        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());

        Assert.assertEquals("TU 1 should have no unconnected ports",0, tu1vo.getPorts(false).size());
        Assert.assertEquals("TU 1 should have two wired out port", 2, tu1vo.getPorts(true, SBPortDirection.OUT).size());

        // Two wired in ports, one for each RBS accepting a PoPS signal

        Assert.assertEquals("TU 1 should have two wired in ports", 2, tu1vo.getPorts(true, SBPortDirection.IN).size());

        Assert.assertEquals("Sink should have no unconnected ports", 0, sinkVo.getPorts(false).size());
        Assert.assertEquals("Sink should have one wired in port", 1, sinkVo.getPorts(true, SBPortDirection.IN).size());

        // Should be two interactions (both activations) due to exporting of phosphorylated protein.
        // Phosphorylation interaction is encapsulated wuithin TU1.

        Assert.assertEquals("Root should have 2 interactions", 2, interactions.size());

        // Should be one more wire than plain activation, including an additional wire between the activation
        // and TU1, due to the additional PoPS input port.

        // TO-DO: dont wire promoter if wired to PoPS modulator

        //Assert.assertEquals("Root should have 7 nested wires", 7, wires.size());

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-autoregulation-phosphorylated");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }

    }

    @Test
    public void testAutoRegulationWithModulator() {

        SvpModel model = ExampleModuleFactory.createAutoRegulationWithModulator(ws, CONTEXTS);
        SvpPView view = model.getMainView();

        // Expand all nodes

        view.nodeSet().forEach(view::expand);

        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        Set<SvpWire> wires = rootVo.getNestedWires();rootVo.getSvis().size();
        Collection<SviVo> interactions = rootVo.getSvis();

        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        SvmVo tu1vo = sequenceChildren.get(1).getAsSubclass(SvmVo.class);
        SvmVo sinkVo = sequenceChildren.get(2).getAsSubclass(SvmVo.class);

        debugSvmWiring(0, view.getRoot());

        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());

        Assert.assertEquals("TU 1 should have no unconnected ports", 0, tu1vo.getPorts(false).size());
        Assert.assertEquals("TU 1 should have one wired out port", 1, tu1vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 1 should have one wired in port", 1, tu1vo.getPorts(true, SBPortDirection.IN).size());

        Assert.assertEquals("Sink should have no unconnected ports", 0 , sinkVo.getPorts(false).size());
        Assert.assertEquals("Sink should have one wired in port", 1, sinkVo.getPorts(true, SBPortDirection.IN).size());

        // Adding the interact adds protein FC in parent, which creates an interaction with the mapped Promoter
        // Therefore, an activation interaction expected in root, and in tu1

        Assert.assertEquals("Root should have 2 nested wires", 2, wires.size());
        Assert.assertEquals("Root should have no interactions", 0, interactions.size());

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-autoregulation-modulator");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }



    @Test
    public void testGeneratorModulatorSink() {

        SvpModel model = ExampleModuleFactory.createGeneratorModulatorSink(ws, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        debugSvmWiring(0, view.getRoot());

        Assert.assertEquals("Root SVM should contain 6 sequence children", 6, sequenceChildren.size());
        Assert.assertTrue("All sequence children should be SVMs", sequenceChildren.stream().allMatch(c -> SvmVo.class.isAssignableFrom(c.getClass())));

        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());
        
        SvmVo tu1vo = sequenceChildren.get(1).getAsSubclass(SvmVo.class);
        Assert.assertEquals("TU 1 should have no unconnected ports", 0, tu1vo.getPorts(false).size());
        Assert.assertEquals("TU 1 should have one wired out ports", 1, tu1vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 1 should have one wired in port", 1, tu1vo.getPorts(true, SBPortDirection.IN).size());

        SvmVo tu2vo = sequenceChildren.get(2).getAsSubclass(SvmVo.class);
        Assert.assertEquals("TU 2 should have no unconnected ports", 0, tu2vo.getPorts(false).size());
        Assert.assertEquals("TU 2 should have two wired out port", 2, tu2vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 2 should have two wired in ports", 2, tu2vo.getPorts(true, SBPortDirection.IN).size());

        SvmVo tu3vo = sequenceChildren.get(3).getAsSubclass(SvmVo.class);
        Assert.assertEquals("TU 3 should have no unconnected ports", 0, tu3vo.getPorts(false).size());
        Assert.assertEquals("TU 3 should have two wired out ports", 2, tu3vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 3 should have two wired in ports", 2, tu3vo.getPorts(true, SBPortDirection.IN).size());

        SvmVo tu4vo = sequenceChildren.get(4).getAsSubclass(SvmVo.class);
        Assert.assertEquals("TU 4 should have no unconnected ports", 0, tu4vo.getPorts(false).size());
        Assert.assertEquals("TU 4 should have two wired out ports", 2, tu4vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 4 should have two wired in ports", 2, tu4vo.getPorts(true, SBPortDirection.IN).size());

        SvmVo tu5vo = sequenceChildren.get(5).getAsSubclass(SvmVo.class);
        Assert.assertEquals("Sink should have no unconnected ports", 0, tu5vo.getPorts(false).size());
        Assert.assertEquals("Sink should one wired in port", 1, tu5vo.getPorts(true, SBPortDirection.IN).size());

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-gen-mod-sink");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }
    
    @Test
    public void testAutoRegulationUsingComplex() {
        
        SvpModel model = ExampleModuleFactory.createAutoRegulationUsingComplex(ws, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);
        
        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        Set<SvpWire> wires = rootVo.getNestedWires();rootVo.getSvis().size();
        Collection<SviVo> interactions = rootVo.getSvis();
        
        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        SvmVo tu1vo = sequenceChildren.get(1).getAsSubclass(SvmVo.class);
        SvmVo sinkVo = sequenceChildren.get(2).getAsSubclass(SvmVo.class);
        
        // debug SVM heirarchy
        
        debugSvmWiring(0, view.getRoot());
        
        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());

        Assert.assertEquals("TU 1 should have no unconnected ports", 0, tu1vo.getPorts(false).size());
        Assert.assertEquals("TU 1 should have two wired out port", 2, tu1vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 1 should have two wired in ports", 2, tu1vo.getPorts(true, SBPortDirection.IN).size());

        // RBS_A -> CDS_A, RBS_B-> CDS_B, CDS_A -> binding, CDS_B -> binding, PoPS -> activation, Complex -> activation
        
        Assert.assertEquals("TU 1 should have 9 nested wires", 9, tu1vo.getNestedWires().size());

        Assert.assertEquals("Sink should have no unconnected ports", 0, sinkVo.getPorts(false).size());
        Assert.assertEquals("Sink should have one wired in port", 1, sinkVo.getPorts(true, SBPortDirection.IN).size());

        // 2x TU1 IN, 2x activateX -> TU1, 1x activateY -> Sink, 2x TU1_Complex to activateX/Y

        // What about disassociation?
        //        Assert.assertEquals("Root should have 7 nested wires", 7, wires.size());

        // 2x Complex Activations, complex degradation and disassociation

        Assert.assertEquals("Root should have 4 interactions", 4, interactions.size());

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-autoregulation-with-complex");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }

    }
    
    @Test
    public void testRepressilator() {
        SvpModel model = ExampleModuleFactory.createRepressilator(ws, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);
        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        Set<SvpWire> wires = rootVo.getNestedWires();
        //rootVo.getSvis().size();
        Collection<SviVo> interactions = rootVo.getSvis();
        
        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        SvpVo opSvp = sequenceChildren.get(1).getAsSubclass(SvpVo.class);
        SvmVo tu1vo = sequenceChildren.get(2).getAsSubclass(SvmVo.class);
        SvmVo tu2vo = sequenceChildren.get(3).getAsSubclass(SvmVo.class);
        SvmVo sinkVo = sequenceChildren.get(4).getAsSubclass(SvmVo.class);

        debugSvmWiring(0, view.getRoot());
        
        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());

        Assert.assertEquals("TU 1 should have one wired out port", 1, tu1vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 1 should have one wired in ports", 1, tu1vo.getPorts(true, SBPortDirection.IN).size());
        Assert.assertEquals("TU 1 should have no unconnected ports", 0, tu1vo.getPorts(false).size());

        Assert.assertEquals("TU 2 should have one wired out port", 1, tu2vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 2 should have one wired in ports", 1, tu2vo.getPorts(true, SBPortDirection.IN).size());
        Assert.assertEquals("TU 2 should have no unconnected ports", 0, tu2vo.getPorts(false).size());

        Assert.assertEquals("Sink should have one wired in port", 1, sinkVo.getPorts(true, SBPortDirection.IN).size());
        Assert.assertEquals("Sink should have one wired out port", 1, sinkVo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("Sink should have no unconnected ports", 0, sinkVo.getPorts(false).size());

        Assert.assertEquals("Root should have 6 nested wires", 6, wires.size());
        Assert.assertEquals("Root should have 1 interaction", 1, interactions.size());

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-repressilator");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }

    }

    @Test
    public void testNegativeAutoRegulationWithModulator() throws SBIdentityException {

        SvpModel model = ExampleModuleFactory.createNegativeAutoRegulationWithModulator(ws, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);
 
        SvmVo rootVo = view.getRoot();
        List<SvVo> sequenceChildren = view.getRoot().getOrderedSequenceComponents();
        Set<SvpWire> wires = rootVo.getNestedWires();rootVo.getSvis().size();
        Collection<SviVo> interactions = rootVo.getSvis();
        
        SvmVo generatorVo = sequenceChildren.get(0).getAsSubclass(SvmVo.class);
        SvpVo opVo = sequenceChildren.get(1).getAsSubclass(SvpVo.class);
        SvmVo tu1vo = sequenceChildren.get(2).getAsSubclass(SvmVo.class);
        SvmVo sinkVo = sequenceChildren.get(3).getAsSubclass(SvmVo.class);

        debugSvmWiring(0, view.getRoot());
        
        Assert.assertEquals("Generator should have no unconnected ports", 0, generatorVo.getPorts(false).size());
        Assert.assertEquals("Generator should have one wired out port", 1, generatorVo.getPorts(true, SBPortDirection.OUT).size());

        Assert.assertEquals("TU 1 should have no unconnected ports", 0, tu1vo.getPorts(false).size());
        Assert.assertEquals("TU 1 should have two wired out port", 2, tu1vo.getPorts(true, SBPortDirection.OUT).size());
        Assert.assertEquals("TU 1 should have one wired in port", 1, tu1vo.getPorts(true, SBPortDirection.IN).size());

        Assert.assertEquals("Sink should have no unconnected ports", 0, sinkVo.getPorts(false).size());
        Assert.assertEquals("Sink should have one wired in port", 1, sinkVo.getPorts(true, SBPortDirection.IN).size());

        // Should be one more wire than activation, additionally including generator -> operator
        // TO-DO: dont wire promoter if wired to PoPS modulator

        Assert.assertEquals("Root should have 7 nested wires", 7, wires.size());
        Assert.assertEquals("Root should have 2 interactions", 2, interactions.size());

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-negative-autoregulation-with-modulator");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }
    
     
    @Test
    public void testUnitSmlMolComplex() {

        SvpModel model = ExampleModuleFactory.createUnitSmlMolComplex(ws, CONTEXTS);
        SvpPView view = model.getMainView();
        view.nodeSet().forEach(view::expand);
        debugSvmWiring(0, view.getRoot());

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-unit-smlmol-complex");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }
    
    @Test
    public void testAutoRegulation() {
        
        LOGGER.debug("================== Test Autoregulation ==================");

        SBIdentityFactory f = ws.getIdentityFactory();

        // Create Identities
        
        SBIdentity svmId = f.getIdentity("http://www.synbad.org", "svm1", "1.0");
        SBIdentity promoterId = f.getIdentity("http://www.synbad.org", "promoter1", "1.0");
        SBIdentity rbsId = f.getIdentity("http://www.synbad.org", "rbs1", "1.0");
        SBIdentity cdsId = f.getIdentity("http://www.synbad.org", "cds1", "1.0");
        SBIdentity terminatorId = f.getIdentity("http://www.synbad.org", "terminator", "1.0");
        SBIdentity cds_activates_promotoer = f.getIdentity("http://www.synbad.org", "cds_activate_promoter", "1.0");
        
        SBSvpBuilder b = new SvpActionBuilderImpl(ws, CONTEXTS);
        
        b.createPromoter(promoterId, 1.0);
        b.createRBS(rbsId, 0.5, 1.0);
        b.createCDS(cdsId, 1.0);
        b.createTerminator(terminatorId);
        b.createSvm(svmId);
        b.addSvp(svmId, promoterId, null);
        b.addSvp(svmId, rbsId, null);
        b.addSvp(svmId, cdsId, null);
        b.addSvp(svmId, terminatorId, null);
        b.asInteractionBuilder().createActivateByPromoter(cds_activates_promotoer, 
                ComponentType.Protein, SynBadPortState.Default, cdsId.getIdentity(), promoterId.getIdentity(), 0.23, 1.0);
        ws.perform(b.build());
        
        SvpModule svm = ws.getObject(svmId.getIdentity(), SvpModule.class, CONTEXTS);
        ModuleDefinition md = svm.as(ModuleDefinition.class).get();
        
        debugWires(ws, svm);
        List<SBSvpSequenceObject<? extends SvpEntity>> instances = svm.getOrderedInstances();

        SviInstance activation = svm.getSviInstances(InteractionType.TranscriptionActivation).iterator().next();

        Assert.assertEquals("TThere should be 4 instances", 4, instances.size());
        Assert.assertNotNull("Activation should not be null", activation);
        Assert.assertTrue(instances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(instances.get(3).is(FunctionalComponent.TYPE));
        
        Assert.assertTrue("RBS and CDS should be wired", svm.areWired(instances.get(1), instances.get(2)));
        Assert.assertTrue("CDS and Terminator should not be wired", !svm.areWired(instances.get(2), instances.get(3)));
        Assert.assertTrue("CDS and Activation should be wired", svm.areWired(instances.get(2), activation));
        Assert.assertTrue("Promoter and Activation should be wired", svm.areWired(instances.get(0), activation));
        Assert.assertTrue("Activation and RBS should be wired", svm.areWired(activation, instances.get(1)));
        Assert.assertTrue("Promoter and RBS should not be wired", !svm.areWired(instances.get(0), instances.get(1)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, svm);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);
            simulateModel(new SBMLHandler().GetSBML(doc));
            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-manual-autoregulation");
        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }

    
    

    /**
         * Test of setupWorkspace method, of class ExampleFactory.
     */
    @Test
    public void testSubtilinReceiver() throws SBIdentityException {
        
        LOGGER.debug("================== Test Example Factory ==================");
            
        SvpModule subtilinReceiver = ExampleFactory.setupWorkspace(ws, CONTEXTS);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class)
                .createModel(SvpModel.class, subtilinReceiver);

        debugWires(ws, subtilinReceiver);
        
        List<SBSvpSequenceObject<? extends SvpEntity>> instances = subtilinReceiver.getOrderedInstances();
        
        SvpModule receiver =  (SvpModule) instances.get(0).getDefinition().get();
        List<SBSvpSequenceObject<? extends SvpEntity>> receiverInstances = receiver.getOrderedInstances();
        
        receiverInstances.stream().forEach(sc -> LOGGER.debug("SC: {}", sc.getDisplayId()));

        SvpModule reporter = (SvpModule) instances.get(1).getDefinition().get();
        List<SBSvpSequenceObject<? extends SvpEntity>> reporterInstances = reporter.getOrderedInstances();

        Assert.assertEquals(2, instances.size());
        Assert.assertTrue(instances.get(0).is(Module.TYPE));
        Assert.assertTrue(instances.get(1).is(Module.TYPE));
        Assert.assertTrue("Receiver and Reporter should be wired", subtilinReceiver.areWired(instances.get(0), instances.get(1)));

        SviInstance activation = receiver.as(ModuleDefinition.class).get().getInteractions().stream()
                .map(i -> i.as(SviInstance.class)).filter(i -> i.isPresent()).map(i -> i.get())
                .filter(i -> i.getDefinition().map(svi -> svi.getInteractionType() == InteractionType.TranscriptionActivation).orElse(Boolean.FALSE))
                .findFirst().orElse(null);

        SviInstance phosphorylation = receiver.getSviInstances(InteractionType.Phosphorylation).stream()
                .filter(i -> i.getDefinition().map(svi -> !svi.isInternal()).orElse(Boolean.FALSE))
                .findFirst().orElse(null);

        Assert.assertEquals(7, receiverInstances.size());
        Assert.assertNotNull("Activation should not be null", activation);
        Assert.assertNotNull("Phosphorylation should not be null", phosphorylation);
        
        Assert.assertTrue(receiverInstances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(receiverInstances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(receiverInstances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue(receiverInstances.get(3).is(FunctionalComponent.TYPE));
        Assert.assertTrue(receiverInstances.get(4).is(FunctionalComponent.TYPE));
        Assert.assertTrue(receiverInstances.get(5).is(FunctionalComponent.TYPE));
        Assert.assertTrue("Child 7 should be FunctionalComponent, is " + receiverInstances.get(6).getClass().getSimpleName(), receiverInstances.get(6).is(FunctionalComponent.TYPE));
        
        Assert.assertTrue("Promoter 1 and RBS 1 should be wired", receiver.areWired(receiverInstances.get(0), receiverInstances.get(1)));
        Assert.assertTrue("RBS 1 and CDS 1 should be wired", receiver.areWired(receiverInstances.get(1), receiverInstances.get(2)));
        Assert.assertTrue("RBS 2 and CDS 2 should be wired", receiver.areWired(receiverInstances.get(3), receiverInstances.get(4)));
        Assert.assertTrue("RBS 1 and CDS 2 should not be wired", !receiver.areWired(receiverInstances.get(1), receiverInstances.get(4)));
        Assert.assertTrue("CDS 1 and Phosphorylation should be wired", receiver.areWired(receiverInstances.get(2), phosphorylation));
        Assert.assertTrue("CDS 2 and Phosphorylation should be wired", receiver.areWired(receiverInstances.get(4), phosphorylation));
        Assert.assertTrue("Phosphorylation and Activation should be wired", receiver.areWired(phosphorylation, activation));
        Assert.assertTrue("Activation and Promoter 2 should be wired", receiver.areWired(receiverInstances.get(6), activation));
    
        Assert.assertEquals(3, reporterInstances.size());
        
        Assert.assertTrue(reporterInstances.get(0).is(FunctionalComponent.TYPE));
        Assert.assertTrue(reporterInstances.get(1).is(FunctionalComponent.TYPE));
        Assert.assertTrue(reporterInstances.get(2).is(FunctionalComponent.TYPE));
        Assert.assertTrue("RBS 3 and CDS 3 should be wired", reporter.areWired(reporterInstances.get(0), reporterInstances.get(1)));

        // test export to SBML

        SbmlWriter writer = new SbmlWriter(ws, CONTEXTS);

        try {
            SBMLDocument doc = writer.getSbmlDocument(model);

            ListOf<Species> species = doc.getModel().getListOfSpecies();

            Assert.assertEquals(
                    "SBML model should contain " + ExampleFactory.C_SUBTILIN.getDisplayID(),
                    1,
                    species.stream().filter(s -> s.getId().equals(ExampleFactory.C_SUBTILIN.getDisplayID())).count());

            Assert.assertEquals(
                    "SBML model should contain " + ExampleFactory.P_SPAK.getDisplayID() + "Phosphorylated",
                    1,
                    species.stream().filter(s -> s.getId().equals(ExampleFactory.P_SPAK.getDisplayID() + "Phosphorylated")).count());

            Assert.assertEquals(
                    "SBML model should contain " + ExampleFactory.P_SPAR.getDisplayID() + "Phosphorylated",
                    1,
                    species.stream().filter(s -> s.getId().equals(ExampleFactory.P_SPAR.getDisplayID() + "Phosphorylated")).count());

            simulateModel(new SBMLHandler().GetSBML(doc));

            writeToXml(new SBMLHandler().GetSBML(doc), "sbml-subtilinreceiver-from-actions");

        } catch (Exception e) {
            LOGGER.error("Could not simulate model", e);
            Assert.fail("Could not simulate model");
        } finally {
            model.close();
        }
    }
    
    public void debugWires(SBWorkspace result, SvpModule module) {
        SvpModel model = new SvpModel(result, module);
        SvpPView view = model.getMainView();
        view.hierarchyIterator(model.getViewRoot()).forEachRemaining(n -> {
           LOGGER.debug("---{}---", n.getDisplayId());
           if(SvmVo.class.isAssignableFrom(n.getClass())) {
               SvmVo svm = n.getAsSubclass(SvmVo.class);
               svm.getSvpModule().getWires().stream().forEach(w -> {
                   LOGGER.debug("\t\t{} ------> {}", w.getFrom().getOwner().getDisplayId(), w.getTo().getOwner().getDisplayId());
               });
           } 
       });
    }

//    public static void main(String[] args) throws SBIdentityException {
//        CreateModulesTests tuTests = new CreateModulesTests();
//        tuTests.setUp();
//        tuTests.createAutoRegulationUsingComplex();
//        tuTests.render();
//    }
    
    public void render()  {

        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        
        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, rootSvm);
        SvpPView view = model.getMainView();
        view.nodeSet().stream().forEach(n -> view.expand(n));

        SvpGraphStreamPanel panel = new SvpGraphStreamPanel();
        panel.setView(view.getRoot(), view);
        
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        panel.run();
    }

    @Override
    public void onEvent(SBEvent e) {
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {
            SBGraphEvent evt = (SBGraphEvent)e;
            
            if(evt.getGraphEntityType() == SBGraphEntityType.EDGE) {
                SynBadEdge edge = (SynBadEdge) evt.getData();
                if(edge.getEdge().toASCIIString().equals(SynBadTerms.SbolModule.hasFunctionalComponent)) {
                    System.out.println(evt.getType() + ": " + edge);
                }
                
                if(edge.getEdge().toASCIIString().equals(SynBadTerms.SynBadPart.hasInternalSpecies)) {
                    System.out.println(evt.getType() + ": " + edge);
                }
            }
        }
    }

    private void simulateModel(String sbml) throws Exception{
        SimpleCopasiSimulator s = new SimpleCopasiSimulator(12800, 1280);
        s.setModel(sbml);

        //s.addInitialConcentration("smlMol", 200.0);
        if(s.run()) {
           // renderChart(s.getResults());
//            s.getResults().getSpecies().stream().forEach(
//                    m -> LOGGER.debug("{}: {}", m, Arrays.toString(s.getResults().getTimeCourse(m).toArray(new Double[] {})))
//            );
        }
    }

    public static void writeToXml(String sbml, String filename) {
        //LOGGER.debug(sbml);

        try
        {
            FileWriter writer = new FileWriter("exports/" + filename + ".xml");
            writer.write(sbml);
            writer.flush();
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }

    public void debugSvm(int indent, SvmVo svm) {

        StringBuilder tab = new StringBuilder();
        for(int i = 0; i < indent; i++) {
            tab.append("\t");
        }
        String tabString = tab.toString();

        LOGGER.debug("{}{}: {}", tabString, svm.getClass().getSimpleName(), svm.getDisplayId());

        svm.getView().getChildren(svm).forEach(c -> {
            LOGGER.debug("{}\tChild: {}", tabString, c.getDisplayId());
        });

        //svm.getSvpModule().getExtension().getSvpInstances().stream().forEach(fc -> LOGGER.debug("{}FC-- {}", tabString, fc.getDisplayId()));

        svm.getPorts(SBPortDirection.IN).forEach(p -> {
            LOGGER.debug("{}\tP IN: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(SBPortDirection.OUT).forEach(p -> {
            LOGGER.debug("{}\tP OUT: {}", tabString, p.getDisplayId());
        });


        svm.getNestedPorts(false, SBPortDirection.IN).forEach(p -> {
            LOGGER.debug("{}\tUnwired IN: {}", tabString, p.getDisplayId());
        });

        svm.getNestedPorts(false, SBPortDirection.OUT).forEach(p -> {
            LOGGER.debug("{}\tUnwired OUT: {}", tabString, p.getDisplayId());
        });

        svm.getNestedWires().forEach(w -> {
            LOGGER.debug("{}\tWire: {} -> {}", tabString, w.getFrom().getDisplayId(), w.getTo().getDisplayId());
        });

        svm.getOrderedSequenceComponents().stream().forEach(c -> {


            if(c.is(Module.TYPE) || c.is(ModuleDefinition.TYPE)) {
                debugSvm(indent + 1, (SvmVo)c);
            } else {
                LOGGER.debug("{}\t{}: {}", tabString, c.getClass().getSimpleName(), c.getDisplayId());
            }
        });


    }

    public void debugSvmWiring(int indent, SvmVo svm) {

        StringBuilder tab = new StringBuilder();

        for(int i = 0; i < indent; i++) {
            tab.append("\t");
        }

        String tabString = tab.toString();

        LOGGER.debug("{}{}: {}", tabString, svm.getClass().getSimpleName(), svm.getDisplayId());

        svm.getPorts(false, SBPortDirection.IN).forEach(p -> {
            LOGGER.debug("{}\t-IN: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(true, SBPortDirection.IN).forEach(p -> {
            LOGGER.debug("{}\t+IN: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(false, SBPortDirection.OUT).forEach(p -> {
            LOGGER.debug("{}\t-OUT: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(true, SBPortDirection.OUT).forEach(p -> {
            LOGGER.debug("{}\t+OUT: {}", tabString, p.getDisplayId());
        });

        svm.getNestedWires().stream().map(w ->
                w.getFrom().getOwner().getDisplayId()
                        .concat(":")
                        .concat(w.getFrom().getDisplayId())
                        .concat(" -> ")
                        .concat(w.getTo().getOwner().getDisplayId()
                                .concat(":")
                                .concat(w.getTo().getDisplayId())))
                .sorted().forEach(w -> LOGGER.debug("{}\tWIRE: {}", tabString, w));

        svm.getSvis().forEach(svi -> {
            LOGGER.debug("{}\tSVI: {}", tabString, svi.getDisplayId());
        });

        svm.getOrderedSequenceComponents().stream().forEach(c -> {
            if(c.is(Module.TYPE) || c.is(ModuleDefinition.TYPE)) {
                debugSvmWiring(indent + 1, (SvmVo)c);
            } else {
                LOGGER.debug("{}\t{}: {}", tabString, c.getClass().getSimpleName(), c.getDisplayId());
            }
        });
    }

    public void renderChart(TimeCourseTraces result) {

        JPanel panel = new TimeCoursePlot(result).getPanel();

        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;

        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}
