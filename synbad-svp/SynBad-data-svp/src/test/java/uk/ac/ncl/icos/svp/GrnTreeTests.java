/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.svp;

import org.junit.*;
import org.netbeans.junit.MockServices;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.api.domain.*;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfMemService;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSviBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBTuBuilder;
import uk.ac.ncl.icos.synbad.svp.api.SynBadSvp;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.obj.SvmInstance;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.parser.GrnTreeParser;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

import java.net.URI;
import java.util.Arrays;

/**
 *
 * @author owengilfellon
 */
public class GrnTreeTests {

    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/updatertests/1.0");

    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 

    }
    
    @After
    public void tearDown() {   

    }


    public static void main(String[] args) {

    }

    /**
     * Test of createSvp method, of class SvpActionBuilder.
     */
    @Test
    public void testCreateTu() {

        URI[] contexts = {URI.create("http://synbad.org/context1")};
        SBWorkspace ws = new SBWorkspaceBuilder(URI.create("http://synbad.org/GraphTest/1.0"))
                .addRdfService(new DefaultSBRdfMemService()).build();
        GrnTreeParser parser = new GrnTreeParser(ws, contexts);

        GRNTree c1 = GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
                        "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        ws.getDispatcher().subscribe(new SBEventFilter.DefaultFilter(), e -> {
            if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {
                SBGraphEvent ge = (SBGraphEvent) e;
                if(ge.getGraphEntityType() == SBGraphEntityType.NODE) {
                    SBIdentified v = (SBIdentified) ws.getObject((URI)e.getData(), (Class<? extends SBValued>)e.getDataClass(), contexts);
                    System.out.println("Adding " + v.getDisplayId()+ ":" + v.getType().toASCIIString());
                }
            }
        });

        SvpModule m = parser.createModuleFromGrnTree(SBIdentity.getIdentity("http://synbad.org", "testModel", "1.0"), c1);
        SBWorkspaceGraph g = new SBWorkspaceGraph(ws, m.getContexts());

        g.getTraversal().v(ws.getObjects(SvpModule.class, contexts).toArray(new SvpModule[]{}))
                .as("P")
                .e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasChild).v(SBDirection.OUT, Svp.class).as("SVP1")
                .e(SBDirection.IN, SynBadTerms.SynBadInteraction.isSvpParticipant).v(SBDirection.IN, SvpInteraction.class)
                .filter(svi -> !svi.isInternal())
                .filter(svi -> svi.getInteractionType() == InteractionType.Phosphorylation)
                .e(SBDirection.OUT, SynBadTerms.SynBadInteraction.isSvpParticipant).v(SBDirection.OUT, Svp.class).hasNotLabel("SVP1").as("SVP")
                .e(SBDirection.IN, SynBadTerms.SynBadEntity.hasChild).v(SBDirection.IN, SvpModule.class)
                .hasLabel("P").select("SVP")
                .forEachRemaining(svp -> {System.out.println("svp: " + svp);});

        g.getTraversal().v(ws.getObjects(Interaction.class, contexts).toArray(new Interaction[]{}))
                .as("I")
                .e(SBDirection.OUT, SynBadTerms.SbolInteraction.hasParticipation).v(SBDirection.OUT, Participation.class)
                .e(SBDirection.OUT, SynBadTerms.SbolInteraction.participant).v(SBDirection.OUT, FunctionalComponent.class)
                .as("FC")
                .select(SBIdentified.class, "I", "FC")
                .forEachRemaining(n -> {
                    System.out.println("-----");
                    if(n.is(Interaction.TYPE)) {
                        System.out.println("I: " + n);
                    } else if (n.is(FunctionalComponent.TYPE)) {
                        System.out.println("    FC: " + n);
                    }
                });


        m.getOrderedChildren(Svp.class).forEach(x -> {
            g.getTraversal()
                    .v(x).e(SBDirection.IN, SynBadTerms.SynBadInteraction.isSvpParticipant)
                    .v(SBDirection.IN, SvpInteraction.class)
                    .filter(svi -> svi.isInternal())
                    .filter(svi -> svi.getInteractionType() == InteractionType.Phosphorylation)
                    .forEachRemaining(svi -> {System.out.println(x.getDisplayId() + " -> " + svi.getDisplayId());});

            // list any parts that have an external interaction with a part in their own TU



        });


    }

    @Test
    public void testCreateTu2() {


        SBWorkspace ws = new SBWorkspaceBuilder(URI.create("http://synbad.org/GraphTest/1.0"))
                .addRdfService(new DefaultSBRdfMemService()).build();

        for(int i = 0; i < 10; i++) {
            System.out.println("Doing " + i);
            URI[] contexts = {URI.create("http://synbad.org/context" + i)};
            GrnTreeParser parser = new GrnTreeParser(ws, contexts);
            String id1 = SVPManager.getSVPManager().getRBS().getName();
            String id2 = SVPManager.getSVPManager().getRBS().getName();
            String id3 = SVPManager.getSVPManager().getRBS().getName();
            GRNTree c1 = GRNTreeFactory.getGRNTree(
                    "PspaRK:Prom; " + id1 + ":RBS; SpaR:CDS; " + id2 + ":RBS; SpaK:CDS; BO_4296:Ter; " +
                            "PspaS:Prom; " + id3 + ":RBS; GFP_rrnb:CDS; BO_4296:Ter");
            SvpModule m = parser.createModuleFromGrnTree(SBIdentity.getIdentity("http://synbad.org", "testModel", "1.0"), c1);
            SBWorkspaceGraph g = new SBWorkspaceGraph(ws, m.getContexts());
            System.out.println(Arrays.stream(m.getContexts()).map(URI::toASCIIString).reduce("", (a, b)-> a + " " + b));
            ws.getObjects(SvpModule.class, m.getContexts()).forEach(s -> System.out.println(s));
            g.getTraversal().v(ws.getObjects(SvpModule.class, m.getContexts()).toArray(new SvpModule[]{}))
                    .as("P")
                    .e(SBDirection.OUT, SynBadTerms.SynBadEntity.hasChild).v(SBDirection.OUT, Svp.class).as("SVP1")
                    .e(SBDirection.IN, SynBadTerms.SynBadInteraction.isSvpParticipant).v(SBDirection.IN, SvpInteraction.class)
                    .filter(svi -> !svi.isInternal())
                    .filter(svi -> svi.getInteractionType() == InteractionType.Phosphorylation)
                    .e(SBDirection.OUT, SynBadTerms.SynBadInteraction.isSvpParticipant).v(SBDirection.OUT, Svp.class).hasNotLabel("SVP1").as("SVP")
                    .e(SBDirection.IN, SynBadTerms.SynBadEntity.hasChild).v(SBDirection.IN, SvpModule.class)
                    .hasLabel("P").select(Svp.class, "SVP")
                    .forEachRemaining(svp -> {System.out.println("svp: " + svp.getDisplayId() + ": " + Arrays.stream(svp.getContexts()).map(URI::toASCIIString).reduce("", (a, b)-> a + " " + b));});
            System.out.println("Done");
            ws.removeContext(contexts[0]);
            System.out.println(ws.getContextIds().length);
        }



    }
}
