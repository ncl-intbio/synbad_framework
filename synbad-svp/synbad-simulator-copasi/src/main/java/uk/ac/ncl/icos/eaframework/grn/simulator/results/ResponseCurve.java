/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator.results;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a series of outputs produced in response in a series of inputs.
 * @author owengilfellon
 */
public class ResponseCurve  {

    private final List<Double> orderedKeys;
    private final List<Double> orderedValues;
    private final String input;
    private final String output;

    public ResponseCurve(String input, String output, int increment, List<Double> outputValues) {
        this(input, output, range(increment, outputValues.size()), outputValues);
    }
    
     public ResponseCurve(String input, String output, int increment, double[] responseCurve) {
        this(input, output, increment, Arrays.stream(responseCurve).boxed().collect(Collectors.toList()));
    }
     
    public ResponseCurve(String input, String output, List<Double> inputValues, List<Double> outputValues) {
        this.input = input;
        this.output = output;
        this.orderedKeys = inputValues;
        this.orderedValues = outputValues;
    }
    
    /**
     * 
     * @return the input species label
     */
    public String getInput() {
        return input;
    }

    /**
     * 
     * @return the output species label
     */
    public String getOutput() {
        return output;
    }
    
    /**
     * 
     * @return input values
     */
    public List<Double> getInputs() {
        return orderedKeys;
    }

    /**
     * 
     * @return the output values for each value of input
     */
    public List<Double> getOutputs() {
        return orderedValues;
    }
    
    public Double getMaxInput() {
        return orderedKeys.get(orderedKeys.size() - 1);
    }
    
    /**
     * 
     * @return The value of output for the maximum input
     */
    public Double getMaxInducedOutput() {
        return orderedValues.get(orderedValues.size() - 1);
    }
    
    private static List<Double> range(double increment, double size) {
        List<Double> list = new LinkedList<>();
        for (int i = 0; i <= size; i++) {
            list.add(increment * i);
        }

        return list;
    }
}