/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator.results;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a single time series from a simulation for a single species.
 * @author owengilfellon
 */
public class TimeCourseTrace extends ArrayList<Double> {

    private final String label;

    public TimeCourseTrace(String label, List<Double> trace) {
        super(trace);
        this.label = label;
    }

    public TimeCourseTrace(String label, double[] trace) {
        this.label = label;
        addAll(Arrays.stream(trace).boxed().collect(Collectors.toList()));
    }

    public String getLabel() {
        return label;
    }
}