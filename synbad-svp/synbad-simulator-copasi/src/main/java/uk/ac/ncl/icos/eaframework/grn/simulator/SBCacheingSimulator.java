package uk.ac.ncl.icos.eaframework.grn.simulator;

import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import java.util.List;

/**
 *
 * @author ogilfellon
 */
public interface SBCacheingSimulator extends SBSbmlSimulator {
    
    List<TimeCourseTraces> getAllResults();
    
    void clearResults();
    
}
