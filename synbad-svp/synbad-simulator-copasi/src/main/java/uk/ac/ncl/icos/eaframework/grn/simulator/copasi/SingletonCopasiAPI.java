/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;

import org.COPASI.CCopasiDataModel;
import org.COPASI.CCopasiRootContainer;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;

/**
 *
 * @author owengilfellon
 */
public class SingletonCopasiAPI {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SingletonCopasiAPI.class);
    private static SingletonCopasiAPI api;

    private SingletonCopasiAPI() { }
    
    public static SingletonCopasiAPI getCopasiAPI() {
        if(api == null)
            api = new SingletonCopasiAPI();
        return api;
    }
    
    public CCopasiDataModel getDataModel() {
        synchronized(SBMLDocument.class)  {
            return CCopasiRootContainer.addDatamodel();
        }
    }
    
    public void  removeDataModel(CCopasiDataModel dataModel) {
        synchronized(SBMLDocument.class)  {
            if (!CCopasiRootContainer.removeDatamodel(dataModel)) {
                LOGGER.error("Could not remove data model: {}", dataModel.getFileName());
            }

            dataModel.deleteOldData();
            dataModel.delete();
            CCopasiRootContainer.getDatamodelList().cleanup();
        }
    }
    
    public void addSBMLFromString(CCopasiDataModel datamodel, String sbml) throws Exception {
        synchronized(SBMLDocument.class)  {
            datamodel.importSBMLFromString(sbml);
        }
    }
}
