/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator.results;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Collects a single time series for a number of species.
 * @author owengilfellon
 */
public class TimeCourseTraces  {
    
    private final Map<String, TimeCourseTrace> traces;

    public TimeCourseTraces(List<String> metabolites, double[][] results) {
        this.traces = getTimecourses(metabolites, results);
    }
    
    public TimeCourseTraces(Map<String, TimeCourseTrace> results) {
       this.traces = results;
    }
    
    public TimeCourseTraces(List<String> species, List<TimeCourseTrace> traces) {
       Map<String, TimeCourseTrace> r = new HashMap<>();
       for(int i = 0; i < species.size(); i++) {
           r.put(species.get(i), traces.get(i));
       }
       this.traces = r;
    }
     
    public Set<String> getSpecies() {
        return traces.keySet();
    }
    
    public TimeCourseTrace getTimeCourse(String species) {
        return traces.get(species);
    }
    
    private Map<String, TimeCourseTrace> getTimecourses(List<String> metabolites, double[][] results) {
        
        Map<String, TimeCourseTrace> traces = new HashMap<>();
        
        for(int i = 0; i < metabolites.size(); i++) {
            traces.put(metabolites.get(i), getTimecourse(metabolites.get(i), results[i]));
        }
        
        return traces;
    }
    
    private TimeCourseTrace getTimecourse(String metabolite, double[] results) {
        
        List<Double> trace = new LinkedList<>();
        
        for(double d : results) {
            trace.add(d);
        }
        
        return new TimeCourseTrace(metabolite, trace);
    }
    
}
