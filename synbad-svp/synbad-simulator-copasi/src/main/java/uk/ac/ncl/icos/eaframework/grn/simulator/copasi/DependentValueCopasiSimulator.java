package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import uk.ac.ncl.icos.eaframework.grn.simulator.AbstractSBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBCacheingSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

/**
 * 
 * @author ogilfellon
 */
public class DependentValueCopasiSimulator extends ResponseCurveSimulator implements SBCacheingSimulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DependentValueCopasiSimulator.class);

    private double increments;

    public DependentValueCopasiSimulator(   int duration,
                                            int runlength,
                                            int increments,
                                            int maxMetaboliteConcentration,
                                            String independentMetabolite,
                                            String dependantMetabolite) {
        super(duration, runlength, maxMetaboliteConcentration, independentMetabolite, dependantMetabolite);
        this.increments = increments;
    }

    @Override
    public synchronized boolean run() {

        try (SBCopasiModel m = new SBCopasiModel()) {
            m.setDuration(duration);
            m.setStepNumber(stepNumber);
            m.setSbml(sbml);
            m.setResultMetabolites(Arrays.asList(getDependentMetabolite(), getIndependentMetabolite()));

            List<TimeCourseTrace> dependentMetaboliteValues = new ArrayList<>();
            List<Double> independentMetaboliteValues = new ArrayList<>();

            for (int increment = 0; increment < increments; increment++) {
                double metaboliteValue = increment * ((double)maxIncrementValue / increments);
                independentMetaboliteValues.add(metaboliteValue);
                m.setMetaboliteInitialValue(getIndependentMetabolite(), metaboliteValue);
                if(!m.run())
                    return false;
            }

            this.dependentMetaboliteValues = dependentMetaboliteValues;
            this.independentMetaboliteValues = new TimeCourseTrace(getIndependentMetabolite(), independentMetaboliteValues);

            allResults = m.getTimeCourseTraces();
        } catch (Exception ex) {
            LOGGER.error("Could not perform simulation", ex);
            return false;
        }

        return true;
    }

    @Override
    public TimeCourseTraces getResults() {
        if(!getAllResults().isEmpty())
            return getAllResults().get(getAllResults().size() - 1);
        return null;
    }
}
