/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public interface SBSbmlSimulatorFactory<T extends SBSbmlSimulator> extends Serializable {
    
    public T getSimulator(int duration, int runtime);
    
}
