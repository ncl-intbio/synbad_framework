/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator;

/**
 *
 * @author owengilfellon
 * @param <T> Simulation
 * @param <V> Results
 */
public interface SbResultsProcessor<T, V> {
    
    V processResults(T simulation);
    Class<V> getResultsClazz();
    
}
