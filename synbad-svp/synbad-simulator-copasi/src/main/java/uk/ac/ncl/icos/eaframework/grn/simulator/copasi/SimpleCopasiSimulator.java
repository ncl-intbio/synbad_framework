package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;
import java.util.*;

import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.AbstractSBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.core.util.Config;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;

/**
 * Basic simulator that uses COPASI. After each run, @see uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator#getTimeSeries()\
 * is cleared and replaced with the results of the completed simulation.
 * @author ogilfellon
 */
public class SimpleCopasiSimulator extends AbstractSBSbmlSimulator {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCopasiSimulator.class);
    private final Map<String, Double> initialConcentrations = new HashMap<>();

    public SimpleCopasiSimulator(int duration, int steps) {
        super(duration, steps);
    }

    public SimpleCopasiSimulator(int duration, int steps, Map<String, Double> initialConcentrations) {
        super(duration, steps);
        this.initialConcentrations.putAll(initialConcentrations);
    }

    @Override
    public Config getConfig() {
        Config.ConfigBuilder b = Config.create()
                .addClassName(getClass().getName())
                .addInt(duration)
                .addInt(stepNumber);

        for(String key: initialConcentrations.keySet()) {
            b = b.addMappedPair(key, initialConcentrations.get(key));
        }

        return b.build();
    }

    public void addInitialConcentration(String metabolite, Double value) {
        this.initialConcentrations.put(metabolite, value);
    }

    @Override
    public boolean run() {
        try (SBCopasiModel m = new SBCopasiModel()) {
            m.setDuration(duration);
            m.setStepNumber(stepNumber);
            m.setSbml(sbml);
            m.setResultMetabolites(m.getMetaboliteNames());

            for (String metabolite : initialConcentrations.keySet()) {
                m.setMetaboliteInitialValue(metabolite, initialConcentrations.get(metabolite));
            }

            if(m.run()) {
                List<TimeCourseTraces> traces = m.getTimeCourseTraces();
                if (!traces.isEmpty())
                    results = traces.get(traces.size() - 1);
                return true;
            } else return false;
        } catch (Exception ex) {
            LOGGER.error("Simulation failed", ex);
            return false;
        }
    }
}
