package uk.ac.ncl.icos.eaframework.grn.simulator;

import java.util.HashMap;
import java.util.Map;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author ogilfellon
 */
public abstract class AbstractSBSbmlSimulator implements SBSbmlSimulator {

    protected int duration;
    protected int stepNumber;
    protected String sbml;
    protected TimeCourseTraces results = null;
    /**
     * Default constructor. Sets duration to 15,000 and runlength to 1000
     */
    public AbstractSBSbmlSimulator() {
        this(15000, 1000);
    }

    @Override
    public Config getConfig() {
        return Config.create()
            .addClassName(getClass().getName())
            .addInt(duration)
            .addInt(stepNumber).build();
    }

    public AbstractSBSbmlSimulator(int duration, int stepNumber) {
        this.duration = duration;
        this.stepNumber = stepNumber;
    }

    @Override
    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public void setStepNumber(int runlength) {
        this.stepNumber = runlength;
    }

    @Override
    public void setModel(String sbml)  {
        this.sbml = sbml;
    }

    @Override
    public TimeCourseTraces getResults() {
        return results;
    }

    @Override
    public abstract boolean run();
    
}
