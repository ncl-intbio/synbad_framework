package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import uk.ac.ncl.icos.eaframework.grn.simulator.AbstractSBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBCacheingSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author ogilfellon
 */
public class DynamicTimeCopasiSimulator extends ResponseCurveSimulator implements SBCacheingSimulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DynamicTimeCopasiSimulator.class);

    private double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    private final int MIN_NUMBER_OF_SIMULATIONS = 50;
    private int increment = 50;

    public DynamicTimeCopasiSimulator(
            int duration,
            int runlength,
            int increment,
            double minChange,
            String independentMetabolite,
            String dependantMetabolite) {
        super(duration, runlength, 0, independentMetabolite, dependantMetabolite);
        this.increment = increment;
        this.MIN_CHANGE = minChange;
    }

    public DynamicTimeCopasiSimulator(
            int duration,
            int runlength,
            String independentMetabolite,
            String dependantMetabolite) {
        super(duration, runlength, 0, independentMetabolite, dependantMetabolite);
    }



    @Override
    public Config getConfig() {
        return Config.create().addClassName(getClass().getName())
                .addInt(duration)
                .addInt(stepNumber)
                .addInt(increment)
                .addDouble(getMinChange())
                .addString(getIndependentMetabolite())
                .addString(getDependentMetabolite())
                .build();
    }

    public double getIncrement() {
        return increment;
    }

    public double getMinChange() {
        return MIN_CHANGE;
    }

    @Override
    public boolean run() {


            try (SBCopasiModel m = new SBCopasiModel()) {

                m.setDuration(duration);
                m.setStepNumber(stepNumber);
                m.setSbml(sbml);
                m.setResultMetabolites(Arrays.asList(getDependentMetabolite(), getIndependentMetabolite()));

                double previousValue = 0;
                double currentValue = 0;
                double metaboliteValue = 0;

                // while the change in output still increases above a certain value

                List<TimeCourseTrace> dependantValues = new ArrayList<>();
                List<Double> independents = new ArrayList<>();

                // for each

                do {

                    independents.add(metaboliteValue);
                    m.setMetaboliteInitialValue(getIndependentMetabolite(), metaboliteValue);

                    if (m.run()) {
                        allResults = m.getTimeCourseTraces();

                        if (!allResults.isEmpty()) {
                            TimeCourseTrace lastResults = allResults.get(allResults.size() - 1).getTimeCourse(getDependentMetabolite());
                            if (!lastResults.isEmpty())
                                dependantValues.add(lastResults);
                        }

                        previousValue = currentValue;
                        TimeCourseTrace lastDependentTrace = dependantValues.get(dependantValues.size() - 1);
                        currentValue = lastDependentTrace.get(lastDependentTrace.size() - 1);
                        metaboliteValue += increment;
                        this.results = allResults.get(allResults.size() - 1);
                    } else {
                        return false;
                    }

                } while (shouldSimulate(currentValue, previousValue, dependantValues.size()));

                this.dependentMetaboliteValues = dependantValues;
                this.independentMetaboliteValues = new TimeCourseTrace(getIndependentMetabolite(), independents);

            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
                ex.printStackTrace();
                return false;
            }

        return true;
    }
    
    private boolean shouldSimulate(double currentValue, double previousValue, int simulations) {
        return ((((currentValue - previousValue) / previousValue) / (double)increment) * 100) > MIN_CHANGE ||
                simulations < MIN_NUMBER_OF_SIMULATIONS;
    }
}
