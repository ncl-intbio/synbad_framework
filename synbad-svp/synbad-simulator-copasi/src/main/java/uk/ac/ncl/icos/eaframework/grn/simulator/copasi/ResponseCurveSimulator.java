package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.AbstractSBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBCacheingSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author ogilfellon
 */
public abstract class ResponseCurveSimulator extends AbstractSBSbmlSimulator implements SBCacheingSimulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseCurveSimulator.class);

    protected int maxIncrementValue;

    private String independentMetabolite;
    private String dependentMetabolite;

    protected transient List<TimeCourseTrace> dependentMetaboliteValues;
    protected transient TimeCourseTrace independentMetaboliteValues;

    protected transient List<TimeCourseTraces> allResults = new LinkedList<>();

    public ResponseCurveSimulator(int duration, int runlength, int maxMetaboliteConcentration, String independentMetabolite, String dependantMetabolite) {
        super(duration, runlength);
        this.maxIncrementValue = maxMetaboliteConcentration;
        this.independentMetabolite = independentMetabolite;
        this.dependentMetabolite = dependantMetabolite;
    }

    public String getIndependentMetabolite()
    {
        return this.independentMetabolite;
    }

    public String getDependentMetabolite() { return this.dependentMetabolite; }

    /**
     *
     * @return a series of time course traces for a single dependent metabolite.
     */
    public synchronized List<TimeCourseTrace> getDependentMetaboliteValues()
    {
        return dependentMetaboliteValues;
    }

    /**
     *
     * @return a time course trace, representing the series of fixed values used
     * for the independent metabolite across a range of simulations, each value
     * corresponding to each trace in dependentMetabolites
     */
    public synchronized TimeCourseTrace getIndependentMetaboliteValues()
    {
        return independentMetaboliteValues;
    }

    @Override
    public List<TimeCourseTraces> getAllResults() { return allResults; }

    @Override
    public void clearResults() {
        this.allResults.clear();
    }

    @Override
    public TimeCourseTraces getResults() {
        if(!getAllResults().isEmpty())
            return getAllResults().get(getAllResults().size() - 1);
        return null;
    }
}
