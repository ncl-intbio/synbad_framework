package uk.ac.ncl.icos.eaframework.grn.simulator;

import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.core.util.Configured;

import java.io.Serializable;

/**
 *
 * @author ogilfellon
 */
public interface SBSbmlSimulator extends Configured {
    
    /**
     * Set the duration of the simulation in seconds.
     * @param duration 
     */
    public void setDuration(int duration);
    
    /**
     * Set the number of steps to simulate. Higher values provides higher
     * resolution results.
     * @param steps
     */
    public void setStepNumber(int steps);
    
    /**
     * Sets the model, passing on any exceptions that are thrown by COPASI while
     * parsing the model.
     * @param sbml
     * @throws Exception 
     */
    public void setModel(String sbml) throws Exception;

    boolean run();
    
    TimeCourseTraces getResults();
    
}
