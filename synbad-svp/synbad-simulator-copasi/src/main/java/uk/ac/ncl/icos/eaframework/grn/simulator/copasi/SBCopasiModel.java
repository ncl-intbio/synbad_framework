package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;



import org.COPASI.*;
import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;


/**
 *
 * @author owengilfellon
 */
public class SBCopasiModel implements AutoCloseable {
    
    private static final SingletonCopasiAPI COPASI_API = SingletonCopasiAPI.getCopasiAPI();
    private static final Logger LOGGER = LoggerFactory.getLogger(SBCopasiModel.class);

    private CCopasiDataModel datamodel;
    private CModel model;
    private CTrajectoryTask trajectoryTask;
    private CTrajectoryProblem problem;
    private CTimeSeries timeSeries;

    private HashMap<String, Double> metaboliteInitialValues;
    private List<String> metabolitesToCapture;
    private List<TimeCourseTraces> tcresult = new LinkedList<>();

    private int stepNumber;
    private int duration;

    /**
     * Sets the model.
     * @param sbml
     * @throws Exception 
     */
    public void setSbml(String sbml) throws Exception {
            datamodel = COPASI_API.getDataModel();
            COPASI_API.addSBMLFromString(datamodel, sbml);
            model = datamodel.getModel();
            metaboliteInitialValues = new HashMap<>();
    }

    public List<String> getMetaboliteNames()
    {
            List<String> names = new ArrayList<>();
            for (int i = 0; i < model.getMetabolites().size(); i++) {
                names.add(model.getMetabolite(i).getObjectName());
            }
            return model == null ? new ArrayList<>() : names;
    }

    public void updateModel() {

        if(model == null)
            LOGGER.error("Cannot update null model");


            ObjectStdVector container = new ObjectStdVector();

            for(String s : metaboliteInitialValues.keySet()) {
                CMetab metab = model.getMetabolite(s);
                if(metab != null) {
                    metab.setInitialConcentration(metaboliteInitialValues.get(metab.getObjectName()));
                    container.add(metab.getInitialConcentrationReference());
                    metab.cleanup();
                    metab.delete();
                }
            }

            if(!container.isEmpty()) {
                model.updateInitialValues(container);
                model.setInitialTime(0.0);
                container.clear();
            }

            container.delete();
        }

    private void debugState(CState state, String msg) {
        StringBuilder b = new StringBuilder();
        b.append(msg);
        b.append(" | ").append(state.isValid()).append(" [ ");
        for(long l = 0; l < state.getNumIndependent(); l++) {
            b.append(state.getIndependent(l)).append(" ");

        }
        b.append(" ]");
        LOGGER.debug(b.toString());
    }

    public boolean steadyState() {

        if (datamodel == null)
            LOGGER.error("Cannot run null data model");

        synchronized(SBMLDocument.class)  {


            CSteadyStateTask t = (CSteadyStateTask) datamodel.getTask(CCopasiTask.steadyState);
            assert(t != null);
            t.setUpdateModel(true);
            LOGGER.debug("Result: {}", t.getResult());


            //CSteadyStateProblem p = new CSteadyStateProblem(datamodel);

            try {

                if(LOGGER.isDebugEnabled()) {
                    debugState(datamodel.getModel().getState(), "Original: ");
                }

          /*      t.getResult();

                t.setInitialState();*/

               CState state = t.getState();

                if(state == null) {
                    LOGGER.debug("Could not find steady state");
                    close();
                    return false;
                }

                if(LOGGER.isDebugEnabled()) {
                    debugState(datamodel.getModel().getState(), "Updated: ");
                }
                //datamodel.getModel().setState(state);
            } catch (java.lang.Exception ex) {
                LOGGER.error("Running the time course simulation failed.");
                String lastError = t.getProcessError();
                if (lastError.length() > 0) {
                    LOGGER.error("{}: {}", ex.getMessage(),lastError);
                }
                close();
                return false;
            }
        }

        return  true;
    }

    public boolean run() {

        // construct trajectory task

        if (datamodel == null)
            LOGGER.error("Cannot run null data model");

            updateModel();

            trajectoryTask = (CTrajectoryTask) datamodel.getTask("Time-Course");
            assert trajectoryTask != null;

            trajectoryTask.setMethodType(CCopasiMethod.deterministic);
            trajectoryTask.getProblem().setModel(model);
            trajectoryTask.setScheduled(true);

            problem = (CTrajectoryProblem) trajectoryTask.getProblem();
            problem.setStepNumber(stepNumber);
            problem.setDuration(duration);
            problem.setTimeSeriesRequested(true);

            try {
                boolean result = trajectoryTask.processWithOutputFlags(true, (int) CCopasiTask.ONLY_TIME_SERIES);
                if (!result) {
                    LOGGER.error("An error occured while running the time course simulation.");
                    if (CCopasiMessage.size() > 0) {
                        LOGGER.error(CCopasiMessage.getAllMessageText(true));
                    }
                    close();
                    return false;
                }
            } catch (java.lang.Exception ex) {
                LOGGER.error("Running the time course simulation failed.");
                String lastError = trajectoryTask.getProcessError();
                if (lastError.length() > 0) {
                    LOGGER.error("{}: {}", ex.getMessage(), lastError);
                }
                close();
                return false;
            }


            timeSeries = trajectoryTask.getTimeSeries();
            double[][] allRecordedTimecourses = new double[metabolitesToCapture.size()][];
            int recordedSteps = (int) timeSeries.getRecordedSteps();

            for (int m = 0; m < metabolitesToCapture.size(); m++) {
                double[] timecourse = new double[recordedSteps];
                String metab = metabolitesToCapture.get(m);
                CMetab mtb = model.getMetabolite(metab);
                long mtbId = model.findMetabByName(metab) + 1;
                for (int j = 0; j < recordedSteps; j++) {
                    if (mtb != null)
                        timecourse[j] = timeSeries.getConcentrationData(j, mtbId);
                }
                if (mtb != null)
                    mtb.delete();

                allRecordedTimecourses[m] = timecourse;
            }

            tcresult.add(new TimeCourseTraces(metabolitesToCapture, allRecordedTimecourses));

        return true;
    }

    public void close() {


            if(timeSeries != null) {
                timeSeries.finish();
                timeSeries.clear();
                timeSeries.delete();
            }

            if(problem != null) {
                problem.clear();
                problem.delete();
            }

            if(trajectoryTask != null) {
                trajectoryTask.cleanup();
                trajectoryTask.delete();
            }

            if(model != null) {
                model.clearRefresh();
                model.delete();
            }

            if(datamodel!=null) {
                COPASI_API.removeDataModel(datamodel);
            }

            datamodel = null;
            model = null;
            timeSeries = null;
            problem = null;
            trajectoryTask = null;

            metaboliteInitialValues = new HashMap<>();
    }


    /**
     * A list of the metabolites to be included in the Results.
     * @param metabolites
     */
    public void setResultMetabolites(List<String> metabolites) {
        this.metabolitesToCapture = metabolites;
    }

    public void clearResultMetabolites() {
        this.metabolitesToCapture.clear();
    }

    /**
     * Set the duration of the simulation in seconds.
     * @param duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Set the number of steps to simulate. Higher values provides higher
     * resolution results.
     * @param stepNumber
     */
    public void setStepNumber(int stepNumber) {
        this.stepNumber =  stepNumber > 1 ? stepNumber - 1 : stepNumber;
    }


    public void setMetaboliteInitialValue(String metabolite, double metaboliteValue)
    {
        metaboliteInitialValues.put(metabolite, metaboliteValue);
    }
    
    public double getMetaboliteInitialValue(String metabolite)
    {
        return metaboliteInitialValues.get(metabolite);
    }

    /**
     * Returns the results of all runs of this model
     * @return 
     */
    public List<TimeCourseTraces> getTimeCourseTraces() {
        return tcresult;
    }
}

