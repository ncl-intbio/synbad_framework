/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.xml.stream.XMLStreamException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

/**
 *
 * @author owengilfellon
 */
public class SimpleCopasiSimulatorTest {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCopasiSimulatorTest.class);
    private static final int STEPS = 1000;
    private static final int DURATION_SECONDS = 1000;
    
    public SimpleCopasiSimulatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class SimpleCopasiSimulator.
     */
    @Test
    public void testSimpleSimulator() {
        SBSbmlSimulator s = new SimpleCopasiSimulator(DURATION_SECONDS, STEPS);
        testSimulator(s, 9);
    }
    
    @Test
    public void testDependentValueSimulator() {
        DependentValueCopasiSimulator s = new DependentValueCopasiSimulator(DURATION_SECONDS, STEPS, 100, 10000, "Subtilin", "GFP_rrnb");
        testSimulator(s, 2);
    }
    
    @Test
    public void testDynamicValueSimulator() {
        DynamicTimeCopasiSimulator s = new DynamicTimeCopasiSimulator(DURATION_SECONDS, STEPS, 50, 50,  "Subtilin", "GFP_rrnb");
        testSimulator(s, 2);
    }
    
    private void testSimulator(SBSbmlSimulator s, int expectedSpecies) {
        try {
            SBMLDocument doc = SBMLReader.read(new File("SubtilinReceiver.xml"));
            
            s.setModel(new SBMLHandler().GetSBML(doc));
            s.run();
            
            Assert.assertEquals("Should have results for all species", expectedSpecies, s.getResults().getSpecies().size());
            
            String species = s.getResults().getSpecies().iterator().next();
            TimeCourseTrace trace = s.getResults().getTimeCourse(species);
                        
            Assert.assertEquals("Results should contain  all steps", STEPS, trace.size());
            
//            s.getResults().getSpecies().stream().forEach(
//                m -> System.out.println(Arrays.toString(s.getResults().getTimeCourse(m).toArray(new Double[] {})))//LOGGER.info("{}: {}", m, Arrays.toString(s.getResults().getTimeCourse(m).toArray(new Double[] {})))
//            );
        } catch (XMLStreamException | IOException ex) {
            LOGGER.error("Could not simulate SBML file", ex);
            Assert.fail("XML/IO Exception");
        } catch (Exception ex) {
            LOGGER.error("Could not simulate SBML file", ex);
            Assert.fail(ex.getLocalizedMessage());
        }
    }
}
