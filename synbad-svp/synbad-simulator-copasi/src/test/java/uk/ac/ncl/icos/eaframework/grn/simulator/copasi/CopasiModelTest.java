/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.simulator.copasi;

import org.junit.*;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author owengilfellon
 */
public class CopasiModelTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CopasiModelTest.class);
    private static final int STEPS = 1000;
    private static final int DURATION_SECONDS = 1000;

    public CopasiModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testModel() {
        try {
            SBMLDocument doc = SBMLReader.read(new File("SubtilinReceiver.xml"));

            SBCopasiModel m = new SBCopasiModel();
            m.setSbml(new SBMLHandler().GetSBML(doc));
//            m.steadyState();
//            m.getMetaboliteNames().forEach(n ->
//                   LOGGER.debug("[ {} ] {}", m.getMetaboliteInitialValue(n), n));
        } catch (XMLStreamException | IOException ex) {
            LOGGER.error("Could not simulate SBML file", ex);
            Assert.fail("XML/IO Exception");
        } catch (Exception ex) {
            LOGGER.error("Could not simulate SBML file", ex);
            Assert.fail(ex.getLocalizedMessage());
        }
    }
}
