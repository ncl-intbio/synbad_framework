/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * @author owengilfellon
 */
public class SynBadFormatter extends Formatter {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss:SS");
    
    @Override
    public String format(LogRecord record) {

        int lastIndexOfPeriod = record.getSourceClassName().lastIndexOf(".");
        
        String className = lastIndexOfPeriod == -1 ? 
                record.getSourceClassName() :
                record.getSourceClassName().substring(lastIndexOfPeriod);
        
        Date date = new Date(record.getMillis());
        
        return String.format("[%1$s] %2$s: \t%3$s \t%3$s", 
                DATE_FORMAT.format(date), 
                record.getLevel(),
                className,
                record.getMessage());
    }
    
}
