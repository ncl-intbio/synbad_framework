val BASE = 10;

# --------------------------------------------------------------
# Comment lines start with #s, Constraint lines start with %
# --------------------------------------------------------------

% A[12800] BASE < O         # STL Constraint
% |A - O| = 0               # Boolean constraint - IDs of CDSs also refer to protein concentrations
% A~P > smol^2              # Rules that specify a phosphorylation relay in which the concentration
% O~P > A~P^2               # of the signal is squared at each step.
module sensor1(O:CDS) {
    Prom; RBS; A:CDS; RBS; O; Ter;
    smol -* A;
    A~P -* O;
}

s

# ========================================

# Implement for later chapter?

#=========================================

kmValues = { 0.213, 0.23312, 0.12523, 0.34542, 0.73453, 0.2349423, 0.2392342, 0.23, 0.64223, 0.9234, 0.2384, 0.58942 }

module reporter(I:CDS, D:Prom) {

    # --------------------------------------------------------------
    # Alternatives can be specified within curly braces using bars
    # e.g. {O1|O2|O3}:CDS or Km={0.5234|0.1233}. Can also pass in
    # array e.g. Km={kmValues} or range e.g { 0.5234->0.6233 }
    # --------------------------------------------------------------

    A:Prom; RBS; {activator|repressor}; Ter;
    I -> A (Km=0.123123);
    activator -> D (km = kmValues);
    repressor -| D (km = kmValues);

}

sensor(A); reporter(A, B);

module inverter(I:CDS, O:CDS) {
    Prom(0.123); A:Op; RBS(0.323); O:CDS(0.0012); Ter;
    I -| A (0.0123);
}

// ids for modules?
tu1:inverter(C, D); tu2:inverter(D, C); tu3:reporter(C~P, E);

subtilin -* C (kd=0.0012, kf=0.123, kdeP=0.0010);

// nested modules

module repressilator(I:CDS, O:CDS) {
    inverter(I, B); inverter(B, C); inverter(C, I);
    reporter(I~P, O);

    x -* I (kd=0.0024, kf=0.54123, kdeP=0.4351);
}

repressilator(E, F);


# --------------------------------------------------------------
# Members of modules can be referred to if they are identified
# IMPLEMENT LATER
# --------------------------------------------------------------

module sensor {
    X:Prom; RBS; A:CDS; RBS; O; Ter;
    smol -* A;
    A~P -* O;
}

smol -* sensor[A]
tu1[A~P] -* tu1[B]
tu1[B~P] -* tu2[C]
tu2[C~P] -* tu3[D]
tu3[D] -> tu1[X]

smol -* tu1.A
tu1.A~P -* tu1.B
tu1.B~P -* tu2.C
tu2.C~P -* tu3.D
tu3.D -> tu1.X

root.tu1.mod.



# --------------------------------------------------------------
# Defined connection points for convenience, but can address
# any members of module using explicit mapping
# --------------------------------------------------------------

sensor2(a); sensor2[A=b, O=a]

# --------------------------------------------------------------
inverter[I=a, O=b]; inverter[I=b, O=c]; inverter[I=c, O=a]
# --------------------------------------------------------------