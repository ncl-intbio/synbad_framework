/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public abstract class AbstractNode implements GRNTreeNode, Serializable {

    transient protected static SVPManager m;
    protected InterfaceType interfaceType;
    protected GRNTreeNode parent = null;
    protected String name = null;
    protected String displayName = null;
    protected NodeManager nodeManager = null;
    protected String id = null;

    protected AbstractNode() {
        m = SVPManager.getSVPManager();
    }

    protected AbstractNode(InterfaceType interfaceType) {
        m = SVPManager.getSVPManager();
        this.interfaceType = interfaceType;
        this.id = IdManager.getID();
    }

    @Override
    public void setNodeManager(NodeManager nm){
        nodeManager = nm;
        if(this instanceof BranchNode) {

            BranchNode bn = (BranchNode) this;

            for(GRNTreeNode n:bn.getChildren()) {
                n.setNodeManager(nm);
            }
        }
    }

    @Override
    public InterfaceType getInterfaceType() {
        return interfaceType;
    }

    @Override
    public void setInterfaceType(InterfaceType interfaceType) {
        this.interfaceType = interfaceType;
    }

    @Override
    public GRNTreeNode getParent() {
        return parent;
    }

    @Override
    public void setParent(GRNTreeNode parent) {
        this.parent = parent;
    }

    @Override
    public String getName() {
        if(name==null) {
            return getId();
        }
        else {
           return name;
        } 
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

//    @Override
//    public boolean hasSiblings() {
//        if(this.getParent() == null) {
//            return false;
//        }
//        else {
//            return this.getParent().getChildren().size() > 1;
//        }
//    }

//    @Override
//    public List<GRNTreeNode> getSiblings() {
//
//        List<GRNTreeNode> siblings = new ArrayList<>();
//
//        if(this.getParent() == null) {
//            return siblings;
//        }
//        else {
//
//            siblings = this.getParent().getChildren();
//            siblings.remove(this);
//            return siblings;
//        }
//    }

    @Override
    public int getDepth() {
        int depth = 1;
           GRNTreeNode n = this;
           while(n.getParent()!=null)
           {
               depth++;
               n = n.getParent();
           }
           return depth;      
    }

    @Override
    public String getId()
    {
        return this.id;
    }

    @Override
    public void replaceNode(GRNTreeNode node) throws Exception {
        if(parent!=null) {
            parent.addNode(parent.getChildren().indexOf(this), node);
            parent.removeNodeAndDescendents(this);
        }
        else {
            throw new Exception("Cannot call method on Root Node");
        }
    }

    public void alert(GRNTreeNode node) {
        nodeManager.alert(node);
    }

    @Override
    public boolean addNode(GRNTreeNode node)  { return false; }

    @Override
    public void addNode(int index, GRNTreeNode node) throws Exception { throw new Exception("Method can not be called on a Leaf Node"); }

    @Override
    public boolean removeNode(GRNTreeNode node) { throw new UnsupportedOperationException("Not supported by LeafNode."); }

    @Override
    public boolean removeNodeAndDescendents(GRNTreeNode node) { throw new UnsupportedOperationException("Not supported yet."); }

    @Override
    public void setNodes(List<GRNTreeNode> nodes) { throw new UnsupportedOperationException("Not supported yet."); }

    @Override
    public boolean isTranscriptionUnit() { return false; }

    @Override
    public boolean isLeafNode() { return false; }

    @Override
    public boolean isBranchNode() { return false; }

    @Override
    public boolean isPrototype() {return false;}

    @Override
    public Part getSVP() {
        return null;
    }
    
    
    
    

}
