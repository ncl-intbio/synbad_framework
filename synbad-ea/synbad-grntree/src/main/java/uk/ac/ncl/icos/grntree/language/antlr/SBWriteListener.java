// Generated from /Users/owengilfellon/Documents/Code/Misc/synbad_framework/synbad-ea/synbad-grntree/src/main/antlr4/uk/ac/ncl/icos/grntree/language/antlr/SBWrite.g4 by ANTLR 4.8
package uk.ac.ncl.icos.grntree.language.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SBWriteParser}.
 */
public interface SBWriteListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svp}.
	 * @param ctx the parse tree
	 */
	void enterSvp(SBWriteParser.SvpContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svp}.
	 * @param ctx the parse tree
	 */
	void exitSvp(SBWriteParser.SvpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svm}.
	 * @param ctx the parse tree
	 */
	void enterSvm(SBWriteParser.SvmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svm}.
	 * @param ctx the parse tree
	 */
	void exitSvm(SBWriteParser.SvmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#leftParticipant}.
	 * @param ctx the parse tree
	 */
	void enterLeftParticipant(SBWriteParser.LeftParticipantContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#leftParticipant}.
	 * @param ctx the parse tree
	 */
	void exitLeftParticipant(SBWriteParser.LeftParticipantContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#rightParticipant}.
	 * @param ctx the parse tree
	 */
	void enterRightParticipant(SBWriteParser.RightParticipantContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#rightParticipant}.
	 * @param ctx the parse tree
	 */
	void exitRightParticipant(SBWriteParser.RightParticipantContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#interaction}.
	 * @param ctx the parse tree
	 */
	void enterInteraction(SBWriteParser.InteractionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#interaction}.
	 * @param ctx the parse tree
	 */
	void exitInteraction(SBWriteParser.InteractionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#instanceList}.
	 * @param ctx the parse tree
	 */
	void enterInstanceList(SBWriteParser.InstanceListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#instanceList}.
	 * @param ctx the parse tree
	 */
	void exitInstanceList(SBWriteParser.InstanceListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svmDef}.
	 * @param ctx the parse tree
	 */
	void enterSvmDef(SBWriteParser.SvmDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svmDef}.
	 * @param ctx the parse tree
	 */
	void exitSvmDef(SBWriteParser.SvmDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svpDef}.
	 * @param ctx the parse tree
	 */
	void enterSvpDef(SBWriteParser.SvpDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svpDef}.
	 * @param ctx the parse tree
	 */
	void exitSvpDef(SBWriteParser.SvpDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svpParameter}.
	 * @param ctx the parse tree
	 */
	void enterSvpParameter(SBWriteParser.SvpParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svpParameter}.
	 * @param ctx the parse tree
	 */
	void exitSvpParameter(SBWriteParser.SvpParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svpParameterList}.
	 * @param ctx the parse tree
	 */
	void enterSvpParameterList(SBWriteParser.SvpParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svpParameterList}.
	 * @param ctx the parse tree
	 */
	void exitSvpParameterList(SBWriteParser.SvpParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svmParameter}.
	 * @param ctx the parse tree
	 */
	void enterSvmParameter(SBWriteParser.SvmParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svmParameter}.
	 * @param ctx the parse tree
	 */
	void exitSvmParameter(SBWriteParser.SvmParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svmParameterList}.
	 * @param ctx the parse tree
	 */
	void enterSvmParameterList(SBWriteParser.SvmParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svmParameterList}.
	 * @param ctx the parse tree
	 */
	void exitSvmParameterList(SBWriteParser.SvmParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SBWriteParser#svpwrite}.
	 * @param ctx the parse tree
	 */
	void enterSvpwrite(SBWriteParser.SvpwriteContext ctx);
	/**
	 * Exit a parse tree produced by {@link SBWriteParser#svpwrite}.
	 * @param ctx the parse tree
	 */
	void exitSvpwrite(SBWriteParser.SvpwriteContext ctx);
}