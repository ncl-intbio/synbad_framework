/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;


/**
 *
 * @author ogilfellon
 */
public enum SVPType {Prom, Op, RBS, CDS, Ter, Shim, Other;


    public static SVPType fromString(String type) {
        
        if(type.equalsIgnoreCase("promoter")) {
            return Prom;
        } else if (type.equalsIgnoreCase("operator")) { 
            return Op;
        } else if (type.equalsIgnoreCase("rbs")) { 
            return RBS;
        } else if (type.equalsIgnoreCase("functionalpart")) { 
            return CDS;
        } else if (type.equalsIgnoreCase("terminator")) { 
            return Ter;
        } else if (type.equalsIgnoreCase("shim")) { 
            return Shim;
        } else {
            return Other;
        }
    }

}
 