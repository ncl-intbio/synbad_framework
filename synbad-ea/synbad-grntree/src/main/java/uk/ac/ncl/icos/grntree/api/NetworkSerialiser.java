package uk.ac.ncl.icos.grntree.api;

/**
 * Provides an interface for Exporters that take GRNTrees are arguments, and convert to another Type.
 * @param <T> Implementations must specify the Type of the Exported object
 */
public interface NetworkSerialiser<T> {

    public GRNTree importNetwork(T toImport);
    public T exportNetwork(GRNTree toExport);

}
