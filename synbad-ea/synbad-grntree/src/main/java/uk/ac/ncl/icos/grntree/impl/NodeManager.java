package uk.ac.ncl.icos.grntree.impl;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.ModelObserver;
import uk.ac.ncl.icos.grntree.api.ModelSubject;
import uk.ac.ncl.icos.tree.Node;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by owengilfellon on 27/02/2014.
 */
public class NodeManager implements ModelSubject, Serializable {

    private final Logger LOGGER = LoggerFactory.getLogger(NodeManager.class);

    private final transient SVPManager m = SVPManager.getSVPManager();

    private final Set<InteractionRecord> interactions = new HashSet<>();
    private final Map<NodeWrapper, NodeRecord> leafRecords;
    private final Map<NodeWrapper, NodeRecord> branchRecords;
    private final Map<NodeWrapper, Set<GRNTreeNode>> wrapperToNode;

    private final transient List<ModelObserver> observers = new ArrayList<>();
    private boolean detectInteractions = true;

    public NodeManager()
    {
        leafRecords = new HashMap<>();
        branchRecords = new HashMap<>();
        wrapperToNode = new  HashMap<>();
    }

    // ================================ READ METHODS ===============================================================

    /**
     * Returns true if the part represented by the leaf node is in the tree
     * @param node
     * @return 
     */
    public synchronized boolean containsNode(GRNTreeNode node)
    {
        if(!node.isLeafNode())
            return false;
        return leafRecords.containsKey(new NodeWrapper(node));
    }

    /**
     * Returns the branch nodes that contain the given leaf node
     * @param node
     * @return 
     */
    public List<GRNTreeNode> getParents(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);
        if(!leafRecords.containsKey(nw))
            return new ArrayList<>();
        return new ArrayList<>(leafRecords.get(nw).parents);
    }



    public synchronized boolean isDetectingInteractions() {
        return detectInteractions;
    }

    
    /**
     * Returns the total number of branch nodes (modules) in the tree
     * @return 
     */
    public synchronized int getBranchSize() {
        int size = 0;
        for (NodeWrapper nw : leafRecords.keySet()) {
            size += leafRecords.get(nw).parents.size();
        }
        return size;
    }

    /**
     * Returns the total number of leaf nodes (parts) in the tree
     * @return 
     */
    public synchronized int getLeafSize() {
        int size = 0;
        for(NodeWrapper nw : leafRecords.keySet()) {
            size += leafRecords.get(nw).parents.size();
        }
        return size;
    }

    public synchronized int getInstancesSize(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);
        if (!leafRecords.containsKey(nw))
            return 0;
        return leafRecords.get(nw).parents.size();
    }


    public synchronized int getAllInteractionsSize(LeafNode node)
    {
        return interactions.size();
    }

    /**
     * Returns all interactions within the tree
     * @return
     */
    public Set<Interaction> getAllInteractions() {
        return interactions.stream().map(i -> i.interaction).collect(Collectors.toSet());
    }


    public synchronized int getInstancedInteractionsSize(LeafNode node)
    {
        NodeWrapper nw = new NodeWrapper(node);
        if (!leafRecords.containsKey(nw))
            return 0;

        return leafRecords.get(nw).interactions.size();
    }

    /**
     * Returns all interactions within the tree
     * @return
     */
    public Set<Interaction> getInstancedInteractions() {
        Set<Interaction> interactions = new HashSet<>();
            for (NodeWrapper nw : leafRecords.keySet()) {
                NodeRecord nr = leafRecords.get(nw);
                synchronized (nr.interactions) {
                    Set<InteractionRecord> interactionRecords = nr.interactions;
                    for (InteractionRecord record : interactionRecords) {
                        interactions.add(record.interaction);
                    }
                }
        }
        return interactions;
    }

    /**
     * Returns the interactions within the tree involving the supplied leaf node.
     * @param node
     * @return
     */
    public synchronized Set<Interaction> getInstancedInteractions(GRNTreeNode node){
        if(!node.isLeafNode())
            return Collections.EMPTY_SET;

        Set<Interaction> interactions = new HashSet<>();
        Set<InteractionRecord> interactionRecords = leafRecords.get(new NodeWrapper(node)).interactions;
        synchronized (interactionRecords) {
            for (InteractionRecord ir : interactionRecords) {
                interactions.add(ir.interaction);
            }
        }
        return interactions;
    }

    public synchronized List<BranchNode> getBranchNodes() {
        return branchRecords.keySet().stream().map(n -> (BranchNode) n.node).collect(Collectors.toList());
    }

    public synchronized List<LeafNode> getLeafNodes() {
        return leafRecords.keySet().stream().map(n -> (LeafNode) n.node).collect(Collectors.toList());
    }

    /**
     * Returns the parts in the tree associated with a given interaction
     * @param interaction
     * @return
     */
    public synchronized List<GRNTreeNode> getNodes(Interaction interaction) {
        List<GRNTreeNode> parts = new ArrayList<>();
        InteractionRecord ir = new InteractionRecord(interaction);
            for (NodeWrapper nw : leafRecords.keySet()) {
                if (leafRecords.get(nw).interactions.contains(ir)) {
                    parts.add(nw.node);
                }
        }
        return parts;
    }

    public synchronized Part getPart(String name) {
        for (NodeWrapper nw : leafRecords.keySet()) {
            if (nw.node.getSVP().getName().equals(name)) {
                return nw.node.getSVP();
            }
        }
        return null;
    }

    public synchronized List<GRNTreeNode> getNodes(String name) {
        List<GRNTreeNode> parts = new ArrayList<>();
        for (NodeWrapper nw : leafRecords.keySet()) {
            if (nw.node.getSVP().getName().equals(name)) {
                for (GRNTreeNode n : leafRecords.get(nw).parents) {
                    for (GRNTreeNode child : n.getChildren()) {
                        if (child.isLeafNode() && ((LeafNode) child).getSVP().getName().equalsIgnoreCase(name)) {
                            parts.add(child);
                        }
                    }
                }
            }
        }
        return parts;
    }

    /**
     * Returns the leaf nodes that interact with the given leaf node
     * @param node
     * @return
     */
    public synchronized List<GRNTreeNode> getInteractingNodes(GRNTreeNode node)
    {

        if(!node.isLeafNode())
            return Collections.EMPTY_LIST;

        NodeWrapper nw = new NodeWrapper(node);

        List<GRNTreeNode> nodes = new ArrayList<>();

        if (!leafRecords.containsKey(nw)) {
            return nodes;
        }

        Set<String> identifiers = new HashSet<>();

        Set<InteractionRecord> interactions = leafRecords.get(nw).interactions;
        for (InteractionRecord interaction : interactions) {
            for (String partName : interaction.interaction.getParts()) {
                if (!partName.equals(node.getSVP().getName())) {
                    identifiers.add(partName);
                }
            }
        }

        for (NodeWrapper n : leafRecords.keySet()) {
            if (identifiers.contains(n.node.getSVP().getName())) {
                nodes.addAll(wrapperToNode.get(n));
            }
        }

        return nodes;
    }

    // ================================ WRITE METHODS ===============================================================

    public synchronized void setDetectInteractions(boolean detectInteractions) {
        this.detectInteractions = detectInteractions;
        for (NodeWrapper node : leafRecords.keySet()) {
            detectInteractions(node);
        }
    }

    public synchronized boolean  removeInteraction(Interaction interaction) {

        InteractionRecord record = new InteractionRecord(interaction);
        Set<NodeWrapper> nodeWrappers = this.leafRecords.keySet().stream()
                .filter(lr -> leafRecords.get(lr).interactions.contains(record))
                .collect(Collectors.toSet());

        nodeWrappers.forEach(nw -> {
            NodeRecord r = leafRecords.get(nw);
            synchronized (r.interactions) {
                r.interactions.remove(record);
            }
        });
        interactions.remove(record);
        return !nodeWrappers.isEmpty();
    }

    private void updateInteractions(NodeWrapper n) {

        if(!n.node.isLeafNode())
            return;

        Set<InteractionRecord> partialMatches = interactions.stream()
                .filter(i -> i.interaction.getParts().contains(n.node.getName()))
                .collect(Collectors.toSet());

        for(InteractionRecord r : partialMatches) {
            List<LeafNode> parts = new ArrayList<>();
            List<String> ids = r.interaction.getParts();
            if (ids != null) {
                for (NodeWrapper nw : leafRecords.keySet()) {
                    if (nw.node instanceof LeafNode && ids.contains(nw.node.getSVP().getName()))
                        parts.add((LeafNode) nw.node);
                }

                boolean allAdded = true;

                if(ids.containsAll(parts.stream().map(AbstractNode::getName).collect(Collectors.toSet()))) {
                    for (LeafNode node : parts) {
                        NodeWrapper nw = new NodeWrapper(node);
                        NodeRecord rec = leafRecords.get(nw);
                        synchronized (rec.interactions) {
                            leafRecords.get(nw).interactions.add(r);
                        }
                    }
                }
            }
        }
    }

    public synchronized boolean addInteraction(Interaction interaction)
    {
            List<LeafNode> parts = new ArrayList<>();
            List<String> ids = interaction.getParts();
            InteractionRecord ir = new InteractionRecord(interaction);


            if (ids != null) {
                for (NodeWrapper nw : leafRecords.keySet()) {
                    if (nw.node instanceof LeafNode && ids.contains(nw.node.getSVP().getName()))
                        parts.add((LeafNode) nw.node);
                }

                boolean allAdded = true;

                for (LeafNode node : parts) {

                    if (!containsNode(node))
                        return false;

                    NodeWrapper nw = new NodeWrapper(node);
                    NodeRecord r = leafRecords.get(nw);
                    synchronized (r.interactions) {
                        allAdded = leafRecords.get(nw).interactions.add(ir) && allAdded;
                    }
                }
            }

            interactions.add(ir);

            return false;
    }

    private void removeNode(NodeWrapper node)
    {

            if(node.node instanceof BranchNode) {
                branchRecords.remove(node);
                return;
            }

            NodeRecord nr = leafRecords.get(node);
            nr.parents.remove(node.node.getParent());

            if(nr.parents.isEmpty()) {

                // Remove interactionDocuments referring to this part from any interacting part

                if(!nr.interactions.isEmpty()) {

                    for(InteractionRecord i:nr.interactions){

                        List<String> parts = i.interaction.getParts();

                        for(String p:parts) {

                            if(!p.equals(node.node.getSVP().getName())) {

                                // p == the id of the other part in interaction
                                // Find node that has this id

                                Iterator<NodeWrapper> node_it = leafRecords.keySet().iterator();
                                NodeWrapper matchedNode = null;

                                while(node_it.hasNext() && matchedNode == null) {
                                    NodeWrapper n = node_it.next();
                                    if(n.node.getSVP().getName().equals(p)) {
                                        matchedNode = n;
                                    }
                                }

                                // Can only remove interaction from nodes that still exist...?
                                // TODO: If interactions are removed when the first participant is removed, matched node should never equal null?

                                if(matchedNode != null) {
                                    // Retrieve associated record

                                    NodeRecord nrToAmmend = leafRecords.get(matchedNode);
                                    Iterator<InteractionRecord> it = nrToAmmend.interactions.iterator();

                                    InteractionRecord toRemove = null;
                                    while(it.hasNext()) {
                                        InteractionRecord ir = it.next();

                                        if(ir.interaction.getParts().contains(node.node.getSVP().getName())) {
                                            toRemove = ir;
                                        }
                                    }

                                    if(toRemove!=null) {
                                        nrToAmmend.interactions.remove(toRemove);
                                        //interactions.remove(toRemove);
                                    }
                                }

                                // Find interaction of interest and remove;
                            }
                        }
                    }
                }

                leafRecords.remove(node);
            }
    }

    private void addNode(NodeWrapper node)
    {

            Map<NodeWrapper, NodeRecord> records = node.node instanceof BranchNode ? branchRecords : leafRecords;
            // If the node doesn't exist in records, add it

            if (!records.containsKey(node)) {
                NodeRecord nr = new NodeRecord();
                records.put(node, nr);
            }

            records.get(node).parents.add((BranchNode) node.node.getParent());

            updateInteractions(node);

            if (detectInteractions) {
                detectInteractions(node);
            }
    }

    void detectInteractions(NodeWrapper node) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Detecting interactions: [ {} ]", node);

        List<Interaction> allPartInteractions;

        // Retrieve any interactions in the repository (non-repository interactions must be added manually)

        if( LeafNode.class.isAssignableFrom(node.node.getClass()) &&
                !(node.node instanceof PrototypeNode)) {

            //List<SignalType> signals = ((LeafNode)node.node).getInputSignal();
            //signals.addAll(((LeafNode)node.node).getOutputSignal());

            //if(!signals.contains(SignalType.None)) {

            allPartInteractions = m.getInteractions(node.node.getSVP());

            // Loop through all interactionDocuments the part is involved in


            if(allPartInteractions!=null && allPartInteractions.size() > 0) {

                HashMap<String, Interaction> potentialInteractions = new HashMap<>();

                // Add interactionDocuments to a temporary map, with other part's name as key

                for(Interaction interaction:allPartInteractions) {
                    for(String partName:interaction.getParts()) {
                        if(!partName.equals(node.node.getSVP().getName())) {
                            potentialInteractions.put(partName, interaction);
                        }
                    }
                }

                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("{} potential interactions identified", potentialInteractions.size());

                // If a node exists in records that has interactionDocuments with the added part, add interaction under that node

                int interactionsAdded = 0;


                    for(NodeWrapper n:leafRecords.keySet()) {
                        if(potentialInteractions.containsKey(n.node.getSVP().getName())) {
                            InteractionRecord ir = new InteractionRecord(potentialInteractions.get(n.node.getSVP().getName()));
                            leafRecords.get(n).interactions.add(ir);
                            leafRecords.get(node).interactions.add(ir);
                            interactionsAdded++;
                        }
                    }

                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("{} of {} potential interactions present", interactionsAdded, potentialInteractions.size());

                    }

            // }
        }
    }

    // ================================ OBSERVER METHODS ===============================================================

    @Override
    public void alert() {

        for(ModelObserver o:observers)
        {
            o.update(this);
        }
    }

    /**
     * Notifies the node manager of an update to the tree
     * @param node 
     */
    public void alert(GRNTreeNode node)
    {
        if(node instanceof BranchNode) {
            this.alert((BranchNode) node);
        }
        else if (node instanceof LeafNode){
            this.alert((LeafNode) node);
        }
    }

    private void alert(BranchNode node)
    {
        
        BranchNode parent = (BranchNode) node.getParent();
        NodeWrapper nw = new NodeWrapper(node);
        if(!parent.getChildren().contains(node)) {
            removeNode(nw);
        }
        else {
            addNode(nw);
        }

        for(GRNTreeNode child : node.getChildren()) {
            if(child.isLeafNode()) {
                this.alert(child);
            } else if (child.isBranchNode()) {
                this.alert(child);
            }
        }

        alert();
    }

    private void alert(LeafNode node)
    {

        BranchNode parent = (BranchNode) node.getParent();
        NodeWrapper nw = new NodeWrapper(node);
        

        if(!parent.getChildren().contains(node)) {
            removeNode(nw);
            
            if(wrapperToNode.containsKey(nw))
                wrapperToNode.get(nw).remove(node);
            
            if(wrapperToNode.isEmpty()) {
                wrapperToNode.remove(nw);
            }
        }
        else {
            addNode(nw);
            if(!wrapperToNode.containsKey(nw)) {
                wrapperToNode.put(nw, new HashSet<>());
            }

            wrapperToNode.get(nw).add(node);
        }

        alert();
    }

    @Override
    public void attach(ModelObserver o) {
        this.observers.add(o);
    }

    @Override
    public void dettach(ModelObserver d) {
        this.observers.remove(d);
    }

    // ================================ ADDITIONAL METHODS ===============================================================

    public synchronized NodeManager duplicate() {

            NodeManager n = new NodeManager();

            Map<String, GRNTreeNode> nodeMap = new HashMap<>();

            for (NodeWrapper nodeWrapper : branchRecords.keySet()) {
                GRNTreeNode node = nodeWrapper.node.duplicate();
                node.setNodeManager(n);
                nodeMap.put(node.toString(), node);
                n.branchRecords.put(new NodeWrapper(node), duplicateNodeRecord(branchRecords.get(nodeWrapper), n));
            }

            for (NodeWrapper nodeWrapper : leafRecords.keySet()) {
                GRNTreeNode node = nodeWrapper.node.duplicate();
                node.setNodeManager(n);
                nodeMap.put(node.toString(), node);
                n.leafRecords.put(new NodeWrapper(node), duplicateNodeRecord(leafRecords.get(nodeWrapper), n));
            }

            for (NodeWrapper nodeWrapper : wrapperToNode.keySet()) {
                GRNTreeNode node = nodeMap.get(nodeWrapper.node.toString()).duplicate();
                node.setNodeManager(n);
                n.wrapperToNode.put(new NodeWrapper(node),
                        wrapperToNode.get(nodeWrapper).stream().map(x -> {GRNTreeNode y = x.duplicate(); y.setNodeManager(n); return y;}).collect(Collectors.toSet()));
            }

            return n;

    }

    private NodeRecord duplicateNodeRecord(NodeRecord record, NodeManager nm) {

        return new NodeRecord(
            record.interactions,
            record.parents.stream().map(bn -> {
                 BranchNode n = (BranchNode)bn.duplicate();
                 n.setNodeManager(nm);
                 return n;
            }).collect(Collectors.toList()));
    }

    public synchronized void debug() {

        String format = "%-20s %-13s %-10s %n";
        String divider = "--------------------------------------------------";
        String subformat = "%-30s %30s %n";
        System.out.println(divider);
        System.out.printf(format, "Part", "Interactions", "Instances");
        System.out.println(divider);

        for(NodeWrapper n:leafRecords.keySet())  {
            System.out.printf(format, n, leafRecords.get(n).interactions.size(), leafRecords.get(n).parents.size());
        }
    }

    // ================================ WRAPPER / RECORD CLASSES =========================================================

    /*
        Groups together information about a node
     */
    @XStreamAlias("NodeRecord")
    private class NodeRecord implements Serializable {
        private final Set<InteractionRecord> interactions;
        private final List<BranchNode> parents;

        public NodeRecord() {
            interactions = new HashSet<>();
            parents = new ArrayList<>();
        }

        public NodeRecord(Set<InteractionRecord> interactions, List<BranchNode> parents) {
           this.interactions = new HashSet<>(interactions);
           this.parents = new LinkedList<>(parents);
        }
    }
    /*
        Wrapper class for Interaction to provide logical equivalency
     */
    @XStreamAlias("InteractionRecord")
    public class InteractionRecord implements Serializable {

        private final Interaction interaction;

        public InteractionRecord(Interaction interaction)
        {
            this.interaction = interaction;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof InteractionRecord))
                return false;

            InteractionRecord ir = (InteractionRecord) obj;

            return ir.interaction.getName().equals(this.interaction.getName());
        }


        @Override
        public int hashCode() {
            return interaction.getName().hashCode() * interaction.getInteractionType().hashCode();
        }
    }

    @XStreamAlias("NodeWrapper")
    private class NodeWrapper implements Serializable {

        private final GRNTreeNode node;

        public NodeWrapper(GRNTreeNode node)
        {
            this.node = node;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof NodeWrapper))
                return false;   
            if(!node.getClass().equals(((NodeWrapper)obj).node.getClass()))
                return false;

            if (BranchNode.class.isAssignableFrom(((NodeWrapper) obj).node.getClass())) {

                BranchNode remote = (BranchNode) ((NodeWrapper) obj).node;
                BranchNode local = (BranchNode) node;
                
                if(remote.getChildren().size() != local.getChildren().size())
                    return false;
  
                for(int i = 0; i < remote.getChildren().size(); i++) {
                    if(!new NodeWrapper(remote.getChildren().get(i)).equals(
                            new NodeWrapper(local.getChildren().get(i))))
                        return false;
                }
                
                return true;

            } else if (LeafNode.class.isAssignableFrom(((NodeWrapper) obj).node.getClass())) {
                Part p = ((NodeWrapper) obj).node.getSVP();

                return p.getName().equals(node.getSVP().getName()) &&
                    p.getType().equals(node.getSVP().getType());
            }
            
            return false;

        }

        @Override
        public int hashCode() {
            
            if(LeafNode.class.isAssignableFrom(node.getClass()))
                return node.getSVP().getName().hashCode()
                    * node.getSVP().getType().hashCode();
            else if (node instanceof BranchNode) {
                int prime = 13;
                for(GRNTreeNode child : ((BranchNode)node).getChildren()) {
                    prime = prime * child.getName().hashCode();
                }
                return prime;
            }
            return 0;
        }

        @Override
        public String toString() {
            return node.toString();
        }
    }



}
