/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.traversal;


import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author owengilfellon
 */
public class BreadthFirstIterator implements Iterator<GRNTreeNode> {

   Queue<GRNTreeNode> q = new LinkedList<GRNTreeNode>();

   public  BreadthFirstIterator(GRNTree tree) {
        q.add(tree.getRootNode());
    }
 
    @Override
    public boolean hasNext() {
        if(!q.isEmpty())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public GRNTreeNode next() {
        if(!q.isEmpty())
        {
            GRNTreeNode currentNode = q.remove();
            if(currentNode.getChildren().size() > 0)
            {
                q.addAll(currentNode.getChildren());
            }
            return currentNode;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
