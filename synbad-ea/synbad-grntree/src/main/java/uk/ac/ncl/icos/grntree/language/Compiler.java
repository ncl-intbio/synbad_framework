package uk.ac.ncl.icos.grntree.language;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteBaseListener;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteLexer;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteParser;

import java.io.*;
import java.util.*;

public class Compiler extends SBWriteBaseListener {

    private final static Logger LOGGER  = LoggerFactory.getLogger(Compiler.class);
    private String svpWrite = "";
    private boolean processingDefinition = false;
    private final List<SBWriteParser.SvpContext> svps = new LinkedList<>();
    private final List<SBWriteParser.InteractionContext> interactions = new LinkedList<>();
    private int idPostfix = 0;

    private class Promoter {

    }

    private class Cds {

    }

    public GRNTree compile(String svpWrite) {
        this.svpWrite = svpWrite;
        compile();
        return null;
    }

    public GRNTree compile(InputStream is) {
        try( BufferedReader br = new BufferedReader( new InputStreamReader( is )))
        {
            StringBuilder sb = new StringBuilder();
            String line;
            while(( line = br.readLine()) != null ) {
                sb.append( line );
                sb.append( '\n' );
            }
            this.svpWrite = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return compile();
    }

    private GRNTree compile() {

        this.svpWrite = new ModuleRewriter().rewrite(svpWrite);

       // LOGGER.debug(svpWrite);

        SBWriteParser parser = getParser( new ByteArrayInputStream(this.svpWrite.getBytes()));
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, parser.svpwrite());
        SVPHelper HELPER = SVPHelper.getSVPHelper();

        for(SBWriteParser.InteractionContext ctx: interactions) {
            LOGGER.debug("{} {} {}",
                    ctx.leftParticipant().getText(),
                    ctx.INTERACTION_OPERATOR().getText(),
                    ctx.rightParticipant().getText());

//            left.put(ctx.leftParticipant().getText(), ctx);
//            right.put(ctx.rightParticipant().getText(), ctx);

//            ctx.ANNOTATION().forEach(x -> {
//                if(x.getText().startsWith("@description")) {
//                    LOGGER.debug("\t\tDescription: {}", x.getText().substring(13));
//                } else if(x.getText().startsWith("@url")) {
//                    LOGGER.debug("\t\tURL: {}", x.getText().substring(5));
//                } else if(x.getText().startsWith("@organism")) {
//                    LOGGER.debug("\t\tOrganism: {}", x.getText().substring(10));
//                } else if(x.getText().startsWith("@displayName")) {
//                    LOGGER.debug("\t\tDisplayName: {}", x.getText().substring(13));
//                }
//            });
        }

        for (SBWriteParser.SvpContext ctx : svps) {

            String id = ctx.ID() == null ? "internal_" + idPostfix++ : ctx.ID().getText();
            String type = ctx.TYPE().getText();
            LOGGER.debug("{}:{}", id, type);

            GRNTreeNode node = null;

            switch (type) {
                case "Prom":
//                    if(right.containsKey(id)) {
//                        node = HELPER.getInducPromoter(id, 0);
//                    } else {
//                        node = HELPER.getConstPromoter(id, 0);
//                    }
                    break;
                case "Op":
                    node = HELPER.getOperator(id);
                    break;
                case "RBS":
                    node = HELPER.getRBS(id, 0);
                    break;
                case "CDS":
                    boolean phosphorylated = interactions.stream().anyMatch(i -> i.rightParticipant().getText().equals(id) && i.INTERACTION_OPERATOR().getText().equals("-*"));
                    //boolean bySmlMol = svps.stream().noneMatch(svp -> svp.ID().getText());
//                    if(interactions.stream().anyMatch(i -> i.r)) {
//                        node = HELPER.getCDSWithPhosphorylated(id, 0, 0, 0);
//                    } else {
//                        node = HELPER.getCDS(id, 0);
//                    }
                    break;
                case "Ter":
                    node = HELPER.getTerminator(id);
                    break;
            }
        }
        return null;
    }

    private SBWriteParser getParser(InputStream is) {
        CharStream charStream = null;
        try {
            charStream = CharStreams.fromStream(is);
        } catch (IOException e) {
            LOGGER.error("Could not get input", e);
            return null;
        }
        SBWriteLexer lexer = new SBWriteLexer(charStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        SBWriteParser parser = new SBWriteParser(tokenStream);
        return parser;
    }



    @Override
    public void exitSvp(SBWriteParser.SvpContext ctx) {
        super.exitSvp(ctx);
        if(!processingDefinition) {
            svps.add(ctx);
        }

    }

    @Override
    public void exitInteraction(SBWriteParser.InteractionContext ctx) {
        super.exitInteraction(ctx);
        if(!processingDefinition) {
            interactions.add(ctx);
        }

    }

    @Override
    public void enterSvmDef(SBWriteParser.SvmDefContext ctx) {
        processingDefinition = true;
    }

    @Override
    public void exitSvmDef(SBWriteParser.SvmDefContext ctx) {
        super.exitSvmDef(ctx);
        processingDefinition = false;
    }

    @Override
    public void enterSvpDef(SBWriteParser.SvpDefContext ctx) {
        super.enterSvpDef(ctx);
        processingDefinition = true;
    }

    @Override
    public void exitSvpDef(SBWriteParser.SvpDefContext ctx) {
        super.exitSvpDef(ctx);
        processingDefinition = false;
        LOGGER.debug("DEF: {}", ctx.ID());
        ctx.ANNOTATION().forEach(x -> {
            if(x.getText().startsWith("@description")) {
                LOGGER.debug("\t\tDescription: {}", x.getText().substring(13));
            } else if(x.getText().startsWith("@url")) {
                LOGGER.debug("\t\tURL: {}", x.getText().substring(5));
            } else if(x.getText().startsWith("@organism")) {
                LOGGER.debug("\t\tOrganism: {}", x.getText().substring(10));
            } else if(x.getText().startsWith("@displayName")) {
                LOGGER.debug("\t\tDisplayName: {}", x.getText().substring(13));
            }
        });
    }

}
