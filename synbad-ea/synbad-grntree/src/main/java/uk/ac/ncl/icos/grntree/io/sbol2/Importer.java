/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;
import org.sbolstandard.core2.Annotation;
import org.sbolstandard.core2.Component;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.DirectionType;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.SBOLDocument;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author goksel
 */
public class Importer
{

    public Importer()
    {
    }

    public GRNTree convert(SBOLDocument document) throws Exception
    {
        HashMap<URI, GRNTreeNode> createdNodes = new HashMap<URI, GRNTreeNode>();
        HashMap<URI, ComponentDefinition> usedInParents = new HashMap<URI, ComponentDefinition>();
        Map<URI, Interaction> internalInteractions = new HashMap<>();
        List<Interaction> interactions = SBOLToSVP.getInteractions(document);
       
        for (ComponentDefinition componentDef : document.getComponentDefinitions()) {
            
            // OG 12/04/15: Added clause to avoid empty root node being added as Leaf
            
            if(!SBOLHelper.isRoot(componentDef)) {
                createNode(componentDef, document, createdNodes, usedInParents, false);
            }
        }
        
        GRNTree tree = addNodesToTree(document, createdNodes, usedInParents);
   
        for (Interaction interaction : interactions) {
            //if(!interaction.getIsInternal().booleanValue()) {
                tree.addInteraction(interaction);
            //}
        }

        return tree;
    }

    private GRNTree addNodesToTree(SBOLDocument document, Map<URI, GRNTreeNode> createdNodes, Map<URI, ComponentDefinition> usedInParents) throws SBOL2SerialisationException
    {
        GRNTree tree = GRNTreeFactory.getGRNTree();
        for (Map.Entry<URI, GRNTreeNode> entry : createdNodes.entrySet())
        {
            URI componentDefUri = entry.getKey();
            GRNTreeNode node = entry.getValue();
            try
            {
                if (!usedInParents.containsKey(componentDefUri))
                {
                    ComponentDefinition componentDef = document.getComponentDefinition(componentDefUri);
                    if (SBOLHelper.isLeaf(componentDef))
                    {   //Leaf nodes can't be attached to the node directly. They need to be contained by a parent node all the time.
                        //Create a parent node for the leaf node and add it to the root node.
                        GRNTreeNode leafNodeContainer = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
                        leafNodeContainer.addNode(node);
                        tree.getRootNode().addNode(leafNodeContainer);
                    }
                    else
                    {
                        tree.getRootNode().addNode(node);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new SBOL2SerialisationException("Could not add the node for component " + componentDefUri.toString() + " to the tree.", exception);
            }
        }
        return tree;
    }

    private GRNTreeNode createNode(ComponentDefinition componentDef, SBOLDocument document, HashMap<URI, GRNTreeNode> createdNodes, HashMap<URI, ComponentDefinition> usedInParents, boolean isSubComponent) throws Exception
    {
        try
        {
            GRNTreeNode node = createdNodes.get(componentDef.getIdentity());
            if (node != null)
            {
                if (usedInParents.containsKey(componentDef.getIdentity()))
                {
                    //if used as a sub component for another parent, duplicate the node.
                    if (isSubComponent)
                    {
                        node = node.duplicate();
                    }
                    else
                    {//If it is a standalone component, use the same node that has been used in a parent before.
                        node = createdNodes.get(componentDef.getIdentity());
                    }
                }
            }
            
            if (SBOLHelper.isLeaf(componentDef))
            {
                Part part = SBOLToSVP.getSVP(componentDef, document);
                node = GRNTreeNodeFactory.getLeafNode(part, InterfaceType.NONE);
            }
            else 
            {
                node = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
                addInterfaces(node, document, componentDef);
                addSubComponents(node, document, componentDef, createdNodes, usedInParents);
            }
            getSynBadProperties(node, componentDef.getAnnotations());
            createdNodes.put(componentDef.getIdentity(), node);
      
            return node;
        }
        catch (Exception exception)
        {
            throw new SBOL2SerialisationException(exception.getMessage() + "Could not process the component definition " + componentDef.getIdentity(), exception);
        }
    }

    private void addInterfaces(GRNTreeNode node, SBOLDocument document, ComponentDefinition componentDef)
    {
        ModuleDefinition moduleDef = SBOLHelper.getModuleDefinition(document, componentDef);
        if (moduleDef != null && moduleDef.getFunctionalComponents() != null)
        {
            for (FunctionalComponent functionalComp : moduleDef.getFunctionalComponents())
            {
                //Search the functional component that do not point to a branch component definition
                if (functionalComp.getDefinition() != null && !functionalComp.getDefinition().equals(componentDef.getIdentity()))
                {
                    if (functionalComp.getDirection() != null && functionalComp.getDirection().equals(DirectionType.IN))
                    {
                        //TODO: How to add the inputs and outputs
                    }
                }
            }
        }
    }

    private void addSubComponents(GRNTreeNode node, SBOLDocument document, ComponentDefinition componentDef, HashMap<URI, GRNTreeNode> createdNodes, HashMap<URI, ComponentDefinition> usedInParents) throws Exception
    {
        
        if (componentDef.getComponents() != null && componentDef.getComponents().size() > 0)
        {        
            List<Component> orderedComponents = SBOLHelper.getOrderedComponents(componentDef);
            for (Component component : orderedComponents)
            {
                ComponentDefinition childComponentDef = document.getComponentDefinition(component.getDefinitionURI());
                if (childComponentDef != null)
                {
                    GRNTreeNode childNode = createNode(childComponentDef, document, createdNodes, usedInParents, true);
                    usedInParents.put(childComponentDef.getIdentity(), childComponentDef);
                    node.addNode(childNode);
                }
                else
                {
                    throw new SBOL2SerialisationException("No such component definition with the URI of " + component.getDefinition().toString());
                }
            }
        }
    }

   
    private void getSynBadProperties(GRNTreeNode node, List<Annotation> annotations)
    {
        for (Annotation annotation : annotations)
        {
            /*
            NamedProperty<QName> property = annotation.getValue();
            
            

            if (property.getName().equals(Terms.synbadTerms.isPrototype))
            {
                //TODO:  How do we set these properties
            }*/
        }
    }

    
}
