package uk.ac.ncl.icos.grntree.impl;

/**
 * Created by owengilfellon on 02/06/2014.
 */
public class NewGRNEdge {

    private InteractionType type;

    public NewGRNEdge(InteractionType type) {
        this.type = type;
    }

   public String toString()
   {
       if(this.type == InteractionType.ACTIVATION) {
           return "+";
       } else {
           return "-";
       }
   }

}
