package uk.ac.ncl.icos.grntree.api;

/**
 * Created by owengilfellon on 15/07/2014.
 */
public interface ModelObserver<T> {

    public void update(ModelSubject<T> s);

}
