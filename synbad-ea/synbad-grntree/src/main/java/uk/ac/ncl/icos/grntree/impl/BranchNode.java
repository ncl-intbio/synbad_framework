package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.grntree.api.GRNEdge;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author owengilfellon
 */
public class BranchNode extends AbstractNode implements Serializable {
    
    private final Logger logger = LoggerFactory.getLogger(BranchNode.class);
    private List<GRNTreeNode> nodes;
    private List<GRNEdge> edges;

    public static BranchNode getBranchNode(InterfaceType interfaceType)
    {
        return new BranchNode(interfaceType);
    }
    
    public static BranchNode getBranchNode(InterfaceType interfaceType, List<GRNTreeNode> nodes)
    {
        return new BranchNode(interfaceType, nodes);
    }

    private BranchNode() {

    }
    
    /**
     * Constructs an empty Node for containing Nodes and Edges.
     * @param interfaceType The interfaceType of the node being created. 
     */
    protected BranchNode(InterfaceType interfaceType) {
        super(interfaceType);
        nodes = new ArrayList<>();
        edges = new ArrayList<>();
    }
    
    /**
     * Constructs a Node that contains Nodes and Edges. Nodes are supplied as a parameter, edges are calculated automatically.
     * @param interfaceType The interfaceType of the node being created. 
     * @param nodes The nodes 
     */
    protected BranchNode(InterfaceType interfaceType, List<GRNTreeNode> nodes) {
        super(interfaceType);
        this.setNodes(nodes);
        edges = new ArrayList<GRNEdge>();
    }

    /**
     *
     * @return A deep copy of the BranchNode and all children
     */
    @Override
    public GRNTreeNode duplicate() {
        
        List<GRNTreeNode> duplicates = new ArrayList<GRNTreeNode>();

        synchronized (nodeManager) {
            for (GRNTreeNode n : nodes) {
                duplicates.add(n.duplicate());
            }
        }

        return BranchNode.getBranchNode(interfaceType, duplicates);
    }

    /**
     * Returns the parts contained within any nodes that are InterfaceType: INPUT
     * @return A list of parts. An empty list if no Input nodes are found.
     */
    @Override
    public List<GRNTreeNode> getInputNodes() {
        List<GRNTreeNode> inputNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        
        while(it.hasNext())
        {
            GRNTreeNode node = it.next();
            
             if(node.getInterfaceType().equals(InterfaceType.INPUT) ||
                    node.getInterfaceType().equals(InterfaceType.BOTH))
                {
                        inputNodes.addAll(node.getInputNodes());         
                }
        }
        
        return inputNodes;
    }
    
    /**
     * Returns the parts contained within any nodes that are InterfaceType: OUTPUT
     * @return A list of parts. An empty list if no Output nodes are found.
     */
    @Override
    public List<GRNTreeNode> getOutputNodes() {
        List<GRNTreeNode> outputNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        
        while(it.hasNext())
        {
            GRNTreeNode node = it.next();
            
            if(node.getInterfaceType().equals(InterfaceType.OUTPUT) ||
                    node.getInterfaceType().equals(InterfaceType.BOTH))
            {
                    outputNodes.addAll(node.getOutputNodes());    
            }
        }
        
        return outputNodes;
    }

    /**
     * Retrieves all parts contained within the current node, or children of the
     * current node.
     * @return 
     */
    @Override
    public List<Part> getDescendents() {

        // Recursive call, to retrieve all descendents. LeafNode also has a getDescendents method which returns
        // the Part that they contain

        List<Part> parts = new ArrayList<Part>();
        ListIterator<GRNTreeNode> it = nodes.listIterator();
        while(it.hasNext())
        {
            GRNTreeNode node = it.next();
            parts.addAll(node.getDescendents());        
        }
        
        return parts;
    }

    /**
     * Returns the nodes contained only within the node, ignoring those contained
     * by children.
     * @return 
     */
    @Override
    public List<GRNTreeNode> getChildren() {
        return nodes;
    }

    /**
     * Returns children of a specified SvpType
     * @return
     */
    public List<LeafNode> getChildren(SVPType type) {
        return getChildren().stream()
            .filter(GRNTreeNode::isLeafNode).map(n -> (LeafNode) n)
            .filter(n -> n.getType() == type)
            .collect(Collectors.toList());
    }




    // =============================================================
    // TODO Delete this method - if used, use node manager instead
    // Filter by interfaces? Or should this be performed by another
    // Class?
    // =============================================================

     /**
     * Retrieves a list of edges contained within the current node, and all 
     * child nodes.
     * @return 
     */
    @Override
    public List<GRNEdge> getEdges() {


        List<GRNEdge> edges2 = new ArrayList<GRNEdge>();
        List<GRNTreeNode> outputNodes = new ArrayList<GRNTreeNode>();
        List<GRNTreeNode> inputNodes = new ArrayList<GRNTreeNode>();
        ListIterator<GRNTreeNode> n_it = nodes.listIterator();
        
        /*
         * Iterate through all child nodes of the current node.
         */
 
        while(n_it.hasNext()) { 
            
            GRNTreeNode node = n_it.next();
            
            
            /*
             * Retrieve the INPUT and OUTPUT nodes available to the current node.
             * Retrieving Nodes rather than Parts directly allows for edges to
             * be added between the Nodes.
             */
            
            inputNodes.addAll(node.getInputNodes());
            outputNodes.addAll(node.getOutputNodes());
        }
       
        if(outputNodes.size() > 0) {

            ListIterator<GRNTreeNode> outputIterator = outputNodes.listIterator();

            while(outputIterator.hasNext()){
                
                /*
                 * If OUTPUT nodes are found, iterate through them, retrieving
                 * their Parts. All OUTPUT nodes returned from getOutputNodes()
                 * are LeafNodes, so the cast to LeafNode is safe.
                 */

                GRNTreeNode outputnode = outputIterator.next();
                LeafNode leaf1 = (LeafNode) outputnode;
                Part part1 = leaf1.getSVP(); 
                
                /*
                 * Check every OUTPUT part against every OUTPUT part. This could
                 * be optimised.
                 */
                
                ListIterator<GRNTreeNode> outputIterator2 = outputNodes.listIterator();

                while(outputIterator2.hasNext()){
                    
                    GRNTreeNode outputnode2 = outputIterator2.next();
                    LeafNode leaf2 = (LeafNode) outputnode2;
                    Part part2 = leaf2.getSVP();
                    
                    /*
                     * A pair of parts have been retrieved. Any interactionDocuments between
                     * these two parts are retrieved using a method in SVPManager
                     */
                    
                    List<Interaction> interactions = m.getInteractions(part1, part2);
                    
                     if(interactions.size() > 0) {
                        
                         ListIterator<Interaction> interactionsIterator = interactions.listIterator();
 
                        /*
                         * If there are interactionDocuments, iterate through them.
                         */

                         while(interactionsIterator.hasNext()) {

                             Interaction i = interactionsIterator.next();
                             
                             /*
                              * For now, the only OUTPUT-OUTPUT interactionDocuments we are interested
                              * in are Phosphorylation interactionDocuments
                              */

                             if(i.getInteractionType().equals("Phosphorylation")){

                                 List<InteractionPartDetail> ipd = i.getPartDetails();
                                 ListIterator<InteractionPartDetail> ipd_it = ipd.listIterator();
                                 
                                 while(ipd_it.hasNext()){

                                     InteractionPartDetail d = ipd_it.next();
                                     
                                     /*
                                      * If Part 1 acts on Part 2 in the interaction - i.e.
                                      * if Part 1 phosphorylates Part 2, add an edge to this Node.
                                      */

                                     if(d.getInteractionRole().equals("Input") &&
                                             d.getPartForm().equals("Phosphorylated") &&
                                             d.getPartName().equals(part1.getName())) {

                                         GRNInteractionEdge e = new GRNInteractionEdge(outputnode, outputnode2, InteractionType.PHOSPHORYLATION);

                                         /*
                                          * Duplicate check, due to the inefficient method of comparing
                                          * all OUTPUT nodes with all OUTPUT nodes.
                                          */
                                         
                                         if(!edges2.contains(e)){                                         
                                            edges2.add(e); 
                                         }
                                     }
                                 }
                             }
                         }
                     }
                }

                /*
                 * Check every OUTPUT part against every INPUT part.
                 */

                if(inputNodes.size() > 0){

                    ListIterator<GRNTreeNode> inputIterator = inputNodes.listIterator();

                    while(inputIterator.hasNext()){

                         GRNTreeNode inputnode = inputIterator.next();
                         LeafNode leaf2 = (LeafNode) inputnode;
                         Part part2 = leaf2.getSVP();
                         List<Interaction> interactions = m.getInteractions(part1, part2);
                        
                        /*
                        * A pair of parts have been retrieved. Any interactionDocuments between
                        * these two parts are retrieved using a method in SVPManager
                        */

                         if(interactions.size() > 0) {
                             
                            
                             ListIterator<Interaction> interactionsIterator = interactions.listIterator();

                             while(interactionsIterator.hasNext()) {

                                 Interaction i = interactionsIterator.next();
                                 
                                 /*
                                * For now, the only OUTPUT-INPUT interactionDocuments we are interested
                                * in are Transcriptional Regulation
                                */
             
                                 if(i.getInteractionType().equals("Transcriptional activation") ||
                                         i.getInteractionType().equals("Transcriptional repression") ||
                                         i.getInteractionType().equals("Transcriptional repression using an operator") ||
                                         i.getInteractionType().equals("Transcriptional activation using an operator")){

                                     List<InteractionPartDetail> ipd = i.getPartDetails();
                                     ListIterator<InteractionPartDetail> ipd_it = ipd.listIterator();

                                     while(ipd_it.hasNext()){

                                         InteractionPartDetail d = ipd_it.next();
                                         
                                         /*
                                        * If Part 1 regulates Part 2 add an edge to this Node.
                                        */

                                         if(d.getInteractionRole().equals("Modifier") &&
                                                 d.getPartName().equals(part1.getName())){

                                             InteractionType type = i.getInteractionType().equals("Transcriptional activation") ? InteractionType.ACTIVATION : InteractionType.REPRESSION;
                                             GRNInteractionEdge e = new GRNInteractionEdge(outputnode, inputnode, type);

                                             if(!edges2.contains(e)){                        
                                                edges2.add(e); 
                                             }
                                        }
                                    }
                                }
                            }
                        }
                    }                  
                }
            }
        }
        
           return edges2;
    }

    /**
     * Sets, or replaces if already set, the child nodes of the current node with
     * those provided.
     * @param nodes 
     */
    @Override
    final public void setNodes(List<GRNTreeNode> nodes) {

        this.nodes = new LinkedList<>(nodes);

        for(GRNTreeNode node:nodes) {
            node.setParent(this);

            if(this.nodeManager!=null) {
                node.setNodeManager(nodeManager);
                super.alert(node);
            }

        }

    }
 
    @Override
    public boolean addNode(GRNTreeNode node) {
        
        /*
         *  ADD CODE for ensuring uniqueness within tree
         */
        
        node.setParent(this);
        boolean added = nodes.add(node);
        if(added && nodeManager != null) {
            node.setNodeManager(nodeManager);
            super.alert(node);
        }
        return added;
    }

    @Override
    public void addNode(int index, GRNTreeNode node) throws Exception {
        
        /*
         *  ADD CODE for ensuring uniqueness within tree
         */
        
        node.setParent(this);
        if(index >= nodes.size()) {
            nodes.add(node);
        } else
        {
            nodes.add(index, node);
        }

        if(nodeManager != null) {
            node.setNodeManager(nodeManager);
            super.alert(node);
        }
       
    }

    /**
     * Removes the provided node from the list of child nodes
     * @param node
     * @return 
     */
    @Override
    public boolean removeNode(GRNTreeNode node) {

        // If node is Transcription Unit remove all parts it contains

        if(node.isBranchNode())
        {
            /*
            List<GRNTreeNode> toRemove = new ArrayList<GRNTreeNode>();
            toRemove.addAll(node.getChildren());
            for(GRNTreeNode n:toRemove)
            {
                node.removeNode(n);
            }
            */
            boolean b = nodes.remove(node);
            if(nodeManager!=null){
                super.alert(node);
            }
            return b;
        }

        // Otherwise move all child nodes up to removed node's parent

        else
        {
           if(nodes.contains(node))
           {
               for(GRNTreeNode n:node.getChildren())
               {
                   try {
                       addNode(n);
                   } catch (Exception ex) {
                       logger.error(ex.getMessage());
                   }
               }
           }
           
           boolean b = nodes.remove(node);
           if(nodeManager!=null){
               super.alert(node);
           }

           return b;
        }     
    }


    @Override
    public boolean removeNodeAndDescendents(GRNTreeNode node) {
        boolean b =  nodes.remove(node);
        if(b && nodeManager != null) {
            super.alert(node);
        }
        return b;
    }

    /**
     * Returns a string description of 
     * @return 
     */
    @Override
    public String toString() {     
        StringBuilder sb = new StringBuilder();       
        ListIterator<GRNTreeNode> it = nodes.listIterator();

        while(it.hasNext())
        {            
            GRNTreeNode n = it.next();
            
            sb.append(n);
            
            if(n instanceof LeafNode)
            {
                sb.append("; ");
            }
        }
        
        return sb.toString();
    }
    
    @Override
    public boolean isTranscriptionUnit() {
       
        if(nodes.isEmpty())
        {
            return false;
        }

       ListIterator<GRNTreeNode> it = nodes.listIterator();
       while(it.hasNext())
       {
           GRNTreeNode n = it.next();
           if(n instanceof BranchNode)
           {
               return false;
           }
       }
       return true;
    }



    @Override
    public boolean isBranchNode() {
        return true;
    }

    @Override
    public int getHeight() {
        int bestHeight = 0;
        for(GRNTreeNode node : nodes) {
            if(node.getHeight() > bestHeight) bestHeight = (node.getHeight() +1);
        }
        return bestHeight;
    }
    
    
}