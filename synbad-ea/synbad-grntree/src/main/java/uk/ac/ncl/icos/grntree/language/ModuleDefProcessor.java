package uk.ac.ncl.icos.grntree.language;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.sbolstandard.core2.Interaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteBaseVisitor;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteParser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ModuleDefProcessor extends SBWriteBaseVisitor<ModuleDefProcessor> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleDefProcessor.class);
    private final Map<String, SBWriteParser.InteractionContext> interactions;
    private final List<String> parameters = new LinkedList<>();
    private String moduleId;

    public ModuleDefProcessor() {
        interactions = new HashMap<>();
    }

    @Override
    public ModuleDefProcessor visitSvmParameter(SBWriteParser.SvmParameterContext ctx) {
        parameters.add(ctx.ID().getText());
        return super.visitSvmParameter(ctx);
    }

    @Override
    public ModuleDefProcessor visitInteraction(SBWriteParser.InteractionContext ctx) {

        interactions.put(moduleId.concat(".").concat(ctx.leftParticipant().getText()), ctx);
        interactions.put(moduleId.concat(".").concat(ctx.rightParticipant().getText()), ctx);
        return super.visitInteraction(ctx);
    }

    @Override
    public ModuleDefProcessor visitSvmDef(SBWriteParser.SvmDefContext ctx) {
        this.moduleId = ctx.ID().getText();

        return super.visitSvmDef(ctx);
    }

    public List<String> getParameters() {
        return parameters;
    }

    public Map<String, SBWriteParser.InteractionContext> getInteractions() {
        return interactions;
    }
}
