/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.api;

import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.List;


/**
 *
 * @author owengilfellon
 */
public interface GRNTreeNode extends Serializable {

    /**
     * Returns a node's parent
     * @return
     */
    GRNTreeNode getParent();

    /**
     * Returns the interface type of a node
     * @return
     */
    InterfaceType getInterfaceType();

    /**
     * Return a node's name, or a short description of the node, if not set.
     * @return
     */
    String getName();

    /**
     * Returns the Nodes that are descendants of this node, are InterfaceType: INPUT and contain this
     * node within their interaction bounds.
     * @return A list of parts. An empty list if no INPUT nodes are found.
     */
    List<GRNTreeNode> getInputNodes();

    /**
     * Returns the Nodes that are descendants of this node, are InterfaceType: OUTPUT and contain this
     * node within their interaction bounds.
     * @return A list of parts. An empty list if no OUTPUT nodes are found.
     */
    List<GRNTreeNode> getOutputNodes();

    /**
     * Returns only those nodes that have this node as a direct parent (i.e. a list of siblings)
     * @return
     */
    List<GRNTreeNode> getChildren();




    /**
     * Retrieves all parts contained within the current node, or it's descendants, regardless of interaction bounds.
     * @return 
     */
    List<Part> getDescendents();


    /**
     * Retrieves a list of edges contained within the current node, and all descendants (i.e. interactionDocuments that occur
     * between nodes that are descendants of this node)
     * @return 
     */
    List<GRNEdge> getEdges();

    /**
     * Determines whether the current node is a Transcription Unit. A node is considered to be a transcription unit if
     * it is non-empty, holds LeafNodes (those representing parts), and contains no BranchNodes.
     * @return true if node is a transcription unit
     */
    boolean isTranscriptionUnit();

    // TODO Add this method
    /**
     * Not currently supported.
     * @return
     */
    int getHeight();

    /**
     * Returns the depth of the node (i.e. the inclusive number of nodes between this, and the root node)
     * @return
     */
    int getDepth();

    /**
     *
     * @param interfaceType
     */
    void setInterfaceType(InterfaceType interfaceType);
    void setName(String name);
    void setDisplayName(String displayName);

    /**
     * Sets, or replaces if already set, the child nodes of the current node with
     * those provided.
     * @param nodes
     */
    void setNodes(List<GRNTreeNode> nodes);

    /**
     * Adds a single node to the list of child nodes.
     * Constraints:
     * <ol>
     * <li>Can not be called on LeafNode</li>
     * <li>Part can not break encapsulation</li>
     * <li>LeafNodes and BranchNodes cannot be mixed.</li>
     * <li>Argument node must be unique within the tree</li>
     * <li>There can be only one promoter, or terminator per TU.</li>
     * <li>Parts must have valid context
     * <ul>
     * <li>Promoters must not follow any part</li>
     * <li>Operators must follow Promoters or Operators</li>
     * <li>RBSs must follow Promoters, Operators, or CDSs (in the case of operons).</li>
     * <li>CDSs must follow RBSs</li>
     * <li>Terminators must follow CDSs</li>
     * </ul></li></ol>

     * @param node
     * @return
     */
    boolean addNode(GRNTreeNode node) ;

    /**
     * Adds a node at a specific index within the List of Child Nodes.
     * @param index
     * @param node
     * @throws Exception
     */
    void addNode(int index, GRNTreeNode node) throws Exception;

    /**
     * Replaces this node with a provided node.
     * @param node
     * @throws Exception
     */
    void replaceNode(GRNTreeNode node) throws Exception;
    
    /**
     * Removes the provided node from the list of child nodes, keeping it's child nodes, and moving them up one level.
     * @param node
     * @return 
     */
    boolean removeNode(GRNTreeNode node);

    /**
     * Removes the provided node from the list of child nodes, and all descendants.
     * @param node
     * @return
     */
    boolean removeNodeAndDescendents(GRNTreeNode node);

    /**
     * Sets the parent of a node (i.e. moves it within the tree)
     * @param parent
     */
    void setParent(GRNTreeNode parent);

    GRNTreeNode duplicate();

    void setNodeManager(NodeManager nm);

    String getId();
    
    boolean isLeafNode();
    
    boolean isBranchNode();
    
    boolean isPrototype();
    
    Part getSVP();

}
