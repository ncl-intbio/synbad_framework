/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.SignalType;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNEdge;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.ncl.intbio.virtualparts.entity.Parameter;

/**
 * Represents the smallest, indivisible level of the representation of a Genetic
 * Regulatory Network (GRN )- a standard biological part. This particular
 * representation is built around the standard virtual parts (SVP) standard and
 * the SVP representation from the library written by Dr. Goksel Misirli at Newcastle
 * University. It could and should be abstracted, so that any representation of a
 * part within a GRN could be used.
 * @author owengilfellon
 */

// TODO remove much of this code and add to AbstractNode instead

public abstract class LeafNode extends AbstractNode implements Serializable {

    protected Part svp = null;
    private SVPType type;
    private String partId;

    private List<SignalType> inputSignal = new ArrayList<>();
    private List<SignalType> outputSignal = new ArrayList<>();

    boolean hasDocument = false;
    protected transient SBMLDocument document = null;

    public static LeafNode getLeafNode(Part svp, InterfaceType interfaceType)
    {
        return new ConcreteNode(svp, interfaceType);
    }

    public static LeafNode getLeafNode(Part svp, List<Interaction> internalEvents, InterfaceType interfaceType)
    {
        return new PrototypeNode(svp, internalEvents, interfaceType);
    }

    protected LeafNode()
    {
       m = SVPManager.getSVPManager();
    }

    /**
     * Constructor for repository parts for which internal events are stored in a repository
     * @param svp
     * @param interfaceType
     */
    public LeafNode(Part svp, InterfaceType interfaceType)
    {
        super(interfaceType);
        this.svp = svp;
        this.partId = svp.getName();
        this.type = SVPType.fromString(svp.getType());
    }

    public List<GRNParameter> getParameters() {
       return getInternalEvents().stream()
        .flatMap(i -> i.getParameters().stream())
        .filter(p -> p.getValue() != null)
//               .map(p ->  {
//                   System.out.println(p.getName() + ":" +  p.getParameterType());
//                   return p;})
        .map(p -> new GRNParameter(p.getValue(), p.getName()))
        .filter(p -> p.getParameterType() != null)
        .collect(Collectors.toList());
    }

    public SBMLDocument getDocument()
    {
        return document;
    }
    
    public boolean hasDocument()
    {
        return document != null;
    }

    @Override
    public List<GRNEdge> getEdges() { return new ArrayList<GRNEdge>(); }

    /**
     *
     * @return  the @Part that the LeafNode represents.
     */
    @Override
    public Part getSVP() { return svp; }

    /**
     *
     * @return
     */
    @Override
    public List<Part> getDescendents()
    {
        List<Part> parts = new ArrayList<Part>();
        parts.add(svp);
        return parts;
    }

    /**
     * If the node's InterfaceNode is Input, returns the node. This method is
     * the bottom level of a recursive method that can be called on any node
     * within the GRN tree, which retrieves any input nodes that are available
     * to that node.
     * @return All input nodes available to the node upon which the function is
     * called
     */
    @Override
    public List<GRNTreeNode> getInputNodes()
    {
        List<GRNTreeNode> node = new ArrayList<GRNTreeNode>();
        if(this.interfaceType.equals(InterfaceType.INPUT) ||
                this.interfaceType.equals(InterfaceType.BOTH))
        {
            node.add(this);
        }

        return node;
    }




    /**
     * If the node's InterfaceNode is Output, returns the node. This method is
     * the bottom level of a recursive method that can be called on any node
     * within the GRN tree, which retrieves any output nodes that are available
     * to that node.
     * @return All output nodes available to the node upon which the function is
     * called
     */
    @Override
    public List<GRNTreeNode> getOutputNodes()
    {
        List<GRNTreeNode> node = new ArrayList<GRNTreeNode>();
        if(this.interfaceType.equals(InterfaceType.OUTPUT) ||
                this.interfaceType.equals(InterfaceType.BOTH))
        {
            node.add(this);
        }
        return node;
    }

    /*
     * I don't beleive this is actually overriding anything, and returns a List
     * so as merely to be consistent with it's Interface. Is there a better way
     * to structure this?
     */
    @Override
    public List<GRNTreeNode> getChildren()
    {
        List<GRNTreeNode> node = new ArrayList<GRNTreeNode>();
        return node;
    }

    /**
     * @return an SVPWrite description of the part, in the form of
     * "partname:parttype". These part descriptions can be concatanated, separated
     * by semicolons (partname:parttype; partname:parttype) to create a
     * compilable SVPWrite statement.
     */
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(svp.getName()).append(":").append(type);
        return sb.toString();
    }

    public SVPType getType()
    {
        return type;
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        svp.setName(name);
    }

    @Override
    public void setDisplayName(String displayName) {
        super.setDisplayName(displayName);
        svp.setDisplayName(displayName);
    }
    
    public void setSequence(String sequence) {
        svp.setSequence(sequence);
    }

    public List<SignalType> getInputSignal() {
        return inputSignal;
    }

    public List<SignalType> getOutputSignal() {
        return outputSignal;
    }
    
    public abstract List<Interaction> getInternalEvents();

    public void setSVP(Part svp)
    {
        
        boolean inputFound = false;
        boolean outputFound = false;
        
        for(Interaction event : getInternalEvents()) {
    
            for(Parameter parameter : event.getParameters()) {

                if(parameter.getParameterType().equalsIgnoreCase("pops")) {
                    if(parameter.getName().contains("Input")) {
                        inputSignal.add(SignalType.PoPS);
                        inputFound = true;
                    } else if (parameter.getName().contains("Output")){
                        outputSignal.add(SignalType.PoPS);
                        outputFound = true;
                    }
                } else if (parameter.getParameterType().equalsIgnoreCase("rips")) {
                    if(parameter.getName().contains("Input")) {
                        inputSignal.add(SignalType.RiPS);
                        inputFound = true;
                    } else if (parameter.getName().contains("Output")){
                        outputSignal.add(SignalType.RiPS);
                        outputFound = true;
                    }
                } else if (parameter.getParameterType().equalsIgnoreCase("mrna")) {
                    if(parameter.getName().contains("Input")) {
                        inputSignal.add(SignalType.mRNA);
                        inputFound = true;
                    } else if (parameter.getName().contains("Output")){
                        outputSignal.add(SignalType.mRNA);
                        outputFound = true;
                    }
                }
            }
        }
        
        if(!inputFound) {
            inputSignal.add(SignalType.None);
        }
        
        if(!outputFound) {
            outputSignal.add(SignalType.None);
        }
    }

    @Override
    public int getHeight() {
        return 1;
    }
    
    

    @Override
    public boolean isLeafNode() {
        return true;
    }
    
    
}
