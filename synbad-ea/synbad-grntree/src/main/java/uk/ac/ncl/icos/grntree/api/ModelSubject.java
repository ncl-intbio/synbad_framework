package uk.ac.ncl.icos.grntree.api;

/**
 * Created by owengilfellon on 15/07/2014.
 */
public interface ModelSubject<T> {

    public void attach(ModelObserver<T> o);
    public void dettach(ModelObserver<T> d);
    public void alert();

}
