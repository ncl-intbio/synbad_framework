/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;


/**
 *
 * @author ogilfellon
 */
public enum SVPState {

    Default,
    Phosphorylated;

    public static SVPState fromString(String state) {
        
        if(state.equalsIgnoreCase("phosphorylated")) {
            return Phosphorylated;
        } else {
            return Default;
        }
    }

}
 