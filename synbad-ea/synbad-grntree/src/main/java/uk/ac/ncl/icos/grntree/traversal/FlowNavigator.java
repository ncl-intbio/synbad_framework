package uk.ac.ncl.icos.grntree.traversal;

import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by owengilfellon on 26/02/2014.
 */
public class FlowNavigator {
    
    private final Logger logger = LoggerFactory.getLogger(FlowNavigator.class);
    private List<GRNTreeNode> visited = new ArrayList<>();
    private Queue<GRNTreeNode> toVisit = new LinkedList<>();
    private GRNTree tree;
    private SVPManager m = SVPManager.getSVPManager();

    public FlowNavigator(GRNTree tree, LeafNode startNode)
    {
        this.tree = tree;
        toVisit.add(startNode.getParent());
    }

    public FlowNavigator(GRNTree tree, String startNode)
    {
        this.tree = tree;
        Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
        while(it.hasNext()) {
            GRNTreeNode node = it.next();
            if(node instanceof LeafNode){
                LeafNode ln = (LeafNode) node;
                if(ln.getSVP().getName().equals(startNode))
                {
                    if(!toVisit.contains(ln.getParent())){
                        toVisit.add(ln.getParent());
                    }
                }
            }
        }
    }

    public boolean hasNext()
    {
        if(!toVisit.isEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    public GRNTreeNode next()
    {
        // starting from list of branch nodes, move from toVisit TU to Visited TU

        BranchNode node = (BranchNode)toVisit.remove();
        visited.add(node);

        logger.debug("Visiting node");
        
        for(GRNTreeNode child:node.getChildren())
        {
            LeafNode ln = (LeafNode) child;
            
            // for all promoters and operators

            if(ln.getType() == SVPType.Prom || ln.getType() == SVPType.Op) {
                
                // get interacting parts in tree

                List<GRNTreeNode> interactingParts = tree.getInteractingParts(ln);
                Set<Interaction> interactions = tree.getInteractions(ln);

                // Check to see if regulating protein is phosphorylated, add parents of phosphorylator

                for(Interaction i:interactions) {
                    List<InteractionPartDetail> interactionPartDetails = i.getPartDetails();
                    for(InteractionPartDetail ipd:interactionPartDetails) {
                        if(ipd.getPartForm().equals("Phosphorylated")) {

                            // If so, add Phosphorylating part

                            for(GRNTreeNode n:interactingParts) {
                                if(((LeafNode)n).getType().equals(SVPType.CDS)) {
                                    List<GRNTreeNode> potentialPhosphorylatingParts = tree.getInteractingParts(n);
                                    for(GRNTreeNode ppp: potentialPhosphorylatingParts) {
                                        if (((LeafNode)ppp).getType().equals(SVPType.CDS)) {
                                            for(GRNTreeNode parent:tree.getParents(ppp)) {
                                                if(!toVisit.contains(parent) && !visited.contains(parent))  {
                                                    toVisit.add(parent);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                // add regulator

                for(GRNTreeNode ipart:interactingParts)
                {
                    for(GRNTreeNode parent:tree.getParents(ipart)) {
                        if(!toVisit.contains(parent) && !visited.contains(parent))  {
                            toVisit.add(parent);
                        }
                    }
                }
            }
        }

        return node;
    }

    public List<GRNTreeNode> getVisited() {
        return visited;
    }

    public List<GRNTreeNode> getUnvisited()
    {
        List<GRNTreeNode> unvisited = new ArrayList<>();

        Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
        while(it.hasNext()) {
            GRNTreeNode node = it.next();
            if(node instanceof BranchNode && node.isTranscriptionUnit())
            {
                BranchNode bn = (BranchNode) node;
                if(!visited.contains(bn)) {
                    unvisited.add(bn);
                }
            }
        }

        return unvisited;
    }
}
