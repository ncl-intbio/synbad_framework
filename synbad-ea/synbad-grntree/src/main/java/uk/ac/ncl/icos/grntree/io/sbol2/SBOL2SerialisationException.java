/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

/**
 *
 * @author goksel
 */
public class SBOL2SerialisationException extends Exception
{

    public SBOL2SerialisationException()
    {
    }

    public SBOL2SerialisationException(String string)
    {
        super(string);
    }

    public SBOL2SerialisationException(String string, Throwable thrwbl)
    {
        super(string, thrwbl);
    }

    public SBOL2SerialisationException(Throwable thrwbl)
    {
        super(thrwbl);
    }

    public SBOL2SerialisationException(String string, Throwable thrwbl, boolean bln, boolean bln1)
    {
        super(string, thrwbl, bln, bln1);
    }
    
}
