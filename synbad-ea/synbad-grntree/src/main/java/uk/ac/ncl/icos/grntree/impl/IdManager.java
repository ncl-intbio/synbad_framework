package uk.ac.ncl.icos.grntree.impl;

import java.util.HashSet;
import java.util.Random;

/**
 * Created by owengilfellon on 15/07/2014.
 */
public class IdManager {

    private static HashSet<String> ids = new HashSet<String>();
    private static Random r = new Random();
    private static final String prefix = "Node";

    private IdManager() { }

    public static String getID()
    {
        String id;

        do {
            int i = r.nextInt(Integer.MAX_VALUE);
            id = prefix + i;
        }
        while(ids.contains(id));

        ids.add(id);
        return id;
    }
}
