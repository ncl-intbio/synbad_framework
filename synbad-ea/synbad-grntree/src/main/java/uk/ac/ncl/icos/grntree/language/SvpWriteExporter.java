package uk.ac.ncl.icos.grntree.language;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InteractionType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.intbio.virtualparts.entity.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SvpWriteExporter  {

    private boolean minimise;

    Logger LOGGER = LoggerFactory.getLogger(SvpWriteExporter.class);
    Map<String, String> moduleDefinitions = new HashMap<>();
    List<String> partInteractions = new LinkedList<>();
    DecimalFormat df = new DecimalFormat();

    public SvpWriteExporter() {
        this(false);
    }

    public SvpWriteExporter(boolean minimise) {
        this.minimise = minimise;
        df.setMaximumFractionDigits(20);
    }

    public String write(GRNTree tree) {

        StringBuilder s = new StringBuilder();

        String instanceString = getNodeString(tree.getRootNode());

        String definitionsString = moduleDefinitions.values().stream()
                .reduce("", (s1, s2) -> s1.concat("\n\n").concat(s2));

        s.append(definitionsString)
                .append("\n\n")
                .append(instanceString)
                .append(";\n");

        tree.getInteractions().forEach(i -> s.append(getInteractionString(tree, i)));

        partInteractions.forEach(s::append);

        String svpWrite = s.toString();

        if(minimise) {
            svpWrite = svpWrite.replaceAll("[\\s\\n]", "").replaceAll("module", "module ");
        }

        LOGGER.debug(svpWrite);
        return svpWrite;
    }

    private String  getNodeString(GRNTreeNode node) {
        if(node.isBranchNode()) {
            return getNodeString((BranchNode) node);
        } else if (node.isLeafNode()) {
            return getNodeString((LeafNode) node);
        }

        return "";
    }

    private String getNodeString(BranchNode node) {

        String parameterList = "";

        if(!moduleDefinitions.containsKey(node.getName())) {
            StringBuilder b = new StringBuilder();
            List<GRNTreeNode> inputs = node.getInputNodes();
            List<GRNTreeNode> outputs = node.getOutputNodes();

            parameterList = Stream.concat(inputs.stream(), outputs.stream())
                    .map(GRNTreeNode::getName)
                    .distinct()
                    .reduce("", (a, c) -> a.concat(c).concat(", "));

            for (GRNTreeNode n : node.getChildren()) {
                b.append(getNodeString(n).concat("; "));
            }

            if(!parameterList.isEmpty()) {
                parameterList = parameterList.substring(0, parameterList.length() -2);
            }
            moduleDefinitions.put(node.getName(), "module " + node.getName().toLowerCase() + "(".concat(parameterList) + "){\n\t" + b.toString() + "\n}");
        }
        return node.getName().toLowerCase() + "(" + parameterList + ")";
    }

    private String getNodeString(LeafNode node) {
        List<String> partParameters = new LinkedList<>();
        List<String> interactionParameters = new LinkedList<>();

        // process internal interactions

        for(Interaction interaction : node.getInternalEvents()) {
            boolean containsPhos = interaction.getPartDetails().stream()
                    .anyMatch(p -> p.getPartForm().equals("Phosphorylated"));

            for(Parameter p : interaction.getParameters()) {
                String name = p.getName().toLowerCase();
                switch (name) {
                    case "ktr":
                        partParameters.add("ktr=" +  df.format(p.getValue()));
                        break;
                    case "ktl":
                        partParameters.add("ktl=" +  df.format(p.getValue()));
                        break;
                    case "kdep":
                        interactionParameters.add("kdeP=" +  df.format(p.getValue()));
                        break;
                    case "kf":
                        interactionParameters.add("kf=" +  df.format(p.getValue()));
                        break;
                    case "kd":
                        if(!containsPhos)
                            partParameters.add("kd=" +  df.format(p.getValue()));
                        else
                            interactionParameters.add("kd=" +  df.format(p.getValue()));
                        break;
                }
            }
        }

        if(!interactionParameters.isEmpty()) {

            // Add internal small molecule interaction

            Interaction smlMolPhos = node.getInternalEvents().stream()
                    .filter( x -> x.getPartDetails().stream().anyMatch(p -> p.getInteractionRole().equalsIgnoreCase("Modifier")))
                    .findFirst().orElse(null);

            if(smlMolPhos != null) {

                InteractionPartDetail smlMol = smlMolPhos.getPartDetails().stream()
                        .filter(p -> p.getInteractionRole().equalsIgnoreCase("Modifier"))
                        .findFirst().orElse(null);

                String from =  smlMol.getPartName();

                String to = smlMolPhos.getPartDetails().stream()
                        .filter(pd -> pd.getInteractionRole().equalsIgnoreCase("output") &&
                                pd.getPartForm().equalsIgnoreCase("phosphorylated"))
                        .map(InteractionPartDetail::getPartName).findFirst().orElse("");


                String pars = interactionParameters.stream().reduce("", (a, b) -> a.isEmpty() ? b : a + ", " + b);

                partInteractions.add(buildInteractionString(from, "-*", to, pars));
            }
        }

        return node.toString() + (partParameters.isEmpty() ? "" :
                "(" + partParameters.stream().reduce("", (a, b) -> a.isEmpty() ? a + b : a + ", " + b) + ")");
    }


    private String getInteractionString(GRNTree tree, Interaction i) {

        String from = "";
        String type = "";
        String to = "";
        String parameters = "";

        InteractionType interactionType = InteractionType.fromString(i.getInteractionType());

        if(interactionType == null)
            return "";

        // Construct external interactions

        switch (interactionType) {
            case ACTIVATION:
                type = "->";
                from = i.getPartDetails().stream()
                        .filter(pd -> pd.getInteractionRole().equalsIgnoreCase("modifier"))
                        .map(pd -> pd.getPartName() + (pd.getPartForm().equalsIgnoreCase("phosphorylated") ? "~P" : "")).findFirst().orElse("");
                to = getPartNotInIPDs(i);
                parameters = i.getParameters().stream().filter(p -> p.getName().equalsIgnoreCase("km")).findFirst().map(p -> "Km=" + df.format(p.getValue())).orElse("");
                break;
            case REPRESSION:
                type =  "-|";
                from = i.getPartDetails().stream()
                        .filter(pd -> pd.getInteractionRole().equalsIgnoreCase("modifier"))
                        .map(pd -> pd.getPartName() + (pd.getPartForm().equalsIgnoreCase("phosphorylated") ? "~P" : "")).findFirst().orElse("");
                to = getPartNotInIPDs(i);
                parameters = i.getParameters().stream().filter(p -> p.getName().equalsIgnoreCase("km")).findFirst().map(p -> "Km=" + df.format(p.getValue())).orElse("");
                break;
            case PHOSPHORYLATION:
                type = "-*";
                from = i.getPartDetails().stream()
                        .filter(pd -> pd.getPartForm().equalsIgnoreCase("phosphorylated"))
                        .map(pd -> pd.getPartName() + "~P" ).findFirst().orElse("");

                to = i.getPartDetails().stream()
                        .filter(pd -> pd.getInteractionRole().equalsIgnoreCase("output") &&
                                pd.getPartForm().equalsIgnoreCase("phosphorylated"))
                        .map(InteractionPartDetail::getPartName).findFirst().orElse("");

                List<Parameter> pars = new LinkedList<>(i.getParameters().stream()
                        .filter(p -> Arrays.asList("kd", "kdep", "kf").contains(p.getName().toLowerCase()))
                        .collect(Collectors.toList()));

                // Phosphorylation requires retrieval of parameters from internal interactions

                tree.getParts(to).stream().findFirst().ifPresent(n -> {
                    pars.addAll(((LeafNode) n).getInternalEvents().stream()
                            .filter(interaction -> interaction.getPartDetails().stream().anyMatch(ipd -> ipd.getPartForm().contains("Phosphorylated")))
                            .flatMap(interaction -> interaction.getParameters().stream())
                            .filter(p -> Arrays.asList("kd", "kdep", "kf").contains(p.getName().toLowerCase()))
                            .collect(Collectors.toList()));
                });

                parameters = pars.stream().map(p -> p.getName().concat("=").concat(df.format(p.getValue()))).reduce("", (a, b) -> a.isEmpty() ? b : a + ", " + b);
                break;
        }

        return buildInteractionString(from, type, to, parameters);
    }

    private String getPartNotInIPDs(Interaction i) {
        final Set<String> parts = i.getPartDetails().stream().map(InteractionPartDetail::getPartName).collect(Collectors.toSet());
        return i.getParts().stream().filter(part -> !parts.contains(part)).findFirst().orElse("");
    }

    private String buildInteractionString(String from, String type, String to, String parameters) {
        StringBuilder s = new StringBuilder(from).append(" ").append(type).append(" ").append(to);
        if(!parameters.isEmpty())
            s = s.append(" (").append(parameters).append(")");
        return  s.append(";").append("\n").toString();
    }
}
