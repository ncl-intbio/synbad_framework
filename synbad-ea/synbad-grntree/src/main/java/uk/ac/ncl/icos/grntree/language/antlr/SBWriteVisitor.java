// Generated from /Users/owengilfellon/Documents/Code/Misc/synbad_framework/synbad-ea/synbad-grntree/src/main/antlr4/uk/ac/ncl/icos/grntree/language/antlr/SBWrite.g4 by ANTLR 4.8
package uk.ac.ncl.icos.grntree.language.antlr;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SBWriteParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SBWriteVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvp(SBWriteParser.SvpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvm(SBWriteParser.SvmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#leftParticipant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLeftParticipant(SBWriteParser.LeftParticipantContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#rightParticipant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRightParticipant(SBWriteParser.RightParticipantContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#interaction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteraction(SBWriteParser.InteractionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#instanceList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstanceList(SBWriteParser.InstanceListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svmDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvmDef(SBWriteParser.SvmDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svpDef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvpDef(SBWriteParser.SvpDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svpParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvpParameter(SBWriteParser.SvpParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svpParameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvpParameterList(SBWriteParser.SvpParameterListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svmParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvmParameter(SBWriteParser.SvmParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svmParameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvmParameterList(SBWriteParser.SvmParameterListContext ctx);
	/**
	 * Visit a parse tree produced by {@link SBWriteParser#svpwrite}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSvpwrite(SBWriteParser.SvpwriteContext ctx);
}