package uk.ac.ncl.icos.grntree.impl;

import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.sbml.jsbml.SBMLDocument;

/**
 * Created by owengilfellon on 19/01/2015.
 */
final public class PrototypeNode extends LeafNode {

    private final List<Interaction> internalEvents;

    protected PrototypeNode(Part svp, InterfaceType interfaceType)
    {
        super(svp, interfaceType);
        this.internalEvents = new LinkedList<>();
        this.setSVP(svp);
        setName(svp.getName());
    }

    protected PrototypeNode(Part svp, List<Interaction> internalEvents, InterfaceType interfaceType)
    {
        super(svp, interfaceType);
        this.internalEvents = new LinkedList<>(internalEvents);
        this.setSVP(svp);
        setName(svp.getName());

        // Create part for any environmental constants in internal interactions

        List<Part> parts = this.internalEvents.stream()
                .flatMap(i -> i.getPartDetails().stream())
                .filter(pd -> pd.getMathName().equals("EnvironmentConstant"))
                .map(pd -> {
                    Part ec = new Part();
                    ec.setDisplayName(pd.getPartName());
                    ec.setName(pd.getPartName());
                    ec.setMetaType("Environment Constant");
                    return ec;
                }).collect(Collectors.toList());
        parts.add(svp);

        this.document = m.createPartDocument(svp, internalEvents, parts);
    }

    private PrototypeNode(Part svp, List<Interaction> internalEvents, InterfaceType interfaceType, SBMLDocument document)
    {
        super(svp, interfaceType);
        this.internalEvents = new LinkedList<>(internalEvents);
        this.setSVP(svp);
        setName(svp.getName());
        this.document = document;
    }

    @Override
    public SBMLDocument getDocument() {
        return document;
    }

    public List<Interaction> getInternalEvents()
    {
        return new LinkedList<>(internalEvents);
    }

    public boolean setInternalEvents(List<Interaction> interactions)
    {
        this.clearInternalEvents();
        boolean b = this.addInternalEvents(interactions);
     //   synchronized (SVPManager.getSVPManager()) {
            this.document = m.createPartDocument(svp, this.internalEvents);
   //     }
        return b;
    }

    public boolean addInternalEvents(List<Interaction> interactions)
    {
        return this.internalEvents.addAll(interactions);
    }

    public boolean removeInternalEvents(List<Interaction> interactions)
    {
        return internalEvents.removeAll(interactions);
    }

    public void clearInternalEvents()
    {
        this.internalEvents.clear();
    }

    @Override
    public GRNTreeNode duplicate()
    {
        return new PrototypeNode(this.getSVP(), this.internalEvents, interfaceType, document != null ? document.clone() : null);
    }

    @Override
    public boolean isPrototype() {
        return true;
    }

    private void writeObject(ObjectOutputStream out) throws Exception
    {
        synchronized (m) {
            out.defaultWriteObject();
            SBMLHandler handler = new SBMLHandler();
            if (hasDocument) {
                String sbml = handler.GetSBML(this.document);
                out.writeObject(sbml);
            }
        }
    }

    private void readObject(ObjectInputStream in) throws IOException, XMLStreamException, ClassNotFoundException
    {
        synchronized (m) {
            in.defaultReadObject();
            SBMLHandler handler = new SBMLHandler();
            if (hasDocument) {
                String sbml = (String) in.readObject();
                this.document = handler.GetSBML(sbml);
            }
        }
    }
}
