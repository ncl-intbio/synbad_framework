/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.sbolstandard.core2.AccessType;
import org.sbolstandard.core2.Component;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.ComponentInstance;
import org.sbolstandard.core2.DirectionType;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.ModuleDefinition;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeHelper;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author goksel
 */
public class SbolExporter
{

    private static final Logger logger = LoggerFactory.getLogger(SbolExporter.class);
    
    public SbolExporter()
    {
    }

    public SBOLDocument export(GRNTree tree, SBOLDocument document, String baseURL, String version) throws SBOL2SerialisationException
    {
        
        document.setDefaultURIprefix(baseURL);
        GRNTreeNode rootNode = tree.getRootNode();
        try {
            // Convert nodes in tree recursively
            
            convertNode(rootNode, document, baseURL, version);
            
            // Add interactions to the document
            
            addInteractions(tree, document,  version, baseURL);
            
        } catch (SBOLValidationException ex) {
            logger.error(ex.getMessage());
        }
        
        return document;
    }
    
    // OG: 14/04/2015: Modified to support conversion of Prototype nodes

    private ComponentDefinition convertNode(GRNTreeNode node, SBOLDocument document, String baseURL, String version) throws SBOL2SerialisationException, SBOLValidationException
    {
      
        // Create component definition and sequence
        
        ComponentDefinition componentDef = createComponentDefinition(document, baseURL, node, version);
        Sequence sequence = document.getSequence(componentDef.getSequenceURIs().stream().findAny().orElse(null));

        if (!node.isLeafNode()) {
            
            // If node is a branch node, store as module
            
            ModuleDefinition moduleDefinition = document.createModuleDefinition(
                    SBOLHelper.getValidDisplayId(node.getId()),
                    version);

            // Add functional components for any input / output nodes
            
            createFunctionalComponents(document, node, baseURL, moduleDefinition);

            // Add functional component for the module - is it a fc for the module?

            FunctionalComponent fc = moduleDefinition.createFunctionalComponent(
                    SBOLHelper.getValidDisplayId(node.getId()),
                    AccessType.PUBLIC,
                    componentDef.getIdentity(),
                    DirectionType.NONE);
        }
        
        // Iterate through the children of the current tree node

        int rangeIndex = 1;
        boolean prototypeFound = false;
        URI prevChild = null;
        
        for (GRNTreeNode childNode : node.getChildren())
        {     
            // Convert the child node recursively
            
            ComponentDefinition childComponentDef=convertNode(childNode, document, baseURL, version);
            Component childComponent = componentDef.createComponent(childNode.getName(), AccessType.PUBLIC, childComponentDef.getIdentity());            
            Sequence childSequence = document.getSequence(childComponentDef.getSequenceURIs().stream().findAny().orElse(null));

            int start = 1;
            
            if(childNode.isPrototype() || childSequence == null) {
                 prototypeFound = true;
            }
            
            // if the node is not a prototype, create a sequence for the module that concatanates the sequences of its children
               
            if(!prototypeFound) {

                // If sequence has not been created yet, create based on the first child sequence
                
                if (sequence == null) {
                    sequence = SBOLHelper.createNucleicAcidSequence(document, componentDef, version, childSequence.getElements());
                }
                
                // Otherwise, add the current sequence to the existing sequence, and identify the start and end nucleotides
                
                else {
                    start = sequence.getElements().length() + 1;
                    sequence.setElements(sequence.getElements() + childSequence.getElements());
                }
                
                int end = start + childSequence.getElements().length() - 1;
                
                SBOLHelper.createSequenceAnnotation(componentDef, childComponent, rangeIndex, start, end);
            
            } else {
                
                // If node is a prototype, add internal interactions
                
                /*    
                OG: Leave this for now...
          
                if(childNode instanceof PrototypeNode) {
                    List<Interaction> internalEvents = ((PrototypeNode)childNode).getInternalEvents();
                    if(internalEvents!=null && internalEvents.size()>0) {
                        for(Interaction interaction:internalEvents) {
                             SVPToSBOL.addInteractionModuleDefinition(document, baseURL, interaction);
                        }
                    }
                }   
                        */
                // Create Component 
                
            }

            if(prevChild!=null) {   
                Component prevComponent = null;

                // OG: ComponentDef.getSubComponent(URI) returns null, so need to loop through instead

                for(Component c: componentDef.getComponents()) {
                    if(c.getIdentity().equals(prevChild)) {
                        prevComponent = c;
                    }
                }                     

                SBOLHelper.createSequenceConstraint(componentDef, prevComponent, childComponent, rangeIndex);
            }
            
            prevChild = childComponent.getIdentity();
            rangeIndex++;
        }
            
  
        return componentDef;
    }

    private ComponentDefinition createComponentDefinition(SBOLDocument document, String baseURL, GRNTreeNode node, String version) throws SBOL2SerialisationException, SBOLValidationException
    {
        // Create the component definition
 
        ComponentDefinition componentDef = document.createComponentDefinition(
                SBOLHelper.getValidDisplayId(node.getId()),
                version,
                new HashSet<>(Arrays.asList(getNodeType(node))));

        if (node.getName() != null) {
            componentDef.setName(node.getName());
        }
        
        Part part = node.getSVP();
        if (part != null) {
            
            // Add properties from SVP to component definition
            
            SVPToSBOL.addSVPProperties(componentDef, part,document, version);
            
            // Create the nucleotide sequence for the component definition
            
            SBOLHelper.createNucleicAcidSequence(document, componentDef, version, part.getSequence());
        }
        
        // Add any custom properties from SynBad
        
        addSynBadProperties(componentDef,node);
       
        return componentDef;
    }

    private void addSynBadProperties(ComponentDefinition componentDef, GRNTreeNode node)
    {
        if (node.getParent()==null)
        {
            SBOLHelper.addAnnotation(componentDef, Terms.synbadTerms.isRoot, "true");
        }
        if (node.isPrototype())
        {
             SBOLHelper.addAnnotation(componentDef, Terms.synbadTerms.isPrototype, "true");
        }        
        if (node.isTranscriptionUnit())
        {
             SBOLHelper.addAnnotation(componentDef, Terms.synbadTerms.isTranscriptionUnit, "true");
        }
        if (GRNTreeHelper.isRegulatedTranscriptionUnit(node))
        {
             SBOLHelper.addAnnotation(componentDef, Terms.synbadTerms.isRegulatedTranscriptionUnit, "true");
        }   
    }
    
    private void createFunctionalComponents(SBOLDocument document, GRNTreeNode node, String baseURL, ModuleDefinition moduleDefinition) throws SBOLValidationException
    {
        // Creates functional components for input and outputs
        
        List<GRNTreeNode> nodes = node.getInputNodes();
        for (GRNTreeNode publicnode : nodes) {
            
            ComponentDefinition definition = SBOLHelper.getComponetDefinition(document, node);
            
            moduleDefinition.createFunctionalComponent(
                    SBOLHelper.getValidDisplayId(publicnode.getId()),
                    AccessType.PUBLIC,
                    definition.getDisplayId(),
                    definition.getVersion(),
                    DirectionType.IN);
        }

        nodes = node.getOutputNodes();
        
        for (GRNTreeNode publicnode : nodes) {
            
            ComponentDefinition definition = SBOLHelper.getComponetDefinition(document, node);
           
            moduleDefinition.createFunctionalComponent(
                    SBOLHelper.getValidDisplayId(publicnode.getId()),
                    AccessType.PUBLIC,
                    definition.getDisplayId(),
                    definition.getVersion(),
                    DirectionType.OUT);
        }
    }

    private void addInteractions(GRNTree tree, SBOLDocument document, String version, String baseURL)
    {
        // Retrieve all interactions from the tree and add to document
        
        Set<Interaction> interactions = tree.getInteractions();
        for (Interaction interaction : interactions)
        {            
            List<GRNTreeNode> parts = tree.getParts(interaction);
            try {
                SVPToSBOL.addInteractionModuleDefinition(document, baseURL, version, interaction, parts);
            } catch (SBOLValidationException ex) {
                logger.error(ex.getMessage());
            }
        }
    }

    private URI getComponentDefURI(String baseURL, GRNTreeNode node)
    {
        return URIFactory.getComponentDefURI(baseURL, getNodeDisplayID(node));
    }

    private URI getModuleDefURI(String baseURL, GRNTreeNode node)
    {
        return URIFactory.getModuleDefURI(baseURL,  getNodeDisplayID(node));
    }

    private URI getFunctionalCompURI(ModuleDefinition moduleDef, GRNTreeNode node)
    {
         return URIFactory.getFunctionalCompURI(moduleDef, getNodeDisplayID(node));
    }   

    private String getNodeDisplayID(GRNTreeNode node)
    {
        if (node.isLeafNode())
        {
            return node.getName();
        }
        else
        {
            return node.getId();
        }
    }

    private URI getNodeRole(GRNTreeNode node)
    {
        URI uri = Terms.componentRole.generic;
        Part part = node.getSVP();
        if (part != null)
        {
            uri = Mappings.toSO(part.getType());
        }
        return uri;
    }

    private URI getNodeType(GRNTreeNode node)
    {
        URI uri = Terms.componentType.dna;
        Part part = node.getSVP();
        if (part != null && !part.getMetaType().equals("Part"))
        {
            uri = Terms.componentType.smallMolecule;
        }
        return uri;
    }
}
