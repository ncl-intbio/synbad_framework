/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io;

import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.NetworkSerialiser;
import uk.ac.ncl.icos.svpcompiler.Compiler.SBMLCompiler;

/**
 *
 * @author owengilfellon
 */
public class SBMLSerialiser implements NetworkSerialiser<SBMLDocument> {

    private static final Logger logger = LoggerFactory.getLogger(SBMLSerialiser.class);
    
    @Override
    public GRNTree importNetwork(SBMLDocument toImport) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SBMLDocument exportNetwork(GRNTree toExport) {

            logger.debug("Exporting to SBML: {}", toExport.toString());
            
            // TODO: Update with name
            
            SBMLCompiler compiler = new  SBMLCompiler("untitled");
            compiler.setParts(GRNCompilableFactory.getCompilables(toExport));
            compiler.compileAll();
            return compiler.getDocument();
    }
}
