package uk.ac.ncl.icos.grntree.api;

import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class GRNTreeHelper {

    private static final SVPManager m = SVPManager.getSVPManager();

    public static List<BranchNode> getBranchNodes(GRNTree tree) {
        List<BranchNode> branches = new LinkedList<>();
        tree.getPreOrderIterator().forEachRemaining(n -> {
            if(n.isBranchNode() && (n.getChildren().isEmpty() || n.getChildren().stream().allMatch(GRNTreeNode::isLeafNode)))
                branches.add((BranchNode) n);
        });
        return branches;
    }

    public static List<LeafNode> getLeafNodes(GRNTree tree) {
        List<LeafNode> leaves = new LinkedList<>();
        tree.getPreOrderIterator().forEachRemaining(n -> {
            if(n.isLeafNode())
                leaves.add((LeafNode)n);
        });
        return leaves;
    }
    /**
     * Determines whether the node is a regulated Transcription Unit. Returns true if the node is a transcription
     * unit, as defined in isTranscriptionUnit(), and if the node contains an Operator or a Regulated Promoter.
     * @return
     */
    public static  boolean isRegulatedTranscriptionUnit(GRNTreeNode node) {

        if(!node.isBranchNode())
            return false;

        if(node.getChildren().isEmpty())
            return false;

        for (GRNTreeNode n : node.getChildren()) {
            if (n.isLeafNode()) {
                LeafNode ln = (LeafNode) n;
                if (ln.getType() == SVPType.Op || (ln.getType() == SVPType.Prom && m.isRegulatedPromoter(ln.getSVP()))) {
                    return true;
                }
            }
        }
        return false;
    }
}
