package uk.ac.ncl.icos.grntree.language;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteBaseListener;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteLexer;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteParser;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class ModuleRewriter extends SBWriteBaseListener {

    private class Range implements Comparable<Range> {

        private final int start;
        private final int end;
        public Range(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        @Override
        public int compareTo(Range o) {
            return start - o.start;
        }
    }
    private final static Logger LOGGER  = LoggerFactory.getLogger(ModuleRewriter.class);
    private boolean processingModule = false;
    private String svpWrite = "";
    private Map<String, ParseTree> modules = new HashMap<>();
    private int increment = 0;
    private TreeMap<Range, String> rewrites = new TreeMap<>();

    public String rewrite(String svpWrite) {
        this.svpWrite = svpWrite;
        rewrite();
        return this.svpWrite;
    }

    private void rewrite() {
        SBWriteParser parser = getParser( new ByteArrayInputStream(this.svpWrite.getBytes()));
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, parser.svpwrite());
        if(!rewrites.isEmpty()) {
            for (Range range : rewrites.descendingKeySet()) {
                String pre = svpWrite.substring(0, range.getStart());
                String insert = rewrites.get(range);
                String post = svpWrite.substring(range.getEnd());
                this.svpWrite = pre.concat(insert).concat(post);
            }
            rewrites.clear();
            rewrite();
        }
    }

    private SBWriteParser getParser(InputStream is) {
        CharStream charStream = null;
        try {
            charStream = CharStreams.fromStream(is);
        } catch (IOException e) {
            LOGGER.error("Could not get input", e);
            return null;
        }
        SBWriteLexer lexer = new SBWriteLexer(charStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        SBWriteParser parser = new SBWriteParser(tokenStream);
        return parser;
    }

    @Override
    public void enterSvm(SBWriteParser.SvmContext ctx) {
        super.enterSvm(ctx);
        if(!processingModule) {
            int index = ctx.getParent().children.indexOf(ctx);

            // substring to replace

            int startIndex = ctx.getStart().getStartIndex();
            int stopIndex = ((TerminalNode)ctx.getParent().getChild(index + 1)).getSymbol().getStopIndex() + 1;

            SBWriteParser.SvmDefContext svm = ((SBWriteParser.SvmDefContext)modules.get(ctx.ID().getText()));

            int instanceStart = svm.instanceList().getStart().getStartIndex();
            int instanceEnd = svm.instanceList().getStop().getStopIndex() + 1;

            List<String> instanceIds = ctx.svmParameterList() != null ?
                    ctx.svmParameterList().svmParameter().stream().map(n -> n.ID().getText().concat(n.STATE() == null ? "" : n.STATE().getText())).collect(Collectors.toList()) :
                    new LinkedList<>();
            List<String> definitionIds = svm.svmParameterList() != null ?
                    svm.svmParameterList().svmParameter().stream().map(n -> n.ID().getText().concat(n.STATE() == null ? "" : n.STATE().getText())).collect(Collectors.toList()) :
                    new LinkedList<>();

            String instanceList = svpWrite.substring(instanceStart, instanceEnd);

            Map<String, String> oldIdToNewId = new HashMap<>();

            for(SBWriteParser.SvpContext svp : svm.instanceList().svp()) {
                if (svp.ID() != null && !definitionIds.contains(svp.ID().getText())) {
                    oldIdToNewId.put(svp.ID().getText(), svp.ID().getText() + increment++);
                }
            }

            for(int i = svm.instanceList().getChildCount() - 1; i >= 0; i--) {

                ParseTree child = svm.instanceList().getChild(i);
                if(SBWriteParser.SvmContext.class.isAssignableFrom(child.getClass())) {
                    SBWriteParser.SvmContext c = (SBWriteParser.SvmContext)child;
                    if(c.svmParameterList() != null) {
                        for(int j = c.svmParameterList().getChildCount() - 1; j >= 0; j--) {
                            SBWriteParser.SvmParameterContext pc = c.svmParameterList().svmParameter(j);
                            if (pc != null && definitionIds.contains(pc.ID().getText())) {
                                String pre = instanceList.substring(0, pc.getStart().getStartIndex() - instanceStart );
                                String newID = instanceIds.get(definitionIds.indexOf(pc.ID().getText())) + (pc.STATE() != null ? pc.STATE().getText() : "");
                                String post = instanceList.substring(pc.getStop().getStopIndex() - instanceStart + 1);
                                instanceList = pre.concat( newID).concat(post);
                            } else if (pc != null && oldIdToNewId.containsKey(pc.ID().getText())) {
                                // Update IDs in modules so they're unique
                                String pre = instanceList.substring(0, pc.getStart().getStartIndex() - instanceStart );
                                String newID = oldIdToNewId.get(pc.ID().getText());
                                String post = instanceList.substring(pc.getStop().getStopIndex() - instanceStart + 1);
                                instanceList = pre.concat( newID).concat(post);
                            }
                        }
                    }
                } else if(SBWriteParser.SvpContext.class.isAssignableFrom(child.getClass())) {
                    SBWriteParser.SvpContext c = (SBWriteParser.SvpContext)child;
                    if (c.ID() != null && definitionIds.contains(c.ID().getText())) {
                        String pre = instanceList.substring(0, c.ID().getSymbol().getStartIndex() - instanceStart );
                        String newID = instanceIds.get(definitionIds.indexOf(c.ID().getText()));
                        String post = instanceList.substring(c.ID().getSymbol().getStopIndex() - instanceStart + 1);
                        instanceList = pre.concat( newID).concat(post);
                    }  else if (c.ID() != null) {

                        // Update IDs in modules so they're unique

                        String pre = instanceList.substring(0, c.ID().getSymbol().getStartIndex() - instanceStart );
                        String newID = oldIdToNewId.get(c.ID().getText());
                        String post = instanceList.substring(c.ID().getSymbol().getStopIndex() - instanceStart + 1);
                        instanceList = pre.concat( newID).concat(post);
                    }
                } else if(SBWriteParser.InteractionContext.class.isAssignableFrom(child.getClass())) {
                    SBWriteParser.InteractionContext c = (SBWriteParser.InteractionContext)child;

                    SBWriteParser.RightParticipantContext r = c.rightParticipant();
                    if (definitionIds.contains(r.getText())) {
                        String pre = instanceList.substring(0, r.getStart().getStartIndex() - instanceStart );
                        String newID = instanceIds.get(definitionIds.indexOf(r.getText()));
                        String post = instanceList.substring(r.getStop().getStopIndex() - instanceStart + 1);
                        instanceList = pre.concat( newID).concat(post);
                    } else if (oldIdToNewId.containsKey(r.getText())) {

                        // Update IDs in modules so they're unique

                        String pre = instanceList.substring(0, r.getStart().getStartIndex() - instanceStart );
                        String newID = oldIdToNewId.get(r.getText());
                        String post = instanceList.substring(r.getStop().getStopIndex() - instanceStart + 1);
                        instanceList = pre.concat( newID).concat(post);
                    }
                    SBWriteParser.LeftParticipantContext l = c.leftParticipant();
                    if (definitionIds.contains(l.getText())) {
                        String pre = instanceList.substring(0, l.getStart().getStartIndex() - instanceStart );
                        String newID = instanceIds.get(definitionIds.indexOf(l.getText()));
                        String post = instanceList.substring(l.getStop().getStopIndex() - instanceStart + 1);
                        instanceList = pre.concat( newID).concat(post);
                    } else if (oldIdToNewId.containsKey(l.getText())) {
                        // Update IDs in modules so they're unique
                        String pre = instanceList.substring(0, l.getStart().getStartIndex() - instanceStart );
                        String newID = oldIdToNewId.get(l.getText());
                        String post = instanceList.substring(l.getStop().getStopIndex() - instanceStart + 1);
                        instanceList = pre.concat( newID).concat(post);
                    }
                }
            }
            rewrites.put(new Range(startIndex, stopIndex), instanceList);
        }
    }

    private String doInteraction(String instanceList, SBWriteParser.InteractionContext c, Map<String, String> oldIdToNewId, int instanceStart, List<String> instanceIds, List<String> definitionIds) {


            return instanceList;
        }

    @Override
    public void enterSvmDef(SBWriteParser.SvmDefContext ctx) {
        super.enterSvmDef(ctx);
        processingModule = true;
    }

    @Override
    public void exitSvmDef(SBWriteParser.SvmDefContext ctx) {
        super.exitSvmDef(ctx);
        this.modules.put(ctx.ID().getText(), ctx);
        processingModule = false;
    }

}
