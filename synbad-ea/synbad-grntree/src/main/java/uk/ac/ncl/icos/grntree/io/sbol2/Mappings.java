/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import java.util.Set;

/**
 *
 * @author goksel
 */
public class Mappings {
    
    public static URI toSO(String vprType)
    {
        URI uri=Terms.componentRole.generic;
         
        switch(vprType)
           {
               case Terms.vprTypes.promoter:
                   uri=Terms.componentRole.promoter;
                   break;
               case Terms.vprTypes.rbs:
                   uri=Terms.componentRole.rbs;
                   break;
               case Terms.vprTypes.functionalPart:
                   uri=Terms.componentRole.cds;
                   break;
               case Terms.vprTypes.terminator:
                   uri=Terms.componentRole.terminator;
                   break;      
               case Terms.vprTypes.operator:
                   uri=Terms.componentRole.operator;
                   break;
               case Terms.vprTypes.shim:
                   uri=Terms.componentRole.shim;
                   break;                   
               case Terms.vprTypes.mrna:
                   uri=Terms.componentRole.mrna;
                   break;  
               case Terms.vprTypes.rna:
                   uri=Terms.componentRole.rna;
                   break;  
               default:
                   break;
           }
        return uri;
    }
    
    public static String fromSO(Set<URI> soURIs) throws SBOL2SerialisationException
    {
        String partType = null;
        for (URI soURI : soURIs)
        {
            String soId = getSoId(soURI);

            if (soId.equals(getSoId(Terms.componentRole.cds)))
            {
                partType = Terms.vprTypes.functionalPart;
            }
            else if (soId.equals(getSoId(Terms.componentRole.mrna)))
            {
                partType = Terms.vprTypes.mrna;
            }
            else if (soId.equals(getSoId(Terms.componentRole.operator)))
            {
                partType = Terms.vprTypes.operator;
            }
            else if (soId.equals(getSoId(Terms.componentRole.promoter)))
            {
                partType = Terms.vprTypes.promoter;
            }
            else if (soId.equals(getSoId(Terms.componentRole.rbs)))
            {
                partType = Terms.vprTypes.rbs;
            }
            else if (soId.equals(getSoId(Terms.componentRole.rna)))
            {
                partType = Terms.vprTypes.rna;
            }
            else if (soId.equals(getSoId(Terms.componentRole.shim)))
            {
                partType = Terms.vprTypes.shim;
            }
            else if (soId.equals(getSoId(Terms.componentRole.terminator)))
            {
                partType = Terms.vprTypes.terminator;
            }
            else
            {
                partType = soId;
            }

        }
        return partType;
    }
     
     private static String getSoId(URI soURI) throws SBOL2SerialisationException
     {
        String soTerm=soURI.toString();
        if (soTerm.length()>7)
        {
            return soTerm.substring(soTerm.length()-7);
        }
        else
        {
            throw new SBOL2SerialisationException("Incorrect SO URI:" + soURI);
        }
     }
    
}
