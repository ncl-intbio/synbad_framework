package uk.ac.ncl.icos.grntree.processing;

import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.SBWritableGraph;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultSBGraph;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

import java.util.*;

/**
 * Created by owengilfellon on 27/02/2014.
 */
public class Grapher {


   private GRNTree tree;

   public Grapher(GRNTree tree, String ignore)
   {
       this.tree = tree;
   }

   public String getId(Set<String> ids, String label) {
       int index = 0;
       String id = label + "_" + index++;
       while(!ids.add(id)) {
           id = label + "_" + index++;
       }
       return id;
   }

   public void doGraph() {
       SBWritableGraph<Node, String> g = new DefaultSBGraph<>();
       List<ComponentNode> pops = new LinkedList<>();
       List<InteractionNode> mrnas = new LinkedList<>();

       Set<String> ids = new HashSet<>();

       tree.getPreOrderIterator().forEachRemaining(n -> {

           if(n.isLeafNode()) {

               LeafNode ln = (LeafNode) n;

               if(ln.getType() == SVPType.Prom || ln.getType() == SVPType.Op) {
                   ComponentNode node = new ComponentNode(getId(ids, ln.getName()));
                   g.addNode(node);
                   pops.add(node);
               }

               if(ln.getType() == SVPType.RBS) {
                   InteractionNode node = new InteractionNode(getId(ids, "mRNA"));
                   g.addNode(node);
                   mrnas.add(node);
                   pops.forEach(pop -> {
                       g.addEdge(pop, node, pop.getLabel() + " -> " + node.getLabel());
                   });
               }

               if(ln.getType() == SVPType.CDS) {

                   // add protein
                    ComponentNode node = new ComponentNode(ln.getName());
                    g.addNode(node);
                    mrnas.forEach(mrna -> {
                        g.addEdge(mrna, node, mrna.getLabel() + " -> " + node.getLabel());
                    });

                   // add phosphorylated form

                   // add any small molecules
               }

               if(ln.getType() == SVPType.Ter) {
                    mrnas.clear();
                    pops.clear();
               }
           }
       });

       tree.getInteractions().stream().forEach(i -> {

           // add interaction

           g.addNode(new InteractionNode(i.getName()));

           // add products?

           // add edges to participants

           //i.getPartDetails().stream().forEach();

       });
   }

    // ------------------------------------
    //              Classes
    // ------------------------------------

   public static abstract class Node {

       private final String label;

       public Node(String label) {
           this.label = label;
       }

       public String getLabel() {
           return label;
       }

       public abstract NodeType getNodeType();
   }

   public static enum NodeType {
       COMPONENT,
       INTERACTION
   }

   public static class ComponentNode extends Node {

       public ComponentNode(String label) {
           super(label);
       }

       @Override
       public NodeType getNodeType() {
           return NodeType.COMPONENT;
       }
   }

   public static class InteractionNode extends Node {

       public InteractionNode(String label) {
           super(label);
       }

       @Override
       public NodeType getNodeType() {
           return NodeType.INTERACTION;
       }
   }
}
