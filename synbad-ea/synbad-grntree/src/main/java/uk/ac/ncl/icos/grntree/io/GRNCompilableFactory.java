package uk.ac.ncl.icos.grntree.io;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.svpcompiler.Compilable.Compilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpcompiler.Compilable.SignalType;

/**
 * Created by owengilfellon on 29/09/2014.
 */
public class GRNCompilableFactory extends CompilableFactory {


    public static List<ICompilable> getCompilables(GRNTree tree) {

        List<ICompilable> compilables = new  ArrayList<>();

        // Retrieve list of all compilable parts

        List<LeafNode> parts = new ArrayList<>();

        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
        while(i.hasNext()) {
            GRNTreeNode treeNode = i.next();
            if(treeNode.isLeafNode()) {
                parts.add((LeafNode)treeNode);
            }
        }

        for(LeafNode n : parts)
        {

            // Retrieve document from node

            if((!n.getOutputSignal().contains(SignalType.None) ||  n.getType().equals(SVPType.CDS) ))
            {
             //   SBMLDocument svpDocument = n.getDocument();
                Map<Interaction, List<Part>> extInteraction = new HashMap<>();

                // Create interaction models

                for(Interaction interaction: tree.getInteractions(n)) {

                    List<Part> interactionParts = tree.getParts(interaction).stream()
                            .filter(GRNTreeNode::isLeafNode)
                            .map(GRNTreeNode::getSVP)
                            .collect(Collectors.toList());

                    if(!interactionParts.isEmpty()) {
                        extInteraction.put(interaction, interactionParts);
                    }
                }
      
                List<ICompilable> c = (!extInteraction.isEmpty()) ?
                        CompilableFactory.getCompilable(n.getSVP(), n.getInternalEvents(), extInteraction, SVPManager.getRepositoryUrl()) :
                        CompilableFactory.getCompilable(n.getSVP(), n.getInternalEvents(), SVPManager.getRepositoryUrl());

                compilables.addAll(c);

            } else if (n.getType().equals(SVPType.Ter)){
                compilables.add(new Compilable(n.getSVP(),
                            n.getInternalEvents(),
                            null));
            }
        }

        return compilables;
    }
}
