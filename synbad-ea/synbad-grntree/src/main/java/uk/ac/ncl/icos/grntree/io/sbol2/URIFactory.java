/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.io.sbol2;

import java.net.URI;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.ModuleDefinition;

/**
 *
 * @author goksel
 */
public class URIFactory
{

    public static URI getModuleDefURI(String baseURL, String id)
    {
        return URI.create(baseURL + "moduledef/" + id);
    }

    public static URI getInteractionURI(ModuleDefinition moduleDef, String interactionID)
    {
        return URI.create(moduleDef.getIdentity().toString() + "/interaction/" + interactionID);
    }

    public static URI getFunctionalCompURI(ModuleDefinition moduleDef, String id)
    {
        return URI.create(moduleDef.getIdentity().toString() + "/fc/" + id);
    }

    public static URI getComponentDefURI(String baseURL, String id)
    {
        return URI.create(baseURL + "compdef/" + id);
    }

    public static URI getParticipationURI(URI interactionURI, String participationID)
    {
        return URI.create(interactionURI.toString() + "/participation/" + participationID);
    }

    public static URI getComponentURI(ComponentDefinition componentDef, String componentID)
    {
        return URI.create(componentDef.getIdentity().toString() + "/" + componentID);
    }
    
    public static URI getRangeURI(ComponentDefinition componentDef, String rangeId)
    {
        return  URI.create(componentDef.getIdentity().toString() + "/range/" + rangeId);
    }
    
     public static URI getSequenceAnnotationURI(ComponentDefinition componentDef, String annotationId)
    {
        return  URI.create(componentDef.getIdentity().toString() + "/annotation/" + annotationId);        
    }
     
    public static URI getSequenceURI(ComponentDefinition componentDef)
    {
        return URI.create(componentDef.getIdentity().toString() + "/na");
    }
    
      public static URI getSequenceConstraintURI(ComponentDefinition componentDef, String constraintId)
    {
        return  URI.create(componentDef.getIdentity().toString() + "/constraint/" + constraintId);        
    }
     
}
