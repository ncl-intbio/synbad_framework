package uk.ac.ncl.icos.grntree.api;

/**
 * Created by owengilfellon on 14/01/2015.
 */

import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.PrototypeNode;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.*;

import java.util.*;

import uk.ac.ncl.icos.grntree.impl.ConcreteNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.svpmanager.SVPManager;


/**
 * @author b1050029
 */
public class SVPHelper {

    private final static String repositoryURL = SVPManager.getRepositoryUrl();
    private final AbstractSVPManager manager = AbstractSVPManager.getSVPManager();

    private final static SVPHelper m = new SVPHelper();

    private final Random r = new Random();
    private final Property dataSource = new Property();
    private final Property dnaComponent = new Property();

    private SVPHelper()
    {
        dataSource.setName("Data Source");
        dataSource.setValue("None");
        dnaComponent.setName("type");
        dnaComponent.setValue("DnaComponent");
    }

    /**
     * Factory method for obtaining an SVPManager. The SVPManager is part singleton -
     * each method call returns part reference to the single SVPManager instance.
     * @return
     */
    public static SVPHelper getSVPHelper()
    {
        return m;
    }

    // ==============================================================
    //  Part Methods
    // ==============================================================

    public GRNTreeNode getConstPromoter(String id, double transcriptionRate)
    {
        Part part = manager.getConstPromoterPart(id);
        return(GRNTreeNodeFactory.getLeafNode(part, manager.getPoPSProduction(part, transcriptionRate), InterfaceType.NONE));
    }

    public GRNTreeNode getInducPromoter(String id, double transcriptionRate)
    {
        Part part = manager.getInducPromoterPart(id);
        return(GRNTreeNodeFactory.getLeafNode(part, manager.getPoPSProduction(part, transcriptionRate), InterfaceType.NONE));
    }

    public GRNTreeNode getRBS(String id, double translationRate)
    {
        Part rbs = manager.getRBS(id);
        return GRNTreeNodeFactory.getLeafNode(rbs,  manager.getRiPSProduction(rbs, translationRate), InterfaceType.NONE);
    }

    public GRNTreeNode getCDSEnvConst(String id, String smallMolecule, double degradationRate, double kf, double dePhos, double phosDeg)
    {
        Part part = manager.getCDS(id);
        List<Interaction> interactions = new LinkedList<>(manager.getProteinProductionAndDegradation(part, degradationRate));
        interactions.add(getPhosphorylationBySmlMol(part, smallMolecule, kf));
        interactions.addAll(manager.getDephosphorylationandDegradation(part, dePhos, phosDeg));
        return GRNTreeNodeFactory.getLeafNode(part, interactions, InterfaceType.NONE);
    }

    public GRNTreeNode getCDS(String id, double degradationRate)
    {
        Part part = manager.getCDS(id);
        return GRNTreeNodeFactory.getLeafNode(part, manager.getProteinProductionAndDegradation(part, degradationRate), InterfaceType.NONE);
    }

    public GRNTreeNode getCDSWithPhosphorylated(String id, double degradationRate, double dePhosRate, double phosDegRate)
    {
        Part part = manager.getCDS(id);
        List<Interaction> interactions = new LinkedList<>(manager.getProteinProductionAndDegradation(part, degradationRate));
        interactions.addAll(manager.getDephosphorylationandDegradation(part, dePhosRate, phosDegRate));
        return GRNTreeNodeFactory.getLeafNode(part, interactions, InterfaceType.NONE);
    }

    /*
    public GRNTreeNode updateWithParam(LeafNode n, String paramName, double value) {
        n.getSVP();

        Set<Interaction> interactions = new HashSet<>();
        boolean modified = false;

        for(Interaction i : n.getInternalEvents()) {
            boolean containsParam = i.getParameters().stream().anyMatch(p -> p.getParameterType().equals(paramName));

            if(!containsParam)
                interactions.add(i);
            else {
                List<Parameter> parameters = new LinkedList<>();
                for(Parameter p : i.getParameters()) {
                    if(!modified && p.getParameterType().equals(paramName)) {
                        parameters.add(new Parameter(p.getParameterType(), p.getName(), value, p.getEvidenceType(), p.getScope()));
                        modified = true;
                    } else {
                        parameters.add(p);
                    }
                }

                Interaction interaction = new Interaction();
                interaction.setIsInternal(i.getIsInternal());
                interaction.setParameters(parameters);
                interaction.setConstraints(i.getConstraints());
                interaction.setDescription(i.getDescription());
                interaction.setFreeTextMath(i.getFreeTextMath());
                interaction.setInteractionType(i.getInteractionType());
                interaction.setIsReaction(i.getIsReaction());
                interaction.setIsReversible(i.getIsReversible());
                interaction.setMathName(i.getMathName());
                interaction.setName(i.getName());
                interaction.

                interactions.add(interaction);/
            }
        }
    }*/

    public GRNTreeNode getOperator(String id)
    {
        Part part = manager.getNegativeOperator(id);
        return GRNTreeNodeFactory.getLeafNode(part, manager.getOperatorPoPSModulation(part), InterfaceType.NONE);
    }

    public GRNTreeNode getTerminator(String id)
    {
        Part part = new Part();
        part.setName(id);
        part.setDisplayName(id);
        part.setType("Terminator");
        part.setMetaType("Terminator");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property property = new Property();
        property.setName("type");
        property.setValue("Terminator");
        part.setProperties(properties);
        part.setStatus("Prototype");
        return(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.NONE));
    }

    public GRNTreeNode getShim(String id)
    {
        Part part = new Part();
        part.setName(id);
        part.setDisplayName(id);
        part.setType("Shim");
        part.setMetaType("Shim");
        part.setMetaType("Part");

        List<Property> properties = new ArrayList<Property>();
        properties.add(dataSource);
        properties.add(dnaComponent);
        Property property = new Property();
        property.setName("type");
        property.setValue("Shim");
        part.setProperties(properties);
        part.setStatus("Prototype");

        return(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.NONE));
    }

    // ==============================================================
    //  Interaction Methods
    // ==============================================================

    private Interaction getPhosphorylationBySmlMol(Part phosphorylated, String smallMolecule, double kf) {
        StringBuilder sb = new StringBuilder();
        sb.append(phosphorylated.getName()).append(" + ").append(smallMolecule)
                .append(" -> ")
                .append(phosphorylated.getName()).append("~P").append(" + ").append(smallMolecule);

        List<InteractionPartDetail> constraints = new ArrayList<InteractionPartDetail>();
        constraints.add(new InteractionPartDetail(smallMolecule, "Default", "Modifier", 1, "EnvironmentConstant"));
        constraints.add(new InteractionPartDetail(phosphorylated.getName(), "Default", "Input", 1, "Protein"));
        constraints.add(new InteractionPartDetail(phosphorylated.getName(), "Phosphorylated", "Output", 1, "ProteinPhosphorylated"));

        Interaction interaction = new Interaction();
        interaction.AddPart(phosphorylated.getName());
        interaction.AddPart(smallMolecule);
        interaction.setName(phosphorylated.getName() + "_ph_by_" + smallMolecule);

        interaction.setInteractionType("Phosphorylation");
        interaction.setIsReaction(true);
        interaction.setIsInternal(true);
        interaction.setFreeTextMath(sb.toString());
        interaction.setMathName("PhosphorylationWithModifier");
        interaction.setPartDetails(constraints);

        Parameter kfPar = new Parameter();
        kfPar.setName("kf");
        kfPar.setParameterType("kf");
        kfPar.setScope("Local");
        kfPar.setValue(kf);
        interaction.AddParameter(kfPar);

        return interaction;
    }
    
        
    public PrototypeNode convertPrototypeNode(GRNTreeNode node) {
        
        if(node.isPrototype()) {
            return (PrototypeNode)node;
        }
        
        PrototypeNode prototype = (PrototypeNode)GRNTreeNodeFactory.getLeafNode(node.getSVP(),
                SVPManager.getSVPManager().getInternalEvents(node.getSVP()),
                node.getInterfaceType());
        
        prototype.setName(node.getName());
        prototype.getSVP().setStatus("Prototype");
        
        return prototype;
    }
}

