/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;

/**
 *
 * @author ogilfellon
 */
public enum SVPParameterType {

    TRANSCRIPTION_RATE("ktr"),
    DNA_BINDING("Km"),
    SPECIES_BINDING("kf"),
    TRANSLATION_RATE("ktl"),
    DEGRADATION("kd"),
    DEPHOSPHORYLATION("kdeP");

    private final String MATH_NAME;

    SVPParameterType(String mathName) {
        this.MATH_NAME = mathName;
    }

    public String getMathName() {
        return MATH_NAME;
    }

    public static SVPParameterType fromString(String state) {
        switch(state.toLowerCase()) {
            case "ktr":
                return TRANSCRIPTION_RATE;
            case "ktl":
                return TRANSLATION_RATE;
            case "km":
                return DNA_BINDING;
            case "kf":
                return SPECIES_BINDING;
            case "kd":
                return DEGRADATION;
            case "kdep":
                return DEPHOSPHORYLATION;
        }
        return null;
    }
}