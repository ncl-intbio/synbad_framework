package uk.ac.ncl.icos.grntree.language;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.InteractionType;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.language.antlr.*;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Parameter;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.*;
import java.util.*;

public class Compiler2 extends SBWriteBaseListener {

    private final AbstractSVPManager MANAGER = AbstractSVPManager.getSVPManager();
    private final SVPHelper HELPER = SVPHelper.getSVPHelper();

    private final static Logger LOGGER  = LoggerFactory.getLogger(Compiler2.class);
    private Map<String, SBWriteParser.SvmDefContext> definitions;
    private boolean processingModuleDef = false;
    private int inc = 0;
    private final GRNTree tree;
    private final List<InteractionCtx> interactions = new LinkedList<>();

    public Compiler2() {
        this.tree = GRNTreeFactory.getGRNTree();
    }

    public GRNTree compile(String svpWrite) {
        return internalCompile(svpWrite);
    }

    public GRNTree compile(InputStream is) {
        try( BufferedReader br = new BufferedReader( new InputStreamReader( is ))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while(( line = br.readLine()) != null ) {
                sb.append( line );
                sb.append( '\n' );
            }
            return internalCompile(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private GRNTree internalCompile(String svpWrite) {
        this.definitions = new ModuleDefinitionGatherer().getModules(svpWrite);
        SBWriteParser parser = getParser( new ByteArrayInputStream(svpWrite.getBytes()));
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, parser.svpwrite());
        return tree;
    }

    @Override
    public void enterSvmDef(SBWriteParser.SvmDefContext ctx) {
        super.enterSvmDef(ctx);
        processingModuleDef = true;
    }

    @Override
    public void exitSvmDef(SBWriteParser.SvmDefContext ctx) {
        super.exitSvmDef(ctx);
        processingModuleDef = false;
    }

    private Double getParFromNode(LeafNode n, String parKey) {
         return n.getInternalEvents()
            .stream().flatMap(in -> in.getParameters().stream())
            .filter(par -> par.getName().equals(parKey))
            .map(Parameter::getValue).findFirst().orElse(0.0);
    }

    @Override
    public void enterInstanceList(SBWriteParser.InstanceListContext ctx) {
        if(!processingModuleDef) {
            CompilerContext c = new ModuleInstancer().visitInstanceList(ctx);
            tree.getRootNode().addNode(c.node);
            for(InteractionCtx i : c.interactions) {

                Part left = tree.getPart(i.leftId);
                Part right = tree.getPart(i.rightId);

                if(left != null) {
                    switch (i.type) {
                        case ACTIVATION:
                            tree.getParts(i.rightId).stream().forEach(n -> {
                                try {
                                    GRNTreeNode p = HELPER.getInducPromoter(i.rightId, getParFromNode((LeafNode)n, "ktr"));
                                    n.replaceNode(p);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                            tree.addInteraction(MANAGER.getRegulationInteraction(right, left, i.leftPhos ? MolecularForm.PHOSPHORYLATED : MolecularForm.DEFAULT, RegulationRole.ACTIVATOR, i.parameters.get("km")));
                            break;
                        case REPRESSION:
                            tree.addInteraction(MANAGER.getRegulationInteraction(right, left, i.leftPhos ? MolecularForm.PHOSPHORYLATED : MolecularForm.DEFAULT, RegulationRole.REPRESSOR, i.parameters.get("km")));
                            break;
                        case PHOSPHORYLATION:
                           tree.getParts(i.rightId).stream().forEach(n -> {
                                try {
                                    GRNTreeNode p = HELPER.getCDSWithPhosphorylated(i.rightId, getParFromNode((LeafNode)n, "kd"),  i.parameters.get("kdep"),  i.parameters.get("kd"));
                                    n.replaceNode(p);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                            tree.addInteraction(MANAGER.getPhosphorylationInteraction(left, tree.getPart(i.rightId), i.parameters.get("kf")));
                            break;
                    }
                    LOGGER.debug("{}{} {} {}{}", i.leftId, i.leftPhos ? "~P" : "", i.type.toString(), i.rightId, i.rightPhos ? "~P" : "");
                } else {
                    if(i.type == InteractionType.PHOSPHORYLATION) {
                        for (GRNTreeNode part : tree.getParts(i.rightId)) {
                            try {
                                part.replaceNode(HELPER.getCDSEnvConst(i.rightId, i.leftId, getParFromNode((LeafNode)part, "kd"), i.parameters.get("kf"), i.parameters.get("kdep"), i.parameters.get("kd")));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    LOGGER.debug("{}{} {} {}{}", i.leftId, i.leftPhos ? "~P" : "", i.type.toString(), i.rightId, i.rightPhos ? "~P" : "");
                }
            }
        }
    }

    private static SBWriteParser getParser(InputStream is) {
        CharStream charStream = null;
        try {
            charStream = CharStreams.fromStream(is);
        } catch (IOException e) {
            LOGGER.error("Could not get input", e);
            return null;
        }
        SBWriteLexer lexer = new SBWriteLexer(charStream);
        TokenStream tokenStream = new CommonTokenStream(lexer);
        return new SBWriteParser(tokenStream);
    }

    private final class CompilerContext {

        private final List<InteractionCtx> interactions;
        private final GRNTreeNode node;

        public CompilerContext(GRNTreeNode node, List<InteractionCtx> interactions) {
            this.interactions = new LinkedList<>(interactions);
            this.node = node;
        }
    }

    private static final class InteractionCtx {

        private final String leftId;
        private final boolean leftPhos;

        private final String rightId;
        private final boolean rightPhos;

        private final InteractionType type;

        private final Map<String, Double> parameters;

        public InteractionCtx(String leftId, boolean leftPhos, String rightId, boolean rightPhos, InteractionType type) {
            this.leftId = leftId;
            this.leftPhos = leftPhos;
            this.rightId = rightId;
            this.rightPhos = rightPhos;
            this.type = type;
            this.parameters = new HashMap<>();
        }

        public InteractionCtx(String leftId, boolean leftPhos, String rightId, boolean rightPhos, InteractionType type, Map<String, Double> parameters) {
            this.leftId = leftId;
            this.leftPhos = leftPhos;
            this.rightId = rightId;
            this.rightPhos = rightPhos;
            this.type = type;
            this.parameters = new HashMap<>(parameters);
        }

        InteractionCtx addParameter(String name, Double value) {
            Map<String, Double> newParameters = new HashMap<>(parameters);
            newParameters.put(name, value);
            return new InteractionCtx(leftId, leftPhos, rightId, rightPhos, type, newParameters);
        }
    }



    private final class ModuleInstancer extends SBWriteBaseVisitor<CompilerContext> {

        private final SVPHelper HELPER = SVPHelper.getSVPHelper();
        private final Random r = new Random();
        private final Stack<ModuleContext> contextStack = new Stack<>();
        private final Set<String> allIds = new HashSet<>();

        public CompilerContext visitSvmDef(SBWriteParser.SvmDefContext ctx) {
            return visitInstanceList(ctx.instanceList());
        }

        public CompilerContext visitInstanceList(SBWriteParser.InstanceListContext ctx) {
            GRNTreeNode node = GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH);
            List<InteractionCtx> ctxs = new LinkedList<>();
            for(ParseTree c : ctx.children) {
                if(c instanceof SBWriteParser.SvpContext)
                    node.addNode(visitSvp((SBWriteParser.SvpContext) c).node);
                if(c instanceof SBWriteParser.SvmContext) {
                    CompilerContext cc = visitSvm((SBWriteParser.SvmContext) c);
                    node.addNode(cc.node);
                    ctxs.addAll(cc.interactions);
                }
                if(c instanceof SBWriteParser.InteractionContext)
                   ctxs.addAll(visitInteraction((SBWriteParser.InteractionContext) c).interactions);
            };

            // Remove branch nodes that only contain another branch node

            if(node.getChildren().size() == 1 && node.getChildren().get(0).isBranchNode())
                node = node.getChildren().get(0);

            return new CompilerContext(node, ctxs);
        }

        @Override
        public CompilerContext visitInteraction(SBWriteParser.InteractionContext ctx) {

            String left = ctx.leftParticipant().ID().getText();
            String right = ctx.rightParticipant().ID().getText();

            // if interaction variable is passed as parameter, update

            if (!contextStack.isEmpty()){
                ModuleContext c = contextStack.peek();
                if(c.instanceVariables.containsKey(left)) {
                    left = c.getVariable(left);
                }
                if(c.instanceVariables.containsKey(right)) {
                    right = c.getVariable(right);
                }
            }

            // if parameters specify state

            boolean left_phos = left.endsWith("~P");
            if(left_phos)
                left = left.substring(0, left.length() - 2);

            boolean right_phos = right.endsWith("~P");
            if(right_phos)
                right = right.substring(0, right.length() - 2);

            // if interaction definition specifies state

            if(ctx.leftParticipant().STATE() != null)
                left_phos = true;

            if(ctx.rightParticipant().STATE() != null)
                right_phos = true;

            InteractionType type = null;
            List<String> expectedParameters = new LinkedList<>();

           switch(ctx.INTERACTION_OPERATOR().getText()) {
                case "-*":
                    type = InteractionType.PHOSPHORYLATION;
                    expectedParameters.addAll(Arrays.asList("kd", "kf", "kdeP"));
                    break;
                case "->":
                    type = InteractionType.ACTIVATION;
                    expectedParameters.addAll(Arrays.asList( "km"));
                    break;
                case "-|":
                    type = InteractionType.REPRESSION;
                    expectedParameters.addAll(Arrays.asList("km"));
                    break;
            }

            ctx.ANNOTATION().forEach(x -> {
                if(x.getText().startsWith("@description")) {
                    LOGGER.debug("\t\tDescription:\t{}", x.getText().substring(13));
                } else if(x.getText().startsWith("@url")) {
                    LOGGER.debug("\t\tURL:\t{}", x.getText().substring(5));
                } else if(x.getText().startsWith("@organism")) {
                    LOGGER.debug("\t\tOrganism:\t{}", x.getText().substring(10));
                } else if(x.getText().startsWith("@displayName")) {
                    LOGGER.debug("\t\tDisplayName:\t{}", x.getText().substring(13));
                }
            });

            InteractionCtx iCtx = new InteractionCtx(left, left_phos, right, right_phos, type);
            int parIndex = 0;
            for (String p: expectedParameters) {
                iCtx = iCtx.addParameter(p, getParameter(ctx, p, expectedParameters));
            }

            return new CompilerContext(null, Arrays.asList(iCtx));
        }

        @Override
        public CompilerContext visitSvm(SBWriteParser.SvmContext ctx) {
            LOGGER.debug("{}", ctx.ID().getText());
            if(definitions.containsKey(ctx.ID().getText())) {
                SBWriteParser.SvmDefContext defCtx = definitions.get(ctx.ID().getText());
                updateContextStack(defCtx, ctx);
                CompilerContext n = visitSvmDef(defCtx);
                contextStack.pop();
                return  n;
            }

            return new CompilerContext(GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH), Collections.EMPTY_LIST);
        }

        @Override
        public CompilerContext visitSvp(SBWriteParser.SvpContext ctx) {



            String id = ctx.ID() == null ? "_" : ctx.ID().getText();

            if(id.equals("_"))
                // if anonymous part, create identifier
                id = ctx.TYPE().getText().toLowerCase() + "_" + inc++;
            else if (!contextStack.isEmpty() && contextStack.peek().instanceVariables.containsKey(id)){
                // if passed in by instantiation, update identifier
                id = contextStack.peek().getVariable(id);
                if(id.endsWith("~P"))
                    id = id.substring(0, id.length() - 2);
            } else {
                // update internal identifiers and add to context
                String oldId = id;
                if(allIds.contains(id)) {
                    id = id + "_" + inc++;
                }
                if(!contextStack.isEmpty()) {
                    contextStack.peek().instanceVariables.put(oldId, id);
                }
            }
            allIds.add(id);
            LOGGER.debug("\t{}", id);
            GRNTreeNode svp = null;
            switch (ctx.TYPE().getText()) {
                case "Prom":
                    svp = HELPER.getConstPromoter(id, getParameter(ctx, "ktr", Arrays.asList( "ktr" )));
                    break;
                case "Op":
                    svp = HELPER.getOperator(id);
                    break;
                case "RBS":
                    svp = HELPER.getRBS(id, getParameter(ctx, "ktl", Arrays.asList( "ktl" )));
                    break;
                case "CDS":
                    svp = HELPER.getCDS(id, getParameter(ctx, "kd", Arrays.asList( "kd" )));
                    break;
                case "Ter":
                    svp = HELPER.getTerminator(id);
                    break;
            }
            return new CompilerContext(svp, Collections.EMPTY_LIST);
        }

        private double getParameter(SBWriteParser.InteractionContext ctx, String id, List<String> expectedIds) {
            if(ctx.svpParameterList() == null || !expectedIds.contains(id))
                return r.nextDouble();

            List<SBWriteParser.SvpParameterContext> listContext = ctx.svpParameterList().svpParameter();
            String par = listContext.get(expectedIds.indexOf(id)).PARAM().getText();

            try {
                return Double.parseDouble(par);
            } catch (NumberFormatException ex ){
                return r.nextDouble();
            }
        }

        private double getParameter(SBWriteParser.SvpContext ctx, String id, List<String> expectedIds) {
            if(ctx.svpParameterList() == null || !expectedIds.contains(id))
                return r.nextDouble();

            List<SBWriteParser.SvpParameterContext> listContext = ctx.svpParameterList().svpParameter();
            String par = listContext.get(expectedIds.indexOf(id)).PARAM().getText();

            try {
                return Double.parseDouble(par);
            } catch (NumberFormatException ex ){
                return r.nextDouble();
            }
        }

        private final void updateContextStack(SBWriteParser.SvmDefContext defCtx, SBWriteParser.SvmContext ctx) {

            Map<String, String> variables = new HashMap<>();

            if(defCtx.svmParameterList() != null) {

                if(ctx.svmParameterList() == null || ctx.svmParameterList().svmParameter().size() != defCtx.svmParameterList().svmParameter().size())
                    LOGGER.error("Definition {} requires {} parameters", defCtx.ID().getText(), defCtx.svmParameterList().svmParameter().size());

                ModuleContext cc = contextStack.isEmpty() ? null : contextStack.peek();

                for (int i = 0; i < defCtx.svmParameterList().svmParameter().size(); i++) {
                    SBWriteParser.SvmParameterContext c = defCtx.svmParameterList().svmParameter().get(i);
                    SBWriteParser.SvmParameterContext c2 = ctx.svmParameterList().svmParameter().get(i);
                    String state = c2.STATE() != null ? c2.STATE().getText() : "";
                    variables.put(c.ID().getText(), cc != null && cc.instanceVariables.containsKey(c2.ID().getText()) ? cc.getVariable(c2.ID().getText()) + state : c2.getText());
                }
            }

            contextStack.push(new ModuleContext(variables));
        }

        private class ModuleContext {

            private final Map<String, String> instanceVariables;

            ModuleContext(Map<String, String> instanceVariables) {
                this.instanceVariables = instanceVariables;
            }

            public String getVariable(String variable) {
                return instanceVariables.get(variable);
            }
        }
    }

    private class ModuleDefinitionGatherer extends SBWriteBaseListener {

        private final Map<String, SBWriteParser.SvmDefContext> modules = new HashMap<>();

        public Map<String, SBWriteParser.SvmDefContext> getModules(String svpWrite) {
            SBWriteParser parser = getParser( new ByteArrayInputStream(svpWrite.getBytes()));
            ParseTreeWalker walker = new ParseTreeWalker();
            walker.walk(this, parser.svpwrite());
            return modules;
        }

        @Override
        public void exitSvmDef(SBWriteParser.SvmDefContext ctx) {
            super.exitSvmDef(ctx);
            LOGGER.debug("Storing {}", ctx.ID().getText());
            modules.put(ctx.ID().getText(), ctx);
        }
    }
}
