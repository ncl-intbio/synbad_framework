package uk.ac.ncl.icos.grntree.impl;

public class GRNParameter {

    private final double value;
    private final SVPParameterType parameterType;

    public GRNParameter(double value, String mathName) {
        this(value, SVPParameterType.fromString(mathName));
    }

    public GRNParameter(double value, SVPParameterType mathName) {
        this.value = value;
        this.parameterType = mathName;
    }

    public double getValue() {
        return value;
    }

    public SVPParameterType getParameterType() {
        return parameterType;
    }

    @Override
    public String toString() {
        return parameterType.getMathName() + ":  [ " + value + " ]";
    }
}
