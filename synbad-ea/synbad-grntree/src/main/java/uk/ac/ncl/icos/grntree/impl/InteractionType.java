/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.impl;

/**
 *
 * @author owengilfellon
 */
public enum InteractionType {

    ACTIVATION,
    REPRESSION,
    PHOSPHORYLATION;

    public static InteractionType fromString(String operator) {
        switch(operator.toLowerCase()) {
            case "->":
                return ACTIVATION;
            case "-|":
                return REPRESSION;
            case "-*":
                return PHOSPHORYLATION;
        }

        if(operator.toLowerCase().contains("activation"))
            return  ACTIVATION;

        if(operator.toLowerCase().contains("repression"))
            return REPRESSION;

        if(operator.toLowerCase().contains("phosphorylation")){
            return PHOSPHORYLATION;
        }
        return null;
    }

}


