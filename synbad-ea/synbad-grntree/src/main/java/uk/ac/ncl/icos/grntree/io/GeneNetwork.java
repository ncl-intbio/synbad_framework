package uk.ac.ncl.icos.grntree.io;


import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.GRNInteractionEdge;
import uk.ac.ncl.icos.grntree.impl.InteractionType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public class GeneNetwork {

    Set<GRNInteractionEdge> edges = new HashSet<GRNInteractionEdge>();
    List<GRNTreeNode> nodes = new ArrayList<GRNTreeNode>();

    public void addNode(GRNTreeNode node)
    {
        nodes.add(node);
    }

    public void addEdge(GRNTreeNode from, GRNTreeNode to, InteractionType type)
    {
        edges.add(new GRNInteractionEdge(from, to, type));
    }

    public List<GRNTreeNode> getNodes() {
        return nodes;
    }

    public List<GRNInteractionEdge> getEdges() {
        List<GRNInteractionEdge> edges = new ArrayList<GRNInteractionEdge>();
        for(GRNInteractionEdge edge:this.edges) {
            edges.add(edge);
        }
        return edges;
    }


}

