package uk.ac.ncl.icos.grntree.language;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteBaseVisitor;
import uk.ac.ncl.icos.grntree.language.antlr.SBWriteParser;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InstanceProcessor extends SBWriteBaseVisitor<InstanceProcessor> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InstanceProcessor.class);

//    @Override
//    public InstanceProcessor visitSvpParameter(SBWriteParser.SvpParameterContext ctx) {
//        LOGGER.debug("\t\tPAR:{}={}", ctx.PARAM_TYPE(), ctx.PARAM());
//        return super.visitSvpParameter(ctx);
//    }
//
//    @Override
//    public InstanceProcessor visitSvmParameter(SBWriteParser.SvmParameterContext ctx) {
//        LOGGER.debug("\t\tPAR: {}:{}", ctx.ID(), ctx.STATE());
//        return super.visitSvmParameter(ctx);
//    }

    @Override
    public InstanceProcessor visitInteraction(SBWriteParser.InteractionContext ctx) {
        LOGGER.debug("\t\tI:  {} {} {} ", ctx.leftParticipant(), ctx.INTERACTION_OPERATOR(), ctx.rightParticipant());
        return super.visitInteraction(ctx);
    }

    @Override
    public InstanceProcessor visitSvp(SBWriteParser.SvpContext ctx) {
        LOGGER.debug("\t\tSVP: {}: {}", ctx.ID(), ctx.TYPE());
        List<String> pars = new LinkedList<>();
        for(SBWriteParser.SvpParameterContext pc : ctx.svpParameterList().svpParameter()) {
            String s = pc.PARAM().getText();
            s = s.concat(pc.PARAM_TYPE() != null ? pc.PARAM_TYPE().getText() : "");
            pars.add(s);
        }
        String p = pars.stream().reduce("", (a, b) -> a.concat(b).concat(", "));
        p = p.substring(0, p.length() - 2);
        LOGGER.debug("\t\tSVP: {}:{}({})", ctx.ID(),  ctx.TYPE(), p);

        return super.visitSvp(ctx);
    }

    @Override
    public InstanceProcessor visitSvm(SBWriteParser.SvmContext ctx) {

        List<String> pars = new LinkedList<>();
        for(SBWriteParser.SvmParameterContext pc : ctx.svmParameterList().svmParameter()) {
            String s = pc.ID().getText();
            s = s.concat(pc.STATE() != null ? pc.STATE().getText() : "");
            pars.add(s);
        }
        String p = pars.stream().reduce("", (a, b) -> a.concat(b).concat(", "));
        p = p.substring(0, p.length() - 2);
        LOGGER.debug("\t\tSVM: {}({})", ctx.ID(), p);

        return super.visitSvm(ctx);
    }
}
