// Generated from /Users/owengilfellon/Documents/Code/Misc/synbad_framework/synbad-ea/synbad-grntree/src/main/antlr4/uk/ac/ncl/icos/grntree/language/antlr/SBWrite.g4 by ANTLR 4.8
package uk.ac.ncl.icos.grntree.language.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SBWriteParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, IGNORE=11, ANNOTATION=12, INTERACTION_OPERATOR=13, PARAM_TYPE=14, 
		PARAM=15, TYPE=16, STATE=17, ID=18;
	public static final int
		RULE_svp = 0, RULE_svm = 1, RULE_leftParticipant = 2, RULE_rightParticipant = 3, 
		RULE_interaction = 4, RULE_instanceList = 5, RULE_svmDef = 6, RULE_svpDef = 7, 
		RULE_svpParameter = 8, RULE_svpParameterList = 9, RULE_svmParameter = 10, 
		RULE_svmParameterList = 11, RULE_svpwrite = 12;
	private static String[] makeRuleNames() {
		return new String[] {
			"svp", "svm", "leftParticipant", "rightParticipant", "interaction", "instanceList", 
			"svmDef", "svpDef", "svpParameter", "svpParameterList", "svmParameter", 
			"svmParameterList", "svpwrite"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "':'", "'('", "')'", "';'", "'module'", "'{'", "'}'", "'part'", 
			"'='", "','"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, "IGNORE", 
			"ANNOTATION", "INTERACTION_OPERATOR", "PARAM_TYPE", "PARAM", "TYPE", 
			"STATE", "ID"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SBWrite.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SBWriteParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class SvpContext extends ParserRuleContext {
		public TerminalNode TYPE() { return getToken(SBWriteParser.TYPE, 0); }
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public SvpParameterListContext svpParameterList() {
			return getRuleContext(SvpParameterListContext.class,0);
		}
		public SvpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvpContext svp() throws RecognitionException {
		SvpContext _localctx = new SvpContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_svp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(26);
				match(ID);
				setState(27);
				match(T__0);
				}
			}

			setState(30);
			match(TYPE);
			setState(35);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(31);
				match(T__1);
				setState(32);
				svpParameterList();
				setState(33);
				match(T__2);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvmContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public SvmParameterListContext svmParameterList() {
			return getRuleContext(SvmParameterListContext.class,0);
		}
		public SvmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvmContext svm() throws RecognitionException {
		SvmContext _localctx = new SvmContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_svm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37);
			match(ID);
			setState(38);
			match(T__1);
			setState(40);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(39);
				svmParameterList();
				}
			}

			setState(42);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LeftParticipantContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public TerminalNode STATE() { return getToken(SBWriteParser.STATE, 0); }
		public LeftParticipantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_leftParticipant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterLeftParticipant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitLeftParticipant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitLeftParticipant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LeftParticipantContext leftParticipant() throws RecognitionException {
		LeftParticipantContext _localctx = new LeftParticipantContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_leftParticipant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44);
			match(ID);
			setState(46);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STATE) {
				{
				setState(45);
				match(STATE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RightParticipantContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public TerminalNode STATE() { return getToken(SBWriteParser.STATE, 0); }
		public RightParticipantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rightParticipant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterRightParticipant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitRightParticipant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitRightParticipant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RightParticipantContext rightParticipant() throws RecognitionException {
		RightParticipantContext _localctx = new RightParticipantContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_rightParticipant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48);
			match(ID);
			setState(50);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STATE) {
				{
				setState(49);
				match(STATE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InteractionContext extends ParserRuleContext {
		public LeftParticipantContext leftParticipant() {
			return getRuleContext(LeftParticipantContext.class,0);
		}
		public TerminalNode INTERACTION_OPERATOR() { return getToken(SBWriteParser.INTERACTION_OPERATOR, 0); }
		public RightParticipantContext rightParticipant() {
			return getRuleContext(RightParticipantContext.class,0);
		}
		public List<TerminalNode> ANNOTATION() { return getTokens(SBWriteParser.ANNOTATION); }
		public TerminalNode ANNOTATION(int i) {
			return getToken(SBWriteParser.ANNOTATION, i);
		}
		public SvpParameterListContext svpParameterList() {
			return getRuleContext(SvpParameterListContext.class,0);
		}
		public InteractionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interaction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterInteraction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitInteraction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitInteraction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InteractionContext interaction() throws RecognitionException {
		InteractionContext _localctx = new InteractionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_interaction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ANNOTATION) {
				{
				{
				setState(52);
				match(ANNOTATION);
				}
				}
				setState(57);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(58);
			leftParticipant();
			setState(59);
			match(INTERACTION_OPERATOR);
			setState(60);
			rightParticipant();
			setState(66);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(61);
				match(T__1);
				setState(63);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PARAM_TYPE || _la==PARAM) {
					{
					setState(62);
					svpParameterList();
					}
				}

				setState(65);
				match(T__2);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstanceListContext extends ParserRuleContext {
		public List<SvpContext> svp() {
			return getRuleContexts(SvpContext.class);
		}
		public SvpContext svp(int i) {
			return getRuleContext(SvpContext.class,i);
		}
		public List<SvmContext> svm() {
			return getRuleContexts(SvmContext.class);
		}
		public SvmContext svm(int i) {
			return getRuleContext(SvmContext.class,i);
		}
		public List<InteractionContext> interaction() {
			return getRuleContexts(InteractionContext.class);
		}
		public InteractionContext interaction(int i) {
			return getRuleContext(InteractionContext.class,i);
		}
		public InstanceListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instanceList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterInstanceList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitInstanceList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitInstanceList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstanceListContext instanceList() throws RecognitionException {
		InstanceListContext _localctx = new InstanceListContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_instanceList);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(75); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					{
					setState(71);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
					case 1:
						{
						setState(68);
						svp();
						}
						break;
					case 2:
						{
						setState(69);
						svm();
						}
						break;
					case 3:
						{
						setState(70);
						interaction();
						}
						break;
					}
					setState(73);
					match(T__3);
					}
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(77); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvmDefContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public List<TerminalNode> ANNOTATION() { return getTokens(SBWriteParser.ANNOTATION); }
		public TerminalNode ANNOTATION(int i) {
			return getToken(SBWriteParser.ANNOTATION, i);
		}
		public SvmParameterListContext svmParameterList() {
			return getRuleContext(SvmParameterListContext.class,0);
		}
		public InstanceListContext instanceList() {
			return getRuleContext(InstanceListContext.class,0);
		}
		public SvmDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svmDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvmDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvmDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvmDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvmDefContext svmDef() throws RecognitionException {
		SvmDefContext _localctx = new SvmDefContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_svmDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ANNOTATION) {
				{
				{
				setState(79);
				match(ANNOTATION);
				}
				}
				setState(84);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(85);
			match(T__4);
			setState(86);
			match(ID);
			setState(87);
			match(T__1);
			setState(89);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(88);
				svmParameterList();
				}
			}

			setState(91);
			match(T__2);
			setState(92);
			match(T__5);
			setState(94);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ANNOTATION) | (1L << TYPE) | (1L << ID))) != 0)) {
				{
				setState(93);
				instanceList();
				}
			}

			setState(96);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvpDefContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public TerminalNode TYPE() { return getToken(SBWriteParser.TYPE, 0); }
		public List<TerminalNode> ANNOTATION() { return getTokens(SBWriteParser.ANNOTATION); }
		public TerminalNode ANNOTATION(int i) {
			return getToken(SBWriteParser.ANNOTATION, i);
		}
		public SvpParameterListContext svpParameterList() {
			return getRuleContext(SvpParameterListContext.class,0);
		}
		public List<InteractionContext> interaction() {
			return getRuleContexts(InteractionContext.class);
		}
		public InteractionContext interaction(int i) {
			return getRuleContext(InteractionContext.class,i);
		}
		public SvpDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svpDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvpDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvpDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvpDef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvpDefContext svpDef() throws RecognitionException {
		SvpDefContext _localctx = new SvpDefContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_svpDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ANNOTATION) {
				{
				{
				setState(98);
				match(ANNOTATION);
				}
				}
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(104);
			match(T__7);
			setState(105);
			match(ID);
			setState(106);
			match(T__0);
			setState(107);
			match(TYPE);
			setState(108);
			match(T__1);
			setState(110);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PARAM_TYPE || _la==PARAM) {
				{
				setState(109);
				svpParameterList();
				}
			}

			setState(112);
			match(T__2);
			setState(113);
			match(T__5);
			setState(119);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ANNOTATION || _la==ID) {
				{
				{
				setState(114);
				interaction();
				setState(115);
				match(T__3);
				}
				}
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(122);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvpParameterContext extends ParserRuleContext {
		public TerminalNode PARAM_TYPE() { return getToken(SBWriteParser.PARAM_TYPE, 0); }
		public TerminalNode PARAM() { return getToken(SBWriteParser.PARAM, 0); }
		public SvpParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svpParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvpParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvpParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvpParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvpParameterContext svpParameter() throws RecognitionException {
		SvpParameterContext _localctx = new SvpParameterContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_svpParameter);
		try {
			setState(128);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PARAM_TYPE:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(124);
				match(PARAM_TYPE);
				setState(125);
				match(T__8);
				setState(126);
				match(PARAM);
				}
				}
				break;
			case PARAM:
				enterOuterAlt(_localctx, 2);
				{
				setState(127);
				match(PARAM);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvpParameterListContext extends ParserRuleContext {
		public List<SvpParameterContext> svpParameter() {
			return getRuleContexts(SvpParameterContext.class);
		}
		public SvpParameterContext svpParameter(int i) {
			return getRuleContext(SvpParameterContext.class,i);
		}
		public SvpParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svpParameterList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvpParameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvpParameterList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvpParameterList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvpParameterListContext svpParameterList() throws RecognitionException {
		SvpParameterListContext _localctx = new SvpParameterListContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_svpParameterList);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(130);
					svpParameter();
					setState(131);
					match(T__9);
					}
					} 
				}
				setState(137);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
			}
			setState(138);
			svpParameter();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvmParameterContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SBWriteParser.ID, 0); }
		public TerminalNode STATE() { return getToken(SBWriteParser.STATE, 0); }
		public SvmParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svmParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvmParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvmParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvmParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvmParameterContext svmParameter() throws RecognitionException {
		SvmParameterContext _localctx = new SvmParameterContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_svmParameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(ID);
			setState(142);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STATE) {
				{
				setState(141);
				match(STATE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvmParameterListContext extends ParserRuleContext {
		public List<SvmParameterContext> svmParameter() {
			return getRuleContexts(SvmParameterContext.class);
		}
		public SvmParameterContext svmParameter(int i) {
			return getRuleContext(SvmParameterContext.class,i);
		}
		public SvmParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svmParameterList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvmParameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvmParameterList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvmParameterList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvmParameterListContext svmParameterList() throws RecognitionException {
		SvmParameterListContext _localctx = new SvmParameterListContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_svmParameterList);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(144);
					svmParameter();
					setState(145);
					match(T__9);
					}
					} 
				}
				setState(151);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			setState(152);
			svmParameter();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SvpwriteContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SBWriteParser.EOF, 0); }
		public List<SvpDefContext> svpDef() {
			return getRuleContexts(SvpDefContext.class);
		}
		public SvpDefContext svpDef(int i) {
			return getRuleContext(SvpDefContext.class,i);
		}
		public List<SvmDefContext> svmDef() {
			return getRuleContexts(SvmDefContext.class);
		}
		public SvmDefContext svmDef(int i) {
			return getRuleContext(SvmDefContext.class,i);
		}
		public List<InstanceListContext> instanceList() {
			return getRuleContexts(InstanceListContext.class);
		}
		public InstanceListContext instanceList(int i) {
			return getRuleContext(InstanceListContext.class,i);
		}
		public SvpwriteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_svpwrite; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).enterSvpwrite(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SBWriteListener ) ((SBWriteListener)listener).exitSvpwrite(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SBWriteVisitor ) return ((SBWriteVisitor<? extends T>)visitor).visitSvpwrite(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SvpwriteContext svpwrite() throws RecognitionException {
		SvpwriteContext _localctx = new SvpwriteContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_svpwrite);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(157);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
				case 1:
					{
					setState(154);
					svpDef();
					}
					break;
				case 2:
					{
					setState(155);
					svmDef();
					}
					break;
				case 3:
					{
					setState(156);
					instanceList();
					}
					break;
				}
				}
				setState(159); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__7) | (1L << ANNOTATION) | (1L << TYPE) | (1L << ID))) != 0) );
			setState(161);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\24\u00a6\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\5\2\37\n\2\3\2\3\2\3\2\3\2\3\2"+
		"\5\2&\n\2\3\3\3\3\3\3\5\3+\n\3\3\3\3\3\3\4\3\4\5\4\61\n\4\3\5\3\5\5\5"+
		"\65\n\5\3\6\7\68\n\6\f\6\16\6;\13\6\3\6\3\6\3\6\3\6\3\6\5\6B\n\6\3\6\5"+
		"\6E\n\6\3\7\3\7\3\7\5\7J\n\7\3\7\3\7\6\7N\n\7\r\7\16\7O\3\b\7\bS\n\b\f"+
		"\b\16\bV\13\b\3\b\3\b\3\b\3\b\5\b\\\n\b\3\b\3\b\3\b\5\ba\n\b\3\b\3\b\3"+
		"\t\7\tf\n\t\f\t\16\ti\13\t\3\t\3\t\3\t\3\t\3\t\3\t\5\tq\n\t\3\t\3\t\3"+
		"\t\3\t\3\t\7\tx\n\t\f\t\16\t{\13\t\3\t\3\t\3\n\3\n\3\n\3\n\5\n\u0083\n"+
		"\n\3\13\3\13\3\13\7\13\u0088\n\13\f\13\16\13\u008b\13\13\3\13\3\13\3\f"+
		"\3\f\5\f\u0091\n\f\3\r\3\r\3\r\7\r\u0096\n\r\f\r\16\r\u0099\13\r\3\r\3"+
		"\r\3\16\3\16\3\16\6\16\u00a0\n\16\r\16\16\16\u00a1\3\16\3\16\3\16\2\2"+
		"\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2\2\u00b0\2\36\3\2\2\2\4\'\3\2"+
		"\2\2\6.\3\2\2\2\b\62\3\2\2\2\n9\3\2\2\2\fM\3\2\2\2\16T\3\2\2\2\20g\3\2"+
		"\2\2\22\u0082\3\2\2\2\24\u0089\3\2\2\2\26\u008e\3\2\2\2\30\u0097\3\2\2"+
		"\2\32\u009f\3\2\2\2\34\35\7\24\2\2\35\37\7\3\2\2\36\34\3\2\2\2\36\37\3"+
		"\2\2\2\37 \3\2\2\2 %\7\22\2\2!\"\7\4\2\2\"#\5\24\13\2#$\7\5\2\2$&\3\2"+
		"\2\2%!\3\2\2\2%&\3\2\2\2&\3\3\2\2\2\'(\7\24\2\2(*\7\4\2\2)+\5\30\r\2*"+
		")\3\2\2\2*+\3\2\2\2+,\3\2\2\2,-\7\5\2\2-\5\3\2\2\2.\60\7\24\2\2/\61\7"+
		"\23\2\2\60/\3\2\2\2\60\61\3\2\2\2\61\7\3\2\2\2\62\64\7\24\2\2\63\65\7"+
		"\23\2\2\64\63\3\2\2\2\64\65\3\2\2\2\65\t\3\2\2\2\668\7\16\2\2\67\66\3"+
		"\2\2\28;\3\2\2\29\67\3\2\2\29:\3\2\2\2:<\3\2\2\2;9\3\2\2\2<=\5\6\4\2="+
		">\7\17\2\2>D\5\b\5\2?A\7\4\2\2@B\5\24\13\2A@\3\2\2\2AB\3\2\2\2BC\3\2\2"+
		"\2CE\7\5\2\2D?\3\2\2\2DE\3\2\2\2E\13\3\2\2\2FJ\5\2\2\2GJ\5\4\3\2HJ\5\n"+
		"\6\2IF\3\2\2\2IG\3\2\2\2IH\3\2\2\2JK\3\2\2\2KL\7\6\2\2LN\3\2\2\2MI\3\2"+
		"\2\2NO\3\2\2\2OM\3\2\2\2OP\3\2\2\2P\r\3\2\2\2QS\7\16\2\2RQ\3\2\2\2SV\3"+
		"\2\2\2TR\3\2\2\2TU\3\2\2\2UW\3\2\2\2VT\3\2\2\2WX\7\7\2\2XY\7\24\2\2Y["+
		"\7\4\2\2Z\\\5\30\r\2[Z\3\2\2\2[\\\3\2\2\2\\]\3\2\2\2]^\7\5\2\2^`\7\b\2"+
		"\2_a\5\f\7\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2\2bc\7\t\2\2c\17\3\2\2\2df\7\16"+
		"\2\2ed\3\2\2\2fi\3\2\2\2ge\3\2\2\2gh\3\2\2\2hj\3\2\2\2ig\3\2\2\2jk\7\n"+
		"\2\2kl\7\24\2\2lm\7\3\2\2mn\7\22\2\2np\7\4\2\2oq\5\24\13\2po\3\2\2\2p"+
		"q\3\2\2\2qr\3\2\2\2rs\7\5\2\2sy\7\b\2\2tu\5\n\6\2uv\7\6\2\2vx\3\2\2\2"+
		"wt\3\2\2\2x{\3\2\2\2yw\3\2\2\2yz\3\2\2\2z|\3\2\2\2{y\3\2\2\2|}\7\t\2\2"+
		"}\21\3\2\2\2~\177\7\20\2\2\177\u0080\7\13\2\2\u0080\u0083\7\21\2\2\u0081"+
		"\u0083\7\21\2\2\u0082~\3\2\2\2\u0082\u0081\3\2\2\2\u0083\23\3\2\2\2\u0084"+
		"\u0085\5\22\n\2\u0085\u0086\7\f\2\2\u0086\u0088\3\2\2\2\u0087\u0084\3"+
		"\2\2\2\u0088\u008b\3\2\2\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a"+
		"\u008c\3\2\2\2\u008b\u0089\3\2\2\2\u008c\u008d\5\22\n\2\u008d\25\3\2\2"+
		"\2\u008e\u0090\7\24\2\2\u008f\u0091\7\23\2\2\u0090\u008f\3\2\2\2\u0090"+
		"\u0091\3\2\2\2\u0091\27\3\2\2\2\u0092\u0093\5\26\f\2\u0093\u0094\7\f\2"+
		"\2\u0094\u0096\3\2\2\2\u0095\u0092\3\2\2\2\u0096\u0099\3\2\2\2\u0097\u0095"+
		"\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009a\3\2\2\2\u0099\u0097\3\2\2\2\u009a"+
		"\u009b\5\26\f\2\u009b\31\3\2\2\2\u009c\u00a0\5\20\t\2\u009d\u00a0\5\16"+
		"\b\2\u009e\u00a0\5\f\7\2\u009f\u009c\3\2\2\2\u009f\u009d\3\2\2\2\u009f"+
		"\u009e\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3\2"+
		"\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4\7\2\2\3\u00a4\33\3\2\2\2\30\36%*"+
		"\60\649ADIOT[`gpy\u0082\u0089\u0090\u0097\u009f\u00a1";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}