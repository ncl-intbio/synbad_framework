/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.traversal;

/**
 *
 * @author owengilfellon
 */
public interface GRNTreeIterator  {
    
    public int getDepth();
    
}
