grammar SBWrite;

fragment LOWERCASE: [a-z];
fragment UPPERCASE: [A-Z];
fragment DIGITS: [0-9];
fragment SPECIALCHARACTERS: ('/' | '~' | '.');

fragment WHITESPACE: (' ' | '\t' | '\r' | '\n');
fragment COMMENT: '#' (~('\r' | '\n'))*;
IGNORE: (WHITESPACE | COMMENT) -> skip;

ANNOTATION: '@' (~('\r' | '\n'))*;

fragment REPRESS: '-|';
fragment ACTIVATE: '->';
fragment PHOSPHORYLATE: '-*';
fragment COMPLEX: '-%';
INTERACTION_OPERATOR: (REPRESS | ACTIVATE | PHOSPHORYLATE | COMPLEX);

PARAM_TYPE :  'ktr' | 'ktl' | 'kd' | 'Km' | 'kf' | 'kdeP';
PARAM: DIGITS+('.'DIGITS+)?;

TYPE: 'Prom' | 'RBS' | 'CDS' | 'Ter' | 'Shim' | 'Op' ;
STATE: '~' UPPERCASE;

ID : (LOWERCASE | UPPERCASE | DIGITS)+ (LOWERCASE | UPPERCASE | DIGITS | '_')* ;

svp: (ID  ':')? TYPE ('(' svpParameterList ')')?;
svm : ID  '(' svmParameterList? ')';

leftParticipant: ID STATE?;
rightParticipant: ID STATE?;

interaction: ANNOTATION* leftParticipant INTERACTION_OPERATOR rightParticipant  ('(' svpParameterList? ')')?;

instanceList : (((svp | svm | interaction ) ';') )+;
svmDef : ANNOTATION* 'module'  ID  '(' svmParameterList?  ')'  '{' instanceList? '}';
svpDef : ANNOTATION* 'part'  ID ':' TYPE '(' svpParameterList?  ')'  '{' (interaction ';')* '}';

svpParameter: (PARAM_TYPE '=' PARAM) | PARAM;
svpParameterList: (svpParameter ',' )* svpParameter;
svmParameter: ID STATE?;
svmParameterList: (svmParameter ',' )* svmParameter;

// Interactions

// An SVP list is one or more SVPs or SVPs with optional whitespace

svpwrite: (svpDef | svmDef | instanceList )+ EOF;