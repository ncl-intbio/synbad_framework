package uk.ac.ncl.icos.grntree.tests;

import org.junit.*;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.grntree.language.Compiler;
import uk.ac.ncl.icos.grntree.language.Compiler2;
import uk.ac.ncl.icos.grntree.language.SvpWriteExporter;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpcompiler.Compiler.SBMLCompiler;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class SBWriteTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(SBWriteTests.class);

    private AbstractSVPManager ABSTRACT_MANAGER = AbstractSVPManager.getSVPManager();


    // ===============================================
    // Create Parts, providing a Part ID
    // ===============================================

    private Part a = ABSTRACT_MANAGER.getConstPromoterPart("promA");
    private Part b = ABSTRACT_MANAGER.getNegativeOperator("opB");
    private Part c = ABSTRACT_MANAGER.getRBS("rbsC");
    private Part d = ABSTRACT_MANAGER.getCDS("cdsD");

    private Part e = ABSTRACT_MANAGER.getConstPromoterPart("promE");
    private Part f = ABSTRACT_MANAGER.getNegativeOperator("opF");
    private Part g = ABSTRACT_MANAGER.getRBS("rbsG");
    private Part h = ABSTRACT_MANAGER.getCDS("cdsH");

    private Part i = ABSTRACT_MANAGER.getConstPromoterPart("promI");
    private Part j = ABSTRACT_MANAGER.getNegativeOperator("opJ");
    private Part k = ABSTRACT_MANAGER.getRBS("rbsK");
    private Part l = ABSTRACT_MANAGER.getCDS("cdsL");

    // ===============================================
    // Create Internal Interactions, passing parts
    // and parameters, if needed.
    // ===============================================

    private List<Interaction> a_i = ABSTRACT_MANAGER.getPoPSProduction(a, 0.0269);
    private List<Interaction> e_i = ABSTRACT_MANAGER.getPoPSProduction(e, 0.03242);
    private List<Interaction> i_i = ABSTRACT_MANAGER.getPoPSProduction(i, 0.02513);

    private List<Interaction> b_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(b);
    private List<Interaction> f_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(f);
    private List<Interaction> j_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(j);

    private List<Interaction> c_i = ABSTRACT_MANAGER.getRiPSProduction(c, 0.34255);
    private List<Interaction> g_i = ABSTRACT_MANAGER.getRiPSProduction(g, 0.51213);
    private List<Interaction> k_i = ABSTRACT_MANAGER.getRiPSProduction(k, 0.4685);

    private List<Interaction> d_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(d, 0.01412);
    private List<Interaction> h_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(h, 0.02134);
    private List<Interaction> l_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(l, 0.03214);

    // ===============================================
    // Create Part-Part Interactions
    // ===============================================

    private Interaction dRepressesf = ABSTRACT_MANAGER.getRegulationInteraction(   f, d,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.3456);

    private Interaction hRepressesj = ABSTRACT_MANAGER.getRegulationInteraction(   j, h,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.4567);

    private Interaction lRepressesb = ABSTRACT_MANAGER.getRegulationInteraction(   b, l,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.3754);

    @BeforeClass
    public static void setUpClass() { }

    @AfterClass
    public static void tearDownClass() { }

    @Before
    public void setUp() { }

    @After
    public void tearDown() { }


    @Test
    public void  testSimpleSBWrite() throws Exception
    {
        Compiler2 c = new Compiler2();
        GRNTree tree = c.compile( "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter;" );
        SvpWriteExporter exporter = new SvpWriteExporter(true);
        String exported = exporter.write(tree);
        LOGGER.debug(exported);
    }

    @Test
    public void  testImportRepressilator() throws Exception
    {
        Compiler2 c = new Compiler2();
        GRNTree tree = c.compile( new FileInputStream("testDeviceRepressilator.sb"));
        SvpWriteExporter exporter = new SvpWriteExporter(true);
        String exported = exporter.write(tree);
        LOGGER.debug(exported);
    }


    @Test
    public void  testSBWrite() throws Exception
    {
        Compiler2 c = new Compiler2();
        GRNTree tree = c.compile( new FileInputStream("testDevice.sb"));
        SvpWriteExporter exporter = new SvpWriteExporter(true);
        String exported = exporter.write(tree);
        LOGGER.debug(exported);
        GRNTree tree2 = new Compiler2().compile(exported);
        compareSimulations(tree, tree2, "B", "C", "E", "F", "EPhosphorylated");
    }

    @Test
    public void  testSBWriteExporter()
    {
        GRNTree tree = GRNTreeFactory.getGRNTree("PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; "
                + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        SvpWriteExporter exporter = new SvpWriteExporter();
        Compiler2 c = new Compiler2();
        GRNTree tree2 = c.compile( new ByteArrayInputStream(exporter.write(tree).getBytes()));
        compareSimulations(tree, tree2, "SpaR", "SpaK", "GFP_rrnb");
    }

    @Test
    public void testGrnTreeConstructionRepressilator() throws Exception
    {
        GRNTree tree = GRNTreeFactory.getGRNTree();

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(a, a_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(b, b_i, InterfaceType.INPUT),
                        GRNTreeNodeFactory.getLeafNode(c, c_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(d, d_i, InterfaceType.OUTPUT))));

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(e, e_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(f, f_i, InterfaceType.INPUT),
                        GRNTreeNodeFactory.getLeafNode(g, g_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(h, h_i, InterfaceType.OUTPUT))));

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(i, i_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(j, j_i, InterfaceType.INPUT),
                        GRNTreeNodeFactory.getLeafNode(k, k_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(l, l_i, InterfaceType.OUTPUT))));

        tree.addInteraction(dRepressesf);
        tree.addInteraction(hRepressesj);
        tree.addInteraction(lRepressesb);

        SvpWriteExporter exporter = new SvpWriteExporter();
        Compiler2 compiler = new Compiler2();
        GRNTree tree2 = compiler.compile( new ByteArrayInputStream(exporter.write(tree).getBytes()));
        compareSimulations(tree, tree2, d.getName(), h.getName(), l.getName());
    }

    private void compareSimulations(GRNTree tree1, GRNTree tree2, String... keys) {

        SimpleCopasiSimulator simulator = new SimpleCopasiSimulator(12800, 1000);

        SBMLCompiler compiler = new SBMLCompiler("test");

        compiler.setParts(GRNCompilableFactory.getCompilables(tree1));
        compiler.compileAll();
        simulator.setModel(compiler.getModelString());
        simulator.addInitialConcentration("x", 100.0);
        simulator.run();

        TimeCourseTraces allTraces1 = simulator.getResults();

        // TODO: errors if the same compiler is reused

        compiler = new SBMLCompiler("test");

        compiler.setParts(GRNCompilableFactory.getCompilables(tree2));
        compiler.compileAll();
        simulator.setModel(compiler.getModelString());
        simulator.addInitialConcentration("x", 100.0);
        simulator.run();

        TimeCourseTraces allTraces2 = simulator.getResults();

        Arrays.stream(keys).forEach( species -> {
                    TimeCourseTrace trace1 = allTraces1.getTimeCourse(species);
                    TimeCourseTrace trace2 = allTraces2.getTimeCourse(species);

                    Assert.assertEquals("Trace for " + species + " should be identical",
                            trace1.get(trace1.size() - 1),
                            trace2.get(trace2.size() - 1));
                }
        );
    }
}
