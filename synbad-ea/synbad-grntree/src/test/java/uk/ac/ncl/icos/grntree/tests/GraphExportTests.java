/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.tests;

import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphMapping;
import org.jgrapht.alg.isomorphism.IsomorphismInspector;
import org.jgrapht.alg.isomorphism.VF2GraphIsomorphismInspector;
import org.jgrapht.alg.isomorphism.VF2SubgraphIsomorphismInspector;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.io.GeneNetworkSerialiser;
import uk.ac.ncl.icos.grntree.io.GeneNetworkSerialiser.ExportEdgeWrapper;
import uk.ac.ncl.icos.grntree.io.GeneNetworkSerialiser.ExportNodeWrapper;
import uk.ac.ncl.icos.grntree.io.SBMLSerialiser;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import javax.xml.transform.TransformerConfigurationException;

/**
 *
 * @author owengilfellon
 */
public class   GraphExportTests {

    private final GeneNetworkSerialiser serialiser = new GeneNetworkSerialiser();
    private final GraphMLExporter<ExportNodeWrapper, ExportEdgeWrapper> exporter = new GraphMLExporter<>();

    private void  exportToSystemOut(DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> g) {
        try {
            exporter.export(new OutputStreamWriter(System.out), g);
        } catch (SAXException | TransformerConfigurationException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testStandardSubtilinReceiver()
    {
        String model1 = "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_4296:Ter; " +
               "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter";

        GRNTree tree = GRNTreeFactory.getGRNTree(model1);
        tree.setDetectInteractions(true);

        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> g = serialiser.exportNetwork(tree);

        Assert.assertEquals("There should be two vertices", 2, g.vertexSet().size());
        Assert.assertEquals("There should be one edge", 1, g.edgeSet().size());

       // exportToSystemOut(g);
    }

    @Test
    public void testDoublePspasSubtilinReceiver()
    {
        String model2 = "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_4296:Ter; " +
                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter";

        GRNTree tree2 = GRNTreeFactory.getGRNTree(model2);
        tree2.setDetectInteractions(true);

        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> g = serialiser.exportNetwork(tree2);

        Assert.assertEquals("There should be two vertices", 2, g.vertexSet().size());
        Assert.assertEquals("There should be two edges", 2, g.edgeSet().size());

       // exportToSystemOut(g);
    }

    //@Test
    public void testEvolvedSubtilinReceiver()
    {
        String model3 = "BO_2917:Prom; BO_28510:RBS; SpaK:CDS; BO_27993:RBS; BO_32147:CDS; BO_27893:RBS; BO_32743:CDS; " +
                "BO_4973:Ter; PspaS:Prom; BO_4227:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; " +
                "BO_5427:Ter; BO_2939:Prom; BO_3583:Op; BO_28333:RBS; SpaR:CDS; BO_4775:Ter; PspaS:Prom; BO_3789:Op; " +
                "BO_28475:RBS; SpaK:CDS; BO_28017:RBS; BO_32147:CDS; BO_5498:Ter; BO_3143:Prom; BO_28493:RBS; " +
                "BO_28950:CDS; BO_28224:RBS; BO_32997:CDS; BO_6476:Ter; BO_27718:Prom; BO_28220:RBS; BO_31762:CDS; " +
                "BO_28275:RBS; BO_31001:CDS; BO_4813:Ter; BO_3403:Prom; BO_28182:RBS; BO_32601:CDS; BO_28486:RBS; " +
                "BO_32601:CDS; BO_28234:RBS; BO_32227:CDS; BO_28270:RBS; BO_30746:CDS; BO_5738:Ter; PspaS:Prom; " +
                "BO_3600:Op; BO_3906:Op; BO_28092:RBS; BO_32307:CDS; BO_28142:RBS; BO_32307:CDS; BO_28096:RBS; " +
                "BO_28950:CDS; BO_28317:RBS; BO_32731:CDS; BO_28152:RBS; BO_28950:CDS; BO_5427:Ter; BO_27782:Prom; " +
                "BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; SpaK:CDS; BO_28518:RBS; BO_30680:CDS; BO_5205:Ter; " +
                "BO_27780:Prom; BO_3596:Op; BO_27809:RBS; BO_28831:CDS; BO_28322:RBS; SpaR:CDS; BO_5139:Ter; " +
                "PspaS:Prom; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27809:RBS; BO_28831:CDS; " +
                "BO_27910:RBS; BO_31762:CDS; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_5427:Ter; BO_2937:Prom; BO_28152:RBS; " +
                "BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_3906:Op; BO_28260:RBS; BO_31762:CDS; BO_5483:Ter; PspaS:Prom; " +
                "BO_3559:Op; BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28144:RBS; BO_32307:CDS; BO_28326:RBS; " +
                "BO_32307:CDS; BO_28157:RBS; BO_28950:CDS; BO_28182:RBS; BO_32601:CDS; BO_5427:Ter; BO_3388:Prom; " +
                "RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28185:RBS; BO_32743:CDS; BO_4296:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; " +
                "BO_32307:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_28082:RBS; BO_32307:CDS; " +
                "BO_28096:RBS; BO_28950:CDS; BO_27910:RBS; BO_31762:CDS; BO_5427:Ter; PspaS:Prom; BO_3906:Op; BO_28326:RBS; " +
                "BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; " +
                "BO_28152:RBS; BO_28950:CDS; BO_27836:RBS; BO_28950:CDS; BO_6420:Ter; PspaS:Prom; BO_4190:Op; BO_3600:Op; " +
                "BO_3906:Op; BO_27847:RBS; BO_32307:CDS; BO_28326:RBS; BO_32307:CDS; BO_27903:RBS; BO_32307:CDS; " +
                "BO_28096:RBS; BO_28950:CDS; BO_28051:RBS; BO_32731:CDS; BO_28391:RBS; BO_30753:CDS; BO_5427:Ter; " +
                "PspaS:Prom; BO_3906:Op; BO_28134:RBS; BO_32307:CDS; BO_28326:RBS; BO_28892:CDS; BO_5427:Ter; " +
                "BO_27782:Prom; BO_3711:Op; BO_28326:RBS; BO_32307:CDS; BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; " +
                "GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_28326:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; " +
                "GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; PspaS:Prom; BO_3606:Op; " +
                "BO_3906:Op; BO_28326:RBS; BO_32307:CDS; BO_28319:RBS; BO_31337:CDS; BO_27900:RBS; BO_32307:CDS; " +
                "BO_5427:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28144:RBS; BO_32307:CDS; BO_4296:Ter; PspaS:Prom; " +
                "RBS_SpaK:RBS; GFP_rrnb:CDS; BO_4296:Ter; BO_27782:Prom; BO_28152:RBS; BO_32633:CDS; BO_28298:RBS; " +
                "SpaK:CDS; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter; BO_3387:Prom; BO_28096:RBS; " +
                "BO_32997:CDS; BO_4583:Ter; PspaS:Prom; RBS_SpaK:RBS; GFP_rrnb:CDS; BO_28144:RBS; BO_32307:CDS; " +
                "BO_4296:Ter; BO_27629:Prom; BO_28287:RBS; SpaR:CDS; BO_28249:RBS; BO_32653:CDS; BO_6472:Ter";

        GRNTree tree3 = GRNTreeFactory.getGRNTree(model3);
        tree3.setDetectInteractions(true);

        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> g = serialiser.exportNetwork(tree3);
        exportToSystemOut(g);
    }

    @Test
    public void testGraphLibraryIsomorphismAlgorithm()
    {
        String model1 = "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_4296:Ter; " +
                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter";

        String model2 = "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_4296:Ter; " +
                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter";

        GRNTree tree = GRNTreeFactory.getGRNTree(model1);
        GRNTree tree2 = GRNTreeFactory.getGRNTree(model2);

        tree.setDetectInteractions(true);
        tree2.setDetectInteractions(true);

        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> g = serialiser.exportNetwork(tree);
        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> g2 = serialiser.exportNetwork(tree2);

        IsomorphismInspector<ExportNodeWrapper, ExportEdgeWrapper> iso = new VF2SubgraphIsomorphismInspector<>(g2, g);


        if(iso.isomorphismExists()) {
            Iterator<GraphMapping<ExportNodeWrapper, ExportEdgeWrapper>> it = iso.getMappings();

            int vertexMatches = 0;
            int edgeMatches = 0;

            while(it.hasNext()) {
                GraphMapping<ExportNodeWrapper, ExportEdgeWrapper> mapping = it.next();

                vertexMatches += g2.vertexSet().stream().map(v -> mapping.getVertexCorrespondence(v, true))
                        .filter(v -> v!=null).count();
                edgeMatches += g2.edgeSet().stream().map(e -> mapping.getEdgeCorrespondence(e, true))
                        .filter(e -> e!=null).count();
            }

            // System.out.println("V: " + vertexMatches + " E: " + edgeMatches);
        }
    }
}
