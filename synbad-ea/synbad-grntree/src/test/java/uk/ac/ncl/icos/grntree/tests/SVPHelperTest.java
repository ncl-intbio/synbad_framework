package uk.ac.ncl.icos.grntree.tests;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.PrototypeNode;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 * Created by owengilfellon on 19/01/2015.
 */
public class SVPHelperTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SVPHelperTest.class);
    private static final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private static final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();

    @Test
    public void createTwoComponentSystem() throws Exception {

        GRNTree tree = GRNTreeFactory.getGRNTree();

        GRNTreeNode tu1 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);

        tree.getRootNode().addNode(tu1);
        tree.getRootNode().addNode(tu2);

        // Create Parts

        GRNTreeNode donor = SVP_HELPER.getCDSEnvConst("CdsC", "smlMol", 0.0012, 0.00000532, 0.012, 0.012);
        GRNTreeNode acceptor = SVP_HELPER.getCDSWithPhosphorylated("CdsE", 0.0012, 0.1, 0.012);
        GRNTreeNode promoter = SVP_HELPER.getInducPromoter("PromG", 0.0023);

        List<GRNTreeNode> nodes = new ArrayList<>();
        nodes.add(SVP_HELPER.getConstPromoter("PromA", 0.00238));
        nodes.add(SVP_HELPER.getRBS("RbsB", 0.314));
        nodes.add(donor);
        nodes.add(SVP_HELPER.getRBS("RbsD", 0.523));
        nodes.add(acceptor);
        nodes.add(SVP_HELPER.getTerminator("TerF"));

        List<GRNTreeNode> nodes2 = new ArrayList<>();
        nodes2.add(promoter);
        nodes2.add(SVP_HELPER.getRBS("RbsH", 0.495));
        nodes2.add(SVP_HELPER.getCDS("CdsI", 0.0012));
        nodes2.add(SVP_HELPER.getTerminator("TerJ"));
        
        // Add Parts to TUs

        tu1.setNodes(nodes);
        tu2.setNodes(nodes2);

        // Add promoter regulation

        tree.addInteraction(SVP_MANAGER.getPhosphorylationInteraction(donor.getSVP(), acceptor.getSVP(), 0.0000043));
        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(promoter.getSVP(), acceptor.getSVP(),
                MolecularForm.PHOSPHORYLATED, RegulationRole.ACTIVATOR, 0.042));

        // Compile tree to SBML

        CompilationDirector director = new CompilationDirector(SVPManager.getRepositoryUrl());
        director.setModelCompilables(GRNCompilableFactory.getCompilables(tree));
        writeToXml(director.getSBMLString("twocomponentsystem"), "twocomponentsystem");

//        tree.getPreOrderIterator().forEachRemaining(x -> {
//            if(x.isLeafNode()) {
//               // System.out.println("--- " + x.getName() + " ---");
//                ((LeafNode)x).getParameters().stream().forEach(System.out::println);
//            }
//        });
    }

    @Test
    public void createRepressilator() throws Exception {

        GRNTree tree = GRNTreeFactory.getGRNTree();

        GRNTreeNode tu1 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu3 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);

        tree.getRootNode().addNode(tu1);
        tree.getRootNode().addNode(tu2);
        tree.getRootNode().addNode(tu3);

        GRNTreeNode operator1 = SVP_HELPER.getOperator("opB");
        GRNTreeNode operator2 = SVP_HELPER.getOperator("opF");
        GRNTreeNode operator3 = SVP_HELPER.getOperator("opJ");

        GRNTreeNode cds1 = SVP_HELPER.getCDS("cdsD", 0.0012);
        GRNTreeNode cds2 = SVP_HELPER.getCDS("cdsH", 0.0012);
        GRNTreeNode cds3 = SVP_HELPER.getCDS("cdsL", 0.0012);

        // Add Parts to TUs

        tu1.setNodes(Arrays.asList(
            SVP_HELPER.getConstPromoter("promA", 0.00238),
            operator1,
            SVP_HELPER.getRBS("rbsC", 0.314),
            cds1,
            GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu2.setNodes(Arrays.asList(
            SVP_HELPER.getConstPromoter("promE", 0.00238),
            operator2,
            SVP_HELPER.getRBS("rbsG", 0.314),
            cds2,
            GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu3.setNodes(Arrays.asList(
            SVP_HELPER.getConstPromoter("promI", 0.00238),
            operator3,
            SVP_HELPER.getRBS("rbsK", 0.314),
            cds3,
            GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        // Add promoter regulation

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator2.getSVP(), cds1.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 0.042));

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator3.getSVP(), cds2.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 0.042));

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator1.getSVP(),cds3.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 0.042));

        // Compile tree to SBML

        CompilationDirector director = new CompilationDirector(SVPManager.getRepositoryUrl());
        director.setModelCompilables(GRNCompilableFactory.getCompilables(tree));
        writeToXml(director.getSBMLString("repressilator"), "repressilator");

        /*tree.getPreOrderIterator().forEachRemaining(x -> {
            if(x.isLeafNode()) {
                System.out.println("--- " + x.getName() + " ---");
                ((LeafNode)x).getParameters().stream().forEach(System.out::println);
            }
        });*/
    }

    public static void writeToXml(String sbml, String filename) {

        try
        {
            File f = new File("exports");
            if(!f.exists())
                f.mkdir();

            FileWriter writer = new FileWriter("exports/" + filename + ".xml");
            writer.write(sbml);
            writer.flush();
        }
        catch(Exception ex)
        {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }



}
