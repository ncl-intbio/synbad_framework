package uk.ac.ncl.icos.grntree.tests;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

/**
 * Created by owengilfellon on 28/02/2014.
 */
public class EqualityTests {
    
    private static Logger LOGGER = LoggerFactory.getLogger(EqualityTests.class);

    //@Test
    public void testEquality() {
        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
                        "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
                        "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter");

        Assert.assertEquals(
                "First leaf nodes in first two branch nodes should be identical",
                tree.getRootNode().getChildren().get(0).getChildren().get(0),
                tree.getRootNode().getChildren().get(1).getChildren().get(0));

        Assert.assertNotEquals(
                "PSpaS in TU 1 and RBS_SpaR in TU 2 should not be identical",
                tree.getRootNode().getChildren().get(0).getChildren().get(0),
                tree.getRootNode().getChildren().get(1).getChildren().get(1));


        Assert.assertEquals(
                "First two branch nodes should be identical",
                tree.getRootNode().getChildren().get(0),
                tree.getRootNode().getChildren().get(1));

        Assert.assertNotEquals(
                "Branch nodes one and three should not be identical",
                tree.getRootNode().getChildren().get(0),
                tree.getRootNode().getChildren().get(2));

        GRNTree tree2 = tree.duplicate();

        Assert.assertEquals(
                "Root nodes of duplicate tree should be identical",
                tree.getRootNode(),
                tree2.getRootNode());

        tree2.getRootNode().removeNode(tree2.getRootNode().getChildren().get(1));

        Assert.assertEquals(
                "Root nodes of modified duplicate tree should not be identical",
                tree.getRootNode(),
                tree2.getRootNode());
    }
}
