/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.grntree.tests;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.io.SBMLSerialiser;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;

/**
 *
 * @author owengilfellon
 */
public class SubtilinReceiverConstructsTests {
    
    private static Logger LOGGER = LoggerFactory.getLogger(SubtilinReceiverConstructsTests.class);
    private SVPManager m = SVPManager.getSVPManager();

   // @Test
    public void createSubtilinReceiverWtParts() throws Exception {
        List<GRNTreeNode> subtilinReceiverWTparts = new ArrayList<>();
        
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PolyA_Tail"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("EcoRI_rs"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("NotI_rs_shim"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PspaRK_shim"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PacI_rs"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaK_shim"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("SpaRK"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("taa_codon"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnB_T1_ter"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnO_ter"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnO_shim"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PspaS_core"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaS"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("Shim_RBS_ds"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("met_codon"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("GFP_rrnb"), InterfaceType.NONE));
        subtilinReceiverWTparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PolyA_Tail_5bp"), InterfaceType.NONE));
        
        GRNTree tree = GRNTreeFactory.getGRNTree();
        tree.setDetectInteractions(true);
        tree.getRootNode().setNodes(subtilinReceiverWTparts);
       // tree.debugInfo();
        
        SBMLDocument wtpartsmodel = new SBMLSerialiser().exportNetwork(tree);
        SimpleCopasiSimulator simulator = new SimpleCopasiSimulator(12800, 640);
        simulator.setModel(new SBMLHandler().GetSBML(wtpartsmodel));
    }

   // @Test
    public void createSubtilinReceiverPspaSParts() throws Exception {
        List<GRNTreeNode> subtilinReceiver_pspasparts = new ArrayList<>();

        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("EcoRI_rs_9bp"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("NotI_rs_shim"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PspaS_1"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PacI_rs"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaK_shim"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("SpaRK"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("taa_codon"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnB_T1_ter"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnO_ter"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnO_shim"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PspaS_core"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaS"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("Shim_RBS_ds"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("met_codon"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("GFP_rrnb"), InterfaceType.NONE));
        subtilinReceiver_pspasparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PolyA_Tail_5bp"), InterfaceType.NONE));

        GRNTree pspastree = GRNTreeFactory.getGRNTree();
        pspastree.setDetectInteractions(true);
        pspastree.getRootNode().setNodes(subtilinReceiver_pspasparts);
       // pspastree.debugInfo();

        SBMLDocument pspasmodel = new SBMLSerialiser().exportNetwork(pspastree);
        SimpleCopasiSimulator simulator = new SimpleCopasiSimulator(12800, 640);
        simulator.setModel(new SBMLHandler().GetSBML(pspasmodel));
    }

   // @Test
    public void createSubtilinReceiverPliaGParts() throws Exception {
        List<GRNTreeNode> subtilinReceiver_pliagparts = new ArrayList<>();

        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("EcoRI_rs_9bp"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("NotI_rs_shim"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PliaG"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PacI_rs"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaK_shim_18bp"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("SpaRK"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("taa_codon"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnB_T1_ter"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnO_ter"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("rrnO_shim"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PspaS_core"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaS"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("Shim_RBS_ds"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("met_codon"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("GFP_rrnb"), InterfaceType.NONE));
        subtilinReceiver_pliagparts.add(GRNTreeNodeFactory.getLeafNode(m.getPart("PolyA_Tail_5bp"), InterfaceType.NONE));

        GRNTree pliagtree = GRNTreeFactory.getGRNTree();
        pliagtree.setDetectInteractions(true);
        pliagtree.getRootNode().setNodes(subtilinReceiver_pliagparts);
        //LOGGER.debug(pliagtree.debugInfo());

        SBMLDocument pliagmodel = new SBMLSerialiser().exportNetwork(pliagtree);
        SimpleCopasiSimulator simulator = new SimpleCopasiSimulator(12800, 640);
        simulator.setModel(new SBMLHandler().GetSBML(pliagmodel));
    }
    
}
