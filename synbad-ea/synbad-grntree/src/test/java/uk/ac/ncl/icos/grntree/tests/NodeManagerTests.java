package uk.ac.ncl.icos.grntree.tests;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by owengilfellon on 19/01/2015.
 */
public class NodeManagerTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeManagerTests.class);
    private static final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private static final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();

    @Test
    public void addPrototypeInteractionAfterParts() throws Exception {
        NodeManager nodeManager = new NodeManager();
        GRNTree tree = new GRNTree(GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE), nodeManager);

        GRNTreeNode root =  tree.getRootNode();

        GRNTreeNode a = SVP_HELPER.getCDS("A", 0.0012);
        GRNTreeNode b = SVP_HELPER.getCDS("B", 0.0012);

        root.addNode(a);
        root.addNode(b);

        Interaction a_phos_b = SVP_MANAGER.getPhosphorylationInteraction(a.getSVP(), b.getSVP(), 0.00000521);

        tree.addInteraction( a_phos_b);

        Assert.assertTrue("A's interacting nodes should contain B", nodeManager.getInteractingNodes(a).contains(b));
        Assert.assertTrue("B's interacting nodes should contain A", nodeManager.getInteractingNodes(b).contains(a));

        Assert.assertEquals("Tree should have one instanced interaction", 1, nodeManager.getInstancedInteractions().size());
        Assert.assertEquals("Tree should have one interaction definition", 1, nodeManager.getAllInteractions().size());

        root.removeNode(a);

        Assert.assertFalse("A's interacting nodes should not contain B", nodeManager.getInteractingNodes(a).contains(b));
        Assert.assertFalse("B's interacting nodes should not contain A", nodeManager.getInteractingNodes(b).contains(a));

        Assert.assertEquals("Tree should have no instanced interactions", 0, nodeManager.getInstancedInteractions().size());
        Assert.assertEquals("Tree should have one interaction definition", 1, nodeManager.getAllInteractions().size());

    }


    @Test
    public void addPrototypeInteractionBeforeParts() throws Exception {
        NodeManager nodeManager = new NodeManager();
        GRNTree tree = new GRNTree(GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE), nodeManager);

        GRNTreeNode root =  tree.getRootNode();

        GRNTreeNode a = SVP_HELPER.getCDS("A", 0.0012);
        GRNTreeNode b = SVP_HELPER.getCDS("B", 0.0012);

        Interaction a_phos_b = SVP_MANAGER.getPhosphorylationInteraction(a.getSVP(), b.getSVP(), 0.00000521);

        tree.addInteraction( a_phos_b);

        root.addNode(a);
        root.addNode(b);

        Assert.assertTrue("A's interacting nodes should contain B", nodeManager.getInteractingNodes(a).contains(b));
        Assert.assertTrue("B's interacting nodes should contain A", nodeManager.getInteractingNodes(b).contains(a));

        Assert.assertEquals("Tree should have one instanced interaction", 1, nodeManager.getInstancedInteractions().size());
        Assert.assertEquals("Tree should have one interaction definition", 1, nodeManager.getAllInteractions().size());

        tree.removeInteraction(a_phos_b);

        Assert.assertFalse("A's interacting nodes should not contain B", nodeManager.getInteractingNodes(a).contains(b));
        Assert.assertFalse("B's interacting nodes should not contain A", nodeManager.getInteractingNodes(b).contains(a));

        Assert.assertEquals("Tree should have no instanced interactions", 0, nodeManager.getInstancedInteractions().size());
        Assert.assertEquals("Tree should have no interaction definitions", 0, nodeManager.getAllInteractions().size());


    }
}
