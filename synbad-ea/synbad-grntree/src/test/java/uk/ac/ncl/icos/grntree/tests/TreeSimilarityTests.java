package uk.ac.ncl.icos.grntree.tests;


import org.junit.*;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author b1050029
 */

public class TreeSimilarityTests {
    
    private PartsHandler        p;
    private SBMLDocument        container;
    private SBMLHandler         sbmlhandler;
    private String              server = SVPManager.getRepositoryUrl();

    private AbstractSVPManager  ABSTRACT_MANAGER = AbstractSVPManager.getSVPManager();
    private SVPManager          MANAGER = SVPManager.getSVPManager();

    private SVPHelper           HELPER = SVPHelper.getSVPHelper();


    private ModelBuilder        mb;
    private static final Logger logger = LoggerFactory.getLogger(TreeSimilarityTests.class);


    // ===============================================
    // Create Parts, providing a Part ID
    // ===============================================

    private Part a = ABSTRACT_MANAGER.getConstPromoterPart("promA");
    private Part b = ABSTRACT_MANAGER.getNegativeOperator("opB");
    private Part c = ABSTRACT_MANAGER.getRBS("rbsC");
    private Part d = ABSTRACT_MANAGER.getCDS("cdsD");

    private Part e = ABSTRACT_MANAGER.getConstPromoterPart("promE");
    private Part f = ABSTRACT_MANAGER.getNegativeOperator("opF");
    private Part g = ABSTRACT_MANAGER.getRBS("rbsG");
    private Part h = ABSTRACT_MANAGER.getCDS("cdsH");

    private Part i = ABSTRACT_MANAGER.getConstPromoterPart("promI");
    private Part j = ABSTRACT_MANAGER.getNegativeOperator("opJ");
    private Part k = ABSTRACT_MANAGER.getRBS("rbsK");
    private Part l = ABSTRACT_MANAGER.getCDS("cdsL");

    // ===============================================
    // Create Internal Interactions, passing parts
    // and parameters, if needed.
    // ===============================================

    private List<Interaction> a_i = ABSTRACT_MANAGER.getPoPSProduction(a, 0.0269);
    private List<Interaction> e_i = ABSTRACT_MANAGER.getPoPSProduction(e, 0.03242);
    private List<Interaction> i_i = ABSTRACT_MANAGER.getPoPSProduction(i, 0.02513);

    private List<Interaction> b_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(b);
    private List<Interaction> f_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(f);
    private List<Interaction> j_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(j);

    private List<Interaction> c_i = ABSTRACT_MANAGER.getRiPSProduction(c, 0.34255);
    private List<Interaction> g_i = ABSTRACT_MANAGER.getRiPSProduction(g, 0.51213);
    private List<Interaction> k_i = ABSTRACT_MANAGER.getRiPSProduction(k, 0.4685);

    private List<Interaction> d_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(d, 0.01412);
    private List<Interaction> h_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(h, 0.02134);
    private List<Interaction> l_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(l, 0.03214);

    // ===============================================
    // Create Part-Part Interactions
    // ===============================================

    private Interaction dRepressesf = ABSTRACT_MANAGER.getRegulationInteraction(   f, d,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.3456);

    private Interaction hRepressesj = ABSTRACT_MANAGER.getRegulationInteraction(   j, h,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.4567);

    private Interaction lRepressesb = ABSTRACT_MANAGER.getRegulationInteraction(   b, l,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.3754);

    @BeforeClass
    public static void setUpClass() { }

    @AfterClass
    public static void tearDownClass() { }

    @Before
    public void setUp() {
        p = new PartsHandler(server);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);
    }

    @After
    public void tearDown() { }

    private GRNTree constructTree() throws Exception {
        GRNTree tree = GRNTreeFactory.getGRNTree();

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(a, a_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(b, b_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(c, c_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(d, d_i, InterfaceType.BOTH))));

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(e, e_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(f, f_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(g, g_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(h, h_i, InterfaceType.BOTH))));

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(i, i_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(j, j_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(k, k_i, InterfaceType.NONE),
                        GRNTreeNodeFactory.getLeafNode(l, l_i, InterfaceType.BOTH))));

        tree.addInteraction(dRepressesf);
        tree.addInteraction(hRepressesj);
        tree.addInteraction(lRepressesb);
        return tree;
    }

    private void doTree(GRNTree tree) {
        int totalNodes = tree.getOutputNodes().size();
        double combined = 0;
        Map<String, Integer> individual = new HashMap<>();
        for(GRNTreeNode n : tree.getOutputNodes()) {
            FlowNavigator nav = tree.getFlowNavigator(n.getName());
            System.out.println(n.getName());
            int outputNodeUpstream = 0;
            while(nav.hasNext()) {
                System.out.println("            " + nav.next());
                outputNodeUpstream++;
            }
            outputNodeUpstream--;
            combined += outputNodeUpstream;
            individual.put(n.getName() + n.hashCode(), outputNodeUpstream);
        }
        combined = combined / (double)totalNodes;
        for(String key : individual.keySet()) {
            System.out.println(key + ": " + individual.get(key) + " AVG [ " + combined + " ]");
        }
    }

    @Test
    public void testGrnTreeConstructionRepressilator() throws Exception
    {
        GRNTree tree = constructTree();
        doTree(tree);
    }

    @Test
    public void testGrnTreeConstructionRepressilato23r() throws Exception
    {
        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; "
                        + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        doTree(tree);
    }

    @Test
    public void testGrnTreeConstructionRepressil2ato23r() throws Exception
    {
        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaS:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; "
                        + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        doTree(tree);
    }

    @Test
    public void testGrnTreeConstructionRepressilato2r() throws Exception
    {
        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; "
                + "PspaS:Prom; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; "
                + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        doTree(tree);
    }

    @Test
    public void testGrnTreeConstructionRepressadilato2r() throws Exception
    {
        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaS:Prom; RBS_SpaR:RBS; SpaR:CDS; BO_28140:RBS; SpaK:CDS; BO_27925:RBS; BO_31152:CDS; BO_4296:Ter; " +
                        "BO_27654:Prom; BO_4062:Op; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter;" +
                        "BO_3017:Prom; BO_27814:RBS; SpaR:CDS; BO_28246:RBS; BO_32147:CDS; BO_27875:RBS;" +
                        "BO_32147:CDS; BO_28522:RBS; BO_28831:CDS; BO_28458:RBS; BO_28831:CDS; BO_28458:RBS;" +
                        "BO_28831:CDS; BO_5248:Ter; BO_3475:Prom; BO_28458:RBS; BO_28831:CDS; BO_6486:Ter; BO_3403:Prom;" +
                        "BO_27793:RBS; SpaK:CDS; BO_5388:Ter; BO_3403:Prom; BO_28140:RBS; BO_32147:CDS; BO_5418:Ter");

        doTree(tree);
    }
}