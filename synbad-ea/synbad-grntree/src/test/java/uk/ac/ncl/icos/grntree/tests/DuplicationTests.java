package uk.ac.ncl.icos.grntree.tests;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.NodeManager;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by owengilfellon on 28/02/2014.
 */
public class DuplicationTests {
    
    private static Logger LOGGER = LoggerFactory.getLogger(DuplicationTests.class);
    private static final SVPManager m = SVPManager.getSVPManager();

    @Test
    public void testEquality() {
        GRNTree tree1 = GRNTreeFactory.getGRNTree(
                        "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter;" +
                        "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        GRNTree tree2 = tree1.duplicate();

        Iterator<GRNTreeNode> i1 = tree1.getPreOrderIterator();
        Iterator<GRNTreeNode> i2 = tree2.getPreOrderIterator();

        while(i1.hasNext() || i2.hasNext()) {
            Assert.assertEquals("Both iterators should have same state", i1.hasNext(), i2.hasNext());
            GRNTreeNode node1 = i1.next();
            GRNTreeNode node2 = i2.next();
            LOGGER.debug("Testing {} and {}", node1, node2);
            Assert.assertNotEquals("Nodes should be different objects", node1, node2);
        }
    }

    @Test
    public void testNodeManagerEquality() {

        NodeManager nm1 = new NodeManager();

        GRNTreeNode root = GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH);

        // manager must be set before nodes added for node manager to be updated

        root.setNodeManager(nm1);

        root.setNodes(Arrays.asList(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(m.getPart("PspaRK"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaR"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("SpaR"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaK"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("SpaK"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("BO_4296"), InterfaceType.BOTH))),
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(m.getPart("PspaS"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("RBS_SpaS"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("GFP_rrnb"), InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(m.getPart("BO_4296"), InterfaceType.BOTH)))));

        NodeManager nm2 = nm1.duplicate();

        List<BranchNode> bn1 = nm1.getBranchNodes();
        List<BranchNode> bn2 = nm2.getBranchNodes();

        Iterator<BranchNode> i1 = bn1.iterator();
        Iterator<BranchNode> i2 = bn2.iterator();

        while(i1.hasNext() || i2.hasNext()) {
            Assert.assertEquals("Both iterators should have same state", i1.hasNext(), i2.hasNext());
            BranchNode node1 = i1.next();
            BranchNode node2 = i2.next();
            LOGGER.debug("Testing {} and {}", node1, node2);
            Assert.assertNotSame("Nodes should be different objects", node1, node2);
        }

        List<LeafNode> ln1 = nm1.getLeafNodes();
        List<LeafNode> ln2 = nm2.getLeafNodes();

        Iterator<LeafNode> i3 = ln1.iterator();
        Iterator<LeafNode> i4 = ln2.iterator();

        while(i3.hasNext() || i4.hasNext()) {
            Assert.assertEquals("Both iterators should have same state", i3.hasNext(), i4.hasNext());
            LeafNode node1 = i3.next();
            LeafNode node2 = i4.next();
            LOGGER.debug("Testing {} and {}", node1, node2);
            Assert.assertNotEquals("Nodes should be different objects", node1, node2);
        }

        Assert.assertEquals("Interactions should be identical",
                nm1.getInstancedInteractions().stream().map(Interaction::getName).collect(Collectors.toSet()),
                nm2.getInstancedInteractions().stream().map(Interaction::getName).collect(Collectors.toSet()) );

        GRNTreeNode pspas = nm2.getNodes("PspaS").iterator().next();
        pspas.getParent().removeNodeAndDescendents(pspas);

        Assert.assertNotEquals("Interactions should not be identical after modification",
                nm1.getInstancedInteractions().stream().map(Interaction::getName).collect(Collectors.toSet()),
                nm2.getInstancedInteractions().stream().map(Interaction::getName).collect(Collectors.toSet()) );


    }

    @Test
    public void testDuplicateAndModification() {
        GRNTree tree1 = GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter;" +
                        "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");

        GRNTree tree2 = tree1.duplicate();


        GRNTreeNode spar = tree2.getParts("SpaR").iterator().next();
        GRNTreeNode sparParent = spar.getParent();
        sparParent.removeNode(spar);

        Assert.assertFalse("SpaR should have been removed from tree 2", tree2.containsLeafNode(spar));

        Assert.assertEquals("Tree 1 should still have 2 interactions", 2, tree1.getInteractionsSize());
        Assert.assertEquals("Tree 2 should have 0 interactions", 0, tree2.getInteractionsSize());

        try {
            sparParent.addNode(4, spar);
        } catch (Exception e) {
           LOGGER.error(e.getLocalizedMessage(), e);
        }

        Assert.assertTrue("SpaR should have been added to tree 2", tree2.containsLeafNode(spar));

        Assert.assertEquals("Tree 1 should still have 2 interactions", 2, tree1.getInteractionsSize());
        Assert.assertEquals("Tree 2 should have 2 interactions", 2, tree2.getInteractionsSize());


    }
}
