package uk.ac.ncl.icos.grntree.tests;


import org.junit.*;
import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author b1050029
 */

public class PrototypeSvpTests {
    
    private PartsHandler        p;
    private SBMLDocument        container;
    private SBMLHandler         sbmlhandler;
    private String              server = SVPManager.getRepositoryUrl();

    private AbstractSVPManager  ABSTRACT_MANAGER = AbstractSVPManager.getSVPManager();
    private SVPManager          MANAGER = SVPManager.getSVPManager();

    private SVPHelper           HELPER = SVPHelper.getSVPHelper();


    private ModelBuilder        mb;
    private static final Logger logger = LoggerFactory.getLogger(PrototypeSvpTests.class);


    // ===============================================
    // Create Parts, providing a Part ID
    // ===============================================

    private Part a = ABSTRACT_MANAGER.getConstPromoterPart("promA");
    private Part b = ABSTRACT_MANAGER.getNegativeOperator("opB");
    private Part c = ABSTRACT_MANAGER.getRBS("rbsC");
    private Part d = ABSTRACT_MANAGER.getCDS("cdsD");

    private Part e = ABSTRACT_MANAGER.getConstPromoterPart("promE");
    private Part f = ABSTRACT_MANAGER.getNegativeOperator("opF");
    private Part g = ABSTRACT_MANAGER.getRBS("rbsG");
    private Part h = ABSTRACT_MANAGER.getCDS("cdsH");

    private Part i = ABSTRACT_MANAGER.getConstPromoterPart("promI");
    private Part j = ABSTRACT_MANAGER.getNegativeOperator("opJ");
    private Part k = ABSTRACT_MANAGER.getRBS("rbsK");
    private Part l = ABSTRACT_MANAGER.getCDS("cdsL");

    // ===============================================
    // Create Internal Interactions, passing parts
    // and parameters, if needed.
    // ===============================================

    private List<Interaction> a_i = ABSTRACT_MANAGER.getPoPSProduction(a, 0.0269);
    private List<Interaction> e_i = ABSTRACT_MANAGER.getPoPSProduction(e, 0.03242);
    private List<Interaction> i_i = ABSTRACT_MANAGER.getPoPSProduction(i, 0.02513);

    private List<Interaction> b_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(b);
    private List<Interaction> f_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(f);
    private List<Interaction> j_i = ABSTRACT_MANAGER.getOperatorPoPSModulation(j);

    private List<Interaction> c_i = ABSTRACT_MANAGER.getRiPSProduction(c, 0.34255);
    private List<Interaction> g_i = ABSTRACT_MANAGER.getRiPSProduction(g, 0.51213);
    private List<Interaction> k_i = ABSTRACT_MANAGER.getRiPSProduction(k, 0.4685);

    private List<Interaction> d_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(d, 0.01412);
    private List<Interaction> h_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(h, 0.02134);
    private List<Interaction> l_i = ABSTRACT_MANAGER.getProteinProductionAndDegradation(l, 0.03214);

    // ===============================================
    // Create Part-Part Interactions
    // ===============================================

    private Interaction dRepressesf = ABSTRACT_MANAGER.getRegulationInteraction(   f, d,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.3456);

    private Interaction hRepressesj = ABSTRACT_MANAGER.getRegulationInteraction(   j, h,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.4567);

    private Interaction lRepressesb = ABSTRACT_MANAGER.getRegulationInteraction(   b, l,
            MolecularForm.DEFAULT,
            RegulationRole.REPRESSOR,
            0.3754);

    @BeforeClass
    public static void setUpClass() { }

    @AfterClass
    public static void tearDownClass() { }

    @Before
    public void setUp() {
        p = new PartsHandler(server);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);
    }

    @After
    public void tearDown() { }

    @Test
    public void testGrnTreeConstructionRepressilator() throws Exception
    {
        GRNTree tree = GRNTreeFactory.getGRNTree();

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(a, a_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(b, b_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(c, c_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(d, d_i, InterfaceType.BOTH))));

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(e, e_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(f, f_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(g, g_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(h, h_i, InterfaceType.BOTH))));

        tree.getRootNode().addNode(
                GRNTreeNodeFactory.getBranchNode(InterfaceType.BOTH, Arrays.asList(
                        GRNTreeNodeFactory.getLeafNode(i, i_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(j, j_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(k, k_i, InterfaceType.BOTH),
                        GRNTreeNodeFactory.getLeafNode(l, l_i, InterfaceType.BOTH))));

        tree.addInteraction(dRepressesf);
        tree.addInteraction(hRepressesj);
        tree.addInteraction(lRepressesb);

        CompilationDirector director  = new CompilationDirector(SVPManager.getRepositoryUrl());
        director.setModelCompilables(GRNCompilableFactory.getCompilables(tree));

        SimpleCopasiSimulator s = new SimpleCopasiSimulator(12800, 640);
        s.setModel(director.getSBMLString("repressilator"));
        Assert.assertTrue("Model should simulate correctly", s.run());
    }


    @Test
    public void testManualConstructionRepressilator() throws Exception
    {

        // =======================================================
        // Create SBML models from parts and internal interactions
        // =======================================================

        SBMLDocument a_doc = p.CreatePartModel(a, a_i);
        SBMLDocument b_doc = p.CreatePartModel(b, b_i);
        SBMLDocument c_doc = p.CreatePartModel(c, c_i);
        SBMLDocument d_doc = p.CreatePartModel(d, d_i);
        SBMLDocument e_doc = p.CreatePartModel(e, e_i);
        SBMLDocument f_doc = p.CreatePartModel(f, f_i);
        SBMLDocument g_doc = p.CreatePartModel(g, g_i);
        SBMLDocument h_doc = p.CreatePartModel(h, h_i);
        SBMLDocument i_doc = p.CreatePartModel(i, i_i);
        SBMLDocument j_doc = p.CreatePartModel(j, j_i);
        SBMLDocument k_doc = p.CreatePartModel(k, k_i);
        SBMLDocument l_doc = p.CreatePartModel(l, l_i);

        // =======================================================
        // Create SBML Models for Interactions
        // (JParts attempts to retrieve non-existent parts)
        // TODO Update JParts to use above lists of parts
        // =======================================================

        SBMLDocument dTof = p.CreateInteractionModel(Arrays.asList(d, f), dRepressesf);
        SBMLDocument hToj = p.CreateInteractionModel(Arrays.asList(h, j), hRepressesj);
        SBMLDocument lTob = p.CreateInteractionModel(Arrays.asList(l, b), lRepressesb);

        SBMLDocument mrna1 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna2 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna3 = p.GetModel(p.GetPart("mRNA"));

        // =======================================================
        // Compose Part and Interaction Models into a
        // simulateable model
        // TODO update compiler to automate this process
        // TODO can abstract parts and concrete parts be mixed?
        // =======================================================

        mb.Add(a_doc);
        mb.Link(a_doc, b_doc);
        mb.Add(b_doc);
        mb.Link(b_doc, lTob);
        mb.Add(lTob);
        mb.Add(mrna1);
        mb.Link(lTob, mrna1);
        mb.Link(mrna1, c_doc);
        mb.Add(c_doc);
        mb.Link(c_doc, d_doc);
        mb.Add(d_doc);

        mb.Add(e_doc);
        mb.Link(e_doc, f_doc);
        mb.Add(f_doc);
        mb.Link(f_doc, dTof);
        mb.Add(dTof);
        mb.Add(mrna2);
        mb.Link(dTof, mrna2);
        mb.Link(mrna2, g_doc);
        mb.Add(g_doc);
        mb.Link(g_doc, h_doc);
        mb.Add(h_doc);

        mb.Add(i_doc);
        mb.Link(i_doc, j_doc);
        mb.Add(j_doc);
        mb.Link(j_doc, hToj);
        mb.Add(hToj);
        mb.Add(mrna3);
        mb.Link(hToj, mrna3);
        mb.Link(mrna3, k_doc);
        mb.Add(k_doc);
        mb.Link(k_doc, l_doc);
        mb.Add(l_doc);

        SimpleCopasiSimulator s = new SimpleCopasiSimulator(12800, 640);
        s.setModel(mb.GetModelString());
        Assert.assertTrue("Model should simulate correctly", s.run());
    }
}
