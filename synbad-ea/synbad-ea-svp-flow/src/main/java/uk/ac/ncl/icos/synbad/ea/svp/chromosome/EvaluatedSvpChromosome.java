/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ea.svp.chromosome;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.operator.Operator;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.TimeCourseResult;

/**
 *
 * @author owengilfellon
 */
public class EvaluatedSvpChromosome<T extends SvpChromosome> extends EvaluatedChromosome<T>{
    
    protected T c;
    private Fitness f;
    private Operator o;
    private List<CResult> constraintResults;
    private List<TimeCourseResult> results;

    public EvaluatedSvpChromosome(T c, Fitness f, Operator o) {
        super((T)c.duplicate(), f, o);
        this.constraintResults = Collections.EMPTY_LIST;
    }
    
    public EvaluatedSvpChromosome(T c, Fitness f, List<CResult> constraintResults, List<TimeCourseResult> results, Operator o) {
        super((T)c.duplicate(), f, o);
        this.constraintResults = constraintResults;
        this.results = results;
    }

    public List<CResult> getConstraintResults() {
        return constraintResults;
    }

    public List<TimeCourseResult> getResults() {
        return results;
    }
    
}
