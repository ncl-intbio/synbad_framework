/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ea.svp.fitness;

import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.constraint.Constraint;
import uk.ac.ncl.icos.eaframework.constraint.ConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.datatype.SimulatorResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.ACopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.fitness.CFitness;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulatorFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.synbad.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.ea.svp.chromosome.SvpChromosome;
import uk.ac.ncl.icos.synbad.flow.view.object.SBViewComponent;
import uk.ac.ncl.icos.synbad.hgraph.traversal.SBHIterator;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.svp.model.SvpModel;
import uk.ac.ncl.icos.synbad.svp.parser.SVPWriter;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;

/**
 * 
 * @author owengilfellon
 */

public class SvpCopasiConstraintEvaluator extends ACopasiConstraintEvaluator<SvpChromosome> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpCopasiConstraintEvaluator.class);

    public SvpCopasiConstraintEvaluator(
            CopasiSimulatorFactory cs, 
            ConstraintHandler handler,
            List<Constraint<SimulatorResult, Double>> constraints,
            List<SBMLModifier> modifiers,
            int duration, 
            int runtime) {
        super(cs, handler, constraints, modifiers, duration, runtime);
    }

    @Override
    public CFitness evaluate(SvpChromosome c) {  
        CopasiSimulator s = cs.getSimulator(getDuration(), getRuntime());
        
        
        SvpModel model = c.getSvpModel();
        SvpPView view = model.getView(SvpPView.class);
        SBHIterator<SBViewComponent<SBIdentified>>  i = view.hierarchyIterator(view.getRoot());
        
        view.getModel().
        
        while(i.hasNext()) {
            SBViewComponent<SBIdentified> vc = i.next();
            if(vc.objectIs(FunctionalComponent.class)) {
                LOGGER.debug("Found: {}", vc.getDisplayId());
                
                new SVPWriter(vc.getWorkspaceId(), vc.getContexts()).write(svp);
            }
        }
        
        
        s.setModel(SVPParser.getSBML(exporter.export(c), getModifiers()));
        if(!s.run())
            return null;
       
        SimulatorResult r = new SimulatorResult(s.getTimeSeries(), c, 0);      
        List<CResult> result = getConstraints().stream().parallel().map(constraint -> new CResult(constraint.toString(), constraint.apply(r))).collect(Collectors.toList());
        return new CFitness(
            getHandler().processConstraints(result.stream().parallel().map(ce -> ce.getEvaluation()).collect(Collectors.toList())).getFitness(),
            result,
            s.getTimeSeries());
    }
}
