package uk.ac.ncl.icos.synbad.ea.svp.chromosome;


import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.*;
import uk.ac.ncl.icos.eaframework.chromosome.Chromosome;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.ModelObserver;
import uk.ac.ncl.icos.grntree.api.ModelSubject;
import uk.ac.ncl.icos.synbad.svp.model.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;


/**
 * A svpModel-based representation of a Genetic Regulatory Network (GRN), with identification of transcription units.
 * "Interface Types" can be specified in order to limit the interactionDocuments permitted between nodes (e.g. INPUT, OUTPUT).
 * A node (e.g. Promoter) with interface type INPUT can interact with sibling OUTPUT nodes. If a parent has an Interface
 * Type, the bounds of a node's potential interactionDocuments are extended to the parent's siblings. The set of nodes specified
 * by a given node's interaction bounds and it's compliment must be orthogonal (i.e. interactionDocuments are not allowed outside
 * of a given node's interaction bounds).
 *
 * @author owengilfellon
 */

//TODO stick this behind an api / interface

public class SvpChromosome implements Chromosome, ModelSubject {

    private SvpModel svpModel;
 
    //public SvpChromosome() { this.svpModel = GRNTreeFactory.getGRNTree(); }
    
    public SvpChromosome(SvpModel svpModel) { this.svpModel = svpModel;  }

    public SvpModel getSvpModel()
    {
        return svpModel;
    }

    public SvpModule getRootNode()
    {
        return svpModel.getRootNode();
    }

    /**
     * Returns the number of parts (i.e. SVPs, Genetic Features) in the svpModel.
     * @return
     */
    public int getPartsSize()
    {
        return svpModel.getPartsSize();
    }

    public int getInteractionsSize()
    {
        return svpModel.getInteractionsSize();
    }

    public SvpChromosome duplicate() {
        return new SvpChromosome();
    }

    @Override
    public void attach(ModelObserver o) {
        o.update(new );
        svpModel.attach(o);
    }

    @Override
    public void dettach(ModelObserver d) {
        svpModel.attach(d);
    }

    @Override
    public void alert() {
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SvpChromosome))
            return false;

        SvpChromosome other = (SvpChromosome) obj;
        return other.toString().equals(this.toString());
    }
}
