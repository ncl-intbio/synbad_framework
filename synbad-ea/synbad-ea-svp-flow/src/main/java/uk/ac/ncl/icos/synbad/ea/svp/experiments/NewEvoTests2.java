package uk.ac.ncl.icos.synbad.ea.svp.experiments;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import uk.ac.ncl.icos.eaframework.evoengines.EvoEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.Operator;
import uk.ac.ncl.icos.eaframework.terminationconditions.FitnessTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.constraint.ConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.constraint.LinearityConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.LowerBoundConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.RatioConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.UpperBoundConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.LowestConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.constraint.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.constraint.SummationHandler;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.ACopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SvpHibernateObserver;
import uk.ac.ncl.icos.eaframework.grn.population.PopulationProcess;
import uk.ac.ncl.icos.eaframework.grn.population.SelectAndMutateGRNProcess;
import uk.ac.ncl.icos.eaframework.grn.population.SelectProcess;
import uk.ac.ncl.icos.eaframework.grn.population.Strategy;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.CopasiSimulatorFactory;
import uk.ac.ncl.icos.eaframework.observer.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.SimplePopObserver;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournamnet;
import uk.ac.ncl.icos.eaframework.selection.PopulationIteratorSelection;
import uk.ac.ncl.icos.eaframework.selection.Selection;
import uk.ac.ncl.icos.eaframework.selection.SigmaScaledRouletteWheel;
import uk.ac.ncl.icos.eaframework.selection.Tournament;
import uk.ac.ncl.icos.eaframework.selection.Truncation;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.hibernate.HOperator;
import uk.ac.ncl.icos.hibernate.HExperiment;
import uk.ac.ncl.icos.hibernate.HibernateUtil;
import uk.ac.ncl.icos.hibernate.HPopulation;
import uk.ac.ncl.icos.hibernate.HSelectionProcess;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier.RemoveInducibleBaseLevel;
import uk.ac.ncl.icos.synbad.ea.svp.SvpEngine;
import uk.ac.ncl.icos.synbad.ea.svp.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.synbad.ea.svp.chromosome.SvpChromosome;
import uk.ac.ncl.icos.synbad.ea.svp.chromosome.SvpChromosomeFactory;
import uk.ac.ncl.icos.synbad.ea.svp.fitness.SvpCopasiConstraintEvaluator;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.model.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.workspace.api.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.api.SBWorkspaceManager;

/**
 * Main class for the CIBCB experiments. Uses a Mutant Champ strategy to evolve the SpaR-SpaK system to produce a
 * linear response to Subtilin, as determined by the concentration of GFP expressed by a PspaS promoter.
 *
 * @author owengilfellon
 */
public class NewEvoTests2 {
    
    static private final Logger LOGGER = LoggerFactory.getLogger(NewEvoTests2.class);
    
    static String NAME_PREFIX = "Experiment_";
    final Boolean restart;
    
    // ALGORITHM
    
    static final int MAX_GENERATIONS = 1000;
    static  int MUTATIONS_PER_GENERATION = 1;
    static final String INDEPENDENT_METABOLITE = "Subtilin";
    static final String DEPENDENT_METABOLITE = "GFP_rrnb";
    
    // POPULATIONS AND SELECTION
    
    static final int THREAD_POOL_SIZE = 30;
    static int CHILD_POPULATION_SIZE = 30;
    static int SURVIVAL_POPULATION_SIZE = 10;
    
    static int TRUNCATION_SIZE = 12;
    static int TOURNAMENT_SIZE = 30;
    
    // Summation handler seems to have more difficulty in finding and
    // keeping PspaS duplications
    static ConstraintHandler constraintHandler = new MultiplyHandler();
    
    // CONSTRAINT
    
    static final double TARGET_RATIO = 0.14918591815273405;
    static final double TARGET_LOWER = 0.0;
    static final double TARGET_UPPER = 21428.5714;
    static int MAX_PARTS = 30;
    static int MAX_UNPENALISEDPARTS = 10;
    static final int MAX_TUS = 3;

    // RESPONSE CURVE

    static final double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    static final int INCREMENT = 50;
    
    // COPASI
    
    static final int DURATION = 21600;
    static final int STEPS = 2160;
    
    public NewEvoTests2(boolean restart) {
        this.restart = restart;
    }
    
    public static List<HExperiment> getExperiments() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        EntityManager m = session.getEntityManagerFactory().createEntityManager();
        TypedQuery<HExperiment> query = m.createQuery("SELECT e FROM HExperiment e WHERE e.name LIKE '" + NAME_PREFIX +  "%'", HExperiment.class);
        List<HExperiment> exp = null;
        try {
            exp = query.getResultList();
        } catch( NoResultException e ) {
            exp = new ArrayList<>();
        }
        m.close();
        t.commit();
        session.close();
        return exp;
    }

    public static List<HExperiment> getUnfinishedExperiments(List<HExperiment> experiments) {
        return experiments.stream()
            .filter(e -> {
                int currentGeneration = e.getPopulations().get(e.getPopulations().size()-1).getGeneration();
                return e.getMaxGenerations() > currentGeneration;})
            .collect(Collectors.toList());
    }
    
    public static List<Operator<SvpChromosome>> getOperators(HExperiment experiment, HSelectionProcess process) {
       
        List<Operator<SvpChromosome>> operators = new ArrayList<>();
            for(HOperator operator : process.getOperators()) {
                try {
                    Class operatorClass = Class.forName("uk.ac.ncl.icos.eaframework.grn.operator." + operator.getOperatorName());
                    Constructor constructor = operatorClass.getConstructor();
                    operators.add((Operator<SvpChromosome>) constructor.newInstance());
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage());
                }
            
            }
            return operators;
    }
    
    public static Strategy getStrategy(HExperiment experiment,  ACopasiConstraintEvaluator f) {               
     
        if(experiment == null) return null;
        
        // TO DO: Load population processes from DB
        
         Strategy<SvpChromosome> strategy = new Strategy.Overlapping<>(
                getSelectionProcess(experiment, experiment.getReproductionProcess(), f), 
                getSelectionProcess(experiment, experiment.getSurvivalProcess(), f));
         
         return strategy;
    }
    
    
     public static PopulationProcess<SvpChromosome> getSelectionProcess(HExperiment experiment, HSelectionProcess selection, ACopasiConstraintEvaluator f) {

        if(selection.getStrategyName().equals("SigmaScaledRouletteWheel")) {
            List<Operator<SvpChromosome>> operators = new ArrayList<>();
            
            
            
            for(HOperator operator : selection.getOperators()) {
                try {
                    Class operatorClass = Class.forName("uk.ac.ncl.icos.eaframework.grn.operator." + operator.getOperatorName());
                    Constructor constructor = operatorClass.getConstructor();
                    operators.add((Operator<SvpChromosome>) constructor.newInstance());
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage());
                }
            }

            
            Operator<SvpChromosome> o = selection.getWeights() != null ? new OperatorGroup(operators, selection.getWeights().getWeights(), selection.getWeights().isSigmaScaled()) : new OperatorGroup(operators);
            return new SelectAndMutateGRNProcess<>(new SigmaScaledRouletteWheel<>(true), o, f, selection.getPopulationSize(), MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
        }
        
        if(selection.getStrategyName().equals("Uniform"))
           return new SelectProcess<>(new Uniform(), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("Truncation"))
           return new SelectProcess<>(new Truncation<>(TRUNCATION_SIZE), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("Tournament"))
            return new SelectProcess<>(new Tournament<>(), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("FitnessProportionalTournamnet"))
            return new SelectProcess<>( new FitnessProportionalTournamnet<>(TOURNAMENT_SIZE), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("PopulationIteratorSelection")) {
            LOGGER.debug("Found PopulationIterator!");
            return new SelectProcess<>( new PopulationIteratorSelection<>(), selection.getPopulationSize());
        }
            
        return null;
    }
    
    
    public void runExperiment() {
        
    }


    public static void main(String[] args)
    {  
        
        ClassLoader cl = ClassLoader.getSystemClassLoader();

        URL[] urls = ((URLClassLoader)cl).getURLs();

        for(URL url: urls){
        	System.out.println(url.getFile());
        }

        boolean restart = false;

        for (String arg : args) {
            if(arg.equals("-r"))
                restart = true;
            else if (arg.startsWith("-pr")) {
                
                String pr = arg.trim().substring(3, arg.trim().length());
                try {
                    Integer prInt = Integer.parseInt(pr);
                    if(prInt > 0) {
                        LOGGER.debug("Setting Child Population Size: " + prInt);
                        CHILD_POPULATION_SIZE = prInt;
                    }  
                    else
                        LOGGER.error("-pr ("+ prInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -pr, using default");
                }
            } else if (arg.startsWith("-ps")) {
                String ps = arg.trim().substring(3, arg.trim().length());
                try {
                    Integer psInt = Integer.parseInt(ps);
                    if(psInt > 0) {
                        LOGGER.debug("Setting Survival Population Size: " + psInt);
                        SURVIVAL_POPULATION_SIZE = psInt;
                    }
                        
                    else
                        LOGGER.debug("ps ("+ psInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -ps, using default");
                }
            } else if (arg.startsWith("-parts")) {
                String parts = arg.trim().substring(6, arg.trim().length());
                try {
                    Integer partsInt = Integer.parseInt(parts);
                    if(partsInt > 0) {
                        LOGGER.debug("Setting Max Parts: " + parts);
                        MAX_PARTS = partsInt;
                    }
                    else
                        LOGGER.error("-parts ("+ partsInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -parts, using default");
                }
            }
            else if (arg.startsWith("-uparts")) {
                String parts = arg.trim().substring(7, arg.trim().length());
                try {
                    Integer partsInt = Integer.parseInt(parts);
                    if(partsInt > 0) {
                        LOGGER.debug("Setting Max Unpenalised Parts: " + parts);
                        MAX_PARTS = partsInt;
                    }
                    else
                        LOGGER.error("-parts ("+ partsInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -uparts, using default");
                }
            }
            else if (arg.startsWith("-m")) {
                String mutations = arg.trim().substring(2, arg.trim().length());
                try { 
                    Integer mutationsInt = Integer.parseInt(mutations);
                    if(mutationsInt > 0) {
                        LOGGER.debug("Setting # of mutations: " + mutations);
                        MUTATIONS_PER_GENERATION = mutationsInt;
                    }
                    else
                        LOGGER.error("-m ("+ mutationsInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -m, using default");
                }
            }
            else if (arg.startsWith("-name")) {
                String name = arg.trim().substring(5, arg.trim().length());
                if(name != null && !name.isEmpty()) {
                    LOGGER.debug("Setting Name: " + name);
                    NAME_PREFIX = name;
                }  
                else
                    LOGGER.error("-name ("+ name + ") is not valid");
            } else if (arg.startsWith("-c")) {
                String constraint = arg.trim().substring(2, arg.trim().length());
                if(constraint != null && !constraint.isEmpty()) {
                    if(constraint.equalsIgnoreCase("product")) {
                        LOGGER.debug("Setting Constraint Handler: " + constraint);
                        constraintHandler = new MultiplyHandler();
                    } else if (constraint.equalsIgnoreCase("sum")) {
                        constraintHandler = new SummationHandler();
                    } else if (constraint.equalsIgnoreCase("lowest")) {
                        constraintHandler = new LowestConstraintHandler();
                    }                     
                }  
            }
        }
        
        
        //PropertyConfigurator.configure("log4j.properties");
       
        HExperiment experiment = null;
        
       /* HExperiment experiment = restart 
                ? getUnfinishedExperiments(getExperiments()).stream().findFirst().orElse(null)
                : null;

        if(experiment!=null)
            LOGGER.debug("Restarting " + experiment.getName());
        */
        String experimentName = experiment == null ?
                NAME_PREFIX + new SimpleDateFormat("yyMMdd_HHmmssSSS").format(new Date()) :
                experiment.getName();
        
        SvpModule module = ExampleFactory.setupWorkspace();
        SBWorkspace ws = SBWorkspaceManager.getManager().getWorkspace(module.getWorkspaceId());
        SvpModel model = new SvpModel(ws, module);
        SvpChromosome seed =new SvpChromosome(model);
        ChromosomeFactory<SvpChromosome> cf = new SvpChromosomeFactory(Arrays.asList(seed), SURVIVAL_POPULATION_SIZE);
        TerminationCondition generationTermination = experiment == null ?
                new GenerationTermination(MAX_GENERATIONS) :
                new GenerationTermination(experiment.getMaxGenerations());

        TerminationCondition t = new TerminationGroup(Arrays.asList(
                generationTermination,
                new FitnessTermination(100.0)));
        
        CopasiSimulatorFactory<CopasiSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime);
            cs.setNumberOfMetaboliteIncrements(100);
            cs.setMaximumMetaboliteConcentration(10000);
            cs.setMetabolitePair("Subtilin", "GFP_rrnb");
            cs.setIncrement(INCREMENT);
            cs.setMinChange(MIN_CHANGE);
            return cs;
        };

        SvpCopasiConstraintEvaluator f = new SvpCopasiConstraintEvaluator(
            simulatorFactory,
            constraintHandler,
            Arrays.asList(       
                new LinearityConstraint("GFP_rrnb", "Subtilin"),
                new LowerBoundConstraint("GFP_rrnb",  TARGET_LOWER, 1.0),
                new UpperBoundConstraint("GFP_rrnb", TARGET_UPPER, 1.0),
                new RatioConstraint(DEPENDENT_METABOLITE, INDEPENDENT_METABOLITE, TARGET_RATIO)
                //new PenaliseRangeDecrease("GFP_rrnb", TARGET_UPPER, 1.0),
                //new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS)
                ),
            Arrays.asList(new RemoveInducibleBaseLevel()),
            DURATION, STEPS        
        );

        Strategy<SvpChromosome> strategy = null;//getStrategy(experiment, f);
        
        if(strategy == null) {
            List<Operator<SvpChromosome>> operatorList = 
            Arrays.asList(
//                new RandomiseRBS(),
//                new AddRegulation(),
//                new DuplicateParts(),
//                new DuplicateTU(),
//                new RandomiseConstPromoter(),
//                new RemoveRegulation(),
//                new SplitTU(),
//                new SwapParts(),
//                new RemoveParts(),
//                // New Operators
//
//                new AddPromoter(),
//                new MergeTU(),
//                new RemovePromoter(),
//                new RemoveTU()
            );
            
            double[] weights= {
                1,      //RandomiseRBS
                0.1,    //AddRegulation
                0.1,    //DuplicateParts
                0.1,    //DuplicateTU
                1,      //RandomiseConstPromoter
                0.1,    //RemoveRegulation
                0.1,    //SplitTU
                0.1,    //SwapParts
                0.1,    //RemoveParts
                0.1,    //AddPromoter
                0.1,    //MergeTU
                0.1,    //RemovePromoter
                0.1 };  //RemoveTU
            
            Operator<SvpChromosome> o = new OperatorGroup(operatorList, weights, false);

            Selection<SvpChromosome> s1 = new SigmaScaledRouletteWheel<>(true); 
           // Selection<SvpChromosome> s1 = new Uniform<>();
            //Selection<SvpChromosome> s2 = new Truncation<>(1);
            Selection<SvpChromosome> s2 = new FitnessProportionalTournamnet<>(TOURNAMENT_SIZE);
            //Selection<SvpChromosome> s2 = new Truncation<>(TRUNCATION_SIZE);
                
            PopulationProcess<SvpChromosome> reproduction = new SelectAndMutateGRNProcess<>(s1, o, f, CHILD_POPULATION_SIZE, MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
            //PopulationProcess<SvpChromosome> survival = new SelectProcess<>(new Truncation<>(TRUNCATION_SIZE), SURVIVAL_POPULATION_SIZE);
            //PopulationProcess<SvpChromosome> survival = new SelectProcess<>(new Tournament<>(), SURVIVAL_POPULATION_SIZE);
            PopulationProcess<SvpChromosome> survival  = new SelectProcess<>( s2, SURVIVAL_POPULATION_SIZE);
            
            strategy = new Strategy.Overlapping<>(
                reproduction, 
                survival);  
        }
        

        /*
        Strategy<SvpChromosome> strategy = new Strategy.NonOverlapping<>(
            sigmaSelectAndMutate, truncationSurvival);*/

        EvoEngine<SvpChromosome> grnTreeEngine = new SvpEngine( CHILD_POPULATION_SIZE, SURVIVAL_POPULATION_SIZE, cf, strategy, f, t);
       /* else {
            List<HPopulation> populations = experiment.getPopulations();
            HPopulation lastPopulation = populations.get(populations.size()-1);
            List<EvaluatedChromosome<SvpChromosome>> chromosomes = (List<EvaluatedChromosome<SvpChromosome>>)lastPopulation.getPopulation().stream()
                               .map(ev -> { 
                                   ev.
                                   SBWorkspace ws = SBWorkspaceManager.getManager().getWorkspace(ev.)
                                   
                                   SvpModel model = new SvpModel(ws, module);
                                   
                                   return new EvaluatedSvpChromosome<>(
                                       new SvpChromosome(),
                                       new Fitness(ev.getFitness().getFitness()),
                                       ev.getFitness().getResult().stream().map(r-> new CResult(r.getName(), r.getEvaluation())).collect(Collectors.toList()),
                                       Collections.EMPTY_LIST,
                                       null); }).collect(Collectors.toList());
            grnTreeEngine = new SvpEngine( lastPopulation.getGeneration(), 
                    chromosomes,
                    experiment.getReproductionProcess().getPopulationSize(), 
                    experiment.getSurvivalProcess().getPopulationSize(), 
                    cf, strategy, f, t, THREAD_POOL_SIZE);
        }*/

       // EvolutionObserver writer = new SVPTreeResultsFileWriter(experimentName, Arrays.asList("GFP_rrnb"));
        grnTreeEngine.attach(new SimplePopObserver());
      //  grnTreeEngine.attach(writer);
       /* grnTreeEngine.attach(new SvpHibernateObserver(experimentName, 
                INDEPENDENT_METABOLITE, 
                DEPENDENT_METABOLITE, 
                INCREMENT, 
                MAX_GENERATIONS, 
                CHILD_POPULATION_SIZE, 
                SURVIVAL_POPULATION_SIZE));*/
        grnTreeEngine.run(); 
    }

     
}
