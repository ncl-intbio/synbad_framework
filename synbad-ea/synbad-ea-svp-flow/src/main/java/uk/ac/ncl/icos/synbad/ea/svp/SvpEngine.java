package uk.ac.ncl.icos.synbad.ea.svp;

import uk.ac.ncl.icos.synbad.ea.svp.chromosome.SvpChromosome;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.AbstractEvoEngine;
import uk.ac.ncl.icos.eaframework.factories.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationCondition;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.fitness.CFitness;
import uk.ac.ncl.icos.eaframework.grn.population.OrderedSelectAndMutateGRNProcess;
import uk.ac.ncl.icos.eaframework.grn.population.OrderedSelectAndMutateGRNProcess.IndexedObject;
import uk.ac.ncl.icos.eaframework.grn.population.ResourcePool;
import uk.ac.ncl.icos.eaframework.grn.population.Strategy;
import uk.ac.ncl.icos.eaframework.selection.SelectionIterator;
import uk.ac.ncl.icos.synbad.ea.svp.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.synbad.ea.svp.fitness.SvpCopasiConstraintEvaluator;

/**
 * Implementation of EvoEngine with functionality specific to evolving GRNs. Uses SvpChromosome as a GRN representation.
 *
 * @see SvpChromosome
 * @author owengilfellon
 */
public class SvpEngine extends AbstractEvoEngine<SvpChromosome> {

    /**
     * The Evolutionary Algorithm is instantiated with its necessary components
     *
     * @param cf a ChromosomeFactory for generating populations
     * @param o an Operator (or group of Operators) for applying mutations to the population
     * @param s a Selection operator for selecting parts based on fitness for reproduction
     * @param e a Fitness function for evaluating a solution's behaviour in comparison to some target behaviour
     * @param t a Termination Condition, or group of Termination Conditions, that determine when to exit the algorithm
     */
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpEngine.class);
    Strategy<SvpChromosome> evolutionStrategy;
    private final int POPULATION_SIZE;
    private final int POOL_SIZE;
    
    private List<EvaluatedChromosome<SvpChromosome>> evaluateInitialPopulation(List<EvaluatedChromosome<SvpChromosome>> chromosomes)  {

        ExecutorService pool = new ThreadPoolExecutor(POOL_SIZE, POOL_SIZE, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
       
        // Store number of processed chromosomes using thread-safe integer
        AtomicInteger completion = new AtomicInteger(0);
        AtomicInteger failure = new AtomicInteger(0);
        
        // Store evolved population on completion
        EvaluatedChromosome<SvpChromosome>[] processed = new EvaluatedChromosome[POPULATION_SIZE];
        
        // Create iterator over population that will apply given selection strategy
        ResourcePool<IndexedObject<EvaluatedSvpChromosome<SvpChromosome>>> selectionPool = new ResourcePool<>(POPULATION_SIZE);
        
        for(int i = 0; i < POPULATION_SIZE; i++) {
            EvaluatedSvpChromosome<SvpChromosome> t =  (EvaluatedSvpChromosome<SvpChromosome>)chromosomes.get(i);
            selectionPool.insert(new IndexedObject<>(t, i));
            LOGGER.debug("Inserted at {}: [ {} ]", i, t.getChromosome());
        }
        
        while(completion.get() < POPULATION_SIZE) {
            
            
            if(!selectionPool.isEmpty()) {
   
                IndexedObject<EvaluatedSvpChromosome<SvpChromosome>> c = selectionPool.acquire();
            
                
                CompletableFuture<IndexedObject<EvaluatedChromosome<SvpChromosome>>> cf2 = CompletableFuture.supplyAsync(()-> {
                    return new IndexedObject<EvaluatedSvpChromosome<SvpChromosome>>(c.getObject(), c.getIndex());}, pool).thenApply(chrom -> {
                    if(chrom == null) {
                        //selectionPool.insert(it.next());
                        throw new NullPointerException("Evaluation error: chromosome is null");
                    }
                    
                    Fitness f = getFitnessEvaluator().evaluate(chrom.getObject().getChromosome());
                    
                    
                    if(f == null || Double.isNaN(f.getFitness())) {
                        //selectionPool.insert(it.next());
                        throw new NullPointerException("Evaluation error: fitness is null");
                    }
                    
                    EvaluatedSvpChromosome<SvpChromosome> ec = null;
               
                    if(f instanceof CFitness)
                        ec = new EvaluatedSvpChromosome<>(chrom.getObject().getChromosome(), f,  ((CFitness)f).getResult(), ((CFitness)f).getSimulationResults(), null);
                    else {
                        ec = new EvaluatedSvpChromosome<>(chrom.getObject().getChromosome(), f,  Collections.EMPTY_LIST, ((CFitness)f).getSimulationResults(), null);
                 
                    }
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("Adding to child population at {}: [ {} ]", chrom.getIndex(), chrom.getObject());
                    
                   
                    processed[chrom.getIndex()] = ec;
                    
                    completion.incrementAndGet();
                    
                    return new IndexedObject<>(ec, chrom.getIndex());
                    
                });

                // need to change failures to exceptions

                cf2.exceptionally(e -> {
                    LOGGER.error(e.getMessage());
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("ERROR! Reinserted: [ {} ]", c.getIndex());
                    selectionPool.insert(c);
                    failure.incrementAndGet();
                    return null;
                });

            }
        } 

        if(failure.get() > 0)
            LOGGER.warn("Returning from selection with {} failures", failure.get());
        
        pool.shutdown();
        
        return Arrays.asList(processed).stream().collect(Collectors.toList());
    }
    
    

    
    public SvpEngine(int generation, List<EvaluatedChromosome<SvpChromosome>> initialPopulation, int childPopulationSize, int survivalPopulationSize, ChromosomeFactory<SvpChromosome> cf, Strategy<SvpChromosome> strategy, SvpCopasiConstraintEvaluator e, TerminationCondition t, int poolSize)
    {
        super(cf, e, t);
        
        LOGGER.info("Reconstructing GRNTreeEngine at generation " + generation);
        // Evaluate population to generate simulation results - which may be
        // required if any of the population survive to the next generation


        this.evolutionStrategy = strategy;
        this.POPULATION_SIZE = survivalPopulationSize;
        this.currentGeneration = generation;
        this.POOL_SIZE = poolSize;
        
        LOGGER.info("Strategy: {}", evolutionStrategy.getClass().getSimpleName());
        LOGGER.info("Reproduction Selection: {}", evolutionStrategy.getReproduction().getClass().getSimpleName());
        LOGGER.info("Survival Selection: {}", evolutionStrategy.getSurvival().getClass().getSimpleName());
        LOGGER.info("Population Size (Reproduction | Survival): {}|{}", childPopulationSize, POPULATION_SIZE);

        
        this.evaluatedPopulation = evaluateInitialPopulation(initialPopulation);
        population = evaluatedPopulation.stream().map(p -> p.getChromosome()).collect(Collectors.toList());

        
        /*
        population.stream()
            .map(c -> new EvaluatedChromosome<>(c, e.evaluate(c), null))
            .forEach(evaluatedPopulation::add);
        */
        
    }

    public SvpEngine(int childPopulationSize, int survivalPopulationSize, ChromosomeFactory<SvpChromosome> cf, Strategy<SvpChromosome> strategy, SvpCopasiConstraintEvaluator e, TerminationCondition t)
    {
        super(cf, e, t);
        
        LOGGER.info("Constructing new GRNTreeEngine");

        this.population = cf.generatePopulation();
        
        // for identical population!
        
        CFitness f = null;
        for(int i = 0; i < survivalPopulationSize; i++) {
            if(f == null)
                f = e.evaluate(population.get(i));
            
            evaluatedPopulation.add(new EvaluatedSvpChromosome<>(population.get(i),  f, f.getResult(), f.getSimulationResults(), null));
        }
        
        this.evolutionStrategy = strategy;
        this.POPULATION_SIZE = survivalPopulationSize;
        this.POOL_SIZE = 1;
        
        LOGGER.info("Strategy: {}", evolutionStrategy.getClass().getSimpleName());
        LOGGER.info("Reproduction Selection: {}", evolutionStrategy.getReproduction().getClass().getSimpleName());
        LOGGER.info("Survival Selection: {}", evolutionStrategy.getSurvival().getClass().getSimpleName());
        LOGGER.info("Population Size: {}", POPULATION_SIZE);
        
    }


    /**
     * Steps the algorithm forward a single generation.
     */
    @Override
    public void stepForward() {
   
        currentGeneration++;
        LOGGER.debug("Stepping forward to generation " + currentGeneration);
        
        if(evaluatedPopulation != null && evaluatedPopulation.size() > 0 ){
            population.clear();
        }
        
        do {
            
            LOGGER.debug("Stepping forward with: [{}]", evolutionStrategy.getClass().getSimpleName());
            evaluatedPopulation = evolutionStrategy.nextPopulation(evaluatedPopulation);
            //System.out.println(currentGeneration + ": " + evaluatedPopulation.size());
        } while (evaluatedPopulation == null || evaluatedPopulation.size() < POPULATION_SIZE);
        
        population = evaluatedPopulation.stream().map(p -> p.getChromosome()).collect(Collectors.toList());

       // Logger.getLogger(SvpEngine.class.getName()).log(Level.INFO, "Pop1: {0}", evaluatedPopulation.size());
       // Logger.getLogger(SvpEngine.class.getName()).log(Level.INFO, "Pop2: {0}", evaluatedPopulation.size());
        alert();
    }

    /**
     * Runs the algorithm by repeatedly calling stepForward() until a termination condition is met.
     * @return The population, current as of when the termination conditions are met.
     */
    @Override
    public List<SvpChromosome> run()
    {
        LOGGER.info("Run");
        
        do
        {
            stepForward();
        }
        while(!t.shouldTerminate(getPopulationStats()));
        
        return population;
    }

    /**
     * Returns statistics for the most current generation - includes the best chromosome, the best fitness, the current
     * generation, the population size, and the mean fitness of the population.
     * @return An object containing the statistics
     */
    @Override
    public PopulationStats<SvpChromosome> getPopulationStats() {

        /*
        GRNSimulationEvaluator se = (GRNSimulationEvaluator) e;
        CopasiSimulator cs =  se.getSimulator();
        if(cs instanceof DependentValueCopasiSimulator)
        {
            DependentValueCopasiSimulator dvcs = (DependentValueCopasiSimulator) cs;
            ResponseCurve rc = new ResponseCurve(dvcs.getDependentMetaboliteValues());
            return new SVPStats(rc, super.getPopulationStats());
        }
        else
        {*/
            return super.getPopulationStats();
        //}
    }

    @Override
    public SvpCopasiConstraintEvaluator getFitnessEvaluator() {
        return (SvpCopasiConstraintEvaluator)super.getFitnessEvaluator();
    }

 
    @Override
    public List<EvaluatedSvpChromosome<SvpChromosome>> getEvaluatedPopulation() {
        synchronized(this) {
            return super.evaluatedPopulation.stream().filter(ec  -> ec instanceof EvaluatedSvpChromosome).map(ec -> (EvaluatedSvpChromosome<SvpChromosome>) ec).collect(Collectors.toList());
        }
            
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Strategy<SvpChromosome> getEvolutionaryStrategy() {
        return evolutionStrategy;
    }
        
    
    
    
    
    
    

}