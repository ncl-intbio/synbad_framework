/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.ea.core;

import org.bson.types.ObjectId;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.chromosome.StringChromosome;
import uk.ac.ncl.icos.eaframework.selection.Tournament;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author owengilfellon
 */
public class TournamentTests {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    double CONCENTRATION = 128.0;
    double[] observed = { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 16.0, 32.0, 64.0, 128.0, 256.0 , 512.0, 1024.0, 2048.0, 5096.0};

    public TournamentTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    private EvaluatedChromosome<StringChromosome> getChromosome(String c) {
        return new EvaluatedChromosome<>(new StringChromosome(c), new Fitness(c.length()),  null);
    }

    @Test
    public void testTournament() {

        List<EvaluatedChromosome<StringChromosome>> strings = Stream.of(
                "a",
                "aa",
                "aaa",
                "aaaa",
                "aaaaa",
                "aaaaaa").map(this::getChromosome).collect(Collectors.toList());

        Tournament<StringChromosome> t = new Tournament<>(6);
        Assert.assertEquals("aaaaaa", t.select(strings).next().getChromosome().getString());


        Tournament<StringChromosome> t2 = new Tournament<>(6);
        SelectionIterator<StringChromosome> it = t2.select(strings);
        for(int i = 0; i < 20; i++) {
            System.out.println(it.next().getChromosome().getString());
        }
    }
}