package uk.ac.ncl.icos.datatypes;

import java.io.Serializable;

public class Fitness implements Serializable {

    private double fitness;
    
    //constructor
    public Fitness(double fitness)
    {
        this.fitness = fitness;
    }

    public double getFitness()
    {
        return fitness;
    }

    public void setFitness(double fitness)
    {
        this.fitness = fitness;
    }

    @Override
    public String toString() {
        return fitness + "";
    }
    
    
}
