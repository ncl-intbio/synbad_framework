/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.constraints;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.ConstraintHandler;

/**
 *
 * @author owengilfellon
 */
public class LowestConstraintHandler implements ConstraintHandler {

    @Override
    public Fitness processConstraints(List<Double> values) {
        values.sort(Double::compareTo);
        if(!values.isEmpty()) {
            return new Fitness(values.get(0));
        } 
        else
            return new Fitness(0.0);
    }
}
