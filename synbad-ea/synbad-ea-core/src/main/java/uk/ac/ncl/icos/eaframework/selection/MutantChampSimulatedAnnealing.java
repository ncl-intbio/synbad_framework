/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Mutant Champ Simulated Annealing")
public class MutantChampSimulatedAnnealing<T extends Chromosome> implements Selection<T> {

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .build();
    }

    private final ParameterScheduler scheduler;

    public MutantChampSimulatedAnnealing(ParameterScheduler scheduler) {
        super();
        this.scheduler = scheduler;
    }

    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) {
        return new MutantChampSAIterator<>(scheduler, population);
    }
    
    class MutantChampSAIterator<T extends Chromosome> implements SelectionIterator<T> {
        
        private final List<EvaluatedChromosome<T>> population;
        private EvaluatedChromosome<T> bestChromosome;
        private final ParameterScheduler scheduler;

        public MutantChampSAIterator(ParameterScheduler scheduler, List<EvaluatedChromosome<T>> population) {
            this.population = population.stream().collect(Collectors.toList());
            this.scheduler = scheduler;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {

            if(bestChromosome==null) {
                for(EvaluatedChromosome<T> c : population) {
                    if(bestChromosome == null)
                        bestChromosome = c;
                    if(c.getFitness().getFitness() > bestChromosome.getFitness().getFitness())
                        bestChromosome = c;
                }
            }
            
            double val = population.get(0).getFitness().getFitness() *
                    (new Random().nextDouble() * scheduler.getParameter());

            if( val > bestChromosome.getFitness().getFitness()) {
                bestChromosome = population.get(0);
            }
            
            return bestChromosome;
        }
    }
}
