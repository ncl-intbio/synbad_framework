/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import java.io.Serializable;
import java.util.List;
import uk.ac.ncl.icos.datatypes.Fitness;

/**
 *
 * @author owengilfellon
 */
public interface ConstraintHandler extends Serializable {
    
    public Fitness processConstraints(List<Double> input);

}
