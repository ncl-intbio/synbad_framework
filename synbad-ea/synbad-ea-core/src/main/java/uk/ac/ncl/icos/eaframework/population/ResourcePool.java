/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.population;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class ResourcePool<T> {
 
    private static final Logger logger = LoggerFactory.getLogger(ResourcePool.class);
    private final int POOL_SIZE;
    private final BlockingQueue<T> resources;

    public ResourcePool(int poolSize) {
        this.POOL_SIZE = poolSize;
        this.resources = new ArrayBlockingQueue<>(POOL_SIZE);
    }
    
    public synchronized boolean hasSpace() {
        return resources.remainingCapacity() > 0;
    }
    
    public synchronized boolean isEmpty() {
        return resources.isEmpty();
    }

    public synchronized T acquire() {
    
        try {
            return resources.take();
        } catch (InterruptedException ex) {
            logger.error(ex.getMessage());
            return null;
        } finally {
             
        }
    }
    
    public synchronized void insert(T resource) {
       try {    
           if(resource == null)
               throw new NullPointerException("Cannot insert null resource");
           resources.add(resource);
       } catch(Exception e) {
           logger.error(e.getMessage());
       } finally {
           
       }
    }

}
