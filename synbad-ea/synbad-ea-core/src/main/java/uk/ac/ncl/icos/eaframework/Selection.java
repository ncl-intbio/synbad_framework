/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.synbad.core.util.Configured;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface Selection<T extends Chromosome> extends Configured {
    
    // Implements iterator returning next x items
    SelectionIterator<T> select(List<EvaluatedChromosome<T>> population);

}
