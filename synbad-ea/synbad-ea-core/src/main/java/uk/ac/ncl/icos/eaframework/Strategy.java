/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import java.util.List;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

/**
 *
 * @author owengilfellon
 */
public interface Strategy<T extends Chromosome> extends PopulationProcess<T> {
    
    PopulationProcess<T> getReproduction();

    PopulationProcess<T> getSurvival();
    
    class Overlapping<V extends Chromosome> implements Strategy<V> {

        private final PopulationProcess<V> reproduction;

        private final PopulationProcess<V> survival;
        
        public Overlapping( PopulationProcess<V> reproductionSelection, PopulationProcess<V> survivalSelection) {
            this.reproduction = reproductionSelection;
            this.survival = survivalSelection;
        }
   
        @Override
        public List<EvaluatedChromosome<V>> nextPopulation(List<EvaluatedChromosome<V>> population) {
            List<EvaluatedChromosome<V>> nextPopulation = reproduction.nextPopulation(population);
            nextPopulation.addAll(population);
            return survival.nextPopulation(nextPopulation);
        }

        @Override
        public List<EvaluatedChromosome<V>> getPopulation() {
            return survival.getPopulation();
        }

        @Override
        public PopulationProcess<V> getReproduction() {
            return reproduction;
        }

        @Override
        public PopulationProcess<V> getSurvival() {
            return survival;
        }

        @Override
        public Selection<V> getSelection() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Operator<V> getOperator() {
            return null;
        }
        
        @Override
       public int getPopulationSize() {
           return survival.getPopulationSize();
       }

    }
    
    class NonOverlapping<V extends Chromosome> implements Strategy<V> {

        private final PopulationProcess<V> reproduction;
        private final PopulationProcess<V> survival;
        
        public NonOverlapping( PopulationProcess<V> reproductionSelection, PopulationProcess<V> survivalSelection) {
            this.reproduction = reproductionSelection;
            this.survival = survivalSelection;
        }
   
        @Override
        public List<EvaluatedChromosome<V>> nextPopulation(List<EvaluatedChromosome<V>> population) {
            List<EvaluatedChromosome<V>> nextPopulation = reproduction.nextPopulation(population);
            return survival.nextPopulation(nextPopulation);
        }

        @Override
        public List<EvaluatedChromosome<V>> getPopulation() {
            return survival.getPopulation();
        }

        @Override
        public PopulationProcess<V> getSurvival() {
            return survival;
        }

        @Override
        public PopulationProcess<V> getReproduction() {
            return reproduction;
        }

        @Override
        public Selection<V> getSelection() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Operator<V> getOperator() {
            return null;
        }
        
        @Override
        public int getPopulationSize() {
            return survival.getPopulationSize();
        }

    }

}
