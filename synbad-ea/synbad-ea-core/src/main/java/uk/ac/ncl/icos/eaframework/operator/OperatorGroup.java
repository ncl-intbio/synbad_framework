package uk.ac.ncl.icos.eaframework.operator;


import org.bson.types.ObjectId;
import uk.ac.ncl.icos.eaframework.Operator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;

/**
 *
 * @author owengilfellon
 */
public class OperatorGroup<T extends Chromosome> extends AbstractOperator<T> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(OperatorGroup.class);
   // private SVPManager m = SVPManager.getSVPManager();
    private List<Operator<T>> operators = new ArrayList<>();
    private Operator<T> o;
    private Iterator<Operator<T>> it = null;
    private final int MAX_ATTEMPTS = 10;
    private final double[] probabilities;
    private final boolean sigmaScaled;
    private final Class<T> chromosomeClass;

    private int MUTATIONS;

    public OperatorGroup(List<Operator<T>> operators, Class<T> chromosomeClass, int mutationsPerGeneration) {
        this.operators = operators;
        this.sigmaScaled = false;
        probabilities = null;
        this.MUTATIONS = mutationsPerGeneration;
        this.chromosomeClass = chromosomeClass;

    }
    
    public OperatorGroup(List<Operator<T>> operators,  Class<T> chromosomeClass, int mutationsPerGeneration, double[] probabilities, boolean sigmaScaled) {
        this.operators = operators;
        this.sigmaScaled = sigmaScaled;
        this.probabilities = probabilities != null && probabilities.length == operators.size() ? probabilities : null;
        this.MUTATIONS = mutationsPerGeneration;
        this.chromosomeClass = chromosomeClass;
    }


    public List<Operator<T>> getOperators() {
        return operators;
    }
    
    public double[] getWeights() {
        return probabilities;
    }

    @Override
    public T apply(T c) {
        
        T chromosome = c;
        T previousChromosome = null;
 
        boolean mutationOccured = false;
        int attempt = 0;
        previousChromosome = chromosome;

        for(int i = 0; i < MUTATIONS; i++) {
            while(!mutationOccured && attempt < MAX_ATTEMPTS)
            {
                o = chooseOperator();
                if(LOGGER.isInfoEnabled())
                    LOGGER.info("Attempt {}, applying {}", attempt + 1, o.getClass().getSimpleName());
                chromosome = o.apply(chromosome);
                if(chromosome.getId() != previousChromosome.getId()) {
                    mutationOccured = true;
                }
                attempt++;
            }

            if(!mutationOccured)
                LOGGER.warn("Could not apply operators after {} attempts", MAX_ATTEMPTS);
        }

        Chromosome dup = chromosome.duplicate(chromosome.getId(), new ObjectId[] { c.getId() } );

        return chromosomeClass.cast(dup);
    }
    
    private Operator<T> chooseOperator() {
        
        if(probabilities == null)
            return operators.get(new Random().nextInt(operators.size()));
        
        if(it == null)
            it = new RouletteWheelIterator(sigmaScaled, operators, probabilities);
        
        return it.next();
    }

    @Override
    public Operator<T> getOperator() {
        return o;
    }

    public boolean isSigmaScaled() {
        return sigmaScaled;
    }

    public int getMutationsPerGeneration() {
        return MUTATIONS;
    }

    public Class<T> getChromosomeClass() {
        return chromosomeClass;
    }

    class RouletteWheelIterator implements Iterator<Operator<T>> {

        private final boolean sigmaScaled;
        private double totalValue = 0;
        private final double totalSigma = 0;
        private double avgFitness = 0;
        private double variance = 0;
        private double sd = 0;
        private final List<WeightedOperator> unscaledOperators;
        private List<WeightedOperator> sigmaScaledOperators;
        private final Random r = new Random();

        public RouletteWheelIterator(boolean sigmaScaled, List<Operator<T>> operators, final double[] probabilities) {
            this.unscaledOperators = operators.stream()
                .map(o -> new WeightedOperator(o, probabilities[operators.indexOf(o)]))
                .collect(Collectors.toList());
            this.sigmaScaled = sigmaScaled;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public Operator<T> next() {
            
            if(sigmaScaledOperators == null) {
                sigmaScaledOperators = new ArrayList<>();
                calculateStats();
            }

            double rand = r.nextDouble();
            double ptr = (rand * totalValue);
            double ptr2 = 0.0;
            int modelIndex = 0;
            WeightedOperator c = null;

            while(ptr2 < ptr) {
                c = unscaledOperators.get(modelIndex);
                ptr2 += sigmaScaled ? sigmaScaledOperators.get(modelIndex).weight : c.weight;
                modelIndex++;
            }

            if(c != null)
                return c.operator;
            
            else return null;
        }
        
        private void calculateStats() {
            
            for(WeightedOperator weightedOperator:unscaledOperators) {
                totalValue += weightedOperator.weight;
            }

            avgFitness = ( totalValue / unscaledOperators.size() );

            if(sigmaScaled) {
                for(WeightedOperator weightedOperator:unscaledOperators) {
                    variance += Math.pow((weightedOperator.weight - avgFitness), 2);
                }

                sd = Math.sqrt(variance);

                for(WeightedOperator weightedOperator:unscaledOperators) {
                    double sigmaScaledValue = getSigmaScaledValue(weightedOperator, sd, avgFitness);
                    WeightedOperator sigmaScaledWeightedOperator = new WeightedOperator(weightedOperator.operator, sigmaScaledValue);
                    sigmaScaledOperators.add(sigmaScaledWeightedOperator);
                }

                totalValue = 0;

                for(WeightedOperator weightedOperator:sigmaScaledOperators) {
                    totalValue += weightedOperator.weight;
                }

                avgFitness = 0;

                avgFitness = ( totalValue / sigmaScaledOperators.size() );
            }
        }
        
        private double getSigmaScaledValue(WeightedOperator operator, double sd, double average)
        {

            double sigma = 0;
            if(sd != 0)
            {
                sigma =  1.0 + ((operator.weight - average) / (2.0 * sd));
            }
            else
            {
                sigma =  1.0;
            }

            return sigma;
        }
    }
    
    class WeightedOperator {
        
        final Operator<T> operator;
        final double weight;

        public WeightedOperator(Operator<T> operator, double weight) {
            this.operator = operator;
            this.weight = weight;
        }
    }
        /**/
    
}
