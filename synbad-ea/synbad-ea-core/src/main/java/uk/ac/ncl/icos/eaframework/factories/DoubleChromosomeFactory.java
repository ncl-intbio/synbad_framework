/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.factories;

import uk.ac.ncl.icos.eaframework.chromosome.DoubleChromosome;
import uk.ac.ncl.icos.eaframework.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
public class DoubleChromosomeFactory extends AbstractChromosomeFactory<DoubleChromosome> {
    
    private final int NUMBER_OF_INITIAL_MUTATIONS = 10;
    private final int CHROMOSOME_LENGTH = 0;

    public DoubleChromosomeFactory(List<DoubleChromosome> seeds, int POPULATON_SIZE) {
        super(seeds, POPULATON_SIZE);
    }
    
    @Override
    public List<DoubleChromosome> generatePopulation() {
        
        Random r = new Random();
        List<DoubleChromosome> population = new ArrayList<DoubleChromosome>();
        
        for(int i=0; i<POPULATON_SIZE; i++)
        {
            DoubleChromosome seed = seeds.get(r.nextInt(seeds.size()));
            double[] chromosome = seed.getChromosome();
            
            if(chromosome.length != CHROMOSOME_LENGTH)
            {
                throw new IllegalStateException("Chromosome length is not " + CHROMOSOME_LENGTH);
            }
            
            for(int j=0; j<NUMBER_OF_INITIAL_MUTATIONS; j++)
            {
                int index = r.nextInt(chromosome.length);
                chromosome[index] = r.nextDouble();
            }
            
            population.add(new DoubleChromosome(chromosome));
        }
        
        return population;
    }

    @Override
    public List<DoubleChromosome> generatePopulation(List<Operator<DoubleChromosome>> operators) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
