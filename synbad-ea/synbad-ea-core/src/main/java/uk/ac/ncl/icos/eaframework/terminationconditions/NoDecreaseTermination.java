/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "No Increase")
public class NoDecreaseTermination implements TerminationCondition, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(NoDecreaseTermination.class);

    private int generationOfLastDecrease = 0;
    private final int GENERATIONS_WITHOUT_DECREASE;
    private double bestFitness = Double.MAX_VALUE;

    public NoDecreaseTermination(int generationsWithoutDecrease) {
        this.GENERATIONS_WITHOUT_DECREASE = generationsWithoutDecrease;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addInt(GENERATIONS_WITHOUT_DECREASE)
                .build();
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        
        if(es.getBestFitness() < bestFitness)
        {
            bestFitness = es.getBestFitness();
            generationOfLastDecrease = es.getCurrentGeneration();
        }
        
        if((es.getCurrentGeneration() - generationOfLastDecrease) >= GENERATIONS_WITHOUT_DECREASE)
        {
            logger.info("No Fitness Decrease Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
}
