/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Sigma Scaled Roulette Wheel")
public class Truncation<T extends Chromosome> implements Selection<T>, Serializable {

    private final int truncationSize;

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
               .addInt(truncationSize).build();
    }
    
    public Truncation(int truncationSize) {
        this.truncationSize = truncationSize;
    }
    
    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) {
        return new TruncationIterator<>(population, truncationSize);
    }

    class TruncationIterator<T extends Chromosome> implements SelectionIterator<T> {

        private final List<EvaluatedChromosome<T>> sortedPopulation;
        private int index = 0;

        public TruncationIterator(List<EvaluatedChromosome<T>> population, int truncationSize) {
            List<EvaluatedChromosome<T>> filtered = population.stream().filter(e -> {
                    return e.getFitness() != null && 
                    !Double.isNaN(e.getFitness().getFitness()) ;
                }).sorted((x, y) -> { 
                    // switch order of x and y to get reverse ordering
                    return new Double(y.getFitness().getFitness()).compareTo(x.getFitness().getFitness()); })
                .collect(Collectors.toList());
            
            sortedPopulation = filtered.size() > truncationSize ?
                    filtered.subList(0, truncationSize) :
                    filtered;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {    
            EvaluatedChromosome<T> c = sortedPopulation.get(index);
            index = index < sortedPopulation.size() - 1 ? ++index : 0;
            return c;
        }
    }
}
