package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;

/**
 * Evolution Observers record the state of the Evolutionary Algorithm after 
 * the calculation of population fitness, but before the selection of the 
 * next generation.
 * 
 * EvolutionObservers are implemented using the observer design. They are
 * attached to classes that implement the EvolutionSubject interface, and
 * receive an EvolutionSubject object as an argument, which can be used to
 * derive information about the state of the object.
 * 
 * @author owengilfellon
 */
public interface EvolutionObserver<T extends Chromosome> {
    
    /**
     * Alerts the observer to a change in the subject, allowing the subject's
     * state to be accessed and used at a specific time. Examples of use include
     * exporting results to external files, or printing the progress of the
     * algorithm to console.
     * 
     * @param s An EvoEngine implementing the EvolutionSubject interface.
     */
    public <V extends EvoEngine<T>> void update(V s);
    
}
