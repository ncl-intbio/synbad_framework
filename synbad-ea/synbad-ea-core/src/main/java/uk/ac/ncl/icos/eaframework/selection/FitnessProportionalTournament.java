/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Sigma Scaled Roulette Wheel")
public class FitnessProportionalTournament<T extends Chromosome> implements Selection<T>, Serializable {

    private final int DEFAULT_TOURNAMENT_SIZE = 2;
    private final boolean SIGMA_SCALED; 
    private final Logger LOGGER = LoggerFactory.getLogger(FitnessProportionalTournament.class);
    private final int TOURNAMENT_SIZE;

    public FitnessProportionalTournament() {
        this.SIGMA_SCALED = true;
        this.TOURNAMENT_SIZE = DEFAULT_TOURNAMENT_SIZE;
    }

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addBoolean(SIGMA_SCALED)
                .addInt(TOURNAMENT_SIZE).build();
    }
    
    public FitnessProportionalTournament(int tournamentSize) {
        this.SIGMA_SCALED = true;
        this.TOURNAMENT_SIZE = tournamentSize;
    }
    
    public FitnessProportionalTournament(boolean sigmaScaled) {
        this.SIGMA_SCALED = sigmaScaled;
        this.TOURNAMENT_SIZE = DEFAULT_TOURNAMENT_SIZE;
    }
    
    public FitnessProportionalTournament(boolean sigmaScaled, int tournamentSize) {
        this.SIGMA_SCALED = sigmaScaled;
        this.TOURNAMENT_SIZE = tournamentSize;
    }
    
    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) {
        return new TournamentIterator<>(population);
    }

    class TournamentIterator<T extends Chromosome> implements SelectionIterator<T> {

        private final Random r = new Random();
        private final RouletteWheelIterator<T> i;

        public TournamentIterator(List<EvaluatedChromosome<T>> population) { 
            i = new RouletteWheelIterator<>(SIGMA_SCALED, population);
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {
        
            List<EvaluatedChromosome<T>> competitors = new ArrayList<>();

            while(competitors.size() < TOURNAMENT_SIZE) {
                competitors.add(i.next());
            }
            
            EvaluatedChromosome<T> winner = null;
            
            for(EvaluatedChromosome<T> competitor : competitors) {
                if(winner == null || competitor.getFitness().getFitness() > winner.getFitness().getFitness()) {
                    winner = competitor;
                }
            }
            
            return winner;
        }
    }
 
    private class RouletteWheelIterator<T extends Chromosome> implements SelectionIterator<T> {

        private final boolean sigmaScaled;
        private final List<EvaluatedChromosome<T>> population;
        private double totalFitness = 0;
        private final double totalSigma = 0;
        private double avgFitness = 0;
        private double variance = 0;
        private double sd = 0;
        private List<EvaluatedChromosome<T>> sigmaScaledFitnesses;
        private final Random r = new Random();

        public RouletteWheelIterator(boolean sigmaScaled, List<EvaluatedChromosome<T>> population) {
            this.population = population.stream().collect(Collectors.toList());
            this.sigmaScaled = sigmaScaled;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {
            if(sigmaScaledFitnesses == null) {
                sigmaScaledFitnesses = new ArrayList<>();
                calculateStats();
            }

            
            double rand = r.nextDouble();
            double ptr = (rand * totalFitness);
            double ptr2 = 0.0;
            int modelIndex = 0;
            EvaluatedChromosome<T> c = null;
            
            //System.out.println("Ptr: " + ptr + " | Ttl: " + totalFitness );

            while(ptr2 < ptr) {
                c = population.get(modelIndex);
                ptr2 += sigmaScaled ? sigmaScaledFitnesses.get(modelIndex).getFitness().getFitness() : c.getFitness().getFitness();
                modelIndex++;
            }

           // System.out.println("Selecting: " + c.getFitness().getFitness());
            
            return c;

        }
        
        private void calculateStats() {
            
            for(EvaluatedChromosome ec:population) {
                totalFitness += ec.getFitness().getFitness();
            }

            avgFitness = ( totalFitness / population.size() );

            if(sigmaScaled) {
                for(EvaluatedChromosome ec:population) {
                    variance += Math.pow((ec.getFitness().getFitness() - avgFitness), 2);
                }

                sd = Math.sqrt(variance);

                for(EvaluatedChromosome<T> ec:population) {
                    double sigma = getSigmaScaledFitness(ec, sd, avgFitness, totalSigma);
                    EvaluatedChromosome<T> ec2 = new EvaluatedChromosome(ec.getChromosome(), new Fitness(sigma), null);
                    sigmaScaledFitnesses.add(ec2);
                }

                totalFitness = 0;

                for(EvaluatedChromosome<T> ec:sigmaScaledFitnesses) {
                    totalFitness += ec.getFitness().getFitness();
                }

                avgFitness = 0;

                avgFitness = ( totalFitness / sigmaScaledFitnesses.size() );
            }
        }
        
        private double getSigmaScaledFitness(EvaluatedChromosome<T> ec, double sd, double avgFitness, double totalSigma)
        {

            double sigma = 0;
            if(sd != 0)
            {
                sigma =  1.0 + ((ec.getFitness().getFitness() - avgFitness) / (2.0 * sd));
            }
            else
            {
                sigma =  1.0;
            }

            totalSigma += sigma;

            return sigma;
        }
    }
}
