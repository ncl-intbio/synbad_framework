/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import java.util.List;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

/**
 *
 * @author owengilfellon
 */
public interface PopulationProcess<T extends Chromosome> {

    List<EvaluatedChromosome<T>> getPopulation();

    List<EvaluatedChromosome<T>> nextPopulation(List<EvaluatedChromosome<T>> population);

    Selection<T> getSelection();

    Operator<T> getOperator();

    int getPopulationSize();
    
}
