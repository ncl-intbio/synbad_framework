/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.evoengines;

import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.stats.PopulationStatGenerator;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.TerminationCondition;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Strategy;

/**
 *
 * @author owengilfellon
 */
public abstract class AbstractEvoEngine<T extends Chromosome> implements EvoEngine<T>, Serializable {
    
    private final Logger LOGGER = LoggerFactory.getLogger(AbstractEvoEngine.class);

    protected List<EvolutionObserver<T>> observers = new ArrayList<>();
    protected Evaluator e;
    protected ChromosomeFactory cf;
    protected TerminationCondition t;
    protected Strategy<T> evolutionStrategy;
    
    // change population to a class of it's own
    
    protected List<T> population = new ArrayList<>();

    protected List<EvaluatedChromosome<T>> evaluatedReproductionPopulation = new ArrayList<>();
    protected List<EvaluatedChromosome<T>> evaluatedSurvivalPopulation = new ArrayList<>();
    protected int currentGeneration = 0;
    protected Long duration = 0L;

    protected PopulationStatGenerator statGenerator;

    protected int POOL_SIZE;

    public AbstractEvoEngine(ChromosomeFactory<T> cf, Strategy<T> strategy, Evaluator<T> e, TerminationCondition t, int poolSize) {
        this.evolutionStrategy = strategy;
        this.cf = cf;
        this.e = e;
        this.t = t;
        this.POOL_SIZE = poolSize;
        this.statGenerator = new PopulationStatGenerator.IncreasingFitnessStatGenerator();
    }

    public AbstractEvoEngine(ChromosomeFactory<T> cf, Strategy<T> strategy, Evaluator<T> e, TerminationCondition t, PopulationStatGenerator statGenerator, int poolSize) {
        this(cf, strategy, e, t, poolSize);
        this.statGenerator = statGenerator;
    }

    @Override
    public boolean isFitnessFunctionMinimiser() {
        return PopulationStatGenerator.DecreasingFitnessStatGenerator.class.isAssignableFrom(statGenerator.getClass());
    }

    @Override
    public int getThreadPoolSize() {
        return POOL_SIZE;
    }

    @Override
    public void setPoolSize(int poolSize) {
        this.POOL_SIZE = poolSize;
    }

    @Override
    public PopulationStats<T> getPopulationStats() {
        return this.statGenerator.getPopulationStats(evaluatedSurvivalPopulation, currentGeneration, duration);
    }
    
    @Override
    public List<T> getPopulation()
    {
        return population;// evaluatedSurvivalPopulation.stream().map(e -> e.getChromosome()).collect(Collectors.toList());
    }


    @Override
    public TerminationCondition getTerminationCondition() {
        return t;
    }


    @Override
    public Evaluator<T> getFitnessEvaluator()
    {
        return e;
    }
    
    @Override
    public Strategy<T> getEvolutionaryStrategy() {
        return evolutionStrategy;
    }
        
    @Override
    public void stepForward() {
   
        currentGeneration++;
        
        LOGGER.debug("Stepping forward to generation " + currentGeneration);
        
        if(evaluatedSurvivalPopulation != null && evaluatedSurvivalPopulation.size() > 0 ){
            population.clear();
        }

        LocalDateTime startTime = LocalDateTime.now();
        
        do {
            evaluatedSurvivalPopulation = evolutionStrategy.nextPopulation(evaluatedSurvivalPopulation).stream()
                    //.map(p -> p.duplicate(ChromosomeIdGenerator.getId(), p.getChromosome().getParentIds())).collect(Collectors.toList());
                    .map(EvaluatedChromosome::duplicate).collect(Collectors.toList());
        } while (evaluatedSurvivalPopulation == null || evaluatedSurvivalPopulation.size() < evolutionStrategy.getPopulationSize());
        
        population = evaluatedSurvivalPopulation.stream().map(p -> p.getChromosome()).collect(Collectors.toList());

        LocalDateTime endTime = LocalDateTime.now();

        this.duration = ChronoUnit.MILLIS.between(startTime, endTime);

        alert();
    }

    /**
     * Runs the algorithm by repeatedly calling stepForward() until a termination condition is met.
     * @return The population, current as of when the termination conditions are met.
     */
    @Override
    public List<T> run()
    {
        LOGGER.info("Run");
        
        do
        {
            stepForward();
        }
        while(!t.shouldTerminate(getPopulationStats()));
        
        return population;
    }

    @Override
    public void alert() {
        for(EvolutionObserver observer:observers)
        {
            observer.update(this);
        }
    }

    
    @Override
     public <V extends EvolutionObserver> void attach(V o) {
        observers.add(o);
    }

    @Override
    public <V extends EvolutionObserver> void dettach(V d) {
        observers.remove(d);
    }

    @Override
    public List<EvolutionObserver<T>> getObservers() {
        return new LinkedList<>(observers);
    }
}
