package uk.ac.ncl.icos.eaframework.observer;

import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.filemanagement.Export;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class StringResultsFileWriter implements EvolutionObserver<Chromosome> {

    String timestamp;
    private static final Logger logger = LoggerFactory.getLogger(StringResultsFileWriter.class);
    
    
    public StringResultsFileWriter() {
         DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmssSSS");
         timestamp = dateFormat.format(new Date()); 
    }

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s) {
        EvoEngine e = (EvoEngine) s;
        PopulationStats stats = e.getPopulationStats();
        ArrayList<Double> fitnesses = new ArrayList<>();
        fitnesses.add(stats.getMeanFitness());

            if (!Export.exportDoubles(null,
                    "Results",
                    "StringExperiments",
                    "Experiment_" + timestamp,
                    "AllFitnesses_" + timestamp + ".txt",
                    fitnesses,
                    true)) {  logger.error("Cannot export all fitness results");}
         
    }
}
