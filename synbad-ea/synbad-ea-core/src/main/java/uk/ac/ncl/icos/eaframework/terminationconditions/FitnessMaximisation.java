/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Function Maximisation")
public class FitnessMaximisation implements TerminationCondition, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(FitnessMaximisation.class);
    
    double fitness;
    
    public FitnessMaximisation(double fitness) {
        this.fitness = fitness;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addDouble(fitness)
                .build();
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        if(es.getBestFitness() >= fitness )
        {
            logger.info("Max Fitness Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    
}
