/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Mutant Champ")
public class MutantChamp<T extends Chromosome> implements Selection<T>{

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .build();
    }

    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) { 
       return new MutantChampIterator<>(population);
    }
    
    class MutantChampIterator<T extends Chromosome> implements SelectionIterator<T> {
        
        private List<EvaluatedChromosome<T>> population = new ArrayList<>();
        private EvaluatedChromosome<T> bestChromosome;

        public MutantChampIterator(List<EvaluatedChromosome<T>> population) {
            this.population = population.stream().collect(Collectors.toList());
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {
            
            if(bestChromosome==null) {
                for(EvaluatedChromosome<T> c : population) {
                    if(bestChromosome == null)
                        bestChromosome = c;
                    if(c.getFitness().getFitness() > bestChromosome.getFitness().getFitness())
                        bestChromosome = c;
                }
            }
            
            
            return bestChromosome;
        }

    }

}
