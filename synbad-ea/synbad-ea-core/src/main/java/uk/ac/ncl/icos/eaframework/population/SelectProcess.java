/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.population;

import uk.ac.ncl.icos.eaframework.PopulationProcess;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.ChromosomeIdGenerator;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;

/**
 *
 * @author owengilfellon
 */
public class SelectProcess<T extends Chromosome> implements PopulationProcess<T> {

    private final Logger logger = LoggerFactory.getLogger(SelectProcess.class);
    private final int POPULATION_SIZE;
    private final Selection<T> selection;
    private List<EvaluatedChromosome<T>> population;
    
    public SelectProcess(Selection<T> selection, int population) {
        this.selection = selection;
        this.POPULATION_SIZE = population;
        this.population = Collections.EMPTY_LIST;
    }

    @Override
    public List<EvaluatedChromosome<T>> getPopulation() {
        return new LinkedList<>(population);
    }

    @Override
    public List<EvaluatedChromosome<T>> nextPopulation(List<EvaluatedChromosome<T>> population) {
        
        logger.debug("Selecting from {} with: [{}]", population != null? population.size() : "null", selection.getClass().getSimpleName());

        List<EvaluatedChromosome<T>> populationFutures = new ArrayList<>();
        
        try {
            SelectionIterator<T> it = selection.select(population);
        
            while(populationFutures.size() < POPULATION_SIZE) {
                EvaluatedChromosome<T> c = it.next();
                if(c!=null) {
                    //populationFutures.add(c.duplicate(ChromosomeIdGenerator.getId(), c.getChromosome().getParentIds() ));
                    populationFutures.add(c.duplicate());
                } else {
                    logger.warn("Evaluated chromosome is null");
                }
            }
            
            logger.debug("Selected " + populationFutures.size() + " individuals");
            this.population = populationFutures;
            return getPopulation();
        } catch(Exception e) {
            logger.warn("Could not get population: " + e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public Selection<T> getSelection() {
        return selection;
    }

    @Override
    public Operator<T> getOperator() {
        return null;
    }

    @Override
    public int getPopulationSize() {
        return POPULATION_SIZE;
    }

}
