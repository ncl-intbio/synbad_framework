/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.synbad.core.util.Configured;

import java.io.Serializable;
import java.util.function.Function;

/**
 *
 * @author owengilfellon
 */
public interface Constraint<T, V> extends Function<T, V>, Serializable, Configured {
 
    public Double getPenaltyFactor();
    
    public void setPenaltyFactory(Double penaltyFactor);
    
    public abstract class AConstraint<T, V> implements Constraint<T, V> {
        
        private Double penaltyFactor = 1.0;

        @Override
        public Double getPenaltyFactor() {
            return penaltyFactor;
        }

        @Override
        public void setPenaltyFactory(Double penaltyFactor) {
            this.penaltyFactor = penaltyFactor;
        }
    }
}
