/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.population;

import org.sbml.jsbml.SBMLDocument;
import uk.ac.ncl.icos.eaframework.PopulationProcess;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvaluatedChromosomeFactory;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class SelectAndMutateProcess<T extends Chromosome> implements PopulationProcess<T> {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(SelectAndMutateProcess.class);
    
    private final int POOL_SIZE;
    private final int POPULATION_SIZE;
    private final int MUTATIONS_PER_GENERATION;

    private final ExecutorService pool;

    private final Selection<T> selection;
    private final Operator<T> operator;
    private final Evaluator<T> evaluator;
    private final EvaluatedChromosomeFactory<T> chromFactory;

    private List<EvaluatedChromosome<T>> population;

    public SelectAndMutateProcess(Selection<T> selection, Operator<T> operator, Evaluator<T> evaluator, EvaluatedChromosomeFactory<T> chromFactory,  int population, int mutationsPerGeneration, int poolSize) {
        this.selection = selection;
        this.operator = operator;
        this.evaluator = evaluator;
        this.chromFactory = chromFactory;
        this.POPULATION_SIZE = population;
        this.MUTATIONS_PER_GENERATION = mutationsPerGeneration;       
        this.POOL_SIZE = poolSize;
        this.population = Collections.EMPTY_LIST;
        this.pool  = new ThreadPoolExecutor( POOL_SIZE, POOL_SIZE, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    } 

    private EvaluatedChromosome<T>[] process(ResourcePool<EvaluatedChromosome<T>> selectionPool, boolean mutate) {
        

        AtomicInteger completion = new AtomicInteger(0);
        AtomicInteger failure = new AtomicInteger(0);
        
        // Store evolved population on completion

        List<EvaluatedChromosome<T>> processed = new LinkedList<>();
        
        // Create iterator over population that will apply given selection strategy
   
        while(completion.get() < POPULATION_SIZE) {
            
            if(!selectionPool.isEmpty()) {
   
                EvaluatedChromosome<T> c = selectionPool.acquire();

                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("Acquired: [ {} ]", c);

                CompletableFuture<EvaluatedChromosome<T>> cf = CompletableFuture.supplyAsync(()-> {

                    T o = c.getChromosome();
                    synchronized (SelectAndMutateProcess.class) {
                        if(mutate) {
                            o = operator.apply(o);
                            if (LOGGER.isTraceEnabled())
                                LOGGER.trace("Mutated: [ {} ]", o);
                        }
                    }

                    synchronized (SelectAndMutateProcess.class) {
                        Fitness f = evaluator.evaluate(o);
                        if(f == null || Double.isNaN(f.getFitness())) {
                            throw new NullPointerException("Evaluation error: fitness is null");
                        }

                        EvaluatedChromosome<T> ec = chromFactory.getEvaluated(o, f);

                        if(LOGGER.isTraceEnabled())
                            LOGGER.trace("Adding to child population: [ {} ]", o);
                        return ec;
                    }



                }, pool);

                // need to change failures to exceptions

                cf.exceptionally(e -> {
                    LOGGER.error("Could not evaluate: {} | {}", c.toString(), e.getCause().getMessage());
                    e.printStackTrace();
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("ERROR! Reinserted: [ {} ]", c);
                    selectionPool.insert(c);
                    failure.incrementAndGet();
                    return null;
                });

                cf.thenAcceptAsync(ch -> {

                    // System.out.println("Done");
                    processed.add(ch);
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("Added to population: [ {} ]", ch.getChromosome().toString());
                    completion.incrementAndGet();
                }, pool);
            }
        } 

        if(failure.get() > 0)
            LOGGER.warn("Returning from selection with {} failures", failure.get());
        
        return processed.toArray(new EvaluatedChromosome[] {});
    }

    @Override
    public List<EvaluatedChromosome<T>> getPopulation() {
        return new LinkedList<>(population);
    }

    @Override
    public List<EvaluatedChromosome<T>> nextPopulation(List<EvaluatedChromosome<T>> population) {

        // Create iterator over population that will apply given selection strategy
        SelectionIterator<T> it = selection.select(population);
        ResourcePool<EvaluatedChromosome<T>> selectionPool = new ResourcePool<>(POPULATION_SIZE);
        
        for(int i = 0; i < POPULATION_SIZE; i++) {
            selectionPool.insert(it.next());
        }
        
        List<EvaluatedChromosome<T>> populationFutures =  new LinkedList<>(Arrays.asList(process(selectionPool, true)));
        this.population = populationFutures;
        return getPopulation();
    }

    @Override
    public Selection<T> getSelection() {
         return selection;
    }

    @Override
    public Operator<T> getOperator() {
        return operator;
    }
    
    @Override
    public int getPopulationSize() {
        return POPULATION_SIZE;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        pool.shutdown();
    }
}
