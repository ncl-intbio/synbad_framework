/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Generation Limit")
public class GenerationTermination implements TerminationCondition, Serializable {
    
    private int maxGenerations;
    private static final Logger logger = LoggerFactory.getLogger(GenerationTermination.class);


    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addInt(maxGenerations)
                .build();
    }
    public GenerationTermination(int maxGenerations) {
        this.maxGenerations = maxGenerations;
    }

    public int getMaxGenerations() {
        return maxGenerations;
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        if(es.getCurrentGeneration() >= maxGenerations)
        {
            logger.info("Generation Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
}
