/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

import org.bson.types.ObjectId;
import uk.ac.ncl.icos.eaframework.Chromosome;

/**
 *
 * @author owengilfellon
 */
public class DoubleChromosome implements Chromosome {
    
    private final double[] chromosome;
    private ObjectId id;
    private ObjectId parentId;

    public DoubleChromosome(double[] chromosome) {
        this.chromosome = chromosome;
        this.id = new ObjectId();
        this.parentId = null;
    }

    public DoubleChromosome(double[] chromosome, ObjectId parentId) {
        this.chromosome = chromosome;
        this.parentId = parentId;
        this.id = new ObjectId();
    }

    public DoubleChromosome(double[] chromosome, ObjectId parentId, ObjectId id) {
        this.chromosome = chromosome;
        this.parentId = parentId;
        this.id = id;
    }

    @Override
    public ObjectId getId() {
        return id;
    }

    @Override
    public ObjectId[] getParentIds() {
        return parentId != null ? new ObjectId[] { parentId } : new ObjectId[]{};
    }

    public double[] getChromosome() {
        return chromosome;
    }

    @Override
    public Chromosome duplicate(ObjectId id, ObjectId[] parentIds) {
        return new DoubleChromosome(chromosome.clone(), id, parentIds[0]);
    }

    @Override
    public Chromosome duplicate() {
        return new DoubleChromosome(chromosome.clone(), getId());
    }

    
}
