
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.util.List;

/**
 * Defines operations on an Evolutionary Algorithm
 * @author owengilfellon
 * @param <T> The Chromosome type that the Evolutionary Algorithm is to operate on.     Operators, FitnessEvaluators should be identically typed.
 */
public interface EvoEngine<T extends Chromosome> {

    /**
     * Starts the Evolutionary Algorithm, by repeatedly calling stepForward until
     * a termination condition is met.
     * @return The fittest Chromosome evolved during the evolution process.
     */
    List<T> run();

    /**
     * Moves forward one generation by applying evolutionary Operators, and running
     * selection. Termination conditions are not checked.
     */
    void stepForward();
    
    /**
     * Returns statistics for the most current generation - includes the best 
     * chromosome, the best fitness, the current generation, the population size, 
     * and the mean fitness of the population.
     * @return An object containing the statistics
     */
    PopulationStats<T> getPopulationStats();
    
    /**
     * Retrieves the entire population.
     * @return 
     */
    List<T> getPopulation();

    <V extends EvaluatedChromosome<T>> List<V> getEvaluatedReproductionPopulation();

    <V extends EvaluatedChromosome<T>> List<V> getEvaluatedSurvivalPopulation();

    Strategy<T> getEvolutionaryStrategy();

    List<EvolutionObserver<T>> getObservers();
    /**
     * Returns the Evaluator passed to the Evolutionary Algorithm at
     * construction.
     * @return 
     */
    <V extends Evaluator<T>> V getFitnessEvaluator();

     boolean isFitnessFunctionMinimiser();
    
    TerminationCondition getTerminationCondition();

    int getThreadPoolSize();

    void setPoolSize(int poolSize);
    
    <V extends EvolutionObserver> void attach(V o);

    <V extends EvolutionObserver> void dettach(V d);

    void alert();
}
