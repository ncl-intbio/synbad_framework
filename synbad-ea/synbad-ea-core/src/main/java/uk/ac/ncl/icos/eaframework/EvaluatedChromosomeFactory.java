/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

/**
 *
 * @author owengilfellon
 * @param <T> The type of the chromosome that is being evaluated
 * @param <V> The type of the evaluated chromosome that is returned
 */
public interface EvaluatedChromosomeFactory<T extends Chromosome> {
    
    EvaluatedChromosome<T> getEvaluated(T chromosome, Fitness fitness);
    
}
