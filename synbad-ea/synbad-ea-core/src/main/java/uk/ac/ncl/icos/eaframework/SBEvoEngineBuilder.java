/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;

import java.util.List;

/**
 *
 * @author owengilfellon
 * @param <T>
 */
public interface SBEvoEngineBuilder<T extends Chromosome> {
    
    // Evolution model

    SBEvoEngineBuilder<T> setChromosomeFactory(ChromosomeFactory<T> factory);
    
    SBEvoEngineBuilder<T> setEvaluator(Evaluator<T> evaluator);
    
    SBEvoEngineBuilder<T> setOverlapping(boolean overlapping);

    /**
     * Sets the generation for restarting an incomplete experiment
     * @param generation
     * @return
     */
    SBEvoEngineBuilder<T> setCurrentGeneration(int generation);

    /**
     * Sets the population for restarting an incomplete experiment
     * @param population
     * @return
     */
    SBEvoEngineBuilder<T> setCurrentPopulation(List<EvaluatedChromosome<T>> population);



    /**
     * If set to true, the population will be used to perform evolution of single candidates
     * in parallel (i.e. parallel mutant champ). If false, then the population will be used as a batch of
     * competing candidates.
     * @param ordered
     * @return
     */
    SBEvoEngineBuilder<T> setOrderedPopulations(boolean ordered);

    SBEvoEngineBuilder<T> setOrderedPopulations(boolean ordered, boolean minimiseFitness);
    
    SBEvoEngineBuilder<T> addTerminationCondition(TerminationCondition condition);
    
    SBEvoEngineBuilder<T> setReproductionPopulationSize(int populationSize);
    
    SBEvoEngineBuilder<T> setReproductionOperators(OperatorGroup operators);
    
    SBEvoEngineBuilder<T> setReproductionSelection(Selection<T> selection);
    
    SBEvoEngineBuilder<T> setSurvivalPopulation(int populationSize);
    
    SBEvoEngineBuilder<T> setSurvivalOperators(OperatorGroup operators);
    
    SBEvoEngineBuilder<T> setSurvivalSelection(Selection<T> selection);

    SBEvoEngineBuilder<T> setReproductionMutationsPerGeneration(int mutations);

    SBEvoEngineBuilder<T> setSurvivalMutationsPerGeneration(int mutations);

    SBEvoEngineBuilder<T> setThreads(int threads);
    
    EvoEngine<T> build();
    
    
    
}
