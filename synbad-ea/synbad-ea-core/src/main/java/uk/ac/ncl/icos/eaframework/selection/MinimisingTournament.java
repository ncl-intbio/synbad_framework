/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author owengilfellon
 */
public class MinimisingTournament<T extends Chromosome> implements Selection<T>, Serializable {

    private final int DEFAULT_TOURNAMENT_SIZE = 2;
    private final int TOURNAMENT_SIZE;
    private final Logger LOGGER = LoggerFactory.getLogger(MinimisingTournament.class);

    public MinimisingTournament() {
        this.TOURNAMENT_SIZE = DEFAULT_TOURNAMENT_SIZE;
    }

    public MinimisingTournament(int tournamentSize) {
        this.TOURNAMENT_SIZE = tournamentSize;
    }

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addInt(TOURNAMENT_SIZE)
                .build();
    }

    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) {
        if(population.size() < TOURNAMENT_SIZE) {
            LOGGER.error("Population is smaller than tournament size");
        }
        return new TournamentIterator<>(population);
    }

    class TournamentIterator<T extends Chromosome> implements SelectionIterator<T> {

        private final Set<EvaluatedChromosome<T>> population;
        private final Random r = new Random();

        public TournamentIterator(List<EvaluatedChromosome<T>> population) {
            this.population = new HashSet<>(population);
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {

            List<EvaluatedChromosome<T>> potentials = new LinkedList<>(this.population);
            Collections.shuffle(potentials);
            Iterator<EvaluatedChromosome<T>> it = potentials.iterator();
            List<EvaluatedChromosome<T>> competitors = new ArrayList<>();

            while(competitors.size() < TOURNAMENT_SIZE) {
                if(!it.hasNext())
                   it = potentials.iterator();
                competitors.add(it.next());
            }

            EvaluatedChromosome<T> winner = null;

            for(EvaluatedChromosome<T> competitor : competitors) {
                if(winner == null || competitor.getFitness().getFitness() < winner.getFitness().getFitness()) {
                    winner = competitor;
                }
            }

            return winner;
        }
    }
    
}
