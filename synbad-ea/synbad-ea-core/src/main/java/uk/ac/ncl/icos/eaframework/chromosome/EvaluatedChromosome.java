/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

import org.bson.types.ObjectId;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Operator;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public class EvaluatedChromosome<T extends Chromosome> implements Serializable {
    
    protected T c;
    private Fitness f;
    protected Operator o;
    
    public EvaluatedChromosome(T c, Fitness f, Operator o) {
        this.c = (T)c;
        this.f = f;
        this.o = o;
    }

    public Fitness getFitness(){
        return f;
    }
    
    public T getChromosome(){
        return c;
    }
    
    public String getOperator(){
        if(o!=null)
        {
            return o.getOperator().getClass().getSimpleName();
        }
        else return null; 
    }

    public EvaluatedChromosome<T> duplicate(ObjectId id, ObjectId[] parentIds) {
        return new EvaluatedChromosome<>((T)c.duplicate(id, parentIds), f, o);
    }

    public EvaluatedChromosome<T> duplicate() {
        return new EvaluatedChromosome<>((T)c, f, o);
    }

    @Override
    public String toString() {
        return c.toString() + " [" + f + "]";
    }
    
    
}
