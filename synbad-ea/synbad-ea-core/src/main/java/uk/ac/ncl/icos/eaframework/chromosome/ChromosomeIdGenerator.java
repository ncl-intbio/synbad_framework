package uk.ac.ncl.icos.eaframework.chromosome;

import uk.ac.ncl.icos.eaframework.Chromosome;

import java.util.Date;
import java.util.Objects;
import java.util.Random;

/**
 * Provides a unique identity for chromosomes
 */
public class ChromosomeIdGenerator {
    private static final Date d = new Date();
    private static final Random r = new Random();

    public synchronized  static long getId() {
            return d.getTime() * r.nextInt();
    }
}
