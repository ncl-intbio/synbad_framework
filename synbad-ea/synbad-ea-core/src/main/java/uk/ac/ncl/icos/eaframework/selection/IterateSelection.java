/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Uniform")
public class IterateSelection<T extends Chromosome> implements Selection<T>, Serializable {
    
    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) {
        return new LoopingIterator<>(population);
    }

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .build();
    }

    class LoopingIterator<T extends Chromosome> implements SelectionIterator<T> {

        private final List<EvaluatedChromosome<T>> population;
        private int index;

        public LoopingIterator(List<EvaluatedChromosome<T>> population) {
            this.population = population.stream().collect(Collectors.toList());
            this.index = 0;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public EvaluatedChromosome<T> next() {        
            index = (index < population.size() - 1) ? index++ : 0;
            return population.get(index);
        }
    }
    
}
