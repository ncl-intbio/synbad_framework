/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.factories;

import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Operator;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public abstract class AbstractChromosomeFactory<T extends Chromosome> implements ChromosomeFactory<T>, Serializable {

    protected final List<T> seeds;
    protected final int POPULATON_SIZE;

    public AbstractChromosomeFactory(List<T> seeds, int POPULATON_SIZE) {
        this.seeds = seeds;
        this.POPULATON_SIZE = POPULATON_SIZE;
    }

    @Override
    public int getPopulationSize() {
        return POPULATON_SIZE;
    }
    
    @Override
    abstract public List<T> generatePopulation();

    @Override
    abstract public List<T> generatePopulation(List<Operator<T>> operators);
}
