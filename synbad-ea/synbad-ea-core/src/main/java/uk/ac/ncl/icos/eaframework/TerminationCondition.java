/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.synbad.core.util.Configured;

/**
 *
 * @author owengilfellon
 */
public interface TerminationCondition extends Configured {
    
    public boolean shouldTerminate(PopulationStats es);
    
}
