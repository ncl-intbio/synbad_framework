/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.selection;

import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "Uniform")
public class PopulationIteratorSelection<T extends Chromosome> implements Selection<T>, Serializable {

    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .build();
    }


    @Override
    public SelectionIterator<T> select(List<EvaluatedChromosome<T>> population) {
        return new PopulationIterator<>(population);
    }



    class PopulationIterator<T extends Chromosome> implements SelectionIterator<T> {


        private final Random r = new Random();
        private final Iterator<EvaluatedChromosome<T>> i;
        
        public PopulationIterator(List<EvaluatedChromosome<T>> population) {
            this.i = population.iterator();
        }

        @Override
        public boolean hasNext() {
            return i.hasNext();
        }

        @Override
        public EvaluatedChromosome<T> next() {        
            return i.next();
        }
    }
    
}
