/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.stats;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface PopulationStatGenerator {

   <T extends Chromosome> PopulationStats<T> getPopulationStats(
       List<EvaluatedChromosome<T>> population,
       int generation,
       long duration);

   public class IncreasingFitnessStatGenerator implements PopulationStatGenerator {

       @Override
       public <T extends Chromosome> PopulationStats<T> getPopulationStats(List<EvaluatedChromosome<T>> population, int generation, long duration) {
           double meanfitness = 0.0;

           EvaluatedChromosome<T> best = null;

           for(EvaluatedChromosome<T> c: population) {
               if(best == null ||  c.getFitness().getFitness() > best.getFitness().getFitness()) {
                   best = c;
               }

               meanfitness += c.getFitness().getFitness();
           }

           meanfitness /= population.size();

           return new PopulationStats(best.getChromosome(),
                   generation,
                   population.size(),
                   meanfitness,
                   best.getFitness().getFitness(),
                   duration);
       }
   }

    public class DecreasingFitnessStatGenerator implements PopulationStatGenerator {

        @Override
        public <T extends Chromosome> PopulationStats<T> getPopulationStats(List<EvaluatedChromosome<T>> population, int generation, long duration) {
            double meanfitness = 0.0;

            EvaluatedChromosome<T> best = null;

            for(EvaluatedChromosome<T> c: population) {
                if(best == null ||  c.getFitness().getFitness() < best.getFitness().getFitness()) {
                    best = c;
                }

                meanfitness += c.getFitness().getFitness();
            }

            meanfitness /= population.size();

            return new PopulationStats(best.getChromosome(),
                    generation,
                    population.size(),
                    meanfitness,
                    best.getFitness().getFitness(),
                    duration);
        }
    }
}
