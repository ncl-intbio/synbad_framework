/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.chromosome;

import org.bson.types.ObjectId;
import uk.ac.ncl.icos.eaframework.Chromosome;

/**
 *
 * @author owengilfellon
 */
public class StringChromosome implements Chromosome {
    
    private final String chromosome;

    private ObjectId id;
    private ObjectId parentId;

    public StringChromosome(String chromosome) {
        this.chromosome = chromosome;
        this.id = new ObjectId();
        this.parentId = null;
    }

    public StringChromosome(String chromosome, ObjectId parentId) {
        this.chromosome = chromosome;
        this.id = new ObjectId();
        this.parentId = parentId;
    }

    public StringChromosome(String chromosome, ObjectId id, ObjectId parentId) {
        this.chromosome = chromosome;
        this.id = id;
        this.parentId = parentId;
    }

    @Override
    public ObjectId getId() {
        return id;
    }

    @Override
    public ObjectId[] getParentIds() {
        return parentId != null ? new ObjectId[] {parentId} : new ObjectId[]{};
    }

    public String getString()
    {
        return chromosome;
    }


    @Override
    public Chromosome duplicate(ObjectId id, ObjectId[] parentIds) {
        return new StringChromosome(chromosome, id, parentIds[0]);
    }

    @Override
    public Chromosome duplicate() {
        return new StringChromosome(chromosome, getId());
    }



}
