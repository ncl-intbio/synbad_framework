/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.stats;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class PopulationStats<T extends Chromosome> implements Serializable{
       
    private T bestChromosome;
    private int currentGeneration;
    private int populationSize;
    private double meanFitness;
    private double bestFitness;
    private long duration;

    public static <T extends Chromosome> PopulationStats<T> getPopulationStats(List<EvaluatedChromosome<T>> population, int generation, long duration) {
        double meanfitness = 0.0;

        EvaluatedChromosome<T> best = null;

        for(EvaluatedChromosome<T> c: population) {

            // LOGGER.debug("Processing: {}:{}", c.getFitness().getFitness(), c.getChromosome());

            if(best == null ||  c.getFitness().getFitness() > best.getFitness().getFitness()) {
                best = c;
            }

            meanfitness += c.getFitness().getFitness();
        }

        meanfitness /= population.size();

        return new PopulationStats(best.getChromosome(),
                generation,
                population.size(),
                meanfitness,
                best.getFitness().getFitness(),
                duration);
    }


    public PopulationStats(T bestChromosome, int currentGeneration, int populationSize, double meanFitness, double bestFitness, long duration) {
        this.bestChromosome = bestChromosome;
        this.currentGeneration = currentGeneration;
        this.populationSize = populationSize;
        this.meanFitness = meanFitness;
        this.bestFitness = bestFitness;
        this.duration = duration;
    }
   
    /**
     * @return the bestChromosome
     */
    public T getBestChromosome() {
        return bestChromosome;
    }

    /**
     * @return the currentGeneration
     */
    public int getCurrentGeneration() {
        return currentGeneration;
    }

    /**
     * @return the populationSize
     */
    public int getPopulationSize() {
        return populationSize;
    }

    /**
     * @return the meanFitness
     */
    public double getMeanFitness() {
        return meanFitness;
    }

    /**
     * @return the bestFitness
     */
    public double getBestFitness() {
        return bestFitness;
    }

    public long getDuration() { return duration; }
   
}
