/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
@EAModule( visualName = "No Increase")
public class NoIncreaseTermination implements TerminationCondition, Serializable {
    
    private static final Logger logger = LoggerFactory.getLogger(NoIncreaseTermination.class);
    
    private int generationOfLastIncrease = 0;
    private final int GENERATIONS_WITHOUT_INCREASE;
    private double bestFitness = 0.0;

    public NoIncreaseTermination(int generationsWithoutIncrease) {
        this.GENERATIONS_WITHOUT_INCREASE = generationsWithoutIncrease;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addInt(GENERATIONS_WITHOUT_INCREASE)
                .build();
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        
        if(es.getBestFitness() > bestFitness)
        {
            bestFitness = es.getBestFitness();
            generationOfLastIncrease = es.getCurrentGeneration();
        }
        
        if((es.getCurrentGeneration() - generationOfLastIncrease) >= GENERATIONS_WITHOUT_INCREASE)
        {
            logger.info("No Fitness Increase Termination");
            return true;
        }
        else
        {
            return false;
        }
    }
}
