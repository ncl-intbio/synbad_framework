package uk.ac.ncl.icos.eaframework.observer;

import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class SimplePopObserver<T extends Chromosome> implements EvolutionObserver<T>, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(SimplePopObserver.class);
    
    
    public SimplePopObserver() {
    }

    @Override
    public <V extends EvoEngine<T>> void update(V s) {
       
            EvoEngine e = (EvoEngine) s;
            List<EvaluatedChromosome> pop = e.getEvaluatedSurvivalPopulation();
            PopulationStats ps = e.getPopulationStats();
           
            StringBuilder sb = new StringBuilder();

            Chromosome c = ps.getBestChromosome();

            sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
            sb.append("AvgFit: ").append(ps.getMeanFitness()).append(" | ");
            sb.append("[").append(c.getParentIds()[0]).append("]->")
                .append("[").append(c.getId()).append("] ")
                .append("Best Fit: ").append(ps.getBestFitness());
            logger.info(sb.toString());
    }
    
}
