/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.terminationconditions;

import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class TerminationGroup implements TerminationCondition, Serializable {
    
    List<TerminationCondition> t = new ArrayList<TerminationCondition>();

    @Override
    public Config getConfig() {
        return Config.create().build();
    }

    public TerminationGroup(List<TerminationCondition> t) {
        this.t.addAll(t);
    }

    public List<TerminationCondition> getConditions() {
        return t;
    }

    @Override
    public boolean shouldTerminate(PopulationStats es) {
        
        for(TerminationCondition tc:t)
        {
            if(tc.shouldTerminate(es))
            {
                
                return true;
            }
        }
        return false;
    }
    
}
