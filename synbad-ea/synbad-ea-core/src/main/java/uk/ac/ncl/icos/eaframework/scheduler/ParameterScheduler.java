package uk.ac.ncl.icos.eaframework.scheduler;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;

import java.io.Serializable;

/**
 * Created by owengilfellon on 06/03/2014.
 */
public class ParameterScheduler<T extends Chromosome>  implements EvolutionObserver<T>, Serializable {

    private final int startGeneration;
    private final int endGeneration;
    private final double startParameter;
    private final double endParameter;
    private int currentGeneration = 1;
    private double parameter;

    public ParameterScheduler(int startGeneration, int endGeneration, double startParameter, double endParameter) {
        this.startGeneration = startGeneration;
        this.endGeneration = endGeneration;
        this.startParameter = startParameter;
        this.endParameter = endParameter;
        this.parameter = startParameter;
    }

    public double getParameter() {

        if(currentGeneration <= startGeneration) {
            return startParameter;
        }
        else if(currentGeneration >= endGeneration) {
            return endParameter;
        }
        else {
            int currentStep = currentGeneration - startGeneration;
            int duration = endGeneration - startGeneration;
            double range = endParameter - startParameter;
            double increment = range / (double) duration;
            return startParameter + currentStep * increment;
        }
    }

    @Override
    public <V extends EvoEngine<T>> void update(V s) {
        if(s instanceof EvoEngine)
        {
            EvoEngine e = (EvoEngine) s;

            // Alert is called at the end of each generation, so set for next generation

            currentGeneration = (e.getPopulationStats().getCurrentGeneration() + 1);
        }
    }
}
