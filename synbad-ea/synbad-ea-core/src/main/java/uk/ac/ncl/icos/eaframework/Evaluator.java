/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.synbad.core.util.Configured;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public interface Evaluator<T extends Chromosome>  {

    <V extends Fitness> V evaluate(T c);

}
