/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import java.util.Iterator;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

/**
 *
 * @author owengilfellon
 */
public interface SelectionIterator<T extends Chromosome> extends Iterator<EvaluatedChromosome<T>> {
    
    
    
}
