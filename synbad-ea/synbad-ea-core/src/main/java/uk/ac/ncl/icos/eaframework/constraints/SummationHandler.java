/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.constraints;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.ConstraintHandler;

/**
 *
 * @author owengilfellon
 */
public class SummationHandler implements ConstraintHandler {
    
    private static final Logger logger = LoggerFactory.getLogger(SummationHandler.class);

    @Override
    public Fitness processConstraints(List<Double> values) {
        Double fitness = values.stream().reduce(0.0, (Double t, Double u) -> t + u) / values.size();
        if(fitness < 0 || fitness > 1) {
            logger.error("Fitness is out of range: {}", fitness);
        }
        return new Fitness(fitness);
    }
}
