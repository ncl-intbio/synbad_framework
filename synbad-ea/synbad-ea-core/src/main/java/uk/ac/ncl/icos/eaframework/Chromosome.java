/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework;

import org.bson.types.ObjectId;

/**
 *
 * @author owengilfellon
 */
public interface Chromosome {
    
    Chromosome duplicate();
    Chromosome duplicate(ObjectId id, ObjectId[] parentIds);
    ObjectId getId();
    ObjectId[] getParentIds();
    
}
