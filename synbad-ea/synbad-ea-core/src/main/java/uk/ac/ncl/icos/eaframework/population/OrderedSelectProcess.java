/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.population;

import com.sun.org.apache.bcel.internal.generic.POP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class OrderedSelectProcess<T extends Chromosome> implements PopulationProcess<T> {

    private final Logger logger = LoggerFactory.getLogger(OrderedSelectProcess.class);
    private final int POPULATION_SIZE;
    private final Selection<T> selection;
    private List<EvaluatedChromosome<T>> population;
    private final boolean MINIMISE;

    public OrderedSelectProcess(Selection<T> selection, int population, boolean minimise) {
        this.selection = selection;
        this.POPULATION_SIZE = population;
        this.population = Collections.EMPTY_LIST;
        this.MINIMISE = minimise;
    }

    @Override
    public List<EvaluatedChromosome<T>> getPopulation() {
        return new LinkedList<>(population);
    }

    @Override
    public List<EvaluatedChromosome<T>> nextPopulation(List<EvaluatedChromosome<T>> population) {
        
        logger.debug("Selecting from {} with: [{}]", population != null? population.size() : "null", selection.getClass().getSimpleName());

        List<EvaluatedChromosome<T>> populationFutures = new ArrayList<>();

        if(population.size() != POPULATION_SIZE * 2) {
            logger.error("Selection pool is not twice the population size");
        }

        try {
            for(int chromosome = 0; chromosome < POPULATION_SIZE; chromosome++) {
                EvaluatedChromosome<T> child = population.get(chromosome);
                EvaluatedChromosome<T> parent = population.get(chromosome + POPULATION_SIZE);
                boolean c2fitter = chromosomeOneIsFitter(parent.getFitness().getFitness(), child.getFitness().getFitness());
                if(c2fitter) {
                    if(logger.isDebugEnabled())
                        logger.debug("Parent is fitter ({} {} {}), adding {}",
                                parent.getFitness().getFitness(),
                                MINIMISE ? "<" : ">",
                                child.getFitness().getFitness(), parent.toString());
                    populationFutures.add(parent);
                } else {
                    if(logger.isDebugEnabled())
                        logger.debug("Child is fitter ({} {} {}), adding {}",
                                child.getFitness().getFitness(),
                                MINIMISE ? "<" : ">",
                                parent.getFitness().getFitness(), child.toString());
                    populationFutures.add(child);
                }
            }

            this.population = populationFutures;
            return getPopulation();
        } catch(Exception e) {
            logger.warn("Could not get population: " + e.getLocalizedMessage());
            return null;
        }
    }

    private boolean chromosomeOneIsFitter(double fitness1, double fitness2) {
        return MINIMISE ? (fitness1 < fitness2) : (fitness1 > fitness2);
    }

    @Override
    public Selection<T> getSelection() {
        return selection;
    }

    @Override
    public Operator<T> getOperator() {
        return null;
    }

    @Override
    public int getPopulationSize() {
        return POPULATION_SIZE;
    }

}
