package uk.ac.ncl.icos.eaframework.observer;

import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;

/**
 * Created by owengilfellon on 06/03/2014.
 */
public class GenerationObserver implements EvolutionObserver<Chromosome>, Serializable {

    private EvoEngine e = null;

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s)
    {
        this.e = (EvoEngine) s;
    }

    public int getGeneration()
    {
        if(e != null)
        {
            PopulationStats stats = e.getPopulationStats();
            if(stats != null)
            {
                return stats.getCurrentGeneration();
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
}
