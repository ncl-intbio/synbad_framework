/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.population;

import uk.ac.ncl.icos.eaframework.PopulationProcess;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvaluatedChromosomeFactory;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.SelectionIterator;

/**
 * The order of chromosomes in a population as produced by reproduction selection
 * is used to order children.
 * 
 * @author owengilfellon
 */
public class OrderedSelectAndMutateProcess<T extends Chromosome> implements PopulationProcess<T> {
    
    private final  Logger LOGGER = LoggerFactory.getLogger(OrderedSelectAndMutateProcess.class);
    private final ExecutorService pool;
    private final int POOL_SIZE;
    private final int POPULATION_SIZE;
    private final int MUTATIONS_PER_GENERATION;

    private final Selection<T> selection;
    private final Operator<T> operator;
    private final Evaluator<T> evaluator;
    private final EvaluatedChromosomeFactory<T> chromFactory;

    private final boolean MINIMISE;

    private List<EvaluatedChromosome<T>> population;



    public OrderedSelectAndMutateProcess(Selection<T> selection, Operator<T> operator, Evaluator<T> evaluator, EvaluatedChromosomeFactory<T> chromFactory, boolean minimise, int population, int mutationsPerGeneration, int poolSize) {
        this.selection = selection;
        this.operator = operator;
        this.evaluator = evaluator;
        this.chromFactory = chromFactory;
        this.POPULATION_SIZE = population;
        this.MUTATIONS_PER_GENERATION = mutationsPerGeneration;       
        this.POOL_SIZE = poolSize;
        this.MINIMISE = minimise;
        this.pool =  new ThreadPoolExecutor(POOL_SIZE, POOL_SIZE, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        this.population = new LinkedList<>();
    }
    
    
    private EvaluatedChromosome<T>[] process(ResourcePool<IndexedObject<EvaluatedChromosome<T>>> selectionPool, boolean mutate) {
        

        AtomicInteger completion = new AtomicInteger(0);
        AtomicInteger failure = new AtomicInteger(0);
        
        // Store evolved population on completion
        EvaluatedChromosome<T>[] processed = new EvaluatedChromosome[POPULATION_SIZE];
        
        // Create iterator over population that will apply given selection strategy
   
      while(completion.get() < POPULATION_SIZE) {

            if(!selectionPool.isEmpty()) {
   

                IndexedObject<EvaluatedChromosome<T>> c = selectionPool.acquire();

                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("Acquired: [ {} ]", c.index);
                
                CompletableFuture<IndexedObject<EvaluatedChromosome<T>>> cf = CompletableFuture.supplyAsync(()-> {

                    IndexedObject<EvaluatedChromosome<T>> o = c;

                    synchronized (OrderedSelectAndMutateProcess.class) {
                        if (mutate) {
                            EvaluatedChromosome<T> ec = chromFactory.getEvaluated(
                                    operator.apply(o.getObject().getChromosome()),
                                    new Fitness(0.0));
                            o = new IndexedObject<>(ec, c.index);
                            if (LOGGER.isTraceEnabled())
                                LOGGER.trace("Mutated: [ {} ]", o.index);
                        }
                    }

                    synchronized (OrderedSelectAndMutateProcess.class) {

                        Fitness f = evaluator.evaluate(o.getObject().getChromosome());
                        if (f == null || Double.isNaN(f.getFitness())) {
                            throw new NullPointerException("Evaluation error: fitness is null");
                        }

                        EvaluatedChromosome<T> ec = chromFactory.getEvaluated(o.getObject().getChromosome(), f);

                        if (LOGGER.isTraceEnabled())
                            LOGGER.trace("Adding to child population at {}: [ {} ]", o.getIndex(), o.getObject());
                        return new IndexedObject<>(ec, o.getIndex());
                    }
                }, pool);

                // need to change failures to exceptions

                cf.exceptionally(e -> {
                    LOGGER.error(e.getMessage());
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("ERROR! Reinserted: [ {} ]", c.index);
                    selectionPool.insert(c);
                    failure.incrementAndGet();
                    return null;
                });

                cf.thenAcceptAsync(ch -> {


                    int index = ch.index;
                    EvaluatedChromosome<T> chromosome = ch.object;

//                    if(chromosomeOneIsFitter(chromosome.getFitness().getFitness(), this.population.get(index).getFitness().getFitness())) {
//                        LOGGER.debug("Adding child to survival population at {}  with fitness {}: [ {} ]", index, chromosome.getFitness().getFitness(),  chromosome.getChromosome().toString());
                        processed[index] = chromosome;
//                    } else {
//                        LOGGER.debug("Adding parent to survival population at {} with fitness {}: [ {} ]", index, population.get(index).getFitness().getFitness(),  population.get(index).getChromosome().toString());
//                        processed[index] = population.get(index);
//                    }

                    LOGGER.trace("Added to survival population at {}: [ {} ]", ch.index + 1,  ch.object.getChromosome().toString());
                    completion.incrementAndGet();
                }, pool);
            }
        } 

        if(failure.get() > 0)
            LOGGER.warn("Returning from selection with {} failures", failure.get());

        return processed;
    }

    private boolean chromosomeOneIsFitter(double fitness1, double fitness2) {
        return MINIMISE ? (fitness1 < fitness2) : (fitness1 > fitness2);
    }

    @Override
    public List<EvaluatedChromosome<T>> nextPopulation(List<EvaluatedChromosome<T>> population) {

        // Create iterator over population that will apply given selection strategy
   
        SelectionIterator<T> it = selection.select(population);
        ResourcePool<IndexedObject<EvaluatedChromosome<T>>> selectionPool = new ResourcePool<>(POPULATION_SIZE);

        boolean initialise = this.population.isEmpty();

        for(int i = 0; i < POPULATION_SIZE; i++) {
            EvaluatedChromosome<T> t = it.next();
            selectionPool.insert(new IndexedObject<>(t, i));
            if(initialise) {
                this.population.add(t);
            }
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Inserted at {}: [ {} ]", i, t.getChromosome());
        }
        
        EvaluatedChromosome<T>[] processed = process(selectionPool, true);
        this.population = new LinkedList<>(Arrays.asList(processed));
        return getPopulation();
    }

    @Override
    public List<EvaluatedChromosome<T>> getPopulation() {
        return new LinkedList<>(population);
    }

    @Override
    public Selection<T> getSelection() {
         return selection;
    }

    @Override
    public Operator<T> getOperator() {
        return operator;
    }
    
    @Override
    public int getPopulationSize() {
        return POPULATION_SIZE;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        pool.shutdown();
    }
    
    public final static class IndexedObject<O> {
        
        final O object;
        final int index;

        public IndexedObject(O object, int index) {
            this.object = object;
            this.index = index;
        } 

        public int getIndex() {
            return index;
        }

        public O getObject() {
            return object;
        }
        
    }    
}
