/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.population;

import uk.ac.ncl.icos.eaframework.PopulationProcess;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvaluatedChromosomeFactory;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.Selection;

/**
 * The order of chromosomes in a population as produced by reproduction selection
 * is used to order children.
 * 
 * @author owengilfellon
 */
public class OrderedEvaluationProcess<T extends Chromosome> implements PopulationProcess<T> {
    
    private final  Logger LOGGER = LoggerFactory.getLogger(OrderedEvaluationProcess.class);
    private final int POOL_SIZE;
    private final int POPULATION_SIZE;

    private final Evaluator<T> evaluator;
    private final EvaluatedChromosomeFactory<T> chromFactory;

    private List<EvaluatedChromosome<T>> population;
    
    public OrderedEvaluationProcess(Evaluator<T> evaluator, EvaluatedChromosomeFactory<T> chromFactory, int population, int poolSize) {
        this.evaluator = evaluator;
        this.chromFactory = chromFactory;
        this.POPULATION_SIZE = population;    
        this.POOL_SIZE = poolSize;
        this.population  = Collections.emptyList();
    }
    
    
    private EvaluatedChromosome<T>[] process(ResourcePool<IndexedObject<EvaluatedChromosome<T>>> selectionPool) {
        
        ExecutorService pool = new ThreadPoolExecutor( POOL_SIZE, POOL_SIZE, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
       
        AtomicInteger completion = new AtomicInteger(0);
        AtomicInteger failure = new AtomicInteger(0);
        
        // Store evolved population on completion
        EvaluatedChromosome<T>[] processed = new EvaluatedChromosome[POPULATION_SIZE];
        
        // Create iterator over population that will apply given selection strategy
   
        while(completion.get() < POPULATION_SIZE) {
            
            
            if(!selectionPool.isEmpty()) {
   
                IndexedObject<EvaluatedChromosome<T>> c = selectionPool.acquire();

                if(LOGGER.isTraceEnabled())
                    LOGGER.trace("Acquired: [ {} ]", c.index);
                
                CompletableFuture<IndexedObject<T>> cf = CompletableFuture.supplyAsync(()-> {
                    return new IndexedObject<>(c.object.getChromosome(), c.index);}, pool);
          
                
                CompletableFuture<IndexedObject<EvaluatedChromosome<T>>> cf2 = cf.thenApply(chrom -> {
                    if(chrom == null) {
                        //selectionPool.insert(it.next());
                        throw new NullPointerException("Evaluation error: chromosome is null");
                    }
                    
                    Fitness f = evaluator.evaluate(chrom.object);
                    
                    
                    if(f == null || Double.isNaN(f.getFitness())) {
                        //selectionPool.insert(it.next());
                        throw new NullPointerException("Evaluation error: fitness is null");
                    }
                    
                    EvaluatedChromosome<T> ec = chromFactory.getEvaluated(chrom.object, f);

                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("Adding to child population at {}: [ {} ]", chrom.index, chrom.object);
                    return new IndexedObject<>(ec, chrom.index);
                });

                // need to change failures to exceptions

                cf2.exceptionally(e -> {
                    LOGGER.error(e.getMessage());
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("ERROR! Reinserted: [ {} ]", c.index);
                    selectionPool.insert(c);
                    failure.incrementAndGet();
                    return null;
                });

                cf2.thenAccept(ch -> {
                    int index = ch.index;
                    EvaluatedChromosome<T> chromosome = ch.object;
                    processed[index] = chromosome;
                    if(LOGGER.isTraceEnabled())
                        LOGGER.trace("Added to population at {}: [ {} ]", ch.index + 1,  ch.object.getChromosome().toString());
                    completion.incrementAndGet();
         
                });
            }
        } 

        if(failure.get() > 0)
            LOGGER.warn("Returning from selection with {} failures", failure.get());
        
        pool.shutdown();
        
        
        return processed;
    }

    @Override
    public List<EvaluatedChromosome<T>> getPopulation() {
        return new LinkedList<>(population);
    }

    @Override
    public List<EvaluatedChromosome<T>> nextPopulation(List<EvaluatedChromosome<T>> population) {

        // Create iterator over population that will apply given selection strategy
        
        ResourcePool<OrderedEvaluationProcess.IndexedObject<EvaluatedChromosome<T>>> selectionPool = new ResourcePool<>(POPULATION_SIZE);
        
        for(int i = 0; i < POPULATION_SIZE; i++) {
            EvaluatedChromosome<T> t =  (EvaluatedChromosome<T>)population.get(i);
            selectionPool.insert(new OrderedEvaluationProcess.IndexedObject<>(t, i));
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Inserted at {}: [ {} ]", i, t.getChromosome());
        }
        
        // Store evolved population on completion
        EvaluatedChromosome<T>[] processed = process(selectionPool);
        
        this.population = Arrays.asList(processed).stream().collect(Collectors.toList());
        return getPopulation();
    }

    @Override
    public Selection<T> getSelection() {
         return null;
    }

    @Override
    public Operator<T> getOperator() {
        return null;
    }
    
    @Override
    public int getPopulationSize() {
        return POPULATION_SIZE;
    }
    
    public final static class IndexedObject<O> {
        
        final O object;
        final int index;

        public IndexedObject(O object, int index) {
            this.object = object;
            this.index = index;
        } 

        public int getIndex() {
            return index;
        }

        public O getObject() {
            return object;
        }
        
    }    
}
