/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.operator;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.parser.SVPReader;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.view.SBHView;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;

/**
 *
 * @author owen
 */
public class Operators {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Operators.class);
    private static final Random RANDOM = new Random();
    
    /**
     * Retrieves a random SVP instance from the model
     */
    public static SvpVo getRandomPart(SvpModel model) {
        List<SvpVo> allParts = model.getSvps();

        if(allParts.isEmpty()) { 
            LOGGER.warn("No {} SVP with property in model {}");
            return null;
        }
            
        SvpVo selected = allParts.get(RANDOM.nextInt(allParts.size()));
        return selected;
    }
    
    /**
     * Retrieves a random SVP from the model with the provided ComponentRole
     * and a Property object with the provided value

     */
    public static SvpVo getRandomPart(SvpModel model, ComponentRole role, String... propertyValues) {

        List<String> properties = Arrays.asList(propertyValues);

        List<SvpVo> allParts = model.getSvps(role).stream()
            .filter(svp -> svp.getSvp().getProperties().stream()
                    .anyMatch(p -> properties.contains(p.getPropertyValue())))
            .collect(Collectors.toList());

        if(allParts.isEmpty()) { 
            LOGGER.warn("No {} SVP with properties in model {}", role.toString(),  model.getId());
            return null;
        }

        return allParts.get(RANDOM.nextInt(allParts.size()));
    }


    public static String getSvpWrite(SvpModel model) {
        return getSvpWrite(model.getViewRoot());
    }

    public static String getSvpWrite(SvVo vo) {
        if(SvmVo.class.isAssignableFrom(vo.getClass())) {
            ((SvpPView)vo.getView()).expand(vo);
            return ((SvmVo)vo).getOrderedSequenceComponents().stream()
                .map(Operators::getSvpWrite)
                .reduce("{", (a, b) -> a.equals("{")
                        ? a.concat(b)
                        : a.concat("; ".concat(b))).concat("}");
        } else if(SvpVo.class.isAssignableFrom(vo.getClass())) {
            return vo.getDisplayId();
        }
        return "";
    }

    /**
     * Returns a random SVP from the model with the provided ComponentRole, predicate and value
     * @param model
     * @param role
     * @param predicate
     * @param value
     * @return 
     */
    public static SvpVo getRandomPart(SvpModel model, ComponentRole role, String predicate, SBValue value) {
        List<SvpVo> allParts = model.getSvps(role).stream()
            .filter(svp -> svp.getValues(predicate).contains(value))
            .collect(Collectors.toList());

        if(allParts.isEmpty()) { 
            LOGGER.warn("No {} SVP with predicate and value in model {}");
            return null;
        }
            
        SvpVo selected = allParts.get(RANDOM.nextInt(allParts.size()));
        return selected;
    }
    
    /**
     * Returns a random SVP from the model with the provided ComponentRole
     * @param model
     * @param role
     * @return 
     */
    public static SvpVo getRandomPart(SvpModel model, ComponentRole role) {
        List<SvpVo> allParts = model.getSvps(role);

        if(allParts.isEmpty()) { 
            LOGGER.warn("No {} SVP in model {}");
            return null;
        }
            
        SvpVo selected = allParts.get(RANDOM.nextInt(allParts.size()));
        return selected;
    }
    

    public static boolean replaceSvp(SvpModel model, SvpVo replacee, Part replacer) {

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(model.getWorkspace(), model.getRootNode().getContexts());

        SBWorkspace ws = model.getWorkspace();
        
        // Read part into SvpBuilder
        
        new SVPReader(b.getWorkspace(), b.getContexts(), "http://www.synbad.org", "1.0").read(b, replacer);
 
        // get index of part to replace

        SvmVo parent = model.getCurrentView().getParent(replacee);

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Replacing {} in {} with {}", replacee.getDisplayId(), parent.getDisplayId(), replacer.getName() );
        
        List<SvVo> children = parent.getOrderedSequenceComponents();

        URI replaceeComponent = replacee.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null);

        int replaceeIndex = children.indexOf(replacee);

        // get component that the rbs precedes

        b.removeChild(parent.getSvpModule().getIdentity(), replaceeComponent);

        if(b.isEmpty())
            return false;

        ws.perform(b.build());

        b = new SvpActionBuilderImpl(ws, model.getRootNode().getContexts());
        
        SBIdentityFactory f = ws.getIdentityFactory();
        
        SBIdentity replacerSvp = f.getIdentity("http://www.synbad.org", replacer.getName(), "1.0");
        
        // add SVP at end, or preceding the identified component
        
        // should be component identity.....

        SvVo preceded = replaceeIndex + 1 <= ( children.size() - 1 ) ? children.get(replaceeIndex + 1) : null;

        b.addSvp(f.getIdentity(parent.getSvpModule().getIdentity()),
                replacerSvp,
                preceded == null ? null : f.getIdentity(preceded.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null) ));

        if(b.isEmpty())
            return false;

        ws.perform(b.build());

        return true;
    }
    
    public static boolean swapSvps(SvpModel model, SvpVo svp1, SvpVo svp2) {

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(model.getWorkspace(), model.getRootNode().getContexts());

        SBWorkspace ws = model.getWorkspace();

        SvmVo parent1 = model.getCurrentView().getParent(svp1);
        SvmVo parent2 = model.getCurrentView().getParent(svp2);

        List<SvVo> children1 = parent1.getOrderedSequenceComponents();
        List<SvVo> children2 = parent2.getOrderedSequenceComponents();

        String svpWrite1 = children1.stream().map(SBIdentified::getDisplayId).reduce("", (id, c) -> id.isEmpty() ? c : id.concat("; ").concat(c));
        String svpWrite2 = children2.stream().map(SBIdentified::getDisplayId).reduce("", (id, c) -> id.isEmpty() ? c : id.concat("; ").concat(c));

//        LOGGER.debug("Applying to 1: {}", svpWrite1);
//        LOGGER.debug("Applying to 2: {}", svpWrite2);

        URI c1Id = svp1.getObject().as(SvpInstance.class).map(i -> {
           Component c =  i.getMappedToComponent();
           return c.getIdentity();
        }).orElse(null);
        URI c2Id = svp2.getObject().as(SvpInstance.class).map(i -> {
            Component c =  i.getMappedToComponent();
            return c.getIdentity();
        }).orElse(null);

        int svpIndex1 = children1.indexOf(svp1);
        int svpIndex2 = children2.indexOf(svp2);
        
        // get component that the rbs precedes

        b.removeChild(parent1.getSvpModule().getIdentity(), c1Id);
        b.removeChild(parent2.getSvpModule().getIdentity(), c2Id);

        if(b.isEmpty())
            return false;

        ws.perform(b.build());

        b = new SvpActionBuilderImpl(model.getWorkspace(), model.getRootNode().getContexts());

        // add SVP at end, or preceding the identified component
        
        SvVo preceded1 = svpIndex1 + 1 <= ( children1.size() - 1 ) ?
                children1.get(svpIndex1 + 1) : null;
        SvVo preceded2 = svpIndex2 + 1 <= ( children2.size() - 1 ) ?
                children2.get(svpIndex2 + 1) : null;
        
        LOGGER.info("Swapping {}:{} before {} and {}:{} before {}",
                svpIndex1, svp1.getDisplayId(), preceded2 == null ? "X" : preceded2.getDisplayId(),
                svpIndex2, svp2.getDisplayId(), preceded1 == null ? "X" : preceded1.getDisplayId());
        
        SBIdentityFactory f = ws.getIdentityFactory();
        
        SBIdentity parent1Id = f.getIdentity(parent1.getSvpModule().getIdentity());
        SBIdentity parent2Id = f.getIdentity(parent2.getSvpModule().getIdentity());
        
        SBIdentity svp1Id = f.getIdentity(svp1.getSvp().getIdentity());
        SBIdentity svp2Id = f.getIdentity(svp2.getSvp().getIdentity());
        
        b.addSvp(parent2Id, svp1Id, 
                ( preceded2 == null ? null : f.getIdentity(preceded2.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null) )));
        b.addSvp(parent1Id, svp2Id, 
                ( preceded1 == null ? null : f.getIdentity(preceded1.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null) )));

        if(b.isEmpty())
            return false;

        ws.perform(b.build());

        return true;
    }
}
