package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInstance;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

import java.net.URI;
import java.util.List;

/**
 * A random part is chosen from a randomly selected transcriptional unit, and
 * is swapped with a randomly chosen part of the same type in a second random
 * TU.
 * 
 * @author owengilfellon
 */
public class SwapParts extends AbstractOperator<SvpChromosome> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwapParts.class);
    private final int        MAX_ATTEMPTS = 10;

    @Override
    public SvpChromosome apply(SvpChromosome c) {
        
        //LOGGER.debug("Duplicating {}", c.toString());

        SvpChromosome t = c.duplicate();
        boolean mutationOccured = false;
        int attempt = 0;
        SBWorkspace ws = t.getModel().getWorkspace();
        while ( !mutationOccured && attempt < MAX_ATTEMPTS ) {

            //LOGGER.debug("Attempt #{} on {}", attempt, t.toString());
            SvpVo node1 = Operators.getRandomPart(t.getModel());
            SvpVo node2 = Operators.getRandomPart(t.getModel(), node1.getPartTypeAsRole());

            if (!node1.getIdentity().equals(node2.getIdentity())) {
                try {
                    SvpActionBuilderImpl b = new SvpActionBuilderImpl(node1.getWorkspace(), node1.getContexts());
                    //LOGGER.debug("Swapping {} and {}", node1.getDisplayId(), node2.getDisplayId());
                  
                    SvmVo parent1 = t.getModel().getCurrentView().getParent(node1);
                    SvmVo parent2 = t.getModel().getCurrentView().getParent(node2);

                    List<SvVo> children1 = parent1.getOrderedSequenceComponents();
                    List<SvVo> children2 = parent2.getOrderedSequenceComponents();

                    if(LOGGER.isTraceEnabled()) {
                        String svpWrite1 = children1.stream().map(SBIdentified::getDisplayId).reduce("", (id, x) -> id.isEmpty() ? x : id.concat("; ").concat(x));
                        String svpWrite2 = children2.stream().map(SBIdentified::getDisplayId).reduce("", (id, x) -> id.isEmpty() ? x : id.concat("; ").concat(x));
                        LOGGER.trace("Applying to 1: {}", svpWrite1);
                        LOGGER.trace("Applying to 2: {}", svpWrite2);
                    }

                    SBIdentityFactory f = ws.getIdentityFactory();

                    SBIdentity parent1Id = f.getIdentity(parent1.getSvpModule().getIdentity());
                    SBIdentity parent2Id = f.getIdentity(parent2.getSvpModule().getIdentity());

                    SBIdentity node1Id = f.getIdentity(node1.getSvp().getIdentity());
                    SBIdentity node2Id = f.getIdentity(node2.getSvp().getIdentity());

                    URI c1Id = node1.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null);
                    URI c2Id = node2.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null);

                    int svpIndex1 = children1.indexOf(node1);
                    int svpIndex2 = children2.indexOf(node2);

                    // get component that the rbs precedes

                    b.removeChild(parent1.getSvpModule().getIdentity(), c1Id);
                    b.removeChild(parent2.getSvpModule().getIdentity(), c2Id);

                    ws.perform(b.build());

                    b = new SvpActionBuilderImpl(node1.getWorkspace(), node1.getContexts());

                    // add SVP at end, or preceding the identified component

                    SvVo preceded1 = svpIndex1 + 1 <= ( children1.size() - 1 ) ?
                            children1.get(svpIndex1 + 1) : null;
                    SvVo preceded2 = svpIndex2 + 1 <= ( children2.size() - 1 ) ?
                            children2.get(svpIndex2 + 1) : null;

                    LOGGER.debug("Swapping {}:{} before {} and {}:{} before {}",
                            svpIndex1, node1.getDisplayId(), preceded2 == null ? "X" : preceded2.getDisplayId(),
                            svpIndex2, node2.getDisplayId(), preceded1 == null ? "X" : preceded1.getDisplayId());

                    b.addSvp(parent2Id, node1Id,
                            ( preceded2 == null ? null : f.getIdentity(preceded2.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null) )));
                    b.addSvp(parent1Id, node2Id,
                            ( preceded1 == null ? null : f.getIdentity(preceded1.getObject().as(SvpInstance.class).map(i -> i.getMappedToComponent().getIdentity()).orElse(null) )));

                    ws.perform(b.build());
                    mutationOccured = true;
                } catch (Exception ex) {
                   LOGGER.error("Bah!", ex);
                   t.getModel().close();
                   return c;
                }
            } 

            attempt++;

            if(!mutationOccured && attempt >= MAX_ATTEMPTS) {
                LOGGER.warn("Couldn't apply SwapParts after {} attempts", MAX_ATTEMPTS);
                   t.getModel().close();
                return c;
            }
        }

     //   LOGGER.debug("Done: {}", t.getModel());
        return t;
    }
}
