/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import java.net.URI;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author owen
 */
public interface SBChromosomeManager {
    
    public Collection<URI> getWorkspaces();
     
    public Collection<URI> getChromosomeContexts(URI ws);
    
    public URI getNextChromosomeContext(URI ws, URI context);
    
}
