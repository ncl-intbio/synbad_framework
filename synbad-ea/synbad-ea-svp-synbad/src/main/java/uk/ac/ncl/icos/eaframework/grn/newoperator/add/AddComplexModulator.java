    package uk.ac.ncl.icos.eaframework.grn.newoperator.add;

import java.net.URI;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;


/**
 * Creates a Generator module, which generates a PoPS signal, within an SvpModel.
 * 
 * @author owengilfellon
 */
public class AddComplexModulator extends AddSequenceModule {
  
    @Override
    public SvpChromosome apply(SvpChromosome c) {

        // Create new model
        
        SvpChromosome t = (SvpChromosome) c.duplicate();

        // Get workspace, contexts, and create SBIdentity of new module
        
        SvmVo root = t.getModel().getViewRoot();
        root.getView().nodeSet().forEach(root.getView()::expand);
        
        URI[] CONTEXTS = root.getContexts();
        SBWorkspace ws = root.getWorkspace();
        SBIdentityFactory idFactory = ws.getIdentityFactory();

        // Retrieve random parent SVM
        
        SvmVo parent = getRandomParentModule(t.getModel());
        if(parent == null) {
            LOGGER.warn("Could not find parent for new generator");
            t.getModel().close();
            return c;
        }
        
        Integer index = getPositionInParent(parent, true);

        // Create identities
        
        SBIdentity modulatorId = idFactory.getUniqueIdentity(idFactory.getIdentity(
                root.getPrefix(), "complexModulator", root.getVersion()));
        SBIdentity parentId = idFactory.getIdentity(parent.getSvpModule().getIdentity());
        
        // Perform modification of model
        
        InteractionMode mode = getRandomModulationMode();
        LOGGER.debug("Adding {}:{} to {} at {}", modulatorId.getDisplayID(), mode, parentId.getDisplayID(), index);
        
        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS);
        b = b.createComplexationUnit(modulatorId, mode)
                .setCdsIdentity(idFactory.getIdentity(modulatorId.getUriPrefix(), modulatorId.getDisplayID() + "_cds1", modulatorId.getVersion()))
                .setCds2Identity(idFactory.getIdentity(modulatorId.getUriPrefix(), modulatorId.getDisplayID() + "_cds2", modulatorId.getVersion()))
                .setSmlMolIdentity(idFactory.getIdentity(modulatorId.getUriPrefix(), modulatorId.getDisplayID() + "_smlMol", modulatorId.getVersion()))
                .builder();
        
        if(index == -1)
            b = b.addSvm(parentId, modulatorId, null);
        else
            b = b.addSvm(parentId, modulatorId, index);
        ws.perform(b.build());
        
        // Close old model and return modified model

      //  c.getModel().close();
        return t;
    }


}