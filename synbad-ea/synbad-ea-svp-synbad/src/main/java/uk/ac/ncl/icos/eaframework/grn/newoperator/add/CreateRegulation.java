    package uk.ac.ncl.icos.eaframework.grn.newoperator.add;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;


/**
 * Creates a Generator module, which generates a PoPS signal, within an SvpModel.
 * 
 * @author owengilfellon
 */
public class CreateRegulation  extends AbstractOperator<SvpChromosome> {
    
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(CreateRegulation.class);
    protected final Random RANDOM = new Random();
    
    @Override
    public SvpChromosome apply(SvpChromosome c) {

        // Create new model
        
        SvpChromosome t = (SvpChromosome) c.duplicate();

        // Get workspace, contexts, and create SBIdentity of new module
        
        SvmVo root = t.getModel().getViewRoot();
        root.getView().nodeSet().forEach(root.getView()::expand);
        
        URI[] CONTEXTS = root.getContexts();
        SBWorkspace ws = root.getWorkspace();
        SBIdentityFactory idFactory = ws.getIdentityFactory();

        // Retrieve random parent SVM
        
        SvmVo parent = getRandomParentModule(t.getModel());
        if(parent == null) {
            LOGGER.warn("Could not find parent for new generator");
            t.getModel().close();
            return c;
        }
        
        
        SvpViewPort port = getModulatorPort(parent);
        

        // Create identities
        
      
        SBIdentity parentId = idFactory.getIdentity(parent.getSvpModule().getIdentity());
    
        
        // identity from part
        
        SBIdentity modulator = null;
        
        // If modulator is complex or phosphorylated, port owner is interaction... 
        // otherwise, just leave null
        
        SBIdentity portOwner = null;
        
        // Get parent of modulator
        
        SBIdentity modulatorOwner = null;
        
//        SynBadPortType modulatorType = (SynBadPortType) port.getDefinition().getPortType();
//        SynBadPortState modulatorState = (SynBadPortState) port.getDefinition().getPortState();
        
        // if positive, then use existing promoter, otherwise if negative,
        // create new operator
        
        SBIdentity modulated = null;
        
        // create from modulator, type, state and modulated
        
        SBIdentity activateId = idFactory.getUniqueIdentity(idFactory.getIdentity(
        root.getPrefix(), "sink", root.getVersion()));
                
        // Perform modification of model

//        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS);
//        b = b.createActivateUnit(activateId)
//                .setModulator(null)
//                .setPortOwner(null)
//                .setModulatorOwner(null)
//                .setModulatorType(modulatorType)
//                .setModulatorState(modulatorState)
//                .setModulated(null).builder();
        
//        LOGGER.debug("Adding {} to {} at {}", activateId.getDisplayID(), parentId.getDisplayID(), port.getDisplayId());
        /*

        
        if(index == -1)
            b = b.addSvm(parentId, activateId, null);
        else
            b = b.addSvm(parentId, activateId, index);
        ws.perform(b.build());
        
        // Close old model and return modified model
        */
        c.getModel().close();
        return t;
    }
    
    protected SvmVo getRandomParentModule(SvpModel model) {
        List<SvmVo> svms = model.getSvms().stream()
            .filter(svm -> svm.getSvpModule().getValue(SynBadTerms.SynBadTu.moduleType) == null)
            .collect(Collectors.toList());
       
        if(svms.isEmpty())
            return null;
        
        return svms.get(RANDOM.nextInt(svms.size()));
        
    }

    protected SvpViewPort getPromoter(SvmVo parent) {

        Optional<SvpVo> promoter = parent.getView().getChildren(parent, SvpVo.class)
                .stream().filter(c -> c.getPartTypeAsRole() == ComponentRole.Promoter).findAny();

//        if(promoter.isPresent())
//            return promoter.get();

        LOGGER.warn("Could not find promoter in module");

        return null;

    }
    
    protected SvpViewPort getModulatorPort(SvmVo parent) {
        List<SvpViewPort> openPorts = parent.getNestedPorts(false, SBPortDirection.OUT).stream()
            .filter(p -> p.getObject().getDefinition().getPortType() == SynBadPortType.Protein ||
                    p.getObject().getDefinition().getPortType() == SynBadPortType.Complex)
            .collect(Collectors.toList());
        
        if(openPorts.isEmpty()) {
            LOGGER.warn("No open ports!");
            return null;
        }
        
        SvpViewPort port = openPorts.get(RANDOM.nextInt(openPorts.size()));
        return port;
    }
    
    protected TuActionsFactory.InteractionMode getRandomModulationMode() {
        TuActionsFactory.InteractionMode mode = RANDOM.nextInt(1) == 0 ? TuActionsFactory.InteractionMode.POSITIVE : TuActionsFactory.InteractionMode.NEGATIVE; 
        return mode;
    }
    
    
    protected SvpViewPort getModulatedPart(SvmVo parent, InteractionMode interactionMode) {
        
        if(interactionMode == InteractionMode.POSITIVE) {
            
            // Get identit of existing promoter
            
        } else {
            
            // Get identity of operator to be created????
            
        }
        
        return null;
        
    }


}