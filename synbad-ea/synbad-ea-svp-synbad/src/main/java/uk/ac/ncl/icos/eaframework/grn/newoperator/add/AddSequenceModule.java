/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.newoperator.add;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;

/**
 *
 * @author owen
 */
public abstract class AddSequenceModule extends AbstractOperator<SvpChromosome>{
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(AddSequenceModule.class);
    protected final SBWorkspaceManager WORKSPACE_MANAGER = Lookup.getDefault().lookup(SBWorkspaceManager.class);
    protected final Random RANDOM = new Random();
        /**
     * 
     * @param model The model to search for a random parent module
     * @return A random SvmVo that does not have an SvpModuleType.
     */    
    protected SvmVo getRandomParentModule(SvpModel model) {
        List<SvmVo> svms = model.getSvms().stream()
            .filter(svm -> svm.getSvpModule().getValue(SynBadTerms.SynBadTu.moduleType) == null)
            .collect(Collectors.toList());
       
        if(svms.isEmpty())
            return null;
        
        return svms.get(RANDOM.nextInt(svms.size()));
        
        
    }
    
    protected int getPositionInParent(SvmVo svm, boolean attachToOut) {
        List<SvpViewPort> openPorts = svm.getNestedPorts(false, attachToOut ? SBPortDirection.OUT : SBPortDirection.IN).stream()
            .filter(p -> p.getObject().getDefinition().getPortType() == SynBadPortType.PoPS)
            .collect(Collectors.toList());
        
        if(openPorts.isEmpty()) {
            LOGGER.trace("No open ports, adding at beginning");
            return -1;
        }
        
        SvpViewPort port = openPorts.get(RANDOM.nextInt(openPorts.size()));
        SvVo owner = port.getOwner(); 
        int index = svm.getView().getParent(owner).getOrderedSequenceComponents().indexOf(owner);
        return index == -1 ? -1 : attachToOut ? index + 1 : index;
    }
    
    protected InteractionMode getRandomModulationMode() {
        InteractionMode mode = RANDOM.nextInt(1) == 0 ? InteractionMode.POSITIVE : InteractionMode.NEGATIVE; 
        return mode;
    }
    
}
