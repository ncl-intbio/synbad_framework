/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.net.URI;
import java.util.Collection;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owen
 */
@ServiceProvider(service = SBChromosomeManager.class)
public class DefaultChromosomeManager implements SBChromosomeManager, SBSubscriber {
    
   // SBWorkspaceManager m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultChromosomeManager.class);
    private final Multimap<URI, URI> chromosomeContexts = HashMultimap.create();

    @Override
    public Collection<URI> getWorkspaces() {
        return chromosomeContexts.keySet();
    }
    
    @Override
    public Collection<URI> getChromosomeContexts(URI ws) {
        return chromosomeContexts.get(ws);
    }
    
    @Override
    public synchronized URI getNextChromosomeContext(URI ws, URI context) {
        if(!chromosomeContexts.containsEntry(ws, context)) {
            if(chromosomeContexts.get(ws).isEmpty()) {
                chromosomeContexts.put(ws, context);
            } else {
                chromosomeContexts.put(ws, context);
                return context;
            }
        }


 
        String prefix = SBIdentityHelper.getPrefixFromId(context);
        
        String displayId = SBIdentityHelper.isValidChildPersistentId(context) ?
            SBIdentityHelper.getChildDisplayIdFromId(context) :
            SBIdentityHelper.isValidId(context) ?
            SBIdentityHelper.getDisplayIdFromId(context) :
                "";
        
        String version = SBIdentityHelper.getVersionFromId(context);
        
        if(prefix == null || prefix.isEmpty()) {
            LOGGER.error("Couldn't get prefix from {}", context.toASCIIString());
        }
        
        if(displayId == null || displayId.isEmpty()) {
            LOGGER.error("Couldn't get display ID from {}", context.toASCIIString());
        }
        
        if(version == null || version.isEmpty()) {
            version = "1.0";
        }
        
        String displayIdSansIncrement = SBIdentityHelper.getBaseDisplayIdFromIncrementing(displayId);
        int increment =  SBIdentityHelper.getIncrementFromIncrementing(displayId);

        Collection<URI> contexts = chromosomeContexts.get(ws);
        URI nextContext = context;
        
        while(contexts.contains(nextContext)) {
            nextContext = SBIdentityHelper.getURI(prefix + "/" + displayIdSansIncrement + "_" + increment++ + "/" + version);
        }
        
        chromosomeContexts.put(ws, nextContext);
        
 
        
        LOGGER.debug("Returning context from {} existing contexts: {}", contexts.size(), nextContext.toASCIIString());
        
        return nextContext;
    }

    @Override
    public void onEvent(SBEvent e) {
        if(e.getType() == SBGraphEventType.REMOVED) {
            chromosomeContexts.removeAll((URI)e.getData());
        }
        
           
    }
}
