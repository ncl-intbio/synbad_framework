package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;



/**
 * Duplicates a random RBS-CDS pair and places them before a randomly chosen
 * RBS.
 * 
 * @author owengilfellon
 */
public class DuplicateParts extends AbstractOperator<SvpChromosome> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DuplicateParts.class);

    @Override
    public SvpChromosome apply(SvpChromosome c) {
        
        SvpChromosome t = (SvpChromosome) c.duplicate();
        List<SvpVo> svps = t.getModel().getSvps(ComponentRole.RBS);

        if(svps.isEmpty()) {
            t.getModel().close();
            return c;
        }
        
        // Choose a random RBS-CDS pair for duplication
        
        Random r = new Random(new Date().getTime());
        
        int index1 = r.nextInt(svps.size());
        SvpVo source = svps.get(index1);

        List<SvpVo> sourceAndSiblings = source.getParent().getOrderedChildren(SvpVo.class);
        SvpVo cds = sourceAndSiblings.get(sourceAndSiblings.indexOf(source) + 1);
        
        if(cds.getSvp().getPartTypeAsRole() != ComponentRole.CDS)
        {
            LOGGER.warn("Could not find CDS following {}", source.getDisplayId());
            t.getModel().close();
            return c;
        }

        // choose a destination (preceding a randomly chosen RBS)
            
        int index2 = r.nextInt(svps.size());
        SvpVo target = svps.get(index2);

        SvmVo parent = target.getParent();
        SvpModule parentModule = parent.getSvpModule();
        List<SvpVo> targetAndSiblings = target.getParent().getOrderedChildren(SvpVo.class);
        
        int index = targetAndSiblings.indexOf(target);
        
        parentModule.addChild(source.getSvp(), index);
        parentModule.addChild(cds.getSvp(), index + 1);

        return t;
    }
}
