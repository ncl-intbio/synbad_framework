package uk.ac.ncl.icos.eaframework.grn.operator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Randomly selects a TU from within a GRNTree and duplicates it. The duplicate
 * is placed within the same parent as the original.
 * 
 * @author owengilfellon
 */
public class RemoveTU extends AbstractOperator<SvpChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();
    private final static Logger LOGGER = LoggerFactory.getLogger(RemoveTU.class);
    private final static int MAX_ATTEMPTS = 10;

    @Override
    public SvpChromosome apply(SvpChromosome c) {
        
        SvpChromosome t = (SvpChromosome) c.duplicate();
        SvpModule root = t.getRootNode();

        // If no TUs, then mutation cannot be performed. return t.

        List<SvmVo> nonRootModules = t.getModel().getSvms().stream()
                .filter(svm -> !svm.isRoot()).collect(Collectors.toList());

        if(nonRootModules.isEmpty()) {
            t.getModel().close();
            return c;
        }

        // Select a random TU to duplicate

        Random random = new Random();

        SvmVo toRemove = nonRootModules.get(random.nextInt(nonRootModules.size()));
        
        // add child module to parent and return modified chromosome

        SvmVo parent = toRemove.getParent();

        LOGGER.debug("Removing {} from {}", toRemove.getDisplayId(), parent.getDisplayId());

        SvpActionBuilderImpl b = new SvpActionBuilderImpl(t.getModel().getWorkspace(), toRemove.getContexts());
        b.removeChild(parent.getSvpModule().getIdentity(), toRemove.getIdentity());
        t.getModel().getWorkspace().perform(b.build());

        LOGGER.debug(Operators.getSvpWrite(t.getModel()));
   //     c.getModel().close();
        return t;
    }
}
