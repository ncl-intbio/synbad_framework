    package uk.ac.ncl.icos.eaframework.grn.newoperator.add;

    import org.openide.util.Lookup;
    import org.slf4j.Logger;
    import org.slf4j.LoggerFactory;
    import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
    import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
    import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
    import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
    import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
    import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
    import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
    import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
    import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
    import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;
    import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
    import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
    import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

    import java.net.URI;
    import java.util.List;
    import java.util.Random;
    import java.util.stream.Collectors;


    /**
     * Creates a Generator module, which generates a PoPS signal, within an SvpModel.
     *
     * @author owengilfellon
     */
    public class CreateNegativeRegulation extends AbstractOperator<SvpChromosome> {


        protected static final Logger LOGGER = LoggerFactory.getLogger(CreateNegativeRegulation.class);
        protected final Random RANDOM = new Random();

        @Override
        public SvpChromosome apply(SvpChromosome c) {

            // Create new model

            SvpChromosome t = (SvpChromosome) c.duplicate();

            // Get workspace, contexts, and create SBIdentity of new module

            SvmVo root = t.getModel().getViewRoot();
            root.getView().nodeSet().forEach(root.getView()::expand);

            URI[] CONTEXTS = root.getContexts();
            SBWorkspace ws = root.getWorkspace();
            SBIdentityFactory idFactory = ws.getIdentityFactory();

            // Retrieve random parent SVM

            SvmVo parent = getRandomParentModule(t.getModel());
            if(parent == null) {
                LOGGER.warn("Could not find parent for new generator");
                t.getModel().close();
                return c;
            }





            SvpViewPort port = getModulatorPort(parent);


            // Create identities


            SBIdentity parentId = idFactory.getIdentity(parent.getSvpModule().getIdentity());


            // identity from part

            SBIdentity modulator = idFactory.getIdentity(port.getOwner().getIdentity());

            // If modulator is complex or phosphorylated, port owner is interaction...
            // otherwise, just leave null

            SBIdentity portOwner = idFactory.getIdentity(port.getOwner().getIdentity());

            // Get parent of modulator

            SBIdentity modulatorOwner = null;

    //        SynBadPortType modulatorType = (SynBadPortType) port.getDefinition().getPortType();
    //        SynBadPortState modulatorState = (SynBadPortState) port.getDefinition().getPortState();

            // if positive, then use existing promoter, otherwise if negative,
            // create new operator

            SBIdentity modulated = null;

            // create from modulator, type, state and modulated

            SBIdentity activateId = idFactory.getUniqueIdentity(idFactory.getIdentity(
            root.getPrefix(), "sink", root.getVersion()));

            // Perform modification of model

            // Create SVM with an instance of existing SVM and operator

            // Replace existing SVM with new SVM

            // create interaction between Operator and chosen modulator

//            SBCelloBuilder b = new CelloActionBuilder(WORKSPACE_ID, CONTEXTS);
//            b = b.createRepressUnit(activateId).set
//
//            if(index == -1)
//                b = b.addSvm(parentId, activateId, null);
//            else
//                b = b.addSvm(parentId, activateId, index);
//            ws.perform(b.build());

            // Close old model and return modified model
            /* */
            c.getModel().close();
            return t;
        }

        protected SvmVo getRandomParentModule(SvpModel model) {
            List<SvmVo> svms = model.getSvms().stream()
                .filter(svm -> svm.getSvpModule().getValue(SynBadTerms.SynBadTu.moduleType) == null)
                .collect(Collectors.toList());

            if(svms.isEmpty())
                return null;

            return svms.get(RANDOM.nextInt(svms.size()));

        }

        protected SvpViewPort getModulatorPort(SvmVo parent) {
            List<SvpViewPort> openPorts = parent.getNestedPorts(false, SBPortDirection.OUT).stream()
                .filter(p -> p.getObject().getDefinition().getPortType() == SynBadPortType.Protein ||
                        p.getObject().getDefinition().getPortType() == SynBadPortType.Complex)
                .collect(Collectors.toList());

            if(openPorts.isEmpty()) {
                LOGGER.warn("No open ports!");
                return null;
            }

            SvpViewPort port = openPorts.get(RANDOM.nextInt(openPorts.size()));
            return port;
        }

        protected InteractionMode getRandomModulationMode() {
            InteractionMode mode = RANDOM.nextInt(1) == 0 ? InteractionMode.POSITIVE : InteractionMode.NEGATIVE;
            return mode;
        }


        protected SvpViewPort getModulatedPart(SvmVo parent, InteractionMode interactionMode) {

            if(interactionMode == InteractionMode.POSITIVE) {

                // Get identit of existing promoter

            } else {

                // Get identity of operator to be created????

            }

            return null;

        }


    }