package uk.ac.ncl.icos.eaframework.grn;


import java.net.URI;
import java.util.Arrays;
import java.util.Objects;

import org.bson.types.ObjectId;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.ChromosomeIdGenerator;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.model.SBModelManager;

public class SvpChromosome implements Chromosome  {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpChromosome.class);
    private final SvpModel model;
    private final String name;
    private ObjectId id;
    private ObjectId parentId;
    
    public SvpChromosome(SvpModel model) 
    { 
        this.model = model;  
        this.name = model.getRootNode().getDisplayId() + ":" + model.getWorkspace().getIdentityFactory().getIdentity(model.getRootNode().getContexts()[0]).getDisplayID();
        this.id = new ObjectId();
        this.parentId = null;
    }

    public SvpChromosome(SvpModel model, ObjectId parentId)
    {
        this.model = model;
        this.name = model.getRootNode().getDisplayId() + ":" + model.getWorkspace().getIdentityFactory().getIdentity(model.getRootNode().getContexts()[0]).getDisplayID();
        this.id = new ObjectId();
        this.parentId = parentId;
    }


    public SvpChromosome(SvpModel model, ObjectId id, ObjectId parentId)
    {
        this.model = model;
        this.name = model.getRootNode().getDisplayId() + ":" + model.getWorkspace().getIdentityFactory().getIdentity(model.getRootNode().getContexts()[0]).getDisplayID();
        this.id = id;
        this.parentId = parentId;
    }

    @Override
    public ObjectId getId() {
        return id;
    }

    @Override
    public ObjectId[] getParentIds() {
        return parentId != null ? new ObjectId[] {parentId} : new ObjectId[] {};
    }

    private SvpModel duplicateModel() {
        SBWorkspace ws = model.getRootNode().getWorkspace();

        SBChromosomeManager cManager = Lookup.getDefault().lookup(SBChromosomeManager.class);

        SBIdentityFactory idFactory = ws.getIdentityFactory();

        URI[] oldContexts = getRootNode().getContexts();

        if(oldContexts.length > 1) {
            LOGGER.warn("Creating context from first of {} contexts", oldContexts.length);
        }

        URI newContext = oldContexts.length > 0 ?
                cManager.getNextChromosomeContext(ws.getIdentity(), oldContexts[0]) :
                URI.create("http;//www.synbad.org/defaultContext/1.0");

        URI oldId = getRootNode().getIdentity();

        ws.perform(new DefaultSBDomainBuilder(ws, new URI[]{ newContext })
                .duplicateObject(oldId, oldContexts, true).build());

        URI nextId =//oldId;
                idFactory.getNextIdentity(idFactory.getIdentity(getRootNode().getIdentity())).getIdentity();

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("...creating model....");

        SvpModule newRoot = ws.getObject(nextId, SvpModule.class, new URI[] { newContext });
        SBModelManager m = Lookup.getDefault().lookup(SBModelManager.class);
        SvpModel duplicateModel = m.createModel(SvpModel.class, newRoot);
        //duplicateModel.setCurrentView(duplicateModel.getMainView());
        duplicateModel.getMainView().nodeSet().stream().forEach(n -> duplicateModel.getMainView().expand(n));
        return duplicateModel;
    }

    @Override
    public SvpChromosome duplicate(ObjectId id, ObjectId[] parentId) {
        return new SvpChromosome( duplicateModel(), id, parentId[0] );
    }

    @Override
    public SvpChromosome duplicate() {
        return new SvpChromosome( duplicateModel(), getId());
    }

    public String toSvpString() {
        return getModel().getViewRoot().toString();
    }

    
    public SvpModel getModel()
    {
        return model;
    }

    public SvpModule getRootNode()
    {
        return model.getRootNode();
    }

    public int getPartsSize()
    {
        return getModel().getSvps().size();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SvpChromosome))
            return false;

        SvpChromosome other = (SvpChromosome) obj;
        
        SvpModule thisRoot = getRootNode();
        SvpModule otherRoot = other.getRootNode();
        
        return thisRoot.getIdentity().equals(otherRoot.getIdentity()) &&
                Arrays.equals(thisRoot.getContexts(), otherRoot.getContexts());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(getRootNode().getIdentity());
        for(URI uri : getRootNode().getContexts()) {
            hash = hash + Objects.hashCode(uri);
        }
        return hash;
    }

}
