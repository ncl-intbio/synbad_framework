/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.cache.*;
import org.openide.util.Exceptions;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.ConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBCacheingSimulator;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.svp.parser.SbmlWriter;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

/**
 * 
 * @author owengilfellon
 */

public class SBSvpCopasiConstraintEvaluator extends ASbmlConstraintEvaluator<SvpChromosome> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SBSvpCopasiConstraintEvaluator.class);

    public SBSvpCopasiConstraintEvaluator(
            SBSbmlSimulatorFactory cs, 
            ConstraintHandler handler,
            List<Constraint<TimeCourseResult, Double>> constraints,
            List<SBMLModifier> modifiers,
            int duration, 
            int runtime) {
        super(cs, handler, constraints, modifiers, duration, runtime);
    }

    @Override
    public CFitness evaluate(SvpChromosome c) {

        String svpWrite = Operators.getSvpWrite(c.getModel());


       /* CFitness f = cache.getIfPresent(svpWrite);

        if(f != null) {
            LOGGER.info("Returning cached fitness: {}:{}", f.getFitness(), svpWrite);
            return f;
        }*/

        SBSbmlSimulator s = cs.getSimulator(getDuration(), getRuntime());
        SbmlWriter exporter = new SbmlWriter(c.getRootNode().getWorkspace(), c.getRootNode().getContexts());

        synchronized (SBMLDocument.class) {
            try {
                s.setModel(new SBMLHandler().GetSBML(exporter.getSbmlDocument(c.getModel())));
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }

            if (!s.run())
                return null;
        }
       
        List<TimeCourseTraces> results = SBCacheingSimulator.class.isAssignableFrom(s.getClass()) ?
                ((SBCacheingSimulator)s).getAllResults() : 
                Arrays.asList(s.getResults());
        TimeCourseResult r = new TimeCourseResult(results, c, 0);      
        List<CResult> result = getConstraints().stream().parallel().map(constraint -> new CResult(constraint.toString(), constraint.apply(r))).collect(Collectors.toList());
        CFitness f = new CFitness(
            getHandler().processConstraints(result.stream().parallel().map(ce -> ce.getEvaluation()).collect(Collectors.toList())).getFitness(),
            result
        );

        LOGGER.info("Returning fitness: {}:{}", f.getFitness(), svpWrite);

        return f;
    }
}
