/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import uk.ac.ncl.icos.eaframework.factories.AbstractChromosomeFactory;
import uk.ac.ncl.icos.eaframework.Operator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;

/**
 * A Factory class for generating populations of GRNTree, with or without diversity achieved by application of
 * mutation Operators.
 * @author owengilfellon
 */
public class SvpChromosomeFactory extends AbstractChromosomeFactory<SvpChromosome> implements Serializable {

    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpChromosomeFactory.class);
    /**
     * @param seeds The seed models from which the population is generated
     * @param populationSize The number of instances to add to the population
     */
    public SvpChromosomeFactory(List<SvpChromosome> seeds, int populationSize)
    {
         super(seeds, populationSize);
    }

    /**
     * Generates a population of GRNTree, created by duplicating random GRNTrees from the seed list
     * @return
     */
    @Override
    public List<SvpChromosome> generatePopulation() {
        
        List<SvpChromosome> pop = new ArrayList<>();
        
        LOGGER.debug("Generating population from seeds");
        Random r = new Random();
        for(int i=0; i<POPULATON_SIZE; i++)
        {
            SvpModel tree = seeds.get(r.nextInt(seeds.size())).getModel();
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Adding " + tree.toString());
            pop.add(new SvpChromosome(tree));
        }
        return pop;
    }

    /**
     * Generates a population of GRNTree, created by duplicating random GRNTrees from the seed list, and performing
     * a single mutation on each, chosen at random from the provided operators.
     * @param operators the mutation operators to be applied to the seeds to generate diversity.
     * @return
     */
    @Override
    public List<SvpChromosome> generatePopulation(List<Operator<SvpChromosome>> operators) {
        List<SvpChromosome> pop = new ArrayList<>();
        
        LOGGER.debug("Generating population from seeds and operators");
        Random r = new Random();
        for(int i=0; i<POPULATON_SIZE; i++){
            SvpChromosome c = seeds.get(r.nextInt(seeds.size()));
            Operator<SvpChromosome> o = operators.get(r.nextInt(operators.size()));
            SvpChromosome mutatedTree = o.apply(c);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Applied " + o.getClass().getSimpleName() + " and added " + mutatedTree.toString() );
            pop.add(mutatedTree);
        }
        return pop;
    }
}
