package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.svp.api.SBSvpUtil;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;

/**
 * Randomly selects a TU from within a GRNTree and duplicates it. The duplicate
 * is placed within the same parent as the original.
 * 
 * @author owengilfellon
 */
public class DuplicateTU extends AbstractOperator<SvpChromosome> {
    
    private final SVPManager m = SVPManager.getSVPManager();
    private final static Logger LOGGER = LoggerFactory.getLogger(DuplicateTU.class);
    private boolean singleLayer = true;
    private final static int MAX_ATTEMPTS = 10;
 
    private boolean hasLoop(SvpModule parent, SvpModule child) {
        
        if(parent.getIdentity().equals(child.getIdentity()))
            return true;
    
        SvpModule tmpParent = parent;
        while(tmpParent.getParent() != null) {
            tmpParent = (SvpModule)tmpParent.getParent();
            if(tmpParent.getIdentity().equals(child.getIdentity()))
                return true;
        }
        
        return false;
    }
    
    
    
    public void setIsSingleLayer( boolean singleLayer) {
        this.singleLayer = singleLayer;
    }
    
    public boolean isSingleLayer() {
        return singleLayer;
    }
    

    @Override
    public SvpChromosome apply(SvpChromosome c) {
        
        SvpChromosome t = (SvpChromosome) c.duplicate();
        SvpModule root = t.getRootNode();
        
        List<SvpModule> parentTus = Arrays.asList(root);
        List<SvpModule> childTus = new ArrayList<>(root.getDescendantModules());
        
        // If no TUs, then mutation cannot be performed. return t.
        
        if(childTus.isEmpty()) {
            t.getModel().close();
            return c;
        }
        
        if(!isSingleLayer()) {
            parentTus.addAll(childTus);
        }

        // Select a random TU to duplicate
        
        Random random = new Random();
        int childSize = childTus.size();
        
        // Identify a parent and child without a loop
        
        SvpModule parent = parentTus.get(random.nextInt(parentTus.size()));
        SvpModule child = childTus.get(random.nextInt(childSize));
        
        int attemptCount = 0;
        
        while(hasLoop(parent, child) && attemptCount++ < MAX_ATTEMPTS) {
            child = childTus.get(random.nextInt(childSize));
            if(!isSingleLayer())
                parent = parentTus.get(random.nextInt(childSize));
        }

        if(hasLoop(parent, child)) {
            t.getModel().close();
            return c;
        }
        
        // add child module to parent and return modified chromosome

        LOGGER.debug("Duplicating {} into {}", child.getDisplayId(), parent.getDisplayId());
        parent.addChild(child);
        LOGGER.debug(Operators.getSvpWrite(t.getModel()));
   //     c.getModel().close();
        return t;
    }
}
