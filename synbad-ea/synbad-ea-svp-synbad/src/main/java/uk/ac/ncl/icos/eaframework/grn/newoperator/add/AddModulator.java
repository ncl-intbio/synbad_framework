    package uk.ac.ncl.icos.eaframework.grn.newoperator.add;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortType;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.TuActionsFactory.InteractionMode;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvVo;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpViewPort;


/**
 * Creates a Generator module, which generates a PoPS signal, within an SvpModel.
 * 
 * @author owengilfellon
 */
public class AddModulator  extends AddSequenceModule {
    
    
    @Override
    public SvpChromosome apply(SvpChromosome c) {

        // Create new model
        
        SvpChromosome t = (SvpChromosome) c.duplicate();

        // Get workspace, contexts, and create SBIdentity of new module
        
        SvmVo root = t.getModel().getViewRoot();
        
        root.getView().nodeSet().forEach(root.getView()::expand);
        
        URI[] CONTEXTS = root.getContexts();
        
        LOGGER.debug("{}", String.join(", ", Arrays.asList(CONTEXTS).stream().map(context -> context.toASCIIString()).collect(Collectors.toSet())));
        
        SBWorkspace ws = root.getWorkspace();
        SBIdentityFactory idFactory = ws.getIdentityFactory();

        // Retrieve random parent SVM
        
        SvmVo parent = getRandomParentModule(t.getModel());
        if(parent == null) {
            LOGGER.warn("Could not find parent for new generator");
            t.getModel().close();
            return c;
        }
        
        Integer index = getPositionInParent(parent);
        InteractionMode mode = RANDOM.nextInt(1) == 0 ? InteractionMode.POSITIVE : InteractionMode.NEGATIVE; 
        
        // Create identities
        
        SBIdentity modulatorId = idFactory.getUniqueIdentity(idFactory.getIdentity(
                root.getPrefix(), "modulator", root.getVersion()));
        SBIdentity parentId = idFactory.getIdentity(parent.getSvpModule().getIdentity());
        
        // Perform modification of model
        
        LOGGER.debug("Adding {}:{} to {} at {}", modulatorId.getDisplayID(), mode, parentId.getDisplayID(), index);
        
        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS);
        b = b.createUnit(modulatorId, mode).builder();
        
        if(index == -1)
            b = b.addSvm(parentId, modulatorId, null);
        else
            b = b.addSvm(parentId, modulatorId, index);
        ws.perform(b.build());
        
        // Close old model and return modified model
        
      //  c.getModel().close();
        return t;
    }

    /**
     * 
     * @param model The model to search for a random parent module
     * @return A random SvmVo that does not have an SvpModuleType.
     */    
    protected SvmVo getRandomParentModule(SvpModel model) {
        List<SvmVo> svms = model.getSvms().stream()
            .filter(svm -> svm.getSvpModule().getValue(SynBadTerms.SynBadTu.moduleType) == null)
            .collect(Collectors.toList());
       
        if(svms.isEmpty())
            return null;
        
        return svms.get(RANDOM.nextInt(svms.size()));
        
        
    }
    
    protected int getPositionInParent(SvmVo svm) {
        List<SvpViewPort> openPorts = svm.getNestedPorts(false, SBPortDirection.OUT).stream()
            .filter(p -> p.getObject().getDefinition().getPortType() == SynBadPortType.PoPS)
            .collect(Collectors.toList());
        
        if(openPorts.isEmpty()) {
            LOGGER.warn("No open ports!");
            return -1;
        }
        
        SvpViewPort port = openPorts.get(RANDOM.nextInt(openPorts.size()));
        SvVo owner = port.getOwner(); 
        int index = svm.getView().getParent(owner).getOrderedSequenceComponents().indexOf(owner);
        return index == -1 ? -1 : index + 1;
    }
}