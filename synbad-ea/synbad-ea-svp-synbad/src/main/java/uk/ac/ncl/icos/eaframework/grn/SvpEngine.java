package uk.ac.ncl.icos.eaframework.grn;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.AbstractEvoEngine;
import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.TerminationCondition;

import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.CFitness;
import uk.ac.ncl.icos.eaframework.grn.fitness.SBSvpCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.Strategy;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;
import uk.ac.ncl.icos.eaframework.population.OrderedEvaluationProcess;

/**
 * Implementation of EvoEngine with functionality specific to evolving GRNs. Uses GRNTreeChromosome as a GRN representation.
 *
 * @see SvpChromosome
 * @author owengilfellon
 */
public class SvpEngine extends AbstractEvoEngine<SvpChromosome> {

    /**
     * The Evolutionary Algorithm is instantiated with its necessary components
     *
     * @param cf a ChromosomeFactory for generating populations
     * @param o an Operator (or group of Operators) for applying mutations to the population
     * @param s a Selection operator for selecting parts based on fitness for reproduction
     * @param e a Fitness function for evaluating a solution's behaviour in comparison to some target behaviour
     * @param t a Termination Condition, or group of Termination Conditions, that determine when to exit the algorithm
     */
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpEngine.class);
    private final int POPULATION_SIZE;
    
    public SvpEngine(
            int generation, 
            List<EvaluatedChromosome<SvpChromosome>> initialPopulation, 
            int childPopulationSize, 
            int survivalPopulationSize, 
            ChromosomeFactory<SvpChromosome> cf, 
            Strategy<SvpChromosome> strategy, 
            Evaluator<SvpChromosome> e, 
            TerminationCondition t, 
            int poolSize)
    {
        super(cf, strategy, e, t, poolSize);
        
        LOGGER.info("Reconstructing GRNTreeEngine at generation " + generation);
        // Evaluate population to generate simulation results - which may be
        // required if any of the population survive to the next generation

        this.POPULATION_SIZE = survivalPopulationSize;
        this.currentGeneration = generation;
        
        LOGGER.info("Strategy: {}", evolutionStrategy.getClass().getSimpleName());
        LOGGER.info("Reproduction Selection: {}", evolutionStrategy.getReproduction().getClass().getSimpleName());
        LOGGER.info("Survival Selection: {}", evolutionStrategy.getSurvival().getClass().getSimpleName());
        LOGGER.info("Population Size: {}", POPULATION_SIZE);
        
        this.evaluatedSurvivalPopulation = new OrderedEvaluationProcess<>(
                this.e,
                new EvaluatedSvpChromFactory(),
                survivalPopulationSize,
                poolSize).nextPopulation(initialPopulation);
        
        population = evaluatedSurvivalPopulation.stream().map(p -> p.getChromosome()).collect(Collectors.toList());
    
        population.stream()
            .map(c -> new EvaluatedChromosome<>(c, e.evaluate(c), null))
            .forEach(evaluatedSurvivalPopulation::add);
    }

    public SvpEngine(
            int childPopulationSize, 
            int survivalPopulationSize, 
            ChromosomeFactory<SvpChromosome> cf, 
            Strategy<SvpChromosome> strategy, 
            Evaluator<SvpChromosome> e, 
            TerminationCondition t)
    {
        super(cf, strategy, e, t, 1);
        
        LOGGER.info("Constructing new GRNTreeEngine");

        this.population = cf.generatePopulation();
        
        // for identical population!
        
        CFitness f = null;
        for(int i = 0; i < survivalPopulationSize; i++) {
            if(f == null)
                f = e.evaluate(population.get(i));
            
            evaluatedSurvivalPopulation.add(new EvaluatedSvpChromosome<>(population.get(i),  f, f.getResult(), //f.getSimulationResults(),
                    null));
        }
        
        this.POPULATION_SIZE = survivalPopulationSize;
        
        LOGGER.info("Strategy: {}", evolutionStrategy.getClass().getSimpleName());
        LOGGER.info("Reproduction Selection: {}", evolutionStrategy.getReproduction().getClass().getSimpleName());
        LOGGER.info("Survival Selection: {}", evolutionStrategy.getSurvival().getClass().getSimpleName());
        LOGGER.info("Population Size: {}", POPULATION_SIZE);
    }

    @Override
    public SBSvpCopasiConstraintEvaluator getFitnessEvaluator() {
        return (SBSvpCopasiConstraintEvaluator)super.getFitnessEvaluator();
    }

    @Override
    public void stepForward() {
//        SvpModel m = population.get(0).getModel();
//        LOGGER.info(" Pre [{}] {}", m.getObjectId(), Operators.getSvpWrite(m));
        super.stepForward();
//        m = population.get(0).getModel();
//        LOGGER.info("Post [{}] {}", m.getObjectId(), Operators.getSvpWrite(m));
        population.stream().map(m -> m.getModel()).forEach(m -> LOGGER.info("[{}] {}", m.getId(), Operators.getSvpWrite(m)));
    }

    @Override
    public List<EvaluatedSvpChromosome<SvpChromosome>> getEvaluatedSurvivalPopulation() {
            return super.evaluatedSurvivalPopulation.stream()
                .filter(ec  -> ec instanceof EvaluatedSvpChromosome)
                .map(ec -> (EvaluatedSvpChromosome<SvpChromosome>) ec)
                .collect(Collectors.toList());
    }

    @Override
    public List<EvaluatedChromosome<SvpChromosome>> getEvaluatedReproductionPopulation() {
        return super.evaluatedReproductionPopulation.stream()
                .filter(ec  -> ec instanceof EvaluatedSvpChromosome)
                .map(ec -> (EvaluatedSvpChromosome<SvpChromosome>) ec)
                .collect(Collectors.toList());
    }
}