/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.PopulationProcess;
import uk.ac.ncl.icos.eaframework.SBEvoEngineBuilder;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.Strategy;
import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.population.OrderedEvaluationProcess;
import uk.ac.ncl.icos.eaframework.population.OrderedSelectAndMutateProcess;
import uk.ac.ncl.icos.eaframework.population.SelectAndMutateProcess;
import uk.ac.ncl.icos.eaframework.population.SelectProcess;
import uk.ac.ncl.icos.eaframework.selection.PopulationIteratorSelection;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;

/**
 *
 * @author owengilfellon
 */
public class SVPEngineBuilder implements SBEvoEngineBuilder<SvpChromosome> {
    
    private final Logger LOGGER = LoggerFactory.getLogger(SVPEngineBuilder.class);
    
    private String NAME_PREFIX = "Experiment_";
    
    private int THREAD_POOL_SIZE = 1;
    private int REPRODUCTION_POPULATION_SIZE = 30;
    private int SURVIVAL_POPULATION_SIZE = 10;
    private final int MUTATIONS_PER_GENERATION = 1;
    private int R_MUTATIONS_PER_GENERATION = 1;
    private int S_MUTATIONS_PER_GENERATION = 1;
    
    private int GENERATIONS = 1000;
    
    private boolean isOverlapping = true;
    
    private OperatorGroup<SvpChromosome> operator1 = null;
    private OperatorGroup<SvpChromosome> operator2 = null;
    
    private Selection<SvpChromosome> selection1 = null;
    private Selection<SvpChromosome> selection2 = null;
    
    private ChromosomeFactory<SvpChromosome> factory = null;
    private Evaluator<SvpChromosome> evaluator = null;
    
    
    private boolean ordered = false;
    
    private final List<TerminationCondition> terminationConditions = new LinkedList<>();

    @Override
    public SVPEngineBuilder setChromosomeFactory(ChromosomeFactory<SvpChromosome> factory) {
        this.factory = factory;
        return this;
    }

    @Override
    public SVPEngineBuilder setEvaluator(Evaluator<SvpChromosome> evaluator) {
        this.evaluator = evaluator;
        return this;
    }
    
    @Override
    public SVPEngineBuilder setOverlapping(boolean overlapping) {
        this.isOverlapping = overlapping;
        return this;
    }

    @Override
    public SVPEngineBuilder addTerminationCondition(TerminationCondition condition) {
        terminationConditions.add(condition);
        return this;
    }
    
        @Override
    public SBEvoEngineBuilder<SvpChromosome> setOrderedPopulations(boolean ordered) {
        this.ordered = ordered;
        return this;
    }

    @Override
    public SVPEngineBuilder setReproductionPopulationSize(int populationSize) {
        this.REPRODUCTION_POPULATION_SIZE = populationSize;
        return this;
    }

    @Override
    public SVPEngineBuilder setReproductionOperators(OperatorGroup operators) {
        this.operator1 = operators;
        return this;
    }

    @Override
    public SVPEngineBuilder setReproductionSelection(Selection<SvpChromosome> selection) {
        this.selection1 = selection;
        return this;
    }

    @Override
    public SVPEngineBuilder setSurvivalPopulation(int populationSize) {
        this.SURVIVAL_POPULATION_SIZE = populationSize;
        return this;
    }

    @Override
    public SVPEngineBuilder setSurvivalOperators(OperatorGroup operators) {
        this.operator2 = operators;
        return this;
    }

    @Override
    public SVPEngineBuilder setSurvivalSelection(Selection<SvpChromosome> selection) {
        this.selection2 = selection;
        return this;
    }


    @Override
    public SBEvoEngineBuilder<SvpChromosome> setReproductionMutationsPerGeneration(int mutations) {
        this.R_MUTATIONS_PER_GENERATION = mutations;
        return this;
    }

    @Override
    public SBEvoEngineBuilder<SvpChromosome> setSurvivalMutationsPerGeneration(int mutations) {
        this.S_MUTATIONS_PER_GENERATION = mutations;
        return this;
    }


    @Override
    public SBEvoEngineBuilder<SvpChromosome> setCurrentGeneration(int generation) {
        this.GENERATIONS = generation;
        return this;
    }

    @Override
    public SBEvoEngineBuilder<SvpChromosome> setCurrentPopulation(List<EvaluatedChromosome<SvpChromosome>> population) {
        return null;
    }

    private boolean check(String name, Object object) {
        if(object == null) {
            LOGGER.error("{} cannot be null", name);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public SBEvoEngineBuilder<SvpChromosome> setThreads(int threads) {
        this.THREAD_POOL_SIZE = threads;
        return this;
    }
    
    @Override
    public EvoEngine<SvpChromosome> build() {
        
        if( !check("Selection 1", selection1) || !check("Selection 2", selection2) ||
                !check("Evaluator", evaluator) || !check("Chromosome Factory", factory))
            return null;
        
        if( operator1 == null && operator2 == null) 
            LOGGER.warn("No evolutionary operators set");
        
        TerminationCondition t = terminationConditions.isEmpty() ? 
                new GenerationTermination(GENERATIONS) : 
                new TerminationGroup(terminationConditions);
        
        PopulationProcess<SvpChromosome> reproduction = 
            ordered ? 
                (operator1 == null) ?
                    new OrderedEvaluationProcess<>(evaluator, new EvaluatedSvpChromFactory(), REPRODUCTION_POPULATION_SIZE, THREAD_POOL_SIZE) :
                    new OrderedSelectAndMutateProcess<>(
                            new PopulationIteratorSelection<>(), operator1, evaluator, new EvaluatedSvpChromFactory<>(), 
                            REPRODUCTION_POPULATION_SIZE, this.R_MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE) :
                (operator1 == null) ?
                    new SelectProcess<>(selection1, REPRODUCTION_POPULATION_SIZE) :
                    new SelectAndMutateProcess<>(
                            selection1, operator1, evaluator, new EvaluatedSvpChromFactory<>(), 
                            REPRODUCTION_POPULATION_SIZE, this.R_MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
        
        PopulationProcess<SvpChromosome> survival = 
                ordered ?
                new SelectProcess<>( new PopulationIteratorSelection<>(), SURVIVAL_POPULATION_SIZE) :
                (operator2 == null) ?
                    new SelectProcess<>(selection2, SURVIVAL_POPULATION_SIZE) :
                    new SelectAndMutateProcess<>(
                            selection2, operator2, evaluator, new EvaluatedSvpChromFactory<>(),
                            SURVIVAL_POPULATION_SIZE, this.S_MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
            
        Strategy<SvpChromosome> strategy = isOverlapping ? 
            new Strategy.Overlapping<>(reproduction, survival) :
            new Strategy.NonOverlapping<>(reproduction, survival);
        
        EvoEngine<SvpChromosome> svpEngine = new SvpEngine(
                REPRODUCTION_POPULATION_SIZE, SURVIVAL_POPULATION_SIZE, factory, strategy, evaluator, t);
        
        return svpEngine;
    }


    
}
