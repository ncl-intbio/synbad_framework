package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.Random;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.BasicMarker;
import org.slf4j.helpers.BasicMarkerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Parameter;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvpVo;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;


/**
 * Identifies all Constitutive Promoters within the GRNTree, and replaces one
 * at random with a randomly chosen constitutive promoter from the repository.
 * 
 * @author owengilfellon
 */
@EAModule(visualName = "Randomise Constitutive Promoter")
public class RandomiseConstPromoter extends AbstractOperator<SvpChromosome> {
    
    private static final String[] CONSTITUTIVE_PROPERTIES = new String[] { "Constitutive SigA Promoter", "ConstitutiveSigAPromoter", "SigA Promoter", "SigAPromoter" };
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RandomiseRBS.class);
    private static final SVPManager SVP_MANAGER = SVPManager.getSVPManager();
    private final SBWorkspaceManager WORKSPACE_MANAGER = Lookup.getDefault().lookup(SBWorkspaceManager.class);
    
    private boolean isPrototype = false;
    private final Random RANDOM = new Random();
    
    
    @Override
    public SvpChromosome apply(SvpChromosome c) {
        
        SvpChromosome t = c.duplicate();
        SvpVo promoterToReplace = Operators.getRandomPart(t.getModel(), ComponentRole.Promoter, CONSTITUTIVE_PROPERTIES);

        if(isPrototype) {
            
            // if prototype, mutate chromosome by changing RBS parameter
            
            Svp svp = promoterToReplace.getSvp();
            Parameter ktr =  svp.getInternalInteractions().stream()
                .flatMap(i -> i.getParameters().stream())
                .filter(p -> p.getParameterType().equalsIgnoreCase("ktr"))
                .findFirst().orElse(null);
           // ktr.setParameterValue(getModifiedParameter(ktr));
            c.getModel().close();
            return t;
            
        } else {
            
            // if repository-based, mutate chromosome by replacing SVP

            if(!Operators.replaceSvp(t.getModel(), promoterToReplace, SVP_MANAGER.getConstPromoter())) {
                LOGGER.warn("No actions to perform for {}", t.getRootNode());
                t.getModel().close();
                return c;
            }

           // c.getModel().close();
            return t;
        }
    }
     
    private double getModifiedParameter(Parameter par) {
        
    
        // multiply by a random number with guassian distribution (mean 1.0, SD 0.3)
        
        double multiplier = RANDOM.nextGaussian() * 0.3 + 1.0;
        double newParameter = par.getParameterValue() * multiplier;
        newParameter = newParameter < 0 ? 0 : newParameter > 1 ? 1 : newParameter;
        return newParameter;
    }

    public boolean isPrototype() {
        return this.isPrototype;
    }

    public void setIsPrototype(boolean prototype) {
        this.isPrototype = prototype;
    }
}
