    package uk.ac.ncl.icos.eaframework.grn.newoperator.add;

import java.net.URI;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;


/**
 * Creates a Generator module, which generates a PoPS signal, within an SvpModel.
 * 
 * @author owengilfellon
 */
public class AddGenerator  extends AddSequenceModule {

    @Override
    public SvpChromosome apply(SvpChromosome c) {
        
        //SBDataDefManager m = SBDataDefManager.getManager();
        
        // Create new model
        
        SvpChromosome t = (SvpChromosome) c.duplicate();

        // Get workspace, contexts, and create SBIdentity of new module
        
        SvmVo root = t.getModel().getViewRoot();
        
        
        root.getView().nodeSet().forEach(root.getView()::expand);
        
        URI[] CONTEXTS = root.getContexts();
        SBWorkspace ws = root.getWorkspace();
        SBIdentityFactory idFactory = ws.getIdentityFactory();

        // Retrieve random parent SVM
        
        SvmVo parent = getRandomParentModule(t.getModel());
        if(parent == null) {
            LOGGER.warn("Could not find parent for new generator");
            t.getModel().close();
            return c;
        }
        
        Integer index = getPositionInParent(parent, false);
        
        // Create identities
        
        SBIdentity generatorId = idFactory.getUniqueIdentity(idFactory.getIdentity(
                root.getPrefix(), "generator", root.getVersion()));
        SBIdentity parentId = idFactory.getIdentity(parent.getSvpModule().getIdentity());
        
        // Perform modification of model
        
        LOGGER.debug("Adding {} to {} at {}", generatorId.getDisplayID(), parentId.getDisplayID(), index);
        
        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS);
        b = b.createGenerator(generatorId).builder();
        
        if(index == -1)
            b = b.addSvm(parentId, generatorId, null);
        else
            b = b.addSvm(parentId, generatorId, index);
        ws.perform(b.build());
        
        // Close old model and return modified model
        
     //   c.getModel().close();
        return t;
    }
}