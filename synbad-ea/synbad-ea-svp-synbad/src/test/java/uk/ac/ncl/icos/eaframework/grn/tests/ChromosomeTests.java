package uk.ac.ncl.icos.eaframework.grn.tests;

import java.net.URI;

import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.junit.MockServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.svp.example.ExampleModuleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;

/**
 * 
 * @author owengilfellon
 */
public class ChromosomeTests {
    
    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/operator_tests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/operator_tests/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(ChromosomeTests.class);

    private SBWorkspace ws;

    @BeforeClass
    public static void setUpClass() { } 
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() {
        MockServices.setServices(
                DefaultSBModelFactory.class,
                SvpModelProvider.class);

        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new EnvConstantUpdater())
                .addService(new PiUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();

        //this.ws.createContext(CONTEXTS[0]);
    }
    
    @After
    public void tearDown() {
        ws.shutdown();
        this.ws = null;
    }
    
    @Test
    public void testDuplicateRepressilator() {
        testModel(ExampleModuleFactory.createRepressilator(ws, CONTEXTS));
    }

    @Test
    public void testDuplicateAutoRegulation() {
        testModel(ExampleModuleFactory.createAutoRegulation(ws, CONTEXTS));
    }

    @Test
    public void testDuplicateGeneratorModulatorSink() {
        testModel(ExampleModuleFactory.createGeneratorModulatorSink(ws, CONTEXTS));
    }

    @Test
    public void testDuplicateAutoRegulationComplex() {
        testModel(ExampleModuleFactory.createAutoRegulationUsingComplex(ws, CONTEXTS));
    }

    public void testModel(SvpModel model) {
        SvpChromosome chromosome = new SvpChromosome(model);
        SvpChromosome chromosome2 = chromosome.duplicate();

        URI[] context1 = chromosome.getRootNode().getContexts();
        URI[] context2 = chromosome2.getRootNode().getContexts();

        Collection<SBValued> objs = ws.getObjects(SBValued.class, context1);
        Collection<SBValued> objs2 = ws.getObjects(SBValued.class, context2);

        Assert.assertEquals("All objects should be in new context", objs.size(), objs2.size());
        Assert.assertEquals("Should be same statements in duplicate systems context",
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(context1)).stream().count(),
                ws.getRdfService().getStatements(null, null, null, ws.getSystemContextIds(context2)).stream().count());
        Assert.assertEquals("Should be same statements in duplicate context",
                ws.getRdfService().getStatements(null, null, null, context1).stream().count(),
                ws.getRdfService().getStatements(null, null, null, context2).stream().count());

        Assert.assertNotEquals("Root nodes should not be identical", chromosome.getRootNode(), chromosome2.getRootNode());
        Assert.assertEquals("Persistent IDs of root nodes should be identical", chromosome.getRootNode().getPersistentId(), chromosome2.getRootNode().getPersistentId());
        Assert.assertEquals("Number of parts in models should be identical", chromosome.getModel().getSvps().size(), chromosome2.getModel().getSvps().size());
    }
}
