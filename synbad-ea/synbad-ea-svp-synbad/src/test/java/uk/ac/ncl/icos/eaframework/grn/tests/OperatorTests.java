package uk.ac.ncl.icos.eaframework.grn.tests;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.net.URI;

import org.netbeans.junit.MockServices;
import uk.ac.ncl.icos.eaframework.Operator;

import javax.swing.JFrame;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddGenerator;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddModulator;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddSink;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddSmlMolModulator;
import uk.ac.ncl.icos.eaframework.grn.operator.DuplicateTU;
import uk.ac.ncl.icos.eaframework.grn.operator.RandomiseConstPromoter;
import uk.ac.ncl.icos.eaframework.grn.operator.RandomiseRBS;
import uk.ac.ncl.icos.eaframework.grn.operator.SwapParts;
import uk.ac.ncl.icos.synbad.flow.object.impl.SBWire;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.model.DefaultSBModelFactory;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModelProvider;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.svp.view.obj.SvmVo;
import uk.ac.ncl.icos.synbad.view.visualisation.SvpGraphStreamPanel;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;

/**
 * 
 * @author owengilfellon
 */
public class OperatorTests {
    
    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/operator_tests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/operator_tests/1.0") };
    private final static Logger LOGGER = LoggerFactory.getLogger(OperatorTests.class);
    
    private final Operator<SvpChromosome> ADD_GENERATOR = new AddGenerator();
    private final Operator<SvpChromosome> ADD_MODULATOR = new AddModulator();
    private final Operator<SvpChromosome> ADD_SML_MOL_MODULATOR = new AddSmlMolModulator();
    private final Operator<SvpChromosome> ADD_SINK = new AddSink();
    private final Operator<SvpChromosome> RANDOMISE_CONST = new RandomiseConstPromoter();
    private final Operator<SvpChromosome> RANDOMISE_RBS = new RandomiseRBS();
    private final Operator<SvpChromosome> SWAP_PARTS = new SwapParts();
    private final Operator<SvpChromosome> DUPLICATE_TU = new DuplicateTU();

    private SBWorkspace ws;
    
    private SvpChromosome evolved;
  
    @BeforeClass
    public static void setUpClass() { } 
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { 
        this.ws = new SBWorkspaceBuilder(WORKSPACE_ID)
                .addService(new EnvConstantUpdater())
                .addService(new PiUpdater())
                .addService(new SviUpdater())
                .addService(new ExportedFcUpdater())
                .addService(new SBWireUpdater()).build();
    }
    
    @After
    public void tearDown() {
        ws.shutdown();
        this.ws = null;
    }
    
    @Test
    public void testSwapParts()
    { 
        LOGGER.debug("--- Testing SwapParts ---");
        
        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, ExampleFactory.setupWorkspace(ws, CONTEXTS));
        
        SvpChromosome chromosome = new SvpChromosome(model);
        int chromosomeSize = chromosome.getModel().getSvps().size();
        chromosome.getModel().getMainView().nodeSet().forEach(chromosome.getModel().getMainView()::expand);
        
        LOGGER.debug("----Applying operator...");
        
        SvpChromosome chromosome2 = SWAP_PARTS.apply(chromosome);
        int chromosomeSize2 = chromosome2.getModel().getSvps().size();

        Assert.assertEquals("No parts should have been added or removed", chromosomeSize, chromosomeSize2);
    }

    @Test
    public void testOperators()
    {  
        LOGGER.debug("--- Testing Operators ---");

        SvpModel model = Lookup.getDefault().lookup(SBModelManager.class).createModel(SvpModel.class, ExampleFactory.setupWorkspace());

        SvpChromosome chromosome = new SvpChromosome(model);
        chromosome.getModel().getMainView().nodeSet().forEach(chromosome.getModel().getMainView()::expand);

        int svpCount1 = chromosome.getModel().getSvps().size();
        int svmCount1 = chromosome.getModel().getSvms().size();

        debugSvmWiring(0, chromosome.getModel().getViewRoot());
        
        LOGGER.debug("----Applying operator...");
        
        SvpChromosome chromosome2 = ADD_GENERATOR.apply(chromosome);
       
        int svpCount2 = chromosome2.getModel().getSvps().size();
        int svmCount2 = chromosome2.getModel().getSvms().size();

        int wiresObjectCount2 = ws.getObjects(SBWire.class, chromosome2.getModel().getRootNode().getContexts()).size();
        long wiresVoCount2 = chromosome2.getModel().getSvms().stream().flatMap(svm -> svm.getNestedWires().stream()).count();

        debugSvmWiring(0, chromosome2.getModel().getViewRoot());

        Assert.assertNotEquals("Parts should have been added", svpCount1, svpCount2);
        
        LOGGER.debug("----Applying operator...");
        
        SvpChromosome chromosome3 = ADD_MODULATOR.apply(chromosome2);
        // chromosome3.getModel().getMainView().nodeSet().forEach(chromosome3.getModel().getMainView()::expand);

        int svpCount3 = chromosome3.getModel().getSvps().size();
        long svmCount3 = chromosome3.getModel().getSvms().size();

        int wiresObjectCount3 = ws.getObjects(SBWire.class, chromosome3.getModel().getRootNode().getContexts()).size();
        long wiresVoCount3 = chromosome3.getModel().getSvms().stream().flatMap(svm -> svm.getNestedWires().stream()).count();

        debugSvmWiring(0, chromosome3.getModel().getViewRoot());
     
        Assert.assertNotEquals("SVMs should have been added", svmCount2, svmCount3);
       // Assert.assertNotEquals("Wires should have been added", wiresObjectCount2, wiresObjectCount3);
     //   Assert.assertNotEquals("View Wires should have been added", wiresVoCount2, wiresVoCount3);
        Assert.assertNotEquals("Parts should have been added", svpCount2, svpCount3);
        
        LOGGER.debug("----Applying operator...");
        
        SvpChromosome chromosome4 = ADD_SML_MOL_MODULATOR.apply(chromosome3);
        // chromosome4.getModel().getMainView().nodeSet().forEach(chromosome4.getModel().getMainView()::expand);
        
        int chromosomeSize4 = chromosome4.getModel().getSvps().size();
        int wiresObjects4 = ws.getObjects(SBWire.class, chromosome4.getModel().getRootNode().getContexts()).size();
        long wiresViewObjects4 = chromosome4.getModel().getSvms().stream().flatMap(svm -> svm.getNestedWires().stream()).count();
        long svms4 = chromosome4.getModel().getSvms().size();
        debugSvmWiring(0, chromosome4.getModel().getViewRoot());
        
        LOGGER.debug("----Applying operator...");
        
        SvpChromosome chromosome5 = ADD_SINK.apply(chromosome4);
        debugSvmWiring(0, chromosome5.getModel().getViewRoot());
        
//        SvpChromosome chromosome6 = RANDOMISE_CONST.apply(chromosome5);
//        debugSvmWiring(0, chromosome6.getModel().getViewRoot());
//        
//        SvpChromosome chromosome7 = RANDOMISE_RBS.apply(chromosome6);
//        debugSvmWiring(0, chromosome7.getModel().getViewRoot());

        LOGGER.debug("----Applying operator...");

        SvpChromosome chromosome8 = SWAP_PARTS.apply(chromosome5);
        debugSvmWiring(0, chromosome8.getModel().getViewRoot());

        LOGGER.debug("----Applying operator...");
        
        SvpChromosome chromosome9 = DUPLICATE_TU.apply(chromosome8);
        debugSvmWiring(0, chromosome9.getModel().getViewRoot());
        
        this.evolved = chromosome9;
       // render(chromosome9);
        
    }
    
//    public static void main(String[] args) {
//        OperatorTests test = new OperatorTests();
//        test.setUp();
//        test.testOperators();
//        test.render(test.evolved);
//        test.tearDown();
//
//    }
    
     private void render(SvpChromosome chromosome) {

        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        
        SBModelManager f = Lookup.getDefault().lookup(SBModelManager.class);
        SvpGraphStreamPanel panel = new SvpGraphStreamPanel();
        panel.setView(chromosome.getModel().getMainView().getRoot(), chromosome.getModel().getMainView());
        
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        panel.run();
    }
    
    private void debugSvmWiring(int indent, SvmVo svm) {

        StringBuilder tab = new StringBuilder();
        for(int i = 0; i < indent; i++) {
            tab.append("\t");
        }
        String tabString = tab.toString();

        LOGGER.trace("{}{}: {}", tabString, svm.getClass().getSimpleName(), svm.getDisplayId());

        svm.getPorts(false, SBPortDirection.IN).forEach(p -> {
            LOGGER.trace("{}\t-IN: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(true, SBPortDirection.IN).forEach(p -> {
            LOGGER.trace("{}\t+IN: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(false, SBPortDirection.OUT).forEach(p -> {
            LOGGER.trace("{}\t-OUT: {}", tabString, p.getDisplayId());
        });

        svm.getPorts(true, SBPortDirection.OUT).forEach(p -> {
            LOGGER.trace("{}\t+OUT: {}", tabString, p.getDisplayId());
        });

        svm.getNestedWires().forEach(w -> {
            LOGGER.trace("{}\tWIRE: {} -> {}", tabString, w.getFrom().getDisplayId(), w.getTo().getDisplayId());
        });

        svm.getOrderedSequenceComponents().stream().forEach(c -> {

            if(c.is(Module.TYPE) || c.is(ModuleDefinition.TYPE)) {
                debugSvmWiring(indent + 1, (SvmVo)c);
            } else {
                LOGGER.trace("{}\t{}: {}", tabString, c.getClass().getSimpleName(), c.getDisplayId());
            }
        });
    }
}
