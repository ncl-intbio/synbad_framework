/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.tests;

import java.net.URI;
import java.util.*;

import org.junit.*;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.grn.constraint.LinearityConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.LowerBoundConstraint;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.SVPEngineBuilder;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.SvpChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.constraint.RatioConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.UpperBoundConstraint;
import uk.ac.ncl.icos.eaframework.grn.fitness.SBSvpCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddGenerator;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddModulator;
import uk.ac.ncl.icos.eaframework.grn.newoperator.add.AddSink;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournamnet;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.eaframework.observer.SimplePopObserver;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfMemService;
import uk.ac.ncl.icos.synbad.svp.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.service.*;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.icos.synbad.model.SBModelManager;

/**
 *
 * @author owengilfellon
 */
public class SvpEngineBuilderTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(SvpEngineBuilderTest.class);

    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/ea_tests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/ea_tests/1.0") };

    private final SBModelManager MODEL_FACTORY = Lookup.getDefault().lookup(SBModelManager.class);

    private final int GENERATIONS = 1;

    String INDEPENDENT_METABOLITE = ExampleFactory.C_SUBTILIN.getDisplayID();
    String DEPENDENT_METABOLITE = ExampleFactory.P_GFP.getDisplayID();

    double TARGET_RATIO = 0.14918591815273405;
    double TARGET_LOWER = 0.0;
    double TARGET_UPPER = 21428.5714;
    int MAX_PARTS = 30;
    int MAX_UNPENALISEDPARTS = 10;

    int REPRODUCTION_POPULATION_SIZE = 5;
    int SURVIVAL_POPULATION_SIZE = 5;

    int MUTATIONS_PER_GENERATION = 1;

    int THREADS = SURVIVAL_POPULATION_SIZE;

    double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    int INCREMENT = 50;
    int DURATION = 21600;
    int STEPS = 2160;
    
    public SvpEngineBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void  testBuild() {
        EvoEngine<SvpChromosome> evoEngine = getEngine();
        for(int i = 1; i <= GENERATIONS; i++) {
            evoEngine.stepForward();
        }
    }

    private EvoEngine<SvpChromosome> getEngine() {

        SBWorkspace ws = new SBWorkspaceBuilder(WORKSPACE_ID)
            //.addRdfService(new DefaultSBRdfDiskService(URI.create("http://synbad.org/ea_experiments/1.0")))
             .addRdfService(new DefaultSBRdfMemService())
//            .addService(new EnvConstantUpdater())
//            .addService(new PiUpdater())
//            .addService(new SviUpdater())
//            .addService(new ExportedFcUpdater())
//            .addService(new SBWireUpdater())
            .build();

        SvpModule rootSvm = ExampleFactory.setupWorkspace(ws, CONTEXTS);
        SvpModel model = MODEL_FACTORY.createModel(SvpModel.class, rootSvm);
        Svp pspark = ws.getObject(ExampleFactory.P_PSPARK.getIdentity(), Svp.class, rootSvm.getContexts());
        pspark.createProperty("type", "ConstitutiveSigAPromoter", "Constitutive SigA Promoter");

        SvpChromosome seed = new SvpChromosome(model);

        Evaluator<SvpChromosome> evaluator = new SBSvpCopasiConstraintEvaluator(
                (int duration, int runtime) -> {
                    DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, 10000, INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE);
                    return cs; },
                new MultiplyHandler(),
                Arrays.asList(
                    new LinearityConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE),
                    new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, 1.0),
                    new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_UPPER),
                    new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO)),
                Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel()),
                DURATION,
                STEPS);

        EvoEngine<SvpChromosome> evoEngine = new SVPEngineBuilder()
                .setChromosomeFactory(new SvpChromosomeFactory(Collections.singletonList(seed), SURVIVAL_POPULATION_SIZE))
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(true)
                .setReproductionPopulationSize(REPRODUCTION_POPULATION_SIZE)
                .setReproductionSelection(new Uniform<>())
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
                        new RandomiseRBS(),
                        new RandomiseConstPromoter(),
                        new SwapParts(),
                        new AddGenerator(),
                        new AddModulator(),
                        new AddSink()
//                        new AddComplexModulator(),
//                        new AddSmlMolModulator(),
//                        new RemoveTU()
                        // new DuplicateTU()
                        // new DuplicateParts(),
                    ), SvpChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(SURVIVAL_POPULATION_SIZE)
                 //.setSurvivalSelection(new MutantChamp<>())
                .setSurvivalSelection(new FitnessProportionalTournamnet<>(false, SURVIVAL_POPULATION_SIZE))
                //.setSurvivalSelection(new Uniform<>())
                .build();

        EvolutionObserver o = new SimplePopObserver();
        evoEngine.attach(o);

        return evoEngine;
    }
}
 