package uk.ac.ncl.icos.eaframework.grn.booleannetwork.ActivationFunctions;

import uk.ac.ncl.icos.eaframework.grn.booleannetwork.ActivationFunction;
import uk.ac.ncl.icos.eaframework.grn.booleannetwork.BooleanNode;

/**
 * Created by owengilfellon on 03/07/2014.
 */
public class And implements ActivationFunction {

    private BooleanNode input1;
    private BooleanNode input2;

    public And(BooleanNode input1, BooleanNode input2)
    {
        this.input1 = input1;
        this.input2 = input2;
    }

    @Override
    public boolean assess() {
        return (input1.getState() && input2.getState());
    }
}
