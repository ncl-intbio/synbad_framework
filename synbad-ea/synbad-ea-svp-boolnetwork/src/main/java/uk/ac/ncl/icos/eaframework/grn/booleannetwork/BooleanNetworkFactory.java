package uk.ac.ncl.icos.eaframework.grn.booleannetwork;

import org.jgrapht.DirectedGraph;
import uk.ac.ncl.icos.eaframework.grn.exporter.GeneNetworkExporter;
import uk.ac.ncl.icos.grntree.api.GRNTree;

import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Created by owengilfellon on 16/06/2014.
 */
public class BooleanNetworkFactory {

    public static GRNBooleanNetwork getBooleanNetwork(GRNTreeChromosome t)
    {
       // List<BooleanNode> nodes = new ArrayList<BooleanNode>();
        GeneNetworkExporter exporter = new GeneNetworkExporter();
        DirectedGraph<  GeneNetworkExporter.ExportNodeWrapper,
                        GeneNetworkExporter.ExportEdgeWrapper> network = exporter.export(t);
        Set<GeneNetworkExporter.ExportNodeWrapper> nodes = network.vertexSet();

        Set<BooleanNode> nodesSet = new HashSet<BooleanNode>();

        for(GeneNetworkExporter.ExportNodeWrapper node:nodes) {
            
        }

        return null;

    }

}
