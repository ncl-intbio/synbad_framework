package uk.ac.ncl.icos.eaframework.grn.booleannetwork;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by owengilfellon on 03/07/2014.
 */
public class GRNBooleanNetwork  {

    private List<BooleanNode> nodes;

    public GRNBooleanNetwork(List<BooleanNode> nodes)
    {
        this.nodes = nodes;
    }

    public void stepForward()
    {
        List<BooleanNode> nextNodes = new ArrayList<BooleanNode>();
        for(BooleanNode node:nodes) {
            nextNodes.add(new BooleanNode(node.nextState(), node.getActivationFunction(), node.getId()));
        }
        nodes = nextNodes;
    }

    public List<BooleanNode> getNodes()
    {
        return nodes;
    }


}
