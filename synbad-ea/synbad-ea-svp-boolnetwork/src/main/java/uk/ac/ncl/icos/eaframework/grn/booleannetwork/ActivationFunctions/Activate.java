package uk.ac.ncl.icos.eaframework.grn.booleannetwork.ActivationFunctions;

import uk.ac.ncl.icos.eaframework.grn.booleannetwork.ActivationFunction;
import uk.ac.ncl.icos.eaframework.grn.booleannetwork.BooleanNode;

/**
 * Created by owengilfellon on 03/07/2014.
 */
public class Activate implements ActivationFunction {

    private BooleanNode activator;

    public Activate(BooleanNode activator)
    {
        this.activator = activator;
    }

    @Override
    public boolean assess() {
        return activator.getState();
    }
}
