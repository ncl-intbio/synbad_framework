/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.chromosome;

import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.constraint.ConstraintHelper;
import uk.ac.ncl.icos.eaframework.grn.constraint.GradedModelSizeConstraint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.constraint.LowerBoundConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.UpperBoundConstraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

/**
 *
 * @author owengilfellon
 */
public class ConstraintTests {
    
    private static final Logger logger = LoggerFactory.getLogger(ConstraintTests.class);
    
    public ConstraintTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void sigmoidTests() {

        for(int i = -10; i < 10; i ++) {
            logger.debug("{}: {}", i, ConstraintHelper.sigmoid(i, 3.0, 24.0));
        }
    }

    /**
     * Test of apply method, of class LowerBoundConstraint.
     */
    @Test
    public void lowerBoundConstraintTests() {
        for(int i = -10; i < 10; i ++) {
            List<TimeCourseTraces> results = new ArrayList<>();
            results.add(new TimeCourseTraces(
                    Arrays.asList("GFP", "Subtilin"), 
                    new double[][] { 
                        {i}, 
                        {i} }));
            
            LowerBoundConstraint instance = new LowerBoundConstraint("Subtilin", "GFP", 0, 1.0);
            Double result = instance.apply(new TimeCourseResult(results, null, 1));
            logger.debug("Result: {}", result);
        }
    }

    @Test
    public void upperBoundConstraintTests() {
        for(int j = 0; j < 10; j +=10) {
            for(int i = -100; i < 1000; i += 100) {
                List<TimeCourseTraces> results = new ArrayList<>();
                results.add(new TimeCourseTraces(Arrays.asList("Subtilin","GFP"), new double[][] { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {i, i, i, i, i, i, i, i, i, i} }));
                UpperBoundConstraint instance = new UpperBoundConstraint("Subtilin", "GFP", j);
                Double result = instance.apply(new TimeCourseResult(results, null, 1));
                logger.debug("Current: {} | Target: {} | Result: {}", i, j, result);
            }
        }
    }

    @Test
    public void modelSizeConstraintTests() {

        List<TimeCourseTraces> results = new ArrayList<>();

        GRNTree tree = GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; "
                        + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter");
        GRNTreeChromosome c = new GRNTreeChromosome(tree);
        for(int i = 0; i < 20; i += 1) {
            GradedModelSizeConstraint instance = new GradedModelSizeConstraint(Math.max(i - 5, 0), i);
            Double result = instance.apply(new TimeCourseResult(results, c, 1));
            logger.debug("Current: {} | Target: {} | Result: {}",  10,i, result);
        }
    }
}
