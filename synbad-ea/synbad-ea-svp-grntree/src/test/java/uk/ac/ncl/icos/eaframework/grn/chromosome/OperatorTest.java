/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.chromosome;

import org.junit.*;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngineBuilder;
import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPResponseCurveObserver;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.AAddRegulation;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.ARandomiseConstPromoter;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.ARandomiseParameter;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.SVPParameterType;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfMemService;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class OperatorTest {

    private static final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private static final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();

    SBWorkspace workspace;

    public OperatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
    @Test
    public void testAddRegulation() {

        Operator<GRNTreeChromosome> o = new AddRegulation();

        GRNTreeChromosome c1 = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
            "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
            "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter"));

        System.out.println(c1.toString());

        for(int i = 0; i < 5; i++) {
            c1 = o.apply(c1);
            System.out.println(c1.toString());
        }
    }

}
