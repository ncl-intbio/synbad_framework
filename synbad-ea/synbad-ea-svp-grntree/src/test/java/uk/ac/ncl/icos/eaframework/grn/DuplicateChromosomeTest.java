/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import org.bson.types.ObjectId;
import org.junit.*;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPResponseCurveObserver;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.ARandomiseParameter;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.SVPParameterType;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
public class DuplicateChromosomeTest {


    String INDEPENDENT_METABOLITE = "Subtilin";
    String DEPENDENT_METABOLITE = "GFP_rrnb";

    int GENERATIONS = 1;

    int POPULATION_SIZE_1 = 5;
    int POPULATION_SIZE_2 = 5;

    boolean OVERLAPPING = false;
    int MUTATIONS_PER_GENERATION = 1;

    int MAX_PARTS = 30;
    int MAX_UNPENALISEDPARTS = 20;

    double TARGET_RATIO = 0.14918591815273405;
    double TARGET_LOWER = 0.0;
    double TARGET_UPPER = 21428.5714;

    double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    int INCREMENT = 50;

    int DURATION = 21600;
    int STEPS = 1080;

    int THREADS = POPULATION_SIZE_2;//(int)Math.ceil((double)POPULATION_SIZE_2 / 2.0);


    private static final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private static final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();

    SBWorkspace workspace;

    public DuplicateChromosomeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
    //@Test
    public void testDuplicatePopulation() throws Exception {

        GRNTree tree = GRNTreeFactory.getGRNTree();

        GRNTreeNode tu1 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu3 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);

        GRNTreeNode operator1 = SVP_HELPER.getOperator("opB");
        GRNTreeNode operator2 = SVP_HELPER.getOperator("opF");
        GRNTreeNode operator3 = SVP_HELPER.getOperator("opJ");

        GRNTreeNode cds1 = SVP_HELPER.getCDS("cdsD", 0.0012);
        GRNTreeNode cds2 = SVP_HELPER.getCDS("cdsH", 0.0012);
        GRNTreeNode cds3 = SVP_HELPER.getCDS("cdsL", 0.0012);

        tu1.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promA", 0.00238),
                operator1,
                SVP_HELPER.getRBS("rbsC", 0.314),
                cds1,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu2.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promE", 0.00238),
                operator2,
                SVP_HELPER.getRBS("rbsG", 0.314),
                cds2,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu3.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promI", 0.00238),
                operator3,
                SVP_HELPER.getRBS("rbsK", 0.314),
                cds3,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tree.getRootNode().addNode(tu1);
        tree.getRootNode().addNode(tu2);
        tree.getRootNode().addNode(tu3);

        // Add Parts to TUs

        // Add promoter regulation

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator2.getSVP(), cds1.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 0.042));

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator3.getSVP(), cds2.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 0.042));

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator1.getSVP(),cds3.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 0.042));

        List<GRNTreeChromosome> seeds = Arrays.asList(new GRNTreeChromosome(tree));

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = SimpleCopasiSimulator::new;

        ConstraintHandler handler = new MultiplyHandler();
        // ConstraintHandler handler = new SummationHandler();
        // ConstraintHandler handler = new LowestConstraintHandler();

        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
                new OscillationConstraint("cdsL", 100, 125),
//                new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, 1.0),
//                new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_UPPER),
//                new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO),
                new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));

        List<SBMLModifier> modifiers = Collections.emptyList();//Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(new GRNChromosomeFactory(seeds, POPULATION_SIZE_1))
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(OVERLAPPING)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                .setReproductionSelection(new Uniform<>())
                //.setReproductionSelection(new FitnessProportionalTournamnet<>(true, POPULATION_SIZE_2))
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
//                    new AAddRegulation(),
                        new ARandomiseParameter(
                                SVPParameterType.DEGRADATION,
                                SVPParameterType.DNA_BINDING,
                                SVPParameterType.TRANSCRIPTION_RATE,
                                SVPParameterType.TRANSLATION_RATE,
                                SVPParameterType.DEPHOSPHORYLATION,
                                SVPParameterType.SPECIES_BINDING),
                        new DuplicateParts(),
                        new DuplicatePromoter(),
                        new DuplicateTU(),
                        new RemoveParts(),
                        new RemovePromoter(),
                        new RemoveTU(),
                        new MergeTU(),
                        new RemoveRegulation(),
                        new SwapParts()
                        //)//, new double[]{0.1, 0.8, 0.1}, false
                ), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .setSurvivalSelection(new FitnessProportionalTournament<>(true, POPULATION_SIZE_1))
                //.setSurvivalSelection(new SigmaScaledRouletteWheel<>())
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();
        EvolutionObserver o = new SVPResponseCurveObserver(Arrays.asList("cdsL"));
        evoEngine.attach(o);


        // Step forward

        evoEngine.stepForward();

        List<EvaluatedChromosome<GRNTreeChromosome>> population1 =  evoEngine.getEvaluatedSurvivalPopulation();
        Assert.assertEquals("Evaluated population should be size of survival population", population1.size(), POPULATION_SIZE_1);

        // Step forward again

        evoEngine.stepForward();

        List<EvaluatedChromosome<GRNTreeChromosome>> population2 =  evoEngine.getEvaluatedSurvivalPopulation();
        Assert.assertEquals("Evaluated population should be size of survival population", population2.size(), POPULATION_SIZE_1);

        evoEngine.stepForward();

        List<EvaluatedChromosome<GRNTreeChromosome>> population3 =  evoEngine.getEvaluatedSurvivalPopulation();
        Assert.assertEquals("Evaluated population should be size of survival population", population3.size(), POPULATION_SIZE_1);

        List<ObjectId> population1Ids = population1.stream().map(c -> c.getChromosome().getId()).collect(Collectors.toList());
        List<ObjectId> population2Ids = population2.stream().map(c -> c.getChromosome().getId()).collect(Collectors.toList());
        List<ObjectId> population3Ids = population3.stream().map(c -> c.getChromosome().getId()).collect(Collectors.toList());

        Assert.assertTrue("All parents of chromosomes in population 2 should be in population 1",
                population2.stream().allMatch(c -> population1Ids.contains(c.getChromosome().getParentIds()[0])));

        Assert.assertTrue("All parents of chromosomes in population 3 should be in population 2",
                population3.stream().allMatch(c -> population2Ids.contains(c.getChromosome().getParentIds()[0])));
    }


}
