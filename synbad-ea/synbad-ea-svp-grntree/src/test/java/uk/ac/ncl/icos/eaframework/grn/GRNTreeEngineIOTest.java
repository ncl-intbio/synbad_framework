/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import org.junit.*;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.observer.SvpMongoObserver;
import uk.ac.ncl.icos.mongo.MExport;
import uk.ac.ncl.icos.eaframework.grn.exporter.SvpMongoImporter;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPResponseCurveObserver;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class GRNTreeEngineIOTest {

    String MONGO_URL = "mongodb://192.168.34.82:27017";
    String DATABASE_NAME = "grntree-ea";

    String INDEPENDENT_METABOLITE = "Subtilin";
    String DEPENDENT_METABOLITE = "GFP_rrnb";

    int GENERATIONS = 1;

    int POPULATION_SIZE_1 = 5;
    int POPULATION_SIZE_2 = 5;

    boolean OVERLAPPING = true;
    int MUTATIONS_PER_GENERATION = 1;


    int MAX_PARTS = 30;
    int MAX_UNPENALISEDPARTS = 20;

    double TARGET_RATIO = 0.14918591815273405;
    double TARGET_LOWER = 0.0;
    double TARGET_UPPER = 21428.5714;

    double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    int INCREMENT = 50;

    int DURATION = 21600;
    int STEPS = 1080;

    int THREADS = POPULATION_SIZE_2;//(int)Math.ceil((double)POPULATION_SIZE_2 / 2.0);


    private static final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private static final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();

    SBWorkspace workspace;

    public GRNTreeEngineIOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }


    //@Test
    public void testExportImport() {

        List<GRNTreeChromosome> seeds = Arrays.asList(
                new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
                        "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
                                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter")));

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE,  INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE);
            return cs;
        };

        ConstraintHandler handler = new MultiplyHandler();

        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
            new LinearityConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE),
            new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, 1.0),
            new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_UPPER),
            new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO),
            new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));

        List<SBMLModifier> modifiers = Collections.emptyList();//Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> engine1 = new GRNTreeEngineBuilder()
            .setChromosomeFactory(new GRNChromosomeFactory(seeds, POPULATION_SIZE_1))
            .setThreads(THREADS)
            .setEvaluator(evaluator)
            .setOverlapping(OVERLAPPING)
            .setReproductionPopulationSize(POPULATION_SIZE_2)
            .setReproductionSelection(new Uniform<>())
            .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
                    new DuplicateParts(),
                    new DuplicatePromoter(),
                    new DuplicateTU(),
                    new RemoveParts(),
                    new RemovePromoter(),
                    new RemoveTU(),
                    new SwapParts()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
            .setSurvivalPopulation(POPULATION_SIZE_1)
            .setSurvivalSelection(new FitnessProportionalTournament<>(true, POPULATION_SIZE_1))
            .build();

        String EXPERIMENT_NAME = "iotest_" + new Date().getTime();

        EvolutionObserver o = new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE));
        EvolutionObserver exp = new SvpMongoObserver(MONGO_URL, DATABASE_NAME, EXPERIMENT_NAME, DEPENDENT_METABOLITE);

        engine1.attach(o);
        engine1.attach(exp);

        engine1.stepForward();

        engine1.dettach(o);
        engine1.dettach(exp);

        SvpMongoImporter importer = new SvpMongoImporter();
        MExport export = importer.getExport(MONGO_URL,  DATABASE_NAME, EXPERIMENT_NAME);
        GRNTreeEngine engine2 = importer.getEngine(export);

        Assert.assertEquals(engine1.getEvolutionaryStrategy().getClass(), engine2.getEvolutionaryStrategy().getClass());

        Assert.assertEquals(engine1.getEvolutionaryStrategy().getSurvival().getClass(), engine2.getEvolutionaryStrategy().getSurvival().getClass());
        Assert.assertEquals(engine1.getEvolutionaryStrategy().getSurvival().getPopulationSize(), engine2.getEvolutionaryStrategy().getSurvival().getPopulationSize());
        Assert.assertEquals(engine1.getEvolutionaryStrategy().getSurvival().getSelection().getConfig(), engine2.getEvolutionaryStrategy().getSurvival().getSelection().getConfig());

        Assert.assertEquals(engine1.getEvolutionaryStrategy().getReproduction().getClass(), engine2.getEvolutionaryStrategy().getReproduction().getClass());
        Assert.assertEquals(engine1.getEvolutionaryStrategy().getReproduction().getPopulationSize(), engine2.getEvolutionaryStrategy().getReproduction().getPopulationSize());
        Assert.assertEquals(engine1.getEvolutionaryStrategy().getReproduction().getSelection().getConfig(), engine2.getEvolutionaryStrategy().getReproduction().getSelection().getConfig());

        Assert.assertEquals(engine1.getThreadPoolSize(), engine2.getThreadPoolSize());
        Assert.assertEquals(engine1.getFitnessEvaluator().getClass(), engine2.getFitnessEvaluator().getClass());

        EvolutionObserver o2 = new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE));
        EvolutionObserver exp2 = new SvpMongoObserver(MONGO_URL, DATABASE_NAME, EXPERIMENT_NAME, DEPENDENT_METABOLITE);

        engine2.attach(o2);
        engine2.attach(exp2);
        engine2.stepForward();
    }
}
