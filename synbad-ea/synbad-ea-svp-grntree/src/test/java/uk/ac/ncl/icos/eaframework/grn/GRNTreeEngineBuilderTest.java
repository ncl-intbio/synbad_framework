/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import org.junit.*;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPResponseCurveObserver;
import uk.ac.ncl.icos.eaframework.grn.observer.SvpMongoObserver;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.AAddRegulation;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.ARandomiseConstPromoter;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.ARandomiseParameter;
import uk.ac.ncl.icos.eaframework.grn.operator.prototype.ARandomiseRBS;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.SVPParameterType;
import uk.ac.ncl.icos.grntree.language.SvpWriteExporter;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class GRNTreeEngineBuilderTest {

    String MONGO_URL = "mongodb://192.168.33.84:27017";
    String DATABASE_NAME = "grntree-ea";
    
    String INDEPENDENT_METABOLITE = "Subtilin";
    String DEPENDENT_METABOLITE = "GFP_rrnb";

    int GENERATIONS = 1;

    int POPULATION_SIZE_1 = 5;
    int POPULATION_SIZE_2 = 5;

    boolean OVERLAPPING = true;
    int MUTATIONS_PER_GENERATION = 1;

    int MAX_PARTS = 30;
    int MAX_UNPENALISEDPARTS = 20;

    double TARGET_RATIO = 0.14918591815273405;
    double TARGET_LOWER = 0.0;
    double TARGET_UPPER = 21428.5714;

    double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    int INCREMENT = 50;

    int DURATION = 21600;
    int STEPS = 1080;

    int THREADS = POPULATION_SIZE_2;//(int)Math.ceil((double)POPULATION_SIZE_2 / 2.0);

    private static final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private static final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();

    SBWorkspace workspace;

    public GRNTreeEngineBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
    //@Test
    public void testBuildLinearity() {

        List<GRNTreeChromosome> seeds = Arrays.asList(new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
            "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
            "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter")));

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE);
            return cs;
        };

        ConstraintHandler handler = new MultiplyHandler();

        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
            new LinearityConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE),
            new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, 1.0),
            new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_UPPER),
            new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO),
            new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));

        List<SBMLModifier> modifiers = Collections.emptyList();//Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
            .setChromosomeFactory(new GRNChromosomeFactory(seeds, POPULATION_SIZE_1))
            .setThreads(THREADS)
            .setEvaluator(evaluator)
            .setOverlapping(OVERLAPPING)
            .setReproductionPopulationSize(POPULATION_SIZE_2)
            .setReproductionSelection(new Uniform<>())
            .addTerminationCondition(new GenerationTermination(GENERATIONS))
            .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
//                    new ARandomiseConstPromoter(),
//                    new ARandomiseRBS(),
//                    new AAddRegulation(),
//                    new ARandomiseParameter(
//                            SVPParameterType.DEGRADATION,
//                            SVPParameterType.DNA_BINDING,
//                            SVPParameterType.TRANSCRIPTION_RATE,
//                            SVPParameterType.TRANSLATION_RATE,
//                            SVPParameterType.DEPHOSPHORYLATION,
//                            SVPParameterType.SPECIES_BINDING),
                    new DuplicateParts(),
                    new DuplicatePromoter(),
                    new DuplicateTU(),
//                    new MergeTU(),
                    new RemoveParts(),
                    new RemovePromoter(),
//                    new RemoveRegulation(),
                    new RemoveTU(),
//                    new SwapParts()
//                    new SwapParts()
                    new SwapParts()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
            .setSurvivalPopulation(POPULATION_SIZE_1)
            .setSurvivalSelection( new FitnessProportionalTournament<>(true, POPULATION_SIZE_1))
            .build();

        EvolutionObserver o = new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE));
        evoEngine.attach(o);
//        EvolutionObserver o2 = new SvpMongoObserver(MONGO_URL, DATABASE_NAME, "linearity_" + new Date().getTime(),  DEPENDENT_METABOLITE);
//        evoEngine.attach(o2);
        evoEngine.run();
    }

    //@Test
    public void testGenerateOscillations() {

        List<GRNTreeChromosome> seeds = Arrays.asList(new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; " +
                        "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter")));

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = SimpleCopasiSimulator::new;

        ConstraintHandler handler = new MultiplyHandler();

        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
                new OscillationConstraint(DEPENDENT_METABOLITE, 200, 250),
                new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));

        List<SBMLModifier> modifiers = Collections.emptyList();//Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(new GRNChromosomeFactory(seeds, POPULATION_SIZE_1))
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(OVERLAPPING)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                .setReproductionSelection(new Uniform<>())
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
                        new ARandomiseConstPromoter(),
//                    new ARandomiseRBS(),
                        new AAddRegulation(),
                        new ARandomiseParameter(
                                SVPParameterType.DEGRADATION,
                                SVPParameterType.DNA_BINDING,
                                SVPParameterType.TRANSCRIPTION_RATE,
                                SVPParameterType.TRANSLATION_RATE,
                                SVPParameterType.DEPHOSPHORYLATION,
                                SVPParameterType.SPECIES_BINDING),
                        new DuplicateParts(),
                        new DuplicatePromoter(),
                        new DuplicateTU(),
//                    new MergeTU(),
                        new RemoveParts(),
                        new RemovePromoter(),
//                    new RemoveRegulation(),
                        new RemoveTU(),
                    new SwapParts()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .setSurvivalSelection(new FitnessProportionalTournament<>(true, POPULATION_SIZE_1))
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();
        EvolutionObserver o = new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE));
        evoEngine.attach(o);
        evoEngine.run();
    }


    //@Test
    public void testAbstractLinearity() throws Exception {
        GRNTree tree = GRNTreeFactory.getGRNTree();

        GRNTreeNode tu1 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);

        GRNTreeNode cds1 = SVP_HELPER.getCDSEnvConst("cdsD", "smD", 0.0012, 1.0e-4, 0.1 , 0.0012);
        GRNTreeNode cds2 = SVP_HELPER.getCDSWithPhosphorylated("cdsF", 0.0012, 0.1, 0.0012);
        GRNTreeNode cds3 = SVP_HELPER.getCDS("cdsI", 0.0012);

        GRNTreeNode prom4 = SVP_HELPER.getInducPromoter("promG", 0.04);
        tu1.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promA", 0.04),
                SVP_HELPER.getRBS("rbsC", 0.1818),
                cds1,
                SVP_HELPER.getRBS("rbsE", 0.1),
                cds2,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu2.setNodes(Arrays.asList(
                prom4,
                SVP_HELPER.getRBS("rbsH", 0.145),
                cds3,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tree.getRootNode().addNode(tu1);
        tree.getRootNode().addNode(tu2);

        // Add Parts to TUs

        // Add promoter regulation

        tree.addInteraction(SVP_MANAGER.getPhosphorylationInteraction(cds1.getSVP(), cds2.getSVP(), 1.0e-4));
        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(prom4.getSVP(),  cds2.getSVP(),
                MolecularForm.PHOSPHORYLATED, RegulationRole.ACTIVATOR, 320.0));

        List<GRNTreeChromosome> seeds = Arrays.asList(new GRNTreeChromosome(tree));

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, "smD", "cdsI");
            return cs;
        };

        ConstraintHandler handler = new MultiplyHandler();

        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
                new LinearityConstraint("smD", "cdsI"),
                new LowerBoundConstraint("smD", "cdsI", TARGET_LOWER, 1.0),
                new UpperBoundConstraint("smD", "cdsI", TARGET_UPPER),
                new RatioConstraint("smD", "cdsI", TARGET_RATIO),
                new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));

        List<SBMLModifier> modifiers = Collections.emptyList();//Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(new GRNChromosomeFactory(seeds, POPULATION_SIZE_1))
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(OVERLAPPING)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                //.setReproductionSelection(new FitnessProportionalTournamnet<>(true))
                //.setReproductionSelection(new Uniform<>())
                .setReproductionSelection(new FitnessProportionalTournament<>(true, 2))
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
//                    new AAddRegulation(),
                        new ARandomiseParameter(
                                SVPParameterType.DEGRADATION,
                                SVPParameterType.DNA_BINDING,
                                SVPParameterType.TRANSCRIPTION_RATE,
                                SVPParameterType.TRANSLATION_RATE,
                                SVPParameterType.DEPHOSPHORYLATION,
                                SVPParameterType.SPECIES_BINDING),
                        new DuplicateParts(),
                        new DuplicatePromoter(),
                        new DuplicateTU(),
                        new RemoveParts(),
                        new RemovePromoter(),
                        new RemoveTU(),
                        new MergeTU(),
                        new RemoveRegulation(),
                        new SwapParts()
                        //)//, new double[]{0.1, 0.8, 0.1}, false
                ), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                //.setSurvivalSelection(new Tournament<>())
                .setSurvivalSelection(new FitnessProportionalTournament<>(true, 7))
                //.setSurvivalSelection(new SigmaScaledRouletteWheel<>())
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();
        EvolutionObserver o = new SVPResponseCurveObserver(Arrays.asList("cdsI"));
        evoEngine.attach(o);
//        EvolutionObserver o2 = new SvpMongoObserver(MONGO_URL, DATABASE_NAME, "abstract_linearity_" + new Date().getTime(), "smD", "cdsI", INCREMENT);
//        evoEngine.attach(o2);
        // EvolutionObserver o3 = new SVPSbolObserver("optimiseRepressilator");
        // evoEngine.attach(o3);
        evoEngine.run();
    }

    //@Test
    public void testOptimiseRepressilator() throws Exception {
        GRNTree tree = GRNTreeFactory.getGRNTree();

        GRNTreeNode tu1 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu2 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);
        GRNTreeNode tu3 = GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE);


        GRNTreeNode operator1 = SVP_HELPER.getOperator("opB");
        GRNTreeNode operator2 = SVP_HELPER.getOperator("opF");
        GRNTreeNode operator3 = SVP_HELPER.getOperator("opJ");

        GRNTreeNode cds1 = SVP_HELPER.getCDS("cdsD", 0.0018);
        GRNTreeNode cds2 = SVP_HELPER.getCDS("cdsH", 0.0012);
        GRNTreeNode cds3 = SVP_HELPER.getCDS("cdsL", 0.0024);

        tu1.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promA", 0.05),
                operator1,
                SVP_HELPER.getRBS("rbsC", 0.1818),
                cds1,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu2.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promE", 0.04),
                operator2,
                SVP_HELPER.getRBS("rbsG", 0.1),
                cds2,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tu3.setNodes(Arrays.asList(
                SVP_HELPER.getConstPromoter("promI", 0.045),
                operator3,
                SVP_HELPER.getRBS("rbsK", 0.145),
                cds3,
                GRNTreeNodeFactory.getLeafNode(SVPManager.getSVPManager().getTerminator(), InterfaceType.NONE)
        ));

        tree.getRootNode().addNode(tu1);
        tree.getRootNode().addNode(tu2);
        tree.getRootNode().addNode(tu3);

        // Add Parts to TUs

        // Add promoter regulation

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator2.getSVP(), cds1.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 325));

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator3.getSVP(), cds2.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 305));

        tree.addInteraction(SVP_MANAGER.getRegulationInteraction(operator1.getSVP(),cds3.getSVP(),
                MolecularForm.DEFAULT, RegulationRole.REPRESSOR, 320));

        List<GRNTreeChromosome> seeds = Arrays.asList(new GRNTreeChromosome(tree));

//        SvpWriteExporter exporter = new SvpWriteExporter();
//        exporter.write(seeds.get(0).getGRNTree());

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = SimpleCopasiSimulator::new;

        ConstraintHandler handler = new MultiplyHandler();
        // ConstraintHandler handler = new SummationHandler();
        // ConstraintHandler handler = new LowestConstraintHandler();

        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
                new OscillationConstraint("cdsL", 100, 125),
//                new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, 1.0),
//                new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_UPPER),
//                new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO),
                new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));

        List<SBMLModifier> modifiers = Collections.emptyList();//Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(new GRNChromosomeFactory(seeds, POPULATION_SIZE_1))
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(OVERLAPPING)
                //.setOrderedPopulations(true)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                //.setReproductionSelection(new FitnessProportionalTournamnet<>(true))
                //.setReproductionSelection(new Uniform<>())
                .setReproductionSelection(new FitnessProportionalTournament<>(true, 2))
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
//                    new AAddRegulation(),
                    new ARandomiseParameter(
                            SVPParameterType.DEGRADATION,
                            SVPParameterType.DNA_BINDING,
                            SVPParameterType.TRANSCRIPTION_RATE,
                            SVPParameterType.TRANSLATION_RATE,
                            SVPParameterType.DEPHOSPHORYLATION,
                            SVPParameterType.SPECIES_BINDING),
                    new DuplicateParts(),
                    new DuplicatePromoter(),
                    new DuplicateTU(),
                    new RemoveParts(),
                    new RemovePromoter(),
                    new RemoveTU(),
                    new MergeTU(),
                    new RemoveRegulation(),
                    new SwapParts()
                //)//, new double[]{0.1, 0.8, 0.1}, false
                ), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                //.setSurvivalSelection(new Tournament<>())
                .setSurvivalSelection(new FitnessProportionalTournament<>(true, 4))
                //.setSurvivalSelection(new SigmaScaledRouletteWheel<>())
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();
        EvolutionObserver o = new SVPResponseCurveObserver(Arrays.asList("cdsL"));
        evoEngine.attach(o);
//        EvolutionObserver o2 = new SvpMongoObserver(MONGO_URL, DATABASE_NAME, "optimise-oscillator_" + new Date().getTime(), INDEPENDENT_METABOLITE, "cdsL", INCREMENT);
//        evoEngine.attach(o2);
       // EvolutionObserver o3 = new SVPSbolObserver("optimiseRepressilator");
       // evoEngine.attach(o3);
        evoEngine.run();
    }


}
