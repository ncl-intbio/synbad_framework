package uk.ac.ncl.icos.eaframework.grn.chromosome;

import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * Created by owengilfellon on 06/10/2014.
 */
public class ProtoCompilerTest {

    private static AbstractSVPManager m = AbstractSVPManager.getSVPManager();
    private static String SERVER = SVPManager.getRepositoryUrl();
    private static PartsHandler PARTS_HANDLER = new PartsHandler(SERVER);

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
 
    //@Test
    public void testApply() throws Exception {

        Part a = m.getConstPromoterPart("A");
        Part b = m.getNegativeOperator("B");
        Part c = m.getRBS("C");
        Part d = m.getCDS("D");

        Part e = m.getConstPromoterPart("E");
        Part f = m.getNegativeOperator("F");
        Part g = m.getRBS("G");
        Part h = m.getCDS("H");

        Part i = m.getConstPromoterPart("I");
        Part j = m.getNegativeOperator("J");
        Part k = m.getRBS("K");
        Part l = m.getCDS("L");

        // ===============================================
        // Create Internal Interactions, passing parts
        // and parameters, if needed.
        // ===============================================

        List<Interaction> a_i = m.getPoPSProduction(a, 0.0269);
        List<Interaction> e_i = m.getPoPSProduction(e, 0.03242);
        List<Interaction> i_i = m.getPoPSProduction(i, 0.02513);

        List<Interaction> b_i = m.getOperatorPoPSModulation(b);
        List<Interaction> f_i = m.getOperatorPoPSModulation(f);
        List<Interaction> j_i = m.getOperatorPoPSModulation(j);

        List<Interaction> c_i = m.getRiPSProduction(c, 0.34255);
        List<Interaction> g_i = m.getRiPSProduction(g, 0.51213);
        List<Interaction> k_i = m.getRiPSProduction(k, 0.4685);

        List<Interaction> d_i = m.getProteinProductionAndDegradation(d, 0.01412);
        List<Interaction> h_i = m.getProteinProductionAndDegradation(h, 0.02134);
        List<Interaction> l_i = m.getProteinProductionAndDegradation(l, 0.03214);

        // ===============================================
        // Create Part-Part Interactions
        // ===============================================

        Interaction dRepressesf = m.getRegulationInteraction(   f, d,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.3456);

        Interaction hRepressesj = m.getRegulationInteraction(   j, h,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.4567);

        Interaction lRepressesb = m.getRegulationInteraction(   b, l,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.3754);

        GRNCompilableFactory factory = new GRNCompilableFactory();
        List<ICompilable> compilables = new ArrayList<ICompilable>();

        Map<Interaction, List<Part>> lRepressesbParts = new HashMap<Interaction, List<Part>>();
        List<Part> partList = new ArrayList<Part>();
        partList.add(b);
        partList.add(l);

        lRepressesbParts.put(lRepressesb, partList);
        Map<Interaction, List<Part>> dRepressesfParts = new HashMap<Interaction, List<Part>>();
        List<Part> partList2 = new ArrayList<Part>();
        partList2.add(d);
        partList2.add(f);
        dRepressesfParts.put(dRepressesf, partList2);
        Map<Interaction, List<Part>> hRepressesjParts = new HashMap<Interaction, List<Part>>();
        List<Part> partList3 = new ArrayList<Part>();
        partList3.add(h);
        partList3.add(j);
        hRepressesjParts.put(hRepressesj, partList3);

        compilables.addAll(GRNCompilableFactory.getCompilable(a, a_i, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(b, b_i, lRepressesbParts, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(c, c_i, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(d, d_i, dRepressesfParts, SERVER));
//        compilables.addAll(GRNCompilableFactory.getCompilables("BO_4296:Ter"));


        compilables.addAll(GRNCompilableFactory.getCompilable(e, e_i, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(f, f_i, dRepressesfParts, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(g, g_i, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(h, h_i, hRepressesjParts, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilables("BO_4296:Ter", SERVER));


        compilables.addAll(GRNCompilableFactory.getCompilable(i, i_i, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(j, j_i, hRepressesjParts, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(k, k_i, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilable(l, l_i, lRepressesbParts, SERVER));
        compilables.addAll(GRNCompilableFactory.getCompilables("BO_4296:Ter", SERVER));

        CompilationDirector director = new CompilationDirector(SERVER);
        director.setModelCompilables(compilables);
        String sbmlOutput = director.getSBMLString("Test");

        try
        {
            FileWriter fw = new FileWriter( "test" + ".xml" );
            fw.write( sbmlOutput );
            fw.flush();
        } catch( Exception ex ) {
            System.out.println( ex.getMessage() );
        }
    }
}
