/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.chromosome;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.intbio.virtualparts.ModelBuilder;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;
import uk.ac.ncl.intbio.virtualparts.SBML.SBMLHandler;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

/**
 *
 * @author owengilfellon
 */
public class ModelConstructionTests {
    
    private static final Logger logger = LoggerFactory.getLogger(ModelConstructionTests.class);
    PartsHandler p;
    SBMLDocument container;
    private SBMLHandler sbmlhandler;
    ModelBuilder mb;
    private final String VPR_URL = "http://vm-vpr.bioswarm.net:8081";

    AbstractSVPManager m = AbstractSVPManager.getSVPManager();
    
    public ModelConstructionTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
 
    @Test
    public void constructSvpCascadeModelTest() throws Exception {

        p = new PartsHandler(VPR_URL);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);
        
        /* ====================================
         * 
         * Create Parts
         * 
         * ==================================== */
          
        Part a = m.getConstPromoterPart("A");
        Part b = m.getRBS("B");
        Part c = m.getCDS("C");

        Part d = m.getConstPromoterPart("D");
        Part e = m.getRBS("E");
        Part f = m.getCDS("F");

        Part g = m.getConstPromoterPart("G");
        Part h = m.getRBS("H");
        Part i = m.getCDS("I");

        Part j = m.getConstPromoterPart("J");
        Part k = m.getRBS("K");
        Part l = m.getCDS("L");
  
         /* ====================================
         * 
         * Create Internal Interactions
         * 
         * ==================================== */
        
        List<Interaction> a_i = m.getPoPSProduction(a, 0.0269);
        List<Interaction> d_i = m.getPoPSProduction(d, 0.0269);
        List<Interaction> g_i = m.getPoPSProduction(g, 0.0269);
        List<Interaction> j_i = m.getPoPSProduction(j, 0.0269);

        List<Interaction> b_i = m.getRiPSProduction(b, 0.51213);
        List<Interaction> e_i = m.getRiPSProduction(e, 0.51213);
        List<Interaction> h_i = m.getRiPSProduction(h, 0.51213);
        List<Interaction> k_i = m.getRiPSProduction(k, 0.51213);

        List<Interaction> c_i = m.getProteinProductionAndDegradation(c, 0.012154);
        List<Interaction> f_i = m.getProteinProductionAndDegradation(f, 0.012154);
        List<Interaction> i_i = m.getProteinProductionAndDegradation(i, 0.012154);
        List<Interaction> l_i = m.getProteinProductionAndDegradation(l, 0.012154);
        
        c_i.addAll(m.getDephosphorylationandDegradation(c, 0.0544545, 0.117454));
        f_i.addAll(m.getDephosphorylationandDegradation(f, 0.0544545, 0.117454));
        i_i.addAll(m.getDephosphorylationandDegradation(i, 0.0544545, 0.117454));
        l_i.addAll(m.getDephosphorylationandDegradation(l, 0.0544545, 0.117454));
        
        /* ====================================
         * 
         * Create External Interactions
         * 
         * ==================================== */

        Interaction cPhosF = m.getPhosphorylationInteraction(c, f, 0.4151);
        Interaction fPhosI = m.getPhosphorylationInteraction(f, i, 0.4151);
        Interaction iPhosL = m.getPhosphorylationInteraction(i, l, 0.4151);
        
        /* ====================================
         * 
         * Create SBML Documents
         * 
         * ==================================== */

        SBMLDocument a_doc = p.CreatePartModel(a, a_i);
        SBMLDocument b_doc = p.CreatePartModel(b, b_i);
        SBMLDocument c_doc = p.CreatePartModel(c, c_i);
        SBMLDocument d_doc = p.CreatePartModel(d, d_i);
        SBMLDocument e_doc = p.CreatePartModel(e, e_i);
        SBMLDocument f_doc = p.CreatePartModel(f, f_i);
        SBMLDocument g_doc = p.CreatePartModel(g, g_i);
        SBMLDocument h_doc = p.CreatePartModel(h, h_i);
        SBMLDocument i_doc = p.CreatePartModel(i, i_i);
        SBMLDocument j_doc = p.CreatePartModel(j, j_i);
        SBMLDocument k_doc = p.CreatePartModel(k, k_i);
        SBMLDocument l_doc = p.CreatePartModel(l, l_i);
        
        List<Part> cTofParts = new ArrayList<Part>();
        cTofParts.add(c);
        cTofParts.add(f);
        
        List<Part> fToIParts = new ArrayList<Part>();
        fToIParts.add(f);
        fToIParts.add(i);
        
        List<Part> iToLParts = new ArrayList<Part>();
        iToLParts.add(i);
        iToLParts.add(l);
        /*  */
        
        SBMLDocument cTof = p.CreateInteractionModel(cTofParts, cPhosF);
        SBMLDocument fToi = p.CreateInteractionModel(fToIParts, fPhosI);
        SBMLDocument iTol = p.CreateInteractionModel(iToLParts, iPhosL);
        
        SBMLDocument mrna1 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna2 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna3 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna4 = p.GetModel(p.GetPart("mRNA"));
        
        /* ====================================
         * 
         * Assemble SBML Model
         * 
         * ==================================== */



        mb.Add(a_doc);
        mb.Link(a_doc, mrna1);
        mb.Add(mrna1);
        mb.Link(mrna1, b_doc);
        mb.Add(b_doc);
        mb.Link(b_doc, c_doc);
        mb.Add(c_doc);
        
        mb.Add(d_doc);
        mb.Link(d_doc, mrna2);
        mb.Add(mrna2);
        mb.Link(mrna2, e_doc);
        mb.Add(e_doc);
        mb.Link(e_doc, f_doc);
        mb.Add(f_doc);
        mb.Add(cTof);
        
        mb.Add(g_doc);
        mb.Link(g_doc, mrna3);
        mb.Add(mrna3);
        mb.Link(mrna3, h_doc);
        mb.Add(h_doc);
        mb.Link(h_doc, i_doc);
        mb.Add(i_doc);
        mb.Add(fToi);
        
        mb.Add(j_doc);
        mb.Link(j_doc, mrna4);
        mb.Add(mrna4);
        mb.Link(mrna4, k_doc);
        mb.Add(k_doc);
        mb.Link(k_doc, l_doc);
        mb.Add(l_doc);
        mb.Add(iTol);

       try
       {
           FileWriter writer = new FileWriter("cascade" + ".xml");
           writer.write(mb.GetModelString());
           writer.flush();
           // System.out.println("\nModel written to " + filename + ".xml");
       }
       catch(Exception ex)
       {
           System.out.println(ex.getMessage());
       }
    }

    @Test
    public void constructRepressilatorModelTest() throws Exception {

        p = new PartsHandler(VPR_URL);
        container = new SBMLHandler().GetSBMLTemplateModel("SVP_Model");
        mb = new ModelBuilder(container);

        // ===============================================
        // Create Parts, providing a Part ID
        // ===============================================

        Part a = m.getConstPromoterPart("A");
        Part b = m.getNegativeOperator("B");
        Part c = m.getRBS("C");
        Part d = m.getCDS("D");

        Part e = m.getConstPromoterPart("E");
        Part f = m.getNegativeOperator("F");
        Part g = m.getRBS("G");
        Part h = m.getCDS("H");

        Part i = m.getConstPromoterPart("I");
        Part j = m.getNegativeOperator("J");
        Part k = m.getRBS("K");
        Part l = m.getCDS("L");

        // ===============================================
        // Create Internal Interactions, passing parts
        // and parameters, if needed.
        // ===============================================

        List<Interaction> a_i = m.getPoPSProduction(a, 0.0269);
        List<Interaction> e_i = m.getPoPSProduction(e, 0.03242);
        List<Interaction> i_i = m.getPoPSProduction(i, 0.02513);

        List<Interaction> b_i = m.getOperatorPoPSModulation(b);
        List<Interaction> f_i = m.getOperatorPoPSModulation(f);
        List<Interaction> j_i = m.getOperatorPoPSModulation(j);

        List<Interaction> c_i = m.getRiPSProduction(c, 0.34255);
        List<Interaction> g_i = m.getRiPSProduction(g, 0.51213);
        List<Interaction> k_i = m.getRiPSProduction(k, 0.4685);

        List<Interaction> d_i = m.getProteinProductionAndDegradation(d, 0.01412);
        List<Interaction> h_i = m.getProteinProductionAndDegradation(h, 0.02134);
        List<Interaction> l_i = m.getProteinProductionAndDegradation(l, 0.03214);

        // ===============================================
        // Create Part-Part Interactions
        // ===============================================

        Interaction dRepressesf = m.getRegulationInteraction(   f, d,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.3456);

        Interaction hRepressesj = m.getRegulationInteraction(   j, h,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.4567);

        Interaction lRepressesb = m.getRegulationInteraction(   b, l,
                MolecularForm.DEFAULT,
                RegulationRole.REPRESSOR,
                0.3754);

        // =======================================================
        // Create SBML models from parts and internal interactions
        // =======================================================

        SBMLDocument a_doc = p.CreatePartModel(a, a_i);
        SBMLDocument b_doc = p.CreatePartModel(b, b_i);
        SBMLDocument c_doc = p.CreatePartModel(c, c_i);
        SBMLDocument d_doc = p.CreatePartModel(d, d_i);
        SBMLDocument e_doc = p.CreatePartModel(e, e_i);
        SBMLDocument f_doc = p.CreatePartModel(f, f_i);
        SBMLDocument g_doc = p.CreatePartModel(g, g_i);
        SBMLDocument h_doc = p.CreatePartModel(h, h_i);
        SBMLDocument i_doc = p.CreatePartModel(i, i_i);
        SBMLDocument j_doc = p.CreatePartModel(j, j_i);
        SBMLDocument k_doc = p.CreatePartModel(k, k_i);
        SBMLDocument l_doc = p.CreatePartModel(l, l_i);

        // =======================================================
        // Create Lists of Parts for each Interaction
        // =======================================================

        List<Part> dTofParts = new ArrayList<Part>();
        dTofParts.add(d);
        dTofParts.add(f);

        List<Part> hTojParts = new ArrayList<Part>();
        hTojParts.add(h);
        hTojParts.add(j);

        List<Part> lTobParts = new ArrayList<Part>();
        lTobParts.add(l);
        lTobParts.add(b);

        // =======================================================
        // Create SBML Models for Interactions
        // (JParts attempts to retrieve non-existent parts)
        // TODO Update JParts to use above lists of parts
        // =======================================================

        SBMLDocument dTof = p.CreateInteractionModel(dTofParts, dRepressesf);
        SBMLDocument hToj = p.CreateInteractionModel(hTojParts, hRepressesj);
        SBMLDocument lTob = p.CreateInteractionModel(lTobParts, lRepressesb);

/*
        SBMLDocument dTof = p.CreateInteractionModel(dRepressesf);
        SBMLDocument hToj = p.CreateInteractionModel(hRepressesj);
        SBMLDocument lTob = p.CreateInteractionModel(lRepressesb);
*/

        SBMLDocument mrna1 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna2 = p.GetModel(p.GetPart("mRNA"));
        SBMLDocument mrna3 = p.GetModel(p.GetPart("mRNA"));
        /**/
        // =======================================================
        // Compose Part and Interaction Models into a
        // simulateable model
        // TODO update compiler to automate this process
        // TODO can abstract parts and concrete parts be mixed?
        // =======================================================

        mb.Add(a_doc);
        mb.Link(a_doc, b_doc);
        mb.Add(b_doc);
        mb.Link(b_doc, lTob);
        mb.Add(lTob);
        mb.Add(mrna1);
        mb.Link(lTob, mrna1);
        mb.Link(mrna1, c_doc);
        mb.Add(c_doc);
        mb.Link(c_doc, d_doc);
        mb.Add(d_doc);

        mb.Add(e_doc);
        mb.Link(e_doc, f_doc);
        mb.Add(f_doc);
        mb.Link(f_doc, dTof);
        mb.Add(dTof);
        mb.Add(mrna2);
        mb.Link(dTof, mrna2);
        mb.Link(mrna2, g_doc);
        mb.Add(g_doc);
        mb.Link(g_doc, h_doc);
        mb.Add(h_doc);

        mb.Add(i_doc);
        mb.Link(i_doc, j_doc);
        mb.Add(j_doc);
        mb.Link(j_doc, hToj);
        mb.Add(hToj);
        mb.Add(mrna3);
        mb.Link(hToj, mrna3);
        mb.Link(mrna3, k_doc);
        mb.Add(k_doc);
        mb.Link(k_doc, l_doc);
        mb.Add(l_doc);/**/

        // =======================================================
        // Write compiled model to file
        // =======================================================

        try
        {
            FileWriter writer = new FileWriter("output" + ".xml");
            writer.write(mb.GetModelString());
            writer.flush();
            // System.out.println("\nModel written to " + filename + ".xml");
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }

    }
    
}
