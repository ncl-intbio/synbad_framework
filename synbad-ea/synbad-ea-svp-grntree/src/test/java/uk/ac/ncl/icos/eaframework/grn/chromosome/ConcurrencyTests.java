/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.chromosome;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class ConcurrencyTests {
    
    final static int N = 20;
    static ExecutorService pool = Executors.newFixedThreadPool(3);
    private static final Logger logger = LoggerFactory.getLogger(ConcurrencyTests.class);
    
    public ConcurrencyTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   // @Test
    public void testApply() throws InterruptedException {

       CountDownLatch startSignal = new CountDownLatch(1);
       CountDownLatch doneSignal = new CountDownLatch(N);
       List<Future<String>> futures = new ArrayList<>();
       
       System.out.println("Initialising Operators...");
       
       for (int i = 0; i < N; ++i) {
           futures.add(pool.submit(new Worker(startSignal, doneSignal)));
       }
       
       System.out.println("...done");
       System.out.println("Running operators...");
       
       startSignal.countDown(); // let all threads proceed
       doneSignal.await(); // wait for all threads to finish
       
       System.out.println("...done");
       
       System.out.println("Collecting results...");
       for(Future<String> f : futures) {
           try {
               String s = f.get(1, TimeUnit.SECONDS);
               System.out.println(s); 
           } catch (ExecutionException | TimeoutException ex) {
               logger.error(ex.getLocalizedMessage());
           }
       }
       
       System.out.println("...done");
       
       pool.shutdown();
    }
    
    static class Worker implements Callable<String>
    {

       private final CountDownLatch startSignal;
       private final CountDownLatch doneSignal;

       Worker(CountDownLatch startSignal, CountDownLatch doneSignal)
       {
          this.startSignal = startSignal;
          this.doneSignal = doneSignal;
       }

       @Override
       public String call()
       {
            String name = Thread.currentThread().getName();
           
            try
            {
               
               startSignal.await();
               
                try
                {
                   Thread.sleep((int)(Math.random()*300));
                }
                catch (InterruptedException ie)
                {
                    
                }
          
               doneSignal.countDown();
               return "Operator has finished in " + name;
            }
            catch (InterruptedException ie)
            {
               return name + " was interrupted";
            }
        }
    }
    
}
