/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experiments.mutantchamp;

import org.junit.*;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngineBuilder;
import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.exporter.SvpMongoImporter;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPResponseCurveObserver;
import uk.ac.ncl.icos.eaframework.grn.observer.SvpMongoObserver;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.Tournament;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.*;

/**
 *
 * @author owengilfellon
 */
public class LinearityExperiments {

    String MONGO_URL = "mongodb://192.168.34.82:27017";
    String DATABASE_NAME = "grntree-ea-linearity";

    String INDEPENDENT_METABOLITE = "Subtilin";
    String DEPENDENT_METABOLITE = "GFP_rrnb";

    boolean RANDOMISE_PARAMETERS = false;
    boolean RANDOMISE_TOPOLOGY = false;
    int MAX_INITIAL_MUTATIONS = 5;

    int GENERATIONS = 1000;

    int POPULATION_SIZE_1 = 25;
    int POPULATION_SIZE_2 = 25;

    boolean OVERLAPPING = true;
    int MUTATIONS_PER_GENERATION = 1;

    int MAX_PARTS = 30;
    int MAX_UNPENALISEDPARTS = 20;

    double TARGET_RATIO = 0.1481643606790526;
    double TARGET_LOWER = 0.0;

    double MAXIMUM_GFP = 21428.5714;
    double STARTING_CONCENTRATION = 1089.689752370406;

    double MIN_CHANGE = 0.0005;
    int INCREMENT = 50;

    int DURATION = 21600;
    int STEPS = 1080;

    int THREADS = POPULATION_SIZE_2;

    public LinearityExperiments() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void linearityMutantChampTest() {

        String name = "linearity_mutant_champ_concrete" +
                (RANDOMISE_TOPOLOGY || RANDOMISE_PARAMETERS ? "_random" : "") +
                (RANDOMISE_TOPOLOGY ? "_topology" : "") +
                (RANDOMISE_PARAMETERS ? "_parameters" : "");

        SvpMongoImporter importer = new SvpMongoImporter();

        EvoEngine<GRNTreeChromosome> evoEngine = importer.exportExists(MONGO_URL, DATABASE_NAME, name) ?
                importer.getEngine( importer.getExport(MONGO_URL,  DATABASE_NAME, name)) :
               setupMutantChampLinearity(
                        getTopologyOperators(),
                        getLinearityConstraints());

        if(evoEngine.getPopulationStats().getCurrentGeneration() < GENERATIONS) {
            System.out.println("Starting " + name);
            evoEngine.attach(new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE)));
            evoEngine.attach(new SvpMongoObserver(MONGO_URL, DATABASE_NAME, name, DEPENDENT_METABOLITE));
            evoEngine.run();
        }

    }

    @Test
    public void linearityTournamentTest() {

        int TOURNAMENT_SIZE = 2;

            String name = "linearity_uniform_tournament_" + TOURNAMENT_SIZE +"_concrete" +
                    (RANDOMISE_TOPOLOGY || RANDOMISE_PARAMETERS ? "_random" : "") +
                    (RANDOMISE_TOPOLOGY ? "_topology" : "") +
                    (RANDOMISE_PARAMETERS ? "_parameters" : "");
            SvpMongoImporter importer = new SvpMongoImporter();

            EvoEngine<GRNTreeChromosome> evoEngine = importer.exportExists(MONGO_URL, DATABASE_NAME, name) ?
                    importer.getEngine( importer.getExport(MONGO_URL,  DATABASE_NAME, name)) :
                    setupUniformTournamentLinearity(
                            TOURNAMENT_SIZE,
                            getTopologyOperators(),
                            getLinearityConstraints());

            if(evoEngine.getPopulationStats().getCurrentGeneration() < GENERATIONS) {
                System.out.println("Starting " + name);
                evoEngine.attach(new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE)));
                evoEngine.attach(new SvpMongoObserver(MONGO_URL, DATABASE_NAME, name, DEPENDENT_METABOLITE));
                evoEngine.run();
            }

    }

    private OperatorGroup<GRNTreeChromosome> getParameterOperators() {
        return new OperatorGroup<>(Arrays.asList(
                new RandomiseRBS(),
                new RandomiseConstPromoter()
              ), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION);
    }

    private OperatorGroup<GRNTreeChromosome> getTopologyOperators() {

        double[] weights= {
                1,      //RandomiseRBS
                0.1,    //AddRegulation
                0.1,    //DuplicateParts
                0.1,    //DuplicateTU
                1,      //RandomiseConstPromoter
                0.1,    //RemoveRegulation
                0.1,    //SplitTU
                0.1,    //SwapParts
                0.1,    //RemoveParts
                0.1,    //AddPromoter
                0.1,    //MergeTU
                0.1,    //RemovePromoter
                0.1 };  //RemoveTU

        return new OperatorGroup<>(Arrays.asList(
                new RandomiseRBS(),
                new AddRegulation(),
                new DuplicateParts(),
                new DuplicateTU(),
                new RandomiseConstPromoter(),
                new RemoveRegulation(),
                new SplitTU(),
                new SwapParts(),
                new RemoveParts(),
                // New Operators
                new AddPromoter(),
                new MergeTU(),
                new RemovePromoter(),
                new RemoveTU()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION, weights, false);
    }

    private List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> getOscillationConstraints(double amplitude, int stepsPerPeriod) {
        return Arrays.asList(
                new OscillationConstraint<>(DEPENDENT_METABOLITE, amplitude, stepsPerPeriod),
                new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));
    }

    private List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> getLinearityConstraints() {
        return Arrays.asList(
                new LinearityConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE),
                new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, STARTING_CONCENTRATION),
                new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, MAXIMUM_GFP),
                new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO),
                new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS));
    }


    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
   // @Test
    public EvoEngine<GRNTreeChromosome> setupUniformTournamentLinearity(int TOURNAMENT_SIZE, OperatorGroup<GRNTreeChromosome> operators, List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints) {

        ChromosomeFactory<GRNTreeChromosome> cf = getChromosomeFactory();

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE);
            return cs;
        };

        ConstraintHandler handler = new MultiplyHandler();
        // ConstraintHandler handler = new SummationHandler();
        // ConstraintHandler handler = new LowestConstraintHandler();

        List<SBMLModifier> modifiers = Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        // Parameter mutations are weighted 10x higher than topology mutations



        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
            .setChromosomeFactory(cf)
            .setThreads(THREADS)
            .setEvaluator(evaluator)
            .setOverlapping(OVERLAPPING)
            .setReproductionPopulationSize(POPULATION_SIZE_2)
            .setReproductionSelection(new Uniform<>())
            .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
            .setReproductionOperators(operators)
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .setSurvivalSelection( new Tournament<>(TOURNAMENT_SIZE))
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
            .build();

        return evoEngine;
    }

    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
    // @Test
    public EvoEngine<GRNTreeChromosome> setupMutantChampLinearity( OperatorGroup<GRNTreeChromosome> operators, List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints) {

        ChromosomeFactory<GRNTreeChromosome> cf = getChromosomeFactory();

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE);
            return cs;
        };

        ConstraintHandler handler = new MultiplyHandler();
        // ConstraintHandler handler = new SummationHandler();
        // ConstraintHandler handler = new LowestConstraintHandler();

        List<SBMLModifier> modifiers = Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        // Parameter mutations are weighted 10x higher than topology mutations

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(cf)
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                //.setOverlapping(OVERLAPPING)
                .setOrderedPopulations(true, false)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(operators)
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();

        return evoEngine;
    }

    public ChromosomeFactory<GRNTreeChromosome> getChromosomeFactory() {

        List<GRNTreeChromosome> chromosomes = new LinkedList<>();

        if(RANDOMISE_PARAMETERS) {
            SVPManager m = SVPManager.getSVPManager();

            for(int i = 0; i < POPULATION_SIZE_1; i++) {

                String promoter = m.getConstPromoter().getName();
                String rbs1 = m.getRBS().getName();
                String rbs2 = m.getRBS().getName();
                String rbs3 = m.getRBS().getName();

                StringBuilder b = new StringBuilder()
                        .append(promoter).append(":Prom; ")
                        .append(rbs1).append(":RBS; SpaK:CDS; ")
                        .append(rbs2).append(":RBS; SpaR:CDS; BO_4296:Ter; PspaS:Prom; ")
                        .append(rbs3).append(":RBS; GFP_rrnb:CDS; BO_4296:Ter");

                GRNTreeChromosome seed = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(b.toString()));

                 chromosomes.add(RANDOMISE_TOPOLOGY ? randomiseTopology(seed) : seed);
            }
        } else {
            GRNTreeChromosome seed = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
                    "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; "
                            + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter"));

            if(RANDOMISE_TOPOLOGY) {
                for(int i = 0; i < POPULATION_SIZE_1; i++) {
                    chromosomes.add(randomiseTopology(seed));
                }
            } else {
                chromosomes.add(seed);
            }
        }

        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(chromosomes, POPULATION_SIZE_1);
        return cf;
    }

    private GRNTreeChromosome randomiseTopology(GRNTreeChromosome c) {
        OperatorGroup<GRNTreeChromosome> g = new OperatorGroup<>(
                Arrays.asList(
                    new AddRegulation(),
                    new DuplicateParts(),
                    new DuplicateTU(),
                    new SplitTU(),
                    new SwapParts(),
                    new AddPromoter(),
                    new MergeTU()), GRNTreeChromosome.class, 1);

        GRNTreeChromosome c2 = c;

        for (int i = 0; i < new Random().nextInt(MAX_INITIAL_MUTATIONS); i++) {
            c2 = g.apply(c2);
        }

        return c2;
    }
}
