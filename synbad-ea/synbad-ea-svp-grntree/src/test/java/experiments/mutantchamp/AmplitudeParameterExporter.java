/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experiments.mutantchamp;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.svpcompiler.parsing.BaseType;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPParameterType;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.mongo.MEvaluated;
import uk.ac.ncl.icos.mongo.MExperiment;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Part;
import uk.ac.ncl.intbio.virtualparts.entity.Property;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 *
 * @author owengilfellon
 */
public class AmplitudeParameterExporter {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    String MONGO_URL = "mongodb://192.168.34.82:27017";
    String DATABASE_NAME = "grntree-ea";

    private final String EXPERIMENTS_COL = "experiments";
    private final String CHROMOSOME_COL = "chromosomes";

    private final CodecRegistry pojoCodecRegistry;

    public AmplitudeParameterExporter() {
        this.pojoCodecRegistry = fromRegistries(com.mongodb.MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
    @Test
    public void targetAmplitudeTest() throws Exception {

        MongoClient mongoClient = MongoClients.create(MONGO_URL);
        MongoDatabase db = mongoClient.getDatabase(DATABASE_NAME);
        SVPManager m = SVPManager.getSVPManager();

        String EXPERIMENT_NAME = "amplitude_concrete_random_seeds_one_point_four";

        // Create collections if they don't already exist

        MongoCollection<MExperiment> experimentCollection = db.getCollection(EXPERIMENTS_COL, MExperiment.class).withCodecRegistry(pojoCodecRegistry);
        FindIterable<MExperiment> experiment = experimentCollection.find(Filters.regex("name", ".*" + EXPERIMENT_NAME + ".*"), MExperiment.class);
        MongoCursor<MExperiment> cursor = experiment.cursor();

        while(cursor.hasNext()) {
            MExperiment e = cursor.next();
            MongoCollection<MEvaluated> chromosomeCollection = db.getCollection(CHROMOSOME_COL, MEvaluated.class).withCodecRegistry(pojoCodecRegistry);
            MongoCursor<MEvaluated> c_it = chromosomeCollection.find(Filters.regex("experimentName", e.getName())).cursor();

            List<List<Double>> chromosomes = new LinkedList<>();

            int index = 0;
            int index2 = 0;

            FileWriter writer = new FileWriter("/Users/owengilfellon/Dropbox/PhD/Results/parameterSpace_" + e.getName() + ".txt", false);
            writer.write("Promoter RBS1 RBS2 RBS3 Fitness\r\n");
            Set<String> svpWrite = new HashSet<>();

            while(c_it.hasNext()) {
                MEvaluated eva = c_it.next();
                if(svpWrite.add(eva.getSvpwrite())) {
                    List<String> parts = SVPParser.getParts(eva.getSvpwrite(), BaseType.PROM, BaseType.RBS);
                    List<Double> chromosome = new LinkedList<>();
                    parts.stream().map(s -> m.getPart(s)).map(p-> (LeafNode)GRNTreeNodeFactory.getLeafNode(p, m.getInternalEvents(p), InterfaceType.NONE))
                            .filter(ln -> ln.getType() == SVPType.RBS || isConstitutivePromoter(ln.getSVP()))
                            .forEach(ln -> {
                                double value = ln.getParameters().stream().filter(p ->
                                        p.getParameterType() == SVPParameterType.TRANSLATION_RATE ||
                                        p.getParameterType() == SVPParameterType.TRANSCRIPTION_RATE).findFirst().orElse(null).getValue();
                                chromosome.add(value);
                            });

                    chromosome.add(eva.getFitness().getFitness());
                    Assert.assertEquals(5, chromosome.size());
                    chromosomes.add(chromosome);

                    if(index % 10 == 0) {
                        doWrite(chromosomes, writer);
                        System.out.println("Done " + index + " | " + index2);
                        chromosomes.clear();
                    }
                    index++;
                }
                index2++;
            }

            doWrite(chromosomes, writer);
            System.out.println("Done " + index + " | " + index2);
            writer.close();
        }

        mongoClient.close();
    }

    public void doWrite(List<List<Double>> chromosomes, FileWriter writer) throws IOException {
        for(List<Double> pars : chromosomes) {
            writer.write(pars.stream().map(Object::toString).collect(Collectors.joining(" ")));
            writer.write("\r\n");
        }
    }

    public boolean isConstitutivePromoter(Part part)
    {
        if(part.getDescription().toLowerCase().contains("constitutive"))
            return true;
        List<Property> properties = part.getProperties();
        if(properties!=null){
            for(Property p:properties){
                if( p.getValue().toLowerCase().contains("constitutive"))
                    return true;
            }
        }

        return false;
    }
}