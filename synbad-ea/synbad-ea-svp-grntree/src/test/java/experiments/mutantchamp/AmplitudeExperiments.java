/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experiments.mutantchamp;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.constraints.LowestConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngineBuilder;
import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.exporter.SvpMongoImporter;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPResponseCurveObserver;
import uk.ac.ncl.icos.eaframework.grn.observer.SvpMongoObserver;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SimpleCopasiSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.MinimisingTournament;
import uk.ac.ncl.icos.eaframework.selection.Tournament;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.*;

/**
 *
 * @author owengilfellon
 */
public class AmplitudeExperiments {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    String MONGO_URL = "mongodb://192.168.34.82:27017";
    String DATABASE_NAME = "grntree-ea";

    String INDEPENDENT_METABOLITE = "Subtilin";
    String DEPENDENT_METABOLITE = "GFP_rrnb";

    boolean RANDOMISE_INITIAL_SEEDS = true;
    // Experiment - set to 1000
    int GENERATIONS = 1000;

    int POPULATION_SIZE_1 = 25;
    int POPULATION_SIZE_2 = 25;

    boolean OVERLAPPING = true;
    int MUTATIONS_PER_GENERATION = 1;

    double MAXIMUM_GFP = 21428.5714;
    double STARTING_CONCENTRATION = 1089.689752370406;

    int DURATION = 21600;
    int STEPS = 1080;

    int THREADS = POPULATION_SIZE_2;//(int)Math.ceil((double)POPULATION_SIZE_2 / 2.0);

    public AmplitudeExperiments() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of build method, of class GRNTreeEngineBuilder.
     */
    @Test
    public void targetAmplitudeTest() {
        Map<Double, String> experimentSetups = new HashMap<>();
//        experimentSetups.put(0.6, "zero_point_six");
//        experimentSetups.put(0.8, "zero_point_eight");
//        experimentSetups.put(1.2, "one_point_two");
        experimentSetups.put(1.4, "one_point_four");

        for(Double target : experimentSetups.keySet()) {
            String name = "amplitude_concrete_" + (RANDOMISE_INITIAL_SEEDS ? "random_seeds_" : "") + experimentSetups.get(target);
            SvpMongoImporter importer = new SvpMongoImporter();

            EvoEngine<GRNTreeChromosome> evoEngine = importer.exportExists(MONGO_URL, DATABASE_NAME, name) ?
                    importer.getEngine( importer.getExport(MONGO_URL,  DATABASE_NAME, name)) :
                    setupMutantChampAmplitude(name, target);

            if(evoEngine.getPopulationStats().getCurrentGeneration() < GENERATIONS) {
                System.out.println("Starting " + name);
                evoEngine.attach(new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE)));
                evoEngine.attach(new SvpMongoObserver(MONGO_URL, DATABASE_NAME, name, DEPENDENT_METABOLITE));
                evoEngine.run();
            }
        }
    }

//    @Test
    public void targetAmplitudeTournamentTest() {

        int TOURNAMENT_SIZE = 2;
        Map<Double, String> experimentSetups = new HashMap<>();
//        experimentSetups.put(0.6, "zero_point_six");
//        experimentSetups.put(0.8, "zero_point_eight");
//        experimentSetups.put(1.2, "one_point_two");
        experimentSetups.put(1.4, "one_point_four");

        for(Double target : experimentSetups.keySet()) {
            String name = "amplitude_uniform_tournament_" + TOURNAMENT_SIZE +"_concrete_" +
                    (RANDOMISE_INITIAL_SEEDS ? "random_seeds_" : "") + experimentSetups.get(target);
            SvpMongoImporter importer = new SvpMongoImporter();

            EvoEngine<GRNTreeChromosome> evoEngine = importer.exportExists(MONGO_URL, DATABASE_NAME, name) ?
                    importer.getEngine( importer.getExport(MONGO_URL,  DATABASE_NAME, name)) :
                    setupUniformTournamentAmplitude(name, TOURNAMENT_SIZE, target);

            if(evoEngine.getPopulationStats().getCurrentGeneration() < GENERATIONS) {
                System.out.println("Starting " + name);
                evoEngine.attach(new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE)));
                evoEngine.attach(new SvpMongoObserver(MONGO_URL, DATABASE_NAME, name, DEPENDENT_METABOLITE));
                evoEngine.run();
            }
        }
    }


    public EvoEngine<GRNTreeChromosome> setupMutantChampAmplitude(String name, double TARGET_CONCENTRATION_FACTOR) {

        ChromosomeFactory<GRNTreeChromosome> cf = RANDOMISE_INITIAL_SEEDS ?
                getChromosomeFactoryRandomisedParts() :
                getChromosomeFactory();

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            SimpleCopasiSimulator cs = new SimpleCopasiSimulator(duration, runtime);
            cs.addInitialConcentration(INDEPENDENT_METABOLITE, 10000.0);
            return cs;
        };

        ConstraintHandler handler = new LowestConstraintHandler();
        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
                new ConcentrationConstraint<>("GFP_rrnb", STARTING_CONCENTRATION * TARGET_CONCENTRATION_FACTOR));
        List<SBMLModifier> modifiers = Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(cf)
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                // .setOverlapping(OVERLAPPING)
                .setOrderedPopulations(true, true)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
                        new RandomiseRBS(),
                        new RandomiseConstPromoter()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();

        return evoEngine;
    }

    public EvoEngine<GRNTreeChromosome> setupUniformTournamentAmplitude(String name, int TOURNAMENT_SIZE, double TARGET_CONCENTRATION_FACTOR) {

        ChromosomeFactory<GRNTreeChromosome> cf = RANDOMISE_INITIAL_SEEDS ?
                getChromosomeFactoryRandomisedParts() :
                getChromosomeFactory();

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            SimpleCopasiSimulator cs = new SimpleCopasiSimulator(duration, runtime);
            cs.addInitialConcentration(INDEPENDENT_METABOLITE, 10000.0);
            return cs;
        };

        ConstraintHandler handler = new LowestConstraintHandler();
        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(
                new ConcentrationConstraint<>("GFP_rrnb", STARTING_CONCENTRATION * TARGET_CONCENTRATION_FACTOR));
        List<SBMLModifier> modifiers = Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(cf)
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(OVERLAPPING)
                .setOrderedPopulations(false, true)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                .setReproductionSelection(new Uniform<>())
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
                        new RandomiseRBS(),
                        new RandomiseConstPromoter()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .setSurvivalSelection(new MinimisingTournament<>(TOURNAMENT_SIZE))
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();

        return evoEngine;
    }

    public ChromosomeFactory<GRNTreeChromosome> getChromosomeFactoryRandomisedParts() {

        List<GRNTreeChromosome> chromosomes = new LinkedList<>();

        SVPManager m = SVPManager.getSVPManager();

        for(int i = 0; i < POPULATION_SIZE_1; i++) {

            String promoter = m.getConstPromoter().getName();
            String rbs1 = m.getRBS().getName();
            String rbs2 = m.getRBS().getName();
            String rbs3 = m.getRBS().getName();

            StringBuilder b = new StringBuilder()
                    .append(promoter).append(":Prom; ")
                    .append(rbs1).append(":RBS; SpaK:CDS; ")
                    .append(rbs2).append(":RBS; SpaR:CDS; BO_4296:Ter; PspaS:Prom; ")
                    .append(rbs3).append(":RBS; GFP_rrnb:CDS; BO_4296:Ter");

            GRNTreeChromosome seed = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(b.toString()));

            chromosomes.add(seed);
        }

        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(chromosomes, POPULATION_SIZE_1);
        return cf;
    }


    public ChromosomeFactory<GRNTreeChromosome> getChromosomeFactory() {
        GRNTreeChromosome seed = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; "
                        + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter"));

        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(Arrays.asList(seed), POPULATION_SIZE_1);

        return cf;
    }

    public EvoEngine<GRNTreeChromosome> setupMathMutantChamp(String name, Constraint<TimeCourseResult<GRNTreeChromosome>, Double> constraint) {
        GRNTreeChromosome seed = new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaK:RBS; SpaK:CDS; RBS_SpaR:RBS; SpaR:CDS; BO_4296:Ter; "
                        + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter"));

        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(Arrays.asList(seed), POPULATION_SIZE_1);

        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE);
            return cs;
        };

        ConstraintHandler handler = new LowestConstraintHandler();
        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = Arrays.asList(constraint);
        List<SBMLModifier> modifiers = Arrays.asList(new SBMLModifier.RemoveInducibleBaseLevel());

        Evaluator<GRNTreeChromosome> evaluator = new GRNTreeCopasiConstraintEvaluator(
                simulatorFactory, handler, constraints, modifiers, DURATION, STEPS);

        EvoEngine<GRNTreeChromosome> evoEngine = new GRNTreeEngineBuilder()
                .setChromosomeFactory(cf)
                .setThreads(THREADS)
                .setEvaluator(evaluator)
                .setOverlapping(OVERLAPPING)
                .setOrderedPopulations(true, true)
                .setReproductionPopulationSize(POPULATION_SIZE_2)
                .setReproductionMutationsPerGeneration(MUTATIONS_PER_GENERATION)
                .setReproductionOperators(new OperatorGroup<>(Arrays.asList(
                        new RandomiseRBS(),
                        new RandomiseConstPromoter()), GRNTreeChromosome.class, MUTATIONS_PER_GENERATION))
                .setSurvivalPopulation(POPULATION_SIZE_1)
                .addTerminationCondition(new GenerationTermination(GENERATIONS))
                .build();

        evoEngine.attach(new SVPResponseCurveObserver(Arrays.asList(DEPENDENT_METABOLITE)));
        evoEngine.attach(new SvpMongoObserver(MONGO_URL, DATABASE_NAME, name + "_" + new Date().getTime(), DEPENDENT_METABOLITE));
        return evoEngine;
    }
}