package uk.ac.ncl.icos.eaframework.grn.operator.prototype;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


/**
 * Identifies all RBSs within the GRNTree, and replaces one
 * at random with a randomly chosen RBS part from the repository.
 * 
 * @author owengilfellon
 */
@EAModule(visualName = "Randomise RBS")
public class ARandomiseRBS extends AbstractOperator<GRNTreeChromosome> {
    
    private static final Logger logger = LoggerFactory.getLogger(ARandomiseRBS.class);
    private final Random r = new Random();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = c.duplicate();
        List<GRNTreeNode> allRBSs = new ArrayList<>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        
        /*
         * Identify all RBSs within the GRN Tree
         */
        
        while(it.hasNext()) {
            GRNTreeNode n = it.next();
            if(n instanceof LeafNode) {
                LeafNode ln = (LeafNode) n;
                if(ln.getType() == SVPType.RBS) {
                    allRBSs.add(n);
                }
            }
        }
        
        /*
         * If no constitutive promoters are found in the model, then the
         * mutation can not be applied. Return c.
         */
        
        if(allRBSs.isEmpty()) {
            return c;
        }
            
        try {

            /*
             * Replace a Promoter randomly selected from allPromoters
             * with a Promoter randomly retrieved from the repository.
             */

            GRNTreeNode  newRbs = SVPHelper.getSVPHelper().getRBS("RBS_" + Math.abs(r.nextInt()), r.nextDouble());
            allRBSs.get(r.nextInt(allRBSs.size())).replaceNode(newRbs);

        } catch (Exception ex) {
            logger.error(ex.getLocalizedMessage());
            return c;
        }
        
        return t;
    }
}
