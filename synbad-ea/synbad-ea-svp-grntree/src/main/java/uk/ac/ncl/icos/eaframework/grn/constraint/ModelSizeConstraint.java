/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
public class ModelSizeConstraint<T extends GRNTreeChromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {
    
    private final int MAX_UNPENALIZED_PARTS;
    private static final Logger logger = LoggerFactory.getLogger(ModelSizeConstraint.class);

    public ModelSizeConstraint(int maxUnpenalisedParts) {
        this.MAX_UNPENALIZED_PARTS = maxUnpenalisedParts;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addInt(MAX_UNPENALIZED_PARTS)
                .addClassName(getClass().getName())
                .build();
    }

    @Override
    public Double apply(TimeCourseResult<T> r) {  
        int parts = r.getChromosome().getPartsSize();
        double maxParts = (double)this.MAX_UNPENALIZED_PARTS;
        if(parts <= maxParts)
            return 1.0;
        
        double result = (1.0 / (((double)parts - maxParts) / getPenaltyFactor() + 1.0));
        //System.out.println("PartsSize: " + result);
        
        if(result > 1.0 || result < 0) {
            logger.error("Constraint is out of range: {}", result);
        }
        
        return result;
    } 

}
