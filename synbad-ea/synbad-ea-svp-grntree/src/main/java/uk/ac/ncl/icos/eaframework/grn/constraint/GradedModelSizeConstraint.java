/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
public class GradedModelSizeConstraint<T extends GRNTreeChromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {
    
    private final int MAX_UNPENALIZED_PARTS;
    private final int MAX_PARTS;
    private final Logger logger = LoggerFactory.getLogger(GradedModelSizeConstraint.class);
    
    public GradedModelSizeConstraint(int maxUnpenalisedParts, int maxParts) {
        this.MAX_UNPENALIZED_PARTS = maxUnpenalisedParts;
        this.MAX_PARTS = maxParts;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addInt(MAX_UNPENALIZED_PARTS)
                .addInt(MAX_PARTS).build();
    }

    @Override
    public Double apply(TimeCourseResult<T> r) {

        if(r.getChromosome() == null) {
            throw new IllegalArgumentException("Could not get chromosome from result");
        }

        int parts = r.getChromosome().getPartsSize();

        // create gradiant - penalise larger models, up to a penalty of 0.5 at max size

        if(parts <= MAX_UNPENALIZED_PARTS) {
            return 1.0;
        }
            
        
        // calculate penalty of up to 0 - 0.5 for parts above unpenalised parts,
        // but below max parts
        
        if(parts <= MAX_PARTS) {
            
            double penalisedParts = (double) parts - (double) MAX_UNPENALIZED_PARTS;
            double penalisedRange = (double) MAX_PARTS - (double ) MAX_UNPENALIZED_PARTS;
            
            // max parts should be larger than unpenalised parts
            
           // logger.debug("penalised parts : {} | penalisedRange: {}", penalisedParts, penalisedRange);
            
            double constraint = 1.0 - ((penalisedParts / (penalisedRange + 1)) * 0.5);
            
            if(constraint > 1.0 || constraint < 0) {
                logger.error("Constraint is out of range: {}", constraint);
            }
            
            return constraint;
        }
           

        
        // for above max parts, calculate penalty between 0.5 - 1 
        
        double constraint =  0.5 * (1.0 / (((double)parts - (double)MAX_PARTS)));
        
        if(constraint > 1.0 || constraint < 0) {
            logger.error("Constraint is out of range: {}", constraint);
        }
        
        return constraint;
    } 

}
