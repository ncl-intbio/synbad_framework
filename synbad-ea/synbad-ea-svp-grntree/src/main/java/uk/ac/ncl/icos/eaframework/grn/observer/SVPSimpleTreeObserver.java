package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 *
 * @author owengilfellon
 */
public class SVPSimpleTreeObserver implements EvolutionObserver<Chromosome>, Serializable {
    
    private final static Logger logger = LoggerFactory.getLogger(SVPSimpleTreeObserver.class);

    public SVPSimpleTreeObserver() {
    }

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s) {
       
            EvoEngine e = (EvoEngine) s;
            PopulationStats ps = e.getPopulationStats();
            double bestFitness = ps.getBestFitness();            
            StringBuilder sb = new StringBuilder();

            sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
            if(ps.getBestChromosome() instanceof GRNTreeChromosome){
                GRNTreeChromosome t = (GRNTreeChromosome) ps.getBestChromosome();
                sb.append("Parts: ").append(t.getPartsSize()).append(" | ");
            }

            sb.append("Curr Fit: ").append(ps.getBestFitness()).append(" | ");
            
            sb.append("Best Fit: ").append(bestFitness);
            logger.info(sb.toString());
        }


    
}
