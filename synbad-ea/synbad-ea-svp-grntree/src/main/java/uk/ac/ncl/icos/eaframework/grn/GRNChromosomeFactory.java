/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import uk.ac.ncl.icos.eaframework.chromosome.ChromosomeIdGenerator;
import uk.ac.ncl.icos.eaframework.factories.AbstractChromosomeFactory;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.grntree.api.GRNTree;

/**
 * A Factory class for generating populations of GRNTree, with or without diversity achieved by application of
 * mutation Operators.
 * @author owengilfellon
 */
public class GRNChromosomeFactory extends AbstractChromosomeFactory<GRNTreeChromosome> implements Serializable {

    
    private static final Logger LOGGER = LoggerFactory.getLogger(GRNChromosomeFactory.class);
    /**
     * @param seeds The seed models from which the population is generated
     * @param populationSize The number of instances to add to the population
     */
    public GRNChromosomeFactory(List<GRNTreeChromosome> seeds, int populationSize)
    {
         super(seeds, populationSize);
    }

    /**
     * Generates a population of GRNTree, created by duplicating random GRNTrees from the seed list
     * @return
     */
    @Override
    public List<GRNTreeChromosome> generatePopulation() {
        
        List<GRNTreeChromosome> pop = new ArrayList<>();
        
        LOGGER.debug("Generating population from seeds");
        Random r = new Random();
        for(int i=0; i<POPULATON_SIZE; i++)
        {
            GRNTreeChromosome c = seeds.get(r.nextInt(seeds.size()));
            GRNTree tree = c.getGRNTree();
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Adding " + tree.toString());
            //pop.add(c.duplicate(ChromosomeIdGenerator.getId(), new long[] { c.getId() } ));
            pop.add(c.duplicate());
        }
        return pop;
    }

    /**
     * Generates a population of GRNTree, created by duplicating random GRNTrees from the seed list, and performing
     * a single mutation on each, chosen at random from the provided operators.
     * @param operators the mutation operators to be applied to the seeds to generate diversity.
     * @return
     */
    @Override
    public List<GRNTreeChromosome> generatePopulation(List<Operator<GRNTreeChromosome>> operators) {
        List<GRNTreeChromosome> pop = new ArrayList<>();
        
        LOGGER.debug("Generating population from seeds and operators");
        Random r = new Random();
        for(int i=0; i<POPULATON_SIZE; i++){
            GRNTreeChromosome c = seeds.get(r.nextInt(seeds.size()));
            Operator<GRNTreeChromosome> o = operators.get(r.nextInt(operators.size()));
            GRNTreeChromosome mutatedTree = o.apply(c);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Applied " + o.getClass().getSimpleName() + " and added " + mutatedTree.toString() );
            pop.add(mutatedTree);
        }
        return pop;
    }




}
