package uk.ac.ncl.icos.eaframework.grn.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.datatype.SVPStats;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class SVPAllFitnessObserver implements EvolutionObserver<Chromosome>, Serializable {

    private final Logger logger = LoggerFactory.getLogger(SVPAllFitnessObserver.class);

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s) {

        PopulationStats ps = s.getPopulationStats();
        StringBuilder sb = new StringBuilder();
        Exporter<String> exporter = new SVPWriteExporter();

        for(EvaluatedChromosome<Chromosome> c : s.getEvaluatedSurvivalPopulation()) {
            sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
            sb.append("Fit: ").append(c.getFitness().getFitness());
            if(c.getChromosome() instanceof GRNTreeChromosome) {
                GRNTreeChromosome tree = (GRNTreeChromosome) c.getChromosome();
                sb.append(" | ").append(exporter.export(tree)).append("\n");
            }
        }

        logger.debug(sb.toString());
    }
}
