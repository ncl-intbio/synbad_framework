/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.population.*;
import uk.ac.ncl.icos.eaframework.selection.PopulationIteratorSelection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStatGenerator;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;

/**
 *
 * @author owengilfellon
 */
public class GRNTreeEngineBuilder implements SBEvoEngineBuilder<GRNTreeChromosome> {
    
    private final Logger LOGGER = LoggerFactory.getLogger(GRNTreeEngineBuilder.class);
    
    private String NAME_PREFIX = "Experiment_";

    private int GENERATON = 1;

    private int DEFAULT_MAX_GENERATIONS = 1000;
    
    private int THREAD_POOL_SIZE = 1;
    private int CHILD_POPULATION_SIZE = 30;
    private int SURVIVAL_POPULATION_SIZE = 10;
    
    private final int MUTATIONS_PER_GENERATION = 1;
    private int R_MUTATIONS_PER_GENERATION = 1;
    private int S_MUTATIONS_PER_GENERATION = 1;

    private boolean MINIMISE_FITNESS = false;

    private boolean isOverlapping = true;

    private boolean ordered = false;
    
    private OperatorGroup<GRNTreeChromosome> operator1 = null;
    private OperatorGroup<GRNTreeChromosome> operator2 = null;
    
    private Selection<GRNTreeChromosome> selection1 = null;
    private Selection<GRNTreeChromosome> selection2 = null;
    
    private ChromosomeFactory<GRNTreeChromosome> factory = null;

    private List<EvaluatedChromosome<GRNTreeChromosome>> populations = new LinkedList<>();

    private Evaluator<GRNTreeChromosome> evaluator = null;
    
    private final List<TerminationCondition> terminationConditions = new LinkedList<>();

    @Override
    public GRNTreeEngineBuilder setChromosomeFactory(ChromosomeFactory<GRNTreeChromosome> factory) {
        this.factory = factory;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setCurrentGeneration(int generation) {
        this.GENERATON = generation;
        return this;
    }

    public GRNTreeEngineBuilder setCurrentPopulation(List<EvaluatedChromosome<GRNTreeChromosome>> populations) {
        this.populations.addAll(populations);
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setOrderedPopulations(boolean ordered, boolean minimiseFitness) {
        this.ordered = ordered;
        this.MINIMISE_FITNESS = minimiseFitness;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setOrderedPopulations(boolean ordered) {
        this.ordered = ordered;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setEvaluator(Evaluator<GRNTreeChromosome> evaluator) {
        this.evaluator = evaluator;
        return this;
    }
    
    @Override
    public GRNTreeEngineBuilder setOverlapping(boolean overlapping) {
        this.isOverlapping = overlapping;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder addTerminationCondition(TerminationCondition condition) {
        terminationConditions.add(condition);
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setReproductionPopulationSize(int populationSize) {
        this.CHILD_POPULATION_SIZE = populationSize;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setReproductionOperators(OperatorGroup operators) {
        this.operator1 = operators;
        return this;
    }
    

    @Override
    public GRNTreeEngineBuilder setReproductionSelection(Selection<GRNTreeChromosome> selection) {
        this.selection1 = selection;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setSurvivalPopulation(int populationSize) {
        this.SURVIVAL_POPULATION_SIZE = populationSize;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setSurvivalOperators(OperatorGroup operators) {
        this.operator2 = operators;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setSurvivalSelection(Selection<GRNTreeChromosome> selection) {
        this.selection2 = selection;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setReproductionMutationsPerGeneration(int mutations) {
        this.R_MUTATIONS_PER_GENERATION = mutations;
        return this;
    }

    @Override
    public GRNTreeEngineBuilder setSurvivalMutationsPerGeneration(int mutations) {
        this.S_MUTATIONS_PER_GENERATION = mutations;
        return this;
    }

    private boolean check(String name, Object object) {
        if(object == null) {
            LOGGER.error("{} cannot be null", name);
            return false;
        } else {
            return true;
        }
    }

    private boolean check(String name1, Object object1, String name2, Object object2) {
        if(object1 == null && object2 == null) {
            LOGGER.error("{} and {} cannot be null", name1, name2);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public GRNTreeEngineBuilder setThreads(int threads) {
        this.THREAD_POOL_SIZE = threads;
        return this;
    }
    
    @Override
    public EvoEngine<GRNTreeChromosome> build() {
        
        if( (!ordered && !check("Selection 1", selection1)) || (!ordered && !check("Selection 2", selection2)) ||
                !check("Evaluator", evaluator) )
            return null;

        if(factory == null && populations.isEmpty()) {
            LOGGER.warn("Either a chromosome factory or a population must be specified");
            return null;
        }
        
        if( operator1 == null && operator2 == null) 
            LOGGER.warn("No evolutionary operators set");

        if( ordered ) {
            if(selection1 != null) {
                LOGGER.warn("Using ordered populations, ignoring selection 1");
            }

            if(selection2 != null) {
                LOGGER.warn("Using ordered populations, ignoring selection 2");
            }

            if(CHILD_POPULATION_SIZE != SURVIVAL_POPULATION_SIZE) {
                LOGGER.warn("Ordered child population size does not match survival population size, setting to {}", SURVIVAL_POPULATION_SIZE);
                CHILD_POPULATION_SIZE = SURVIVAL_POPULATION_SIZE;
            }
        }
        
        TerminationCondition t = terminationConditions.isEmpty() ? 
                new GenerationTermination(DEFAULT_MAX_GENERATIONS) :
                new TerminationGroup(terminationConditions);

        PopulationProcess<GRNTreeChromosome> reproduction =
            ordered ?
                (operator1 == null) ?
                    new OrderedEvaluationProcess<>(evaluator, new EvaluatedSvpChromFactory<>(), CHILD_POPULATION_SIZE, THREAD_POOL_SIZE) :
                    new OrderedSelectAndMutateProcess<>(
                            new PopulationIteratorSelection<>(), operator1, evaluator, new EvaluatedSvpChromFactory<>(),
                            MINIMISE_FITNESS, CHILD_POPULATION_SIZE, this.R_MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE) :
                (operator1 == null) ?
                        new SelectProcess<>(selection1, CHILD_POPULATION_SIZE) :
                        new SelectAndMutateProcess<>(
                                selection1, operator1, evaluator, new EvaluatedSvpChromFactory<>(),
                                CHILD_POPULATION_SIZE, this.R_MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);

        PopulationProcess<GRNTreeChromosome> survival =
            ordered ?
                new OrderedSelectProcess<>( new PopulationIteratorSelection<>(), SURVIVAL_POPULATION_SIZE, MINIMISE_FITNESS) :
                (operator2 == null) ?
                    new SelectProcess<>(selection2, SURVIVAL_POPULATION_SIZE) :
                    new SelectAndMutateProcess<>(
                            selection2, operator2, evaluator, new EvaluatedSvpChromFactory<>(),
                            SURVIVAL_POPULATION_SIZE, this.S_MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
            
        Strategy<GRNTreeChromosome> strategy = isOverlapping || ordered ?
            new Strategy.Overlapping<>(reproduction, survival) :
            new Strategy.NonOverlapping<>(reproduction, survival);

        PopulationStatGenerator generator = MINIMISE_FITNESS ?
                new PopulationStatGenerator.DecreasingFitnessStatGenerator() :
                new PopulationStatGenerator.IncreasingFitnessStatGenerator();

        if(populations.isEmpty()) {
           return new GRNTreeEngine(SURVIVAL_POPULATION_SIZE, factory, strategy, evaluator, t, generator, THREAD_POOL_SIZE);
        } else {
            return new GRNTreeEngine(GENERATON, populations, factory, strategy, evaluator, t, generator, THREAD_POOL_SIZE);
        }
    }
}
