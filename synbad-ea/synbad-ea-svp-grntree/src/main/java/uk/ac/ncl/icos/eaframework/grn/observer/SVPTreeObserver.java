package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.LeafNode;

/**
 *
 * @author owengilfellon
 */
public class SVPTreeObserver implements EvolutionObserver<Chromosome>, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(SVPTreeObserver.class);
    List<String> metabolites = new ArrayList<String>();
    double bestFitness = 0.0;
    
    public SVPTreeObserver(List<String> metabolitesToRecord) {
        this.metabolites = metabolitesToRecord;
    }

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s) {
       
            EvoEngine e = (EvoEngine) s;
            List<EvaluatedChromosome> pop = e.getEvaluatedSurvivalPopulation();
            PopulationStats ps = e.getPopulationStats();
            if(ps.getBestFitness() > bestFitness)
            {
                bestFitness= ps.getBestFitness();
            }
            StringBuilder sb = new StringBuilder();
            sb.append("===============================================================\n");
            sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
            sb.append("Pop: ").append(ps.getPopulationSize()).append(" | ");
            sb.append("Best Fit: ").append(ps.getBestFitness()).append(" | ");
            sb.append("Mean Fit: ").append(ps.getMeanFitness()).append(" | ");
            sb.append("Best All: ").append(bestFitness).append("\n");
            Exporter<String> exporter = new SVPWriteExporter();
            Chromosome ch = ps.getBestChromosome();
            if(ch instanceof GRNTreeChromosome)
            {
                GRNTreeChromosome tree = (GRNTreeChromosome) ch;
                sb.append(exporter.export(tree)).append("\n");
            }
            
            for(EvaluatedChromosome c:pop)
            {
                
                
                if(c.getChromosome() instanceof GRNTreeChromosome)
                {
                    GRNTreeChromosome tree = (GRNTreeChromosome) c.getChromosome();
                    
                    sb.append("Parts: [").append(tree.getPartsSize()).append("] | ");
                    
                    
                    
                    //sb.append(tree.debugInfo()).append("\n");
                    
                    Iterator<GRNTreeNode> it = tree.getPreOrderIterator();
                    
                    while(it.hasNext()) {
            
           
                        GRNTreeNode n = it.next();

                        sb.append("Depth: (").append(n.getDepth()).append(") ");

                        if(!n.getChildren().isEmpty()) {
                            sb.append("Nodes: [").append(n.getChildren().size()).append("] ");
                        }

                        if(n.getParent() == null) {
                            sb.append("ROOT ");
                        }

                        else if(n.isTranscriptionUnit()) {
                            sb.append("TU ");
                        }

                        else if (!(n instanceof LeafNode)) {
                            sb.append("MODULE ");
                        }

                        if(n instanceof LeafNode) {
                            LeafNode ln = (LeafNode) n;
                            sb.append(" - ").append(ln.getSVP().getName()).append(":").append(ln.getSVP().getType());
                        }
                        
                        sb.append("\n");

                       
                    }
                    
                 }
            }

            logger.debug(sb.toString());
    }
    
}
