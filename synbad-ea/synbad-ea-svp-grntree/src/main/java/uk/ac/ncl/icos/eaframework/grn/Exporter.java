package uk.ac.ncl.icos.eaframework.grn;

/**
 * Provides an interface for Exporters that take GRNTrees are arguments, and convert to another Type.
 * @param <T> Implementations must specify the Type of the Exported object
 */
public interface Exporter<T> {

    public T export(GRNTreeChromosome tree);
}
