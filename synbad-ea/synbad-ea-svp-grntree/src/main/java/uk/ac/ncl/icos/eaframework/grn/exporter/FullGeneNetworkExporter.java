package uk.ac.ncl.icos.eaframework.grn.exporter;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InteractionType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;

import java.util.*;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.io.GeneNetworkSerialiser;
import uk.ac.ncl.icos.grntree.traversal.FlowNavigator;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;

/**
 * Created by owengilfellon on 13/05/2014.
 */
public class FullGeneNetworkExporter implements Exporter<DirectedGraph> {

    private final static SVPManager m = SVPManager.getSVPManager();
    private Set<String> ids = new HashSet<String>();


    @Override
    public DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> export(GRNTreeChromosome tree) {

        List<GRNTreeNode> genes = new ArrayList<GRNTreeNode>();
        DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> network =
                new DefaultDirectedGraph<ExportNodeWrapper, ExportEdgeWrapper>(ExportEdgeWrapper.class);

        
        // ===================================================
        // Add nodes to graph
        // ===================================================

        Iterator<GRNTreeNode> i = tree.getPreOrderIterator();
        while(i.hasNext()) {
            GRNTreeNode node = i.next();
            if(node.isLeafNode()) {
                SVPType type = ((LeafNode)node).getType();
                if(type == SVPType.CDS) {
                    network.addVertex(new ExportNodeWrapper(node));
                    genes.add(node);
                }
                
            }
        }
        
        

        // ===================================================
        // Add edges to graph
        // ===================================================

        //System.out.println(tree.debugInfo());
   
        for(GRNTreeNode child: genes) {

            LeafNode cds = (LeafNode) child;
            List<GRNTreeNode> regulatedByCds = tree.getInteractingParts(cds);
            
            for(GRNTreeNode interactingPart:regulatedByCds) {
                
                //System.out.println("----" + interactingPart+  "." + interactingPart.hashCode() + " interacts with " + cds);
                boolean phosporylator = false;
                for(Interaction interaction:m.getInteractions(cds.getSVP(), ((LeafNode)interactingPart).getSVP())) {
                    InteractionType type = null;
                    if(interaction.getInteractionType().startsWith("Transcriptional activation")) {
                        type = InteractionType.ACTIVATION;
                    } else if(interaction.getInteractionType().startsWith("Transcriptional repression")) {
                        type = InteractionType.REPRESSION;
                    } else if(interaction.getInteractionType().equals("Phosphorylation")) {
                        type = InteractionType.PHOSPHORYLATION;
                        
                        boolean hasPhosphorylatedInput = false;
                        boolean hasDefaultOutput = false;
                        boolean hasPhosphorylatedOutput = false;
                        
                        
                        for(InteractionPartDetail ipd : interaction.getPartDetails()) {
                            if(cds.getName().startsWith(ipd.getPartName()) && ipd.getInteractionRole().equals("Input") && ipd.getPartForm().equalsIgnoreCase("phosphorylated")) {
                                hasPhosphorylatedInput = true;
                            }
                            if(cds.getName().startsWith(ipd.getPartName()) && ipd.getInteractionRole().equals("Output") && !ipd.getPartForm().equalsIgnoreCase("phosphorylated")) {
                                hasDefaultOutput = true;
                            }
                            
                            if(!cds.getName().startsWith(ipd.getPartName()) && ipd.getInteractionRole().equals("Output") && ipd.getPartForm().equalsIgnoreCase("phosphorylated")) {
                                hasPhosphorylatedOutput = true;
                            }
                        }
                        
                        phosporylator = hasDefaultOutput && hasPhosphorylatedInput && hasPhosphorylatedOutput;
                    } 
                    
                    


                    ExportNodeWrapper enw1  = new ExportNodeWrapper(child);
                    Set<ExportNodeWrapper> nws = new HashSet<>();

                    // if is regulation, create edge from protein to the proteins regulated

                    if(type == InteractionType.ACTIVATION || type == InteractionType.REPRESSION)  {
                         nws = getRegulatedCdsOf(interactingPart).stream().map(rcds -> new ExportNodeWrapper(rcds)).collect(Collectors.toSet());

                    } else {
                        if(phosporylator)
                            nws = network.vertexSet().stream().filter(n -> n.getName().startsWith(interactingPart.getName())).collect(Collectors.toSet());
                    } 

                    for(ExportNodeWrapper nw : nws) {
                        //if(!network.containsEdge(enw1, nw))
                            network.addEdge(enw1, nw, new ExportEdgeWrapper(type));
                     }
                }
            }
        }
           
        addSubtilin(network);

        return network;
    }
    
    private void addSubtilin(DirectedGraph<ExportNodeWrapper, ExportEdgeWrapper> network) {
        ExportNodeWrapper enw1  = new ExportNodeWrapper("Subtilin");
        network.addVertex(enw1);
        for(ExportNodeWrapper nw : network.vertexSet().stream().filter(n -> n.getName().startsWith("SpaK")).collect(Collectors.toSet())) {
                network.addEdge(enw1, nw, new ExportEdgeWrapper(InteractionType.PHOSPHORYLATION));
         }
    }
    
    public List<GRNTreeNode> getRegulatedCdsOf(GRNTreeNode prom) {
        
        //System.out.println("---" + prom + ":" + prom.getParent());
        
        List<GRNTreeNode> transcriptionalRegulatedCds = prom.getParent()
                .getChildren().stream()
                .filter(n -> (((LeafNode)n).getType() == SVPType.CDS))
                .collect(Collectors.toList());
        
        //transcriptionalRegulatedCds.stream().forEach(System.out::println);
        
        return transcriptionalRegulatedCds;

    }

    public boolean hasFeedback(LeafNode node, GRNTree tree, String part) {
        FlowNavigator fn = new FlowNavigator(tree, node);
        while(fn.hasNext())
            fn.next();
        return fn.getVisited().stream().flatMap(p -> p.getChildren().stream()).anyMatch(svp -> svp.getSVP().getName().equals(part));
    }
    
    public class ExportEdgeWrapper{

        private InteractionType type;
        private List<String> labels;

        public ExportEdgeWrapper(String label) {
            this.labels = new ArrayList<>();
            this.labels.add(label);
        }
        
        public ExportEdgeWrapper(InteractionType type, String... labels) {
            this.type = type;
            this.labels = new ArrayList<>();
            for(String label : labels) {
                this.labels.add(label);
            }
        }

        public String toString()
        {
            if(this.type == null) {
                return labels.get(0);
            }
            
            if(this.type == InteractionType.ACTIVATION) {
                return "+";//labels.contains("feedback") ? "f+" : "+";
            } else if (this.type == InteractionType.REPRESSION) {
                return "-";//labels.contains("feedback") ? "f-" : "-";
            } else {
                return "~";//labels.contains("feedback") ? "f~" : "~";
            }
        }

        public List<String> getLabels() {
            return labels;
        }
    }

    public class ExportNodeWrapper {

        private GRNTreeNode node;
        private List<String> labels;
        private final String name;

        public ExportNodeWrapper(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
        
        
        
        
        public ExportNodeWrapper(GRNTreeNode node, String... labels) {
            this.node = node;
            this.labels = new ArrayList<>();
            for(String label : labels) {
                this.labels.add(label);
            }
            
            StringBuilder sb = new StringBuilder();
            
            LeafNode leafNode = (LeafNode) node;
           
            String id = null;

            if(leafNode.getSVP().getDisplayName() != null) {
                id = leafNode.getSVP().getDisplayName();
                if(id.contains("||")) {
                    id = id.substring(0, id.indexOf("||"));
                }
            } else {
                id = leafNode.getSVP().getName();
            }

            int index = 1;
            String nodeId = id;

            if(!ids.add(nodeId)) {
                while (!ids.add(nodeId + index)) {
                    index++;
                }
                nodeId += index;
            }

            this.name = nodeId;
        }

        public GRNTreeNode getNode()
        {
            return node;
        }

        public List<String> getLabels() {
            return labels;
        }
        
        

        public String toString()
        {
            return name;
            
        }

        public boolean equals(Object obj)
        {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof ExportNodeWrapper))
                return false;

            return  node == null ?  
                    ((ExportNodeWrapper) obj).name.equals(this.name) :
                    ((ExportNodeWrapper) obj).getNode().equals(this.node);
           
        }

        @Override
        public int hashCode() {
            return node == null ? name.hashCode() : node.hashCode();
        }

    }
}



