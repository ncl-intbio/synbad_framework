package uk.ac.ncl.icos.eaframework.grn.observer;

import org.sbolstandard.core2.SBOLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.exporter.SynBadSbolExporter;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public class SVPSbolObserver implements EvolutionObserver<Chromosome>, Serializable {

    private final static Logger LOGGER = LoggerFactory.getLogger(SVPSbolObserver.class);
    private final SynBadSbolExporter exporter;

    public SVPSbolObserver(String projectName) {
        exporter = new SynBadSbolExporter(projectName);
    }

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s) {
        EvoEngine e = (EvoEngine) s;
        PopulationStats ps = e.getPopulationStats();

        if(ps.getBestChromosome() instanceof GRNTreeChromosome){
            GRNTreeChromosome t = (GRNTreeChromosome) ps.getBestChromosome();
            String sbol = exporter.export(t);
         //   LOGGER.error(sbol);
        }
    }
}
