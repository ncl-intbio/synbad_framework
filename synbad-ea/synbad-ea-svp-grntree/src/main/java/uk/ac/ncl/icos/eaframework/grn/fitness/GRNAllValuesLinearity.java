/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;

import java.util.List;


/**
 *
 * @author owengilfellon
 */
public class GRNAllValuesLinearity extends GRNSimulationEvaluator<GRNTreeChromosome> {
     
    public GRNAllValuesLinearity(SBSbmlSimulatorFactory<DynamicTimeCopasiSimulator> cs, int duration, int runtime)
    {
        super(cs, duration, runtime);
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {    
       
        DynamicTimeCopasiSimulator ds = (DynamicTimeCopasiSimulator) cs.getSimulator(getDuration(), getRuntime());        
        GRNTreeChromosome g = (GRNTreeChromosome) c;
       // Double t = Double.valueOf(target);
        try {

        } catch (Exception e) {
            e.printStackTrace();
            return new Fitness(0.0);
        }
        if(ds.run())
        {
            
            List<TimeCourseTrace> dependentTraces = ds.getDependentMetaboliteValues();
            TimeCourseTrace independentTraces = ds.getIndependentMetaboliteValues();
            
            double[] responseCurve = new double[dependentTraces.size()];
            double[] independentsRun = new double[independentTraces.size()];

            for(int i=0; i < dependentTraces.size(); i++)
            {
                // for each dependent trace, get last value and add to response curve
                
                responseCurve[i] = dependentTraces.get(i).get(dependentTraces.get(i).size() - 1);
                independentsRun[i] = independentTraces.get(i);
            }

            PearsonsCorrelation pc = new PearsonsCorrelation();
            double correlation = pc.correlation(independentsRun, responseCurve);
            double fitness = correlation > 0.0 ? correlation * 100 : 0.0;

            return new Fitness(fitness);
        }
        else
        {
            return new Fitness(0.0);
        }           
    } 
}
