package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.BranchNode;


/**
 * Identifies all empty TUs and removes one randomly
 * 
 * @author owengilfellon
 */
@EAModule(visualName = "Randomise TU")
public class RemoveTU extends AbstractOperator<GRNTreeChromosome> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveTU.class);
    private final SVPManager m = SVPManager.getSVPManager();
    private static final Random r = new Random();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        
        /*
         * Identify all empty TUs within the GRN Tree
         */
        
        List<GRNTreeNode> emptyTUs = new ArrayList<>();
        
        while(it.hasNext()) {
            GRNTreeNode n = it.next();
            if(n instanceof BranchNode) {
                BranchNode bn = (BranchNode) n;
                if(n.isTranscriptionUnit()) {
                    if(n.getChildren().stream()
                            .filter(node -> node.isLeafNode())
                            .map(node -> (LeafNode)node)
                            .noneMatch(node -> node.getType() == SVPType.CDS)) {
                        emptyTUs.add(n);
                    }
                }
            }
        }
        
        // Cannot be applied, return c;
        
        if(emptyTUs.isEmpty()) {
            LOGGER.warn("Could not apply operator, returning original chromosome");
            return c;
        }
           
        
        GRNTreeNode emptyTu = emptyTUs.get(r.nextInt(emptyTUs.size()));
        LOGGER.debug("Removing TU: " + emptyTu);
        emptyTu.getParent().removeNodeAndDescendents(emptyTu);

        return t;
    }
}
