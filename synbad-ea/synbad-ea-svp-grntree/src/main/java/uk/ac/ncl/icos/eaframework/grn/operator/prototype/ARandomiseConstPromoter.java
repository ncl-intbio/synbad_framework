package uk.ac.ncl.icos.eaframework.grn.operator.prototype;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;


/**
 * Identifies all Constitutive Promoters within the GRNTree, and replaces one
 * at random with a randomly chosen constitutive promoter from the repository.
 * 
 * @author owengilfellon
 */
@EAModule(visualName = "Randomise Constitutive Promoter")
public class ARandomiseConstPromoter extends AbstractOperator<GRNTreeChromosome> {
    
    
    private final static Logger LOGGER = LoggerFactory.getLogger(ARandomiseConstPromoter.class);
    private final Random r = new Random();
    private final SVPManager m = SVPManager.getSVPManager();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = c.duplicate();
        List<GRNTreeNode> allPromoters = new ArrayList<>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();
        
        /*
         * Identify all constitutive promoters within the GRN Tree
         */
        
        while(it.hasNext())
        {
            GRNTreeNode n = it.next();
            if(n instanceof LeafNode) {
                LeafNode leafNode = (LeafNode) n;
                if(leafNode.getType() == SVPType.Prom) {
                    if(m.isConstitutivePromoter(leafNode.getSVP())){
                            allPromoters.add(n);
                    }
                }
            }
        }

        
        /*
         * If no constitutive promoters are found in the model, then the
         * mutation can not be applied. Return c.
         */
        
        if(allPromoters.isEmpty()) {
            LOGGER.warn("Could not apply operator, returning original chromosome");
            return c;
        }

        try {

            /*
             * Replace a Promoter randomly selected from allPromoters
             * with a Promoter randomly retrieved from the repository.
             */

            GRNTreeNode promoter = allPromoters.get(r.nextInt(allPromoters.size()));
            GRNTreeNode promoter2 = SVPHelper.getSVPHelper().getConstPromoter("Prom_" + Math.abs(r.nextInt()), r.nextDouble() );

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Replacing {} with {} ", promoter, promoter2);
            
             promoter.replaceNode(promoter2);
                   
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return c;
        }
        
        return t;
    }
}
