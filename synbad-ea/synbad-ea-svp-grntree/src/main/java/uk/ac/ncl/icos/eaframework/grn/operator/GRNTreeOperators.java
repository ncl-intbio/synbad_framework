/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.operator;

/**
 *
 * @author owengilfellon
 */
public enum GRNTreeOperators implements Operators {

    AddPromoter("uk.ac.ncl.icos.eaframework.grn.operator"),
    AAddRegulation("uk.ac.ncl.icos.eaframework.grn.operator.prototype"),
    AddRegulation("uk.ac.ncl.icos.eaframework.grn.operator"),
    DuplicateParts("uk.ac.ncl.icos.eaframework.grn.operator"),
    DuplicatePromoter("uk.ac.ncl.icos.eaframework.grn.operator"),
    DuplicateTU("uk.ac.ncl.icos.eaframework.grn.operator"),
    MergeTU("uk.ac.ncl.icos.eaframework.grn.operator"),
    OperatorGroup("uk.ac.ncl.icos.eaframework.grn.operator"),
    ARandomiseConstPromoter("uk.ac.ncl.icos.eaframework.grn.operator.prototype"),
    ARandomiseRBS("uk.ac.ncl.icos.eaframework.grn.operator.prototype"),
    ARandomiseParameter("uk.ac.ncl.icos.eaframework.grn.operator.prototype"),
    RandomiseConstPromoter("uk.ac.ncl.icos.eaframework.grn.operator"),
    RandomiseRBS("uk.ac.ncl.icos.eaframework.grn.operator"),
    RemoveParts("uk.ac.ncl.icos.eaframework.grn.operator"),
    RemovePromoter("uk.ac.ncl.icos.eaframework.grn.operator"),
    RemoveRegulation("uk.ac.ncl.icos.eaframework.grn.operator"),
    SplitTU("uk.ac.ncl.icos.eaframework.grn.operator"),
    SwapParts("uk.ac.ncl.icos.eaframework.grn.operator"),
    RemoveTU("uk.ac.ncl.icos.eaframework.grn.operator");

    private final String pkg;

    private GRNTreeOperators(String pkg) {
        this.pkg = pkg;
    }

    @Override
    public String getPackage() {
        return pkg;
    }

    @Override
    public String getFullRef() {
        return getPackage().concat(".").concat(toString());
    }
}
