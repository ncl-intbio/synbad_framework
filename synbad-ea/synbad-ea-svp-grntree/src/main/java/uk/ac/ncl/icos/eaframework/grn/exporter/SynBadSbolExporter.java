package uk.ac.ncl.icos.eaframework.grn.exporter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.ICompilable;
import uk.ac.ncl.icos.svpcompiler.Compilable.PartBundle;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.io.SBSbolExporter;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBSvpBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SvpActionBuilderImpl;
import uk.ac.ncl.icos.synbad.svp.obj.Svp;
import uk.ac.ncl.icos.synbad.svp.obj.SvpInteraction;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.parser.SVPInteractionReader;
import uk.ac.ncl.icos.synbad.svp.parser.SVPReader;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceBuilder;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.io.ByteArrayOutputStream;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Given a GRNTree, returns an SBOL Document describing the model that the tree represents.
 * @author owengilfellon
 */
public class SynBadSbolExporter implements Exporter<String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SynBadSbolExporter.class);


    private final String prefix = "http://www.synbad.org";
    private final String version = "1.0";
    private SBWorkspace workspace;
    private SBIdentityFactory idFac;
    private URI workspaceURI = SBIdentityHelper.getURI("http://www.synbad.org/eaExports/1.0");
    private long index = 0;
    private final String projectName;

    public SynBadSbolExporter(String projectName)
    {
        this.workspace = new SBWorkspaceBuilder(workspaceURI)
//            .addService(new PiUpdater())
        //    .addService(new EnvConstantUpdater())
//            .addService(new SviUpdater())
//            .addService(new ExportedFcUpdater())
//            .addService(new SBWireUpdater())
                .build();
        this.idFac = workspace.getIdentityFactory();
        this.projectName = projectName;
    }


    /**
     * The model is added to an SBOL document as a Collection. Each transcription unit is added to the Collection as a
     * DnaComponent. Parts within transcription units are added to them as subcomponents. The SBOL descriptions of each
     * individual Part are retrieved from the SVP repository.
     * @param tree A GRNTree containing SVPs that are taken from the SVP Repository.
     * @return An SBOL Document describing the model in the GRNTree.
     */
    @Override
    public String export(GRNTreeChromosome tree)
    {
        URI context = getContext();
        workspace.createContext(context);
        LOGGER.trace("Parsing model...");
        createModuleFromCompilables(idFac.getIdentity(context), tree.getGRNTree(), new URI[]{context});
        LOGGER.trace("Export to SBOL...");
        SBSbolExporter exporter = new SBSbolExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        exporter.export(os, workspace, new URI[]{ context });
        byte[] bytes = os.toByteArray();
        String s = new String( bytes );
        LOGGER.trace("Done");
        workspace.removeContext(context);
        return s;
    }

    private URI getContext() {

        String contextString = projectName;

        synchronized (LOGGER) {
            contextString = contextString + index++;
        }

        URI context = URI.create(prefix + "/" + contextString + "/" + version);

        return context;
    }

    private SvpModule createModuleFromCompilables(SBIdentity identity, GRNTree tree, URI[] CONTEXTS) {

        List<ICompilable> compilables = GRNCompilableFactory.getCompilables(tree);
        LOGGER.debug("Setting up workspace...");
        Map<String, PartBundle> bundles = new HashMap<>();
        // returns a list of parsed SVPs and populates bundles
        List<Svp> svps = parseParts(compilables, bundles, CONTEXTS);
        Set<SvpInteraction> parseInteractions = parseInteractions(compilables, bundles, CONTEXTS);

        SBSvpBuilder b = new SvpActionBuilderImpl(workspace, CONTEXTS)
                .createSvm(identity);

        for(Svp svp : svps) {
            b.addSvp(identity, workspace.getIdentityFactory().getIdentity(svp.getIdentity()), null);
        }

        workspace.perform(b.build());
        return workspace.getObject(identity.getIdentity(), SvpModule.class, CONTEXTS);
    }

    private SvpModule createModuleFromCompilables(SBIdentity identity, List<ICompilable> compilables, URI[] CONTEXTS) {

        LOGGER.debug("Setting up workspace...");
        Map<String, PartBundle> bundles = new HashMap<>();
        // returns a list of parsed SVPs and populates bundles
        List<Svp> svps = parseParts(compilables, bundles, CONTEXTS);
        Set<SvpInteraction> parseInteractions = parseInteractions(compilables, bundles, CONTEXTS);

        SBSvpBuilder b = new SvpActionBuilderImpl(workspace, CONTEXTS)
                .createSvm(identity);

        for(Svp svp : svps) {
            b.addSvp(identity, workspace.getIdentityFactory().getIdentity(svp.getIdentity()), null);
        }

        workspace.perform(b.build());
        return workspace.getObject(identity.getIdentity(), SvpModule.class, CONTEXTS);
    }

    private List<Svp> parseParts(List<ICompilable> compilables, Map<String, PartBundle> bundles, URI[] CONTEXTS) {

        SVPReader reader = new SVPReader(workspace, CONTEXTS);
        List<Svp> svps = new LinkedList<>();

        for(ICompilable compilable : compilables) {

            if(!compilable.getPart().getType().equals("mRNA")) {

                svps.add(reader.read(compilable.getPart(), compilable.getInternalInteractions()));

                Map<Interaction, List<Part>> interactionMap = new HashMap<>();
                for(Interaction interaction : compilable.getInteractions().keySet()) {
                    List<Part> parts = compilables.stream().filter(c -> interaction.getParts().contains(c.getPart().getName()))
                            .map(c -> c.getPart()).collect(Collectors.toList());
                    interactionMap.put(interaction, parts);
                }

                PartBundle bundle = interactionMap.isEmpty() ?
                        new PartBundle(compilable.getPart(), compilable.getInternalInteractions() == null ? Collections.EMPTY_LIST : compilable.getInternalInteractions()) :
                        new PartBundle(compilable.getPart(), compilable.getInternalInteractions() == null ? Collections.EMPTY_LIST : compilable.getInternalInteractions(), interactionMap);

                bundles.put(bundle.getPart().getName(), bundle);
            }
        }
        return svps;
    }

    private Set<SvpInteraction> parseInteractions(List<ICompilable> compilables, Map<String, PartBundle> bundles, URI[] CONTEXTS) {

        SVPInteractionReader iReader = new SVPInteractionReader(workspace, CONTEXTS);
        Set<SvpInteraction> svpInteractions = new HashSet<>();

        for(ICompilable compilable : compilables) {

            for(Interaction interaction : compilable.getInteractions().keySet()) {
                if(interaction.getParts().stream().allMatch(p -> bundles.containsKey(p))) {
                    Map<String, PartBundle> participantBundles = new HashMap<>();
                    interaction.getParts().forEach(p -> {
                        participantBundles.put(p, bundles.get(p));
                    });
                    svpInteractions.add(iReader.read(interaction, participantBundles));
                }
            }
        }

        return svpInteractions;

    }
}
