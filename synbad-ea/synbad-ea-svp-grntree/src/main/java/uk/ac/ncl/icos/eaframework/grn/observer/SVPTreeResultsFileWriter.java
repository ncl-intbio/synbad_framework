package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.exporter.GRNTreeEngineSave;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.filemanagement.Export;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;

/**
 *
 * @author owengilfellon
 */
public class SVPTreeResultsFileWriter implements EvolutionObserver<GRNTreeChromosome>, Serializable {

    private final List<String> metabolites;
    private final String experimentName;
    private final Logger logger = LoggerFactory.getLogger(SVPTreeResultsFileWriter.class);
    
    public SVPTreeResultsFileWriter(String experimentName,
                                    List<String> metabolitesToRecord) {
         this.metabolites = metabolitesToRecord;
         this.experimentName = experimentName;
    }

    public String getExperimentDirectory()
    {
        return experimentName;
    }

    @Override
    public<V extends EvoEngine<GRNTreeChromosome>> void update(V s) {

        GRNTreeEngine e = (GRNTreeEngine) s;

        //System.out.print("Serializing....");

        final File homeDir = new File(System.getProperty("user.home"));
        File directory = new File(homeDir + "/" +  "Results" + "/" + experimentName + "/" +  experimentName);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                logger.error("Couldn't create directory: {}", experimentName);
            }
        }

        File file = new File(directory + "/" + "Save.sez");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(new GRNTreeEngineSave((GRNTreeEngine)e));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        //System.out.println("...done");


        int parameter = 0;
            List<EvolutionObserver<GRNTreeChromosome>> observers = e.getObservers();

            for(EvolutionObserver<GRNTreeChromosome> observer:observers) {
                if(observer instanceof ParameterScheduler) {
                    ParameterScheduler scheduler = (ParameterScheduler) observer;
                    parameter = (int)scheduler.getParameter();
                }
            }

            EvaluatedChromosome evaluatedChromosome = (EvaluatedChromosome) e.getEvaluatedSurvivalPopulation().get(0);
            GRNTreeChromosome t = (GRNTreeChromosome) evaluatedChromosome.getChromosome();
            Fitness f = evaluatedChromosome.getFitness();

            /*
             * Write Operation used
             */

            String operator = evaluatedChromosome.getOperator() == null ? "NONE" : evaluatedChromosome.getOperator();
            List<Double> fitness = new ArrayList<>();
            fitness.add(f.getFitness());
            List<Double> responseCurve = new ArrayList<>();
            StringBuilder penalties = new StringBuilder();

                 GRNTreeCopasiConstraintEvaluator se =  e.getFitnessEvaluator();

                if(se.getSimulatorFactory() instanceof DynamicTimeCopasiSimulator) {
                    DynamicTimeCopasiSimulator dtcs = (DynamicTimeCopasiSimulator) se.getSimulatorFactory();
                    responseCurve = dtcs.getDependentMetaboliteValues().stream().map(
                            list -> list.get(list.size() - 1)).collect(Collectors.toList());
                }     /**/
          
            Exporter<String> exporter = new SVPWriteExporter();

            EvaluatedChromosome<GRNTreeChromosome> bestModel = new EvaluatedChromosome<>(e.getPopulationStats().getBestChromosome(), new Fitness(e.getPopulationStats().getBestFitness()), null);
            String model =  bestModel.getFitness().getFitness() > evaluatedChromosome.getFitness().getFitness() ?
                        exporter.export((GRNTreeChromosome)bestModel.getChromosome()) :
                        exporter.export((GRNTreeChromosome)evaluatedChromosome.getChromosome());

            if (!Export.exportString(null,
                    "Results",
                    experimentName,
                    experimentName,
                    "BestModels.txt",
                    null,
                    "BestModel_" + e.getPopulationStats().getCurrentGeneration() + " " + model + "\n",
                    true)) {  logger.error("Cannot export best chromosome");}


            /*
            if(penalties!=null && !penalties.toString().equals("")) {

                if (!Export.exportString(     null,
                                               "Results",
                                               experimentName,
                                               experimentName + "_" + timestamp,
                                               "FitnessFunctionData_" + timestamp + ".txt",
                                               null,
                                               penalties.toString(),
                                               true)) {  System.out.println("Cannot export all fitness function data");}
            }*/

            if (!Export.exportString(  null,
                                       "Results",
                                       experimentName,
                                       experimentName,
                                       "Operators.txt",
                                       null,
                                       "Operator_" + e.getPopulationStats().getCurrentGeneration() +" " + operator + "\n",
                                           true)) {  logger.error("Cannot Export Operator");}

            /*
             * Write SVPWrite
             */




            if (!Export.exportString(  null,
                                       "Results",
                                       experimentName,
                                       experimentName,
                                       "Models.txt",
                                       null,
                                       "Model_" + e.getPopulationStats().getCurrentGeneration() +" " + exporter.export(t) + "\n",
                                       true)) {  logger.error("Cannot Export Model Description");}



            /*
             * Write Fitnesses
             */


            if(fitness != null && !fitness.isEmpty()) {
                if (!Export.exportDoubles(     null,
                                               "Results",
                                               experimentName,
                                               experimentName,
                                               "AllFitnesses.txt",
                                               fitness,
                                               true)) {  logger.error("Cannot export all fitness results");}
            }
            else {
                if (!Export.exportString(  null,
                                           "Results",
                                           experimentName,
                                           experimentName,
                                           "Operators.txt",
                                           null,
                                               "!ERROR - Fitness is empty\n",
                                       true)) {  logger.error("Cannot Export Model Description");}
            }
            /*
             * Write Response Curve
             */


            if(responseCurve!= null && !responseCurve.isEmpty()){
                if (!Export.exportDoubles( null,
                                               "Results",
                                               experimentName,
                                               experimentName,
                                               "ResponseCurves.txt",
                                               responseCurve,
                                               true)) {  logger.error("Cannot export all acentotic values");}
            }
            else {

                ArrayList<Double> empty = new ArrayList<Double>();
                for(int i=0; i<50; i++){
                    empty.add(0.0);
                }

                if (!Export.exportDoubles( null,
                                               "Results",
                                               experimentName,
                                               experimentName,
                                               "ResponseCurves.txt",
                                               empty,
                                               true)) {  logger.error("Cannot export empty acentotic values");}
            }




            if(e.getTerminationCondition().shouldTerminate(e.getPopulationStats())){
                if (!Export.exportString(  null,
                                           "Results",
                                           experimentName,
                                           experimentName,
                                           "Termination.txt",
                                           null,
                                           "Successfully completed at generation " + e.getPopulationStats().getCurrentGeneration(),
                                           true)) {  logger.error("Cannot Export Termination");}
            }

        
    }
}
