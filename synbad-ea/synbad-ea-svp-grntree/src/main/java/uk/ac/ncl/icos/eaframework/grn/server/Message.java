package uk.ac.ncl.icos.eaframework.grn.server;

import com.google.gson.Gson;

import javax.websocket.*;

public class Message {

    private String from;
    private String to;
    private String content;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static class MessageEncoder implements Encoder.Text<Message> {

        private static Gson gson = new Gson();

        @Override
        public String encode(Message message) throws EncodeException {
            return gson.toJson(message);
        }

        @Override
        public void init(EndpointConfig endpointConfig) {
            // Custom initialization logic
        }

        @Override
        public void destroy() {
            // Close resources
        }
    }

    public static class MessageDecoder implements Decoder.Text<Message> {

        private static Gson gson = new Gson();

        @Override
        public Message decode(String s) throws DecodeException {
            return gson.fromJson(s, Message.class);
        }

        @Override
        public boolean willDecode(String s) {
            return (s != null);
        }

        @Override
        public void init(EndpointConfig endpointConfig) {
            // Custom initialization logic
        }

        @Override
        public void destroy() {
            // Close resources
        }
    }
}



