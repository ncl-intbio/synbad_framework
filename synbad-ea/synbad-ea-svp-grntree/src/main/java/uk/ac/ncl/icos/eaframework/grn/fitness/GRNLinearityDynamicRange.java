/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.List;

/**
 *
 * @author owengilfellon
 */

@EAModule(visualName = "Linearity - Dynamic Range")
public class GRNLinearityDynamicRange extends GRNSimulationEvaluator<GRNTreeChromosome> {
    
    private static final Logger logger = LoggerFactory.getLogger(GRNLinearityDynamicRange.class);
    private static double targetRange = 0.0;
    private final double WEIGHT = 0.002;

    public GRNLinearityDynamicRange(SBSbmlSimulatorFactory<DynamicTimeCopasiSimulator> cs, int duration, int runtime)
    {
        super(cs, duration, runtime);
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {    
       
        DynamicTimeCopasiSimulator ds = (DynamicTimeCopasiSimulator) cs.getSimulator(getDuration(), getRuntime());        
        GRNTreeChromosome g = (GRNTreeChromosome) c;
       // Double t = Double.valueOf(target);
        try {
            CompilationDirector d = new CompilationDirector(SVPManager.getRepositoryUrl());
            d.setModelCompilables(GRNCompilableFactory.getCompilables(g.getGRNTree()));
            ds.setModel(d.getSBMLString("model"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Fitness(0.0);
        }
        if(ds.run()) {
            
            /*
             * Common to both fitness evaluations - move to another class?
             */
            
            List<TimeCourseTrace> dependentTraces = ds.getDependentMetaboliteValues();
            TimeCourseTrace independentTraces = ds.getIndependentMetaboliteValues();
            
            double[] responseCurve = new double[dependentTraces.size()];
            double[] independentsRun = new double[independentTraces.size()];

            for(int i=0; i < dependentTraces.size(); i++)
            {
                // for each dependent trace, get last value and add to response curve
                
                responseCurve[i] = dependentTraces.get(i).get(dependentTraces.get(i).size() - 1);
                independentsRun[i] = independentTraces.get(i);
            }
            
            // Linearity Fitness
            
            PearsonsCorrelation pc = new PearsonsCorrelation();
            double correlation = pc.correlation(independentsRun, responseCurve);
            
            // linearity fitness between 0 - 1
            
            double linearityFitness = correlation > 0.0 ? correlation : 0.0;
            double observedRange = Math.abs(responseCurve[responseCurve.length -1] - responseCurve[0]);
            
            if(targetRange == 0.0) {
                targetRange = observedRange;
            }
            
            // Amplitude Fitness between 0 - 1
                
            logger.debug("Target: {}", targetRange);
            logger.debug("Observ: {}", observedRange);
            
            double rangeFitness;
            
            if(observedRange > targetRange)
            {
                rangeFitness = 1.0;
            }
            else
            {
                rangeFitness = 1.0 / Math.sqrt(1.0 + (Math.abs(observedRange - targetRange) * WEIGHT));
            }
        
            // Calculate combined fitness
            
            logger.debug("Range: {}", rangeFitness);
            logger.debug("Linearity: {}", linearityFitness);
            
            double finalFit = linearityFitness * rangeFitness;
            
            logger.debug("Final fitness: {} ", finalFit);
            
            // return new Fitness(finalFit * 100.0);
            
            return new Fitness(Math.min(rangeFitness, linearityFitness) * 100.0);
        }
        else
        {
            return new Fitness(0.0);
        }           
    } 
}
