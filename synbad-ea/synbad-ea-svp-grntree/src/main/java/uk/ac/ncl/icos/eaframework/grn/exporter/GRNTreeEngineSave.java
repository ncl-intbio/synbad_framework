package uk.ac.ncl.icos.eaframework.grn.exporter;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Stores data required to reconstruct a GRNTreeEngine
 * Created by owengilfellon on 29/05/2014.
 */
public class GRNTreeEngineSave implements Serializable {

    private int generation;
    private List<EvaluatedChromosomeSave> population = new ArrayList<EvaluatedChromosomeSave>();
    private EvaluatedChromosomeSave bestChromosome;


    public GRNTreeEngineSave(GRNTreeEngine e) {
        createSave(e);
    }

    private void createSave(GRNTreeEngine e) {
        SVPWriteExporter exporter = new SVPWriteExporter();

        for(EvaluatedChromosome<GRNTreeChromosome> ev: e.getEvaluatedSurvivalPopulation()) {
            String chromosome = exporter.export((GRNTreeChromosome) ev.getChromosome());
            Double fitness = ev.getFitness().getFitness();
            String operator = ev.getOperator();
            population.add(new EvaluatedChromosomeSave(chromosome, fitness, operator));
        }

        EvaluatedChromosome<GRNTreeChromosome> t =  
                new EvaluatedChromosome<>(
                        e.getPopulationStats().getBestChromosome(),
                        new Fitness(e.getPopulationStats().getBestFitness()), null);
        if(t!=null) {
            String chromosome = exporter.export((GRNTreeChromosome) t.getChromosome());
            Double fitness = t.getFitness().getFitness();
            String operator = t.getOperator();
            bestChromosome = new EvaluatedChromosomeSave(chromosome, fitness, operator);
        }

        generation = e.getPopulationStats().getCurrentGeneration();
    }

    public int getGeneration() {
        return generation;
    }

    public List<EvaluatedChromosomeSave> getPopulation() {
        return population;
    }

    public EvaluatedChromosomeSave getBestChromosome() {
        return bestChromosome;
    }

    public class EvaluatedChromosomeSave implements Serializable{

        private String chromosome;
        private Double fitness;
        private String operator;

        public EvaluatedChromosomeSave(String chromosome, Double fitness, String operator)
        {
            this.chromosome = chromosome;
            this.fitness = fitness;
            this.operator = operator == null ? "NONE" : operator;
        }

        public String getChromosome() {
            return chromosome;
        }

        public Double getFitness() {
            return fitness;
        }

        public String getOperator() {
            return operator;
        }
    }
}


