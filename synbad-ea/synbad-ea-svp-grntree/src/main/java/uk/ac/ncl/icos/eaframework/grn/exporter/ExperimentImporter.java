/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.exporter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.PopulationProcess;
import uk.ac.ncl.icos.eaframework.Strategy;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.grn.fitness.ASbmlConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.population.SelectAndMutateProcess;
import uk.ac.ncl.icos.eaframework.population.SelectProcess;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.PopulationIteratorSelection;
import uk.ac.ncl.icos.eaframework.selection.SigmaScaledRouletteWheel;
import uk.ac.ncl.icos.eaframework.selection.Tournament;
import uk.ac.ncl.icos.eaframework.selection.Truncation;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.hibernate.HExperiment;
import uk.ac.ncl.icos.hibernate.HOperator;
import uk.ac.ncl.icos.hibernate.HSelectionProcess;
import uk.ac.ncl.icos.hibernate.HibernateUtil;

/**
 *
 * @author owengilfellon
 */
public class ExperimentImporter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ExperimentImporter.class);
    
    static int TRUNCATION_SIZE = 12;
    static int TOURNAMENT_SIZE = 30;
    static  int MUTATIONS_PER_GENERATION = 1;
    static final int THREAD_POOL_SIZE = 30;
    static String NAME_PREFIX = "Experiment_";
    
    public static List<HExperiment> getExperiments() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        EntityManager m = session.getEntityManagerFactory().createEntityManager();
        TypedQuery<HExperiment> query = m.createQuery("SELECT e FROM MExperiment e WHERE e.name LIKE '" + NAME_PREFIX +  "%'", HExperiment.class);
        List<HExperiment> exp = null;
        try {
            exp = query.getResultList();
        } catch( NoResultException e ) {
            exp = new ArrayList<>();
        }
        m.close();
        t.commit();
        session.close();
        return exp;
    }

    public static List<HExperiment> getUnfinishedExperiments(List<HExperiment> experiments) {
        return experiments.stream()
            .filter(e -> {
                int currentGeneration = e.getPopulations().get(e.getPopulations().size()-1).getGeneration();
                return e.getMaxGenerations() > currentGeneration;})
            .collect(Collectors.toList());
    }
    
    public static List<Operator<GRNTreeChromosome>> getOperators(HExperiment experiment, HSelectionProcess process) {
       
        List<Operator<GRNTreeChromosome>> operators = new ArrayList<>();
            for(HOperator operator : process.getOperators()) {
                try {
                    Class operatorClass = Class.forName("uk.ac.ncl.icos.eaframework.grn.operator." + operator.getOperatorName());
                    Constructor constructor = operatorClass.getConstructor();
                    operators.add((Operator<GRNTreeChromosome>) constructor.newInstance());
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage());
                }
            
            }
            return operators;
    }
    
    public static Strategy getStrategy(HExperiment experiment,  ASbmlConstraintEvaluator f) {               
     
        if(experiment == null) return null;
        
        // TO DO: Load population processes from DB
        
         Strategy<GRNTreeChromosome> strategy = new Strategy.Overlapping<>(
                getSelectionProcess(experiment, experiment.getReproductionProcess(), f), 
                getSelectionProcess(experiment, experiment.getSurvivalProcess(), f));
         
         return strategy;
    }
    
    
     public static PopulationProcess<GRNTreeChromosome> getSelectionProcess(HExperiment experiment, HSelectionProcess selection, ASbmlConstraintEvaluator f) {

        if(selection.getStrategyName().equals("SigmaScaledRouletteWheel")) {
            List<Operator<GRNTreeChromosome>> operators = new ArrayList<>();
            
            
            
            for(HOperator operator : selection.getOperators()) {
                try {
                    Class operatorClass = Class.forName("uk.ac.ncl.icos.eaframework.grn.operator." + operator.getOperatorName());
                    Constructor constructor = operatorClass.getConstructor();
                    operators.add((Operator<GRNTreeChromosome>) constructor.newInstance());
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage());
                }
            }

            
            Operator<GRNTreeChromosome> o = selection.getWeights() != null ?
                    new OperatorGroup(operators, GRNTreeChromosome.class, MUTATIONS_PER_GENERATION, selection.getWeights().getWeights(), selection.getWeights().isSigmaScaled()) :
                    new OperatorGroup(operators, GRNTreeChromosome.class, MUTATIONS_PER_GENERATION);
            return new SelectAndMutateProcess<>(new SigmaScaledRouletteWheel<>(true), o, f, new EvaluatedSvpChromFactory(), selection.getPopulationSize(), MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
        }
        
        if(selection.getStrategyName().equals("Uniform"))
           return new SelectProcess<>(new Uniform(), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("Truncation"))
           return new SelectProcess<>(new Truncation<>(TRUNCATION_SIZE), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("Tournament"))
            return new SelectProcess<>(new Tournament<>(), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("FitnessProportionalTournamnet"))
            return new SelectProcess<>( new FitnessProportionalTournament<>(TOURNAMENT_SIZE), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("PopulationIteratorSelection")) {
            LOGGER.debug("Found PopulationIterator!");
            return new SelectProcess<>( new PopulationIteratorSelection<>(), selection.getPopulationSize());
        }
            
        return null;
    }
    
}
