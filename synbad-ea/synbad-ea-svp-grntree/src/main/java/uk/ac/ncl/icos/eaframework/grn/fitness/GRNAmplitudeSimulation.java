/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DependentValueCopasiSimulator;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.ArrayList;

/**
 *
 * @author owengilfellon
 */

@EAModule(visualName = "Amplitude")
public class GRNAmplitudeSimulation extends GRNSimulationEvaluator<GRNTreeChromosome> {
    
    double target;
    
    public GRNAmplitudeSimulation(SBSbmlSimulatorFactory<DependentValueCopasiSimulator> cs, double target, int duration, int runtime)
    {
        super(cs, duration, runtime);
        this.target = target;
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {    
        DependentValueCopasiSimulator ds = (DependentValueCopasiSimulator) cs.getSimulator(getDuration(), getRuntime());        
        GRNTreeChromosome g = (GRNTreeChromosome) c;
        Double t = Double.valueOf(target);
        try {
            CompilationDirector d = new CompilationDirector(SVPManager.getRepositoryUrl());
            d.setModelCompilables(GRNCompilableFactory.getCompilables(g.getGRNTree()));
            ds.setModel(d.getSBMLString("model"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Fitness(0.0);
        }
        ds.run();
        ArrayList<Double> dependentsFinalRun = ds.getDependentMetaboliteValues().get(ds.getDependentMetaboliteValues().size()-1);
        Double o = dependentsFinalRun.get(dependentsFinalRun.size()-1);
        return new Fitness(Math.abs(t - o)); 
    }

}
