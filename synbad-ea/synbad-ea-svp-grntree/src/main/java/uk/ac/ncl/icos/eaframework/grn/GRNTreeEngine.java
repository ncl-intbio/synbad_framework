package uk.ac.ncl.icos.eaframework.grn;

import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.evoengines.AbstractEvoEngine;
import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.TerminationCondition;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Evaluator;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.CFitness;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.Strategy;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.population.OrderedEvaluationProcess;
import uk.ac.ncl.icos.eaframework.stats.PopulationStatGenerator;

/**
 * Implementation of EvoEngine with functionality specific to evolving GRNs. Uses GRNTreeChromosome as a GRN representation.
 *
 * @see GRNTreeChromosome
 * @author owengilfellon
 */
public class GRNTreeEngine extends AbstractEvoEngine<GRNTreeChromosome> {


    
    private static final Logger LOGGER = LoggerFactory.getLogger(GRNTreeEngine.class);

    /**
     * The Evolutionary Algorithm is instantiated with its necessary components
     *
     * @param cf a ChromosomeFactory for generating populations
     * @param e a Fitness function for evaluating a solution's behaviour in comparison to some target behaviour
     * @param t a Termination Condition, or group of Termination Conditions, that determine when to exit the algorithm
     */
    public GRNTreeEngine(
            int generation,
            List<EvaluatedChromosome<GRNTreeChromosome>> initialPopulation,
            ChromosomeFactory<GRNTreeChromosome> cf,
            Strategy<GRNTreeChromosome> strategy,
            Evaluator<GRNTreeChromosome> e,
            TerminationCondition t,
            PopulationStatGenerator statGenerator,
            int poolSize)
    {
        super(cf, strategy, e, t, statGenerator, poolSize);
        LOGGER.info("Reconstructing GRNTreeEngine at generation " + generation);
        this.currentGeneration = generation;
        this.evaluatedSurvivalPopulation = new LinkedList<>(initialPopulation);
        population = evaluatedSurvivalPopulation.stream().map(EvaluatedChromosome::getChromosome).collect(Collectors.toList());
    }

    public GRNTreeEngine(
            int survivalPopulationSize,
            ChromosomeFactory<GRNTreeChromosome> cf,
            Strategy<GRNTreeChromosome> strategy,
            Evaluator<GRNTreeChromosome> e,
            TerminationCondition t,
            PopulationStatGenerator statGenerator,
            int poolSize)
    {
        super(cf, strategy, e, t, statGenerator, poolSize);

        LOGGER.info("Constructing new GRNTreeEngine");

        this.population = cf.generatePopulation();

        // for identical population!

        LOGGER.debug("Evaluating initial population");

        Map<String, CFitness> fitnesses = new HashMap<>();
        for(int i = 0; i < survivalPopulationSize; i++) {
            GRNTreeChromosome c = population.get(i);
            if(!fitnesses.containsKey(c.toString())) {
                fitnesses.put(c.toString(), e.evaluate(c));
            }

            CFitness f = fitnesses.get(c.toString());
            evaluatedSurvivalPopulation.add(new EvaluatedSvpChromosome<>(c, f, f.getResult(), null));
        }
    }

    public GRNTreeEngine(
            int survivalPopulationSize,
            ChromosomeFactory<GRNTreeChromosome> cf,
            Strategy<GRNTreeChromosome> strategy,
            Evaluator<GRNTreeChromosome> e,
            TerminationCondition t)
    {
        this(survivalPopulationSize, cf, strategy, e, t, new PopulationStatGenerator.IncreasingFitnessStatGenerator(), 1);
    }

    @Override
    public GRNTreeCopasiConstraintEvaluator getFitnessEvaluator() {
        return (GRNTreeCopasiConstraintEvaluator)super.getFitnessEvaluator();
    }

    @Override
    public List<EvaluatedSvpChromosome<GRNTreeChromosome>> getEvaluatedReproductionPopulation() {
        return getEvolutionaryStrategy().getReproduction().getPopulation().stream()
                .filter(ec  -> ec instanceof EvaluatedSvpChromosome)
                .map(ec -> (EvaluatedSvpChromosome<GRNTreeChromosome>) ec).collect(Collectors.toList());
    }
 
    @Override
    public List<EvaluatedSvpChromosome<GRNTreeChromosome>> getEvaluatedSurvivalPopulation() {
        return getEvolutionaryStrategy().getSurvival().getPopulation().stream()
                .filter(ec  -> ec instanceof EvaluatedSvpChromosome)
                .map(ec -> (EvaluatedSvpChromosome<GRNTreeChromosome>) ec).collect(Collectors.toList());
    }
}