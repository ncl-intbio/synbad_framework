package uk.ac.ncl.icos.eaframework.grn.exporter;

import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;

/**
 * Given a GRNTree, returns an SVPWrite string describing the model that the tree represents.
 * @author owengilfellon
 */
public class SVPWriteExporter implements Exporter<String> {

    /**
     * The toString() method of GRNTree is used to retrieve an SVPWrite String. The toString() method on GRNTree calls
     * the toString() method on it's root node, which recursively calls toString() on it's children. The IDs of any
     * parts within the tree are concatenated with part types, and separators (";").
     * @param tree A GRNTree
     * @return A compilable SVPWrite string
     */
    @Override
    public String export(GRNTreeChromosome tree)
    {
        String s  = tree.toString();
        return s.substring(0, s.length()-2);
    }
}
