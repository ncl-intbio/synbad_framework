package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTreeHelper;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;

/**
 * Adds regulation to a TU, and places the regulating TF in an existing TU, or in
 * a newly created TU under control of a constitutive promoter. Positive
 * or negative regulation is chosen, by choosing the relevant part. TFs for that part
 * identified, and valid locations for those two part determined.
 * The regulated part is added to a valid location, and an valid existing or new TU
 * chosen at random for the TF.
 */
@EAModule(visualName = "Add Promoter")
public class AddPromoter extends AbstractOperator<GRNTreeChromosome> {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(AddPromoter.class);
    private final SVPManager m = SVPManager.getSVPManager();
    private final int        MAX_ATTEMPTS = 10;
    private final Random r = new Random();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;
        
        try {
            
             boolean mutationOccured = false;
            int attempt = 0;

            // =================================================================
            // Perform mutation in a while loop. If any exceptional cases occur,
            // while loop is repeated until mutation is successful.
            // =================================================================
            
            while(!mutationOccured && attempt < MAX_ATTEMPTS) {

                boolean promoterAdded = false;
                boolean regulatorRequired = false;
                boolean regulatorAdded = false;
                boolean phosphorylationRequired = false;
                boolean phosphorylationAdded = true;

                // =================================================================
                // Each mutation attempt uses a clean copy of the solution
                // =================================================================

                t = (GRNTreeChromosome) c.duplicate();
                Part promoter = m.getPromoter();
                List<BranchNode> potentialLocations = GRNTreeHelper.getBranchNodes(t.getGRNTree());

                if(!potentialLocations.isEmpty()) {

                    int locationIndex = r.nextInt(potentialLocations.size());

                    // ========================================================
                    // Insert promoter at beginning of TU
                    // ========================================================

                    potentialLocations.get(locationIndex)
                        .addNode(0, GRNTreeNodeFactory.getLeafNode(promoter, InterfaceType.INPUT));
                    promoterAdded = true;
                }

                List<Part> potentialTranscriptionFactors = m.getModifierParts(promoter);

                if( potentialTranscriptionFactors != null && !potentialTranscriptionFactors.isEmpty()) {

                    regulatorRequired = true;
                    Part regulatingPart = potentialTranscriptionFactors.get( r.nextInt( potentialTranscriptionFactors.size() ) );

                    // =================================================================
                    // All locations should currently be valid, as we are not using
                    // modularity features of GRNTree
                    // TODO replace these checks with something more efficient
                    // =================================================================


                    List<BranchNode> potentialRegulatingTUs = GRNTreeHelper.getBranchNodes(t.getGRNTree());


                    if(!potentialRegulatingTUs.isEmpty()) {

                        List<Interaction> interactions = m.getInteractions( regulatingPart, promoter );

                        // =================================================================
                        // This check no longer seems to be applicable, as there are part
                        // with more than one interaction (e.g. a CDS that both induces and
                        // represses a Promoter)
                        // TODO Test this and update as needed
                        // =================================================================

                        if(interactions.size() != 1) {
                            throw new Exception("No 1 interaction between " + promoter.getName() + " and " + regulatingPart.getName());
                        }

                        // =================================================================
                        // TODO If already present, add with 50% probability
                        // =================================================================

                        regulatorAdded = addPartToModel(regulatingPart, t);

                        // =================================================================
                        // Determine whether phosphorylation is required for regulation
                        // =================================================================

                        List<Part> phosphorylatingParts = null;
                        List<InteractionPartDetail> interactionPartDetails = interactions.get( 0 ).getPartDetails();

                        for(InteractionPartDetail interactionPartDetail : interactionPartDetails) {
                            if( interactionPartDetail.getPartForm().equals( "Phosphorylated" ) ) {

                                phosphorylationRequired = true;
                                phosphorylatingParts  = m.getSinglePhosphorylationPath(regulatingPart);

                                if( phosphorylatingParts == null || phosphorylatingParts.isEmpty() ) {
                                    //System.out.println("No phosphorylating parts for " + regulatingPart.getName());
                                }
                            }
                        }
                        
                        if( phosphorylatingParts != null && !phosphorylatingParts.isEmpty()) {

                            // =================================================================
                            // As for TF, add phosphorylating part to an existing or new TU
                            // =================================================================

                            // =================================================================
                            // TODO If already present, add with 50% probability
                            // =================================================================

                            for(Part phosphorylatingPart :phosphorylatingParts) {
                                if(!addPartToModel(phosphorylatingPart, t)) {
                                    phosphorylationAdded = false;
                                }
                            }
                        } else {
                            phosphorylationAdded = false;
                        }

                    }
                }

                boolean unregulatedPromoterAdded = promoterAdded && !regulatorRequired;
                boolean regulatedPromoterAdded = promoterAdded && regulatorRequired && regulatorAdded && !phosphorylationRequired ;
                boolean promoterRegulaterAndActivatorAdded = promoterAdded && regulatorRequired && regulatorAdded && phosphorylationRequired && phosphorylationAdded;

                if ( unregulatedPromoterAdded || regulatedPromoterAdded || promoterRegulaterAndActivatorAdded ) {
                    mutationOccured = true;
                    LOGGER.debug("Successfully added promoter[{}], regulatorAdded[{}], phosphorylators[{}]",  promoterAdded, regulatorAdded, phosphorylationAdded);
                }

                attempt++;

                if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                    LOGGER.warn("Could not apply operator, returning original chromosome");
                    return c;
                }
            }
            
            
        } catch (Exception ex) {

            // ======================================================
            // In the case of an exception being thrown, the original
            // chromosome is returned, without mutation
            // ======================================================

            LOGGER.error(ex.getMessage());
            return c;
        }  
        
        return t;
    }

    private boolean addPartToModel(Part part, GRNTreeChromosome t) {


        // =================================================================
        // The probability of adding the part to an existing TU, or to a new
        // TU is equally weighted (i.e. 1 / ( # of potentialTUs + 1 ) )
        // =================================================================

        try {

            List<BranchNode> potentialPhosphorylatingTU = GRNTreeHelper.getBranchNodes(t.getGRNTree());
            int phosphorylatingTuIndex = r.nextInt( potentialPhosphorylatingTU.size() + 1 );
            if(phosphorylatingTuIndex == potentialPhosphorylatingTU.size()) {

                LOGGER.debug("Adding part {} to new constitutive TU", part);
                
                List<GRNTreeNode> partsList = new ArrayList<>();
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getConstPromoter(), InterfaceType.INPUT));
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
                partsList.add(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT));
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getTerminator(), InterfaceType.NONE));
                potentialPhosphorylatingTU.get(phosphorylatingTuIndex-1)
                        .getParent()
                        .addNode(GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE, partsList));

                return true;

            }  else {
                
                LOGGER.debug("Adding part {} to existing TU", part);

                GRNTreeNode phosphorylatingTU = potentialPhosphorylatingTU.get(phosphorylatingTuIndex);
                int additionIndex = phosphorylatingTU.getChildren().size() - 1;
                phosphorylatingTU.addNode(additionIndex, GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
                additionIndex++;
                phosphorylatingTU.addNode(additionIndex, GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT));
               return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String toString()
    {
        return "Add Promoter";
    }
}
