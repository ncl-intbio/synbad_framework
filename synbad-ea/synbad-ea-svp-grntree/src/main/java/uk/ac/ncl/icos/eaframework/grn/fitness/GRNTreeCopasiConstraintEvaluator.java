/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.sbml.jsbml.SBMLDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.ConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBCacheingSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.SingletonCopasiAPI;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.PartsHandler;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @author owengilfellon
 */

public class GRNTreeCopasiConstraintEvaluator extends ASbmlConstraintEvaluator<GRNTreeChromosome> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(GRNTreeCopasiConstraintEvaluator.class);

    public GRNTreeCopasiConstraintEvaluator(
            SBSbmlSimulatorFactory cs, 
            ConstraintHandler handler,
            List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints,
            List<SBMLModifier> modifiers,
            int duration, 
            int runtime) {
        super(cs, handler, constraints, modifiers, duration, runtime);
    }

    @Override
    public RCCFitness evaluate(GRNTreeChromosome c) {
        SBSbmlSimulator s = cs.getSimulator(getDuration(), getRuntime());
        CompilationDirector d = new CompilationDirector(SVPManager.getRepositoryUrl());
        d.setSbmlModifiers(getModifiers());
        d.setModelCompilables(GRNCompilableFactory.getCompilables(c.getGRNTree()));

        try {
            synchronized (SBMLDocument.class) {
                s.setModel(d.getSBMLString("model"));
            }
        } catch (Exception e) {
            LOGGER.warn("Could not evaluate chromosome: {}", c.toString(), e);
            return new RCCFitness(0.0, Collections.EMPTY_LIST, Collections.EMPTY_LIST);
        }

        synchronized (SBMLDocument.class) {
            if (!s.run()) {
                LOGGER.error("Could not run simulation: {}", c.toString());
                return null;
            }
        }

        // get simulation result
        
        List<TimeCourseTraces> results = SBCacheingSimulator.class.isAssignableFrom(s.getClass()) ?
            ((SBCacheingSimulator)s).getAllResults() :
            Arrays.asList(s.getResults());
        
        TimeCourseResult<GRNTreeChromosome> r = new TimeCourseResult(results, c, 0);     
        
        // process constraints in parallel
        
        List<CResult> constraintResults = getConstraints().stream().parallel()
                .map(constraint -> new CResult(constraint.toString(), constraint.apply(r)))
                .collect(Collectors.toList());
        
        // get double values from constraints
        
        List<Double> constraintResultValues = constraintResults.stream().parallel()
                .map(CResult::getEvaluation)
                .collect(Collectors.toList());
        
        // get combined fitness from handler
        
        double fitness = getHandler().processConstraints(constraintResultValues).getFitness();

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Returning fitness: {}:{}", fitness, c.toString());

        return new RCCFitness(fitness, constraintResults, results);
    }
}
