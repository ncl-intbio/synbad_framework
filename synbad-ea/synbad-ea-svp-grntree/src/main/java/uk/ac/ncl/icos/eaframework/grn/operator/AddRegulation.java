package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTreeHelper;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;


/**
 * Adds regulation to a TU, and places the regulating TF in an existing TU, or in
 * a newly created TU under control of a constitutive promoter. Positive
 * or negative regulation is chosen, by choosing the relevant part. TFs for that part
 * identified, and valid locations for those two part determined.
 * The regulated part is added to a valid location, and an valid existing or new TU
 * chosen at random for the TF.
 */
public class AddRegulation extends AbstractOperator<GRNTreeChromosome> {
    
    private final static Logger logger = LoggerFactory.getLogger(AddRegulation.class);
    private final SVPManager m = SVPManager.getSVPManager();
    private final int        MAX_ATTEMPTS = 10;
    private Random           r = new Random();

    private BranchNode createNewTuFromCdsPart(Part part) {
        List<GRNTreeNode> partsList = new ArrayList<>();
        partsList.add(GRNTreeNodeFactory.getLeafNode(m.getConstPromoter(), InterfaceType.INPUT));
        partsList.add(GRNTreeNodeFactory.getLeafNode(m.getRBS(), InterfaceType.NONE));
        partsList.add(GRNTreeNodeFactory.getLeafNode(part, InterfaceType.OUTPUT));
        partsList.add(GRNTreeNodeFactory.getLeafNode(m.getTerminator(), InterfaceType.NONE));
        return GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE, partsList);
    }

    private LeafNode getNode(Part p) {
        SVPType t =  SVPType.fromString(p.getType());
        return  GRNTreeNodeFactory.getLeafNode(p,
                t == SVPType.CDS ?
                        InterfaceType.OUTPUT :
                t == SVPType.Prom ||
                t == SVPType.Op ?
                        InterfaceType.INPUT :
                        InterfaceType.NONE );
    }

    private LeafNode lastOfType(BranchNode parent, SVPType type) {
        List<LeafNode> ofType = parent.getChildren(type);

        if (ofType.isEmpty())
            return null;

        return ofType.get(ofType.size() - 1);
    }

    private LeafNode randomOfType(BranchNode parent, SVPType type) {
        List<LeafNode> potentials = parent.getChildren(type);
        return potentials.get(r.nextInt(potentials.size()));
    }

    private void addAllAtIndex(BranchNode parent, int additionIndex, LeafNode... nodes) throws Exception {
        int index = additionIndex;
        for(LeafNode p : nodes) {
            parent.addNode(index++, p);
        }
    }

    private void replaceAllAtIndex(BranchNode parent, int additionIndex, LeafNode... nodes) throws Exception {
        int parentSize = parent.getChildren().size();
        int index = additionIndex;
        for(LeafNode ln : nodes) {
            if(index < parentSize) {
                parent.getChildren().get(index++).replaceNode(ln);
            } else {
                parent.addNode(ln);
            }
        }
    }

    private void addAfterLastOfType(SVPType type, BranchNode parent, LeafNode... parts) throws Exception {
        int additionIndex = parent.getChildren().indexOf(lastOfType(parent, type));
        addAllAtIndex(parent, additionIndex + 1, parts);
    }

    private void addAfterRandomOfType(BranchNode parent, SVPType type, LeafNode... nodes) throws Exception {
        int index = parent.getChildren().indexOf(randomOfType(parent, type));
        addAllAtIndex(parent, index + 1, nodes);
    }

    private void replaceRandomOfType(SVPType type, BranchNode parent, LeafNode... nodes) throws Exception {
        int index = parent.getChildren().indexOf(randomOfType(parent, type));
        replaceAllAtIndex(parent, index, nodes);
    }

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;
        
        try {
            
            boolean mutationOccured = false;
            int attempt = 0;

            // =================================================================
            // Perform mutation in a while loop. If any exceptional cases occur,
            // while loop is repeated until mutation is successful.
            // =================================================================
            
            while(!mutationOccured && attempt < MAX_ATTEMPTS) {

                boolean regulatedAdded = false;
                boolean regulatorAdded = false;
                boolean phosphorylationRequired = false;
                boolean phosphorylationAdded = true;

                // =================================================================
                // Each mutation attempt uses a clean copy of the solution
                // =================================================================

                t = (GRNTreeChromosome) c.duplicate();

                Part regulatedPart = r.nextInt(2) == 0 ?
                                     m.getInducPromoter() :
                                     m.getNegOperator();

                List<Part> modifiers = m.getModifierParts(regulatedPart);


                if( !modifiers.isEmpty() ) {


                    Part modifier = modifiers.get( r.nextInt( modifiers.size() ) );

                    // =================================================================
                    // All locations should currently be valid, as we are not using
                    // modularity features of GRNTree
                    // TODO replace these checks with something more efficient
                    // =================================================================

                    List<BranchNode> regulatableTus = GRNTreeHelper.getBranchNodes(t.getGRNTree());
                    List<BranchNode> regulatingTus =  regulatableTus;
                    List<GRNTreeNode> potentialPhosphorylatingTU = null;
                    
                    if(!regulatableTus.isEmpty() && !regulatingTus.isEmpty()) {

                        int regulatedIndex = r.nextInt(regulatableTus.size());

                        BranchNode regulatedTu = regulatableTus.get( regulatedIndex );
                        LeafNode regulatedNode = GRNTreeNodeFactory.getLeafNode( regulatedPart, InterfaceType.INPUT );
                        LeafNode regulatingNode = GRNTreeNodeFactory.getLeafNode( modifier, InterfaceType.OUTPUT );

                        // =================================================================
                        // Currently, the mathematical definition of SVPs requires positive
                        // regulation to be implemented using Inducible Promoters, and negative
                        // regulation to be implemented using Operators.
                        // =================================================================

                        if( regulatedNode.getType() == SVPType.Prom ) {
                            if (r.nextInt(2) == 0) {
                                replaceRandomOfType(regulatedNode.getType(), regulatedTu,  regulatedNode);
                            } else {
                                regulatedTu.addNode(0, regulatedNode);
                            }
                            regulatedAdded = true;
                        } else if ( regulatedNode.getType() == SVPType.Op ) {
                            addAfterRandomOfType(regulatedTu, SVPType.Prom, regulatedNode);
                            regulatedAdded = true;
                        }
                        
                        List<Interaction> interactions = m.getInteractions( modifier, regulatedPart );

                        // =================================================================
                        // This check no longer seems to be applicable, as there are part
                        // with more than one interaction (e.g. a CDS that both induces and
                        // represses a Promoter)
                        // TODO Test this and update as needed
                        // =================================================================

                        if(interactions.size() != 1) {
                            throw new Exception("No 1 interaction between " + regulatedPart.getName() + " and " + modifier.getName());
                        }

                        // =================================================================
                        // The probability of adding the TF to an existing TU, or to a new
                        // TU is equally weighted (i.e. 1 / ( # of potentialTUs + 1 ) )
                        // =================================================================

                        // =================================================================
                        // TODO If already present, add with 50% probability
                        // =================================================================

                        regulatorAdded = addCdsToNewOrRandomTu(modifier, t);

                        // =================================================================
                        // Determine whether phosphorylation is required for regulation
                        // =================================================================

                        List<InteractionPartDetail> interactionPartDetails = interactions.get( 0 ).getPartDetails();

                        for(InteractionPartDetail interactionPartDetail : interactionPartDetails) {
                            if( interactionPartDetail.getPartForm().equals( "Phosphorylated" ) ) {

                                phosphorylationRequired = true;
                                List<Part> phosphorylatingParts = m.getSinglePhosphorylationPath(modifier);

                                // =================================================================
                                // TODO If already present, add with 50% probability
                                // =================================================================

                                if( phosphorylatingParts.isEmpty() ) {
                                    phosphorylationAdded = false;
                                }

                                for(Part phosphorylatingPart: phosphorylatingParts) {
                                    if(!addCdsToNewOrRandomTu(phosphorylatingPart, t)) {
                                        phosphorylationAdded = false;
                                    }
                                }
                            }
                        }
                    }
                }

                if ( ( regulatedAdded && regulatorAdded && phosphorylationRequired && phosphorylationAdded ) ||
                    ( regulatedAdded && regulatorAdded && !phosphorylationRequired ) ) {
                    mutationOccured = true;
                }

                attempt++;

                if(!mutationOccured && attempt == MAX_ATTEMPTS) {
                    return c;
                }
            }
            
            
        } catch (Exception ex) {

            // ======================================================
            // In the case of an exception being thrown, the original
            // chromosome is returned, without mutation
            // ======================================================

            logger.error(ex.getMessage());
            return c;
        }  
        
        return t;
    }

    private boolean addCdsToNewOrRandomTu(Part part, GRNTreeChromosome t) {


        // =================================================================
        // The probability of adding the part to an existing TU, or to a new
        // TU is equally weighted (i.e. 1 / ( # of potentialTUs + 1 ) )
        // =================================================================

        try {

            List<BranchNode> allBranchNodes = GRNTreeHelper.getBranchNodes(t.getGRNTree());
            int randomIndex = r.nextInt( allBranchNodes.size() + 1 );
            if(randomIndex == allBranchNodes.size()) {
                BranchNode toAdd = createNewTuFromCdsPart(part);
                t.getRootNode().addNode(toAdd);
                return true;
            }  else {
                BranchNode randomBranchNode = allBranchNodes.get(randomIndex);
                addAfterLastOfType(SVPType.CDS, randomBranchNode, getNode(m.getRBS()), getNode(part));
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public String toString() {
        return "Add Regulation";
    }
}
