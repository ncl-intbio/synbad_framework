package uk.ac.ncl.icos.eaframework.grn.operator.prototype;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;
import uk.ac.ncl.icos.grntree.api.SVPHelper;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPParameterType;
import uk.ac.ncl.intbio.virtualparts.entity.*;

import java.util.*;
import java.util.stream.Collectors;


/**

 * @author owengilfellon
 */
@EAModule(visualName = "Randomise Parameter")
public class ARandomiseParameter extends AbstractOperator<GRNTreeChromosome> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ARandomiseParameter.class);
    private final Random r = new Random();
    private final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private final Set<String> types;

    public ARandomiseParameter(SVPParameterType... types) {
        this.types = Arrays.stream(types).map(SVPParameterType::getMathName).collect(Collectors.toSet());
    }

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = c.duplicate();

        List<LeafNode> nodes = new ArrayList<>();

        t.getGRNTree().getPreOrderIterator().forEachRemaining(n -> {
            if(n.isLeafNode()) {
                if(((LeafNode)n).getInternalEvents().stream()
                    .anyMatch(i -> types.isEmpty() || i.getParameters().stream().anyMatch( p -> types.contains(p.getParameterType()))))
                    nodes.add((LeafNode)n);
            }
        });

        List<Interaction> interactions = t.getGRNTree().getInteractions().stream()
                .filter(i -> types.isEmpty() || i.getParameters().stream().anyMatch( p -> types.contains(p.getParameterType())))
                .collect(Collectors.toList());

//        if(LOGGER.isDebugEnabled())
//            LOGGER.debug("Found {} parts and {} interactions with a matching parameter", nodes.size(), interactions.size());

        int indexSize = nodes.size() + interactions.size();

        if(indexSize <= 0)
        {
            LOGGER.error("No nodes or interactions in chromosome: {}", t);
            return c;
        }

        int index = new Random().nextInt(indexSize);

        if(index < nodes.size()) {

            // Mutate internal interaction parameter

            LeafNode toReplace = nodes.get(index);
            GRNTreeNode replacer = perturb(toReplace);

            try {
                toReplace.replaceNode(replacer);
            } catch (Exception e) {
                LOGGER.error("Could not replace " + toReplace, e);
                return c;
            }

        } else {

            // Mutate external interaction parameter

            Interaction interaction = interactions.get(index - nodes.size());
            t.getGRNTree().removeInteraction(interaction);
            t.getGRNTree().addInteraction(perturb(interaction));
        }


        return t;
    }

    private boolean canPeturb(Interaction interaction) {
        return interaction.getParameters().stream().anyMatch(p -> types.contains(p.getParameterType()));
    }

    private GRNTreeNode perturb(GRNTreeNode node) {

        LeafNode ln = (LeafNode) node;

        Part part = node.getSVP();

        List<Interaction> internalInteractions = ln.getInternalEvents();

        // remove peturbable interactions from list

        List<Interaction> perturbable = internalInteractions.stream().filter(this::canPeturb).collect(Collectors.toList());
        internalInteractions.removeAll(perturbable);

        if(perturbable.isEmpty()) {
            LOGGER.error("No peturbable parameters in {}", node.getName());
            return node;
        }

        // peturb a single interaction and return all to list
        Interaction toPeturb = perturbable.get(new Random().nextInt(perturbable.size()));
        perturbable.remove(toPeturb);
        perturbable.add(perturb(toPeturb));

        internalInteractions.addAll(perturbable);

        return GRNTreeNodeFactory.getLeafNode(part, internalInteractions, node.getInterfaceType());
    }

    private Interaction perturb(Interaction interaction) {
            List<Parameter> c = interaction.getParameters();
            List<Parameter> toPeturbList = c.stream().filter(parameter -> types.contains(parameter.getParameterType())).collect(Collectors.toList());
            if(toPeturbList.isEmpty())
                return interaction;
            Parameter toPeturb = toPeturbList.get(r.nextInt(toPeturbList.size()));
            c.remove(toPeturb);
            double newValue = peturb(toPeturb.getValue());
            c.add(new Parameter(toPeturb.getParameterType(), toPeturb.getName(), newValue, toPeturb.getEvidenceType(), toPeturb.getScope()));
            LOGGER.debug("Perturbing {} in {} | {} -> {}", toPeturb.getParameterType(), interaction.getName(), toPeturb.getValue(), newValue);
            Interaction perturbed  = new Interaction();
            perturbed.setName(interaction.getName());
            perturbed.setMathName(interaction.getMathName());
            perturbed.setIsReversible(interaction.getIsReversible());
            perturbed.setIsReaction(interaction.getIsReaction());
            perturbed.setInteractionType(interaction.getInteractionType());
            perturbed.setFreeTextMath(interaction.getFreeTextMath());
            perturbed.setDescription(interaction.getDescription());
            perturbed.setConstraints(interaction.getConstraints());
            perturbed.setParameters(c);
            perturbed.setPartDetails(interaction.getPartDetails());
            perturbed.setParts(interaction.getParts());
            perturbed.setIsInternal(interaction.getIsInternal());
            return perturbed;
    }



    private double peturb(double parameter) {
        double nextGaussian = Math.abs((new Random().nextGaussian()  * 0.5) + 1.0);
        return parameter * nextGaussian;
    }
}
