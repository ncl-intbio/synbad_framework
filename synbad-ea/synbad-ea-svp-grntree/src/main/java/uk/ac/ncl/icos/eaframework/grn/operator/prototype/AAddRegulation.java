package uk.ac.ncl.icos.eaframework.grn.operator.prototype;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.*;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;
import uk.ac.ncl.icos.svpmanager.AbstractSVPManager;
import uk.ac.ncl.icos.svpmanager.MolecularForm;
import uk.ac.ncl.icos.svpmanager.RegulationRole;
import uk.ac.ncl.icos.svpmanager.SVPManager;
import uk.ac.ncl.intbio.virtualparts.entity.Interaction;
import uk.ac.ncl.intbio.virtualparts.entity.InteractionPartDetail;
import uk.ac.ncl.intbio.virtualparts.entity.Part;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Adds regulation to a TU, and places the regulating TF in an existing TU, or in
 * a newly created TU under control of a constitutive promoter. Positive
 * or negative regulation is chosen, by choosing the relevant part. TFs for that part
 * identified, and valid locations for those two part determined.
 * The regulated part is added to a valid location, and an valid existing or new TU
 * chosen at random for the TF.
 */
public class AAddRegulation extends AbstractOperator<GRNTreeChromosome> {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(AAddRegulation.class);
    private final SVPManager m = SVPManager.getSVPManager();
    private final AbstractSVPManager SVP_MANAGER = AbstractSVPManager.getSVPManager();
    private final SVPHelper SVP_HELPER = SVPHelper.getSVPHelper();
    private final int        MAX_ATTEMPTS = 10;
    private Random           r = new Random();

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {

        GRNTreeChromosome t = null;
        
        try {
            
            boolean mutationOccured = false;
            int attempt = 0;

            // =================================================================
            // Perform mutation in a while loop. If any exceptional cases occur,
            // while loop is repeated until mutation is successful.
            // =================================================================
            
            while(!mutationOccured && attempt < MAX_ATTEMPTS) {

                boolean regulatedAdded = false;
                boolean regulatorAdded = false;
                boolean phosphorylationRequired = false;
                boolean phosphorylationAdded = true;

                // =================================================================
                // Each mutation attempt uses a clean copy of the solution
                // =================================================================

                t = c.duplicate();

                boolean POSITIVE_REGULATION = r.nextBoolean();
                boolean PHOSPHORYLATION_REQUIRED = r.nextBoolean();

                GRNTreeNode regulatedPart = POSITIVE_REGULATION ?
                        SVP_HELPER.getInducPromoter("Promoter_" + Math.abs(r.nextInt()), r.nextDouble()) :
                        SVP_HELPER.getOperator("Operator_" + Math.abs(r.nextInt()));

                GRNTreeNode regulatingPart = PHOSPHORYLATION_REQUIRED ?
                        SVP_HELPER.getCDSWithPhosphorylated("CDS_" + Math.abs(r.nextInt()), r.nextDouble(), r.nextDouble(), r.nextDouble()) :
                        SVP_HELPER.getCDS("CDS_" + Math.abs(r.nextInt()), r.nextDouble());

                // =================================================================
                // All locations should currently be valid, as we are not using
                // modularity features of GRNTree
                // TODO replace these checks with something more efficient
                // =================================================================

                List<BranchNode> potentialRegulatableTUs = GRNTreeHelper.getBranchNodes(t.getGRNTree());
                List<BranchNode> potentialRegulatingTUs = potentialRegulatableTUs;
                List<GRNTreeNode> potentialPhosphorylatingTU = null;

                if (!potentialRegulatableTUs.isEmpty() && !potentialRegulatingTUs.isEmpty()) {

                    int regulatedIndex = r.nextInt(potentialRegulatableTUs.size());

                    // =================================================================
                    // Currently, the mathematical definition of SVPs requires positive
                    // regulation to be implemented using Inducible Promoters, and negative
                    // regulation to be implemented using Operators.
                    // =================================================================

                    if (POSITIVE_REGULATION) {

                        boolean REPLACE_EXISTING_PROMOTER = r.nextBoolean();

                        if (REPLACE_EXISTING_PROMOTER) {

                            // ========================================================
                            // Choose a random promoter in the TU, and replace....
                            // ========================================================

                            List<GRNTreeNode> promoters = new ArrayList<>();

                            for (GRNTreeNode node : potentialRegulatableTUs.get(regulatedIndex).getChildren()) {
                                if (((LeafNode) node).getType().equals(SVPType.Prom)) {
                                    promoters.add(node);
                                }
                            }

                            int index = potentialRegulatableTUs.get(regulatedIndex)
                                    .getChildren()
                                    .indexOf(promoters.get(r.nextInt(promoters.size())));

                            potentialRegulatableTUs.get(regulatedIndex)
                                    .getChildren()
                                    .get(index)
                                    .replaceNode(regulatedPart);
                            regulatedAdded = true;

                        } else {

                            // ========================================================
                            // ... or insert promoter at beginning of TU
                            // ========================================================
                            potentialRegulatableTUs.get(regulatedIndex).addNode(0, regulatedPart);
                            regulatedAdded = true;
                        }

                    } else  {

                        // ========================================================
                        // Choose a random promoter in the TU, and append Operator
                        // TODO operator should only be appended to const promoter
                        // ========================================================

                        List<GRNTreeNode> promoters = new ArrayList<>();

                        for (GRNTreeNode node : potentialRegulatableTUs.get(regulatedIndex).getChildren()) {
                            if (((LeafNode) node).getType().equals(SVPType.Prom)) {
                                promoters.add(node);
                            }
                        }

                        int index = potentialRegulatableTUs.get(regulatedIndex)
                                .getChildren()
                                .indexOf(promoters.get(r.nextInt(promoters.size())));

                        try {
                            potentialRegulatableTUs.get(regulatedIndex).addNode(index + 1, regulatedPart);
                        } catch (Exception e) {
                            return c;
                        }
                        regulatedAdded = true;
                    }



                    Interaction interaction = SVP_MANAGER.getRegulationInteraction(regulatedPart.getSVP(), regulatingPart.getSVP(),
                            PHOSPHORYLATION_REQUIRED ? MolecularForm.PHOSPHORYLATED : MolecularForm.DEFAULT,
                            POSITIVE_REGULATION ? RegulationRole.ACTIVATOR : RegulationRole.REPRESSOR, r.nextDouble());

                    // =================================================================
                    // The probability of adding the TF to an existing TU, or to a new
                    // TU is equally weighted (i.e. 1 / ( # of potentialTUs + 1 ) )
                    // =================================================================

                    // TODO If already present, add with 50% probability
                    // =================================================================

                    regulatorAdded = addPartToModel(regulatingPart, t);

                    // =================================================================
                    // Determine whether phosphorylation is required for regulation
                    // =================================================================

                    t.addInteraction(interaction);

                    if (PHOSPHORYLATION_REQUIRED) {

                        phosphorylationRequired = true;

                        // =================================================================
                        // TODO If already present, add with 50% probability
                        // =================================================================

                        GRNTreeNode donor = SVP_HELPER.getCDSEnvConst(
                                "CDS_" + Math.abs(r.nextInt()),
                                "SmallMolecule_" + Math.abs(r.nextInt()),
                                r.nextDouble(),
                                r.nextDouble(),
                                r.nextDouble(),
                                r.nextDouble());

                        if (!addPartToModel(donor, t)) {
                            phosphorylationAdded = false;
                        }

                        t.addInteraction(SVP_MANAGER.getPhosphorylationInteraction(donor.getSVP(), regulatingPart.getSVP(), r.nextDouble()));

                    }
                }

                if ((regulatedAdded && regulatorAdded && phosphorylationRequired && phosphorylationAdded) ||
                        (regulatedAdded && regulatorAdded && !phosphorylationRequired)) {
                    mutationOccured = true;
                }

                attempt++;

                if (!mutationOccured && attempt == MAX_ATTEMPTS) {
                    return c;
                }
            }
            } catch (Exception ex) {

            // ======================================================
            // In the case of an exception being thrown, the original
            // chromosome is returned, without mutation
            // ======================================================

            LOGGER.error(ex.getMessage());
            return c;
        }  
        
        return t;
    }

    /**
     * Adds part to either an existing or new TU. The probability of adding the part to an existing TU, or to a new TU
     * is equally weighted (i.e. 1 / ( no. of potentialTUs + 1 ) )
     * @param part
     * @param t
     * @return
     */
    private boolean addPartToModel(GRNTreeNode part, GRNTreeChromosome t) {

        try {

            List<BranchNode> potentialTUs = GRNTreeHelper.getBranchNodes(t.getGRNTree());
            int chosenIndex = r.nextInt( potentialTUs.size() + 1 );
            if(chosenIndex == potentialTUs.size()) {

                List<GRNTreeNode> partsList = new ArrayList<GRNTreeNode>();
                partsList.add(SVP_HELPER.getConstPromoter("Promoter_" + Math.abs(r.nextInt()), r.nextDouble()));
                partsList.add(SVP_HELPER.getRBS("RBS_" + Math.abs(r.nextInt()), r.nextDouble()));
                partsList.add(part);
               // partsList.add(SVP_HELPER.getTerminator("Terminator_" + Math.abs(r.nextInt())));
                partsList.add(GRNTreeNodeFactory.getLeafNode(m.getTerminator(), InterfaceType.NONE));
                potentialTUs.get(chosenIndex-1)
                        .getParent()
                        .addNode(GRNTreeNodeFactory.getBranchNode(InterfaceType.NONE, partsList));

                return true;

            }  else {

                GRNTreeNode tu = potentialTUs.get(chosenIndex);
                int additionIndex = tu.getChildren().size() - 1;
                tu.addNode(additionIndex, SVP_HELPER.getRBS("RBS_" + Math.abs(r.nextInt()), r.nextDouble()));
                additionIndex++;
                tu.addNode(additionIndex, part);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String toString() {
        return "Add Regulation";
    }
}
