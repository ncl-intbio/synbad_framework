package uk.ac.ncl.icos.eaframework.experiments;

import uk.ac.ncl.icos.eaframework.grn.constraint.*;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.stats.PopulationStatGenerator;
import uk.ac.ncl.icos.eaframework.terminationconditions.FitnessMaximisation;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.ConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.constraints.LowestConstraintHandler;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.constraints.SummationHandler;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.ASbmlConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.observer.SVPTreeResultsFileWriter;
import uk.ac.ncl.icos.eaframework.grn.observer.SvpHibernateObserver;
import uk.ac.ncl.icos.eaframework.PopulationProcess;
import uk.ac.ncl.icos.eaframework.population.SelectProcess;
import uk.ac.ncl.icos.eaframework.Strategy;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.observer.SimplePopObserver;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.PopulationIteratorSelection;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.population.SelectAndMutateProcess;
import uk.ac.ncl.icos.eaframework.selection.SigmaScaledRouletteWheel;
import uk.ac.ncl.icos.eaframework.selection.Tournament;
import uk.ac.ncl.icos.eaframework.selection.Truncation;
import uk.ac.ncl.icos.eaframework.selection.Uniform;
import uk.ac.ncl.icos.hibernate.HOperator;
import uk.ac.ncl.icos.hibernate.HExperiment;
import uk.ac.ncl.icos.hibernate.HibernateUtil;
import uk.ac.ncl.icos.hibernate.HPopulation;
import uk.ac.ncl.icos.hibernate.HSelectionProcess;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;

/**
 * Main class for the CIBCB experiments. Uses a Mutant Champ strategy to evolve the SpaR-SpaK system to produce a
 * linear response to Subtilin, as determined by the concentration of GFP expressed by a PspaS promoter.
 *
 * @author owengilfellon
 */
public class NewEvoTests {
    
    static private final Logger LOGGER = LoggerFactory.getLogger(NewEvoTests.class);
    
    static String NAME_PREFIX = "Experiment_";
    final Boolean restart;
    
    // ALGORITHM
    
    static final int MAX_GENERATIONS = 1000;
    static  int MUTATIONS_PER_GENERATION = 1;
    static final String INDEPENDENT_METABOLITE = "Subtilin";
    static final String DEPENDENT_METABOLITE = "GFP_rrnb";
    
    // POPULATIONS AND SELECTION
    
    static final int THREAD_POOL_SIZE = 30;
    static int CHILD_POPULATION_SIZE = 30;
    static int SURVIVAL_POPULATION_SIZE = 10;
    
    static int TRUNCATION_SIZE = 5;
    static int TOURNAMENT_SIZE = 5;
    
    // Summation handler seems to have more difficulty in finding and
    // keeping PspaS duplications
    static ConstraintHandler constraintHandler = new MultiplyHandler();
    
    // CONSTRAINT
    
    // With base inducement
    
    static final double TARGET_RATIO = 0.14918591815273405;
    static final double TARGET_LOWER = 34.59458302;
    static final double TARGET_UPPER = 21428.5714;
    static final double STARTING_CONCENTRATION = 1089.689752370406;
    
    // Without
    /*
    static final double TARGET_RATIO = 0.14918591815273405;
    static final double TARGET_LOWER = 0.0;
    static final double TARGET_UPPER = 21428.5714;
    */
    static int MAX_PARTS = 30;
    static int MAX_UNPENALISEDPARTS = 10;
    static final int MAX_TUS = 3;

    // RESPONSE CURVE

    static final double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    static final int INCREMENT = 50;
    
    // COPASI 
    
    static final int DURATION = 21600;
    static final int STEPS = 2160;
    
    public NewEvoTests(boolean restart) {
        this.restart = restart;
    }
    
    public static List<HExperiment> getExperiments() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction t = session.beginTransaction();
        EntityManager m = session.getEntityManagerFactory().createEntityManager();
        TypedQuery<HExperiment> query = m.createQuery("SELECT e FROM MExperiment e WHERE e.name LIKE '" + NAME_PREFIX +  "%'", HExperiment.class);
        List<HExperiment> exp = null;
        try {
            exp = query.getResultList();
        } catch( NoResultException e ) {
            exp = new ArrayList<>();
        }
        m.close();
        t.commit();
        session.close();
        return exp;
    }

    public static List<HExperiment> getUnfinishedExperiments(List<HExperiment> experiments) {
        return experiments.stream()
            .filter(e -> {
                int currentGeneration = e.getPopulations().get(e.getPopulations().size()-1).getGeneration();
                return e.getMaxGenerations() > currentGeneration;})
            .collect(Collectors.toList());
    }
    
    public static List<Operator<GRNTreeChromosome>> getOperators(HExperiment experiment, HSelectionProcess process) {
       
        List<Operator<GRNTreeChromosome>> operators = new ArrayList<>();
            for(HOperator operator : process.getOperators()) {
                try {
                    Class operatorClass = Class.forName("uk.ac.ncl.icos.eaframework.grn.operator." + operator.getOperatorName());
                    Constructor constructor = operatorClass.getConstructor();
                    operators.add((Operator<GRNTreeChromosome>) constructor.newInstance());
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage());
                }
            
            }
            return operators;
    }
    
    public static Strategy getStrategy(HExperiment experiment,  ASbmlConstraintEvaluator f) {               
     
        if(experiment == null) return null;
        
        // TO DO: Load population processes from DB
        
         Strategy<GRNTreeChromosome> strategy = new Strategy.Overlapping<>(
                getSelectionProcess(experiment, experiment.getReproductionProcess(), f), 
                getSelectionProcess(experiment, experiment.getSurvivalProcess(), f));
         
         return strategy;
    }
    
    
     public static PopulationProcess<GRNTreeChromosome> getSelectionProcess(HExperiment experiment, HSelectionProcess selection, ASbmlConstraintEvaluator f) {

        if(selection.getStrategyName().equals("SigmaScaledRouletteWheel")) {
            List<Operator<GRNTreeChromosome>> operators = new ArrayList<>();
            
            
            
            for(HOperator operator : selection.getOperators()) {
                try {
                    Class operatorClass = Class.forName("uk.ac.ncl.icos.eaframework.grn.operator." + operator.getOperatorName());
                    Constructor constructor = operatorClass.getConstructor();
                    operators.add((Operator<GRNTreeChromosome>) constructor.newInstance());
                } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    LOGGER.error(ex.getMessage());
                }
            }

            
            Operator<GRNTreeChromosome> o = selection.getWeights() != null ?
                    new OperatorGroup(operators, GRNTreeChromosome.class, MUTATIONS_PER_GENERATION, selection.getWeights().getWeights(), selection.getWeights().isSigmaScaled()) :
                    new OperatorGroup(operators, GRNTreeChromosome.class, MUTATIONS_PER_GENERATION);
            return new SelectAndMutateProcess<>(new SigmaScaledRouletteWheel<>(true), o, f, new EvaluatedSvpChromFactory(), selection.getPopulationSize(), MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
        }
        
        if(selection.getStrategyName().equals("Uniform"))
           return new SelectProcess<>(new Uniform(), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("Truncation"))
           return new SelectProcess<>(new Truncation<>(TRUNCATION_SIZE), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("Tournament"))
            return new SelectProcess<>(new Tournament<>(), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("FitnessProportionalTournamnet"))
            return new SelectProcess<>( new FitnessProportionalTournament<>(TOURNAMENT_SIZE), selection.getPopulationSize());
        
        if(selection.getStrategyName().equals("PopulationIteratorSelection")) {
            LOGGER.debug("Found PopulationIterator!");
            return new SelectProcess<>( new PopulationIteratorSelection<>(), selection.getPopulationSize());
        }
            
        return null;
    }
    
    
    public void runExperiment() {
        
    }


    public static void main(String[] args)
    {  
        
        ClassLoader cl = ClassLoader.getSystemClassLoader();

        URL[] urls = ((URLClassLoader)cl).getURLs();

        for(URL url: urls){
        	System.out.println(url.getFile());
        }

        boolean restart = false;

        for (String arg : args) {
            if(arg.equals("-r"))
                restart = true;
            else if (arg.startsWith("-pr")) {
                
                String pr = arg.trim().substring(3, arg.trim().length());
                try {
                    Integer prInt = Integer.parseInt(pr);
                    if(prInt > 0) {
                        LOGGER.debug("Setting Child Population Size: " + prInt);
                        CHILD_POPULATION_SIZE = prInt;
                    }  
                    else
                        LOGGER.error("-pr ("+ prInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -pr, using default");
                }
            } else if (arg.startsWith("-ps")) {
                String ps = arg.trim().substring(3, arg.trim().length());
                try {
                    Integer psInt = Integer.parseInt(ps);
                    if(psInt > 0) {
                        LOGGER.debug("Setting Survival Population Size: " + psInt);
                        SURVIVAL_POPULATION_SIZE = psInt;
                    }
                        
                    else
                        LOGGER.debug("ps ("+ psInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -ps, using default");
                }
            } else if (arg.startsWith("-parts")) {
                String parts = arg.trim().substring(6, arg.trim().length());
                try {
                    Integer partsInt = Integer.parseInt(parts);
                    if(partsInt > 0) {
                        LOGGER.debug("Setting Max Parts: " + parts);
                        MAX_PARTS = partsInt;
                    }
                    else
                        LOGGER.error("-parts ("+ partsInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -parts, using default");
                }
            }
            else if (arg.startsWith("-uparts")) {
                String parts = arg.trim().substring(7, arg.trim().length());
                try {
                    Integer partsInt = Integer.parseInt(parts);
                    if(partsInt > 0) {
                        LOGGER.debug("Setting Max Unpenalised Parts: " + parts);
                        MAX_PARTS = partsInt;
                    }
                    else
                        LOGGER.error("-parts ("+ partsInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -uparts, using default");
                }
            }
            else if (arg.startsWith("-m")) {
                String mutations = arg.trim().substring(2, arg.trim().length());
                try {
                    Integer mutationsInt = Integer.parseInt(mutations);
                    if(mutationsInt > 0) {
                        LOGGER.debug("Setting # of mutations: " + mutations);
                        MUTATIONS_PER_GENERATION = mutationsInt;
                    }
                    else
                        LOGGER.error("-m ("+ mutationsInt + ") must be larger than 0");
                } catch (NumberFormatException e) {
                    LOGGER.error("Could not parse -m, using default");
                }
            }
            else if (arg.startsWith("-name")) {
                String name = arg.trim().substring(5, arg.trim().length());
                if(name != null && !name.isEmpty()) {
                    LOGGER.debug("Setting Name: " + name);
                    NAME_PREFIX = name;
                }  
                else
                    LOGGER.error("-name ("+ name + ") is not valid");
            } else if (arg.startsWith("-c")) {
                String constraint = arg.trim().substring(2, arg.trim().length());
                if(constraint != null && !constraint.isEmpty()) {
                    if(constraint.equalsIgnoreCase("product")) {
                        LOGGER.debug("Setting Constraint Handler: " + constraint);
                        constraintHandler = new MultiplyHandler();
                    } else if (constraint.equalsIgnoreCase("sum")) {
                        constraintHandler = new SummationHandler();
                    } else if (constraint.equalsIgnoreCase("lowest")) {
                        constraintHandler = new LowestConstraintHandler();
                    }                     
                }  
            }
        }
        
        
        //PropertyConfigurator.configure("log4j.properties");
       
        HExperiment experiment = restart 
                ? getUnfinishedExperiments(getExperiments()).stream().findFirst().orElse(null)
                : null;

        if(experiment!=null)
            LOGGER.debug("Restarting " + experiment.getName());
        
        String experimentName = experiment == null ?
                NAME_PREFIX + new SimpleDateFormat("yyMMdd_HHmmssSSS").format(new Date()) :
                experiment.getName();

        GRNTreeChromosome seed =new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
            "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter; "
                    + "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter"));
        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(Arrays.asList(seed), SURVIVAL_POPULATION_SIZE);

        TerminationCondition generationTermination = experiment == null ?
                new GenerationTermination(MAX_GENERATIONS) :
                new GenerationTermination(experiment.getMaxGenerations());

        TerminationCondition t = new TerminationGroup(Arrays.asList(
                generationTermination,
                new FitnessMaximisation(100.0)));
        
        SBSbmlSimulatorFactory<SBSbmlSimulator> simulatorFactory = (int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, "Subtilin", "GFP_rrnb");
            return cs;
        };

        GRNTreeCopasiConstraintEvaluator f = new GRNTreeCopasiConstraintEvaluator(
            simulatorFactory,
            constraintHandler,
            Arrays.asList(
                    new LinearityConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE),
                    new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_LOWER, STARTING_CONCENTRATION),
                    new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_UPPER),
                    new RatioConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, TARGET_RATIO),
                    new GradedModelSizeConstraint(MAX_UNPENALISEDPARTS, MAX_PARTS)),
            Collections.EMPTY_LIST,
            DURATION, STEPS        
        );

        Strategy<GRNTreeChromosome> strategy = null;//getStrategy(experiment, f);
        
        if(strategy == null) {
            List<Operator<GRNTreeChromosome>> operatorList = 
            Arrays.asList(
                new RandomiseRBS(),
                new AddRegulation(),
                new DuplicateParts(),
                new DuplicateTU(),
                new RandomiseConstPromoter(),
                new RemoveRegulation(),
                new SplitTU(),
                new SwapParts(),
                new RemoveParts(),
                // New Operators

                new AddPromoter(),
                new MergeTU(),
                new RemovePromoter(),
                new RemoveTU());
            /*
            double[] weights= {
                1,      //RandomiseRBS
                0.1,    //AddRegulation
                0.1,    //DuplicateParts
                0.1,    //DuplicateTU
                1,      //RandomiseConstPromoter
                0.1,    //RemoveRegulation
                0.1,    //SplitTU
                0.1,    //SwapParts
                0.1,    //RemoveParts
                0.1,    //AddPromoter
                0.1,    //MergeTU
                0.1,    //RemovePromoter
                0.1 };  //RemoveTU
            */
            
            
            double[] weights= {
                1,      //RandomiseRBS
                0.3,    //AddRegulation
                0.1,    //DuplicateParts
                0.1,    //DuplicateTU
                1,      //RandomiseConstPromoter
                0.1,    //RemoveRegulation
                0.1,    //SplitTU
                0.1,    //SwapParts
                0.1,    //RemoveParts
                0.3,    //AddPromoter
                0.1,    //MergeTU
                0.1,    //RemovePromoter
                0.1 };  //RemoveTU
            
            Operator<GRNTreeChromosome> o = new OperatorGroup(operatorList, GRNTreeChromosome.class, MUTATIONS_PER_GENERATION, weights, false);

            Selection<GRNTreeChromosome> s1 = new SigmaScaledRouletteWheel<>(true); 
           // Selection<GRNTreeChromosome> s1 = new Uniform<>();
            //Selection<GRNTreeChromosome> s2 = new Truncation<>(1);
            Selection<GRNTreeChromosome> s2 = new FitnessProportionalTournament<>(true, TOURNAMENT_SIZE);
            //Selection<GRNTreeChromosome> s2 = new Truncation<>(TRUNCATION_SIZE);
                
            PopulationProcess<GRNTreeChromosome> reproduction = new SelectAndMutateProcess<>(s1, o, f, new EvaluatedSvpChromFactory(), CHILD_POPULATION_SIZE, MUTATIONS_PER_GENERATION, THREAD_POOL_SIZE);
            //PopulationProcess<GRNTreeChromosome> survival = new SelectProcess<>(new Truncation<>(TRUNCATION_SIZE), SURVIVAL_POPULATION_SIZE);
            //PopulationProcess<GRNTreeChromosome> survival = new SelectProcess<>(new Tournament<>(), SURVIVAL_POPULATION_SIZE);
            PopulationProcess<GRNTreeChromosome> survival  = new SelectProcess<>( s2, SURVIVAL_POPULATION_SIZE);
            
            strategy = new Strategy.Overlapping<>(
                reproduction, 
                survival);  
        }
        

        /*
        Strategy<GRNTreeChromosome> strategy = new Strategy.NonOverlapping<>(
            sigmaSelectAndMutate, truncationSurvival);*/

        EvoEngine<GRNTreeChromosome> grnTreeEngine ;
                
        if(experiment == null) 
            grnTreeEngine = new GRNTreeEngine( SURVIVAL_POPULATION_SIZE, cf, strategy, f, t);
        else {
            List<HPopulation> populations = experiment.getPopulations();
            HPopulation lastPopulation = populations.get(populations.size()-1);
            List<EvaluatedChromosome<GRNTreeChromosome>> chromosomes = (List<EvaluatedChromosome<GRNTreeChromosome>>)lastPopulation.getPopulation().stream()
                               .map(ev -> new EvaluatedSvpChromosome<>(
                                       new GRNTreeChromosome(GRNTreeFactory.getGRNTree(ev.getSvpwrite())),
                                       new Fitness(ev.getFitness().getFitness()),
                                       ev.getFitness().getResult().stream().map(r-> new CResult(r.getName(), r.getEvaluation())).collect(Collectors.toList()),
                                      // Collections.EMPTY_LIST,
                                       null)).map(ev -> (EvaluatedChromosome<GRNTreeChromosome>) ev).collect(Collectors.toList());
            grnTreeEngine = new GRNTreeEngine( lastPopulation.getGeneration(), 
                    chromosomes,
                    cf, strategy, f, t, new PopulationStatGenerator.IncreasingFitnessStatGenerator(),THREAD_POOL_SIZE);
        }

        EvolutionObserver<GRNTreeChromosome> writer = new SVPTreeResultsFileWriter(experimentName, Arrays.asList("GFP_rrnb"));
        grnTreeEngine.attach(new SimplePopObserver());
        grnTreeEngine.attach(writer);
        grnTreeEngine.attach(new SvpHibernateObserver(experimentName, 
                INDEPENDENT_METABOLITE, 
                DEPENDENT_METABOLITE, 
                INCREMENT, 
                MAX_GENERATIONS, 
                CHILD_POPULATION_SIZE, 
                SURVIVAL_POPULATION_SIZE));
        grnTreeEngine.run(); 
    }

}
