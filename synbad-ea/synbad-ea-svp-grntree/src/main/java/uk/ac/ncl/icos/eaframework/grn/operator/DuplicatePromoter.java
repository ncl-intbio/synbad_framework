package uk.ac.ncl.icos.eaframework.grn.operator;

import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.eaframework.operator.AbstractOperator;
import uk.ac.ncl.icos.grntree.api.GRNTree;
import uk.ac.ncl.icos.grntree.api.GRNTreeHelper;
import uk.ac.ncl.icos.grntree.api.GRNTreeNode;
import uk.ac.ncl.icos.grntree.impl.BranchNode;
import uk.ac.ncl.icos.grntree.impl.InterfaceType;
import uk.ac.ncl.icos.grntree.impl.LeafNode;
import uk.ac.ncl.icos.grntree.impl.SVPType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.grntree.api.GRNTreeNodeFactory;

/**
 * Randomly duplicates a Promoter, and associated Operators, and places at the beginning
 * of a randomly chosen TU
 * @author owengilfellon
 */
public class DuplicatePromoter extends AbstractOperator<GRNTreeChromosome> {

    private static final Logger logger = LoggerFactory.getLogger(DuplicatePromoter.class);
    final private int        MAX_ATTEMPTS = 10;

    @Override
    public GRNTreeChromosome apply(GRNTreeChromosome c) {
        
        GRNTreeChromosome t = (GRNTreeChromosome) c.duplicate();       
        List<GRNTreeNode> allTranscriptionalUnits = new ArrayList<GRNTreeNode>();
        Iterator<GRNTreeNode> it = t.getBreadthFirstIterator();

        // Retrieve all TUs from model by iterating through nodes and testing
        
        while( it.hasNext() ) {
            GRNTreeNode n = it.next();
            if(n.isTranscriptionUnit()) {
                allTranscriptionalUnits.add(n);
            }
        }
        
        // If no TUs, then mutation cannot be performed. Return t.
        
        if( allTranscriptionalUnits.isEmpty() ){
            return c;
        }
            
        Random r = new Random();
        boolean mutationOccured = false;
        int attempt = 0;

        while( !mutationOccured && attempt < MAX_ATTEMPTS ) {

            GRNTreeNode source = allTranscriptionalUnits.get(r.nextInt(allTranscriptionalUnits.size()));
            List<GRNTreeNode> potentialDuplicates = new ArrayList<GRNTreeNode>();

            for( GRNTreeNode n:source.getChildren() ){
                if( n instanceof LeafNode) {
                    LeafNode ln = (LeafNode) n;
                    if( ln.getType() == SVPType.Prom ){
                        potentialDuplicates.add(n);
                    }
                }
            }

            if( !potentialDuplicates.isEmpty() ){

                // Identify a promoter to duplicate, and any following operators

                LeafNode promoter = (LeafNode) potentialDuplicates.get(r.nextInt(potentialDuplicates.size()));
                List<LeafNode> operators = new ArrayList<LeafNode>();
                int increment = 1;
                int promoterIndex = promoter.getParent().getChildren().indexOf(promoter);

                while(((LeafNode)promoter.getParent()
                                         .getChildren()
                                         .get(promoterIndex + increment))
                                         .getType().equals(SVPType.Op)) {

                    operators.add((LeafNode) promoter.getParent()
                                                     .getChildren()
                                                     .get(promoterIndex + increment++));
                }


                // ==============================================================
                // TODO All destinations should currently be valid - change this?
                // ==============================================================


                try {

                    List<BranchNode> validDestinations = GRNTreeHelper.getBranchNodes(t.getGRNTree());

                    if( !validDestinations.isEmpty() ) {

                        GRNTreeNode destination = validDestinations.get(r.nextInt(validDestinations.size()));
                        GRNTreeNode dupProm = promoter.duplicate();
//                        promoter.getSVP().getStatus() == null || !promoter.getSVP().getStatus().equals("Prototype") ?
//                                GRNTreeNodeFactory.getLeafNode(promoter.getSVP(), InterfaceType.NONE) :
//                                GRNTreeNodeFactory.getLeafNode(promoter.getSVP(), promoter.getInternalEvents(), InterfaceType.NONE);

                        destination.addNode( 0, dupProm);
                        int operatorIndex = 1;

                        for( LeafNode operator : operators ) {

                            GRNTreeNode dupOp = operator.duplicate();
                                    //operator.getSVP().getStatus() == null || !operator.getSVP().getStatus().equals("Prototype") ?
//                                    GRNTreeNodeFactory.getLeafNode(operator.getSVP(), InterfaceType.NONE) :
//                                    GRNTreeNodeFactory.getLeafNode(operator.getSVP(), operator.getInternalEvents(), InterfaceType.NONE);

                            destination.addNode( operatorIndex++, dupOp);
                        }

                        mutationOccured = true;

                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                    return c;
                }

            }

            attempt++;
        }
        return t;
    }
}
