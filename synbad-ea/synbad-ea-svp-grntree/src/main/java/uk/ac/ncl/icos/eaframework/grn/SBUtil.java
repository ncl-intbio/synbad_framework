/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;

/**
 *
 * @author owengilfellon
 */
public interface SBUtil {
    
    
    public static boolean exportResults(String title,
                                        String resultsDirectory,
                                        String experimentGroup,
                                        String experimentDirectory,
                                        String filename,
                                        String columnLabels,
                                        ResponseCurve results,
                                        boolean append) {

        String s = "";

        // Add column titles

        if(title!=null)
        {
            s += title + "\n";
        }

        
        if(columnLabels!=null)
        {
            for (int i = 0; i < results.getOutputs().size(); i++) {

                if (i == (results.getOutputs().size() - 1)) {
                    s += columnLabels + "_" + (i + 1) + "\n";
                } else {
                    s += columnLabels + "_" + (i + 1) + " ";
                }
            }
        }

        for (int i = 0; i < results.getOutputs().size(); i++) {
                if (i == (results.getOutputs().size() - 1)) {
                    s += results.getOutputs().get(i).doubleValue() + "\n";
                } else {
                    s += results.getOutputs().get(i).doubleValue() + " ";
                }
            }

        try {

             final File homeDir = new File(System.getProperty("user.home"));
            File directory = new File(homeDir + "/" +  resultsDirectory + "/" + experimentGroup + "/" + experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdirs()) {
                 //     logger.error("Couldn't create directory: {}", resultsDirectory);
                };
            }
            
            /*
            File exDirectory = new File(directory,  experimentDirectory);
            if (!directory.exists()) {
                if (!directory.mkdir()) {
                      System.out.println("Couldn't create directory: " + resultsDirectory);
                };
            }
            */
       

            File file = new File(directory + "/" + filename);

            BufferedWriter writer = new BufferedWriter(new FileWriter(file, append));
            writer.write(s);
            writer.close();
        } catch (Exception e) {
            return false;
        }

        return true;

    }
}
