/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.annotation.EAModule;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.grntree.io.GRNCompilableFactory;
import uk.ac.ncl.icos.svpcompiler.Compiler.CompilationDirector;
import uk.ac.ncl.icos.svpcompiler.parsing.SVPParser;
import uk.ac.ncl.icos.svpmanager.SVPManager;

import java.util.List;


/**
 * Fitness function that penalises biologically implausible concentrations,
 * and decreases in input-output ratios.
 * @author owengilfellon
 */

@EAModule(visualName = "Linearity - Part Penalties")
public class GRNLinearityWithPartPenalties extends GRNSimulationEvaluator<GRNTreeChromosome> {

    /**
     * To DO:
     *
     * Add weights to the various penalties to allow customisation
     * Also, look into methods used for multi-objective fitness functions.
     */
    
    private final double OUTPUT_LOWER_BOUND;
    private final double OUTPUT_UPPER_BOUND;
    private final ParameterScheduler MAX_UNPENALIZED_PARTS;
    private final double PENALTY_WEIGHT;
    private final double SIZE_PENALTY_WEIGHT = 10;


    public GRNLinearityWithPartPenalties(SBSbmlSimulatorFactory<DynamicTimeCopasiSimulator> cs,
                                         double outputLowerBound,
                                         double outputUpperBound,
                                         ParameterScheduler unpenalizedParts,
                                         double penaltyWeight,
                                         int duration, int runtime)
    {
        super(cs, duration, runtime);
        this.OUTPUT_LOWER_BOUND = outputLowerBound;
        this.OUTPUT_UPPER_BOUND = outputUpperBound;
        this.MAX_UNPENALIZED_PARTS = unpenalizedParts;
        this.PENALTY_WEIGHT = penaltyWeight;
    }
    
    @Override
    public Fitness evaluate(GRNTreeChromosome c)
    {
        
        double targetRatio = 0.0;
        double upperBoundPenalty;
        double lowerBoundPenalty;
        double partsSizePenalty;
        double tusSizePenalty = 0.0;
        double observedRatio;
        double ratioPenalty;
        double correlation;
        double finalFitness;

        GRNTreeChromosome g = (GRNTreeChromosome) c;
        DynamicTimeCopasiSimulator ds = (DynamicTimeCopasiSimulator) cs.getSimulator(getDuration(), getRuntime());
        Exporter<String> exporter = new SVPWriteExporter();
        try {
            CompilationDirector d = new CompilationDirector(SVPManager.getRepositoryUrl());
            d.setModelCompilables(GRNCompilableFactory.getCompilables(g.getGRNTree()));
            ds.setModel(d.getSBMLString("model"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Fitness(0.0);
        }

        if(ds.run())
        {
            List<TimeCourseTrace> dependentTraces = ds.getDependentMetaboliteValues();
            TimeCourseTrace independentTraces = ds.getIndependentMetaboliteValues();
            
            double[] responseCurve = new double[dependentTraces.size()];
            double[] independentsRun = new double[independentTraces.size()];

            for(int i=0; i < dependentTraces.size(); i++)
            {
                // for each dependent trace, get last value and add to response curve
                
                responseCurve[i] = dependentTraces.get(i).get(dependentTraces.get(i).size() - 1);
                independentsRun[i] = independentTraces.get(i);
            }

            /* ==================================================================================

              Linearity calculated using Pearson's Correlation, disregarding negative correlation

               ================================================================================== */

            PearsonsCorrelation pc = new PearsonsCorrelation();
            correlation = pc.correlation(independentsRun, responseCurve);

            correlation = correlation > 0.0 ? correlation * 100 : 0.0;
   

            /* ==================================================================================

              Penalties for infeasible and undesirable output concentrations

               ================================================================================== */
            
            // The penalty for the upper bound penalises values close to OUTPUT_UPPER_BOUND, so the aim is to minimise this value
            // The penalty for the lower bound penalises values deviating from OUTPUT_LOWER_BOUND, so the aim is to maximise this value
            
            double cappedDifference = Math.max(OUTPUT_UPPER_BOUND - responseCurve[responseCurve.length-1], 0.0);

            upperBoundPenalty = 1.0 - (1.0 / (( cappedDifference / PENALTY_WEIGHT) + 1.0));
            lowerBoundPenalty = (1.0 / ((( responseCurve[0] - (OUTPUT_LOWER_BOUND) ) / PENALTY_WEIGHT ) + 1.0 ));
            
            // The fitness is penalised when upperBoundPenalty > 0.0
            // The fitness is penalised when lowerBoundPenalty < 1.0
            
            double upperPenalisedFitness = correlation * upperBoundPenalty;
            double lowerPenalisedFitness = upperPenalisedFitness * lowerBoundPenalty;

            /* ==================================================================================

              Penalties for decreases in the ratio of input to output

               ================================================================================== */
            
            // The input-output observedRatio is calculated using the highest values of each

            observedRatio = responseCurve[responseCurve.length-1] / independentsRun[independentsRun.length-1] ;

            // If starting model input-output observedRatio is not recorded, do so now

            if(targetRatio == 0.0){
                targetRatio = observedRatio;
            }
            
            // The observed input-output observedRatio, relative to that of the initial model is used as a penalty

            double ratio = observedRatio / targetRatio;

            ratioPenalty = ratio >= 1.0 ? 1.0 : ratio;
            
            // Higher input-output ratios are allowed, lower ratios decrease fitness.
            
            double ratioFitness = lowerPenalisedFitness * ratioPenalty;

            /* ==================================================================================

              Penalties for model sizes

               ================================================================================== */

            int parts = g.getPartsSize();
            double maxParts = this.MAX_UNPENALIZED_PARTS.getParameter();

            partsSizePenalty = parts <= maxParts
                                  ? 1.0
                                  : 1.0 / ((((double)parts - maxParts) / SIZE_PENALTY_WEIGHT) + 1.0);

            /* ==================================================================================

              Calculate final fitness

               ================================================================================== */

            /*
            System.out.println("Linear: " +  correlation);
            System.out.println("Upper: " + upperBoundPenalty);
            System.out.println("Lower: " + lowerBoundPenalty);
            System.out.println("Ratio: " + ratioPenalty);
            System.out.println("PaPen: " + partsSizePenalty);
            System.out.println("Fit Penalty: " + (upperBoundPenalty * lowerBoundPenalty * ratioPenalty * partsSizePenalty));
           
            */
            finalFitness = correlation *
                    (upperBoundPenalty *
                     lowerBoundPenalty *
                     ratioPenalty *
                     partsSizePenalty);
           
            // Return the final fitness
            
            
            
            return new Fitness(finalFitness);
        }
        else
        {
            // If there was a problem with the simulation, return Fitness = 0.0
            
            return new Fitness(0.0);
        }           
    } 
}
