package uk.ac.ncl.icos.eaframework.experiments;

import uk.ac.ncl.icos.eaframework.grn.constraint.LowerBoundConstraint;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import org.apache.log4j.PropertyConfigurator;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.ChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.operator.*;
import uk.ac.ncl.icos.eaframework.grn.simulator.copasi.DynamicTimeCopasiSimulator;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.scheduler.ParameterScheduler;
import uk.ac.ncl.icos.eaframework.terminationconditions.FitnessMaximisation;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.TerminationCondition;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.constraint.LinearityConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.ModelSizeConstraint;
import uk.ac.ncl.icos.eaframework.grn.constraint.UpperBoundConstraint;
import uk.ac.ncl.icos.eaframework.constraints.MultiplyHandler;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.PopulationProcess;
import uk.ac.ncl.icos.eaframework.population.SelectProcess;
import uk.ac.ncl.icos.eaframework.Strategy;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromFactory;
import uk.ac.ncl.icos.eaframework.population.SelectAndMutateProcess;
import uk.ac.ncl.icos.eaframework.observer.SimplePopObserver;
import uk.ac.ncl.icos.eaframework.selection.FitnessProportionalTournament;
import uk.ac.ncl.icos.eaframework.selection.SigmaScaledRouletteWheel;
import uk.ac.ncl.icos.eaframework.selection.Tournament;
import uk.ac.ncl.icos.eaframework.selection.Truncation;

/**
 * Sain class for the CIBCB experiments. Uses a Mutant Champ strategy to evolve the SpaR-SpaK system to produce a
 * linear response to Subtilin, as determined by the concentration of GFP expressed by a PspaS promoter.
 *
 * @author owengilfellon
 */
public class TreeEvoTest {
    
    static final int MAX_GENERATIONS = 1000;
    static final int CHILD_POPULATION_SIZE = 30;
    static final int SURVIVAL_POPULATION_SIZE = 30;
    static final int TRUNCATION_SIZE = 30;
    static final int MAX_PARTS = 14;
    static final int MAX_TUS = 3;
    static final int MUTATIONS_PER_GENERATION = 1;

    static final double STARTING_CONCENTRATION = 1089.689752370406;

    static final double MIN_CHANGE = 0.0005; // i.e. change must be > 1 / MIN_CHANGE
    static final int INCREMENT = 50;
    
    
    static private final String INDEPENDENT_METABOLITE = "Subtilin";
    static private final String DEPENDENT_METABOLITE = "GFP_rrnb";

    public static void main(String[] args)
    {  

        // Add logger - primarily used to ignore SBML warnings

        PropertyConfigurator.configure("log4j.properties");

       // String old = System.getProperty("java.library.path");
        // System.out.println(old);
        //System.setProperty("java.library.path", "resources/copasi/:" + old);
        System.out.println(System.getProperty("java.library.path"));

        /*
            The algorithm is run using a the GRNTreeEngine extension to the EvoEngine class.
            It takes:
                a ChromosomeFactory, to generate the population from a seed population
                an Operator, to perform mutations on the population
                a Selection class, to select members of the population for reproduction
                an Evaluator (i.e. Fitness Function), that determines the fitness of individuals in the population
                a TerminationCondition, to determine when the algorithm should terminate

         */

        /*
            Create a ChromosomeFactory by passing seed models, and a population size
         */

        List<GRNTreeChromosome> seed = new ArrayList<>();
        seed.add(new GRNTreeChromosome(GRNTreeFactory.getGRNTree(
                "PspaRK:Prom; RBS_SpaR:RBS; SpaR:CDS; RBS_SpaK:RBS; SpaK:CDS; BO_4296:Ter;" +
                "PspaS:Prom; RBS_SpaS:RBS; GFP_rrnb:CDS; BO_4296:Ter")));
        ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(seed, SURVIVAL_POPULATION_SIZE);
        
        /*
         * Create an operator, in this case, an OperatorGroup which combines other mutation Operators.
         */

        List<Operator<GRNTreeChromosome>> operators = new ArrayList<>();
       
        
        operators.add(new RandomiseRBS());

        operators.add(new AddRegulation());
        operators.add(new DuplicateParts());
        operators.add(new DuplicateTU());
        operators.add(new RandomiseConstPromoter());

        operators.add(new RemoveRegulation());
        operators.add(new SplitTU());
        operators.add(new SwapParts());

        operators.add(new RemoveParts());
                
        // New Operators

        operators.add(new AddPromoter());
        operators.add(new MergeTU());
        operators.add(new RemovePromoter());
         /**/
        Operator<GRNTreeChromosome> o = new OperatorGroup(operators, GRNTreeChromosome.class, MUTATIONS_PER_GENERATION);

        /*
         * Create a termination condition, in this case, a termination group which evaluates multiple termination
         * conditions
         */
  
        List<TerminationCondition> tc = new ArrayList<>();
        tc.add(new GenerationTermination(MAX_GENERATIONS));
        tc.add(new FitnessMaximisation(100.0));
        TerminationCondition t = new TerminationGroup(tc);
        
        /*
         * Create a Simulator, specifying run length (in seconds) and duration (in steps), the number of different
         * metabolite values to simulate, and the range of metabolite concentrations. An independent and dependant
         * metabolite are specified (i.e. the metabolite to be varied, and the metabolite to be observed).
         */

        // 6 Hour Simulation
        
        /*
         * Create a Selection strategy - in this case, a Mutant Champ strategy.
         */

        /*
         * An evaluator is created, passing the simulator as an argument for the purposes of assessing the SBML models.
         * This fitness function takes additional parameters, for the purposes of specifying acceptable metabolite
         * concentrations and model sizes.
         */

        ParameterScheduler scheduler = new ParameterScheduler(1, 800, 40.0, 20.0);


        // Parameters of 20 Minute Simulation - 1200, 1200
        // Parameters of Original Simulation - 15000, 1500
        
        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = new ArrayList<>();
        constraints.add(new LinearityConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE));
        constraints.add(new LowerBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE,0, STARTING_CONCENTRATION));
        constraints.add(new UpperBoundConstraint(INDEPENDENT_METABOLITE, DEPENDENT_METABOLITE, 21428.5714));
        constraints.add(new ModelSizeConstraint(MAX_PARTS));
       
       
        GRNTreeCopasiConstraintEvaluator f = new GRNTreeCopasiConstraintEvaluator((int duration, int runtime) -> {
                DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime, INCREMENT, MIN_CHANGE, "Subtilin", "GFP_rrnb");
                return cs;
            }, new MultiplyHandler(),
            constraints,
            Collections.EMPTY_LIST,
            21600, 2160        
        );
        
 /*
        Evaluator<GRNTreeChromosome> f = new GRNLinearityWithPartPenalties((int duration, int runtime) -> {
            DynamicTimeCopasiSimulator cs = new DynamicTimeCopasiSimulator(duration, runtime);
            cs.setNumberOfMetaboliteIncrements(100);
            cs.setMaximumMetaboliteConcentration(10000);
            cs.setMetabolitePair("Subtilin", "GFP_rrnb");
            return cs;
        }, 0, 21428.5714, scheduler, 20, 21600, 2160);       /**/

        
        PopulationProcess<GRNTreeChromosome> sigmaSelectAndMutate = new SelectAndMutateProcess<>(
                        new SigmaScaledRouletteWheel<>(true), o, f, new EvaluatedSvpChromFactory(),
                        CHILD_POPULATION_SIZE, 
                        MUTATIONS_PER_GENERATION,
                        10);
        
        PopulationProcess<GRNTreeChromosome> truncationSurvival = new SelectProcess<>(
            new Truncation<>(TRUNCATION_SIZE), SURVIVAL_POPULATION_SIZE);
        
        PopulationProcess<GRNTreeChromosome> tournamentSurvival = new SelectProcess<>(
            new Tournament<GRNTreeChromosome>(), SURVIVAL_POPULATION_SIZE);
        
        PopulationProcess<GRNTreeChromosome> fitnessProportionaltournamentSurvival = new SelectProcess<>(
            new FitnessProportionalTournament<>(), SURVIVAL_POPULATION_SIZE);
        
        
        Strategy<GRNTreeChromosome> strategy = new Strategy.Overlapping<>(
                sigmaSelectAndMutate,
                fitnessProportionaltournamentSurvival);
        
        /*
         * Create the EA, passing the Chromosome, Operators, Fitness Evaluators and Termination Conditions.
         */
        
        EvoEngine<GRNTreeChromosome> e = new GRNTreeEngine(SURVIVAL_POPULATION_SIZE,  cf, strategy, f, t);
        
        /*
         * Create an SVPTreeObserver implementing EvolutionObserver. SVPTreeObserver takes a list of metabolite names,
         * the timecourses of which are retrieved from the COPASI simulations and recorded to disk.
         * The EvolutionObservers are attached to the EvoEngine.
         */
        
        ArrayList<String> toRecord = new ArrayList<>();
        toRecord.add("GFP_rrnb");
        //EvolutionObserver observer = new SVPSimpleTreeObserver();
        //EvolutionObserver writer = new SVPTreeResultsFileWriter("LinearityExperiments", toRecord);
        //EvolutionObserver debugger = new NodeManagerDebugger();

        //e.attach(observer);
        e.attach(new SimplePopObserver());
        //e.attach(writer);
        //e.attach(debugger);
        //e.attach((EvolutionObserver)scheduler);
        
        /*
         * The algorithm is run. The algorithm will continue until one of the termination conditions is met, and output
         * it's progress to console and to disk.
         */
        
        e.run(); 
    }

     
}
