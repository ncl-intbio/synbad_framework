/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.exporter;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNChromosomeFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngine;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeEngineBuilder;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.GRNTreeCopasiConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulator;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.grntree.api.GRNTreeFactory;
import uk.ac.ncl.icos.mongo.*;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.synbad.core.util.Config;

import javax.print.Doc;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Class.forName;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 *
 * @author owengilfellon
 */
public class SvpMongoImporter {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SvpMongoImporter.class);
    static final int MUTATIONS_PER_GENERATION = 1;
    static final int TOURNAMENT_SIZE = 1;
    static final int TRUNCATION_SIZE = 1;
    static final int DURATION = 12800;
    static final int RUN_TIME = 1280;


    private final String EXPERIMENTS_COL = "experiments";
    private final String CHROMOSOME_COL = "chromosomes";

    private final CodecRegistry pojoCodecRegistry;

    public SvpMongoImporter() {
        this.pojoCodecRegistry = fromRegistries(com.mongodb.MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    }

    public boolean exportExists(String MONGO_URL, String DB_NAME, String EXPERIMENT_NAME) {
        MongoClient mongoClient = MongoClients.create(MONGO_URL);
        MongoDatabase db = mongoClient.getDatabase(DB_NAME);

        // Create collections if they don't already exist

        if(!db.listCollectionNames().into(new ArrayList<>()).contains(EXPERIMENTS_COL))
            return false;

        MongoCollection<MExperiment> experimentCollection = db.getCollection(EXPERIMENTS_COL, MExperiment.class).withCodecRegistry(pojoCodecRegistry);

        FindIterable<MExperiment> experiment = experimentCollection.find(Filters.regex("name", ".*" + EXPERIMENT_NAME + ".*"), MExperiment.class);
        return experiment.cursor().hasNext();
    }

    public MExport getExport(String MONGO_URL, String DB_NAME, String EXPERIMENT_NAME) {

        MongoClient mongoClient = MongoClients.create(MONGO_URL);
        MongoDatabase db = mongoClient.getDatabase(DB_NAME);

        // Create collections if they don't already exist

        if(!db.listCollectionNames().into(new ArrayList<>()).contains(EXPERIMENTS_COL))
            db.createCollection(EXPERIMENTS_COL);

        if(!db.listCollectionNames().into(new ArrayList<>()).contains(CHROMOSOME_COL))
            db.createCollection(CHROMOSOME_COL);

        MongoCollection<MExperiment> experimentCollection = db.getCollection(EXPERIMENTS_COL, MExperiment.class).withCodecRegistry(pojoCodecRegistry);
        MongoCollection<MEvaluated> chromosomeCollection = db.getCollection(CHROMOSOME_COL, MEvaluated.class).withCodecRegistry(pojoCodecRegistry);

        FindIterable<MExperiment> experiment = experimentCollection.find(Filters.regex("name", ".*" + EXPERIMENT_NAME + ".*"), MExperiment.class);
        MExperiment e = experiment.cursor().next();
        List<MEvaluated> chromosomes = new LinkedList<>();
        MongoCursor<MEvaluated> c_it = chromosomeCollection.find(Filters.regex("experimentName", ".*" + EXPERIMENT_NAME + ".*")).cursor();
        while(c_it.hasNext()) {
            MEvaluated eva = c_it.next();
            chromosomes.add(eva);
        }

        MExport export = new MExport(chromosomes, e);
        mongoClient.close();

        return export;
    }

    public GRNTreeEngine getEngine(MExport export) {

        try{

            int THREADS = export.getExperiment().getThreads();
            int REPRODUCTION_SIZE = export.getExperiment().getReproductionProcess().getPopulationSize();
            int SURVIVAL_SIZE = export.getExperiment().getSurvivalProcess().getPopulationSize();
            boolean OVERLAPPING = export.getExperiment().getOverlapping();

            MExperiment experiment = export.getExperiment();
            List<MPopulation> populations = experiment.getPopulations();
            MPopulation lastPopulation = populations.get(populations.size()-1);

            final Map<ObjectId, MEvaluated> evaluated = export.getChromosomes().stream().distinct().collect(Collectors.toMap(MEvaluated::getChromosomeId, c -> c));

            List<EvaluatedChromosome<GRNTreeChromosome>> chromosomes = lastPopulation.getSurvivalPopulation().stream()
                    .map(evaluated::get)
                    .map(ev -> new EvaluatedSvpChromosome<>(
                                new GRNTreeChromosome(GRNTreeFactory.getGRNTree(ev.getSvpwrite()), ev.getParentId()),
                                new Fitness(ev.getFitness().getFitness()),
                                ev.getFitness().getResult().stream().map(r-> new CResult(r.getName(), r.getEvaluation())).collect(Collectors.toList()),
                                null))
                    .map(ev -> (EvaluatedChromosome<GRNTreeChromosome>) ev).collect(Collectors.toList());


            Evaluator<GRNTreeChromosome> evaluator = getEvaluator(experiment.getEvaluator());


            // TODO: Import chromosome factory - is this needed when restarting an experiment?

            // ChromosomeFactory<GRNTreeChromosome> cf = new GRNChromosomeFactory(Collections.EMPTY_LIST, lastPopulation.getPopulationSize());

            OperatorGroup reproductionOperators = getOperators(experiment, experiment.getReproductionProcess());
            Selection<GRNTreeChromosome> reproductionSelection = (Selection) getObjectFromDocument(experiment.getReproductionProcess().getSelectionConfig());

            OperatorGroup  survivalOperators = getOperators(experiment, experiment.getSurvivalProcess());
            Selection<GRNTreeChromosome> survivalSelection = (Selection) getObjectFromDocument(experiment.getSurvivalProcess().getSelectionConfig());

            GRNTreeEngineBuilder b = new GRNTreeEngineBuilder()
                    .setThreads(THREADS)
                    .setEvaluator(evaluator)
                    .setOverlapping(OVERLAPPING)
                    .setReproductionPopulationSize(REPRODUCTION_SIZE)
                    .setReproductionSelection(reproductionSelection)
                    .setSurvivalPopulation(SURVIVAL_SIZE)
                    .setSurvivalSelection(survivalSelection)
                    .setOrderedPopulations(export.getExperiment().getOrdered(), export.getExperiment().getMinimiseFitnessFunc())

                    // handle ordered, minimise fitness

                    .setCurrentGeneration(experiment.getPopulations().size())
                    .setCurrentPopulation(chromosomes);

            for(TerminationCondition terminationCondition : getTerminationConditions(experiment)) {
                b = b.addTerminationCondition(terminationCondition);
            }

            if(reproductionOperators != null)
                b = b.setReproductionOperators(reproductionOperators);

            if(survivalOperators != null) {
                b = b.setSurvivalOperators(survivalOperators);
            }

            EvoEngine<GRNTreeChromosome> evoEngine = b.build();
            LOGGER.debug("Initialised engine at generation {}", evoEngine.getPopulationStats().getCurrentGeneration());
            return (GRNTreeEngine) evoEngine;
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            LOGGER.error("Could not import experiment: {}", ex.getLocalizedMessage());
            return null;
        }
    }

    public static Object getObjectFromDocument(Document d) {
        List<Object> parameters = d.getList("parameters", Object.class);

        List<Document> pairs = parameters.stream()
                .filter(o -> Document.class.isAssignableFrom(o.getClass()))
                .map(o -> (Document) o)
                .filter(o -> o.containsKey("key") && o.containsKey("value"))
                .collect(Collectors.toList());

        if(!pairs.isEmpty()) {
            Map<String, Double> map = pairs.stream().collect(Collectors.toMap(
                    doc -> doc.getString("key"),
                    doc -> doc.getDouble("value")));
            parameters.removeAll(pairs);
            parameters.add(map);
        }

        String className = d.get("className", String.class);
        Class<?> clazz = null;
        try {
            clazz = forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Object c = null;
        try {
            Constructor<?> constructor = null;
            for(Constructor<?> con : clazz.getConstructors()) {
                if(con.getParameterCount() == parameters.size()) {
                    constructor = con;
                }
            }
            c =  constructor.newInstance(parameters.toArray(new Object[]{}));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return c;

    }

    public static Evaluator getEvaluator(MEvaluator evaluator) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        ConstraintHandler constraintHandler = (ConstraintHandler) forName(evaluator.getConstraintHandler()).newInstance();
        List<Constraint<TimeCourseResult<GRNTreeChromosome>, Double>> constraints = evaluator.getConstraints().stream()
            .map(c -> getObjectFromDocument(c))
            .map(c -> ( Constraint<TimeCourseResult<GRNTreeChromosome>, Double>) c)
            .collect(Collectors.toList());

        List<SBMLModifier> modifiers = new ArrayList<>();

        for (String mod : evaluator.getSbmlModifiers()) {
            Object newInstance = forName(mod).newInstance();
            modifiers.add((SBMLModifier) newInstance);
        }

        GRNTreeCopasiConstraintEvaluator f = new GRNTreeCopasiConstraintEvaluator(
            (duration, runtime) -> (SBSbmlSimulator)getObjectFromDocument(evaluator.getSimulatorConfig()),
            constraintHandler,
            constraints,
            modifiers,
            evaluator.getDuration(),
            evaluator.getRunTime()
        );

        return f;
    }

    public static List<TerminationCondition> getTerminationConditions(MExperiment experiment) {
        return experiment.getTerminationConditions().stream()
                .map(d -> (TerminationCondition) getObjectFromDocument(d))
                .collect(Collectors.toList());
    }

    public static OperatorGroup getOperators(MExperiment experiment, MPopulationProcess process) throws ClassNotFoundException {


        if(process.getOperators() == null)
            return null;

        List<Operator> operators = new ArrayList<>();

        for(String operator : process.getOperators().getOperators()) {
            try {
                Class operatorClass = Class.forName(operator);
                Constructor constructor = operatorClass.getConstructor();
                operators.add((Operator<GRNTreeChromosome>) constructor.newInstance());
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                LOGGER.error(ex.getMessage());
            }
        }

        if(operators.isEmpty())
            return null;

        Class c = Class.forName(process.getOperators().getChromosomeClass());

        OperatorGroup g = new OperatorGroup(
                operators, c, process.getOperators().getMutationsPerGeneration(),
                process.getOperators().getWeights() != null ? process.getOperators().getWeights().stream().mapToDouble(Double::doubleValue).toArray() : null,
                process.getOperators().getSigmaScaled());

        return g;
    }
}
