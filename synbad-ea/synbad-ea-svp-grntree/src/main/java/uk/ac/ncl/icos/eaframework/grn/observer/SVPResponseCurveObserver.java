package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.grn.Exporter;
import uk.ac.ncl.icos.eaframework.grn.datatype.SVPStats;
import uk.ac.ncl.icos.eaframework.grn.exporter.SVPWriteExporter;
import uk.ac.ncl.icos.eaframework.EvolutionObserver;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.grn.GRNTreeChromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;

/**
 *
 * @author owengilfellon
 */
public class SVPResponseCurveObserver implements EvolutionObserver<Chromosome>, Serializable {

    private final Logger logger = LoggerFactory.getLogger(SVPResponseCurveObserver.class);
    List<String> metabolites = new ArrayList<String>();
    
    public SVPResponseCurveObserver(List<String> metabolitesToRecord) {
        this.metabolites.addAll(metabolitesToRecord);
    }

    @Override
    public <V extends EvoEngine<Chromosome>> void update(V s) {

        PopulationStats ps = s.getPopulationStats();
        StringBuilder sb = new StringBuilder();

        sb.append("Gen: ").append(ps.getCurrentGeneration()).append(" | ");
        sb.append("Pop: ").append(ps.getPopulationSize()).append(" | ");
        sb.append("Best Fit: ").append(ps.getBestFitness()).append(" | ");
        sb.append("Mean Fit: ").append(ps.getMeanFitness());

        Exporter<String> exporter = new SVPWriteExporter();
        Chromosome c = ps.getBestChromosome();

        if(c instanceof GRNTreeChromosome)
            {
                GRNTreeChromosome tree = (GRNTreeChromosome) c;
                sb.append(" | ").append(exporter.export(tree));
            }

            if(ps instanceof SVPStats)
            {
                SVPStats ss = (SVPStats) ps;
                ResponseCurve rc = ss.getResponseCurve();
                
                sb.append("RC: ");
                
                for(Double r:rc.getOutputs()){
                    sb.append(r).append(" ");
                }
            }
            
           // sb.append("\n");
            
            logger.debug(sb.toString());

            if(logger.isTraceEnabled()) {
                s.getEvaluatedSurvivalPopulation().forEach(ch ->
                    logger.trace("{} -> {} \t| {}", ch.getChromosome().getParentIds()[0], ch.getChromosome().getId(), ch.getFitness().getFitness()));
            }
    }
    
}
