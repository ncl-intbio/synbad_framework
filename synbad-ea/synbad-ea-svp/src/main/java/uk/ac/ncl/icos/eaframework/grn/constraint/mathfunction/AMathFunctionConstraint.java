/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint.mathfunction;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.util.List;

/**
 *  If correlated, then fitness is between 0.0 and 1.0. If not (by default), fitness is deviation between the
 *  target and observed and algorithm should minimise the fitness function
 * @author owengilfellon
 */
public abstract class AMathFunctionConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    protected final String INPUT;
    protected final String OUTPUT;
    protected boolean CORRELATION = false;

    public AMathFunctionConstraint(String input, String output) {
        this.INPUT = input;
        this.OUTPUT = output;
    }

    public AMathFunctionConstraint(String input, String output, boolean correlation) {
        this.INPUT = input;
        this.OUTPUT = output;
        this.CORRELATION = correlation;
    }

    public abstract double f(double x);

    @Override
    public Double apply(TimeCourseResult<T> cs) {
        
        ResponseCurve curve = cs.getResponseCurve(INPUT, OUTPUT);
        
        List<Double> inputs = curve.getInputs();
        List<Double> outputs = curve.getOutputs();

        double[] metaboliteAValues = new double[inputs.size()];
        double[] metaboliteBValues = new double[outputs.size()];

        int index = 0;

        for(int i = 0; i < inputs.size(); i++) {
            if(inputs.get(i) != 0) {
                metaboliteAValues[index] = f(inputs.get(i));
                metaboliteBValues[index] = outputs.get(i);
                index++;
            }
        }

        if(CORRELATION) {
            PearsonsCorrelation pc = new PearsonsCorrelation();
            double fitness = pc.correlation(metaboliteAValues, metaboliteBValues);
            fitness = Math.max(fitness, 0.0);
            return fitness;
        } else {
            double fitness = 0;
            for(int i = 0; i < metaboliteAValues.length; i++) {
                fitness += Math.abs( metaboliteAValues[i] - metaboliteBValues[i]);
            }
           // fitness = fitness / metaboliteAValues.length;
            return fitness;
        }
    }
}
