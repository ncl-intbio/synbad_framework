/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import java.io.Serializable;
import java.util.List;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

/**
 *
 * @author owengilfellon
 */
public class CFitness extends Fitness {
 
    private final List<CResult> constraintResult;

    public CFitness( double fitness, List<CResult> result ) {
        super(fitness);
        this.constraintResult = result;
    }

    public List<CResult> getResult() {
        return constraintResult;
    }

}
