/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="SelectionProcess")
public class HSelectionProcess implements Serializable {
    
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String processName;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    /*@JoinTable(name="Selection_Operators",
        joinColumns=@JoinColumn(name="SelectionId", referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="OperatorId", referencedColumnName="id"))*/
    private Set<HOperator> operators;
    
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    /*@JoinTable(name="Selection_Operators",
        joinColumns=@JoinColumn(name="SelectionId", referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="OperatorId", referencedColumnName="id"))*/
    private HOperatorWeights weights;
    
    @Column(nullable =  false)
    private int populationSize;
    
    public HSelectionProcess() {
    }

    public HSelectionProcess(Long id, String processName, int populationSize, Set<HOperator> operators) {
        this.id = id;
        this.processName = processName;
        this.populationSize = populationSize;
        this.operators = operators != null ? operators : new HashSet<>();   
        this.weights = null;
    }
    
    public HSelectionProcess(Long id, String processName, int populationSize, Set<HOperator> operators, HOperatorWeights weights) {
        this.id = id;
        this.processName = processName;
        this.populationSize = populationSize;
        this.operators = operators != null ? operators : new HashSet<>();   
        this.weights = weights != null ? weights : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStrategyName() {
        return processName;
    }

    public void setStrategyName(String processName) {
        this.processName = processName;
    }
    
    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int childPopulationSize) {
        this.populationSize = childPopulationSize;
    }

    public Set<HOperator> getOperators() {
        return operators == null ? new HashSet<>() : operators;
    }

    public void setOperators(Set<HOperator> operators) {
        this.operators = operators;
    }

    public HOperatorWeights getWeights() {
        return weights == null ? null : weights;
    }

    public void setWeights(HOperatorWeights weights) {
        this.weights = weights;
    }

}
