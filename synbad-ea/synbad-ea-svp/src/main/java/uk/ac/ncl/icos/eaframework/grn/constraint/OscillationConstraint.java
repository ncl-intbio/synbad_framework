/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.apache.commons.math3.analysis.function.Sin;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
public class OscillationConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    private final int STEPS_PER_PERIOD;
    private final double AMPLITUDE;
    private final String OUTPUT;
    private final Sin sin;

    public OscillationConstraint(String output, double amplitude, int stepsPerPeriod) {
        this.sin = new Sin();
        this.OUTPUT = output;
        this.AMPLITUDE = amplitude;
        this.STEPS_PER_PERIOD = stepsPerPeriod;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(OUTPUT)
                .addDouble(AMPLITUDE)
                .addInt(STEPS_PER_PERIOD)
                .build();
    }

    @Override
    public Double apply(TimeCourseResult<T> cs) {

        TimeCourseTrace curve = cs.getResults().get(cs.getResults().size() - 1).getTimeCourse(OUTPUT);

        if(curve == null) {
           return 0.0;
        }

        double[] observedValues = new double[curve.size()];
        double[] oscillationValues = new double[curve.size()];

        for(int i = 0; i < curve.size(); i++) {
            observedValues[i] = curve.get(i);
            oscillationValues[i] = sin.value(((double) i / (double)STEPS_PER_PERIOD) * Math.PI) * AMPLITUDE;
        }

        // Calculate correlation between observed curve and sin wave

        PearsonsCorrelation pc = new PearsonsCorrelation();
        double correlation = pc.correlation(observedValues, oscillationValues);

        correlation = (correlation + 1.0) / 2.0;

        return correlation;
    } 
}
