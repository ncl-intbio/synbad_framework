/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;


import org.bson.Document;
import org.bson.types.ObjectId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class MEvaluator implements Serializable {

    private String evaluatorClass;
    private Document simulatorConfig;
    private String constraintHandler;
    private List<Document> constraints;
    private List<String> sbmlModifiers;
    private int duration;
    private int runTime;

    public MEvaluator() {
    }

    public MEvaluator(String evaluatorClass,
                      Document simulatorConfig,
                      String constraintHandler,
                      List<Document> constraints,
                      List<String> sbmlModifiers,
                      int duration,
                      int runTime) {
        this.evaluatorClass = evaluatorClass;
        this.simulatorConfig = simulatorConfig;
        this.constraintHandler = constraintHandler;
        this.constraints = constraints;
        this.sbmlModifiers = sbmlModifiers;
        this.duration = duration;
        this.runTime = runTime;
    }

    public String getEvaluatorClass() {
        return evaluatorClass;
    }

    public void setEvaluatorClass(String evaluatorClass) {
        this.evaluatorClass = evaluatorClass;
    }

    public Document getSimulatorConfig() {
        return simulatorConfig;
    }

    public void setSimulatorConfig(Document simulatorConfig) {
        this.simulatorConfig = simulatorConfig;
    }

    public String getConstraintHandler() {
        return constraintHandler;
    }

    public void setConstraintHandler(String constraintHandler) {
        this.constraintHandler = constraintHandler;
    }

    public List<Document> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<Document> constraints) {
        this.constraints = constraints;
    }

    public List<String> getSbmlModifiers() {
        return sbmlModifiers;
    }

    public void setSbmlModifiers(List<String> sbmlModifiers) {
        this.sbmlModifiers = sbmlModifiers;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getRunTime() {
        return runTime;
    }

    public void setRunTime(int runTime) {
        this.runTime = runTime;
    }

}
