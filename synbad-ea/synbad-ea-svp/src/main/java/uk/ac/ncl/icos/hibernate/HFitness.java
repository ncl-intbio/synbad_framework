/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="ConstrainedFitness")
public class HFitness implements Serializable {
    
    @Column(name = "Fitness", nullable = false)
    private Double fitness;
    
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<HConstraintResult> constraintResult;
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, precision = 5, scale = 0)
    private Long id;
    /*
    @OneToOne(cascade = CascadeType.ALL)
    private EvaluatedSvpwrite svpwrite;
*/

    public HFitness() {
    }

    public HFitness(Double fitness, List<HConstraintResult> result) {
        this.fitness = fitness;
        this.constraintResult = result;
    }

    public Double getFitness() {
        return fitness;
    }

    public void setFitness(Double fitness) {
        this.fitness = fitness;
    }
/*
    public EvaluatedSvpwrite getSvpwrite() {
        return svpwrite;
    }

    public void setSvpwrite(EvaluatedSvpwrite svpwrite) {
        this.svpwrite = svpwrite;
    }
*/
    public List<HConstraintResult> getResult() {
        return constraintResult;
    }

    public void setResult(List<HConstraintResult> result) {
        this.constraintResult = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
