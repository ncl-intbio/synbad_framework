package uk.ac.ncl.icos.eaframework.grn.constraint;

public class ConstraintHelper {

    public static double sigmoid(double x, double midpoint, double range) {

        // starting range is ~-6 -> 6 = ~12
        // starting midpoint is 0

        double r = range / 12.0;
        return 1.0 / (1.0 + ( Math.pow( Math.E,  -( (x - midpoint) / r) )));
    }
}


