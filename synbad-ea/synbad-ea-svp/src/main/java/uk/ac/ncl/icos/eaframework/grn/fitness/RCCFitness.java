/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class RCCFitness extends CFitness {

    private final List<TimeCourseTraces> simulationResults;

    public RCCFitness(double fitness, List<CResult> result, List<TimeCourseTraces> simulationResults ) {
        super(fitness, result);
        this.simulationResults = simulationResults;
    }

    public List<TimeCourseTraces> getSimulationResults() {
        return simulationResults;
    }
}
