/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;

import org.bson.Document;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author owengilfellon
 */
public class MPopulationProcess implements Serializable {
    
    private Long id;

    private String className;

    private int populationSize;

    private Document selectionConfig;

    private MOperators operators;

    public MPopulationProcess() {

    }

    public MPopulationProcess(Long id, int populationSize, String className, Document selectionConfig, MOperators operators) {
        this.id = id;
        this.className = className;
        this.populationSize = populationSize;
        this.selectionConfig = selectionConfig;
        this.operators = operators;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public MOperators getOperators() {
        return operators;
    }

    public void setOperators(MOperators operators) {
        this.operators = operators;
    }

    public Document getSelectionConfig() {
        return selectionConfig;
    }

    public void setSelectionConfig(Document selectionConfig) {
        this.selectionConfig = selectionConfig;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int childPopulationSize) {
        this.populationSize = childPopulationSize;
    }

}
