/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.util.Arrays;
import java.util.List;

/**
 *  Returns the deviation of concentration from the target (up to the maximum of the target concentration
 *  i.e - 0 to 2x target concentration) as a percentage, with 0
 *
 * @author owengilfellon
 */
public class ConcentrationConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConcentrationConstraint.class);

    private final String OUTPUT;
    private final Double CONCENTRATION;

    /**
     *
     * @param output
     * @param concentration in nmol/fl
     */
    public ConcentrationConstraint(String output, Double concentration) {
        this.OUTPUT = output;
        this.CONCENTRATION = concentration;
    }

    @Override
    public Config getConfig() {
        return Config.create()
            .addClassName(getClass().getName())
            .addString(OUTPUT)
            .addDouble(CONCENTRATION)
            .build();
    }

    @Override
    public Double apply(TimeCourseResult<T> cs) {
        
        TimeCourseTraces traces = cs.getResults().get(cs.getResults().size() - 1);
        TimeCourseTrace trace = traces.getTimeCourse(OUTPUT);
        Double finalValueOfOutput = trace.get(trace.size() - 1);

        double deviation = Math.abs(CONCENTRATION - finalValueOfOutput);

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Concentration: {} | Target: {} | Fitness: {}", finalValueOfOutput, CONCENTRATION, deviation);

        return deviation;
    } 
}
