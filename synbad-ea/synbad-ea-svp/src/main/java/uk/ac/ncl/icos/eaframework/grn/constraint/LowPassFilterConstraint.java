/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public class LowPassFilterConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LowPassFilterConstraint.class);

    private final String INPUT;
    private final String OUTPUT;
    private final double CUT_OFF;

    /**
     *
     * @param output
     * @param cutOff concentration in nmol/fl
     */
    public LowPassFilterConstraint(String input, String output, double cutOff) {
        this.CUT_OFF = cutOff;
        this.INPUT = input;
        this.OUTPUT = output;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(INPUT)
                .addString(OUTPUT)
                .addDouble(CUT_OFF).build();
    }

    @Override
    public Double apply(TimeCourseResult<T> cs) {
        ResponseCurve c = cs.getResponseCurve(INPUT, OUTPUT);
/*
        if(c == null) {
            LOGGER.error("Could not retrieve response curve for {} and {}", INPUT_SPECIES, OUTPUT_SPECIES);
            return 0.0;
        }

        double uninduced = c.getOutputs().get(0);
        double deviation  = uninduced - LOWER_BOUND;

        // cap deviation between 0 and 1

        double cappedDeviation = deviation > LOWER_BOUND ? deviation : LOWER_BOUND;

        double result =  (1.0 / ((cappedDeviation / MIDPOINT) / PENALTY_WEIGHT + 1.0));

        if(result > 1.0 || result < 0) {
            logger.error("Constraint is out of range: {}", result);
        }*/

        return null;
    } 
}
