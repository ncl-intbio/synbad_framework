package uk.ac.ncl.icos.mongo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.RCCFitness;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.GenerationTermination;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.mongo.*;

import java.util.*;
import java.util.stream.Collectors;

public class MExport {

    private List<MEvaluated> chromosomes;
    private MExperiment experiment;

    public MExport(Collection<MEvaluated> chromosomes, MExperiment experiment) {
        this.experiment = experiment;
        this.chromosomes = new LinkedList<>(chromosomes);
    }

    public List<MEvaluated> getChromosomes() {
        return chromosomes;
    }

    public MExperiment getExperiment() {
        return experiment;
    }
}
