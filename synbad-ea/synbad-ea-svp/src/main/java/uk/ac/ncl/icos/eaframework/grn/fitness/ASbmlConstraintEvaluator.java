/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import uk.ac.ncl.icos.eaframework.Evaluator;

import java.io.Serializable;
import java.util.List;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.ConstraintHandler;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.svpcompiler.Compilable.SBMLModifier;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;

/**
 * 
 * @author owengilfellon
 */

public abstract class ASbmlConstraintEvaluator<T extends Chromosome> implements Evaluator<T>, Serializable {

    protected final SBSbmlSimulatorFactory cs;
    private final ConstraintHandler handler;
    private final List<Constraint<TimeCourseResult<T>, Double>> constraints;
    private final int duration;
    private final int runtime;
    private final SBMLModifier[] modifiers;

    public ASbmlConstraintEvaluator(
            SBSbmlSimulatorFactory cs, 
            ConstraintHandler handler,
            List<Constraint<TimeCourseResult<T>, Double>> constraints,
            List<SBMLModifier> modifiers,
            int duration, 
            int runtime) {
        this.cs = cs;
        this.duration = duration;
        this.runtime = runtime;
        this.handler = handler;
        this.constraints = constraints;
        this.modifiers = modifiers.toArray(new SBMLModifier[]{});
    }

    public SBSbmlSimulatorFactory getSimulatorFactory() {
        return cs;
    }

    public int getDuration() {
        return duration;
    }

    public int getRuntime() {
        return runtime;
    }

    public SBMLModifier[] getModifiers() {
        return modifiers;
    }

    public List<Constraint<TimeCourseResult<T>, Double>> getConstraints() {
        return constraints;
    }

    public ConstraintHandler getHandler() {
        return handler;
    }

}
