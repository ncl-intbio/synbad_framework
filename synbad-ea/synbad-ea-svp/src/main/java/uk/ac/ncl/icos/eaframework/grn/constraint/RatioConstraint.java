/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 * Penalises decreases in the ratio of maximum input to maximum output, returning a value between 0.0 and 1.0. Equal to
 * or higher than the original ratio returns 1.0. Decreases return a value proportional to the decrease (e.g. a decrease
 * of a half returns a value of 0.5).
 * @author owengilfellon
 */
public class RatioConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    private final double TARGET_RATIO;
    private final String INPUT;
    private final String OUTPUT;
    
    private static final Logger logger = LoggerFactory.getLogger(RatioConstraint.class);

    public RatioConstraint(String input, String output, double targetRatio) {
        this.TARGET_RATIO = targetRatio;
        this.INPUT = input;
        this.OUTPUT = output;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(INPUT)
                .addString(OUTPUT)
                .addDouble(TARGET_RATIO)
                .build();
    }

    @Override
    public Double apply(TimeCourseResult<T> r) {     
        
        ResponseCurve curve = r.getResponseCurve(INPUT, OUTPUT);

        // The input-output observedRatio is calculated using the highest values of each

        double dependent = curve.getMaxInducedOutput();
        double independent =  curve.getMaxInput();

        double observedRatio = dependent / independent;

        if(Double.isNaN(observedRatio))
            return 0.0;

        double ratio = observedRatio / TARGET_RATIO;

        // Higher input-output ratios are allowed, lower ratios decrease fitness.

        double ratioPenalty = ratio >= 1.0 ? 1.0 : ratio;

        if(ratioPenalty > 1.0 || ratioPenalty < 0) {
            logger.error("Constraint is out of range: {}", ratioPenalty);
        }

        return ratioPenalty;
    } 

}
