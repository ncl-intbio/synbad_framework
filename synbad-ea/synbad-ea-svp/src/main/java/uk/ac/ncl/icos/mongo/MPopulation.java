/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;


import org.bson.types.ObjectId;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="Population")
public class MPopulation implements Serializable {
    
    @Column(nullable = false)
    private int generation;
    
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ObjectId> survivalPopulation = new ArrayList<>();

    private List<ObjectId> reproductionPopulation = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ObjectId bestChromosome;

    private long duration;

    @Column(nullable = false)
    private int populationSize;

    @Column(nullable = false)
    private double meanFitness;

    @Column(nullable = false)
    private double bestFitness;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ObjectId id;

    public MPopulation() {
    }

    public MPopulation(int generation,
                       List<ObjectId> reproduction,
                       List<ObjectId> survival,
                       ObjectId bestChromosome,
                       int populationSize,
                       double meanFitness,
                       double bestFitness,
                       long duration) {
        this.generation = generation;
        this.survivalPopulation = survival;
        this.reproductionPopulation = reproduction;
        this.bestChromosome = bestChromosome;
        this.populationSize = populationSize;
        this.meanFitness = meanFitness;
        this.bestFitness = bestFitness;
        this.duration = duration;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public List<ObjectId> getSurvivalPopulation() {
        return survivalPopulation;
    }

    public void setSurvivalPopulation(List<ObjectId> population) {
        this.survivalPopulation = population;
    }

    public List<ObjectId> getReproductionPopulation() {
        return reproductionPopulation;
    }

    public void setReproductionPopulation(List<ObjectId> population) {
        this.reproductionPopulation = population;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public ObjectId getBestChromosome() {
        return bestChromosome;
    }

    public void setBestChromosome(ObjectId bestChromosome) {
        this.bestChromosome = bestChromosome;
    }

    public double getBestFitness() {
        return bestFitness;
    }

    public void setBestFitness(double bestFitness) {
        this.bestFitness = bestFitness;
    }

    public double getMeanFitness() {
        return meanFitness;
    }

    public void setMeanFitness(double meanFitness) {
        this.meanFitness = meanFitness;
    }
    
    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }

    public long getDuration() { return duration; }

    public void setDuration(long duration) { this.duration = duration; }
    
}
