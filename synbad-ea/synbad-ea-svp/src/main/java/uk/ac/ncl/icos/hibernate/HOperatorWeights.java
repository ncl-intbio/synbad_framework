/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="OperatorWeights")
public class HOperatorWeights implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private boolean sigmaScaled;
    
    private String weights;

    public HOperatorWeights() {
    }

    public HOperatorWeights(double[] weights, boolean sigmascaled) {
        this.setWeights(weights);
        this.sigmaScaled = sigmascaled;
    }

    public double[] getWeights() {
        
        String[] stringWeights = weights.split(" ");
        
        double[] weights = new double[stringWeights.length];
        
        for(int i = 0; i < weights.length; i++) {
            weights[i] = Double.parseDouble(stringWeights[i]);
        }
        
        return weights;
    }
    
    public final void setWeights(double[] weights) {
        StringBuilder b = new StringBuilder();
        for(int i = 0; i < weights.length; i++) {
            b.append(weights[i]);
            if(i != (weights.length - 1))
                b.append(" ");
        }
        this.weights = b.toString();
    }

    public boolean isSigmaScaled() {
        return sigmaScaled;
    }

    public void setSigmaScaled(boolean sigmaScaled) {
        this.sigmaScaled = sigmaScaled;
    }

}
