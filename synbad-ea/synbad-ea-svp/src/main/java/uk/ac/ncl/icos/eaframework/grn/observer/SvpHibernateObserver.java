package uk.ac.ncl.icos.eaframework.grn.observer;

import uk.ac.ncl.icos.eaframework.EvolutionObserver;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.EvoEngine;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;
import uk.ac.ncl.icos.eaframework.PopulationProcess;
import uk.ac.ncl.icos.eaframework.Operator;
import uk.ac.ncl.icos.eaframework.Selection;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.hibernate.HFitness;
import uk.ac.ncl.icos.hibernate.HConstraintResult;
import uk.ac.ncl.icos.hibernate.HEvaluated;
import uk.ac.ncl.icos.hibernate.HOperator;
import uk.ac.ncl.icos.hibernate.HExperiment;
import uk.ac.ncl.icos.hibernate.HOperatorWeights;
import uk.ac.ncl.icos.hibernate.HibernateUtil;
import uk.ac.ncl.icos.hibernate.HPopulation;
import uk.ac.ncl.icos.hibernate.HSelectionProcess;

/**
 *
 * @author owengilfellon
 */
public class SvpHibernateObserver<T extends Chromosome> implements EvolutionObserver<T>, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(SvpHibernateObserver.class);
    
    private final String experimentName;
    private final String dependentmetabolite;
    private final String independentMetabolite;
    private final double incrementSize;
    private final int maxGenerations;
    private final int childPopulationSize;
    private final int survivalPopulationSize;

    public SvpHibernateObserver(String experimentName, String independentMetabolite, String dependentMetabolite, double incrementSize, int maxGenerations, int childPopulationSize, int survivalPopulationSize) {
        this.experimentName = experimentName;
        this.dependentmetabolite = dependentMetabolite;
        this.independentMetabolite = independentMetabolite;
        this.incrementSize = incrementSize;
        this.maxGenerations = maxGenerations;
        this.childPopulationSize = childPopulationSize;
        this.survivalPopulationSize = survivalPopulationSize;
    }
    
    public List<Operator<T>> getOperators(PopulationProcess<T> process) {
        
        if(process.getOperator() == null || !(OperatorGroup.class.isAssignableFrom(process.getOperator().getClass())))
            return Collections.EMPTY_LIST;

        List<Operator<T>> operators = ((OperatorGroup)process.getOperator()).getOperators();
        logger.debug(process.getClass().getSimpleName() + " has " + operators.size() + " evolutionary operators");
        return operators;
    }

    @Override
    public <V extends EvoEngine<T>> void update(V s) {
        logger.info("Doing hibernate export...");
        
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transction = session.beginTransaction();

        EntityManager m = session.getEntityManagerFactory().createEntityManager();

        TypedQuery<HExperiment> query = m.createQuery("SELECT e FROM MExperiment e WHERE e.name = '" + experimentName + "'", HExperiment.class);

        // Get experiment, or create

        HExperiment exp = null;
        try {
            exp = query.getSingleResult();
        } catch( NoResultException e ) {

            logger.info("...no experiment found - creating experiment...");
            
            exp = new HExperiment();
            exp.setName(experimentName);
            exp.setDependentMetabolite(dependentmetabolite);
            exp.setIndependentMetabolite(independentMetabolite);
            exp.setIncrementSize(incrementSize);
            exp.setPopulations(new ArrayList<>());
            exp.setMaxGenerations(maxGenerations );


            Selection<T> selection1 = s.getEvolutionaryStrategy().getReproduction().getSelection();

            Set<HOperator> evoOperators1 = getOperators(s.getEvolutionaryStrategy().getReproduction()).stream()
                .map(o -> o.getClass().getSimpleName())
                .map(name -> {
                    HOperator eo = new HOperator();
                  //  eo.setOperatorName(Operators.valueOf(name));
                    return eo; })
                .collect(Collectors.toSet());

            HSelectionProcess reproductionProcess = new HSelectionProcess();
            reproductionProcess.setStrategyName(selection1.getClass().getSimpleName());
            reproductionProcess.setPopulationSize(childPopulationSize);
            reproductionProcess.setOperators(evoOperators1);
            
            if(s.getEvolutionaryStrategy().getReproduction().getOperator() != null &&
                    OperatorGroup.class.isAssignableFrom(s.getEvolutionaryStrategy().getReproduction().getOperator().getClass()))
            {
                OperatorGroup group = ((OperatorGroup)s.getEvolutionaryStrategy().getReproduction().getOperator());
                double[] weights = group.getWeights();
                if(weights != null)
                    reproductionProcess.setWeights(new HOperatorWeights(weights, group.isSigmaScaled()));
            }
            
            
            Selection<T> selection2 = s.getEvolutionaryStrategy().getSurvival().getSelection();

            Set<HOperator> evoOperators2 = getOperators(s.getEvolutionaryStrategy().getSurvival()).stream()
                .map(o -> o.getClass().getSimpleName())
                .map(name -> {
                    HOperator eo = new HOperator();
                    //eo.setOperatorName(Operators.valueOf(name));
                    return eo; })
                .collect(Collectors.toSet());
            
            //System.out.println(evoOperators1.size() + " : " + evoOperators2.size());


            HSelectionProcess survivalProcess = new HSelectionProcess();
            survivalProcess.setStrategyName(selection2.getClass().getSimpleName());
            survivalProcess.setPopulationSize(survivalPopulationSize);
            survivalProcess.setOperators(evoOperators2);
            
            if(s.getEvolutionaryStrategy().getSurvival().getOperator() != null && 
                    OperatorGroup.class.isAssignableFrom(s.getEvolutionaryStrategy().getSurvival().getOperator().getClass()))
            {
                OperatorGroup group = ((OperatorGroup)s.getEvolutionaryStrategy().getSurvival().getOperator());
                double[] weights = group.getWeights();
                if(weights != null)
                    survivalProcess.setWeights(new HOperatorWeights(weights, group.isSigmaScaled()));
            }
            
            Stream.concat(evoOperators1.stream(), evoOperators2.stream()).forEach(eo -> {
                if(m.find(HOperator.class, eo.getOperatorName()) == null) {
                    session.save(eo);
                }
            });
            

            session.save(reproductionProcess);
            session.save(survivalProcess);
            
            exp.setReproductionProcess(reproductionProcess);
            exp.setSurvivalProcess(survivalProcess);
            
            
        }

        PopulationStats<T> stats = s.getPopulationStats();
        int GENERATION = stats.getCurrentGeneration();
        List<EvaluatedSvpChromosome<T>> POPULATION = s.getEvaluatedSurvivalPopulation();

        HPopulation pop = new HPopulation();
        List<HEvaluated> chromosomes = new ArrayList<>();

        double bestFitness = 0.0;
        double averageFitness = 0.0;
        HEvaluated bestChromosome = null; 

        for(EvaluatedSvpChromosome<T> ec : POPULATION) {
            HFitness f = new HFitness();

            List<HConstraintResult> r = ec.getConstraintResults().stream().map((CResult cr) ->  {
                HConstraintResult result = new HConstraintResult();
                result.setName(cr.getName());
                result.setEvaluation(cr.getEvaluation());
              //  result.setConstraintedFitness(f);
                return result;
            }).collect(Collectors.toList());

            f.setResult(r);
            f.setFitness(ec.getFitness().getFitness());

            HEvaluated svpWrite = new HEvaluated();
            String svpWriteString = ec.getChromosome().toString();
            svpWrite.setSvpwrite(svpWriteString.substring(0, svpWriteString.length()-2));
            svpWrite.setFitness(f);

            /*List<TimeCourseTraces> ts = ec.getResults();
            List<TimeCourseTrace> metaboliteRuns = ts.stream().map(re -> re.getTimecourse(dependentmetabolite)).collect(Collectors.toList());
            StringBuilder sb = new StringBuilder();
           
            for(TimeCourseTrace run : metaboliteRuns) {
                sb.append(run.get(run.size()-1)).append(" ");
            }*/

            if(bestFitness < f.getFitness()) {
                bestChromosome = svpWrite;
                bestFitness = f.getFitness();
            }

            averageFitness += f.getFitness();
          //  svpWrite.setResponseCurve(sb.toString());

            logger.debug("Creating persisted chromosome {} of {}: [ {} ]", POPULATION.indexOf(ec), POPULATION.size(), svpWriteString);
            chromosomes.add(svpWrite);
        }

        averageFitness /= chromosomes.size();

        pop.setPopulation(chromosomes);
        pop.setBestChromosome(bestChromosome);
        pop.setMeanFitness(averageFitness);
        pop.setBestFitness(bestFitness);
        pop.setGeneration(GENERATION);
        pop.setPopulationSize(chromosomes.size());

        List<HPopulation> population = exp.getPopulations().stream().collect(Collectors.toList());
        population.add(pop);                
        exp.setPopulations(population);

        if(exp.getPopulations().size()==1)
            session.save(exp);
        else
            session.merge (exp);
        transction.commit();
        m.close();
        session.close();
        
        logger.info("...hibernate export done.");
    }
}
