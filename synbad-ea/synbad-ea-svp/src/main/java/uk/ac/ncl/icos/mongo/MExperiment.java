/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;


import org.bson.Document;

import java.util.List;

/**
 *
 * @author owengilfellon
 */
public final class MExperiment {
    
    private String name;

    private List<MPopulation> populations;

    private MPopulationProcess reproductionProcess;

    private MPopulationProcess survivalProcess;

    private List<Document> terminationConditions;

    private MEvaluator evaluator;

    private boolean overlapping;

    private boolean ordered;

    private boolean minimiseFitnessFunc;

    private int threads;

    public MExperiment() {
    }

    public MExperiment(String name,
                       List<MPopulation> populations,
                       boolean overlapping,
                       MEvaluator evaluator,
                       MPopulationProcess reproductionProcess,
                       MPopulationProcess survivalProcess,
                       List<Document> terminationConditions,
                       boolean ordered,
                       boolean minimiseFitnessFunc,
                       int threads) {
        this.name = name;
        this.populations = populations;
        this.evaluator = evaluator;
        this.reproductionProcess = reproductionProcess;
        this.survivalProcess = survivalProcess;
        this.terminationConditions = terminationConditions;
        this.overlapping = overlapping;
        this.ordered = ordered;
        this.minimiseFitnessFunc  = minimiseFitnessFunc;
        this.threads = threads;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public MEvaluator getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(MEvaluator evaluator) {
        this.evaluator = evaluator;
    }

    public List<MPopulation> getPopulations() {
        return populations;
    }

    public void setPopulations(List<MPopulation> populations) {
        this.populations = populations;
    }

    public List<Document> getTerminationConditions() {
        return terminationConditions;
    }

    public void setTerminationConditions(List<Document> terminationConditions) {
        this.terminationConditions = terminationConditions;
    }

    public MPopulationProcess getReproductionProcess() {
        return reproductionProcess;
    }


    public void setReproductionProcess(MPopulationProcess reproductionProcess) {
        this.reproductionProcess = reproductionProcess;
    }

    public MPopulationProcess getSurvivalProcess() {
        return survivalProcess;
    }

    public void setSurvivalProcess(MPopulationProcess survivalProcess) {
        this.survivalProcess = survivalProcess;
    }

    public void setOverlapping(boolean overlapping) {
        this.overlapping = overlapping;
    }

    public boolean getOverlapping() {
        return overlapping;
    }

    public boolean getOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }

    public boolean getMinimiseFitnessFunc() {
        return minimiseFitnessFunc;
    }

    public void setMinimiseFitnessFunc(boolean minimiseFitnessFunc) {
        this.minimiseFitnessFunc = minimiseFitnessFunc;
    }
}
