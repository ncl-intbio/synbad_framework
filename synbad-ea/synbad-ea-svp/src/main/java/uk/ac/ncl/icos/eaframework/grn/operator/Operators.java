package uk.ac.ncl.icos.eaframework.grn.operator;

public interface Operators {

    String getPackage();

    String getFullRef();

}
