/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.datatype;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;

/**
 *
 * @author owengilfellon
 */
public class SVPStats<T extends Chromosome> extends PopulationStats {
    
    ResponseCurve responseCurve;
    
    public SVPStats(ResponseCurve responseCurve, PopulationStats<T> stats){
        super(stats.getBestChromosome(), stats.getCurrentGeneration(), stats.getPopulationSize(), stats.getMeanFitness(), stats.getBestFitness(), stats.getDuration());
        this.responseCurve = responseCurve;
    }
    
    public SVPStats(ResponseCurve responseCurve, Chromosome bestChromosome, int currentGeneration, int populationSize, double meanFitness, double bestFitness, long duration) {
        super(bestChromosome, currentGeneration, populationSize, meanFitness, bestFitness, duration);
        this.responseCurve = responseCurve;
    }

    public ResponseCurve getResponseCurve() {
        return responseCurve;
    }

    
}
