/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint.mathfunction;

import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
public class SquareRootConstraint<T extends Chromosome> extends AMathFunctionConstraint<T> {

    public SquareRootConstraint(String input, String output) {
        super(input, output);
    }

    public SquareRootConstraint(String input, String output, boolean correlation) {
        super(input, output, correlation);
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(INPUT)
                .addString(OUTPUT)
                .addBoolean(CORRELATION)
                .build();
    }

    @Override
    public double f(double x) {
        return Math.sqrt(x);
    }
}
