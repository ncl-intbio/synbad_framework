/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import uk.ac.ncl.icos.eaframework.grn.operator.Operators;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="Operator")
public class HOperator implements Serializable {
    
    @Id
    @Enumerated(value = EnumType.STRING)
    private Operators operatorName;
    
    @ManyToMany(mappedBy = "operators")
    private Set<HSelectionProcess> selection;

    public HOperator() {
    }

    public HOperator(Operators operatorName) {
        this.operatorName = operatorName;
    }

    public Set<HSelectionProcess> getSelection() {
        return selection;
    }

    public void setSelection(Set<HSelectionProcess> selection) {
        this.selection = selection;
    }

    public Operators getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(Operators operatorName) {
        this.operatorName = operatorName;
    }
    
}
