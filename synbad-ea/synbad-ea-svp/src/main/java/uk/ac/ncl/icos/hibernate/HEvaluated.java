/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="EvaluatedSvpwrite")
public class HEvaluated implements Serializable {
    
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    
    @Column(name = "SvpWrite", columnDefinition = "MEDIUMTEXT", nullable = false)
    private String svpwrite;
    
    @Column(columnDefinition = "MEDIUMTEXT", nullable = false)
    private String responseCurve;
    
    @OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private HFitness constrainedFitness;
    
    /*
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false, targetEntity = Population.class)
    private Population population;
    */
    public HEvaluated() {
    }
    
    public HEvaluated(String svpwrite, HFitness fitness, String responseCurve) {
        this.svpwrite = svpwrite;
        this.constrainedFitness = fitness;
        this.responseCurve = responseCurve;
    }

     public HFitness getFitness() {
        return constrainedFitness;
    }

    public void setFitness(HFitness fitness) {
        this.constrainedFitness = fitness;
    }

    public String getSvpwrite() {
        return svpwrite;
    }

    public void setSvpwrite(String svpwrite) {
        this.svpwrite = svpwrite;
    }

    public String getResponseCurve() {
        return responseCurve;
    }

    public void setResponseCurve(String responseCurve) {
        this.responseCurve = responseCurve;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    /*
    public Population getSurvivalPopulation() {
        return population;
    }

    public void setSurvivalPopulation(Population population) {
        this.population = population;
    }*/
    
    

}
