/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 *
 * @author owengilfellon
 */
public class PenaliseRangeDecrease<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {
    
    private final double TARGET_RANGE;
    private final double PENALTY_WEIGHT;
    
    private final String INPUT;
    private final String OUTPUT;
    
    private static final Logger logger = LoggerFactory.getLogger(PenaliseRangeDecrease.class);

    public PenaliseRangeDecrease(String input, String output, double targetRange, double penaltyWeight) {
        this.TARGET_RANGE = targetRange;
        this.PENALTY_WEIGHT = penaltyWeight;
        this.INPUT = input;
        this.OUTPUT = output;
    }

    @Override
    public Config getConfig() {
        return Config.create()
            .addClassName(getClass().getName())
            .addString(INPUT)
            .addString(OUTPUT)
            .addDouble(TARGET_RANGE)
            .addDouble(PENALTY_WEIGHT)
            .build();
    }

    @Override
    public Double apply(TimeCourseResult<T> r) {     
        
        ResponseCurve curve = r.getResponseCurve(INPUT, OUTPUT);
       
        double bottom = curve.getOutputs().get(0);
        double top = curve.getMaxInducedOutput();
        
        // TO-DO: Is use of absolute correct here?
        
        double observedRange = Math.abs(top - bottom);
        double rangeFitness = observedRange > TARGET_RANGE ? 1.0 : 1.0 / Math.sqrt(1.0 + (Math.abs(observedRange - TARGET_RANGE) * PENALTY_WEIGHT));
        
        if(rangeFitness > 1.0 || rangeFitness < 0) {
            logger.error("Constraint is out of range: {}", rangeFitness);
        }
        
        return rangeFitness;
    } 
}
