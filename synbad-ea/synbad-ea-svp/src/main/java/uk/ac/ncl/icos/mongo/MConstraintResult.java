/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="ConstraintResult")
public class MConstraintResult implements Serializable {

    private String name;

    private Double evaluation;

    private Long id;

    public MConstraintResult() { }

    public MConstraintResult(String name, Double evaluation) {
        this.name = name;
        this.evaluation = evaluation;
    }

    public Double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Double evaluation) {
        this.evaluation = evaluation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
/*
    public ConstrainedFitness getConstraintedFitness() {
        return constrainedFitness;
    }

    public void setConstraintedFitness(ConstrainedFitness constraintedFitness) {
        this.constrainedFitness = constraintedFitness;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
