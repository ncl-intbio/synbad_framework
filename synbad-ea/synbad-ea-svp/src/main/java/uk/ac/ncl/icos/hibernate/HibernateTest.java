/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.util.Date;
import org.hibernate.Session;

/**
 *
 * @author owengilfellon
 */
public class HibernateTest {
 
    public static void main(String[] args) {

            System.out.println("Maven + Hibernate + Oracle");
            Session session = HibernateUtil.getSessionFactory().openSession();

            session.beginTransaction();
            TestObject testObject = new TestObject();

            testObject.setUserId(301);
            testObject.setUsername("hello!");
            testObject.setCreatedBy("world!");
            testObject.setCreatedDate(new Date());

            session.save(testObject);
            session.getTransaction().commit();
           
            session.close();
            System.exit(0);
            
	}
}
