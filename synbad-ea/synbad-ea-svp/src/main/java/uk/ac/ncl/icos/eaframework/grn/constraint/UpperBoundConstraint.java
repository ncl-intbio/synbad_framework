/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 * Penalises approaching the upper bound. For the upper 10% of the allowed range, the returned value decreases from 1.0
 * to 0.0 according to a sigmoid function. Above the upper bound, 0.0 is returned.
 * @author owengilfellon
 */
public class UpperBoundConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {
    
    private final double UPPER_BOUND;

    private final double RANGE;
    private final double MIDPOINT;
    
    private final String INPUT;
    private final String OUTPUT;
    
    private final static Logger logger = LoggerFactory.getLogger(UpperBoundConstraint.class);

    public UpperBoundConstraint(String input, String output, double upperBound) {
        this.UPPER_BOUND = upperBound;
        this.INPUT = input;
        this.OUTPUT = output;
        this.RANGE =  UPPER_BOUND / 10.0;
        this.MIDPOINT = UPPER_BOUND - (RANGE / 2.0);
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(INPUT)
                .addString(OUTPUT)
                .addDouble(UPPER_BOUND).build();
    }

    @Override
    public Double apply(TimeCourseResult<T> r) {   
        
        ResponseCurve curve = r.getResponseCurve(INPUT, OUTPUT);

        if(curve.getMaxInducedOutput() > UPPER_BOUND) return 0.0;

        if(curve.getMaxInducedOutput() < UPPER_BOUND - RANGE) return 1.0;

        return 1.0 - ConstraintHelper.sigmoid(curve.getMaxInducedOutput(), MIDPOINT, RANGE);
    }
}
