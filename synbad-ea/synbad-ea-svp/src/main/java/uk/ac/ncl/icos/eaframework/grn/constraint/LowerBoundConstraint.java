/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.synbad.core.util.Config;

/**
 * Penalises when the base level increases above the lower bound. For 10% of the starting range above the
 * lower bound, the returned value decreases from 1.0 to 0.0 according to a sigmoid function. Above the lower bound plus
 * 10% of the starting range, 0.0 is returned.
 * @author owengilfellon
 */
public class LowerBoundConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    private final double STARTING_RANGE;
    private final double LOWER_BOUND;
    
    private final double RANGE;
    private final double MIDPOINT;
    
    private final String INPUT;
    private final String OUTPUT;
    
    private final static Logger logger = LoggerFactory.getLogger(LowerBoundConstraint.class);

    public LowerBoundConstraint(String input, String output, double lowerBound, double startingRange) {
        this.LOWER_BOUND = lowerBound;
        this.STARTING_RANGE = startingRange;
        this.INPUT = input;
        this.OUTPUT = output;
        this.RANGE =  STARTING_RANGE / 10.0;
        this.MIDPOINT = LOWER_BOUND + (RANGE / 2.0);
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(INPUT)
                .addString(OUTPUT)
                .addDouble(LOWER_BOUND)
                .addDouble(STARTING_RANGE).build();
    }

    @Override
    public Double apply(TimeCourseResult<T> r) {   
        
        ResponseCurve curve = r.getResponseCurve(INPUT, OUTPUT);
        double uninduced = curve.getOutputs().get(0);
        if(uninduced <= LOWER_BOUND) return 1.0;
        if(uninduced > LOWER_BOUND + RANGE) return 0.0;
        return 1.0 - ConstraintHelper.sigmoid(uninduced, MIDPOINT, RANGE);
    }
}
