/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public class CResult implements Serializable {
    
    private final String name;
    private final double evaluation;
    
    public CResult(String name, double evaluation) {
        this.name = name;
        this.evaluation = evaluation;
    }

    public double getEvaluation() {
        return evaluation;
    }
    
    public String getName() {
        return name;
    }
}
