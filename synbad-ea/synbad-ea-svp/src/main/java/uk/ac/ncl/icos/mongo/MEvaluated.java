/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;

import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="EvaluatedSvpwrite")
public class MEvaluated implements Serializable {

    @BsonId
    private ObjectId chromosomeId;

    private ObjectId parentId;

    private String experimentName;
    
    @Column(name = "SvpWrite", columnDefinition = "MEDIUMTEXT", nullable = false)
    private String svpwrite;
    
    @Column(columnDefinition = "MEDIUMTEXT", nullable = false)
    private String responseCurve;
    
    @OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private MFitness constrainedFitness;
    
    /*
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional = false, targetEntity = Population.class)
    private Population population;
    */
    public MEvaluated() {
    }
    
    public MEvaluated(String svpwrite, MFitness fitness, String responseCurve) {
        this.svpwrite = svpwrite;
        this.constrainedFitness = fitness;
        this.responseCurve = responseCurve;
    }

     public MFitness getFitness() {
        return constrainedFitness;
    }

    public void setFitness(MFitness fitness) {
        this.constrainedFitness = fitness;
    }

    public String getSvpwrite() {
        return svpwrite;
    }

    public void setSvpwrite(String svpwrite) {
        this.svpwrite = svpwrite;
    }

    public String getResponseCurve() {
        return responseCurve;
    }

    public void setResponseCurve(String responseCurve) {
        this.responseCurve = responseCurve;
    }

    public ObjectId getChromosomeId() {
        return chromosomeId;
    }

    public void setChromosomeId(ObjectId chromosomeId) {
        this.chromosomeId = chromosomeId;
    }

    public ObjectId getParentId() {
        return parentId;
    }

    public void setParentId(ObjectId parentId) {
        this.parentId = parentId;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == this)
            return true;

        if(obj == null)
            return false;

        if(!(obj instanceof MEvaluated))
            return false;

        return ((MEvaluated)obj).getChromosomeId().equals(chromosomeId);
    }

    @Override
    public int hashCode() {
        return getChromosomeId().hashCode();
    }

    /*
    public Population getSurvivalPopulation() {
        return population;
    }

    public void setSurvivalPopulation(Population population) {
        this.population = population;
    }*/
    
    

}
