/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.constraint;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Constraint;
import uk.ac.ncl.icos.eaframework.grn.datatype.TimeCourseResult;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.synbad.core.util.Config;

import javax.sound.sampled.Line;

/**
 *
 * @author owengilfellon
 */
public class LinearityConstraint<T extends Chromosome> extends Constraint.AConstraint<TimeCourseResult<T>, Double> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LinearityConstraint.class);

    private final String INPUT;
    private final String OUTPUT;
    
    public LinearityConstraint(String input, String output) {
        this.INPUT = input;
        this.OUTPUT = output;
    }

    @Override
    public Config getConfig() {
        return Config.create()
                .addClassName(getClass().getName())
                .addString(INPUT)
                .addString(OUTPUT)
                .build();
    }

    @Override
    public Double apply(TimeCourseResult<T> cs) {
        
        ResponseCurve curve = cs.getResponseCurve(INPUT, OUTPUT);
        
        List<Double> inputs = curve.getInputs();
        List<Double> outputs = curve.getOutputs();

        double[] metaboliteAValues = new double[inputs.size()];
        double[] metaboliteBValues = new double[outputs.size()];

        for(int i = 0; i < inputs.size(); i++) {
            metaboliteAValues[i] = inputs.get(i);
            metaboliteBValues[i] = outputs.get(i);
        }

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("{}", Arrays.toString(metaboliteBValues));

        // Linearity calculated using Pearson's Correlation, disregarding 
        // negative correlation

        PearsonsCorrelation pc = new PearsonsCorrelation();
        double correlation = pc.correlation(metaboliteAValues, metaboliteBValues);

        correlation = correlation > 0.0 ? correlation : 0.0;
       
        return correlation;
    } 
}
