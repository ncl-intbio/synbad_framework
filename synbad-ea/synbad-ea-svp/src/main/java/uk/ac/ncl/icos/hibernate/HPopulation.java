/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="Population")
public class HPopulation implements Serializable {
    
    @Column(nullable = false)
    private int generation;
    
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<HEvaluated> chromosomes = new ArrayList<>();
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private HEvaluated bestChromosome;
    
    @Column(nullable = false)
    private int populationSize;
    
    @Column(nullable = false)
    private double meanFitness;
    
    @Column(nullable = false)
    private double bestFitness;
    
    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public HPopulation() {
    }

    public HPopulation(  int generation,
                        List<HEvaluated> population,
                        HEvaluated bestChromosome,
                        int populationSize,
                        double meanFitness,
                        double bestFitness) {
        this.generation = generation;
        this.chromosomes = population;
        this.bestChromosome = bestChromosome;
        this.populationSize = populationSize;
        this.meanFitness = meanFitness;
        this.bestFitness = bestFitness;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<HEvaluated> getPopulation() {
        return chromosomes;
    }

    public void setPopulation(List<HEvaluated> population) {
        this.chromosomes = population;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }
    
    public HEvaluated getBestChromosome() {
        return bestChromosome;
    }

    public void setBestChromosome(HEvaluated bestChromosome) {
        this.bestChromosome = bestChromosome;
    }

    public double getBestFitness() {
        return bestFitness;
    }

    public void setBestFitness(double bestFitness) {
        this.bestFitness = bestFitness;
    }

    public double getMeanFitness() {
        return meanFitness;
    }

    public void setMeanFitness(double meanFitness) {
        this.meanFitness = meanFitness;
    }
    
    public int getPopulationSize() {
        return populationSize;
    }

    public void setPopulationSize(int populationSize) {
        this.populationSize = populationSize;
    }
    
}
