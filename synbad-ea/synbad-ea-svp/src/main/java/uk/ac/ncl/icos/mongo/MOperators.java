package uk.ac.ncl.icos.mongo;

import java.util.List;

public class MOperators {

    private List<String> operators;
    private List<Double> weights;
    private String chromosomeClass;
    private int mutationsPerGeneration;
    private boolean sigmaScaled;

    public MOperators() {
    }

    public MOperators(List<String> operators, List<Double> weights, String chromosomeClass, int mutationsPerGeneration, boolean sigmaScaled) {
        this.operators = operators;
        this.weights = weights;
        this.chromosomeClass = chromosomeClass;
        this.mutationsPerGeneration = mutationsPerGeneration;
        this.sigmaScaled = sigmaScaled;
    }

    public int getMutationsPerGeneration() {
        return mutationsPerGeneration;
    }

    public void setMutationsPerGeneration(int mutationsPerGeneration) {
        this.mutationsPerGeneration = mutationsPerGeneration;
    }

    public List<Double> getWeights() {
        return weights;
    }

    public void setWeights(List<Double> weights) {
        this.weights = weights;
    }

    public List<String> getOperators() {
        return operators;
    }

    public void setOperators(List<String> operators) {
        this.operators = operators;
    }

    public String getChromosomeClass() {
        return chromosomeClass;
    }

    public void setChromosomeClass(String chromosomeClass) {
        this.chromosomeClass = chromosomeClass;
    }

    public boolean getSigmaScaled() {
        return sigmaScaled;
    }

    public void setSigmaScaled(boolean sigmaScaled) {
        this.sigmaScaled = sigmaScaled;
    }
}
