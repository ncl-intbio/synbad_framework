package uk.ac.ncl.icos.eaframework.grn.observer;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.mongo.*;

import java.io.Serializable;
import java.util.*;

import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 *
 * @author owengilfellon
 */
public class SvpMongoObserver<T extends Chromosome> implements EvolutionObserver<T>, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpMongoObserver.class);

    private final CodecRegistry pojoCodecRegistry;

    private final String MONGO_URL;
    private final String DB_NAME;
    private final String EXPERIMENT_NAME;

    private final String DEPENDENT_METABOLITE;

    private final String EXPERIMENTS_COL = "experiments";
    private final String CHROMOSOME_COL = "chromosomes";

    public SvpMongoObserver(String mongoUrl, String databaseName,  String experimentName, String dependentMetabolite) {

        this.MONGO_URL = mongoUrl;
        this.DB_NAME = databaseName;
        this.EXPERIMENT_NAME = experimentName;
        this.DEPENDENT_METABOLITE = dependentMetabolite;

        this.pojoCodecRegistry = fromRegistries(com.mongodb.MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    }

    @Override
    public <V extends EvoEngine<T>> void update(V s) {

        MongoClient mongoClient = MongoClients.create(MONGO_URL);
        MongoDatabase db = mongoClient.getDatabase(DB_NAME);

        // Create collections if they don't already exist

        if(!db.listCollectionNames().into(new ArrayList<>()).contains(EXPERIMENTS_COL))
            db.createCollection(EXPERIMENTS_COL);

        if(!db.listCollectionNames().into(new ArrayList<>()).contains(CHROMOSOME_COL))
            db.createCollection(CHROMOSOME_COL);

        MongoCollection<MExperiment> experimentCollection = db.getCollection(EXPERIMENTS_COL, MExperiment.class).withCodecRegistry(pojoCodecRegistry);
        MongoCollection<MEvaluated> chromosomeCollection = db.getCollection(CHROMOSOME_COL, MEvaluated.class).withCodecRegistry(pojoCodecRegistry);

        // Find existing experiment, if it exists, and do export

        FindIterable<MExperiment> experiment = experimentCollection.find(Filters.regex("name", ".*" + EXPERIMENT_NAME + ".*"), MExperiment.class);
        boolean newExperiment = !experiment.cursor().hasNext();

        SvpMongoExporter<T> exporter = new SvpMongoExporter<T>(EXPERIMENT_NAME, DEPENDENT_METABOLITE);
        MExport e = newExperiment ? exporter.export(s) : exporter.export(new MExport(Collections.EMPTY_SET, experiment.cursor().next()), s);

        // Upsert the experiment and chromosomes

        e.getChromosomes().forEach(ev -> chromosomeCollection.replaceOne(Filters.eq("_id", ev.getChromosomeId()), ev, new ReplaceOptions().upsert(true)));
        experimentCollection.replaceOne(Filters.regex("name", ".*" + EXPERIMENT_NAME + ".*"), e.getExperiment(), new ReplaceOptions().upsert(true));

        mongoClient.close();
    }
}
