/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="ConstraintResult")
public class HConstraintResult implements Serializable {
    
    @Column(name = "Name", nullable = false)
    private String name;
    
    @Column(name = "Evaulation", nullable = false)
    private Double evaluation;
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /*
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = ConstrainedFitness.class, optional = false)
    private ConstrainedFitness constrainedFitness;
*/
    public HConstraintResult() {
    }

    public HConstraintResult(String name, Double evaluation) {
        this.name = name;
        this.evaluation = evaluation;
    }

    public Double getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Double evaluation) {
        this.evaluation = evaluation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
/*
    public ConstrainedFitness getConstraintedFitness() {
        return constrainedFitness;
    }

    public void setConstraintedFitness(ConstrainedFitness constraintedFitness) {
        this.constrainedFitness = constraintedFitness;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
