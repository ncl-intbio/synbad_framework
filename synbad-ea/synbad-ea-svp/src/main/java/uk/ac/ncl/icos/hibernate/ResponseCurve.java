/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="ResponseCurve")
public class ResponseCurve implements Serializable {

    @Column(columnDefinition = "MEDIUMTEXT", nullable = false)
    private String responseCurve;
    
    @Column(nullable = false)
    private String independentMetabolite;
    
    @Column(nullable = false)
    private String dependentMetabolite;
    
    @Column(nullable = false)
    private double incrementSize;
    
    @Column(nullable = false)
    private int noOfIncrements;
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, precision = 5, scale = 0)
    private Long id;
    
    public ResponseCurve() {
    }

    public ResponseCurve(String timeCourse, String independent, String dependent, double increment, int noOfIncrements) {
        this.responseCurve = timeCourse;
        this.independentMetabolite = independent;
        this.dependentMetabolite = dependent;
        this.incrementSize = increment;
        this.noOfIncrements = noOfIncrements;
    }

    
    public String getResponseCurve() {
        return responseCurve;
    }

    public void setResponseCurve(String timecourse) {
        this.responseCurve = timecourse;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
