/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.hibernate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="Experiment")
public class HExperiment implements Serializable {
    
    @Column(name="Name")
    private String name;
    
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    private List<HPopulation> populations;
    
    @OneToOne(cascade = CascadeType.ALL)
    private HSelectionProcess reproductionProcess;
    
    @OneToOne(cascade = CascadeType.ALL)
    private HSelectionProcess survivalProcess;
    
    
    @Column(nullable = false)
    private int maxGenerations;
    


    @Column(nullable = false)
    private String independentMetabolite;
    
    @Column(nullable = false)
    private String dependentMetabolite;
    
    @Column(nullable = false)
    private double incrementSize;
    
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public HExperiment() {
    }

    public HExperiment(  String name, 
                        List<HPopulation> populations,
                        String dependentMetabolite,
                        String independentMetabolite,
                        HSelectionProcess reproductionProcess,
                        HSelectionProcess survivalProcess,
                        double incrementSize,
                        int maxGenerations) {
        this.name = name;
        this.populations = populations;
        this.dependentMetabolite = dependentMetabolite;
        this.independentMetabolite = independentMetabolite;
        this.incrementSize = incrementSize;
        this.maxGenerations = maxGenerations;
        this.reproductionProcess = reproductionProcess;
        this.survivalProcess = survivalProcess;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HPopulation> getPopulations() {
        return populations;
    }

    public void setPopulations(List<HPopulation> populations) {
        this.populations = populations;
    }

    public String getDependentMetabolite() {
        return dependentMetabolite;
    }

    public void setDependentMetabolite(String dependentMetabolite) {
        this.dependentMetabolite = dependentMetabolite;
    }

    public String getIndependentMetabolite() {
        return independentMetabolite;
    }

    public void setIndependentMetabolite(String independentMetabolite) {
        this.independentMetabolite = independentMetabolite;
    }

    public double getIncrementSize() {
        return incrementSize;
    }

    public void setIncrementSize(double incrementSize) {
        this.incrementSize = incrementSize;
    }

    public int getMaxGenerations() {
        return maxGenerations;
    }

    public void setMaxGenerations(int maxGenerations) {
        this.maxGenerations = maxGenerations;
    }

 
    public HSelectionProcess getReproductionProcess() {
        return reproductionProcess;
    }

    public void setReproductionProcess(HSelectionProcess reproductionProcess) {
        this.reproductionProcess = reproductionProcess;
    }
    
    

    public HSelectionProcess getSurvivalProcess() {
        return survivalProcess;
    }

    public void setSurvivalProcess(HSelectionProcess survivalProcess) {
        this.survivalProcess = survivalProcess;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
