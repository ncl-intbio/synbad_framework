/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.datatype;

import java.util.LinkedList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.ResponseCurve;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;

/**
 * Contains one or more TimeCourseTraces from a simulator run
 * @author owengilfellon
 */
public class TimeCourseResult<T extends Chromosome> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeCourseResult.class);
    
    private final List<TimeCourseTraces> traces;
    private final T chromosome;
    private final int generation;

    public TimeCourseResult(List<TimeCourseTraces> traces, T chromosome, int generation) {
        this.traces = new LinkedList<>(traces);
        this.chromosome = chromosome;
        this.generation = generation;
    }

    public T getChromosome() {
        return chromosome;
    }

    public int getGeneration() {
        return generation;
    }

    public List<TimeCourseTraces> getResults() {
        return traces;
    }
    
    /**
     * 
     * @param input
     * @param output
     * @param step The step at which to generate the response curve, null if any
     * TimeCourseTrace do not have enough steps.
     * @return 
     */
    public ResponseCurve getResponseCurve(String input, String output, int step) {
        
        List<Double> inputValues = new LinkedList<>();
        List<Double> outputValues = new LinkedList<>();
        
        for(TimeCourseTraces t : this.traces) {
            
            TimeCourseTrace inputTrace = t.getTimeCourse(input);
            TimeCourseTrace outputTrace = t.getTimeCourse(output);
            
            if(inputTrace == null) {
                LOGGER.error("Could not find traces for {}", input);
                return null;
            } else if(outputTrace == null) {
                LOGGER.error("Could not find traces for {}", output);
                return null;
            } else if(step > inputTrace.size()) {
                LOGGER.error("There is no step {}", step);
                return null;
            } else if (inputTrace.isEmpty() || outputTrace.isEmpty()) {
                LOGGER.error("Traces are empty");
                return null;
            }
            
            inputValues.add(inputTrace.get(step != -1 ? step - 1 : inputTrace.size() - 1));
            outputValues.add(outputTrace.get(step != -1 ? step - 1 : outputTrace.size() - 1));
        }
        
        return new ResponseCurve(input, output, inputValues, outputValues);
    }
    
    
    /**
     * Returns last step of the simulation which was 
     * @param input
     * @param output
     * @return 
     */
    public int getLastFullStep(String input) {

        int lowestIndex = -1;
        
        for(int i = 0; i < traces.size(); i++) {
            
            TimeCourseTrace inputTrace = traces.get(i).getTimeCourse(input);
            
            if(inputTrace == null) {
                LOGGER.error("Could not find traces for {}", input);
                return -1;
            }
            
            if(lowestIndex == -1 || inputTrace.size() < lowestIndex) {
                lowestIndex = inputTrace.size();
            }
        }
        
        return lowestIndex;
    }
    
    public ResponseCurve getResponseCurve(String input, String output) {
        return getResponseCurve(input, output, -1);
    }
    
    
}
