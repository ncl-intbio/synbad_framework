/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.chromosome;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.EvaluatedChromosomeFactory;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.CFitness;

/**
 *
 * @author owengilfellon
 */
public class EvaluatedSvpChromFactory<T extends Chromosome> implements EvaluatedChromosomeFactory<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(EvaluatedSvpChromFactory.class);
    
    @Override
    public EvaluatedChromosome<T> getEvaluated(T chromosome, Fitness fitness) {
        EvaluatedChromosome<T> ec;
               
        if(CFitness.class.isAssignableFrom(fitness.getClass()))
            ec = new EvaluatedSvpChromosome<>(chromosome, fitness,  ((CFitness)fitness).getResult(),// ((CFitness)fitness).getSimulationResults(),
                    null);
        else {
            LOGGER.warn("Fitness is not CFitness");
            ec = new EvaluatedChromosome<>(chromosome, fitness, null);
        }
        
        return ec;
    }
}
