/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.fitness;

import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.Evaluator;

import java.io.Serializable;
import uk.ac.ncl.icos.eaframework.grn.simulator.SBSbmlSimulatorFactory;

/**
 * 
 * @author owengilfellon
 */

public abstract class GRNSimulationEvaluator<T extends Chromosome> implements Evaluator<T>, Serializable {

    protected final SBSbmlSimulatorFactory cs;
    private final int duration;
    private final int runtime;

    GRNSimulationEvaluator(SBSbmlSimulatorFactory cs, int duration, int runtime) {
        this.cs = cs;
        this.duration = duration;
        this.runtime = runtime;
    }


    public SBSbmlSimulatorFactory getSimulatorFactory() {
        return cs;
    }

    public int getDuration() {
        return duration;
    }

    public int getRuntime() {
        return runtime;
    }

    @Override
    abstract public <V extends Fitness> V evaluate(T c);
    
}
