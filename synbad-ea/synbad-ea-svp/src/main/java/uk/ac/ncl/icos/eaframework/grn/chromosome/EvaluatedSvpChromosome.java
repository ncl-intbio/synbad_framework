/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.eaframework.grn.chromosome;

import org.bson.types.ObjectId;
import uk.ac.ncl.icos.datatypes.Fitness;
import uk.ac.ncl.icos.eaframework.Operator;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import uk.ac.ncl.icos.eaframework.Chromosome;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;

/**
 *
 * @author owengilfellon
 */
public class EvaluatedSvpChromosome<T extends Chromosome> extends EvaluatedChromosome<T> {

    private final List<CResult> constraintResults;

    public EvaluatedSvpChromosome(T c, Fitness f, Operator o) {
        super(c, f, o);
        this.constraintResults = Collections.EMPTY_LIST;
    }
    
    public EvaluatedSvpChromosome(T c, Fitness f, List<CResult> constraintResults, Operator o) {
        super(c, f, o);
        this.constraintResults = constraintResults;
    }

    public List<CResult> getConstraintResults() {
        return constraintResults;
    }

    public EvaluatedSvpChromosome<T> duplicate(ObjectId id, ObjectId[] parentIds) {
        return new EvaluatedSvpChromosome<>((T)c.duplicate(id, parentIds), getFitness(), new LinkedList<>(constraintResults), o);
    }

    public EvaluatedChromosome<T> duplicate() {
        return new EvaluatedSvpChromosome<>((T)c, getFitness(), new LinkedList<>(constraintResults), o);
    }
}
