/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
@Entity
@Table(name="ConstrainedFitness")
public class MFitness implements Serializable {
    
    private Double fitness;
    
    private List<MConstraintResult> constraintResult;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false, precision = 5, scale = 0)
    private Long id;

    public MFitness() {
    }

    public MFitness(Double fitness, List<MConstraintResult> result) {
        this.fitness = fitness;
        this.constraintResult = result;
    }

    public Double getFitness() {
        return fitness;
    }

    public void setFitness(Double fitness) {
        this.fitness = fitness;
    }

    public List<MConstraintResult> getResult() {
        return constraintResult;
    }

    public void setResult(List<MConstraintResult> result) {
        this.constraintResult = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
