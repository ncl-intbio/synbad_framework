package uk.ac.ncl.icos.mongo;

import org.apache.commons.lang3.ArrayUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.eaframework.*;
import uk.ac.ncl.icos.eaframework.chromosome.EvaluatedChromosome;
import uk.ac.ncl.icos.eaframework.grn.chromosome.EvaluatedSvpChromosome;
import uk.ac.ncl.icos.eaframework.grn.fitness.ASbmlConstraintEvaluator;
import uk.ac.ncl.icos.eaframework.grn.fitness.CResult;
import uk.ac.ncl.icos.eaframework.grn.fitness.RCCFitness;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTrace;
import uk.ac.ncl.icos.eaframework.grn.simulator.results.TimeCourseTraces;
import uk.ac.ncl.icos.eaframework.operator.OperatorGroup;
import uk.ac.ncl.icos.eaframework.population.OrderedEvaluationProcess;
import uk.ac.ncl.icos.eaframework.population.OrderedSelectAndMutateProcess;
import uk.ac.ncl.icos.eaframework.stats.PopulationStats;
import uk.ac.ncl.icos.eaframework.terminationconditions.TerminationGroup;
import uk.ac.ncl.icos.synbad.core.util.Config;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SvpMongoExporter<T extends Chromosome> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SvpMongoExporter.class);

    private final String EXPERIMENT_NAME;

    private final String DEPENDENT_METABOLITE;

    private final String EXPERIMENTS_COL = "experiments";
    private final String CHROMOSOME_COL = "chromosomes";

    public SvpMongoExporter(String experimentName, String dependentMetabolite) {

        this.EXPERIMENT_NAME = experimentName;
        this.DEPENDENT_METABOLITE = dependentMetabolite;
    }

    public <V extends EvoEngine<T>> MExport export(V s) {

        final MExperiment exp = new MExperiment();

        // Create new experiment if it did not already exist

        exp.setName(EXPERIMENT_NAME);
        exp.setPopulations(new ArrayList<>());
        exp.setThreads(s.getThreadPoolSize());
        exp.setOverlapping(Strategy.Overlapping.class.isAssignableFrom(s.getEvolutionaryStrategy().getClass()));

        exp.setReproductionProcess(getSelection(s.getEvolutionaryStrategy().getReproduction()));
        exp.setSurvivalProcess(getSelection(s.getEvolutionaryStrategy().getSurvival()));

        exp.setOrdered(isOrdered(s));
        exp.setMinimiseFitnessFunc(s.isFitnessFunctionMinimiser());

        exp.setEvaluator(getEvaluator(s));
        exp.setTerminationConditions(getTerminationConditions(new LinkedList<>(),
                s.getTerminationCondition()).stream().map(this::convertConfig)
                .collect(Collectors.toList()));


        return export(new MExport(Collections.EMPTY_LIST, exp), s);
    }

    <V extends EvoEngine<T>> boolean isOrdered(V s) {
        Class reproClazz = s.getEvolutionaryStrategy().getReproduction().getClass();
        Class survClazz = s.getEvolutionaryStrategy().getSurvival().getClass();
        return OrderedSelectAndMutateProcess.class.isAssignableFrom(reproClazz) ||
                OrderedSelectAndMutateProcess.class.isAssignableFrom(survClazz) ||
                OrderedEvaluationProcess.class.isAssignableFrom(reproClazz) ||
                OrderedEvaluationProcess.class.isAssignableFrom(survClazz);
    }

    public  <V extends EvoEngine<T>> MExport export(MExport export, V s) {

        // Export most recent population

        MExperiment exp = export.getExperiment();
        PopulationStats<T> stats = s.getPopulationStats();

        List<MEvaluated> reproductionChromosomes = s.getEvaluatedReproductionPopulation().stream()
                .map(this::getChromosome).collect(Collectors.toList());
        List<MEvaluated> survivalChromosomes = new ArrayList<>();

        double bestFitness = 0.0;
        MEvaluated bestChromosome = null;

        for(EvaluatedChromosome<T> ec : s.getEvaluatedSurvivalPopulation()) {

            MEvaluated chromosome = getChromosome(ec);
            chromosome.setResponseCurve(getResponseCurve(ec));
            if(bestFitness < chromosome.getFitness().getFitness()) {
                bestChromosome = chromosome;
                bestFitness = chromosome.getFitness().getFitness();
            }
            survivalChromosomes.add(chromosome);
        }


        MPopulation pop = new MPopulation();
        pop.setSurvivalPopulation(survivalChromosomes.stream().map(MEvaluated::getChromosomeId).collect(Collectors.toList()));
        pop.setReproductionPopulation(reproductionChromosomes.stream().map(MEvaluated::getChromosomeId).collect(Collectors.toList()));

        if(bestChromosome != null) {
            pop.setBestChromosome(bestChromosome.getChromosomeId());
        }

        pop.setMeanFitness(stats.getMeanFitness());
        pop.setBestFitness(stats.getBestFitness());
        pop.setGeneration(stats.getCurrentGeneration());
        pop.setPopulationSize(survivalChromosomes.size());
        pop.setDuration(stats.getDuration());

        // Add most recent population to experiment

        List<MPopulation> population = new ArrayList<>(exp.getPopulations());
        population.add(pop);
        exp.setPopulations(population);

        // Pre-existing chromosomes plus current

        Collection<MEvaluated> chromosomes = Stream.concat(export.getChromosomes().stream(),
                Stream.concat(reproductionChromosomes.stream(), survivalChromosomes.stream())).collect(Collectors.toList());

        return new MExport(chromosomes, exp);
    }

    public <V extends EvoEngine<T>> MEvaluator getEvaluator(V s) {

        MEvaluator ev = new MEvaluator();

        Evaluator<T> evaluator = s.getFitnessEvaluator();
        if(ASbmlConstraintEvaluator.class.isAssignableFrom(evaluator.getClass())) {
            ASbmlConstraintEvaluator<T> e = (ASbmlConstraintEvaluator<T>) evaluator;
            ev.setConstraintHandler(e.getHandler().getClass().getName());

            ev.setConstraints(e.getConstraints().stream().map(c -> convertConfig(c.getConfig())).collect(Collectors.toList()));
            ev.setEvaluatorClass(e.getClass().getName());
            ev.setSimulatorConfig(convertConfig(e.getSimulatorFactory().getSimulator(e.getDuration(), e.getRuntime()).getConfig()));

            ev.setSbmlModifiers(Stream.of(e.getModifiers()).map(mod -> mod.getClass().getName()).collect(Collectors.toList()));

            ev.setDuration(e.getDuration());
            ev.setRunTime(e.getRuntime());

            return ev;
        } else {
            throw new IllegalArgumentException("Only ASbmlConstraintEvaluators supported, cannot process " + evaluator.getClass().getName());
        }
    }



    private String getResponseCurve(EvaluatedChromosome<T> ec) {
        if(RCCFitness.class.isAssignableFrom(ec.getFitness().getClass())) {
            List<TimeCourseTraces> ts =  ((RCCFitness)ec.getFitness()).getSimulationResults();
            List<TimeCourseTrace> metaboliteRuns = ts.stream().map(re -> re.getTimeCourse(DEPENDENT_METABOLITE)).collect(Collectors.toList());
            StringBuilder sb = new StringBuilder();
            if(metaboliteRuns.size() > 1) {
                for(TimeCourseTrace run : metaboliteRuns) {
                    sb.append(run.get(run.size()-1)).append(" ");
                }
            } else if (!metaboliteRuns.isEmpty() && metaboliteRuns.get(0) != null){
                String s = Arrays.toString(metaboliteRuns.get(0).toArray()).replaceAll(",", "");
                sb.append(s, 1, s.length() - 1);
            }

            return sb.toString();
        }

        return "";
    }

    private MEvaluated getChromosome(EvaluatedChromosome<T> ec) {
        MFitness f = new MFitness();

        if(EvaluatedSvpChromosome.class.isAssignableFrom(ec.getClass())) {
            List<MConstraintResult> r = ((EvaluatedSvpChromosome<T>)ec).getConstraintResults().stream().map((CResult cr) ->  {
                MConstraintResult result = new MConstraintResult();
                result.setName(cr.getName());
                result.setEvaluation(cr.getEvaluation());
                //  result.setConstraintedFitness(f);
                return result;
            }).collect(Collectors.toList());
            f.setResult(r);
        }

        f.setFitness(ec.getFitness().getFitness());

        MEvaluated chromosome = new MEvaluated();
        String svpWriteString = ec.getChromosome().toString();
        chromosome.setExperimentName(EXPERIMENT_NAME);
        chromosome.setSvpwrite(svpWriteString.isEmpty() ? svpWriteString : svpWriteString.substring(0, svpWriteString.length()-2));
        chromosome.setFitness(f);
        chromosome.setChromosomeId(ec.getChromosome().getId());
        if(ec.getChromosome().getParentIds() == null) {
            LOGGER.error("Parent is null: {}", svpWriteString);
        }
        try {
            chromosome.setParentId(ec.getChromosome().getParentIds()[0]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            LOGGER.error("Parent is null: {}", svpWriteString);
        }
        return chromosome;
    }

    private MPopulationProcess getSelection(PopulationProcess<T> process) {

        MPopulationProcess selectionProcess = new MPopulationProcess();

        selectionProcess.setSelectionConfig(convertConfig(process.getSelection().getConfig()));
        selectionProcess.setPopulationSize(process.getPopulationSize());
        selectionProcess.setClassName(process.getClass().getName());

        if(process.getOperator() != null && OperatorGroup.class.isAssignableFrom(process.getOperator().getClass()))
        {
            OperatorGroup<T> group = ((OperatorGroup<T>)process.getOperator());

            List<String> operatorList = getOperatorsFromProcess(process).stream()
                    .map(Object::getClass)
                    .map(Class::getName).collect(Collectors.toList());

            MOperators operators = new MOperators(
                    operatorList,
                    null,
                    group.getChromosomeClass().getName(),
                    group.getMutationsPerGeneration(),
                    group.isSigmaScaled());

            double[] weights = group.getWeights();

            if(weights != null)
                operators.setWeights(Arrays.asList(ArrayUtils.toObject(group.getWeights())));

            selectionProcess.setOperators(operators);
        }

        return selectionProcess;
    }

    private List<Operator<T>> getOperatorsFromProcess(PopulationProcess<T> process) {
        if(process.getOperator() == null || !(OperatorGroup.class.isAssignableFrom(process.getOperator().getClass())))
            return Collections.EMPTY_LIST;

        return  ((OperatorGroup)process.getOperator()).getOperators();
    }

    private List<Config> getTerminationConditions(List<Config> configs, TerminationCondition condition) {
        if(TerminationGroup.class.isAssignableFrom(condition.getClass())) {
            TerminationGroup tg = (TerminationGroup) condition;
            for(TerminationCondition tc: tg.getConditions()) {
                getTerminationConditions(configs, tc);
            }
        } else {
            configs.add(condition.getConfig());
        }

        return configs;
    }

    private Document convertConfig(Config config) {
        Document d = new Document()
                .append("className", config.getClassName())
                .append("parameters", config.getParameters());
        return d;
    }
}
