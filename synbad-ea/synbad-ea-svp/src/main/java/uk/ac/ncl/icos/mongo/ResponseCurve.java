/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.mongo;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author owengilfellon
 */
public class ResponseCurve implements Serializable {

    private String responseCurve;

    private String independentMetabolite;

    private String dependentMetabolite;

    private double incrementSize;

    private int noOfIncrements;

    private Long id;
    
    public ResponseCurve() { }

    public ResponseCurve(String timeCourse, String independent, String dependent, double increment, int noOfIncrements) {
        this.responseCurve = timeCourse;
        this.independentMetabolite = independent;
        this.dependentMetabolite = dependent;
        this.incrementSize = increment;
        this.noOfIncrements = noOfIncrements;
    }

    public String getResponseCurve() {
        return responseCurve;
    }

    public void setResponseCurve(String timecourse) {
        this.responseCurve = timecourse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
