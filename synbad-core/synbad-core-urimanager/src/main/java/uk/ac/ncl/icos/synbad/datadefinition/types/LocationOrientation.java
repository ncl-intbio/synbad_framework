/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;

/**
 *
 * @author owengilfellon
 */
 public enum LocationOrientation implements SBDataDef {
        
    INLINE("http://sbols.org/v2#inline"),
    REVERSE_COMPLIMENT("http://sbols.org/v2#reverseCompliment");

    private final URI uri;

    private LocationOrientation(String uri) {
        this.uri = URI.create(uri);
    }

    @Override
    public URI getUri() {
        return uri;
    }

}
