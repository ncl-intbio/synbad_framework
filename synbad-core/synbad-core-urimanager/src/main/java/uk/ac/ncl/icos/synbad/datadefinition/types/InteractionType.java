/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBNamedDef;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */

public enum InteractionType implements Type, SBNamedDef {
    
    ComplexFormation(UriHelper.vpr.namespacedUri("Complex+formation").toASCIIString()),
    ComplexDisassociation(UriHelper.vpr.namespacedUri("Complex+disassociation").toASCIIString()),
    
    TranscriptionalRepression(UriHelper.vpr.namespacedUri("Transcriptional+repression").toASCIIString()),
    TranscriptionActivation(UriHelper.vpr.namespacedUri("Transcriptional+activation").toASCIIString()),
    TranscriptionalRepressionUsingOperator(UriHelper.vpr.namespacedUri("Transcriptional+repression+using+an+operator").toASCIIString()),
    TranscriptionActivationUsingOperator(UriHelper.vpr.namespacedUri("Transcriptional+activation+using+an+operator").toASCIIString()),



    PoPSProduction(UriHelper.vpr.namespacedUri("PoPS+Production").toASCIIString()),
    OperatorPoPSModulation(UriHelper.vpr.namespacedUri("OperatorPoPSModulation").toASCIIString()),
    PoPSModulation(UriHelper.vpr.namespacedUri("PoPSModulation").toASCIIString()),
    RiPSProduction(UriHelper.vpr.namespacedUri("RiPS+Production").toASCIIString()),
    RiPSModulation(UriHelper.vpr.namespacedUri("RiPS+Modulation").toASCIIString()),
    ProteinProduction(UriHelper.vpr.namespacedUri("Protein+Production").toASCIIString()),
    mRNAProduction(UriHelper.vpr.namespacedUri("Translation").toASCIIString()),
    mRNAModulation(UriHelper.vpr.namespacedUri("mRNA+Modulation").toASCIIString()),
   
    Phosphorylation(UriHelper.vpr.namespacedUri("Phosphorylation").toASCIIString()),
    Dephosphorylation(UriHelper.vpr.namespacedUri("Dephosphorylation").toASCIIString()),
    
    Degradation(UriHelper.vpr.namespacedUri("Degradation").toASCIIString()),
    
    MetabolicReaction(UriHelper.vpr.namespacedUri("Metabolic+Reaction").toASCIIString());

    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private InteractionType(String uri) {
        this.uri = URI.create(uri); 
    }

    @Override
    public String getPrettyName() {
        String path = getUri().getPath();
        path = path.substring(1, path.length());
        path = path.replaceAll("\\+", " ");
        return path;
    }
}
