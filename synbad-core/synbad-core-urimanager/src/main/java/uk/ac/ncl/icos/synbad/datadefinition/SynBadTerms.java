/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

public class SynBadTerms {
    
    public static class Rdf {
        public static final String hasType = UriHelper.rdf.namespacedUri("type").toASCIIString();
    }
    
    public static class RdfType {
        public static final String stringLiteral = "http://www.w3.org/2001/XMLSchema#string";
        public static final String integerLiteral = "http://www.w3.org/2001/XMLSchema#integer";
        public static final String doubleLiteral = "http://www.w3.org/2001/XMLSchema#double";
        public static final String booleanLiteral = "http://www.w3.org/2001/XMLSchema#boolean";
    }

    public static class SbolIdentified {
        public static final String hasName = UriHelper.dcterms.namespacedUri("title").toASCIIString();
        public static final String hasDescription = UriHelper.dcterms.namespacedUri("description").toASCIIString();
        public static final String wasDerivedFrom = UriHelper.prov.namespacedUri("wasDerivedFrom").toASCIIString();
        public static final String hasVersion = "http://sbols.org/v2#version";
        public static final String hasPersistentIdentity = "http://sbols.org/v2#persistentIdentity";
        public static final String hasDisplayId = "http://sbols.org/v2#displayId";
        //public static final String entitytype = UriHelper.synbad.namespacedUri("entitytype");
    }
    
    public static class Sbol {
        public static final String role = "http://sbols.org/v2#role";
        public static final String type = "http://sbols.org/v2#type";
         public static final String definedBy = "http://sbols.org/v2#definition";
    }
    
    public final static class SbolComponent {
        public static final String hasSequence = "http://sbols.org/v2#sequence";
        public static final String hasType = "http://sbols.org/v2#type";
        public static final String hasRole = "http://sbols.org/v2#role";
        public static final String hasComponent = "http://sbols.org/v2#component";
        public static final String hasSequenceAnnotation = "http://sbols.org/v2#sequenceAnnotation";
        public static final String hasSequenceConstraint = "http://sbols.org/v2#sequenceConstraint";
        public static final String hasAccess = "http://sbols.org/v2#access";
        
    }
    
    public static class SbolFunctionalComponent {
        public static final String hasDirection = "http://sbols.org/v2#direction";
    }
    
    public static class SbolModule {
         public static final String hasModel = "http://sbols.org/v2#model";
        public static final String hasModule = "http://sbols.org/v2#module";
        public static final String hasFunctionalComponent = "http://sbols.org/v2#functionalComponent";
        public static final String hasInteraction = "http://sbols.org/v2#interaction";
    }
    
    public static class SbolInteraction {
        public static final String hasParticipation = "http://sbols.org/v2#participation";
        public static final String participant = "http://sbols.org/v2#participant";
    }
    
    public static class SbolSequence {
        public static final String hasElements = "http://sbols.org/v2#elements";
        public static final String hasEncoding = "http://sbols.org/v2#sequence";
    }

    public static class SbolSequenceConstraint {
        public static final String hasRestriction = "http://sbols.org/v2#restriction";
        public static final String hasSubject = "http://sbols.org/v2#subject";
        public static final String hasObject = "http://sbols.org/v2#object";
    }
    
    public static class SbolSequenceAnnotation {
        public static final String hasLocation = "http://sbols.org/v2#location";
        public static final String positionsComponent = "http://sbols.org/v2#component";
    }
    
    public static class SbolLocation {
        public static final String hasOrientation = "http://sbols.org/v2#orientation";
        public static final String rangeStart = "http://sbols.org/v2#start";
        public static final String rangeEnd = "http://sbols.org/v2#end";
        public static final String cutAt = "http://sbols.org/v2#at";
    }
    
     public static class SbolCollecton {
        public static final String hasMember = "http://sbols.org/v2#member";
    }
    
    public static class SbolMapsTo {
        public static final String hasMapsTo = "http://sbols.org/v2#mapsTo";
        public static final String hasRefinement = "http://sbols.org/v2#refinement";
        public static final String hasLocalInstance = "http://sbols.org/v2#local";
        public static final String hasRemoteInstance = "http://sbols.org/v2#remote";
    }
 
     public static class SbolModel {
        public static final String source = "http://sbols.org/v2#source";
        public static final String language = "http://sbols.org/v2#language";
        public static final String framework = "http://sbols.org/v2#framework";
    }
     
    public static class SynBadSystem {
        public static final String dependantOn = UriHelper.synbad.namespacedUri("dependantOn").toASCIIString();
        public static final String owns = UriHelper.synbad.namespacedUri("owns").toASCIIString();
        public static final String outputOf = UriHelper.synbad.namespacedUri("outputOf").toASCIIString();
    }

    public static class SynBadEntity {
        public static final String extensionOf = UriHelper.synbad.namespacedUri("extends").toASCIIString();
        public static final String hasPort = UriHelper.synbad.namespacedUri("hasPort").toASCIIString();
        public static final String hasWire = UriHelper.synbad.namespacedUri("hasWire").toASCIIString();
        public static final String hasChild = UriHelper.synbad.namespacedUri("hasChild").toASCIIString();
    }
    
    public static class SynBadHelper {
        public static final String exported = UriHelper.synbad.namespacedUri("exported").toASCIIString();
        public static final String imported = UriHelper.synbad.namespacedUri("imported").toASCIIString();
    }
    
    public static class SynBadInstance {
        public static final String definedBy = "http://sbols.org/v2#definition";
    }
    
    public static class SynBadModule{
        public static final String instancedTuOf = UriHelper.synbad.namespacedUri("instancedTuOf").toASCIIString();
    }
    
    public static class SynBadPart {
        public static final String hasInteraction = "http://virtualparts.org/externalInteraction";
        public static final String hasInternalInteraction = "http://virtualparts.org/internalInteraction";
        public static final String hasInternalSpecies = "http://virtualparts.org/internalSpecies";
        public static final String hasProperty = "http://virtualparts.org/property";
    }
    
    public static class SynBadInteraction{
        public static final String hasParticipant ="http://www.synbad.org/hasParticipant";
        public static final String hasComponent = UriHelper.synbad.namespacedUri("hasComponent").toASCIIString();
        public static final String hasParameter = "http://virtualparts.org/parameter";
        public static final String isSvpParticipant = UriHelper.synbad.namespacedUri("isSvpParticipant").toASCIIString();;
    }
    
    public static class SynBadParticipant{
        public static final String isSbolParticipant = UriHelper.synbad.namespacedUri("isSbolParticipant").toASCIIString();
        public static final String interactionRole = UriHelper.synbad.namespacedUri("interactionRole").toASCIIString();
    }
    
    public static class SynBadTu{
        public static final String moduleType = UriHelper.synbad.namespacedUri("moduleType").toASCIIString();
    }
    
    
    public static class SynBadPort {
        public static final String isPublic = UriHelper.synbad.namespacedUri("portAccess").toASCIIString();
        public static final String proxies = UriHelper.synbad.namespacedUri("proxyOf").toASCIIString();
        public static final String hasDirection = UriHelper.synbad.namespacedUri("portDirection").toASCIIString();
        public static final String hasPortType = UriHelper.synbad.namespacedUri("portType").toASCIIString();
        public static final String hasPortState = UriHelper.synbad.namespacedUri("portState").toASCIIString();
        public static final String hasIdentityConstraint = UriHelper.synbad.namespacedUri("identityConstraint").toASCIIString();
    }
    
    public static class SynBadPortInstance {
        public static final String hasPortInstance = UriHelper.synbad.namespacedUri("portInstance").toASCIIString();
        public static final String hasDefinition = UriHelper.synbad.namespacedUri("portDefinition").toASCIIString();
    }
    
    public static class SynBadWire {
        public static final String isFrom = UriHelper.synbad.namespacedUri("isFrom").toASCIIString();
        public static final String isTo = UriHelper.synbad.namespacedUri("isTo").toASCIIString();
    }
    
    public static class VPR {

        public static final String organism = "http://virtualparts.org/organism";
        public static final String sequence = "http://virtualparts.org/sequence";
        public static final String sequenceUri = "http://virtualparts.org/sequenceUri";
        public static final String designMethod = "http://virtualparts.org/designMethod";
        public static final String metaType = "http://virtualparts.org/metaType";
        public static final String status = "http://virtualparts.org/status";
        public static final String partType = "http://virtualparts.org/partType";
        public static final String partTypeAsRole = "http://virtualparts.org/partTypeAsRole";
        public static final String interactionType = "http://virtualparts.org/interactionType";
        
        public static final String hasmRNA = "http://virtualparts.org/hasMrna";

// Part properties
        
        public static final String propertyName = "http://virtualparts.org/name";
        public static final String propertyValue = "http://virtualparts.org/value";
        public static final String propertydescription = "http://virtualparts.org/description";
        
        // Interaction parameter
        
        
        public static final String parameterName = "http://virtualparts.org/parameterName";
        public static final String parameterType = "http://virtualparts.org/parameterType";
        public static final String parameterValue = "http://virtualparts.org/parameterValue";
        public static final String parameterScope = "http://virtualparts.org/scope";
        public static final String parameterEvidence = "http://virtualparts.org/evidenceType";
        
        public static final String partDetailName = "http://virtualparts.org/partName";
        public static final String partDetailForm = "http://virtualparts.org/partForm";
        public static final String partDetailStoichiometry = "http://virtualparts.org/stoichiometry";
        public static final String partDetailMathName = "http://virtualparts.org/mathName";
        public static final String partDetailRole = "http://virtualparts.org/interactionRole";

        public static final String freeTextMath = "http://virtualparts.org/freeTextMath";
        public static final String mathName = "http://virtualparts.org/mathName";
        public static final String mathType = "http://virtualparts.org/mathType";
        
        public static final String isInternal = "http://virtualparts.org/isInternal";
        public static final String isReaction = "http://virtualparts.org/isReaction";
        public static final String isReversible = "http://virtualparts.org/isReversible";
        
        
    }

/*
    
    public static class Wire {
        public static final String from = UriHelper.synbad.namespacedUri("from");
        public static final String to = UriHelper.synbad.namespacedUri("to");
    }
    
    public static class Types {
        public static final String DOType = UriHelper.synbad.namespacedUri("dotype");
        public static final String entity = UriHelper.synbad.namespacedUri("entity");
        public static final String instance = UriHelper.synbad.namespacedUri("instance");
        public static final String wire = UriHelper.synbad.namespacedUri("wire");
        public static final String port = UriHelper.synbad.namespacedUri("port");
    }
    
    public static class Instance {
        public static final String portInstance = UriHelper.synbad.namespacedUri("portInstance");
    }
    
    public static class Port {
        
        public static final String defaultPort = UriHelper.synbad.namespacedUri("Port");
        public static final String proxyPort = UriHelper.synbad.namespacedUri("ProxyPort");
       
        public static final String version = UriHelper.synbad.namespacedUri("version");
        public static final String portDirection = UriHelper.synbad.namespacedUri("portDirection");
        public static final String proxies = UriHelper.synbad.namespacedUri("proxies");
        public static final String portAccess = UriHelper.synbad.namespacedUri("portAccess");
        public static final String portType = UriHelper.synbad.namespacedUri("portType");
        public static final String portState = UriHelper.synbad.namespacedUri("portState");
    }
    
    public static class ModelEdge {
        public static final String edgeType = UriHelper.synbad.namespacedUri("edgeType");
    }
    
    */

}
