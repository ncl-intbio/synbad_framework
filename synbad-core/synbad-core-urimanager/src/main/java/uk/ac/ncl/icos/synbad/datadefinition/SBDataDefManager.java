package uk.ac.ncl.icos.synbad.datadefinition;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class SBDataDefManager {

    private static SBDataDefManager DATA_DEF_MANAGER = null;
    private final Multimap<String, SBDataDef> idToDefinition = HashMultimap.create();
    private final Multimap<Class<?>, SBDataDef> classToDefinitions = HashMultimap.create();
    
    private static final Logger logger = LoggerFactory.getLogger(SBDataDefManager.class);

    private SBDataDefManager() {
        Lookup.getDefault().lookupAll(SBDataDefProvider.class).stream()
                .forEach(p -> p.addDefinitions(this));
    }

    public static SBDataDefManager getManager() {
        if(DATA_DEF_MANAGER == null)
            DATA_DEF_MANAGER = new SBDataDefManager();
        return DATA_DEF_MANAGER;
    }


    public void addDefinition(SBDataDef dataDefinition) {
        Class<?> clazz = dataDefinition.getClass();
       
        classToDefinitions.put(clazz, dataDefinition);

        idToDefinition.put(dataDefinition.getUri().toASCIIString(), dataDefinition);

        if(logger.isTraceEnabled()) {
            logger.trace("Added definition: [ {}:{} ]", clazz.getSimpleName(), dataDefinition);
        }
    }


    /**
     * Maps a string to a data definition. If an existing string is provided,
     * the mapping will be overwritten.
     * @param synonym The string to map to a definition
     * @param dataDefinition The data definition
     */
    public void addSynonym(String synonym, SBDataDef dataDefinition) {
        Class<?> clazz = dataDefinition.getClass();
        
        if(!classToDefinitions.containsKey(clazz)) {
            addDefinition(dataDefinition);
        }

        idToDefinition.put(synonym, dataDefinition);

        if(logger.isTraceEnabled())
            logger.trace("Added synonym [ {} ] for [ {} ]", synonym, dataDefinition);
    }


    public boolean containsIdentity(String id) {
        return idToDefinition.containsKey(id);
    }


    public <T> boolean containsDefinition(Class<T> clazz, String id) {
        
        return containsIdentity(id) &&
                idToDefinition.get(id).stream().anyMatch(def ->  clazz.isAssignableFrom(def.getClass()));
    }


    public <T> T getDefinition(Class<T> clazz, String identifier) {

        Collection<SBDataDef> dataDefinition = idToDefinition.get(identifier);
        
        if(dataDefinition.isEmpty())
            return null;

        if(dataDefinition.stream().anyMatch(d -> clazz.isAssignableFrom(d.getClass()))) {
            return clazz.cast(dataDefinition.stream().filter(d -> clazz.isAssignableFrom(d.getClass())).findFirst().orElse(null));
        }

        return null;
    }
    
    public <T> Set<T> getDefinition(Class<T> clazz) {
        
        Collection<SBDataDef> dataDefinitions = classToDefinitions.get(clazz);
        Set<T> toReturn = new HashSet<>();
        
        for(SBDataDef dataDefinition : dataDefinitions) {

            if(clazz.isAssignableFrom(dataDefinition.getClass())) {
                toReturn.add(clazz.cast(dataDefinition));
            }
        }
        
        return toReturn;
        
    }
    
}
