/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.EntityType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.LocationOrientation;
import uk.ac.ncl.icos.synbad.datadefinition.types.ModuleRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ParticipationRole;
import uk.ac.ncl.icos.synbad.servicemanager.SBService;

/**
 *
 * @author owengilfellon
 */
public interface SBDataDefProvider {
    
    public void addDefinitions(SBDataDefManager manager);
    
    @ServiceProvider( service = SBDataDefProvider.class)
    public class DefaultDefinitionProvider implements SBDataDefProvider {

        @Override
        public void addDefinitions(SBDataDefManager manager) {
            
            // Add defined terms
            
            for(ComponentRole role : ComponentRole.values()) {
                manager.addDefinition(role);
                manager.addSynonym(role.name(), role);
            }

            for(ComponentType type : ComponentType.values()) {
                manager.addDefinition(type);
                manager.addSynonym(type.name(), type);
            }
            
            for(ModuleRole role : ModuleRole.values()) {
                manager.addDefinition(role);
                manager.addSynonym(role.name(), role);
            }

            for(InteractionType role : InteractionType.values()) {
                manager.addDefinition(role);
                manager.addSynonym(role.name(), role);
            }
            
            for(EntityType type : EntityType.values()) {
                manager.addDefinition(type);
                manager.addSynonym(type.name(), type);
            }
          
            for(ConstraintRestriction r: ConstraintRestriction.values()) {
                manager.addDefinition(r);
                manager.addSynonym(r.name(), r);
            }
            
            for(LocationOrientation o: LocationOrientation.values()) {
                manager.addDefinition(o);
                manager.addSynonym(o.name(), o);
            }
            
            for(ParticipationRole r: ParticipationRole.values()) {
                manager.addDefinition(r);
                manager.addSynonym(r.name(), r);
            }
            
            // Add synonyms
            
            // VPR Part Types
            
            manager.addSynonym("Promoter", ComponentRole.Promoter);
            manager.addSynonym("RBS", ComponentRole.RBS);
            manager.addSynonym("FunctionalPart", ComponentRole.CDS);
            manager.addSynonym("Operator", ComponentRole.Operator);
            manager.addSynonym("Terminator", ComponentRole.Terminator);
            manager.addSynonym("Shim", ComponentRole.Shim);
            
            manager.addSynonym("Environment Constant", ComponentType.SmallMolecule);
            
            // VPR SBOL2.0 Interaction Types
            
            manager.addSynonym("Complex formation", InteractionType.ComplexFormation);
            manager.addSynonym("Transcriptional repression", InteractionType.TranscriptionalRepression);
            manager.addSynonym("Transcriptional repression using an operator", InteractionType.TranscriptionalRepressionUsingOperator);
            
            
            manager.addSynonym("Transcriptional activation", InteractionType.TranscriptionActivation);
            manager.addSynonym("Transcriptional activation using an operator", InteractionType.TranscriptionActivationUsingOperator);
             
            manager.addSynonym("Metabolic Reaction", InteractionType.MetabolicReaction);
            
             
            manager.addSynonym("PoPS Production", InteractionType.PoPSProduction);
            manager.addSynonym("Operator PoPS Production", InteractionType.PoPSProduction);
            manager.addSynonym("RiPS Production", InteractionType.RiPSProduction);
            manager.addSynonym("Phosphorylation", InteractionType.Phosphorylation);
            manager.addSynonym("Dephosphorylation", InteractionType.Dephosphorylation);
            manager.addSynonym("Degradation", InteractionType.Degradation);
            manager.addSynonym("OperatorPoPSModulation", InteractionType.OperatorPoPSModulation);
            manager.addSynonym("Protein Production", InteractionType.ProteinProduction);
        
        }
        
    }
    
}
