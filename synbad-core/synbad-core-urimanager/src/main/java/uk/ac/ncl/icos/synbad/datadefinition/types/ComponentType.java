package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public enum ComponentType implements Type {

    DNA(UriHelper.biopax.namespacedUri("DnaRegion")),
    mRNA(UriHelper.biopax.namespacedUri("RnaRegion")),
    Protein(UriHelper.biopax.namespacedUri("Protein")),
    Complex(UriHelper.biopax.namespacedUri("Complex")),
    SmallMolecule(UriHelper.biopax.namespacedUri("SmallMolecule"));

    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private ComponentType(URI uri) {
        this.uri = uri;
    }   

}
