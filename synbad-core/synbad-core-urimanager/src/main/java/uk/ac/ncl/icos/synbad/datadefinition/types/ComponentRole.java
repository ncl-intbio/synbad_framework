/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */

public enum ComponentRole implements Role {

    Promoter("http://identifiers.org/so/SO:0000167"),
    RBS("http://identifiers.org/so/SO:0000139"),
    CDS("http://identifiers.org/so/SO:0000316"),
    Terminator("http://identifiers.org/so/SO:0000141"),
    Operator("http://identifiers.org/so/SO:0000057"),
    Gene("http://identifiers.org/so/SO:0000704"),
    Shim("http://identifiers.org/so/SO:0000997"),
    mRNA("http://identifiers.org/so/SO:0000234"),
    RNA("http://identifiers.org/so/SO:0000356"),
    Effector("http://identifiers.org/chebi/CHEBI:35224"),
    Generic("http://identifiers.org/so/SO:0000110");
   
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private ComponentRole(String uri) {
        this.uri = URI.create(uri);
    }  

}

