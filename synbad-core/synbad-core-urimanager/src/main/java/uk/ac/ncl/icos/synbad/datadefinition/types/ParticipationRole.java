/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition.types;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public enum ParticipationRole implements Role {

    Input(UriHelper.synbad.namespacedUri("Input").toASCIIString()),
    Output(UriHelper.synbad.namespacedUri("Output").toASCIIString()),
    Modifier(UriHelper.synbad.namespacedUri("Modifer").toASCIIString());
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private ParticipationRole(String uri) {
        this.uri = URI.create(uri);
    }  

}

