/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.datadefinition;

import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.netbeans.junit.MockServices;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.InteractionType;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;

/**
 *
 * @author owengilfellon
 */
public class IDataDefinitionManagerTest {
    
    public IDataDefinitionManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of containsDefinition method, of class IDataDefinitionManager.
     */
    @org.junit.Test
    public void testContainsDefinition() {
        
        // Set up definition manager
        
        SBDataDefManager definitionManager = SBDataDefManager.getManager();
        
        // Get some ids to work with
        
        Set<String> uris = new HashSet<>();
        
        for(Role role : ComponentRole.values()) {
            uris.add(role.getUri().toASCIIString());
        }
        
        for(Type role : InteractionType.values()) {
            uris.add(role.getUri().toASCIIString());
        }

        // Test retrieval of constants using synonyms

        assert(definitionManager.getDefinition(ComponentRole.class, "Promoter") == ComponentRole.Promoter);
        assert(definitionManager.getDefinition(ComponentRole.class, "RBS") == ComponentRole.RBS);
        assert(definitionManager.getDefinition(ComponentRole.class, "FunctionalPart") == ComponentRole.CDS);

            
        assert(definitionManager.getDefinition(InteractionType.class, "Phosphorylation") == InteractionType.Phosphorylation);
        assert(definitionManager.getDefinition(InteractionType.class, "PoPS Production") == InteractionType.PoPSProduction);
           
    }

    
}
