package uk.ac.ncl.icos.synbad.core.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Config {

    private final List<Object> parameters;
    private final String className;

    public static ConfigBuilder create() {
        return new ConfigBuilder();
    }

    private Config(String className, List<Object> parameters) {
        this.className = className;
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Config config = (Config) o;
        if(!className.equals(config.className))
            return false;
        if(config.getParameters().size() != parameters.size())
            return false;
        for(int i = 0; i < parameters.size(); i++) {
            if(parameters.get(i) != config.parameters.get(i))
                return false;
        }
        return true;
    }

    public static class MappedPair {

        private String key;
        private Double value;

        public MappedPair(String key, Double value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MappedPair that = (MappedPair) o;
            return Objects.equals(key, that.key) &&
                    Objects.equals(value, that.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(className) * Objects.hash(parameters.toArray(new Object[]{}));
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public String getClassName() {
        return className;
    }

    public static class ConfigBuilder {

        private final List<Object> parameters;
        private final String className;

        private ConfigBuilder() {
            className = "";
            parameters = new LinkedList<>();
        }

        private ConfigBuilder(String className, Collection<Object> parameters) {
            this.className = className;
            this.parameters = new LinkedList<>(parameters);
        }

        public ConfigBuilder addClassName(String className) {
            return new ConfigBuilder(className, parameters);
        }

        public ConfigBuilder addString(String string) {
            List<Object> newParameters = new LinkedList<>(parameters);
            newParameters.add(string);
            return new ConfigBuilder(className, newParameters);
        }

        public ConfigBuilder addInt(Integer integer) {
            List<Object> newParameters = new LinkedList<>(parameters);
            newParameters.add(integer);
            return new ConfigBuilder(className, newParameters);
        }

        public ConfigBuilder addDouble(Double d) {
            List<Object> newParameters = new LinkedList<>(parameters);
            newParameters.add(d);
            return new ConfigBuilder(className, newParameters);
        }

        public ConfigBuilder addBoolean(Boolean b) {
            List<Object> newParameters = new LinkedList<>(parameters);
            newParameters.add(b);
            return new ConfigBuilder(className, newParameters);
        }

        public ConfigBuilder addMappedPair(String key, Double value) {
            List<Object> newParameters = new LinkedList<>(parameters);
            newParameters.add(new MappedPair(key, value));
            return new ConfigBuilder(className, newParameters);
        }

        public ConfigBuilder addConfig(Config c) {
            List<Object> newParameters = new LinkedList<>(parameters);
            newParameters.add(c);
            return new ConfigBuilder(className, newParameters);
        }

        public Config build() {
            return new Config(className, new LinkedList<>(parameters));
        }
    }
}
