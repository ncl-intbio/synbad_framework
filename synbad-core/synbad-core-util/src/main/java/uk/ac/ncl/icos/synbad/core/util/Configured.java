package uk.ac.ncl.icos.synbad.core.util;

public interface Configured {

    Config getConfig();
}
