/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author owengilfellon
 */
public class IdentityTest {
    
    private final String PREFIX = "www.synbad.org";
    private final String PARENT_DISPLAY_ID = "parentDisplayId";
    private final String DISPLAY_ID = "displayId";
    private final String VERSION = "1.0";
    private final String INCREMENT = "1";
    
    private final URI NO_VERSION = URI.create(PREFIX.concat("/").concat(DISPLAY_ID));
    private final URI WITH_PARENT_NO_VERSION = URI.create(PREFIX.concat("/").concat(PARENT_DISPLAY_ID).concat("/").concat(DISPLAY_ID));
    private final URI WITH_VERSION = URI.create(PREFIX.concat("/").concat(DISPLAY_ID).concat("/").concat(VERSION));
    private final URI WITH_PARENT_AND_VERSION = URI.create(PREFIX.concat("/").concat(PARENT_DISPLAY_ID).concat("/").concat(DISPLAY_ID).concat("/").concat(VERSION));
    
    public IdentityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getIdentity method, of class SBIdentity.
     */
    @Test
    public void testGetIdentity() {
        SBIdentity no_version = SBIdentity.getIdentity(PREFIX, DISPLAY_ID);
        assertEquals(NO_VERSION, no_version.getIdentity());
    }

    /**
     * Test of getIdentity method, of class SBIdentity.
     */
    @Test
    public void testGetIdentity_3args() {
        SBIdentity with_version = SBIdentity.getIdentity(PREFIX, DISPLAY_ID, VERSION);
        assertEquals(WITH_VERSION, with_version.getIdentity());

    }

    /**
     * Test of getIdentity method, of class SBIdentity.
     */
    @Test
    public void testGetIdentity_4args() {
        SBIdentity with_parent_and_version = SBIdentity.getIdentity(PREFIX, PARENT_DISPLAY_ID, DISPLAY_ID, VERSION);
        assertEquals(WITH_PARENT_AND_VERSION, with_parent_and_version.getIdentity());
    }

    /**
     * Test of getIdentity method, of class SBIdentity.
     */
    @Test
    public void testGetIdentity_URI() throws Exception {
        assertEquals(NO_VERSION, SBIdentity.getIdentity(NO_VERSION).getIdentity());
        assertEquals(WITH_VERSION, SBIdentity.getIdentity(WITH_VERSION).getIdentity());
        assertEquals(WITH_PARENT_AND_VERSION, SBIdentity.getIdentity(WITH_PARENT_AND_VERSION).getIdentity());
        assertEquals(WITH_PARENT_NO_VERSION, SBIdentity.getIdentity(WITH_PARENT_NO_VERSION).getIdentity());
    }
}
