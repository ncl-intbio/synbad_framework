/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.api;

import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class IdentityTests {
    
    private static final URI workspaceId = URI.create("http://www.synbad.org/workspaceManagerTest");
    private SBWorkspace ws;
    private SBIdentityFactory idFactory;
    
    private final String PREFIX = "http://www.synbad.org";
    private final String DISPLAY_ID = "displayId";
    private final String PARENT_DISPLAY_ID = "parentId";
    private final String VERSION = "1.0";
    
    public IdentityTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ws = new DefaultSBWorkspace(workspaceId);
        idFactory = ws.getIdentityFactory();
    }
    
    @After
    public void tearDown() {
        ws.shutdown();
        ws = null;
        idFactory = null;
    }

    @Test
    public void testPrefixDisplayId() {
        SBIdentity identity = idFactory.getIdentity(PREFIX, DISPLAY_ID);
        Assert.assertEquals("Identity is incorrect", identity.getIdentity().toASCIIString(), PREFIX + "/" + DISPLAY_ID);
        Assert.assertEquals("Prefix is incorrect", identity.getUriPrefix(), PREFIX);
        Assert.assertEquals("Display ID is incorrect", identity.getDisplayID(), DISPLAY_ID);
        Assert.assertEquals("Persistent ID is incorrect", identity.getPersistentId(), identity.getIdentity());
    }

    @Test
    public void testPrefixDisplayIdVersion() {
        SBIdentity identity = idFactory.getIdentity(PREFIX, DISPLAY_ID, VERSION);
        Assert.assertEquals("Identity is incorrect", identity.getIdentity().toASCIIString(), PREFIX + "/" + DISPLAY_ID + "/" + VERSION);
        Assert.assertEquals("Prefix is incorrect", identity.getUriPrefix(), PREFIX);
        Assert.assertEquals("Display ID is incorrect", identity.getDisplayID(), DISPLAY_ID);
        Assert.assertEquals("Version is incorrect", identity.getVersion(), VERSION);
        Assert.assertEquals("Persistent ID is incorrect", identity.getPersistentId().toASCIIString() + "/" + VERSION, identity.getIdentity().toASCIIString());
    }
    
    @Test
    public void testPrefixParentDisplayIdVersion() {
        
        SBIdentity identity = idFactory.getIdentity(PREFIX, PARENT_DISPLAY_ID, DISPLAY_ID, VERSION);
        Assert.assertEquals("Identity is incorrect", identity.getIdentity().toASCIIString(), PREFIX + "/" + PARENT_DISPLAY_ID + "/" + DISPLAY_ID + "/" + VERSION);
        Assert.assertEquals("Prefix is incorrect", identity.getUriPrefix(), PREFIX);
        Assert.assertEquals("Display ID is incorrect", identity.getDisplayID(), PARENT_DISPLAY_ID + "/" + DISPLAY_ID);
        Assert.assertEquals("Version is incorrect", identity.getVersion(), VERSION);
        Assert.assertEquals("Persistent ID is incorrect", identity.getPersistentId().toASCIIString() + "/" + VERSION, identity.getIdentity().toASCIIString());
    }
    
 //   @Test
    public void testFromURI() {
        
        SBIdentity identity1 = idFactory.getIdentity(PREFIX, DISPLAY_ID);
        SBIdentity identity1FromURI = idFactory.getIdentity(identity1.getIdentity());
        
        Assert.assertEquals("Identity is incorrect", identity1.getIdentity(), identity1FromURI.getIdentity());
        Assert.assertEquals("Prefix is incorrect", identity1.getUriPrefix(), identity1FromURI.getUriPrefix());
        Assert.assertEquals("Display ID is incorrect", identity1.getDisplayID(), identity1FromURI.getDisplayID());
        Assert.assertEquals("Persistent ID is incorrect", identity1.getPersistentId().toASCIIString(), identity1.getIdentity().toASCIIString());
        
        SBIdentity identity2 = idFactory.getIdentity(PREFIX, DISPLAY_ID, VERSION);
        SBIdentity identity2FromURI = idFactory.getIdentity(identity2.getIdentity());
        
        Assert.assertEquals("Identity is incorrect", identity2.getIdentity(), identity2FromURI.getIdentity());
        Assert.assertEquals("Prefix is incorrect", identity2.getUriPrefix(), identity2FromURI.getUriPrefix());
        Assert.assertEquals("Display ID is incorrect", identity2.getDisplayID(), identity2FromURI.getDisplayID());
        Assert.assertEquals("Persistent ID is incorrect", identity2.getVersion(), identity2FromURI.getVersion());
        Assert.assertEquals("Version is incorrect", identity2.getPersistentId().toASCIIString() + "/" + VERSION, identity2.getIdentity().toASCIIString());
            
        SBIdentity identity3 = idFactory.getIdentity(PREFIX, PARENT_DISPLAY_ID, DISPLAY_ID, VERSION);
        SBIdentity identity3FromURI = idFactory.getIdentity(identity3.getIdentity());
        
        Assert.assertEquals("Identity is incorrect", identity3.getIdentity(), identity3FromURI.getIdentity());
        Assert.assertEquals("Prefix is incorrect", identity3.getUriPrefix(), identity3FromURI.getUriPrefix());
        Assert.assertEquals("Display ID is incorrect", identity3.getDisplayID(), identity3FromURI.getDisplayID());
        Assert.assertEquals("Persistent ID is incorrect", identity3.getVersion(), identity3FromURI.getVersion());
        Assert.assertEquals("Version is incorrect", identity3.getPersistentId().toASCIIString() + "/" + VERSION, identity3.getIdentity().toASCIIString());
    }
    
    @Test
    public void testUniqueIdentity() {
        
        SBIdentity identity1 = idFactory.getIdentity(PREFIX, DISPLAY_ID);
        SBIdentity identity1unique1 = idFactory.getUniqueIdentity(identity1);
        SBIdentity identity1unique2 = idFactory.getUniqueIdentity(identity1unique1);
        SBIdentity identity1unique3 = idFactory.getUniqueIdentity(identity1unique2);
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + DISPLAY_ID + "_1", 
                identity1unique1.getIdentity().toASCIIString());
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + DISPLAY_ID + "_2", 
                identity1unique2.getIdentity().toASCIIString());
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + DISPLAY_ID + "_3", 
                identity1unique3.getIdentity().toASCIIString());

        SBIdentity identity2 = idFactory.getIdentity(PREFIX, DISPLAY_ID, VERSION);
        SBIdentity identity2Unique1 = idFactory.getUniqueIdentity(identity2);
        SBIdentity identity2Unique2 = idFactory.getUniqueIdentity(identity2Unique1);
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + DISPLAY_ID  + "_1" + "/" + VERSION, 
                identity2Unique1.getIdentity().toASCIIString());
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + DISPLAY_ID  + "_2" + "/" + VERSION, 
                identity2Unique2.getIdentity().toASCIIString());

        SBIdentity identity3 = idFactory.getIdentity(PREFIX, PARENT_DISPLAY_ID, DISPLAY_ID, VERSION);
        SBIdentity identity3Unique1 = idFactory.getUniqueIdentity(identity3);
        SBIdentity identity3Unique2 = idFactory.getUniqueIdentity(identity3Unique1);
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + PARENT_DISPLAY_ID + "/" + DISPLAY_ID  + "_1" + "/" + VERSION,
                identity3Unique1.getIdentity().toASCIIString());
        
//        Assert.assertEquals("Persistent SBIdentity is incorrect", PREFIX + "/" + PARENT_DISPLAY_ID + "_" + DISPLAY_ID  + "_1",
//                identity3Unique1.getPersistentId().toASCIIString());
        
        Assert.assertEquals("Identity is incorrect", PREFIX + "/" + PARENT_DISPLAY_ID + "/" + DISPLAY_ID  + "_2" + "/" + VERSION,
                identity3Unique2.getIdentity().toASCIIString());
        
//        Assert.assertEquals("SBIdentity is incorrect", PREFIX + "/" + PARENT_DISPLAY_ID + "_" + DISPLAY_ID  + "_2",
//                identity3Unique2.getPersistentId().toASCIIString());
    }

    @Test
    public void testUniqueIdentity2() throws SBIdentityException {

        URI fc = URI.create("http://www.synbad.org/md_sink/fc_cd_sink_dna/1.0");
        SBIdentity fcId = SBIdentity.getIdentity(fc);
        SBIdentity fcId2 = idFactory.getIdentity(fc);

        Assert.assertEquals(fc, fcId.getIdentity());
        Assert.assertEquals(fcId.getIdentity(), fcId2.getIdentity());
        Assert.assertEquals(fcId.getDisplayID(), fcId2.getDisplayID());
        Assert.assertEquals(fcId.getPersistentId(), fcId2.getPersistentId());

        URI mapsToid = URI.create("http://www.synbad.org/md_sink/fc_cd_sink_dna/mt_md_sink_fc_cd_ter_sink_dna_sameAs_cd_sink_dna_c_cd_ter_sink_dna/1.0");

        SBIdentity mtId1 = idFactory.getIdentity(mapsToid);
        SBIdentity mtId2 = SBIdentity.getIdentity(mapsToid);

        Assert.assertEquals(mapsToid, mtId1.getIdentity());
        Assert.assertEquals(mtId1.getIdentity(), mtId2.getIdentity());
        Assert.assertEquals(mtId1.getDisplayID(), mtId2.getDisplayID());
        Assert.assertEquals(mtId1.getPersistentId(), mtId2.getPersistentId());
    }
}