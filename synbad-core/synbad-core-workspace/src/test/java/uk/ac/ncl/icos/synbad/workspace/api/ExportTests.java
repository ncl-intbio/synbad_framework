/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.api;

import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonFullExporter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.workspace.io.SBJsonFullImporter;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBValued;

/**
 *
 * @author Owen
 */
public class ExportTests {
    
    
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportTests.class);
    private final URI[] contexts = new URI[] {URI.create("http://www.synbad.org/tests/sbolexport_context")};
        
    private SBWorkspace workspace;
    private SBIdentity workspaceId;

    public ExportTests() {}
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp()  {
        

        workspaceId = SBIdentity.getIdentity("http://www.synbad.org", "exportTestWorkspace", "1.0");
        workspace = new DefaultSBWorkspace(workspaceId.getIdentity());

        SBIdentity context1 = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "exportTestContext1", "1.0");
        SBIdentity context2 = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "exportTestContext2", "1.0");
        URI type = URI.create("http://www.synbad.org/testobjecttype");
        SBIdentity obj1Id = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "object1", "1.0");
        SBIdentity obj2Id = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "object2", "1.0");
        URI edge = URI.create("http://www.synbad.org/hasRelationship");
        URI predicate = URI.create("http://www.synbad.org/hasValue");
        DefaultSBDomainBuilder builder = new DefaultSBDomainBuilder(workspace, new URI[]{ context1.getIdentity() });
        builder = builder
                    .createObject(obj1Id.getIdentity(), type, null, null)
                    .createValue(obj1Id.getIdentity(), predicate, SBValue.parseValue(true), type);
        workspace.perform(builder.build());
        DefaultSBDomainBuilder builder2 = new DefaultSBDomainBuilder(workspace, new URI[]{ context2.getIdentity() });
        builder2 = builder2
                    .createObject(obj1Id.getIdentity(), type, null, null)
                    .createValue(obj1Id.getIdentity(), predicate, SBValue.parseValue(true), type)
                    .createObject(obj2Id.getIdentity(), type, null, null)
                    .createValue(obj2Id.getIdentity(), predicate, SBValue.parseValue("A String value"), type)
                    .createEdge(obj1Id.getIdentity(), edge, obj2Id.getIdentity());
        workspace.perform(builder2.build());
    }
    
    @After
    public void tearDown() {       
    }

    
    
    /**
     * Test of doExport method, of class SBWorkspaceExporter.
     */
    @Test
    public void testInAndOutFull() throws SBIdentityException, IOException {
        
        File f = new File("workspaceExport.json");
        
        if (!f.exists()) {
            f.createNewFile();
        }

        if (!f.canWrite()) { 
            f.delete();
        }

        FileOutputStream stream = new FileOutputStream(f);
        new SBJsonFullExporter().export(stream, workspace, null);
        stream.close();

        long workspaces1 = workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().count();
        
        //Lookup.getDefault().lookup(SBWorkspaceManager.class).closeWorkspace(workspaceId.getIdentity());
        workspace.shutdown();

        workspace = new DefaultSBWorkspace(workspaceId.getIdentity());
         
        FileInputStream inStream = new FileInputStream(f);
        new SBJsonFullImporter().importWs(inStream, workspace, null);
       // workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().forEach(System.out::println);
        
        long workspaces2 = workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().count();
        
        assert(workspaces1 == workspaces2);

        workspace.shutdown();
    }
    
     /**
     * Test of doExport method, of class SBWorkspaceExporter.
     */
   // @Test
     /*
    public void testInAndOutFullMultiple() throws SBIdentityException, IOException {
        
        File f = new File("multipleWorkspaces.json");
        
        if (!f.exists()) {
            f.createNewFile();
        }

        if (!f.canWrite()) { 
            f.delete();
        }

        FileOutputStream stream = new FileOutputStream(f);
        Set<SBWorkspace> workspaces = new HashSet<>();
        workspaces.add(workspace);
        
        long workspaces1 = workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().count();
        //workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().forEach(System.out::println);
        new SBJsonFullExporter().export(stream, workspaces);
        stream.close();

        workspace.shutdown();
        workspace = new DefaultSBWorkspace(workspaceId.getIdentity());
        // System.out.println("----");
        FileInputStream inStream = new FileInputStream(f);
        new SBJsonFullImporter().importWs(inStream);
        long workspaces2 = workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().count();
        //workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().forEach(System.out::println);
        assert(workspaces1 == workspaces2);
    } */
    
    @Test
    public void testTransfer() throws SBIdentityException, IOException {
       
        SBIdentity workspaceId2 = workspace.getIdentityFactory().getIdentity("http://www.synbad.org", "duplicateWorkspace", "1.0");
        SBWorkspace duplicate = new DefaultSBWorkspace(workspaceId2.getIdentity());
        
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        new SBJsonFullExporter().export(out, workspace, null);
        new SBJsonFullImporter().importWs(new ByteArrayInputStream(out.toByteArray()), duplicate, null);
        
        long workspace1 = workspace.getRdfService().getStatements(null, null, null, workspace.getContextIds()).stream().count();
        long workspace2 = duplicate.getRdfService().getStatements(null, null, null, duplicate.getContextIds()).stream().count();
        
        assert(workspace1 == workspace2);
        
        duplicate.shutdown();
        workspace.shutdown();
    }
        
    // TestObject class for the above tests
    
    @DomainObject(rdfType = TestObject.TYPE)
    static class TestObject extends ASBValued {
        
        private static final String TYPE = "http://www.synbad.org/testobjecttype";

        public TestObject(URI identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
            super(identity, workspaceIdentity, provider);
        }

        @Override
        public URI getType() {
            return URI.create(TYPE);
        }
    }
}
    

