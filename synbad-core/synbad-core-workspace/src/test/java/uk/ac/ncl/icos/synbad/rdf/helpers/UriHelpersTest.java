/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf.helpers;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openide.util.Exceptions;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class UriHelpersTest {
    
    private final String PREFIX = "http://www.synbad.org";
    private final String PARENT_DISPLAY_ID = "parentDisplayId";
    private final String DISPLAY_ID = "displayId";
    private final String VERSION = "1.0";
    private final String INCREMENT = "1";
    
    private final URI NO_VERSION = URI.create(PREFIX.concat("/").concat(DISPLAY_ID));
    private final URI NO_VERSION_INC = URI.create(PREFIX.concat("/").concat(DISPLAY_ID).concat("_").concat(INCREMENT));
    private final URI WITH_PARENT_NO_VERSION = URI.create(PREFIX.concat("/").concat(PARENT_DISPLAY_ID).concat("/").concat(DISPLAY_ID));
    private final URI WITH_PARENT_NO_VERSION_INC = URI.create(PREFIX.concat("/").concat(PARENT_DISPLAY_ID).concat("/").concat(DISPLAY_ID).concat("_").concat(INCREMENT));
    private final URI WITH_VERSION = URI.create(PREFIX.concat("/").concat(DISPLAY_ID).concat("/").concat(VERSION));
    private final URI WITH_VERSION_INC = URI.create(PREFIX.concat("/").concat(DISPLAY_ID).concat("_").concat(INCREMENT).concat("/").concat(VERSION));
    private final URI WITH_PARENT_AND_VERSION = URI.create(PREFIX.concat("/").concat(PARENT_DISPLAY_ID).concat("/").concat(DISPLAY_ID).concat("/").concat(VERSION));
    private final URI WITH_PARENT_AND_VERSION_INC = URI.create(PREFIX.concat("/").concat(PARENT_DISPLAY_ID).concat("/").concat(DISPLAY_ID).concat("_").concat(INCREMENT).concat("/").concat(VERSION));
    
    
    public UriHelpersTest() { }
    
    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() { }
    
    @After
    public void tearDown() { }

    /**
     * Test of isValidId method, of class SBIdentityHelper.
     */
    @Test
    public void testIsValidId_URI() {
        assertTrue(SBIdentityHelper.isValidId(NO_VERSION));
        assertTrue(SBIdentityHelper.isValidId(WITH_VERSION));
        assertTrue(SBIdentityHelper.isValidId(WITH_PARENT_NO_VERSION));
        assertTrue(SBIdentityHelper.isValidId(WITH_PARENT_AND_VERSION));
    }

    /**
     * Test of isValidId method, of class SBIdentityHelper.
     */
    @Test
    public void testIsValidId_String() {
        try {
            assertEquals(SBIdentity.getIdentity(NO_VERSION).getIdentity(), NO_VERSION);
            assertEquals(SBIdentity.getIdentity(WITH_PARENT_AND_VERSION).getIdentity(), WITH_PARENT_AND_VERSION);
            assertEquals(SBIdentity.getIdentity(WITH_PARENT_NO_VERSION).getIdentity(), WITH_PARENT_NO_VERSION);
            assertEquals(SBIdentity.getIdentity(WITH_VERSION).getIdentity(), WITH_VERSION);
        } catch (SBIdentityException ex) {
            Exceptions.printStackTrace(ex);
        }
    }


    /**
     * Test of getBaseDisplayIdFromIncrementing method, of class SBIdentityHelper.
     */
    @Test
    public void testGetBaseDisplayIdFromIncrementing() {
        assertEquals(DISPLAY_ID, SBIdentityHelper.getBaseDisplayIdFromIncrementing(DISPLAY_ID.concat("_").concat(INCREMENT)));
        assertEquals(DISPLAY_ID, SBIdentityHelper.getBaseDisplayIdFromIncrementing(DISPLAY_ID));
    }

    /**
     * Test of getIncrementFromIncrementing method, of class SBIdentityHelper.
     */
    @Test
    public void testGetIncrementFromIncrementing() {
        assertEquals(1, SBIdentityHelper.getIncrementFromIncrementing(DISPLAY_ID.concat("_").concat(INCREMENT)));
        assertEquals(0, SBIdentityHelper.getIncrementFromIncrementing(DISPLAY_ID));
    }


    /**
     * Test of getPrefixFromId method, of class SBIdentityHelper.
     */
    @Test
    public void testGetPrefixFromId() {
        assertEquals(PREFIX, SBIdentityHelper.getPrefixFromId(NO_VERSION));
        assertEquals(PREFIX, SBIdentityHelper.getPrefixFromId(WITH_PARENT_AND_VERSION));
        assertEquals(PREFIX, SBIdentityHelper.getPrefixFromId(WITH_PARENT_NO_VERSION));
        assertEquals(PREFIX, SBIdentityHelper.getPrefixFromId(WITH_VERSION));
    }

    /**
     * Test of getDisplayIdFromId method, of class SBIdentityHelper.
     */
    @Test
    public void testGetDisplayIdFromId() {
        assertEquals(DISPLAY_ID, SBIdentityHelper.getDisplayIdFromId(NO_VERSION));
        assertEquals(DISPLAY_ID, SBIdentityHelper.getDisplayIdFromId(WITH_PARENT_AND_VERSION));
        assertEquals(DISPLAY_ID, SBIdentityHelper.getDisplayIdFromId(WITH_PARENT_NO_VERSION));
        assertEquals(DISPLAY_ID, SBIdentityHelper.getDisplayIdFromId(WITH_VERSION));
    }

    /**
     * Test of getParentDisplayIdFromId method, of class SBIdentityHelper.
     */
    @Test
    public void testGetParentDisplayIdFromId() {
        assertEquals(PARENT_DISPLAY_ID, SBIdentityHelper.getParentDisplayIdFromId(WITH_PARENT_AND_VERSION));
        assertEquals(PARENT_DISPLAY_ID, SBIdentityHelper.getParentDisplayIdFromId(WITH_PARENT_NO_VERSION));
    }

    /**
     * Test of getChildDisplayIdFromId method, of class SBIdentityHelper.
     */
    @Test
    public void testGetChildDisplayIdFromId() {
        assertEquals(DISPLAY_ID, SBIdentityHelper.getChildDisplayIdFromId(WITH_PARENT_AND_VERSION));
        assertEquals(DISPLAY_ID, SBIdentityHelper.getChildDisplayIdFromId(WITH_PARENT_NO_VERSION));
    }
    
    /**
     * Test of getChildDisplayIdFromId method, of class SBIdentityHelper.
     */
    @Test
    public void testGetChildSeperatorFromId() {
        assertEquals("/", SBIdentityHelper.getChildSeperatorFromId(WITH_PARENT_AND_VERSION));
        assertEquals("/", SBIdentityHelper.getChildSeperatorFromId(WITH_PARENT_NO_VERSION));
    }
    
}
