/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.api;

import java.net.URI;
import java.util.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfMemService;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;

/**
 *
 * @author owengilfellon
 */
public class RdfServiceTests {
    
    private static final String workspace = "http://www.synbad.org/jenaRdfTest";
    private  URI workspaceUri = URI.create(workspace);
    
    public RdfServiceTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getContexts method, of class DefaultMemRdfService.
     */
    @Test
    public void testGetContexts() {
        
       DefaultSBRdfMemService instance = new DefaultSBRdfMemService();
        URI[] contexts = new URI[] {
            URI.create("http://www.synbad.org/jenaRdfTest/context1"),
            URI.create("http://www.synbad.org/jenaRdfTest/context2")
        };
        
        instance.createContext(contexts[0]);
        instance.createContext(contexts[1]);
        
        instance.addStatements(Collections.singleton(new SBStatement("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"))), contexts[0]);
        instance.addStatements(Collections.singleton(new SBStatement("http://www.synbad.org/jenaRdfTest/object2", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Orange"))), contexts[1]);
        
        URI[] expResult = null;
        URI[] result = instance.getContexts();
        assertEquals(contexts.length, result.length);
        
        DefaultSBRdfMemService instance2 = new DefaultSBRdfMemService();
        //assertEquals(instance, instance2);
      //  assertEquals(contexts.length, instance2.getContexts().length);
        instance.shutdown();
      //  instance2.shutdown();
        
    }

    /**
     * Test of getStatements method, of class DefaultMemRdfService.
     */
    @Test
    public void testGetStatementsFromSingleRdfServiceInstance() {
        DefaultSBRdfMemService instance = new DefaultSBRdfMemService();
        URI[] contexts = new URI[] {
            URI.create("http://www.synbad.org/jenaRdfTest/context1"),
            URI.create("http://www.synbad.org/jenaRdfTest/context2")
        };
        
        instance.createContext(contexts[0]);
        instance.createContext(contexts[1]);
        
        instance.addStatements(Arrays.asList(new SBStatement("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"))), contexts[0]); 
        instance.addStatements(Arrays.asList(new SBStatement("http://www.synbad.org/jenaRdfTest/object2", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Orange"))), contexts[1]); 

        Collection<SBStatement> statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"), contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", null, contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", null, null, contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements(null, "http://www.synbad.org/jenaRdfTest/is", null, contexts);
        assertEquals(2, statements.size()); 
        
        instance.shutdown();
    }
    
    @Test
    public void testSetStatementsFromSingleRdfServiceInstance() {
        DefaultSBRdfMemService instance = new DefaultSBRdfMemService();
        URI[] contexts = new URI[] {
            URI.create("http://www.synbad.org/jenaRdfTest/context1"),
            URI.create("http://www.synbad.org/jenaRdfTest/context2")
        };
        
        instance.createContext(contexts[0]);
        instance.createContext(contexts[1]);
        
        instance.addStatements(Arrays.asList(new SBStatement("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"))), contexts[0]); 
        instance.addStatements(Arrays.asList(new SBStatement("http://www.synbad.org/jenaRdfTest/object2", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Orange"))), contexts[1]); 

        Collection<SBStatement> statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"), contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", null, contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", null, null, contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements(null, "http://www.synbad.org/jenaRdfTest/is", null, contexts);
        assertEquals(2, statements.size()); 
        
        instance.shutdown();
    }
    
    @Test
    public void testGetStatementValuesFromMultipleRdfServiceInstances() {
        DefaultSBRdfMemService instance = new DefaultSBRdfMemService();
        URI[] contexts = new URI[] {
            URI.create("http://www.synbad.org/jenaRdfTest/context1"),
            URI.create("http://www.synbad.org/jenaRdfTest/context2")
        };
        
        instance.createContext(contexts[0]);
        instance.createContext(contexts[1]);
        
        instance.addStatements(Collections.singleton(new SBStatement("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"))), contexts[0]);
        instance.addStatements(Collections.singleton(new SBStatement("http://www.synbad.org/jenaRdfTest/object2", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Pip"))), contexts[1]);
        instance.addStatements(Collections.singleton(new SBStatement("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/has", SBValue.parseValue("http://www.synbad.org/jenaRdfTest/object2"))), contexts);

        Collection<SBStatement> statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", SBValue.parseValue("Apple"), contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/is", null, contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", "http://www.synbad.org/jenaRdfTest/has", null, contexts);
        assertEquals(1, statements.size());
        
        SBStatement s = statements.iterator().next();
        
        assert(s.getObject().isURI());
        assertNotNull(s.getObject().asURI());
        assert(s.getObject().asURI().equals(URI.create("http://www.synbad.org/jenaRdfTest/object2")));
      
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object1", null, null, contexts);
        assertEquals(2, statements.size());
        
        statements  = instance.getStatements("http://www.synbad.org/jenaRdfTest/object2", null, null, contexts);
        assertEquals(1, statements.size());
        
        statements  = instance.getStatements(null, "http://www.synbad.org/jenaRdfTest/is", null, contexts);
        assertEquals(2, statements.size()); 
        
        
        statements = instance.getStatements(null, null, null, contexts[0]);
        assertEquals(2, statements.size()); 
        
        statements = instance.getStatements(null, null, null, contexts[1]);
        assertEquals(2, statements.size()); 
         
        instance.shutdown();;
       
    }
}
