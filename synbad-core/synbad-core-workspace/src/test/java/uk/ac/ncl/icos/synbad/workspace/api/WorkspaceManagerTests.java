/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.api;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import java.net.URI;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceManagerTests {
    
    private static final URI workspaceId = URI.create("http://www.synbad.org/workspaceManagerTest");
    
    public WorkspaceManagerTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getWorkspace method, of class SBWorkspaceManager.
     */
    @Test
    public void testGetWorkspace() {
        
        SBWorkspaceManager m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        SBWorkspace result = m.getWorkspace(workspaceId);
        SBRdfService service = result.getRdfService();
        SBWorkspace result2 = m.getWorkspace(workspaceId);
        SBRdfService service2 = result2.getRdfService();
        
        assertEquals("Workspaces should be cached and identical", result, result2);
        assertEquals("Should only be one workspace in manager",  1, m.listWorkspaces().size());
        
        assertEquals("RdfServices should be cached and identical", service, service2);

        m.closeWorkspace(workspaceId);
        
        assertEquals("Should be no workspaces in manager", m.listWorkspaces().size(), 0);
        
        SBWorkspace result3 = m.getWorkspace(workspaceId);
        SBRdfService service3 = result3.getRdfService();
        
        assertNotSame("After closing workspace, new workspace should be new", result, result3);
        assertNotSame("After closing workspace, new service should be new", service, service3);

    }
}
