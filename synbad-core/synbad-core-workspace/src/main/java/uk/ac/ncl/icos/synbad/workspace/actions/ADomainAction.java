/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.Collection;

import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;

/**
 *
 * @author owengilfellon
 */
public abstract class ADomainAction<O> implements SBAction {

    protected final O object;
    protected final URI workspace;
    protected final URI[] contexts;
    protected final SBGraphEventType type;

    public ADomainAction(SBGraphEventType type,  O object, URI workspace, URI[] contexts) {
        this.object = object;
        this.workspace = workspace;
        this.contexts = contexts;
        this.type = type;
    }

    @Override
    public Collection<SBEvent> perform(SBCommandManager commander) {
        
        // With atomic domain actions, we can just retrieve event, pass to
        // commander and return
        
        Collection<SBEvent> evts = getEvents(false);
        commander.perform(evts);
        return evts;
    }
    
    @Override
    public URI[] getContexts() {
        return contexts;
    }
}

