package uk.ac.ncl.icos.synbad.workspace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

public class SBWorkspaceBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(SBWorkspaceBuilder.class);
    private final URI workspaceID;
    private List<SBWorkspaceService> services = new LinkedList<>();
    private SBRdfService service = null;

    public SBWorkspaceBuilder(URI workspaceId) {
        this.workspaceID = workspaceId;
    }

    public SBWorkspaceBuilder addService(SBWorkspaceService service) {
        SBWorkspaceBuilder b = new SBWorkspaceBuilder(workspaceID);
        b.services = new LinkedList<>(services);
        b.services.add(service);
        return b;
    }

    public SBWorkspaceBuilder addRdfService(SBRdfService service) {
        SBWorkspaceBuilder b = new SBWorkspaceBuilder(workspaceID);
        b.service = service;
        return b;
    }

    public SBWorkspace build() {
        SBWorkspace ws = service == null ?
                new DefaultSBWorkspace(workspaceID) :
                new DefaultSBWorkspace(workspaceID, service);

        if(LOGGER.isInfoEnabled())
            LOGGER.info("Constructing workspace with {} services", services.size());

        services.stream().forEach(s -> {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Initialising {}", s.getClass().getSimpleName());
            s.setWorkspace(ws);
        });

        return ws;
    }
}
