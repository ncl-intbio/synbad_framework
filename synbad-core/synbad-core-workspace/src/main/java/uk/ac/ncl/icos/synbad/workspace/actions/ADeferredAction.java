/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.Collection;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 * An {@link SBAction} that defers the creation of the action and related events
 * until {@link SBAction#perform(SBCommandManager)} is called. This is useful in
 * cases where the action might depend on changes that occur between between
 * the creation of an action and its execution. Subclasses should implement
 * {@link #getAction(SBWorkspace workspace)}.
 * @author owengilfellon
 */
public abstract class ADeferredAction implements SBAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ADeferredAction.class);
    private SBAction action = null;
    private boolean performed = false;
    protected final URI[] contexts;
    protected final SBGraphEventType type;
    private final SBWorkspace workspace;

    public ADeferredAction(SBGraphEventType type, SBWorkspace workspace, URI[] contexts) {
        this.contexts = contexts;
        this.type = type;
        this.workspace = workspace;
    }

    protected abstract SBAction getAction(SBWorkspace workspace);
    
    @Override
    public Collection<SBEvent> perform(SBCommandManager subscriber) {
        
        if(!performed && action == null) {
            action = getAction(workspace);
            performed = true;
        }

        return action.perform(subscriber);
    }
    
    @Override
    public Collection<SBEvent> getEvents(boolean undo) {

        if(action == null) {
            throw new SBActionException("Can't get events, action has not been performed");
        }
        
        return action.getEvents(undo);
    }

    @Override
    public URI[] getContexts() {
        return contexts;
    }
}

