/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 * From a provided object, follow owns and dependantOn edges to other objects. 
 * Instances should always be incremented. If object is an instance, they are checked 
 * against a provided list of identities and set a flag to indicate whether their definitions 
 * should be incremented. If their definition is incremented, so should the SVP / SVM definition,
 * and any internal or external interactions that they link to. 
 * What about Domain Events? Map statements to subject. Create object, edge and 
 * value statements as appropriate.
 * 
 * 
 * @author Owen
 */
public class DuplicateObject extends ADeferredAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(DuplicateObject.class);
    
    private final SBIdentityFactory idFactory;
    private final URI identity;
    private final URI[] fromContexts;
    private final URI[] toContexts;
    private SBAction action = null;
    private boolean initialised = false;
    private final Set<URI> processed = new HashSet<>();
    private final Set<SynBadEdge> processedEdges = new HashSet<>();
    private final Map<URI, URI> oldToNewIdentities = new HashMap<>();
    private final boolean updateIdentities;

    public DuplicateObject(URI identity, SBWorkspace workspace, URI[] fromContexts, URI[] toContexts) {
        super(SBGraphEventType.ADDED, workspace, toContexts);
        this.idFactory = workspace.getIdentityFactory();
        this.identity = identity;
        this.fromContexts = fromContexts;
        this.toContexts = toContexts;
        this.updateIdentities = true;
    }
    
    public DuplicateObject(URI identity, SBWorkspace workspace, boolean updateIds, URI[] fromContexts, URI[] toContexts) {
        super(SBGraphEventType.ADDED, workspace, toContexts);
        this.idFactory = workspace.getIdentityFactory();
        this.identity = identity;
        this.fromContexts = fromContexts;
        this.toContexts = toContexts;
        this.updateIdentities = updateIds;
    }
   
    @Override
    protected SBAction getAction(SBWorkspace ws) {
        if(initialised)
            return action;
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Duplicating {}", identity.toASCIIString());
        List<SBAction> actions = addObject(ws, new LinkedList<>(), identity, null);
        this.action = new SBActionMacro(actions, "Duplicate ["+ identity.toASCIIString() +"]");
        initialised = true;
        return action;
    }
    
    private URI doUpdate(URI uri) {
        if(!updateIdentities) 
            return uri;
        
        if(!oldToNewIdentities.containsKey(uri)) {
            oldToNewIdentities.put(uri, idFactory.getNextIdentity(idFactory.getIdentity(uri)).getIdentity());
        }

        return oldToNewIdentities.get(uri);
    }
    
    private List<SBAction> addObject(SBWorkspace ws, List<SBAction> actions, URI object, URI parentType) {

        if(processed.contains(object))
            return actions;

        SBValued o = ws.getObject(object, SBValued.class, fromContexts);

        URI parent = ws.getRdfService().getStatements(
                null, SynBadTerms.SynBadSystem.owns, SBValue.parseValue(object), ws.getSystemContextIds(fromContexts))
                .stream().map(SBStatement::getSubject)
                .map(SBIdentityHelper::getURI)
                .findFirst().orElse(null);
       
        URI id = doUpdate(object);
        URI pid = parent == null ? null : doUpdate(parent);

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Doing: {}", id.toASCIIString());//ws.getIdentityFactory().getIdentity(object).getDisplayID());

        actions.add(new CreateObject(
                id, o.getType().toASCIIString(),
                pid, parentType, ws, toContexts));
        
        processed.add(object);
        

        ws.outgoingEdges(o, fromContexts).stream()
            .forEach(e -> addEdge(ws, actions, e, o.getType(), toContexts));

        ws.getRdfService().getStatements(object.toASCIIString(), null, null, ws.getSystemContextIds(fromContexts))
                .stream().map(s -> new SynBadEdge(SBIdentityHelper.getURI(s.getPredicate()), object,  s.getObject().asURI(), ws.getSystemContextIds(fromContexts)))
            .forEach(e -> addEdge(ws, actions, e, o.getType(), ws.getSystemContextIds(toContexts)));

        o.getPredicates().forEach((predicate) -> {
            URI p = SBIdentityHelper.getURI(predicate);
            o.getValues(predicate).forEach((v) -> {
                addValue(ws, actions, o, p, v);
            });
        });

        return actions;
    }
    
    private List<SBAction> addEdge(SBWorkspace ws, List<SBAction> actions, SynBadEdge edge, URI previousType, URI[] context) {

        addObject(ws, actions, edge.getTo(), previousType);

        if(processedEdges.contains(edge))
            return actions;


        URI from = doUpdate(edge.getFrom());
        URI to = doUpdate(edge.getTo());

        if(LOGGER.isTraceEnabled()) {
            LOGGER.trace("Doing edge: {}", new SynBadEdge(edge.getEdge(), from, to, contexts).toString());
        }



        actions.add(new CreateEdge(
                from, 
                edge.getEdge().toASCIIString(), 
                to, ws.getIdentity(), context));

        processedEdges.add(edge);

        return actions;
    }
    
    private List<SBAction> addValue(SBWorkspace ws, List<SBAction> actions, SBValued obj, URI predicate, SBValue value) {

        URI id = doUpdate(obj.getIdentity());

        SBValue updatedValue = value.isURI() && ws.containsIdentity(value.asURI(), fromContexts) ?
            SBValue.parseValue(doUpdate(value.asURI())) :
            value;

        actions.add(new CreateValue(
                id,
                predicate.toASCIIString(),
                updatedValue, obj.getType(), ws, toContexts));

        return actions;
    }
}
