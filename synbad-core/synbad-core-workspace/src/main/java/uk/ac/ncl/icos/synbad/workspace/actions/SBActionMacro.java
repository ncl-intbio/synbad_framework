/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;

/**
 *
 * @author owengilfellon
 */
public class SBActionMacro implements SBAction {
    
    public enum MacroEventType implements SBEventType {

        START,
        STOP;

        @Override
        public String getType() {
            return name();
        }

    }

    private static final Logger LOGGER = LoggerFactory.getLogger(SBActionMacro.class);
    private final List<SBAction> actions;
    private final String label;

    public SBActionMacro(List<SBAction> actions) {
        this.actions = new ArrayList<>(actions);
        this.label = "";
    }

    public SBActionMacro(List<SBAction> actions, String label) {
        this.label = label;
        this.actions = new ArrayList<>(actions);
    }

    @Override
    public Collection<SBEvent> perform(SBCommandManager commander) {

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Perform{}: {} actions", label.isEmpty() ? label : " ".concat(label),  actions.size());
         
        // run perform on all actions and return events
        // allows commander to modify state before 
        // publishing events

        Collection<SBEvent> events = actions.stream().flatMap(a -> a.perform(commander).stream()).collect(Collectors.toList());

        return events;
    }
    
    @Override
    public Collection<SBEvent> getEvents(boolean undo) {
      
        List<SBEvent> ac = actions.stream()
                .flatMap(a -> a.getEvents(undo).stream())
                .collect(Collectors.toList());
        
        if(undo) {
            Collections.reverse(ac);
        }
        
        return ac;
    }

    @Override
    public String toString() {
        if(!label.isEmpty())
            return label;

        StringBuilder sb = new StringBuilder();
        actions.stream().forEach((action) -> {
            sb.append(action.toString()).append(" ");
        });
        return sb.toString();
    }

    @Override
    public URI[] getContexts() {
        return actions.stream().map(a -> a.getContexts())
                .flatMap(uri -> Arrays.asList(uri).stream())
                .collect(Collectors.toSet()).toArray(new URI[]{});
    } 
}