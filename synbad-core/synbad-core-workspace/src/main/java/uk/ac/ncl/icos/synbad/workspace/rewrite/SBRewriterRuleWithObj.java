/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;

/**
 *
 * @author owengilfellon
 */
public interface SBRewriterRuleWithObj<N extends SBValued, E extends SBEdge, G> extends SBRewriterRule<N, E, G>{
    
    public SBAction apply(G g, SBIdentified object);
    public boolean shouldApply(SBIdentified object);
    
}
