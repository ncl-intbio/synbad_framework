package uk.ac.ncl.icos.synbad.workspace.objects;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

import java.net.URI;

public class DefaultStubObject extends ASBIdentified {

    public DefaultStubObject(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }

    @Override
    public URI getType() {
        return null;
    }
}
