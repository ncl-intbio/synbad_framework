/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import java.util.ArrayList;
import java.util.List;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.StmtIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;

/**
 *
 * @author owengilfellon
 */
public abstract class AJenaImporter implements SBWorkspaceImporter {
    
    private final Logger logger = LoggerFactory.getLogger(AJenaImporter.class);
    
    protected List<SBStatement> getStatementsFromModel(String subject, String predicate, SBValue object, Model model) {
        
        List<SBStatement> statements = new ArrayList<>();
        Resource r_subject = subject == null ? null : model.getResource(subject);
        Property r_predicate = predicate == null ? null : model.getProperty(predicate);
        RDFNode r_object = getValue(model, object);

       // logger.trace("Selecting: {} | {} | {}", r_subject, r_predicate, r_object);

        Selector selector = new SimpleSelector(r_subject, r_predicate, r_object);
        StmtIterator i = model.listStatements(selector);

        while (i.hasNext()) {

            org.apache.jena.rdf.model.Statement statement = i.nextStatement();
            String valueOfSubject = statement.getSubject().getURI();
            String valueOfPredicate = statement.getPredicate().getURI();
            SBValue valueOfObject = SBValue.parseValue(
                    statement.getObject().isResource() ? 
                            statement.getObject().asResource().getURI() :
                            statement.getObject().asLiteral().getLexicalForm() 
                            );

            SBStatement s =new SBStatement(
                    valueOfSubject,
                    valueOfPredicate,
                    valueOfObject);

            statements.add(s);
        }
        
        return statements;
    }
    
    protected RDFNode getValue(Model m, SBValue object) {
        RDFNode r_object = null;
            
        if(object != null) {
            if(object.isURI()) {
                r_object = m.createResource(object.asString());
            } else if(object.isBoolean()) {
                r_object = m.createLiteral(""+object.asBoolean());
            } else if (object.isDouble()) {
                r_object = m.createLiteral(""+object.asDouble());
            } else if (object.isInt()) {
                r_object = m.createLiteral(""+object.asInt());
            } else if (object.isQName()) {
               r_object = m.createResource(object.asQName().getNamespaceURI());
            } else if (object.isString()) {
               r_object = m.createTypedLiteral(object.asString());
            } 
        }
        
        if(object != null && r_object == null)
            logger.warn("Parsed value for {} is null", object);

        return r_object;
    }
}
