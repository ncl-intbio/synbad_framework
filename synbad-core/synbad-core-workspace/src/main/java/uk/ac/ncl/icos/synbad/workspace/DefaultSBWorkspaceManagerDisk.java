/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.event.DefaultSBEvent;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfDiskService;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.ServiceDependency;

/**
 *
 * @author owengilfellon
 */

//@ServiceProvider(service = SBWorkspaceManager.class, position = 10)
public class DefaultSBWorkspaceManagerDisk implements SBWorkspaceManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBWorkspaceManagerDisk.class);
    private final Map<URI, WorkspaceRecord> workspaces;
    private final List<Class<? extends SBWorkspaceService>> services;
    private final InstanceContent content;
    private final AbstractLookup lookup;
    private final SBDispatcher dispatcher;
    private boolean servicesInitialised = false;

    public DefaultSBWorkspaceManagerDisk() {
        this.workspaces = new HashMap<>();
        this.services = new LinkedList<>();
        this.content = new InstanceContent();
        this.lookup = new AbstractLookup(this.content);
        this.dispatcher = new DefaultSBDispatcher();
        processServices( new ArrayList<>(),
                Lookup.getDefault().lookupAll(SBWorkspaceService.class).stream()
                        .map(s -> s.getClass()).collect(Collectors.toList()));
        servicesInitialised = true;
        initialiseRecords();
    }

    public DefaultSBWorkspaceManagerDisk(Map<URI, WorkspaceRecord> workspaces, List<Class<? extends SBWorkspaceService>> services, InstanceContent content, AbstractLookup lookup, SBDispatcher dispatcher) {
        this.workspaces = workspaces;
        this.services = services;
        this.content = content;
        this.lookup = lookup;
        this.dispatcher = dispatcher;
        initialiseRecords();
    }
    
    private void initialiseRecords() {
        String current = System.getProperty("user.dir");
        String recordFile = current + "/workspaces.json";
        File file = new File(recordFile);
        if(!file.exists()) {
            return;
        }
        
        ObjectMapper mapper = new ObjectMapper();
        try {
            WorkspacePersistenceRecords records = mapper.readValue(file, WorkspacePersistenceRecords.class);
            records.getRecords().stream().forEach(r -> getWorkspace(URI.create(r.getWorkspaceUri())));
        } catch (IOException ex) {
            LOGGER.error("Could not read workspace record file", ex);
        }
    }
    
    private void updateRecords(List<SBWorkspace> workspaces) {
        
        if(workspaces.isEmpty())
            return;
        
        List<WorkspacePersistenceRecord> recordList = workspaces.stream()
            .filter(ws -> DefaultSBRdfDiskService.class.isAssignableFrom(ws.getRdfService().getClass()))
            .map(ws -> {
                 WorkspacePersistenceRecord r = new WorkspacePersistenceRecord();
                 r.setWorkspaceUri(ws.getIdentity().toASCIIString());
                 r.setDirectory(DefaultSBRdfDiskService.getDirectoryName(ws.getIdentity()));
                 return r;})
            .collect(Collectors.toList());
        
        if(recordList.isEmpty())
            return;
        
        WorkspacePersistenceRecords records = new WorkspacePersistenceRecords();
        records.setRecords(recordList);
        
        String current = System.getProperty("user.dir");
        String recordFile = current + "/workspaces.json";
        File file = new File(recordFile);
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                LOGGER.error("Could not create workspace record file", ex);
                return;
            }
        }
        
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file, records);
        } catch (IOException ex) {
            LOGGER.error("Could not write workspace record file", ex);
        }
    }

    public void close() {
//        Set<URI> toClose = workspaces.keySet();
//        toClose.stream().forEach(r -> closeWorkspace(r));
//        dispatcher.close();
        this.services.clear();
        this.servicesInitialised = false;
    }

    private void processServices(Collection<Class<? extends SBWorkspaceService>> processed, Collection<Class<? extends SBWorkspaceService>> unprocessed) {
        
        int preProcessed = processed.size();

        if(preProcessed == 0) {
            processed.forEach(c -> {
               ServiceDependency[] dependencies = c.getAnnotationsByType(ServiceDependency.class);
               if(dependencies.length > 0) {
                   if(LOGGER.isDebugEnabled())
                       LOGGER.debug("Processing service: {}", c.getSimpleName());
                   processed.add(c);
               } else {
                   unprocessed.add(c);
               }
           });
            
           unprocessed.removeAll(processed);
        }

        unprocessed.stream().forEach(c -> {
            Collection<Class<? extends SBWorkspaceService>> dependencies = Arrays.asList(c.getAnnotationsByType(ServiceDependency.class))
                    .stream().map(d -> d.dependsOn()).collect(Collectors.toSet());

            if(dependencies.stream().allMatch(dep -> processed.stream().anyMatch(p -> dep.isAssignableFrom(p)))) {
                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Processing service: {}", c.getSimpleName());
                processed.add(c);
            }
        });
        
        unprocessed.removeAll(processed);
        
        int postProcessed = processed.size();
        
        if(preProcessed != postProcessed) {
            processServices(processed, unprocessed);
        } else {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Processed {} services, {} unprocessed", processed.size(), unprocessed.size());
            this.services.addAll(processed);
        } 
    }

    @Override
    public SBWorkspace getWorkspace(URI uri) {
        
        if(!workspaces.containsKey(uri)) {

            SBWorkspace ws = new DefaultSBWorkspace(uri);
            
            if(!servicesInitialised) {
                processServices( new ArrayList<>(),
                    Lookup.getDefault().lookupAll(SBWorkspaceService.class).stream()
                            .map(s -> s.getClass()).collect(Collectors.toList()));
                servicesInitialised = true;
            }
            
            List<SBWorkspaceService> serviceInstances = this.services.stream().map(service -> {
                try {
                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("Creating {}", service.getSimpleName());
                    return service.newInstance();
                    } catch (InstantiationException | IllegalAccessException ex) {
                        LOGGER.error(ex.getLocalizedMessage());
                        return null;
                    }
            }).filter(s -> s!=null).collect(Collectors.toList());

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Constructing workspace with {} services", serviceInstances.size());
            
            workspaces.put(uri, new WorkspaceRecord(ws, serviceInstances));
            serviceInstances.stream().forEach(s -> {
                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Initialising {}", s.getClass().getSimpleName());
                s.setWorkspace(ws);
            });
            
            updateRecords(workspaces.values().stream().map(r -> r.getWorkspace()).collect(Collectors.toList()));
            
            publish(new DefaultSBEvent(SBGraphEventType.ADDED, uri, null, null, null));

        }

        SBWorkspace workspace = workspaces.get(uri).getWorkspace();
        //LOGGER.trace("Returning workspace: {}",  workspace.getIdentity().toASCIIString());
//        if(getCurrentWorkspace() == null)
//           setCurrentWorkspace(uri);
        
        return workspace;
    }

    @Override
    public void closeWorkspace(URI uri) {
        if(!workspaces.containsKey(uri))
            return;

        WorkspaceRecord ws = workspaces.remove(uri);
        SBWorkspace current = getCurrentWorkspace();
        if(ws.getWorkspace() == current)
            content.remove(current);


        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Closing workspace: {}", ws.getWorkspace().getIdentity().toASCIIString());
        
        ws.getServices().stream().forEach(s -> s.close());
        ws.getWorkspace().shutdown();
        ws = null;
        publish(new DefaultSBEvent(SBGraphEventType.REMOVED, uri, null, null, null));
    }

    
    public void removeWorkspace(URI uri) {
        if(!workspaces.containsKey(uri))
            return;

        WorkspaceRecord ws = workspaces.remove(uri);
        SBWorkspace current = getCurrentWorkspace();
        if(ws.getWorkspace() == current)
            content.remove(current);

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Closing workspace: {}", ws.getWorkspace().getIdentity().toASCIIString());
        
        ws.getServices().stream().forEach(s -> s.close());
        ws.getWorkspace().shutdown();
        ws = null;
        publish(new DefaultSBEvent(SBGraphEventType.REMOVED, uri, null, null, null));
    }
    
    @Override
    public boolean hasOpenWorkspace(URI uri) {
        return workspaces.containsKey(uri);
    }

    @Override
    public Set<URI> listWorkspaces() {
        return new HashSet<>(workspaces.keySet());
    }

    @Override
    public void setCurrentWorkspace(URI workspace) {
        SBWorkspace ws = workspaces.get(workspace).getWorkspace();
        SBWorkspace existingWorkspace = lookup.lookup(SBWorkspace.class);
        if(existingWorkspace!=null)
            content.remove(existingWorkspace);
        content.add(ws);
    }

    @Override
    public SBWorkspace getCurrentWorkspace() {
        return lookup.lookup(SBWorkspace.class);
    }

    @Override
    public boolean containsWorkspace(URI workspaceId) {
        return workspaces.containsKey(workspaceId);
    }


    @Override
    public Lookup getLookup() {
        return lookup;
    }

    /*public static SBWorkspaceManager getManager() {
        return Lookup.getDefault().lookup(SBWorkspaceManager.class);
    }*/

    public void publish(SBEvent event) {
        dispatcher.publish(event);
    }

    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }

    @Override
    public <T extends SBWorkspaceService> T getService(URI workspace, Class<T> clazz) {
        if(!containsWorkspace(workspace))
            return null;
        
        return workspaces.get(workspace).getService(clazz);
    }

    private final class WorkspaceRecord {
        
        private final SBWorkspace workspace;
        private final Map<Class<? extends SBWorkspaceService>, SBWorkspaceService> services;

        public WorkspaceRecord(SBWorkspace workspace, List<SBWorkspaceService> services) {
            this.workspace =  workspace;
            this.services = new HashMap<>();
            services.stream().forEach(s -> {
                this.services.put(s.getClass(), s);
            });
        }

        public SBWorkspace getWorkspace() {
            return workspace;
        }
        
        public Collection<SBWorkspaceService> getServices() {
            return this.services.values();
        }

        
        public <T extends SBWorkspaceService> T getService(Class<T> clazz) {
            return this.services.keySet().stream()
                .filter(c -> clazz.isAssignableFrom(c))
                .map(c -> clazz.cast(this.services.get(c))).findFirst().orElse(null);
        }
    }
    
    public class WorkspacePersistenceRecord {
    
        private String directory;
        private String workspaceUri;
        public List<String> systemsContexts;

        public String getDirectory() {
            return directory;
        }

        public String getWorkspaceUri() {
            return workspaceUri;
        }

        public void setDirectory(String directory) {
            this.directory = directory;
        }

        public void setWorkspaceUri(String workspaceUri) {
            this.workspaceUri = workspaceUri;
        }

        public List<String> getSystemsContexts() {
            return systemsContexts;
        }

        public void setSystemsContexts(List<String> systemsContexts) {
            this.systemsContexts = systemsContexts;
        }

    
    }
    
    public class WorkspacePersistenceRecords {
    
        private List<WorkspacePersistenceRecord> records;

        public List<WorkspacePersistenceRecord> getRecords() {
            return this.records;
        }

        public void setRecords(List<WorkspacePersistenceRecord> records) {
            this.records = records;
        }



    }

}
