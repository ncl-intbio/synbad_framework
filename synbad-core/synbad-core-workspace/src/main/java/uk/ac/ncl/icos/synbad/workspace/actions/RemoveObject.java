/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.api.domain.*;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.SynBadAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

/**
 *
 * @author owengilfellon
 */
@SynBadAction(type = SBGraphEventType.REMOVED, clazz = SBIdentified.class)
public class RemoveObject<T extends ASBIdentified> extends AObjectAction {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveObject.class);
    private SBAction action = null;
    private final SBValued obj;
    private final Class<? extends SBValued> parentClass;
 
 
    /**
     * 
     * @param identity
     * @param type
     * @param parentId Parent URI ID, or null if object is Top Level
     * @param parentType Parent URI type, or null if object is Top Level
     * @param workspace
     * @param contexts 
     */
    public RemoveObject(URI identity, URI type, URI parentId, URI parentType, SBWorkspace workspace, URI[] contexts) {
        super(SBGraphEventType.REMOVED,  
            identity, 
            type,
            parentId, 
            parentType, 
            workspace,
            contexts);

        SBObjectFactoryManager m = SBObjectFactoryManager.get();
        this.obj = ws.getObject(identity, m.getClassFromType(type), contexts);
        this.parentClass = parentType != null ? m.getClassFromType(parentType) : null;
    }

    @Override
    public Collection<SBEvent> perform(SBCommandManager subscriber) {
        if(this.action == null)
            this.action = getAction(object, rdfType, workspace, contexts);
        
        SBEvent e = new DefaultSBGraphEvent<>(
            SBGraphEventType.REMOVED, 
            SBGraphEntityType.NODE,
            obj.getIdentity(), 
            obj.getClass(), 
            workspace, 
            SBWorkspace.class, 
            parent, 
            parentClass, 
            contexts);
        
        List<SBEvent> evts = new LinkedList<>();
        evts.addAll(action.perform(subscriber));
        evts.add(e);
        if(LOGGER.isDebugEnabled()) {
            SBIdentity objectId = f.getIdentity(object);
            SBIdentity t = f.getIdentity(rdfType);

            LOGGER.debug("{}: {}:{}", SBGraphEventType.REMOVED, objectId.getIdentity().toASCIIString(), t.getDisplayID());
        }

        return evts;
    }
    

    @Override
    public Collection<SBEvent> getEvents(boolean undo) {
        
        List<SBEvent> events = new LinkedList<>();
        
        SBEvent e = new DefaultSBGraphEvent<>(
            undo ? SBGraphEventType.ADDED : SBGraphEventType.REMOVED, 
            SBGraphEntityType.NODE,
            obj.getIdentity(), 
            obj.getClass(), 
            workspace, 
            SBWorkspace.class, 
            parent, 
            parentClass, 
            contexts);

        events.addAll(action.getEvents(undo));
        events.add(undo ? 0 : events.size(), e);

        return events;
       
    }
    
    private SBAction getAction(URI identity, URI type, URI workspaceId, URI[] contexts) {
        
        if(obj == null)
            LOGGER.error("Could not retrieve {}:{}", identity.toASCIIString());
        
        List<SBAction> actions = new LinkedList<>();
        
        for(String p : obj.getPredicates()) {
            actions.addAll(obj.getValues(p).stream().map(v -> 
                    new RemoveValue(identity, p, v, type, ws, contexts))
                    .collect(Collectors.toSet()));
        }

        actions.addAll(Stream.concat(
            ws.incomingEdges(obj, contexts).stream(),
            ws.outgoingEdges(obj, contexts).stream())
                .map(e -> new RemoveEdge(e.getFrom(), e.getEdge().toASCIIString(), e.getTo(), workspaceId, contexts))
                .collect(Collectors.toSet()));

        ws.getRdfService().getStatements(object.toASCIIString(), null, null, ws.getSystemContextIds(contexts))
                .forEach(e -> actions.add(new RemoveEdge(object, e.getPredicate(), e.getObject().asURI(), ws.getIdentity(), ws.getSystemContextIds(contexts))));

        ws.getRdfService().getStatements(null, null, SBValue.parseValue(object), ws.getSystemContextIds(contexts))
                .forEach(e -> actions.add(new RemoveEdge(SBIdentityHelper.getURI(e.getSubject()), e.getPredicate(), object, ws.getIdentity(), ws.getSystemContextIds(contexts))));
//
//
//        actions.addAll(Stream.concat(
//            ws.incomingEdges(obj, ws.getSystemContextIds(contexts)).stream(),
//            ws.outgoingEdges(obj, ws.getSystemContextIds(contexts)).stream())
//                .map(e -> new RemoveEdge(e.getFrom(), e.getEdge().toASCIIString(), e.getTo(), workspaceId, ws.getSystemContextIds(contexts)))
//                    .collect(Collectors.toSet()));
        
        return new SBActionMacro(actions, "RemoveObj [" + identity.toASCIIString() + "]");
    }
}
