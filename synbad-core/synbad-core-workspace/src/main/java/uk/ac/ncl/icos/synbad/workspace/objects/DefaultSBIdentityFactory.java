/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBIdentityFactory implements SBIdentityFactory {
    
    private final LoadingCache<URI, SBIdentity> uriCache = CacheBuilder.newBuilder()
        .expireAfterAccess(20, TimeUnit.MINUTES)
        .build(new CacheLoader<URI, SBIdentity>() {
                @Override
                public SBIdentity load(URI uri) throws Exception {
                    try {
                        return SBIdentity.getIdentity(uri);
                    } catch (SBIdentityException e) {
                        LOGGER.error(e.getLocalizedMessage());
                        return null;
                    }
                }
            }
        );
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBIdentityFactory.class);
    private final SBWorkspace ws;
    private final DefaultIdentityFactory f = new DefaultIdentityFactory();
    
    public DefaultSBIdentityFactory(SBWorkspace ws) {
        this.ws = ws;
    }
    
    private SBIdentity retrieveIdentity(URI uri) {
        //LOGGER.trace("Retrieving cached Identity: {}", uri.toASCIIString());
        try {
            synchronized (LOGGER) {
                if(uri == null) {
                    LOGGER.error("URI cannot be null");
                    return null;
                }
                return uriCache.get(uri);
            }
        } catch (ExecutionException ex) {
            return null;
        }
    }
    
    private SBIdentity generateUniqueIdentity(SBIdentity identity) {

        String oldPersistentId = identity.getPersistentId().toASCIIString();
        StringBuilder b = new StringBuilder();

        if(!SBIdentityHelper.getBaseDisplayIdFromIncrementing(identity.getDisplayID()).equals(identity.getDisplayID())) {
            int increment = SBIdentityHelper.getIncrementFromIncrementing(identity.getDisplayID());
            String persistentId = oldPersistentId.substring(0, oldPersistentId.length() - ("_" + increment).length() );
            b.append(persistentId.concat("_") + ++increment);
        } else {
            b.append(oldPersistentId).append("_1");
        }

        if (identity.getVersion() != null && !identity.getVersion().isEmpty())
            b.append("/").append(identity.getVersion());

        try {
            return SBIdentity.getIdentity(SBIdentityHelper.getURI(b.toString()));
        } catch (SBIdentityException ex) {
            return null;
        }
    }
    
    @Override
    public SBIdentity getUniqueIdentity(SBIdentity identity) {

        SBIdentity uniqueIdentity = generateUniqueIdentity(identity);
        
        while (uniqueIdentity != null && 
                (ws.containsIdentity(uniqueIdentity.getIdentity(), ws.getContextIds()) || 
                uriCache.asMap().containsKey(uniqueIdentity.getIdentity()))) {
            uniqueIdentity = generateUniqueIdentity(uniqueIdentity);
        }
        
        if(uniqueIdentity != null)
            uriCache.put(uniqueIdentity.getIdentity(), uniqueIdentity);
            
        return uniqueIdentity;
    }
    
    @Override
    public SBIdentity getIdentity(String prefix, String displayId) {        
        URI uri = SBIdentityHelper.getURI(prefix + "/" + displayId);
        
        if (uriCache.asMap().containsKey(uri)) {
            return retrieveIdentity(uri);
        }

        SBIdentity identity = new SBIdentity(prefix, displayId, uri);
        uriCache.put(uri, identity);
        //LOGGER.trace("Created Identity: {}", identity.getIdentity().toASCIIString());
        return identity;
    }
    
    @Override
    public SBIdentity getIdentity(String prefix, String displayId, String version) {
        
        URI uri = SBIdentityHelper.getURI(prefix + "/" + displayId + "/" + version);
        
        if (uriCache.asMap().containsKey(uri)) {
            return retrieveIdentity(uri);
        }

        SBIdentity identity = new SBIdentity(prefix, displayId, version, uri);
        uriCache.put(uri, identity);
        //LOGGER.trace("Created Identity: {}", identity);
        return identity;
    }
    
    @Override
    public SBIdentity getIdentity(String prefix, String parentDisplayId, String displayId, String version) {
        
        URI uri = SBIdentityHelper.getURI(prefix + "/" + parentDisplayId + "_" + displayId + "/" + version);
        
        if (uriCache.asMap().containsKey(uri)) {
            return retrieveIdentity(uri);
        }

        SBIdentity identity = SBIdentity.getIdentity(prefix, parentDisplayId, displayId, version);

        uriCache.put(uri, identity);
        //LOGGER.trace("Created Identity: {}", identity);
        return identity;
    }
    
    @Override
    public SBIdentity getIdentity(URI identity) {
        return retrieveIdentity(identity);
    }

    @Override
    public SBIdentity getNextIdentity(String prefix, String displayId, String version) {
        return getIdentity(prefix, displayId, SBIdentityHelper.incrementVersion(version));
    }

    @Override
    public SBIdentity getNextIdentity(String prefix, String parentDisplayId, String displayId, String version) {
        return getIdentity(prefix, parentDisplayId, displayId, SBIdentityHelper.incrementVersion(version));
    }

    @Override
    public SBIdentity getNextIdentity(SBIdentity identity) {
        String version = identity.getVersion();
        version = version != null && !version.isEmpty() ? SBIdentityHelper.incrementVersion(version) : "1.0";
        
//        if(SBIdentityHelper.isValidChildPersistentId(identity.getIdentity())) {
//            String parentId =  SBIdentityHelper.getParentDisplayIdFromId(identity.getIdentity());
//            String childId = SBIdentityHelper.getChildDisplayIdFromId(identity.getIdentity());
//            SBIdentity id = getIdentity(identity.getUriPrefix(), parentId.concat("/").concat(childId), version);
//            return id;
//        }
        synchronized (LOGGER) {
            try {
                return uriCache.get(URI.create(identity.getPersistentId() + "/" + version));
            } catch (ExecutionException e) {
                LOGGER.error(e.getLocalizedMessage());
                return null;
            }
        }
    }
}
