/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SBWorkspaceValidator {
    
    private final Set<SBValidationRule> rules;
    private final SBWorkspace workspace;
 
    public SBWorkspaceValidator(SBWorkspace workspace) {
        this.rules = new HashSet<>();
        this.workspace = workspace;
    }
    
    public void addRule(SBValidationRule rule) {
        this.rules.add(rule);
    }
    
    public void addRules(Collection<SBValidationRule> rules) {
        this.rules.addAll(rules);
    }
    
    public void clearRules() {
        this.rules.clear();
    }
    
    public Set<SBValidationRule> getValidationErrors( SBIdentified object ) {
        Set<SBValidationRule> rules;
        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(workspace, workspace.getContextIds())) {
            rules = this.rules.stream().filter(r -> !r.validate(graph, object)).collect(Collectors.toSet());
        }
        return rules;
    }
}
