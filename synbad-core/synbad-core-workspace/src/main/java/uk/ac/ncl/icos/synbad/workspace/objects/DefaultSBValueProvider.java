/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 *
 * @author owengilfellon
 */
 

public class DefaultSBValueProvider implements SBValueProvider, SBSubscriber {

    private static final long serialVersionUID = -6640463745711860958L;
    transient private SBWorkspace ws;
    transient private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBValueProvider.class);

    private final URI objectIdentity;
    private final Map<String, List<SBValue>> values;
    private final URI[] contexts;
    private final int contextsHashCode;

    public DefaultSBValueProvider(URI objectIdentity, SBWorkspace workspaceIdentity, URI[] contexts) {
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Constructed value provider for: [ {} ] in [ {} ] with [ {} ] contexts", objectIdentity.toASCIIString(), workspaceIdentity.getIdentity().toASCIIString(), contexts.length);
        this.values = new HashMap<>();
        this.contexts = contexts;
        this.ws = workspaceIdentity;
        this.objectIdentity = objectIdentity;
        this.contextsHashCode = Arrays.hashCode(contexts) * 7 *  objectIdentity.hashCode();
    }

    @Override
    public void close() {
        this.ws.getDispatcher().unsubscribe(this);
        this.values.clear();
    }
    
    @Override
    public URI[] getContexts() {
        return contexts;
    }

    @Override
    public Set<String> getPredicates() {
        return values.keySet();
    }

    @Override
    public List<SBValue> getValues(String predicate) {
        return values.containsKey(predicate) ? values.get(predicate)
                .stream().filter(v -> !v.isURI() || !ws.containsObject(v.asURI(), SBValued.class, contexts))
                .collect(Collectors.toList()): new ArrayList<>();
    }

    @Override
    public void addValue(String predicate, SBValue value) {
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Adding value: [ {} ] -> [ {} ] ", predicate, value);
        
        if(!values.containsKey(predicate))
            values.put(predicate,  new ArrayList<>());

        if(!values.get(predicate).contains(value))
            values.get(predicate).add(value);
    }

    @Override
    public void setValues(String predicate, Collection<SBValue> valueCollection) {

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Setting values: [ {} ] -> [ {} ] ", predicate, valueCollection.size());
        
        clearValues(predicate);
        values.put(predicate,  new ArrayList<>());
        values.get(predicate).addAll(valueCollection);
    }

    @Override
    public void removeValue(String predicate, SBValue value) {

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Removing value: [ {} ] -> [ {} ] ", predicate, value);

        if(!values.containsKey(predicate))
            return;

        values.get(predicate).remove(value);

        if(values.get(predicate).isEmpty())
            values.remove(predicate);

    }

    @Override
    public void clearValues(String predicate) {
        values.remove(predicate);
    }

    @Override
    public String toString() {
        return "ValueProvider[ ".concat(objectIdentity.toASCIIString()).concat(" ]");
    }
    
    

    @Override
    public void onEvent(SBEvent e) {
        /*
        LOGGER.trace("Received event: [ {} ]", e);

        Statement statement = (Statement)e.getData();

        if(e.getType() == SBGraphEventType.ADDED) {
           if(!statement.getObject().isURI() || !ws.containsObject(statement.getObject().asURI(), SBValued.class, contexts)) {
                LOGGER.trace("Adding value: " + statement.getSubject() + " : " + statement.getPredicate() + " : " + statement.getObject().toString());
                addValue(statement.getPredicate(), statement.getObject());
           }
        } else if (e.getType() == SBGraphEventType.REMOVED) {

            if( statement.getPredicate().equals(SynBadTerms.Rdf.hasType)) {
                LOGGER.trace("Closing value provider: " + statement.getSubject() + " : " + statement.getPredicate() + " : " + statement.getObject().toString());
                close();
            }

            if(!statement.getObject().isURI() || !ws.containsObject(statement.getObject().asURI(), SynBadObject.class, contexts)) {
                LOGGER.trace("Removing value: " + statement.getSubject() + " : " + statement.getPredicate() + " : " + statement.getObject().toString());
                removeValue(statement.getPredicate(), statement.getObject());
            }
        }*/
    }
    
    @Override
    public boolean equals(Object obj) {

        if(obj == null)
            return false;

        if(obj == this)
            return true;

        if(!(obj instanceof DefaultSBValueProvider))
            return false;

        DefaultSBValueProvider u = (DefaultSBValueProvider) obj;


        return Arrays.asList(contexts).containsAll(Arrays.asList(u.contexts)) 
                && contexts.length == u.contexts.length
                && objectIdentity.equals(u.objectIdentity);

    }            

    @Override
    public int hashCode() {
        return contextsHashCode;
    }

    class ValueProviderFilter implements SBEventFilter {
        
        private final String objectIdentity;

        public ValueProviderFilter(URI objectId) {
            this.objectIdentity = objectId.toASCIIString();
        }
        
        @Override
        public boolean test(SBEvent event) {
            
            if(!SBStatement.class.isAssignableFrom(event.getData().getClass()))
                return false;
            
            return ((SBStatement)event.getData()).getSubject().equals( objectIdentity);
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;

            if (obj == this)
                return true;

            if (!(obj instanceof ValueProviderFilter))
                return false;

            ValueProviderFilter r = (ValueProviderFilter) obj;
            return r.objectIdentity.equals(objectIdentity);
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + Objects.hashCode(this.objectIdentity);
            return hash;
        }
        
    }

}