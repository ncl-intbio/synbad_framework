/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import java.net.URI;
import java.util.Set;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.event.SBEventSource;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;

/**
 * Workspace Manager controls the opening and closing of workspaces, and the management
 * of workspace services.
 * @author Owen
 */
public interface SBWorkspaceManager extends Lookup.Provider, AutoCloseable, SBEventSource {

    /**
     * Closes the workspace with the provided identity, if one exists. All
     * subscribers are unsubscribed, all services are stopped and all other
     * resources are freed.
     * @param uri 
     */
    void closeWorkspace(URI uri);

    /**
     * 
     * @param uri
     * @return an existing workspace with the provided identity, or creates and
     * returns a workspace with the provided identity if one does not currently
     * exist.
     */
    SBWorkspace getWorkspace(URI uri);

    /**
     * 
     * @param uri
     * @return true if there is an existing workspace with the provided identity.
     */
    boolean hasOpenWorkspace(URI uri);
    
    /**
     * @param <T> the type of the service
     * @param workspace the workspace to which the service is registered against
     * @param clazz the class of the service, typed according to <code>T</code>
     * @return the service of the provided class, registered to the workspace with
     * the provided identity. If no such service exists, returns null.
     */
    <T extends SBWorkspaceService> T getService(URI workspace, Class<T> clazz);

    /**
     * 
     * @return a list of all workspaces managed by the Workspace Manager.
     */
    Set<URI> listWorkspaces();
    
    /**
     * 
     * @return the current, default, workspace
     */
    public SBWorkspace getCurrentWorkspace();
        
    /**
     * Sets an existing workspace as the current, default workspace.
     * @param workspace 
     */
    public void setCurrentWorkspace(URI workspace);

    /**
     * 
     * @param workspaceId
     * @return true, if the manager owns a workspace with the provided identity.
     */
    public boolean containsWorkspace(URI workspaceId) ;
}
