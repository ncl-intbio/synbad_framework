/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfFormat;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author Owen
 */
public class SBJsonExporter implements SBWorkspaceExporter {
    
    private final Logger LOGGER = LoggerFactory.getLogger(SBJsonExporter.class);
 
    @Override
    public void export(OutputStream os, SBWorkspace workspace, URI[] contexts) {

        SBRdfService service = workspace.getRdfService();

        service.export(os, SBRdfFormat.JSON, contexts);
        try {
            os.flush();
        } catch (IOException ex) {
            LOGGER.error("Could not write to output stream", ex);
        }
    }
}
