/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.jena.ext.com.google.common.collect.Streams;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.StatementImpl;
import org.apache.jena.rdfxml.xmlinput.JenaReader;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.shared.JenaException;
import org.apache.jena.tdb.TDBFactory;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfFormat;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.event.DefaultSBEvent;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.inference.SBInferenceRule;
import uk.ac.ncl.icos.synbad.workspace.objects.DefaultSBIdentityFactory;

/**
 *
 * @author owen
 */
public abstract class ASBRdfService implements SBRdfService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ASBRdfService.class);
    private final Dataset d;
    private final Set<URI> contexts;

    //private final Reasoner reasoner;

    public ASBRdfService(Dataset d) {
        this.d = d;
        this.contexts = new HashSet<>();

//        StringBuilder b = new StringBuilder();
//        Lookup.getDefault().lookupAll(SBInferenceRule.class).forEach(s -> b.append(s.getRules()));
//        String rules = b.toString();
//        this.reasoner = rules.isEmpty() ? null : new GenericRuleReasoner(Rule.parseRules(rules));

//        if(LOGGER.isInfoEnabled())
//            LOGGER.info("Created RDF service for {}", workspaceId.toASCIIString());
    }

//    public ASBRdfService(Dataset d) {
//        this.workspaceId = ws;
//        this.d = d;
//        this.ws = null;
//        this.contexts = new HashSet<>();
//    }
    
    private void updateContexts(URI[] contexts, boolean add) {
        for(URI context : contexts) {
            updateContexts(context, add);
        }
    }
    
    private void updateContexts(URI context, boolean add) {
        synchronized(this) {
            if(add)
                contexts.add(context);
            else {
                contexts.remove(context);
            }
        }
    }

    public static String getDirectoryName(URI workspaceId) {
        String workspace = workspaceId.getAuthority() + workspaceId.getPath();
        workspace = workspace.replaceAll("\\/", "_").replaceAll("\\.", "_");
        return workspace;
    }

    public Dataset getDataset() {
        return d;
    }

    @Override
    public URI[] getContexts() {
        synchronized(this) {
           return contexts.toArray(new URI[] {}); 
        }
    }
    
    @Override   
    public void createContext(URI context) {
        if(d == null)
            return;
        
        d.begin(ReadWrite.WRITE);
        
        if(d.containsNamedModel(context.toASCIIString())) {
            d.abort();
            d.end();
            return;
        }

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Creating context: {}", context.toASCIIString());

        Model m = ModelFactory.createDefaultModel();
        //InfModel infModel = ModelFactory.createInfModel(reasoner,m);
        updateContexts(context, true);

        d.addNamedModel(context.toASCIIString(), m);

        d.commit();
        d.end();
    }

    @Override
    public void removeContext(URI context) {
        d.begin(ReadWrite.WRITE);
        if(!d.containsNamedModel(context.toASCIIString())) {
            d.abort();
            d.end();
            return;
        }
        
        updateContexts(context, false);
        d.removeNamedModel(context.toASCIIString());
        d.commit();
        d.end();
    }
    
    @Override
    public boolean addRdf(File xmlFile, URI... contexts) {
        try {
            String id = "file:/" + xmlFile.getAbsolutePath();
            Set<URI> groupedContexts = new HashSet<>();
            groupedContexts.add(SBIdentityHelper.getURI(id));
            if(contexts.length > 0) {
                groupedContexts.addAll(Arrays.asList(contexts));
            }
            groupedContexts.forEach(c -> updateContexts(c, true));
            return addRdf(new FileInputStream(xmlFile), groupedContexts.toArray(new URI[]{}));
        } catch (FileNotFoundException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return false;
        }
    }  

    @Override
    public boolean addRdf(InputStream stream, URI... contexts) {

        d.begin(ReadWrite.WRITE);
        
        for(URI uri : contexts) {
            
            updateContexts(uri, true);
            
            if(!d.containsNamedModel(uri.toASCIIString())) {
                Model importedModel = ModelFactory.createDefaultModel();
                new JenaReader().read(importedModel, stream, uri.toASCIIString());
                //InfModel infModel = ModelFactory.createInfModel(reasoner, importedModel);
                d.addNamedModel(uri.toASCIIString(), importedModel);
                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Added {} statements in new context: {}", importedModel.size(), uri.toASCIIString());
            } else {
                Model existingModel = d.getNamedModel(uri.toASCIIString());
                Model updatedModel = ModelFactory.createDefaultModel();
                updatedModel.add(existingModel);
                try {
                    new JenaReader().read(updatedModel, stream, uri.toASCIIString());
                } catch (JenaException ex) {
                    d.abort();
                    d.end();
                    LOGGER.error(ex.getLocalizedMessage());
                    return false;
                }
                d.replaceNamedModel(uri.toASCIIString(), updatedModel);
                Model additions = updatedModel.difference(existingModel);
//                SBDispatcher dispatcher = getWorkspace().getDispatcher();
//                additions.listStatements().toList().stream().map(s ->
//                    new DefaultSBEvent(
//                        SBGraphEventType.ADDED,
//                        new SBStatement(s.getSubject().getURI(), s.getPredicate().getURI(), SBValue.parseValue(s.getObject().toString())),
//                        getWorkspaceId(), null, contexts))
//                    .forEach(e -> dispatcher.publish(e));
//                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Added {} statements in existing context: {}", additions.size(), uri.toASCIIString());
            }
        }
        d.commit();
        d.end();
        return true;
    }

    @Override
    public boolean export(OutputStream out, SBRdfFormat format, URI... contexts) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Exporting RDF for contexts...");


        Arrays.asList(contexts).stream().map(u -> u.toASCIIString()).forEach(s -> LOGGER.debug("\t...{}", s));
        Dataset ds = DatasetFactory.create();
        d.begin(ReadWrite.READ);
      
        for(URI context : contexts) {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Adding context... {}", context.toASCIIString());
            
            if(!d.containsNamedModel(context.toASCIIString()))
                LOGGER.error("Could not find context: [ {} ]", context.toASCIIString());
            else
                ds.addNamedModel(context.toASCIIString(), d.getNamedModel(context.toASCIIString()));
        }
        
        d.end();
        Model toExport = ds.getUnionModel();
        toExport.setNsPrefix(UriHelper.sbol.getPrefix(), UriHelper.sbol.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.synbad.getPrefix(), UriHelper.synbad.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.biopax.getPrefix(), UriHelper.biopax.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.dcterms.getPrefix(), UriHelper.dcterms.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.prov.getPrefix(), UriHelper.prov.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.so.getPrefix(), UriHelper.so.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.sbo.getPrefix(), UriHelper.sbo.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.rdf.getPrefix(), UriHelper.rdf.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.rdfs.getPrefix(), UriHelper.rdfs.getNamespaceURI());
        toExport.setNsPrefix(UriHelper.vpr.getPrefix(), UriHelper.vpr.getNamespaceURI());

        switch (format) {
            case JSON:
                RDFDataMgr.write(out, toExport, RDFFormat.JSONLD_FLAT);
                break;
            case XML:
                RDFDataMgr.write(out, toExport, RDFFormat.RDFXML_PLAIN);
                break;
        }


        return true;
    }
    
    @Override
    public boolean removeRdf(File f) {
        d.begin(ReadWrite.WRITE);
        URI context = SBIdentityHelper.getURI("file:/" + f.getAbsolutePath());
        updateContexts(context, false);
        d.removeNamedModel(context.toASCIIString());
        d.commit();
        d.end();
        return true;
    }

    private Collection<String> getPredicatesFromModel(String subject, Model model) {

        Resource r_subject = subject == null ? null : model.getResource(subject);

        //Selector selector = new SimpleSelector();
        StmtIterator i = model.listStatements(r_subject, (Property) null, (RDFNode) null);

        if(!i.hasNext())
            return Collections.EMPTY_LIST;

        return Streams.stream(i).map(
                statement -> statement.getPredicate().getURI())
                .collect(Collectors.toList());
    }
    
    private Collection<SBStatement> getStatementsFromModel(String subject, String predicate, SBValue object, Model model) {
        
        Resource r_subject = subject == null ? null : model.getResource(subject);
        Property r_predicate = predicate == null ? null : model.getProperty(predicate);
        RDFNode r_object = getValue(object);

        //Selector selector = new SimpleSelector();
        StmtIterator i = model.listStatements(r_subject, r_predicate, r_object);

        if(!i.hasNext())
            return Collections.EMPTY_LIST;

        return Streams.stream(i).map(
            statement -> new SBStatement(
                    statement.getSubject().getURI(),
                    statement.getPredicate().getURI(),
                    SBValue.parseValue(
                            statement.getObject().isResource() ?
                            statement.getObject().asResource().getURI() :
                            statement.getObject().asLiteral().getLexicalForm())))
        .collect(Collectors.toList());
    }

    @Override
    public Collection<String> getPredicates(String subject,  URI... contexts) {
        URI[] statementsContext = contexts.length > 0 ? contexts : getContexts();
        final Set<String> predicates = new HashSet<>();
        for(int i = 0; i < statementsContext.length; i++) {
            d.begin(ReadWrite.READ);
            if(statementsContext[i] == null) {
                LOGGER.error("Context is null for {} : {} : {}", subject);
            } else if(d.containsNamedModel(statementsContext[i].toASCIIString())) {
                Model model = d.getNamedModel(statementsContext[i].toASCIIString());
                predicates.addAll(getPredicatesFromModel(subject, model));
            }
            d.end();
        }
        return predicates;
    }
    @Override
    public Collection<SBStatement> getStatements(String subject, String predicate, SBValue object, URI... contexts) {
        URI[] statementsContext = contexts.length > 0 ? contexts : getContexts();
        final Set<SBStatement> statements = new HashSet<>();
        for(int i = 0; i < statementsContext.length; i++) {
            d.begin(ReadWrite.READ);
            if(statementsContext[i] == null) {
                LOGGER.error("Context is null for {} : {} : {}", subject, predicate, object);
            } else if(d.containsNamedModel(statementsContext[i].toASCIIString())) {
                Model model = d.getNamedModel(statementsContext[i].toASCIIString());
                statements.addAll(getStatementsFromModel(subject, predicate, object, model));
            }
            d.end();
        }

        return statements;
    }
    
    @Override
    public Set<SBValue> getValues(String subject, String predicate, URI... contexts) {
        return getStatements(subject, predicate, null, contexts).stream()
            .map(SBStatement::getObject)
            .collect(Collectors.toSet());
    }

    @Override
    public SBValue getValue(String subject, String predicate, URI... contexts) {
        return getStatements(subject, predicate, null, contexts).stream()
                .map(SBStatement::getObject).findFirst().orElse(null);
    }
    
    @Override
    public boolean addStatements(Collection<SBStatement> statements, URI... contexts) {

        final Set<org.apache.jena.rdf.model.Statement> addedStatements = new HashSet<>();

        d.begin(ReadWrite.WRITE);
        
        updateContexts(contexts, true);

        Set<String> contextsAsStrings = Arrays.asList(contexts).stream()
                .map(u -> u.toASCIIString())
                .collect(Collectors.toSet());
        
        if(contextsAsStrings.isEmpty()) {
             this.contexts.stream().map(c -> c.toASCIIString()).forEach(n -> {
                addedStatements.addAll(addStatementsToModel(statements, n));
            });
        } else {
            addedStatements.addAll(contextsAsStrings.stream()
                .map(model -> addStatementsToModel(statements, model))
                .flatMap(sts -> sts.stream())
                .collect(Collectors.toSet()));
        }

        if(addedStatements.isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("No statements added");
            d.abort();
            d.end();
            return false;
        } else {
            d.commit();
            d.end();
            return true;
        }
            
    }

    @Override
    public boolean removeStatements(Collection<SBStatement> statements, URI... contexts) {
        List<org.apache.jena.rdf.model.Statement> toRemove = new ArrayList<>();

        String[] contextsAsStrings = contexts == null? null : Arrays.asList(contexts).stream()
                .filter(u -> u != null)
                .map(u -> u.toASCIIString())
                .collect(Collectors.toSet()).toArray(new String[]{});

        d.begin(ReadWrite.WRITE);
        if(contextsAsStrings == null || contextsAsStrings.length == 0) {
            toRemove = removeStatementsFromModel(statements, d.getDefaultModel());
        } else {
            toRemove = Arrays.asList(contextsAsStrings).stream()
                .map(d::getNamedModel)
                .map(model -> removeStatementsFromModel(statements, model))
                .flatMap(sts -> sts.stream())
                .collect(Collectors.toList());
        }

        if(toRemove.isEmpty()) {
            LOGGER.warn("No statements removed");
            d.abort();
            d.end();
            return false;
        } else {
            for(URI context:  contexts) {
                if(!d.containsNamedModel(context.toASCIIString())) {
                    updateContexts(context, false);
                }
            }
            d.commit();
            d.end();
            return true;
        }
    }
    
    private List<org.apache.jena.rdf.model.Statement> addStatementsToModel(Collection<SBStatement> statements, String context) {
        
        boolean contextExists = this.d.containsNamedModel(context);
        Model m = contextExists ? this.d.getNamedModel(context) : ModelFactory.createDefaultModel();
        List<org.apache.jena.rdf.model.Statement> toAdd = new ArrayList<>();

        for (SBStatement statement : statements) {

            Resource r = m.createResource(statement.getSubject());
            Property p = m.createProperty(statement.getPredicate());
            RDFNode o;

            if(statement.getObject() == null) {
               o = null;
            } else if(statement.getObject().isURI()) {
               o = m.createResource(statement.getObject().asURI().toASCIIString());
            } else {
               o = getValue(statement.getObject());
            }
            
            org.apache.jena.rdf.model.Statement s = new StatementImpl(r, p, o);
            
            if(!m.contains(s) && !toAdd.contains(s)) {
                toAdd.add(s);
            } else if (LOGGER.isTraceEnabled()){
                LOGGER.trace("Already contains statement: {}", statement);
            }
        }
        
        m.add(toAdd);
        
        if(!contextExists) {
            d.addNamedModel(context, m);
        }
        
        return toAdd;
    }
    
    private List<org.apache.jena.rdf.model.Statement> removeStatementsFromModel(Collection<SBStatement> statements, Model model) {
        
        List<org.apache.jena.rdf.model.Statement> toAdd = new ArrayList<>();

        for (SBStatement statement : statements) {
            
            Resource r = model.createResource(statement.getSubject());
            Property p = model.createProperty(statement.getPredicate());
            RDFNode o;

            if(statement.getObject() == null) {
               o = null;
            } else if(statement.getObject().isURI()) {
               o = model.createResource(statement.getObject().asURI().toASCIIString());
            } else {
               o = getValue(statement.getObject());
            }

            toAdd.add(new StatementImpl(r, p, o));
        }
        
        model.remove(toAdd);

       return toAdd;
        
    }

    private RDFNode getValue(SBValue object) {
        
        Model m = d.getDefaultModel();
        
        RDFNode r_object = null;
            
        if(object != null) {
            if(object.isURI()) {
                r_object = m.createResource(object.asString());
            } else if(object.isBoolean()) {
                r_object = m.createLiteral(""+object.asBoolean());
            } else if (object.isDouble()) {
                r_object = m.createLiteral(""+object.asDouble());
            } else if (object.isInt()) {
                r_object = m.createLiteral(""+object.asInt());
            } else if (object.isQName()) {
               r_object = m.createResource(object.asQName().getNamespaceURI());
            } else if (object.isString()) {
               r_object = m.createTypedLiteral(object.asString());
            } 
        }
        
        if(object != null && r_object == null)
            LOGGER.warn("Parsed value for {} is null", object);

        return r_object;
    }

    @Override
    public void shutdown() {   
        LOGGER.info("Shutdown RDF Service");
        TDBFactory.release(this.d);
        this.d.close();
     }
}
