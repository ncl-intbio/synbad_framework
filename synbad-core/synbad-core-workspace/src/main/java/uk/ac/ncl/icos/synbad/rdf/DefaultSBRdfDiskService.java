/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf;


import java.net.URI;
import org.apache.jena.tdb.TDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBRdfDiskService  extends ASBRdfService{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBRdfDiskService.class);

//    public DefaultSBRdfDiskService(SBWorkspace workspace) {
//        super(workspace,  TDBFactory.createDataset(getDirectoryName(workspace.getIdentity())));
//    }

    public DefaultSBRdfDiskService(URI workspace) {
        super(TDBFactory.createDataset(getDirectoryName(workspace)));
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /*
    @Override
    public void shutdown() {   
        LOGGER.info("Shutdown RDF TDB Service");
        getDataset().close();
        TDBFactory.release(getDataset());
        super.shutdown();
    }*/
}
