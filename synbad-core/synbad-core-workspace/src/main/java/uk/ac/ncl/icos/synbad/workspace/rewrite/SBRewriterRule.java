/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;

/**
 *
 * @author owengilfellon
 */
public interface SBRewriterRule<N extends SBValued, E extends SBEdge, G> {

    void apply(G g);

    boolean shouldApply(G g, SBEvent e);

    SBAction apply(G g, SBEvent e);

    Class<N> getRuleClass();

}
