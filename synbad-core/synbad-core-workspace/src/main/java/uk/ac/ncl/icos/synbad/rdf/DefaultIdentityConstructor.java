/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.rdf.SBEntityConstructor;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class DefaultIdentityConstructor implements SBEntityConstructor<SBIdentity> {

        @Override
        public SBIdentity apply(String t, SBWorkspace u, URI[] contexts) {
            URI uri = SBIdentityHelper.getURI(t);
            return uri == null ? null : u.getIdentityFactory().getIdentity(uri);
        }
    }