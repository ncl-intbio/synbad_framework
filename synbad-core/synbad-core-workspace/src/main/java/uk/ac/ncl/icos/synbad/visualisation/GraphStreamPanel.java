/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.visualisation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.j2dviewer.J2DGraphRenderer;
import org.graphstream.ui.layout.springbox.implementations.SpringBox;
import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.graph.SBGlobalNodeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;

/**
 *
 * @author owengilfellon
 */
public abstract class GraphStreamPanel<N, E, G extends SBGraph<?, ?>> extends JPanel implements ViewerListener, SBSubscriber {

    static final Logger logger = LoggerFactory.getLogger(GraphStreamPanel.class);
    
    private  ViewerPipe fromViewer;
    private  Viewer gsViewer;
    private  DefaultView gsView;
    protected Graph gsGraph;
    private  G view;
    protected Map<E, Edge> edgeMap;
    private  Map<String, N> objMap;
    private boolean loop = true;
    private String rootId = null;
    /**
     * Creates new form HierarchicalGraphPanel
     */
    
    public GraphStreamPanel() {
        setUpPanel();
    }

    public G getView() {
        return view;
    }
    
    public final void setUpPanel() {

        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");


        initComponents();
        this.edgeMap = new HashMap<>();
        this.objMap = new HashMap<>();

        // Create graph
        
        gsGraph = new MultiGraph("Visualisation");
        gsGraph.setStrict(false);
        setUpStyles(gsGraph);
    }
    
    public abstract void setUpStyles(Graph graph);

    public void renderPanel() {
        gsViewer = new Viewer(gsGraph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        gsViewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);
        gsViewer.enableAutoLayout(new SpringBox());
        
        fromViewer = gsViewer.newViewerPipe();
        fromViewer.addViewerListener(this);
        gsView = new DefaultView(gsViewer, TOOL_TIP_TEXT_KEY,new J2DGraphRenderer());

        // View preferences
        
        gsView.setPreferredSize(new Dimension(1280, 720));
        gsView.getCamera().setAutoFitView(true);
     //   gsView.getCamera().setViewPercent(5.0);

        // Add and fill panel
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0; 
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        add(gsView, c);

        gsView.setVisible(true);
    }
    
    protected String getPrettyName(Object node) {
        if (SBIdentified.class.isAssignableFrom(node.getClass()))
            return ((SBIdentified)node).getDisplayId();
        else if (SBEdge.class.isAssignableFrom(node.getClass())) {
           SBEdge edge = ((SBEdge)node);
           return getPrettyName(edge.getEdge()) + ": " + getPrettyName(edge.getFrom()) + " -> " + getPrettyName(edge.getTo());
        } else if (URI.class.isAssignableFrom(node.getClass()))
            return ((URI)node).getFragment();
        else
            return node.toString();
    }

    // =========== User input =============
  
    @Override
    public void viewClosed(String id) {
        loop = false;
    }

    @Override
    public void buttonPushed(String id) {
        /*if(SBHView.class.isAssignableFrom(view.getClass())){
            SBHView v = (SBHView)view;
            if(v.isContracted(objMap.get(id)))
                    v.expand(objMap.get(id));
                else
                    v.contract(objMap.get(id));
        }*/
    }

    @Override
    public void buttonReleased(String id) { }
    
    public void run() {
        int delay = 50;
        new Timer(delay, (ActionEvent evt) -> {
            repaint(); 
            fromViewer.pump();
        }).start();
    }
    
    
    // ========= Instantiation of GraphStream objects ===============

    public String addEntity(N instance) {

        String id = instance.toString() + instance.hashCode();

        if(gsGraph.getNode(id) != null) {
            return null;
        } 
        
        Node n = gsGraph.addNode(id);
        
        if(n == null) {
            logger.error("Could not add node: {}", getPrettyName(instance));
            return null;
        }

        objMap.put(id, instance);
        
        logger.debug("Added node: {}", getPrettyName(instance));
        
        
        addNodeStyle(instance, n);
        
        return id;
    }
    
    public void addEdgeStyle(E instance, Edge edge) {

        edge.addAttribute("ui.style", "fill-color: rgb(100, 100, 100); ");
    }
    
    public abstract void addNodeStyle(N instance, Node node);
    
    public void removeEntity(N instance) {
        
        String id = instance.toString() + instance.hashCode();
       
        if(gsGraph.getNode(id) == null)  {
            logger.error("Could not find node with id: {}", getPrettyName(instance));
            return;
        }
            
        if(gsGraph.removeNode(id) == null) {
            logger.error("Could not remove node: {}",  getPrettyName(instance));
            return;
        }
        
        objMap.remove(id);
        logger.debug("Removed node: {}",  getPrettyName(instance));
    }
    
    public abstract String getEdgeLabel(E edge);
    
    public abstract N getEdgeFrom(E edge);
    
    public abstract N getEdgeTo(E edge);

    public void addEdge(E edge) {

        N source = getEdgeFrom(edge);
        N target = getEdgeTo(edge);
        
        String from = source.toString() + source.hashCode();
        String to = target.toString() + target.hashCode();
        
        
        if(rootId != null && 
                (rootId.equals(source.toString()) || rootId.equals(target.toString())))
                    return;
        
        if(gsGraph.getNode(from) == null) {
            logger.debug("Edge source is not in graph, adding: {}", getPrettyName(source) );
            
            addEntity(source);
        }

        if(gsGraph.getNode(to) == null) {
            logger.debug("Edge target is not in graph, adding: {}", getPrettyName(target) );
            addEntity(target);
        }

        if( gsGraph.getEdge(from + "_" + to) == null) {
            try {
                Edge e = gsGraph.addEdge(from + "_" + to, from, to);
                
                if(e!=null) {
                    edgeMap.put(edge, e);
                    addEdgeStyle(edge, e);
                    e.addAttribute("ui.label", getEdgeLabel(edge));
                    logger.debug("Added edge {}: {} -> {}", getEdgeLabel(edge), getPrettyName(source), getPrettyName(target));
                } else {
                    logger.error("Edge is null: {}", getPrettyName(edge));
                }
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        } else {
            logger.error("Edge already in graph: {}", getPrettyName(edge));
        }
    }
    
    public void removeEdge(E edge) {
        Edge e = edgeMap.get(edge);
        if(e!=null) {
            if(gsGraph.getEdge(e.getId()) != null) {
                gsGraph.removeEdge(e);
                edgeMap.remove(edge);
                logger.debug("Removed edge: {}", getPrettyName(edge));
            } else {
                logger.error("Could not find cached edge: {}", getPrettyName(edge));
            }
        } else {
            logger.error("Could not find cached edge: {}", getPrettyName(edge));
        }
    }
    
    public void setView(N startNode, G view) {
 
        if(this.view!=null) {
            this.view.unsubscribe(this);
        }
        
        logger.debug("Setting view " + view);
        this.view = view;
        this.view.subscribe(new SBEventFilter.DefaultFilter(), this);
        setUpPanel();
        renderPanel();
        
        if(SBHGraph.class.isAssignableFrom(this.view.getClass())) {
            rootId = ((SBHGraph)getView()).getRoot().toString();
        }
    }
    
    public void setView(G view) {
        
        if(SBGlobalNodeProvider.class.isAssignableFrom(view.getClass())) {
            N o = ((SBGlobalNodeProvider<N> )view).nodeSet().isEmpty() ?
                null : 
                ((SBGlobalNodeProvider<N> )view).nodeSet().iterator().next();   
            setView((N)o, view);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
