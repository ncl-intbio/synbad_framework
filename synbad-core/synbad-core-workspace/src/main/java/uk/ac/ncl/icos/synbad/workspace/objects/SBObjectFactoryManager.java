/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;
import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 * Maps rdf:type to object factories, which are used to instantiate domain objects. Object factories
 * are generated from the annotations on domain object classes.
 * @author owengilfellon
 */
public class SBObjectFactoryManager  {

    private static SBObjectFactoryManager MANAGER = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(SBObjectFactoryManager.class);

    private final Map<URI, ObjectFactoryRecord<? extends SBValued>> uriToRecord;
    private final Map<Class<? extends SBValued>, ObjectFactoryRecord<? extends SBValued>> clazzToRecord;

    public static SBObjectFactoryManager get() {
        if(MANAGER != null)
            return MANAGER;

        MANAGER = new SBObjectFactoryManager();
        return MANAGER;
    }

    public SBObjectFactoryManager() {
        this.uriToRecord = new HashMap<>();
        this.clazzToRecord = new HashMap<>();
        setupFactories();
    }

    private void setupFactories() {
        
        try (ScanResult result = new ClassGraph()
                .enableClassInfo()
                .enableAnnotationInfo()
                .scan()) {
            
          ClassInfoList classInfos = result.getClassesWithAnnotation(DomainObject.class.getName());
          List<Class<?>> domainObjects = classInfos.loadClasses();
          doX(domainObjects);
        
        }
        
        if(clazzToRecord.isEmpty()) {
            for(SBDomainObjectProvider provider : Lookup.getDefault().lookupAll(SBDomainObjectProvider.class)) {
                doX(provider.getDomainObjects());
            }
        }
    }
    
    private void doX(Collection<Class<?>> domainObjects) {
        
        domainObjects.forEach(c -> {
            try {
                Set<Constructor<?>> constructors = Arrays.asList(c.getConstructors())
                    .stream()
                    .filter(con -> con.getParameterCount() == 3)
                    .filter(con -> URI.class.isAssignableFrom(con.getParameterTypes()[0]) || SBIdentity.class.isAssignableFrom(con.getParameterTypes()[0]))
                    .filter(con -> SBWorkspace.class.isAssignableFrom(con.getParameterTypes()[1]))
                    .filter(con -> SBValueProvider.class.isAssignableFrom(con.getParameterTypes()[2]))
                    .collect(Collectors.toSet());

                if(constructors.isEmpty()) {
                    throw new InstantiationException("Domain object need a constructor specifying Identity, Workspace URI and Value Provider");
                }

                DomainObject domObj = c.getDeclaredAnnotation(DomainObject.class);
                URI rdfType = SBIdentityHelper.getURI(domObj.rdfType()); 
                SBObjectFactory service = new ADomainFactory(c, rdfType);
                ObjectFactoryRecord fr = new ObjectFactoryRecord(service, rdfType, c);
                uriToRecord.put(rdfType, fr);
                clazzToRecord.put(fr.objectClass, fr);  
            } catch (InstantiationException | IllegalArgumentException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            } 
        });
    }


    /**
     * Returns the RDF types registered with the Object Factory
     * @return
     */
    public Set<URI> getTypes() {
        return uriToRecord.keySet();
    }
    
    public Set<Class<? extends SBValued>> getClasses() {
        return clazzToRecord.keySet();
    }

     public Class<? extends SBValued> getClassFromType(URI uri) {
        ObjectFactoryRecord<? extends SBValued> r =  uriToRecord.get(uri);
        if(r == null) {
            return null;
        }
        return r.objectClass;
    }

    /**
     * Returns the rdf:type associated with an object class
     * @param clazz
     * @return
     */
    public URI getTypeFromClass(Class<? extends SBValued> clazz) {
        if (!clazzToRecord.containsKey(clazz)) {
           // LOGGER.warn("Could not find type for class: [{}]", clazz.getSimpleName());
            return null;
        }
        return clazzToRecord.get(clazz).rdfType;
    }

    public SBObjectFactory<? extends SBValued> getFactoryFromType(URI uri) {
        if(!uriToRecord.containsKey(uri))
            return null;
        return uriToRecord.get(uri).factory;
    }

    /**
     * @param clazz
     * @param <T>
     * @return The factory for creating instances of type `clazz`.
     */
    public <T extends SBValued> SBObjectFactory<T> getFactoryFromClass(Class<T> clazz) {
        if(!clazzToRecord.containsKey(clazz))
            return null;
        return (SBObjectFactory<T>)clazzToRecord.get(clazz).factory;
    }


    public Set<SBObjectFactory<? extends SBValued>> getFactories() {
        return getClasses().stream()
                .map(this::getFactoryFromClass)
                .filter(Objects::nonNull).collect(Collectors.toSet());
    }

    
    private static class ObjectFactoryRecord<T extends SBValued> {
        
        SBObjectFactory<T> factory;
        URI rdfType;
        Class<T> objectClass;

        public ObjectFactoryRecord(SBObjectFactory<T> factory, URI rdfType, Class<T> objectClass) {
            this.factory = factory;
            this.rdfType = rdfType;
            this.objectClass = objectClass;
        }
    }
}
