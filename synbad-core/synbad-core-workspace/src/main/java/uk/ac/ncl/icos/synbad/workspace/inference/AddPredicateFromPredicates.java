package uk.ac.ncl.icos.synbad.workspace.inference;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public abstract class AddPredicateFromPredicates implements SBInferenceRule {

    private final String newPredicate;
    private final Collection<String> oldPredicates;
    private String rules = null;

    public AddPredicateFromPredicates(String newPredicate, Collection<String> oldPredicates) {
        this.newPredicate = newPredicate;
        this.oldPredicates = new HashSet<>(oldPredicates);
    }

    public String getRules() {

        if(this.rules != null) {
            return this.rules;
        }

        StringBuilder b  = new StringBuilder();

        for(String s : oldPredicates) {
             b.append("(?s ").append(s).append(" ?v) -> (?s ").append(newPredicate).append(" ?v)\n");
        }

        this.rules = b.toString();

        return this.rules;
    }
}
