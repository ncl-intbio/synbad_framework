/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WorkspaceTraversal;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WsNodeTraverser;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBValidationRule implements SBValidationRule {
    
    private final String failureMessage;
    private final String validationCode;
    private final WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph> traversal;
    private final Predicate<Collection<SBValued>> predicate;
    
    private static final Logger logger = LoggerFactory.getLogger(DefaultSBValidationRule.class);

    /**
     * 
     * @param validationCode
     * @param failureMessage
     * @param returnInvalidObjects Traversal returns any nodes that are invalid
     */
    public DefaultSBValidationRule(
            String validationCode, 
            String failureMessage, 
            WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph> returnInvalidObjects,
            Predicate<Collection<SBValued>> predicate) {
        this.failureMessage = failureMessage;
        this.validationCode = validationCode;
        this.traversal = returnInvalidObjects.copy();
        this.predicate = predicate;
    }

    @Override
    public String getFailureMessage() {
        return failureMessage;
    }

    @Override
    public String getValidationCode() {
        return validationCode;
    }

    @Override
    public boolean validate(SBWorkspaceGraph graph, SBValued object) {
        WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph> t = traversal.copy();
        t.setGraph(graph);
        WsNodeTraverser<SBValued, SBValued, SBWorkspaceGraph> g = graph.getTraversal().v(object);        
        List<SBValued> 
                o = g.doTraversal(t).getResult().streamVertexes().filter(v -> SBValued.class.isAssignableFrom(v.getClass())).map(v -> (SBValued)v).collect(Collectors.toList());
        boolean b = predicate.test(o);
        logger.debug("Validating {}: [{}:{}]", object, validationCode, b ? "Passed" : "FAILED");
        return b;
    }
}
