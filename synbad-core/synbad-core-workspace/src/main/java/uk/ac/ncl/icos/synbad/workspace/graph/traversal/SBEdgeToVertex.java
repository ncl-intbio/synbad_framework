/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.net.URI;
import java.util.Collection;
import java.util.HashSet;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBLocalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

/**
 *
 * @author owengilfellon
 */
public class SBEdgeToVertex<T extends SBEdge, V extends SBValued> extends DefaultSBTraversalPipe<T, V>{

    private final SBDirection direction;
    private final Class<? extends V> clazz;
    private final String type;
    private final SBWorkspace ws;

    public SBEdgeToVertex(SBWorkspace ws, SBTraversal traversal) {
        super(traversal);
        this.direction = null;
        this.clazz = null;
        this.ws = ws;
        this.type = null;
    }
    
    protected SBEdgeToVertex(SBTraversal parentTraversal, SBEdgeToVertex<T, V> traversal) {
        super(parentTraversal, traversal);
        this.direction = traversal.direction;
        this.clazz = traversal.clazz;
        this.ws = traversal.ws;
        this.type = traversal.type;
    }
    
    public SBEdgeToVertex(SBWorkspace ws, SBTraversal traversal, SBDirection direction) {
        super(traversal);
        this.direction = direction;
        this.clazz = null;
        this.ws = ws;
        this.type = null;
    }
    
    public SBEdgeToVertex(SBWorkspace ws, SBTraversal traversal, SBDirection direction, Class<? extends V> clazz) {
        super(traversal);
        this.direction = direction;
        this.clazz = clazz;
        this.ws = ws;
        URI type = SBObjectFactoryManager.get().getTypeFromClass(clazz);
        this.type = type == null ? null : type.toASCIIString();
    }
    
    private Collection<SBTraverser<V>> addIfIs(Collection<SBTraverser<V>> coll, SBTraverser<T> traverser, V obj, Class<? extends V> clazz) {
        if(clazz == null) {
            coll.add(traverser.duplicate().push(obj));
        } else if(clazz.isAssignableFrom(obj.getClass()) )
            coll.add(traverser.duplicate().push(clazz.cast(obj)));
        else if (type != null && obj.is(type)) {
            obj.as(clazz).ifPresent(o -> coll.add(traverser.duplicate().push(o)));
        } else if (type == null && ws.isObject(obj.getIdentity(), clazz, obj.getContexts())) {
            obj.as(clazz).ifPresent(o -> coll.add(traverser.duplicate().push(o)));
        }
        return coll;
    }
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {

         SBLocalEdgeProvider<V, SBEdge> view = ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();
         Collection<SBTraverser<V>> processed = new HashSet<>();
         
        if(null == direction) {
            V source = view.getEdgeSource(traverser.get());
            V target = view.getEdgeTarget(traverser.get());
            addIfIs(processed, traverser, source, clazz);
            addIfIs(processed, traverser, target, clazz);
        } else switch (direction) {
            case IN: 
                V source = view.getEdgeSource(traverser.get());
                addIfIs(processed, traverser, source, clazz);
                break;
            default:
                V target = view.getEdgeTarget(traverser.get());
                addIfIs(processed, traverser, target, clazz);
                break;
        }

        return processed;
           
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBEdgeToVertex<>(parentTraversal, this);
    }
}
