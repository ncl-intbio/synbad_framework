/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf;

import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author owengilfellon
 */
public class SBStatements extends ArrayList<SBStatement> {

    public SBStatements() {
        super();
    }
    
    public SBStatements(Collection<? extends SBStatement> c) {
        super(c);
    }

    @Override
    public String toString() {
       StringBuilder sb = new StringBuilder();
       sb.append("[");
       for(SBStatement s : this) {
            sb.append("\n").append("\t").append(s);
       }
        sb.append("]");
        
        return sb.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SBStatements))
            return false;
        
        
        SBStatements s = (SBStatements) obj;
        
        if(size() != s.size())
            return false;
        
        for(int i = 0; i < s.size(); i++) {
            if(! (s.get(i).getSubject().equals(get(i).getSubject()) &&
                s.get(i).getPredicate().equals(get(i).getPredicate()) &&
                s.get(i).getObject().equals(get(i).getObject())))
                    return false;
        }
        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        for(SBStatement s : this) {
            hash = 37 * hash + Objects.hashCode(s.getSubject());
            hash = 37 * hash + Objects.hashCode(s.getPredicate());
            hash = 37 * hash + Objects.hashCode(s.getObject());
        }
        
        return hash;
    }
    
    
    

    
}
