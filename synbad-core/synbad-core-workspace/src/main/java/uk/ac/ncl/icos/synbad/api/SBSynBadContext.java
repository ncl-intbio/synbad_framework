/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api;

import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerDisk;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

/**
 *
 * @author owen
 */
public class SBSynBadContext {
    
    /*
        TO-DO: Implement in Scala and Akka
    
        * Spin up isolated Akka contexts for SynBad
            * Add jars on the classpath. Workspace searches for all annotations.
                e.g. domain objects, actions (key, so that events can trigger actions
                using keys, also key lookup, to get display name), processes.
                Processes are services that can update the database in response 
                to events - for example, compiling the SBML file of an updated 
                design or .
            * SynBad is a database updater
        * Switch to simulators as service - returns json results, enables isolated
            Copasi instances
        * Add EA Engines as service - allows isolating and separating out into
            clusters if necessary
        * Web interface for assigning SVPs to components
            * Can expand later to include simulation and EA direct in UI
        * Implement each in their own docker containers, so can run on a single
            computer if needed
        * Describe the workflow of the tool, and how that fits in with the context
            of other SynBio tools
        * Future work (i.e. out of scope)
            * 
    */
    
    private final SBWorkspaceManager manager;
    private final SBDataDefManager dataManager;
    private final SBObjectFactoryManager factoryManager;

    public SBSynBadContext(WorkspaceType type) {
        if (type == WorkspaceType.DISK) {
            this.manager = new DefaultSBWorkspaceManagerDisk();
        } else {
            this.manager = new DefaultSBWorkspaceManagerMem();
        }

        this.dataManager = SBDataDefManager.getManager();
        this.factoryManager = new SBObjectFactoryManager();
    }

    public SBObjectFactoryManager getObjectFactoryManager()  { return factoryManager; }

    public SBDataDefManager getDataManager() {
        return dataManager;
    }

    public SBWorkspaceManager getManager() {
        return manager;
    }

    public enum WorkspaceType {
        MEMORY,
        DISK
    }
    
}
