/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.util.List;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;

/**
 *
 * @author owengilfellon
 */
public interface SBActionTraverser<N> extends SBTraverser<N> {
    
   public List<SBAction> getActions();
   public boolean addAction(SBAction action);
    
}
