/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import java.net.URI;
import java.util.List;

import com.google.common.collect.ObjectArrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithContext;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author Owen
 */
public abstract class AEdgeFromEdgeRule implements SBWorkspaceRule {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AEdgeFromEdgeRule.class);
 
    public abstract List<String> getPredicates();
    
    public abstract SynBadEdge getEdgeToAdd(SynBadEdge statement);
    
    @Override
    public void apply(SBWorkspace g) {
        LOGGER.trace("Applying to WS");
    }

    @Override
    public boolean shouldApply(SBWorkspace g, SBEvent e) {

        if(!SynBadEdge.class.isAssignableFrom(e.getData().getClass()))
            return false;  
       
        if(e.getType() != SBGraphEventType.ADDED)
            return false;
        
        SynBadEdge s = (SynBadEdge) e.getData();

        return getPredicates().contains(s.getEdge().toASCIIString()) &&
                g.containsIdentity(s.getFrom(), g.getDefaultContextIds(s.getContexts())) &&
                g.containsIdentity(s.getTo(), g.getDefaultContextIds(s.getContexts()));
//
//        if(b)
//           LOGGER.trace("Applying!\t{}", s);
//
//        return  b;
    }

    @Override
    public SBAction apply(SBWorkspace g, SBEvent e) {

        if(!SynBadEdge.class.isAssignableFrom(e.getData().getClass()))
            return null;
        
        SynBadEdge s = (SynBadEdge) e.getData();
        SynBadEdge s2 = getEdgeToAdd(s);
        return getAction(e, s2, g);
//        SBIdentity id1 = g.getIdentityFactory().getIdentity(s2.getFrom());
//        SBIdentity id2 = g.getIdentityFactory().getIdentity(s2.getTo());
//
//        String p = s.getEdge().toASCIIString();
//        String p2 = s2.getEdge().toASCIIString();
        
//        int index1 = p.contains("#") ?
//                p.lastIndexOf("#") + 1 :
//                p.lastIndexOf("/") + 1;
//        int index2 = p2.contains("#") ?
//                p2.lastIndexOf("#") + 1 :
//                p2.lastIndexOf("/") + 1;
//
//        String predicate1 = String.join("",
//                p.subSequence(index1, p.length()));
//        String predicate2 = String.join("",
//                p2.subSequence(index2, p2.length()));

//        LOGGER.trace("{}: {} - ( {} | {} ) - {}", e.getType(),
//                id1.getDisplayID(),
//                predicate1,
//                predicate2,
//                id2.getDisplayID());
        

    }
                
    private SBAction getAction(SBEvent e, SynBadEdge s, SBWorkspace g) {

        //LOGGER.trace("AddEdge: {}", s.toString());
        
        URI[] systemsContext;
        
        if(SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            systemsContext = g.getSystemContextIds(((SBEvtWithContext)e).getContexts());
        } else {
            systemsContext = g.getSystemContextIds();
        }
        
        return new CreateEdge(s.getFrom(), s.getEdge().toASCIIString(), s.getTo(), g.getIdentity(), systemsContext);
    }
    
    @Override
    public Class<SBIdentified> getRuleClass() {
        return SBIdentified.class;
    }
    
}
