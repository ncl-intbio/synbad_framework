/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */

public class CreateEdge extends AEdgeAction {

    public CreateEdge(URI from, String predicate, URI to, URI workspace, URI[] contexts) {
        super(SBGraphEventType.ADDED, 
                new SynBadEdge(SBIdentityHelper.getURI(predicate), from, to, contexts),
                workspace,
                contexts);
    }
}
