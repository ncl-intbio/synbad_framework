/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import uk.ac.ncl.icos.synbad.workspace.objects.DefaultSBIdentityFactory;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;
import uk.ac.ncl.icos.synbad.workspace.objects.DefaultIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfMemService;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.SBRdfCommandManager;
import uk.ac.ncl.icos.synbad.workspace.rewrite.DefaultSBWorkspaceRewriter;

/**
 *
 * @author owengilfellon
 */

public class DefaultSBWorkspace implements SBWorkspace {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBWorkspace.class);

    private final URI workspaceId;
    private final SBDispatcher dispatcher;
    private final SBRdfService rdfService;
    private final SBCommandManager commander;
    private final DefaultSBWorkspaceRewriter rewriter;
    private final SBObjectFactoryManager factoryManager;
    private final DefaultIdentityFactory f;
    private final SBIdentityFactory identityFactory;

    public DefaultSBWorkspace(URI workspaceId) {
        this.workspaceId = workspaceId;
        this.dispatcher = new DefaultSBDispatcher();
        this.factoryManager = SBObjectFactoryManager.get();
        this.rewriter = new DefaultSBWorkspaceRewriter(this);
        this.identityFactory = new DefaultSBIdentityFactory(this);
        this.f = new DefaultIdentityFactory();

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Registered {} domain factories", factoryManager.getClasses().stream().count());

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Retrieved {} validation rules", Lookup.getDefault().lookupAll(SBValidationRule.class).stream().count());

        this.rdfService = new DefaultSBRdfMemService();
              //  new DefaultSBRdfDiskService(workspaceId);
        this.commander = new SBRdfCommandManager(rdfService, dispatcher);

        if(LOGGER.isInfoEnabled())
            LOGGER.info("Created workspace: {}", workspaceId.toASCIIString());
    }

    public DefaultSBWorkspace(URI workspaceId, SBRdfService rdfService) {
        this.workspaceId = workspaceId;
        this.dispatcher = new DefaultSBDispatcher();
        this.factoryManager = SBObjectFactoryManager.get();
        this.rewriter = new DefaultSBWorkspaceRewriter(this);
        this.identityFactory = new DefaultSBIdentityFactory(this);
        this.f = new DefaultIdentityFactory();

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Registered {} domain factories", factoryManager.getClasses().stream().count());

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Retrieved {} validation rules", Lookup.getDefault().lookupAll(SBValidationRule.class).stream().count());

        this.rdfService = rdfService;
        this.commander = new SBRdfCommandManager(rdfService, dispatcher);

        if(LOGGER.isInfoEnabled())
            LOGGER.info("Created workspace: {}", workspaceId.toASCIIString());
    }

    private URI getSystemContextId(URI context) {
        try {
            SBIdentity contextId = SBIdentity.getIdentity(context);
            SBIdentity systemContextId = (contextId.getVersion() == null || contextId.getVersion().isEmpty()) ?
                    SBIdentity.getIdentity(contextId.getUriPrefix(), contextId.getDisplayID().concat("_sbSysCtx")) :
                    SBIdentity.getIdentity(contextId.getUriPrefix(), contextId.getDisplayID().concat("_sbSysCtx"), contextId.getVersion());
            return systemContextId.getIdentity();
        } catch (SBIdentityException ex) {
            LOGGER.warn("Could not get Sys Context from {}", context.toASCIIString());
            return null;
        }
    }

    private URI getDefaultContextId(URI context) {
        try {
            SBIdentity contextId = SBIdentity.getIdentity(context);
            SBIdentity systemContextId = (contextId.getVersion() == null || contextId.getVersion().isEmpty()) ?
                    SBIdentity.getIdentity(contextId.getUriPrefix(), contextId.getDisplayID().replace("_sbSysCtx", "")) :
                    SBIdentity.getIdentity(contextId.getUriPrefix(), contextId.getDisplayID().replace("_sbSysCtx", ""), contextId.getVersion());
            return systemContextId.getIdentity();
        } catch (SBIdentityException ex) {
            LOGGER.warn("Could not get default Context from {}", context.toASCIIString());
            return null;
        }
    }

    @Override
    public URI[] getDefaultContextIds(URI... systemContexts) {

        URI[] toReturn = Arrays.stream(systemContexts)
                .map(this::getDefaultContextId)
                .collect(Collectors.toList()).toArray(new URI[]{});

        if(toReturn.length == 0) {
            LOGGER.warn("No system contexts found for {}",
                    String.join(", ", Arrays.stream(systemContexts)
                            .map(c -> c.toASCIIString()).collect(Collectors.toSet())
                            .toArray(new String[] {})));
        }
        return toReturn;
    }

    @Override
    public URI[] getSystemContextIds(URI... contexts) {

        URI[] toReturn = Arrays.stream(contexts)
                .map(this::getSystemContextId)
                .collect(Collectors.toList()).toArray(new URI[]{});

        if(toReturn.length == 0) {
            LOGGER.warn("No system contexts found for {}",
                String.join(", ", Arrays.stream(contexts)
                    .map(c -> c.toASCIIString()).collect(Collectors.toSet())
                    .toArray(new String[] {})));
        }
        return toReturn;
    }
    
    @Override
    public void createContext(URI context) {
        if(Arrays.asList(getContextIds()).contains(context))
            return;
        URI systemContext = getSystemContextId(context);
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Creating context and system context: {} | {}", context.toASCIIString(), systemContext.toASCIIString());
        rdfService.createContext(context);
        rdfService.createContext(systemContext);
    }

    @Override
    public void removeContext(URI context) {
        rdfService.removeContext(context);
        rdfService.removeContext(getSystemContextId(context));
    }
    
    @Override
    public URI[] getContextIds() {
        return Stream.of(rdfService.getContexts())
            .filter(ctx -> !ctx.toASCIIString().contains("_sbSysCtx"))
            .collect(Collectors.toSet()).toArray(new URI[] {});
    }

    @Override
    public boolean containsIdentity(URI identity, URI... contexts) {    
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
  
        if(ctxs.length == 0 || !Arrays.asList(rdfService.getContexts()).containsAll(Arrays.asList(ctxs))) {
            return false;
        }

        return rdfService
                .getPredicates(identity.toASCIIString(), ctxs)
                .contains(SynBadTerms.Rdf.hasType);
    }
    
    /*
    @Override
    public SBIdentity getIdentity(URI identity, URI... contexts) {
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found");

        if(f.isEntity(identity.toASCIIString(), rdfService, ctxs)) {
            SBIdentity id = f.getEntity(identity.toASCIIString(), rdfService, ctxs);
            return id;
        }
            
        return null;
    }*/

    @Override
    public Set<SBIdentity> getIdentities(URI... contexts) {
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for getIdentities");
        
        Set<SBIdentity> identities = new HashSet<>();

        identities.addAll(rdfService.getStatements(null, SynBadTerms.Rdf.hasType, null, ctxs).stream()
            .filter(s -> f.isEntity(s.getSubject(), this, ctxs))
            .map(SBStatement::getSubject)
            .map(s -> f.getEntity(s, this, ctxs))
            .collect(Collectors.toSet()));

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Returning {} identities", identities.size());
        
        return identities;
        
    }

    @Override
    public Set<SBIdentity> getIdentities(URI persistentIdentity, URI... contexts) {
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for getIdentities with persistent identity: [{}]", persistentIdentity.toASCIIString());

        Set<SBIdentity> identities =  rdfService.getStatements(null,
            SynBadTerms.SbolIdentified.hasPersistentIdentity, 
            SBValue.parseValue(persistentIdentity), ctxs).stream()
                .filter(s -> s.getObject().isURI())
                .filter(s -> f.isEntity(s.getObject().asURI().toASCIIString(), this, ctxs))
                .map(SBStatement::getSubject)
                .collect(Collectors.toSet()).stream()
                .map(s -> f.getEntity(s, this, ctxs))
                .collect(Collectors.toSet());

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Found {} identities with persistentIdentity: [{}]", persistentIdentity.toASCIIString());
        return identities;
    }

    @Override
    public Collection<Class<? extends SBValued>> getObjectClasses() {
        return factoryManager.getClasses();
    }

    @Override
    public Collection<Class<? extends SBValued>> getObjectClasses(URI identity, URI... contexts) {

        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for getObjectClasses: [{}]", identity.toASCIIString());
       
        Set<Class<? extends SBValued>> classes = factoryManager.getFactories().stream()
            .filter(f -> f.isEntity(identity.toASCIIString(), this, ctxs))
            .filter(f-> SBValued.class.isAssignableFrom(f.getEntityClass()))
            .map(f -> (Class<? extends SBValued>)f.getEntityClass())
            .collect(Collectors.toSet());
        
        return classes;
    }

    @Override
    public <T extends SBValued> boolean isObject(URI identity, Class<T> clazz, URI[] contexts) {
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        String identityString = identity.toASCIIString();
        
        if(ctxs.length == 0) {
            LOGGER.error("No contexts found for retrieveObject [{}:{}]", clazz.getSimpleName(), identityString);
            return false;
        }
        
        return rdfService.getStatements(identityString, SynBadTerms.Rdf.hasType, null, ctxs).stream()
            .filter(s -> s.getObject().isURI()).map(s -> s.getObject().asURI())
            .map(factoryManager::getClassFromType)
            .filter(Objects::nonNull)
            .anyMatch(clazz::isAssignableFrom);
    }
    
    public boolean isEdge(SBStatement statement, URI[] contexts) {
        
       // LOGGER.debug("Checking " + statement.getPredicate() + " is edge");
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for isEdge: [{}]", statement);
        
        if(statement == null)
            throw new NullPointerException("Statement cannot be null");

        // if object is URI
        
        return statement.getObject().isURI() &&
            isObject(statement.getObject().asURI(), SBValued.class, ctxs) &&
            isObject(SBIdentityHelper.getURI(statement.getSubject()), SBValued.class, ctxs);

    }
  
    private <T extends SBValued> T retrieveObject(URI identity, Class<T> clazz, URI... contexts) {

        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        String identityString = identity.toASCIIString();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for retrieveObject [{}:{}]", clazz.getSimpleName(), identityString);
        
        Class<? extends SBValued> objClazz = rdfService.getStatements(identityString, SynBadTerms.Rdf.hasType, null, ctxs)
            .stream()
            .filter(s -> s.getObject().isURI()).map(s -> s.getObject().asURI())
            .filter((URI uri) -> {
                Class<?> clzz = factoryManager.getClassFromType(uri);
                return clzz != null && clazz.isAssignableFrom(clzz);})
            .map(uri -> factoryManager.getClassFromType(uri))
            .findFirst().orElse(null);

        SBObjectFactory factory = factoryManager.getFactoryFromClass(objClazz);

        if(factory != null ) {
            T object = clazz.cast(factory.getEntity(identityString, this, ctxs));
            return object;
        }    

        return null;
    }

    private SynBadEdge retrieveEdge(SBStatement statement, URI[] contexts) {

        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for retrieving statement: [{}]", statement);
        
        if(statement == null)
            throw new IllegalArgumentException("Statement cannot be null");
        
        if(isEdge(statement, ctxs)) {
            
            SynBadEdge e =  new SynBadEdge(
                    SBIdentityHelper.getURI(statement.getPredicate()), 
                    SBIdentityHelper.getURI(statement.getSubject()),
                    statement.getObject().asURI(),
                    contexts);

            return e;
        }
               
        return null;
    }

    @Override
    public <T extends SBValued> boolean containsObject(URI identity, Class<T> clazz, URI... contexts) {
        return isObject(identity, clazz, contexts);
    }

    @Override
    public <T extends SBValued> T getObject(URI identity, Class<T> clazz, URI... contexts) {
 
        if(clazz == null)
            throw new NullPointerException("Clazz cannot be null");
        
        if(identity == null)
            throw new NullPointerException("Identity cannot be null");
        
        T v= retrieveObject(identity, clazz, contexts);
        return v;
    }

    @Override
    public <T extends SBValued> Collection<T> getObjects(Class<T> clazz, URI... contexts) {
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0) {
            LOGGER.error("No contexts found for getObject: [{}]", clazz.getSimpleName());
            return Collections.EMPTY_LIST;
        }
        // TO DO: Update to be able to supply super classes.
        
        if(clazz == null)
            throw new NullPointerException("Clazz cannot be null");
        
        Map<String, T> matches = new HashMap<>();

        // if can get a type, use to restrict statement retrieval
       
        URI type = factoryManager.getTypeFromClass(clazz);
        
        if(type != null) {
             rdfService.getStatements(null, SynBadTerms.Rdf.hasType, SBValue.parseValue(type), ctxs).stream()
                .map(SBStatement::getSubject)
                .filter(s -> !matches.containsKey(s))
                .map(URI::create)
                .map(s -> retrieveObject(s, clazz))
                .filter(o -> o != null)
                .forEach(o -> matches.put(o.getIdentity().toASCIIString(), o));
        } else {
            rdfService.getStatements(null, SynBadTerms.Rdf.hasType, null, ctxs).stream()
                .map(SBStatement::getSubject)
                .filter(s -> !matches.containsKey(s))
                .map(URI::create)
                .filter(s -> isObject(s, clazz, ctxs))
                .map(s -> retrieveObject(s, clazz))
                .filter(o -> o != null)
                .forEach(o -> matches.put(o.getIdentity().toASCIIString(), o));
        }
        
        return matches.values();
    }

    @Override
    public List<SynBadEdge> outgoingEdges(SBValued subject, URI[] contexts) {
       
        if(getContextIds().length == 0)
            LOGGER.error("Contexts empty in outgoingEdges: [{}]", subject);
        
        if(subject == null)
            throw new NullPointerException("Subject cannot be null");
       
        List<SynBadEdge> e = rdfService.getStatements(
            subject.getIdentity().toASCIIString(),
            null, 
            null, getContextIds()).stream()
                .filter(s -> isEdge(s, contexts))
                .map(s -> retrieveEdge(s, contexts))
               // .filter(edge -> getEdgeSource(edge, contexts).getIdentity().equals(subject.getIdentity()))
                .collect(Collectors.toList());
   
        return e;
    }

    @Override
    public List<SynBadEdge> incomingEdges(SBValued subject, URI[] contexts) {
        
        if(getContextIds().length == 0)
            LOGGER.error("Contexts empty in outgoingEdges: [{}]", subject);
  
        if(subject == null)
            throw new NullPointerException("Subject cannot be null");
        
        // after finding a URI, check if already exists before testing factories...
        
        List<SynBadEdge> edges =  rdfService.getStatements(null,
            null, 
            SBValue.parseValue(subject.getIdentity()), contexts).stream()
                .filter(s -> isEdge(s, contexts))
                .map(s -> retrieveEdge(s, contexts))
               // .filter(e -> getEdgeTarget(e, contexts).getIdentity().equals(subject.getIdentity()))
                .collect(Collectors.toList());

        return edges;
    }

    @Override
    public <T extends SBValued, V extends SBValued> Collection<SynBadEdge> outgoingEdges(T subject, String predicate, Class<V> objectClass, URI[] contexts) {

       URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
       if(ctxs.length == 0)
            LOGGER.error("No contexts found for outgoingEdges: [{}]", subject);
        
        // after finding a URI, check if already exists before testing factories...
        
        if(subject == null)
            throw new NullPointerException("Subject cannot be null");
        
        if(objectClass == null)
            throw new NullPointerException("ObjectClass cannot be null");
        
        Set<SynBadEdge> edges =  rdfService.getStatements(
            subject.getIdentity().toASCIIString(),
            predicate, 
            null, ctxs).stream()
                .filter(s -> s.getObject().isURI())
                .filter(s -> isEdge(s, ctxs))
                .filter(s -> isObject(s.getObject().asURI(), objectClass, ctxs))
                .map(s -> retrieveEdge(s, ctxs))
                .collect(Collectors.toSet());
        
        return edges;
    }
    
    @Override
    public <V extends SBValued> List<SynBadEdge> incomingEdges(SBValued subject, String predicate, Class<V> objectClass, URI[] contexts) {

        if(subject == null)
            throw new NullPointerException("Subject cannot be null");
        
        if(objectClass == null)
            throw new NullPointerException("ObjectClass cannot be null");
        
        URI[] ctxs = contexts.length > 0 ? contexts : getContextIds();
        
        if(ctxs.length == 0)
            LOGGER.error("No contexts found for incomingEdges: [{}]", subject);
        
        // after finding a URI, check if already exists before testing factories...
        
        List<SynBadEdge> edges =  rdfService.getStatements(null,
            predicate,
            SBValue.parseValue(subject.getIdentity()), ctxs).stream()
                .filter(s -> isEdge(s, ctxs))
                .filter(s -> isObject(SBIdentityHelper.getURI(s.getSubject()), objectClass, ctxs))
                .map(s -> retrieveEdge(s, ctxs))
                .collect(Collectors.toList());
        
        //logger.debug("Returning {} incoming edges: [{}]", edges.size(), subject);
        
        return edges;
    }

    @Override
    public SBValued getEdgeSource(SynBadEdge edge, URI[] contexts) {
        SBValued source =  getObject(edge.getFrom(), SBValued.class, contexts);
        //logger.debug("Returning edge source [ {} ] for [ {} ]", edge,  source);
        return source;
    }

    @Override
    public SBValued getEdgeTarget(SynBadEdge edge, URI[] contexts) {
        SBValued target =  getObject(edge.getTo(), SBValued.class, contexts);
        //logger.debug("Getting edge target [ {} ] for [ {} ]", edge,  target);
        return  target;
    
    }

    @Override
    public void shutdown() {
        rdfService.shutdown();
        dispatcher.getListeners().stream().forEach(s -> dispatcher.unsubscribe(s));
    }

    @Override
    public URI getIdentity() {
        return workspaceId;
    }

    @Override
    public void perform(SBAction command) {
        commander.perform(command);
    }

    @Override
    public void undo() {
        if(commander.hasUndo())
            commander.undo();
    }

    @Override
    public void redo() {
        if(commander.hasRedo())
            commander.redo();
    }

    @Override
    public SBDispatcher getDispatcher() {
        return dispatcher;
    }

    @Override
    public SBRdfService getRdfService() {
        return rdfService;
    }

//    @Override
//    public SBWorkspaceValidator getValidator() {
//        return validator; 
//    }

//    @Override
//    public DefaultSBWorkspaceRewriter getRewriter() {
//        return rewriter;
//    }

    @Override
    public SBIdentityFactory getIdentityFactory() {
        return identityFactory;
    }

}
