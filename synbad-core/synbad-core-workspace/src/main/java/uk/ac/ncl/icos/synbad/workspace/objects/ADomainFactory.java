/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBEntityConstructor;
import uk.ac.ncl.icos.synbad.api.rdf.SBEntityIdentifier;
import uk.ac.ncl.icos.synbad.api.rdf.SBEntityConstructor;

/**
 *
 * @author owen
 */
public class ADomainFactory<T>  implements SBObjectFactory<T> {

    private final URI type;
    private final Class<T> clazz;
    private final SBEntityIdentifier identifier;
    private final SBEntityConstructor<T> retriever;

    public ADomainFactory(Class<T> clazz, URI type) {
        this(clazz, 
            type, 
            new SBEntityIdentifier.IdentifyByType(type, clazz),
            new DefaultSBEntityConstructor(clazz));
    }
    
    private ADomainFactory(Class<T> clazz, URI type, SBEntityIdentifier identifier,  DefaultSBEntityConstructor<T> retriever) {
        this.identifier = identifier;
        this.retriever = retriever;
        this.clazz = clazz;
        this.type = type;
        
    }

    public ADomainFactory(Class<T> clazz, URI type, SBEntityIdentifier identifier, SBEntityConstructor<T> retriever) {
        this.identifier = identifier;
        this.retriever = retriever;
        this.clazz = clazz;
        this.type = type;
    }
    

    @Override
    public boolean isEntity(String uri, SBWorkspace service, URI[] contexts) {
        return identifier.test(uri, service, contexts);
    }

    @Override
    public T getEntity(String uri, SBWorkspace c, URI[] contexts) {
        return retriever.apply(uri, c, contexts);
    }

    @Override
    public Class<T> getEntityClass() {
        return clazz;
    }

    @Override
    public URI getType() {
        return type;
    }

}