/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBDomainBuilder extends ASBActionBuilder implements SBDomainBuilder {

    public DefaultSBDomainBuilder(DefaultSBDomainBuilder builder) {
        super(builder);
    }

    public DefaultSBDomainBuilder(SBWorkspace workspace, URI[] contexts) {
       super(workspace, contexts);
    }

    @Override
    public DefaultSBDomainBuilder addAction(SBAction action) {
        if(!isBuilt())
            actions.add(action);
        return this;
    }
    
    @Override
    public DefaultSBDomainBuilder createObject(URI identity, URI rdfType, URI parentIdentity, URI parentRdfType) {
        if(!isBuilt()) {
            actions.add(new CreateObject(identity, rdfType.toASCIIString(), parentIdentity, parentRdfType, getWorkspace(), contexts));
            SBIdentity i = identityFactory.getIdentity(identity);
            actions.add(new CreateValue(identity, SynBadTerms.SbolIdentified.hasDisplayId, SBValue.parseValue(i.getDisplayID()), rdfType, getWorkspace(), contexts));
            
            if(i.getVersion() != null)
                actions.add(new CreateValue(identity, SynBadTerms.SbolIdentified.hasVersion, SBValue.parseValue(i.getVersion()), rdfType, getWorkspace(), contexts));
            /*
            if(parentIdentity != null && systemContext != null)
                actions.add(new CreateEdgeAction(identity, SynBadTerms.SynBadSystem.owns, parentIdentity, getWorkspace(), new URI[] {systemContext}));
            */
            URI persistentId = SBIdentityHelper.getURI(SBIdentityHelper.getPersistentIdFromId(identity));
            if(persistentId != null)
                actions.add(new CreateValue(identity, SynBadTerms.SbolIdentified.hasPersistentIdentity, SBValue.parseValue(persistentId), rdfType, getWorkspace(), contexts));
        }
            
        return this;
    }
    
    @Override
    public DefaultSBDomainBuilder duplicateObject(URI identity, URI[] fromContexts, boolean incrementIds) {
        if(!isBuilt())
            actions.add(new DuplicateObject(identity, getWorkspace(), incrementIds, fromContexts, getContexts()));
        return this;
    }
    
    @Override
    public DefaultSBDomainBuilder removeObjectAndDependants(URI identity, URI ownerIdentity) {
        if(!isBuilt())
            actions.add(new RemoveObjectAndDependants(identity, ownerIdentity, getWorkspace(), contexts));
        return this;
    }
    
    
    @Override
    public DefaultSBDomainBuilder removeObject(URI identity,  URI type,URI ownerIdentity, URI ownerType) {
        if(!isBuilt())
            actions.add(new RemoveObject(identity, type, ownerIdentity, ownerType, getWorkspace(), contexts));
        return this; 
    }
     
    @Override
    public DefaultSBDomainBuilder createEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            actions.add(new CreateEdge(source, predicate.toASCIIString(), target, getWorkspace().getIdentity(), contexts));
        return this;
    }
    
    @Override
    public DefaultSBDomainBuilder removeEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            actions.add(new RemoveEdge(source, predicate.toASCIIString(), target, getWorkspace().getIdentity(), contexts));
        return this;
    }
    
    @Override
    public DefaultSBDomainBuilder createValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            actions.add(new CreateValue(source, predicate.toASCIIString(), value, sourceType, getWorkspace(), contexts));
        return this;
    }
    
    @Override
    public DefaultSBDomainBuilder removeValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            actions.add(new RemoveValue(predicate, predicate.toASCIIString(), value, sourceType, getWorkspace(), contexts));
        return this;
    }
}
