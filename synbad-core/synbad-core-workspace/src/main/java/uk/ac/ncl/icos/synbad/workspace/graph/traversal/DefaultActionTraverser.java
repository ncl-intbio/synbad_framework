/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraverser;

/**
 *
 * @author owengilfellon
 */
public class DefaultActionTraverser<N> extends DefaultSBTraverser<N> implements SBActionTraverser<N> {

    private final List<SBAction> actions;
    
    public DefaultActionTraverser(N start) {
        super(start);
        this.actions = new ArrayList<>();
    }

    protected DefaultActionTraverser(List<SBAction> actions, Collection<SBTraverserPosition> path, Map<String, Set<SBTraverserPosition>> labelledEntities) {
        super(path, labelledEntities);
        this.actions = actions;
    }
    
    @Override
    public List<SBAction> getActions() {
       return new ArrayList<>(actions);
    }

    @Override
    public boolean addAction(SBAction action) {
        return actions.add(action);
    }

    @Override
    public DefaultActionTraverser<N> duplicate() {
        Stack<SBTraverserPosition> newStack = new Stack<>();
    
        newStack.addAll(positionStack.stream().map(cp -> new SBTraverserPosition(cp.getEntity(), cp.getLabels())).collect(Collectors.toList()));
        Map<String, Set<SBTraverserPosition>> newlabelledEntities = new HashMap<>();
        for(String key : labelledEntities.keySet()) {
            newlabelledEntities.put(key, new HashSet<>());
            for(SBTraverserPosition c : labelledEntities.get(key)) {
                newlabelledEntities.get(key).add(c);
            }
        }

        return new DefaultActionTraverser<>(new ArrayList<>(actions), newStack, newlabelledEntities);

    }

}
