/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.events;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceEventDebugger extends ASBWorkspaceSubscriber {

    @Override
    public void onNodeEvent(SBGraphEvent<URI> evt) {
        debug(evt);
    }

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> evt) {
        debug(evt);
    }

    @Override
    public void onValueEvent(SBGraphEvent<SBValue> evt) {
        debug(evt);
    }
    
    public void debug(SBGraphEvent evt) {
        System.out.println(evt);
    }
    
}
