/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import java.io.Serializable;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;

/**
 *
 * @author owengilfellon
 */
public interface SBValueProvider extends Serializable {
    
    Set<String> getPredicates();
    List<SBValue> getValues(String predicate);
    URI[] getContexts();
    void close();
    
    void addValue(String predicate, SBValue value);
    void setValues(String predicate, Collection<SBValue> value);
    void removeValue(String predicate, SBValue value);
    void clearValues(String predicate);
   
}
