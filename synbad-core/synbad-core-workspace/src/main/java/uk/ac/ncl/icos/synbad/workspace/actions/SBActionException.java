/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

/**
 *
 * @author owengilfellon
 */
public class SBActionException extends RuntimeException {

    public SBActionException(String msg) {
        super(msg);
    }

    public SBActionException(Throwable throwable) {
        super(throwable);
    }

    public SBActionException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
 
}
