/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import java.io.OutputStream;
import java.net.URI;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author Owen
 */
public interface SBWorkspaceExporter {
    
    public void export(OutputStream os, SBWorkspace workspace, URI[] contexts);
    
}
