/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

/**
 *
 * @author owengilfellon
 */
public abstract class AObjectAction extends ADomainAction<URI> {

    private static final Logger LOGGER  = LoggerFactory.getLogger(AObjectAction.class);
    protected final URI rdfType;
    protected final URI parent;
    protected final URI parentType;
    protected final SBWorkspace ws;
    protected final SBIdentityFactory f;
    /**
     * 
     * @param type
     * @param object Object ID
     * @param parent Parent ID, or null if object is Top Level, 
     * @param rdfType
     * @param parentUriType Parent Type URI, or null if object is Top Level
     * @param workspace
     * @param contexts 
     */
    public AObjectAction(SBGraphEventType type, URI object, URI rdfType, URI parent, URI parentUriType, SBWorkspace workspace, URI[] contexts) {
        super(type, object, workspace.getIdentity(), contexts);
        this.rdfType = rdfType;
        this.parent = parent;
        this.parentType  = parentUriType;
        this.ws = workspace;
        this.f = ws.getIdentityFactory();
    }

    @Override
    public Collection<SBEvent> getEvents(boolean undo) {
        
        SBGraphEventType t = type;
        if(undo) {
            switch(type) {
                case ADDED :
                    t = SBGraphEventType.REMOVED;
                    break;
                case REMOVED :
                    t = SBGraphEventType.ADDED;    
            }
        }

        SBObjectFactoryManager m = SBObjectFactoryManager.get();

        Class<? extends SBValued> clazz = m.getClassFromType(rdfType);
        Class<? extends SBValued> parentclazz = parentType == null ? null : m.getClassFromType(parentType);

        SBGraphEvent evt = parent != null && parentclazz != null ? 
                new DefaultSBGraphEvent(t, SBGraphEntityType.NODE, object, clazz, workspace, SBWorkspace.class, parent, parentclazz, getContexts()) :
                new DefaultSBGraphEvent(t, SBGraphEntityType.NODE, object, clazz, workspace, SBWorkspace.class, null, null, getContexts());

        if(LOGGER.isDebugEnabled()) {
            SBIdentity objectId = f.getIdentity(object);
            SBIdentity identity = f.getIdentity(rdfType);
            LOGGER.debug("{}: {} [{}] - {}", type, objectId.getIdentity().toASCIIString(), identity.getDisplayID(), contexts[0].toASCIIString());
        }

        return Arrays.asList(evt);
    }

    @Override
    public String toString() {
        return type + " Object: " + object.toASCIIString();
    }
}

