/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import com.google.common.collect.ObjectArrays;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.domain.*;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.SynBadAction;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
@SynBadAction(type = SBGraphEventType.REMOVED, clazz = SBIdentified.class)
public class RemoveObjectAndDependants<T extends ASBIdentified> extends ADeferredAction {
 
    private static final Logger LOGGER  = LoggerFactory.getLogger(AObjectAction.class);
    protected final URI object;
    private SBAction action;
    private final URI parentId;
    private URI rdfType;
    
    /**
     * 
     * @param object
     * @param parentId The object's owner, or the workspace ID if object is top level.
     * @param workspace
     * @param contexts 
     */
    public RemoveObjectAndDependants(URI object, URI parentId, SBWorkspace workspace, URI[] contexts) {
        super(SBGraphEventType.REMOVED, workspace, contexts);
        this.object = object;
        this.parentId = parentId;
    }

   /* @Override
    public Collection<SBEvent> perform(SBCommandManager subscriber) {
        if(action == null)
            action = getAction();

        if(action == null)
            return Collections.EMPTY_SET;
        
        return action.perform(subscriber);
    }*/
    

    @Override
    public Collection<SBEvent> getEvents(boolean undo) {
        if(action == null)
            return Collections.EMPTY_SET;
        return action.getEvents(undo);
    }
    
    @Override
    protected SBAction getAction(SBWorkspace workspace) {
        
        if(action != null)
            return action;

        List<SBAction> actions = getAction(workspace, object, parentId, new LinkedList<>(), new HashSet<>());
        
        if(!actions.isEmpty())
            action = new SBActionMacro(actions, "RemoveAll [" + object.toASCIIString() + "]" );
        
        return action;
    }
    
    private List<SBAction> getAction(SBWorkspace ws, URI objectId, URI parentId, List<SBAction> actions, Set<URI> removedIds) {
  
        if(removedIds.contains(objectId))
            return actions;
        
        if(!ws.containsObject(objectId, SBValued.class, contexts)) {
            LOGGER.warn("Already removed: {}", objectId.toASCIIString());
            return actions;
        }
        
        URI[] systemsContexts = ws.getSystemContextIds(contexts);
        URI[] allContexts =  ObjectArrays.concat(systemsContexts, contexts, URI.class);

        SBValued objectToRemove = ws.getObject(objectId, SBValued.class, allContexts);

        URI objType = objectToRemove.getType();
        parentId = parentId != null ? parentId : getParentURI(objectToRemove, allContexts);
        SBValued parent = parentId == null ? null : ws.getObject(parentId, SBValued .class, allContexts);

        Set<URI> owned = ws.outgoingEdges(objectToRemove, SynBadTerms.SynBadSystem.owns, SBValued.class, allContexts)
                .stream().map(SynBadEdge::getTo)
                .collect(Collectors.toSet());

        Set<URI> dependants = ws.incomingEdges(objectToRemove, SynBadTerms.SynBadSystem.dependantOn, SBValued.class, allContexts)
                .stream().map(SynBadEdge::getFrom)
                .collect(Collectors.toSet());

        Set<SBPredicateValue> values = new HashSet<>();

        for(String predicate : objectToRemove.getPredicates()) {
            for(SBValue value : objectToRemove.getValues(predicate)) {
                values.add(new SBPredicateValue(SBIdentityHelper.getURI(predicate), value));
            }
        }

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Removing {}", objectId.toASCIIString());
        
        actions.add(new RemoveObject(objectId, objType, parentId, parent == null ? null : parent.getType(), ws, contexts));
        values.forEach(v -> actions.add(new RemoveValue(object, v.getPredicate().toASCIIString(), v.getValue(), objType, ws, contexts)));
  
        Set<URI> allUris = new HashSet<>();
        allUris.addAll(owned);
        allUris.addAll(dependants);
        removedIds.add(objectId);
        
        for(URI uri : allUris) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Going from {} -> {} ", objectId.toASCIIString(), uri.toASCIIString());
            getAction(ws, uri, parentId, actions, removedIds);
        }
        
        return actions;
    }

    private URI getParentURI(SBValued objectToRemove, URI[] allContexts) {
        return objectToRemove.getWorkspace().incomingEdges(objectToRemove, SynBadTerms.SynBadSystem.owns, SBValued.class, allContexts)
                .stream().map(e -> e.getFrom()).findFirst().orElse(null);
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
