/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf;


import java.net.URI;
import org.apache.jena.query.DatasetFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**z
 *
 * @author owengilfellon
 */
public class DefaultSBRdfMemService extends ASBRdfService{

    public DefaultSBRdfMemService() {
        super(DatasetFactory.create());
    }

//    public DefaultSBRdfMemService(URI workspaceId) {
//        super(DatasetFactory.create());
//    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
