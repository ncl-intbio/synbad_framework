/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import java.util.Collection;

/**
 * Maps rdf:type to object factories, which are used to instantiate domain objects. Object factories
 * are generated from the annotations on domain object classes.
 * @author owengilfellon
 */
public interface SBDomainObjectProvider  {
    
    Collection<Class<?>> getDomainObjects();

}
