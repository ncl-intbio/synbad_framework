/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.util.function.Function;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;

/**
 * An WorkspaceTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface WsNodeTraverser<T, V, G extends SBWorkspaceGraph> extends SBGraphTraversal<T, V, G> {

    <E> WorkspaceConfigurable<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe);

    WsNodeTraverser<T, V, G> as(String label);

    WsNodeTraverser<T, V, G> hasLabel(String... label);

    WsNodeTraverser<T, V, G> hasNotLabel(String... label);

    <V2> WorkspaceConfigurable<T, V2, G> choose(Predicate<V> predicate, SBGraphTraversal<V, V2, G> branch1, SBGraphTraversal<V, V2, G> branch2);

    WsNodeTraverser<T, V, G> copy();

    WsNodeTraverser<T, V, G> deduplicate();

    <S extends SBGraph> WsNodeTraverser<T, V, G> doFunction(Function<V, S> function);

    WsNodeTraverser<T, V, G> doTraversal(WorkspaceTraversal<V, V, G> traversal);

    WsNodeTraverser <T, V, G> filter(Predicate<V> predicate);

    WsNodeTraverser<T, V, G> has(String key, Object value, Object... additionalValues);

    WsNodeTraverser<T, V, G> hasNot(String key, Object value, Object... additionalValues);

    <X extends WorkspaceTraversal> WsNodeTraverser<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits, boolean inclusive);

    <X extends WorkspaceTraversal> WsNodeTraverser<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits);

    <X extends WorkspaceTraversal> WsNodeTraverser<T, V, G> loop(int iterations, X traversal, boolean emit);

    <V2> WsNodeTraverser<T, V2, G> select(String... labels);

    <V2> WsNodeTraverser<T, V2, G> select(Class<V2> clazz, String... label);
    
    WsEdgeTraverser<T, ? extends SBEdge, G> e();

    WsEdgeTraverser<T, ? extends SBEdge, G> e(String... predicates);

    WsEdgeTraverser<T, ? extends SBEdge, G> e(SBDirection direction);

    WsEdgeTraverser<T, ? extends SBEdge, G> e(SBDirection direction, String... predicates);
}
