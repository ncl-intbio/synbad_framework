/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 * 
 * @author Owen
 * @param <T> 
 */
public class CreateObject<T extends SBIdentified> extends AObjectAction {

    /**
     * 
     * @param identity
     * @param rdfType
     * @param parentIdentity Parent URI ID, or null if object is Top Level
     * @param parentRdfType Parent URI type, or null if object is Top Level
     * @param workspace
     * @param contexts 
     */
    public CreateObject(URI identity, String rdfType, URI parentIdentity, URI parentRdfType, SBWorkspace workspace, URI[] contexts) {
        super(SBGraphEventType.ADDED, 
                identity,
                SBIdentityHelper.getURI(rdfType),
                parentIdentity,
                parentRdfType,
                workspace,
                contexts);    
    }
    
}
