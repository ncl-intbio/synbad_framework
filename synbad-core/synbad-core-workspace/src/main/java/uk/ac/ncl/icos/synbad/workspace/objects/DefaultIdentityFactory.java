/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.rdf.DefaultIdentityConstructor;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owen
 */
public class DefaultIdentityFactory extends ADomainFactory<SBIdentity> {

    public DefaultIdentityFactory() {
        super(SBIdentity.class,
                UriHelper.sbol.namespacedUri("Identity"),
                (String t, SBWorkspace u, URI[] contexts) -> {
                    return u.getRdfService().getValue(t, SynBadTerms.Rdf.hasType, contexts) != null;
                    //u.getValue( t,SynBadTerms.SbolIdentified.hasDisplayId, contexts) != null &&
                    //u.getValue( t, SynBadTerms.SbolIdentified.hasVersion, contexts) != null;
                },
                new DefaultIdentityConstructor());
    }

    /*public static CreateStatements createIdentifiedAction(SBIdentity identity, URI workspace, URI[] contexts) {
        return new CreateStatements(
            new SBStatements(Arrays.asList(new SBStatement(identity.getIdentity().toASCIIString(), SynBadTerms.SbolIdentified.hasDisplayId, SBValue.parseValue(identity.getDisplayID())),
                new SBStatement(identity.getIdentity().toASCIIString(), SynBadTerms.SbolIdentified.hasVersion, SBValue.parseValue(identity.getVersion())),
                new SBStatement(identity.getIdentity().toASCIIString(), SynBadTerms.SbolIdentified.hasPersistentIdentity, SBValue.parseValue(identity.getPersistentId())))), workspace, contexts);
    }*/
}
