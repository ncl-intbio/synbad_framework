/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.rdf;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.rdf.SBEntityConstructor;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.DefaultSBValueProvider;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBEntityConstructor<T> implements SBEntityConstructor<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBEntityConstructor.class);
    private final Class<T> clazz;

    public DefaultSBEntityConstructor(Class<T> clazz) {
        this.clazz = clazz;
    }
    
    @Override
    public T apply(String t, SBWorkspace u, URI[] contexts) {

        try {
            
            if(SBIdentified.class.isAssignableFrom(clazz)) {
                SBIdentity identity = u.getIdentityFactory().getIdentity(SBIdentityHelper.getURI(t));

                if(identity == null) {
                    throw new NullPointerException("Could not create identity from " + t);
                }

                SBValueProvider provider = new DefaultSBValueProvider(identity.getIdentity(), u, contexts);
      
                for(SBStatement statement : u.getRdfService().getStatements(t, null, null, contexts) ) {
                    provider.addValue(statement.getPredicate(), statement.getObject());
                }
                
                return clazz.getConstructor(SBIdentity.class, SBWorkspace.class, SBValueProvider.class)
                            .newInstance(identity, u, provider);
            } else {
                URI identity = SBIdentityHelper.getURI(t);

                if(identity == null) {
                    throw new NullPointerException("Could not create identity from " + t);
                }

                SBValueProvider provider = new DefaultSBValueProvider(identity, u, contexts);

                for(SBStatement statement : u.getRdfService().getStatements(t, null, null, contexts) ){
                    provider.addValue(statement.getPredicate(), statement.getObject());
                }

                return clazz.getConstructor(URI.class, SBWorkspace.class, SBValueProvider.class)
                            .newInstance(identity, u, provider);
            }
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
    }
    
    @Override
    public String toString() {
        return "DefaultConstructor[ ".concat(clazz.getSimpleName()).concat(" ]");
    }
}
