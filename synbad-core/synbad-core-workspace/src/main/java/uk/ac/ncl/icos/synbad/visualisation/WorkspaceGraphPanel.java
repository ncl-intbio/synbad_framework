/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.visualisation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.DefaultView;
import org.graphstream.ui.swingViewer.basicRenderer.SwingBasicGraphRenderer;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBCrawler;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawler;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawlerObserver.SBCrawlerObserverAdapter;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceGraphPanel extends JPanel implements ViewerListener, SBSubscriber {

    private static final Logger logger = LoggerFactory.getLogger(WorkspaceGraphPanel.class);
    
    private  ViewerPipe fromViewer;
    private  Viewer gsViewer;
    private  DefaultView gsView;
    private  Graph gsGraph;
    private  SBWorkspaceGraph view;
    private  Map<SynBadEdge, Edge> edgeMap;
    private  Map< String, SBValued> objMap;
    private boolean loop = true;
    /**
     * Creates new form HierarchicalGraphPanel
     */
    
    public void setUpPanel() {
        
        initComponents();
        this.edgeMap = new HashMap<>();
        this.objMap = new HashMap<>();
        
        // Create graph
        
        gsGraph = new SingleGraph("Visualisation");
        gsGraph.setStrict(false);
        
        // Style graph
        
        gsGraph.addAttribute("ui.stylesheet", 
                "graph {            fill-color: black; } " + 
                "node {             text-color: rgb(0, 120, 0); } "+
                "node.sbobject {    fill-color: rgb(0, 150, 0);  } " +
                "node:clicked {     fill-color: rgb(255, 255, 255);} "+
                "node.definition {  size: 20px; }" +
                "node.module {      fill-color: rgb(0, 255, 0); }" +
                "node.component {   fill-color: rgb(0, 0, 255); }" +
                "node.interaction { fill-color: rgb(255, 0, 255);  }" + 
                "edge {             text-color: rgb(0, 80, 0); }");
        
        // Populate graph
 
    }
    
    public void renderPanel() {
        gsViewer = new Viewer(gsGraph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        gsViewer.setCloseFramePolicy(Viewer.CloseFramePolicy.HIDE_ONLY);
        gsViewer.enableAutoLayout();
        
        fromViewer = gsViewer.newViewerPipe();
        fromViewer.addViewerListener(this);
        //fromViewer.addSink(gsGraph);
        
        gsView = new DefaultView(gsViewer, TOOL_TIP_TEXT_KEY, new SwingBasicGraphRenderer());

        // View preferences
        
        gsView.setPreferredSize(new Dimension(400, 400));
        gsView.getCamera().setAutoFitView(true);
        gsView.getCamera().setViewPercent(7.0);

        // Add and fill panel
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0; 
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        add(gsView, c);

        gsView.setVisible(true);
    }
    
    public WorkspaceGraphPanel() {
        System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    }
    
    // ========= Instantiation of GraphStream objects ===============

    public void addEntity(SBValued instance) {

        String id = instance.getIdentity().toASCIIString() + instance.hashCode();

        if(gsGraph.getNode(id) != null) {
            logger.error("Node " + id + " is already in graph");
            return;
        } 
        
        Node n = gsGraph.addNode(id);
        
        if(n == null) {
            logger.error("Could not add node: " + id);
            return;
        }

        objMap.put(id, instance);
        Set<URI> instanceTypes = instance.getValues(SynBadTerms.Rdf.hasType).stream()
                .filter(v -> v.isURI())
                .map(v -> v.asURI())
                .collect(Collectors.toSet());

        if(instanceTypes.contains(UriHelper.sbol.namespacedUri("ModuleDefinition"))) {
            n.addAttribute("ui.class", "moduledefinition, module, definition, sbobject");
        } else if (instanceTypes.contains(UriHelper.sbol.namespacedUri("Module"))) {
            n.addAttribute("ui.class", "module, instance, sbobject");
        } else if (instanceTypes.contains(UriHelper.sbol.namespacedUri("ComponentDefinition"))) {
            n.addAttribute("ui.class", "component, componentdefinition, definition, sbobject");
        } else if (instanceTypes.contains(UriHelper.sbol.namespacedUri("Component"))) {
            n.addAttribute("ui.class", "component, instance, sbobject");
        } else if (instanceTypes.contains(UriHelper.sbol.namespacedUri("FunctionalComponent"))) {
            n.addAttribute("ui.class", "component, instance, functionalcomponent, sbobject");
        } else if(instanceTypes.contains(UriHelper.sbol.namespacedUri("Interaction")) ) {
           n.addAttribute("ui.class", "interaction, sbobject");
        } else {
            n.addAttribute("ui.class", "sbobject ");
        }

        if(SBIdentified.class.isAssignableFrom(instance.getClass())) {
            SBIdentified identified = (SBIdentified) instance;
            n.addAttribute("ui.label", !identified.getName().isEmpty() ? identified.getName() : identified.getDisplayId());
        } else {
            n.addAttribute("ui.label", instance.getIdentity().toASCIIString());
        }
            
    }
    
    public void removeEntity(SBValued instance) {
        
        String id = instance.getIdentity().toASCIIString() + instance.hashCode();
       
        if(gsGraph.getNode(id) == null)  {
            logger.error("Could not find node with id: " + id);
            return;
        }
            
        if(gsGraph.removeNode(id) == null) {
            logger.error("Could not remove node: " + id);
            return;
        }
        
        objMap.remove(id);
    }

    public void addEdge(SynBadEdge edge) {

        
        
        SBValued source = view.getEdgeSource(edge);
        SBValued target = view.getEdgeTarget(edge);

        String from = source.getIdentity().toASCIIString() + source.hashCode();
        String to = target.getIdentity().toASCIIString() + target.hashCode();

        if(gsGraph.getNode(from) == null)
            addEntity(source);
        
        if(gsGraph.getNode(to) == null)
            addEntity(target);
        

        if( gsGraph.getEdge(from + "_" + to) == null) {
            try {
                Edge e = gsGraph.addEdge(from + "_" + to, from, to);
                if(e!=null) {
                    // logger.debug("Added edge " + edge.getEdge().getFragment() + " "+ e.getId() );
                    edgeMap.put(edge, e);
                    e.addAttribute("ui.style", "fill-color: rgb(100, 100, 100); ");
                    e.addAttribute("ui.label", edge.getEdge().getFragment());
                    } else {
                        logger.error("Edge " + edge.getEdge().getFragment() + " : " + from + "_" + to + " is null");
                }
            } catch(Exception ex) {
                logger.error(ex.getLocalizedMessage());
            }
            
            
        } else {
            logger.error("Edge " + from + "_" + to + " already in graph");
        }
    }
    
    public void removeEdge(SynBadEdge edge) {
        Edge e = edgeMap.get(edge);
        if(e!=null) {
            if(gsGraph.getEdge(e.getId()) != null) {
                gsGraph.removeEdge(e);
                edgeMap.remove(edge);
            } else {
                logger.error("Could not find edge  with Id: " + e.getId());
            }
        } else {
            logger.error("Could not find edge " + edge);
        }
    }
    
    public void setView(SBWorkspaceGraph view, SBValued root, URI... contexts) {
        
        if(this.view!=null) {
            this.view.unsubscribe(this);
        }
        
        logger.debug("Setting view " + view);
        this.view = view;
        setUpPanel();
        SBCrawler crawler = new DefaultSBCrawler();
      
        crawler.addObserver(new SBCrawlerObserverAdapter<SBValued, SynBadEdge, SBCursor<SBValued, SynBadEdge>>() {

            @Override
            public void onEdge(SynBadEdge edge) {
                super.onEdge(edge);
               // logger.debug("Crawling " + edge);
                addEdge(edge);
            }

        });
        
        crawler.run(view.createCursor(root));
       

        //view.addChangeListener(this);
        renderPanel();
        
        
    }
    
    // =========== View events =============

    @Override
    public void onEvent(SBEvent e) {
        
        if(!SBGraphEvent.class.isAssignableFrom(e.getClass()))
            return;
        
        SBGraphEvent evt = (SBGraphEvent)e;

        if(evt.getGraphEntityType() == SBGraphEntityType.NODE) 
        {
            if(e.getType() == SBGraphEventType.ADDED || e.getType() == SBGraphEventType.VISIBLE)
                addEntity((SBValued)e.getData());
            else if(e.getType() == SBGraphEventType.REMOVED || e.getType() == SBGraphEventType.HIDDEN)
                removeEntity((SBValued)e.getData());
        } 
        else if (evt.getGraphEntityType() == SBGraphEntityType.EDGE)
        {
            if(e.getType() == SBGraphEventType.ADDED || e.getType() == SBGraphEventType.VISIBLE)
                addEdge((SynBadEdge)e.getData());
            else if(e.getType() == SBGraphEventType.REMOVED || e.getType() == SBGraphEventType.HIDDEN)
                removeEdge((SynBadEdge)e.getData());
        } 
    }

    // =========== User input =============
  
    @Override
    public void viewClosed(String id) {
        loop = false;
    }


    @Override
    public void buttonReleased(String id) {

    }
    
    public void run() {
        int delay = 50;
    
        new Timer(delay, (ActionEvent evt) -> { 
            repaint(); 
            fromViewer.pump();
        }).start();
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    @Override
    public void buttonPushed(String arg0) {
        
    }

}
