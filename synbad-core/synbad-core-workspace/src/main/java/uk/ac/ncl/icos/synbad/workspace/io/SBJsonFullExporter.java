/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.openide.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author Owen
 */
public class SBJsonFullExporter implements SBWorkspaceExporter {
 
    private static final Logger LOGGER = LoggerFactory.getLogger(SBJsonFullExporter.class);
    
    private List<SBWorkspaceExport.ContextJsonPair> asJsonObj(SBWorkspace workspace, URI[] uriContexts) {
        
        LOGGER.debug("Exporting workspace as JSON: {}", workspace.getIdentity().toASCIIString());
        
        List<SBWorkspaceExport.ContextJsonPair> contextJsonPairs =  new ArrayList<>();
        
        for(URI uri : uriContexts) {
            
            LOGGER.debug("...exporting context: {}", uri.toASCIIString());
            
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            SBJsonExporter exporter = new SBJsonExporter();
            exporter.export(outputStream, workspace, new URI[] { uri });
            try {
                String contextJson = outputStream.toString("UTF-8");
                SBWorkspaceExport.ContextJsonPair pair = new SBWorkspaceExport.ContextJsonPair();
                JsonElement e = new JsonParser().parse(contextJson);
                pair.setJson(e);
                pair.setUri(uri.toASCIIString());
                contextJsonPairs.add(pair);
            } catch (UnsupportedEncodingException ex) {
                LOGGER.error("Could not write context to encoding", ex.getLocalizedMessage());
            }
        }
        
        return contextJsonPairs;
        
    }

    /**
     * Exports a workspace as a WorkspaceJson encoded as JSON in the provided
     * OutputStream. 
     * @param os
     * @param workspace
     * @param contexts Is ignored as the entire workspace is exported.
     */
    @Override
    public void export(OutputStream os, SBWorkspace workspace, URI[] contexts) {
       
        // get contexts
        
        LOGGER.debug("Exporting workspace to outputstream: {}", workspace.getIdentity().toASCIIString());
        
        List<URI> uriContexts = new ArrayList<>();
        for(URI context : workspace.getContextIds()) {
            uriContexts.add(context);
        }
        
        for(URI context : workspace.getSystemContextIds()) {
            uriContexts.add(context);
        }
        
        // get list of context ids and export
        
        List<SBWorkspaceExport.ContextJsonPair> contextJsonPairs = asJsonObj(workspace, uriContexts.toArray(new URI[] {}));
        SBWorkspaceExport.WorkspaceJson toExport = new SBWorkspaceExport.WorkspaceJson();
        toExport.setContexts(contextJsonPairs);
        
        // write to json
        
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(toExport);
        try {
            os.write(json.getBytes());
            os.flush();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        LOGGER.debug("...done");
    }
    
    /**
     * Export a set of workspaces as an SBWorkspaceExport encoded as JSON in the
     * provided OutputStream.
     * @param os
     * @param workspaces 
     */
    public void export(OutputStream os, Set<SBWorkspace> workspaces) {
        
        LOGGER.debug("Exporting {} workspaces to outputstream", workspaces.size());
        
        List<SBWorkspaceExport.WorkspaceJson> workspaceExports = new ArrayList<>();
        
        for(SBWorkspace workspace : workspaces) {
            
            // get contexts

            List<URI> uriContexts = new ArrayList<>();
            for(URI context : workspace.getContextIds()) {
                uriContexts.add(context);
            }

            for(URI context : workspace.getSystemContextIds()) {
                uriContexts.add(context);
            }

            // get list of context ids and export

            List<SBWorkspaceExport.ContextJsonPair> contextJsonPairs = asJsonObj(workspace, uriContexts.toArray(new URI[] {}));
            SBWorkspaceExport.WorkspaceJson toExport = new SBWorkspaceExport.WorkspaceJson();
            toExport.setContexts(contextJsonPairs);
            toExport.setUri(workspace.getIdentity().toASCIIString());
            workspaceExports.add(toExport);
        }
        
        SBWorkspaceExport export = new SBWorkspaceExport();
        export.setWorkspaces(workspaceExports);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(export);
        try {
            os.write(json.getBytes());
            os.flush();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        LOGGER.debug("...done");
        
    }
    
}
