/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owen
 */
public abstract class ASBActionBuilder implements SBActionBuilder {
 
    private static final Logger logger = LoggerFactory.getLogger(DefaultSBDomainBuilder.class);
    protected final List<SBAction> actions;

    protected final SBWorkspace ws;
    protected final SBIdentityFactory identityFactory;
    protected final URI[] contexts;
    private String label = "";
    protected SBAction builtAction = null;

    public ASBActionBuilder(ASBActionBuilder builder) {
        this.actions = new LinkedList<>(builder.actions);
        this.contexts = builder.contexts;
        this.ws = builder.ws;
        this.identityFactory = ws.getIdentityFactory();
    }

    public ASBActionBuilder(SBWorkspace workspace, URI[] contexts) {
        this.actions = new ArrayList<>();
        this.contexts = contexts;
        this.ws = workspace;
        this.identityFactory = ws.getIdentityFactory();
    }

    @Override
    public boolean isEmpty() {
        return actions.isEmpty();
    }

    @Override
    public int getSize() {
        return actions.size();
    }

    @Override
    public boolean isBuilt() {
        return builtAction != null;
    }

    @Override
    public URI[] getContexts() {
        return contexts;
    }

    @Override
    public SBWorkspace getWorkspace() {
        return ws;
    }

    @Override
    public SBAction build() {
        return build("");
    }

    @Override
    public SBAction build(String label) {
        if(actions.isEmpty())
            throw new NullPointerException("SBActionBuilder is empty");
        if(isBuilt())
            return builtAction;

        List<URI> existingContexts = Arrays.asList(ws.getContextIds());
        List<URI> existingSystemsContexts = Arrays.asList(ws.getSystemContextIds(ws.getContextIds()));
        
        for(URI context : contexts) {
            if(!existingContexts.contains(context) && !existingSystemsContexts.contains(context)) {
                ws.createContext(context);
            }
        }

        if(actions.size() > 1) {
            builtAction = label.isEmpty() ? new SBActionMacro(actions) : new SBActionMacro(actions, label);
        } else {
            builtAction = actions.get(0);
        }

        return builtAction;
    }


    @Override
    public <T extends SBActionBuilder> T getAs(Class<T> clazz) {
        if(clazz.isAssignableFrom(getClass()))
            return clazz.cast(this);
        
        return null;
    }
    
    
   
}
