/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.util.function.Function;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHasLabel;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal.Configurable;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
/**
 *
 * @author owengilfellon
 */

public interface WorkspaceConfigurable<T, V, G extends SBWorkspaceGraph> extends Configurable<T, V, G>{

//    <V2 extends SBValued> WsEdgeTraverser<T, V2, G> addE(Class<V2> clazz, String label, String predicate);
    
    <E> WorkspaceTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe);

//    <V2 extends SBValued> WsNodeTraverser<T, V2, G> addV(V2 v);

    WorkspaceConfigurable<T, V, G> as(String label);

    <V2> WorkspaceConfigurable<T, V2, G> choose(Predicate<V> predicate, SBGraphTraversal<V, V2, G> branch1, SBGraphTraversal<V, V2, G> branch2);

    WorkspaceConfigurable<T, V, G> copy();

    WorkspaceConfigurable<T, V, G> deduplicate();

    <S extends SBGraph> WorkspaceConfigurable<T, V, G> doFunction(Function<V, S> function);

    WorkspaceConfigurable<T, V, G> doTraversal(WorkspaceTraversal<V, V, G> traversal);

    WsEdgeTraverser<T, ? extends SBEdge, G> e();

    WsEdgeTraverser<T, ? extends SBEdge, G> e(String... predicates);

    WsEdgeTraverser<T, ? extends SBEdge, G> e(SBDirection direction);

    WsEdgeTraverser<T, ? extends SBEdge, G> e(SBDirection direction, String... predicates);

    WorkspaceConfigurable<T, V, G> filter(Predicate<V> predicate);

    WorkspaceConfigurable<T, V, G> has(String key, Object value, Object... additionalValues);

    WorkspaceConfigurable<T, V, G> hasNot(String key, Object value, Object... additionalValues);

    WorkspaceConfigurable<T, V, G> hasLabel(String... label);

    WorkspaceConfigurable<T, V, G> hasNotLabel(String... label);

    <X extends WorkspaceTraversal> WorkspaceConfigurable<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits);

    <X extends WorkspaceTraversal> WorkspaceConfigurable<T, V, G> loop(int iterations, X traversal, boolean emit);

    <V2> WorkspaceConfigurable<T, V2, G> select(String... labels);

    <V2> WorkspaceConfigurable<T, V2, G> select(Class<V2> clazz, String... label);
   
    WsNodeTraverser<T, ? extends SBValued, G> v();

    WsNodeTraverser<T, ? extends SBValued, G> v(SBDirection direction);

    <V2 extends SBValued> WsNodeTraverser<T, V2, G> v(SBDirection direction, Class<V2> clazz);

    }
