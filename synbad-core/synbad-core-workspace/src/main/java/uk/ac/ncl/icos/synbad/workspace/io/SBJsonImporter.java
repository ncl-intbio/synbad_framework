/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author Owen
 */
public class SBJsonImporter extends AJenaImporter {
    
    private final Logger logger = LoggerFactory.getLogger(SBJsonImporter.class);
 
    @Override
    public void importWs(InputStream os, SBWorkspace workspace, URI[] contexts) {
  
        logger.debug("Importing RDF with contexts...");
        Arrays.asList(contexts).stream().map(u -> u.toASCIIString()).forEach(s -> logger.debug("\t...{}", s));

        for(URI uri : contexts) {
            workspace.createContext(uri);
        }
        
        Model importedModel = ModelFactory.createDefaultModel();
        RDFDataMgr.read(importedModel, os, Lang.JSONLD);
        List<SBStatement> statements = getStatementsFromModel(null, null, null, importedModel);
        workspace.getRdfService().addStatements(statements, contexts);
    }
    
}
