/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.events;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author Owen
 */
public class SBFilterEdgeByPredicates implements SBEventFilter {

    private final Set<URI> predicates;

    public SBFilterEdgeByPredicates(String predicate, String... predicates) {
        this.predicates = new HashSet<>();
        this.predicates.add(URI.create(predicate));
        this.predicates.addAll(Arrays.asList(predicates).stream().map(s -> URI.create(s)).collect(Collectors.toSet()));
    }
    
     public SBFilterEdgeByPredicates(URI predicate, URI... predicates) {
        this.predicates = new HashSet<>();
        this.predicates.add(predicate);
        this.predicates.addAll(Arrays.asList(predicates));
    }

    @Override
    public boolean test(SBEvent event) {
        
        if(!SBGraphEvent.class.isAssignableFrom(event.getClass())) 
            return false;
        
        if(!SynBadEdge.class.isAssignableFrom(event.getDataClass()))
            return false;
        
        SynBadEdge e = (SynBadEdge) event.getData();

        return predicates.contains((URI)e.getEdge());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (obj == this)
            return true;

        if (!(obj instanceof SBFilterEdgeByPredicates))
            return false;

        SBFilterEdgeByPredicates r = (SBFilterEdgeByPredicates) obj;
        return predicates.containsAll(r.predicates) && r.predicates.size() == predicates.size();
        
    
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = predicates.stream().map((c) -> c.hashCode()).reduce(hash, Integer::sum);
        return hash;
    }

    

}