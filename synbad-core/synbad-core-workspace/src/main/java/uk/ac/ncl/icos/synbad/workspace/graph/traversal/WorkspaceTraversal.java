/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSelectPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBGraphSideEffect;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDeduplicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToEdges;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBPutPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByNotValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBLoop;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBWhile;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDoTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHasLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSetPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterByPredicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBChoose;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBAddLabel;
import java.util.function.Function;
import java.util.function.Predicate;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;

/**
 *
 * @author owengilfellon
 */

public class WorkspaceTraversal<T, V, G extends SBWorkspaceGraph> extends DefaultSBGraphTraversal<T, V, G> implements WorkspaceConfigurable<T, V, G>,  WsNodeTraverser<T, V, G>, WsEdgeTraverser<T, V, G> {

    public WorkspaceTraversal(G graph) {
        super(graph);
    }

    protected WorkspaceTraversal(WorkspaceTraversal<T, V, G> traversal) {
        super(traversal);
    }

    public WorkspaceTraversal() {
        super();
    }

    @Override
    public G getGraph() {
        return graph;
    }

    @Override
    public WorkspaceTraversal<T, SBEdge, G> e() {
        return addPipe(new SBVertexToEdges(this, null));
    }

    @Override
    public WorkspaceTraversal<T, SBEdge, G> e(String... predicates) {
        return addPipe(new SBVertexToEdges(this, null, predicates));
    }

    @Override
    public WorkspaceTraversal<T, SBEdge, G> e(SBDirection direction) {
        return addPipe(new SBVertexToEdges(this, direction));
    }

    @Override
    public WorkspaceTraversal<T, SBEdge, G> e(SBDirection direction, String... predicates) {
        return addPipe(new SBVertexToEdges(this, direction, predicates));
    }

    @Override
    public WorkspaceTraversal<T, SBValued, G> v(SBDirection direction) {
        return addPipe(new SBEdgeToVertex(graph.getWorkspace(), this, direction));
    }

    @Override
    public <V2 extends SBValued> WorkspaceTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz) {
         return addPipe(new SBEdgeToVertex(graph.getWorkspace(), this, direction, clazz));
    }
    
    @Override
    public WorkspaceTraversal<T, V, G> has(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByValues(this, key, value, additionalValues));
    }

    @Override
    public WorkspaceTraversal<T, V, G> hasLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, false, label));
    }

    @Override
    public WorkspaceTraversal<T, V, G> hasNotLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, true, label));
    }

    @Override
    public WorkspaceTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByNotValues(this, key, value, additionalValues));
    }
    
    @Override
    public WorkspaceTraversal<T, V, G> as(String label) {
        return addPipe(new SBAddLabel(this, label));
    }

    @Override
    public WorkspaceTraversal<T, V, G> filter(Predicate<V> predicate) {
        return (WorkspaceTraversal<T, V, G>)addPipe(new SBFilterByPredicate<>(this, predicate));
    }

    public <V2> WorkspaceTraversal<T, V2, G> as(Class<V2> clazz) {
        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBVertexAs<>(this, clazz));
    }

    @Override
    public <V2> WorkspaceTraversal<T, V2, G> select(String... labels) {
        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, labels));
    }

    @Override
    public <V2> WorkspaceTraversal<T, V2, G> select(Class<V2> clazz, String... label) {
        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, clazz, label));
    }
    

    @Override
    public <V2> WorkspaceTraversal<T, V2, G> choose(Predicate<V> predicate, SBGraphTraversal<V, V2, G> branch1, SBGraphTraversal<V, V2, G> branch2) {
        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBChoose<>(this,predicate, branch1, branch2));
    }

    @Override
    public <X extends WorkspaceTraversal> WorkspaceTraversal<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits, boolean inclusive) {
        return (WorkspaceTraversal<T, V, G>)addPipe(new SBWhile<V>(this,predicate, doWhileTrue, emit, revisits, inclusive));
    }
    
    

    @Override
    public <X extends WorkspaceTraversal> WorkspaceTraversal<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits) {
        return (WorkspaceTraversal<T, V, G>)addPipe(new SBWhile<V>(this,predicate, doWhileTrue, emit, revisits));
    }
    

    @Override
    public <X extends WorkspaceTraversal> WorkspaceTraversal<T, V, G> loop(int iterations, X traversal, boolean emit) {
        return (WorkspaceTraversal<T, V, G>)addPipe(new SBLoop<>(this, traversal, iterations));
    }

    @Override
    public WorkspaceTraversal<T, V, G> deduplicate() {
        return (WorkspaceTraversal<T, V, G>)addPipe(new SBDeduplicate<>(this));
    }
    
    public <V2 extends SBValued> WorkspaceTraversal<T, V2, G> put(String key, SBValue value, SBValue... additionalValues) {
        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBPutPipe<V2>(this, key, value, additionalValues));
    }

    public <V2 extends SBValued> WorkspaceTraversal<T, V2, G> set(String key, SBValue value, SBValue... additionalValues) {
        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBSetPipe<V2>(this, key, value, additionalValues));
    }
//    
//    @Override
//    public <V2 extends SBValued> WorkspaceTraversal<T, V2, G> addV(V2 v) {
//        return (WorkspaceTraversal<T, V2, G>)addPipe(new SBAddVertex<T, V2, G>(this, v));
//    }
    

    
    @Override
    public WorkspaceTraversal<T, V, G> doTraversal(WorkspaceTraversal<V, V, G> traversal) {
        return addPipe(new SBDoTraversal<>(this, traversal));
    }
    
    
    /*
        ============================================================
            Internal methods
        ============================================================
    */
    

    @Override
    public <E> WorkspaceTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe) {
        if(getPipes().size() > 0) {
            SBTraversalPipe previousPipe = getPipes().get(getPipes().size() - 1);
            previousPipe.setNextPipe(nextPipe);
            nextPipe.setPreviousPipe(previousPipe);
        }

        getPipes().add(nextPipe);

        return  ( WorkspaceTraversal<T, E, G> )this;
    }

    @Override
    public WorkspaceConfigurable<T, V, G> asConfigurable() {
        return (WorkspaceConfigurable<T,V, G>) this;
    }

    @Override
    public WorkspaceTraversal<T, V, G> copy() {
        final WorkspaceTraversal<T, V, G> copy = new WorkspaceTraversal<>(graph);
        getPipes().stream().map(p -> p.copy(copy)).forEach(copy::addPipe);
        return copy;
    }

//    @Override
//    public <V2 extends SBValued>WorkspaceTraversal<T, V2, G> addE(Class<V2> clazz, String label, String predicate) {
//        return addPipe(new SBAddEdge<>(this, clazz, label, predicate));
//    }

    @Override
    public <S extends SBGraph> WorkspaceTraversal<T, V, G> doFunction(Function<V, S> function) {
        return addPipe(new SBGraphSideEffect<>(this, function));
    }

    @Override
    public WorkspaceTraversal<T, ? extends SBValued, G> v() {
        return addPipe(new SBEdgeToVertex(graph.getWorkspace(), this));
    }

    

    

}


