/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import java.util.List;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;

/**
 *
 * @author owengilfellon
 */
public interface SBRewriter<N extends SBValued, E extends SBEdge, G> {
    
    public enum RewriteEventType implements SBEventType {

        START,
        STOP;

        @Override
        public String getType() {
            return name();
        }

    }
     
    public List<SBRewriterRule<? extends N, E, G>> getRules();
    
    public <T extends N> boolean addRewriteRule(SBRewriterRule<T, E, G> rule);
    public <T extends N> boolean addRewriteRule(SBRewriterRule<T, E, G> rule, SBRewriterRule<T, E, G>... rules);
    
    public <T extends N> boolean removeRewriteRule(SBRewriterRule<T, E, G> rule);
    public <T extends N> boolean removeRewriteRule(SBRewriterRule<T, E, G> rule, SBRewriterRule<T, E, G>... rules);
    
    public void applyRules();
    public void applyRules(SBEvent e);
    
}
