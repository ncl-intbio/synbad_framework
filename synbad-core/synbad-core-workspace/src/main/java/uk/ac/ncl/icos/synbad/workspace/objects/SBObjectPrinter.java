/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import java.io.IOException;
import java.io.OutputStream;
import org.openide.util.Exceptions;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public class SBObjectPrinter {
    
    public static void print(SBValued valued, OutputStream stream) {
        try {
            stream.write(print(valued).getBytes());
            stream.flush();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    public static String print(SBValued valued) {
        StringBuilder b = new StringBuilder();
            b.append("---------------------------------------------------------\n")
            .append("\t").append(valued.getIdentity().toASCIIString()).append("\n")
            .append("---------------------------------------------------------\n");
        for(String predicate : valued.getPredicates()) {
            for(SBValue value : valued.getValues(predicate)) {
                b.append(predicate).append(":\t\t").append(value.getValue().getClass().getSimpleName()).append(": ").append(value).append("\n");
            }
        }
        return b.toString();
    }
    
}
