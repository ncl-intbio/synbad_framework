/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.net.URI;

/**
 * Domain Factories are used by a Workspace to identify and retrieve 
 * domain objects from an RDFService.
 * @author owengilfellon
 */
public interface SBObjectFactory<T> {
    
    public boolean isEntity(String uri, SBWorkspace service, URI[] contexts);
    
    public T getEntity(String uri, SBWorkspace c, URI[] contexts);
    
    public Class<T> getEntityClass();
    
    public URI getType();


}
