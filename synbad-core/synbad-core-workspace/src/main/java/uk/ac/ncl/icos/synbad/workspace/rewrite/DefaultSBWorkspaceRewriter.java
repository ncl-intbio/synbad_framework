/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import java.util.ArrayList;
import java.util.List;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.workspace.actions.SBActionMacro;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;


public class DefaultSBWorkspaceRewriter implements SBRewriter<SBIdentified, SynBadEdge, SBWorkspace>, SBSubscriber {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBWorkspaceRewriter.class);
    
    public final List<SBRewriterRule<? extends SBIdentified, SynBadEdge, SBWorkspace>> rules;
    public final SBWorkspace workspace;
    private final List<SBEvent> cachedEvents = new ArrayList<>();

    public boolean rewrite = false;
    private int rewriteCount = 0;
    private int macroCount = 0;
    
    public DefaultSBWorkspaceRewriter(SBWorkspace workspace) {
        this.rules = new ArrayList<>();
        this.workspace = workspace;

        Lookup.getDefault().lookupAll(SBWorkspaceRule.class).stream()
        .forEach(rule -> {
            LOGGER.info("Registering rewrite rule: [ {} ]", rule.getClass().getSimpleName());
            this.addRewriteRule(rule);
        });

        workspace.getDispatcher().subscribe(new SBEventFilter.DefaultFilter(), this);
    }

    @Override
    public <T extends SBIdentified> boolean addRewriteRule(SBRewriterRule<T, SynBadEdge, SBWorkspace> rule) {
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added Rule: {}: {}", rule.getRuleClass().getSimpleName(), rule.getClass().getSimpleName() );
        return this.rules.add(rule);
    }

    @Override
    public <T extends SBIdentified> boolean addRewriteRule(SBRewriterRule<T, SynBadEdge, SBWorkspace> rule, SBRewriterRule<T, SynBadEdge, SBWorkspace>... rules) {
        
        boolean b = addRewriteRule(rule);
        
        if(!b)
            return b;
        else if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added Rule: {}: {}", rule.getRuleClass().getSimpleName(), rule.getClass().getSimpleName() );

        for(SBRewriterRule<T, SynBadEdge, SBWorkspace> rewriteRule : rules) {
            if(!addRewriteRule(rewriteRule))
                b = false;
            else if(LOGGER.isTraceEnabled())
                LOGGER.trace("Added Rule: {}: {}", rule.getRuleClass().getSimpleName(), rule.getClass().getSimpleName() );
        }

        if(!b) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Adding failed, removing rule: {}", rule);
            removeRewriteRule(rule);
            for(SBRewriterRule<T, SynBadEdge, SBWorkspace> rewriteRule : rules) {
               // LOGGER.trace("Adding failed, removing rule: {}", rewriteRule);
                removeRewriteRule(rewriteRule);
            }
        }
        
        return b;
    }

    @Override
    public <T extends SBIdentified> boolean removeRewriteRule(SBRewriterRule<T, SynBadEdge, SBWorkspace> rule) {
        return rules.add(rule);
    }

    @Override
    public <T extends SBIdentified> boolean removeRewriteRule(SBRewriterRule<T, SynBadEdge, SBWorkspace> rule, SBRewriterRule<T, SynBadEdge, SBWorkspace>... rules) {
        boolean b = removeRewriteRule(rule);
        
        if(!b)
            return b;
        
        for(SBRewriterRule<T, SynBadEdge, SBWorkspace> rewriteRule : rules) {
            if(!removeRewriteRule(rewriteRule))
                b = false;
        }
        
        return b;
    }

    @Override
    public void applyRules() {
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Applying rules to workspace");
        for(SBRewriterRule<? extends SBIdentified, SynBadEdge, SBWorkspace> rule : rules) {
            rule.apply(workspace);
          //  LOGGER.trace("Applied rules to workspace: {}: {}", rule.getRuleClass().getSimpleName(), rule.getClass().getSimpleName() );
        }
    }
    
    public void applyRules(List<SBEvent> evts) {
        if (evts.size() > 0) {
            for(SBRewriterRule<? extends SBIdentified, SynBadEdge, SBWorkspace> rule : rules) {
                for(SBEvent e : evts) {
                    SBAction action = rule.apply(workspace, e);
                    if(action != null)
                        workspace.perform(action);
                 }
            }
        }
    }
    
    @Override
    public void applyRules(SBEvent e) {
        for(SBRewriterRule<? extends SBIdentified, SynBadEdge, SBWorkspace> rule : rules) {
            if(rule.shouldApply(workspace, e)) {
                SBAction action = rule.apply(workspace, e);
                if(action != null)
                    workspace.perform(action);
                //LOGGER.trace("Applied rule to event: {}:{}", rule.getClass().getSimpleName(), e);
            }
        }
    }

    @Override
    public List<SBRewriterRule<? extends SBIdentified, SynBadEdge, SBWorkspace>> getRules() {
        return rules;
    }

    @Override
    public void onEvent(SBEvent e) {
        if (e.getType() == RewriteEventType.START) {
            rewriteCount++;
        } else if (e.getType() == RewriteEventType.STOP) {
            if(rewriteCount > 0)
                rewriteCount--;
            doCachedRewrites();
        } else if (e.getType() == SBActionMacro.MacroEventType.START) {
            macroCount++;
        } else if (e.getType() == SBActionMacro.MacroEventType.STOP) {
            if(macroCount > 0)
                macroCount--;
            doCachedRewrites();
        } else if(rewriteCount > 0) {
            cachedEvents.add(e);
        } else {
            applyRules(e);
        }
    }
    
    private void doCachedRewrites() {
        if(rewriteCount == 0 && macroCount == 0 && !cachedEvents.isEmpty()) {
            List<SBEvent> toApply = new ArrayList<>(cachedEvents);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Applying rewrite to {} cached events", toApply.size());
            cachedEvents.clear();
            applyRules(toApply);
        }
    }
}
