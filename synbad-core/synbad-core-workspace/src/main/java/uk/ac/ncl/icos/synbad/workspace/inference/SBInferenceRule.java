package uk.ac.ncl.icos.synbad.workspace.inference;

public interface SBInferenceRule {

    String getRules();
}
