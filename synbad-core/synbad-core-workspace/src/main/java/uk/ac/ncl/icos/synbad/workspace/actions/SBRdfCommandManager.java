/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.api.event.*;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.domain.SBPredicateValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.event.pipes.DefaultSBEventPipeline;
import uk.ac.ncl.icos.synbad.event.pipes.DefaultSBEventPipelineSource;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;


public class SBRdfCommandManager implements SBCommandManager {
        
    private final static Logger LOGGER = LoggerFactory.getLogger(SBRdfCommandManager.class);
    private final SBDispatcher eventService;
    private final List<SBAction> actions;
    private int currentIndex;
    private final SBRdfService service;
    private  SBWorkspace ws;
    private SBObjectFactoryManager m;

    public SBRdfCommandManager(SBRdfService service, SBDispatcher dispatcher) {
        this.actions = new LinkedList<>();
        this.currentIndex = -1;
        this.service = service;
        this.eventService = dispatcher;
        this.m = SBObjectFactoryManager.get();
    }

    @Override
    public synchronized void perform(SBAction action) {

        // If there are re-dos, clear them

        if(hasRedo()) {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Clearing redos");
            Iterator<SBAction> i = actions.listIterator(currentIndex + 1);
            while(i.hasNext()) {
                i.next();
                i.remove();
            }
        }


        actions.add(action);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Performed action [{}]: {}", currentIndex, action.getClass().getSimpleName());
        SBAction toPerform = actions.get(++currentIndex);
        Collection<SBEvent> events = toPerform.perform(this);
        eventService.publish(events);
        //events.forEach(eventService::publish);
    }

    @Override
    public synchronized void perform(Collection<SBEvent> events) {
        new DefaultSBEventPipelineSource<>()
            .push(events.toArray(new SBEvent[]{}))
            //.doSideEffect(event -> event.getType() == SBGraphEventType.REMOVED, eventService::publish)
            .doTraversal(new DefaultSBEventPipeline<SBEvent, SBEvent>()
                .doSideEffect(getFilter(SBGraphEntityType.NODE), this::processObject)
                .doSideEffect(getFilter(SBGraphEntityType.EDGE), this::processEdge)
                .doSideEffect(getFilter(SBGraphEntityType.VALUE), this::processValue)
                .doSideEffect(getFilter(SBGraphEntityType.PORT), this::processObject))
            //.doSideEffect(event -> event.getType() == SBGraphEventType.ADDED, eventService::publish)
            .forEachRemaining(e -> {});
    }



    private synchronized  void processObject(SBEvent e) {
        
        if(!SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            LOGGER.error("No Contexts: {}", e);
            return;
        }
        
        SBGraphEvent<URI> event = (SBGraphEvent<URI>) e;
        URI[] contexts = ((SBEvtWithContext)event).getContexts();
        
        if(m.getFactoryFromClass(event.getDataClass()) == null)
            return;
        
        URI type = m.getFactoryFromClass(event.getDataClass()).getType();
        
        SBStatement s = new SBStatement(
                event.getData().toASCIIString(), 
                SynBadTerms.Rdf.hasType,
                SBValue.parseValue(type));

        if(e.getType() == SBGraphEventType.ADDED)
            service.addStatements(Collections.singleton(s), contexts);
        else if(e.getType() == SBGraphEventType.REMOVED)
            service.removeStatements(Collections.singleton(s), contexts);
    }
    
    private synchronized  void processEdge(SBEvent e) {
        
        if(!SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            LOGGER.error("No Contexts: {}", e);
            return;
        }
        
        SBGraphEvent<SynBadEdge> event = (SBGraphEvent<SynBadEdge>) e;
        
        SBStatement s = new SBStatement(
                event.getData().getFrom().toASCIIString(),
                event.getData().getEdge().toASCIIString(), 
                SBValue.parseValue(event.getData().getTo().toASCIIString()));
        URI[] contexts = ((SBEvtWithContext)event).getContexts();

        if(e.getType() == SBGraphEventType.ADDED)
            service.addStatements(Collections.singleton(s), contexts);
        else if(e.getType() == SBGraphEventType.REMOVED)
            service.removeStatements(Collections.singleton(s), contexts);
    }
    
    private synchronized  void processValue(SBEvent e) {
        if(!SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            LOGGER.error("No Contexts: {}", e);
            return;
        }
        
        SBGraphEvent<SBPredicateValue> event = (SBGraphEvent<SBPredicateValue>) e;
        SBPredicateValue value = event.getData();
        
        URI parent = (URI)((SBEvtWithParent)e).getParent();
        URI[] contexts = ((SBEvtWithContext)event).getContexts();
        SBStatement s = new SBStatement(
                parent.toASCIIString(), 
                value.getPredicate().toASCIIString(),
                value.getValue());

        if(e.getType() == SBGraphEventType.ADDED)
            service.addStatements(Collections.singleton(s), contexts);
        else if(e.getType() == SBGraphEventType.REMOVED)
            service.removeStatements(Collections.singleton(s), contexts);
    }
 

    @Override
    public boolean hasUndo() {
        return currentIndex >= 0;
    }

    @Override
    public boolean hasRedo() {
        return currentIndex < actions.size()-1;
    }

    @Override
    public synchronized void undo() {
        if(hasUndo()) {
            SBAction a = actions.get(currentIndex--);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Undoing action {}: {} ", currentIndex + 1, a.getClass().getSimpleName());
            perform(a.getEvents(true));
        }
    }

    @Override
    public synchronized void redo() {
        if(hasRedo()) {
            SBAction a = actions.get(++currentIndex);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Redoing action {}: {} ", currentIndex, a.getClass().getSimpleName());
            perform(a.getEvents(false));
        }
    }

    private SBEventFilter getFilter(SBGraphEntityType entityType) {
        return (SBEvent event) -> SBGraphEvent.class.isAssignableFrom(event.getClass()) && ((SBGraphEvent)event).getGraphEntityType() == entityType;
    }
}
