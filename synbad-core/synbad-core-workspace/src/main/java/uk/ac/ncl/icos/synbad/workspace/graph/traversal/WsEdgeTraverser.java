/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.util.function.Function;
import java.util.function.Predicate;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;

/**
 * An WorkspaceTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface WsEdgeTraverser<T, V, G extends SBWorkspaceGraph> extends SBGraphTraversal<T, V, G> {

    
//    <V2 extends SBValued> WsEdgeTraverser<T, V2, G> addE(Class<V2> clazz, String label, String predicate);

    <E> WorkspaceTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe);
//
//    <V2 extends SBValued> WsNodeTraverser<T, V2, G> addV(V2 v);

    WsEdgeTraverser<T, V, G> as(String label);

    <V2> WsEdgeTraverser<T, V2, G> choose(Predicate<V> predicate, SBGraphTraversal<V, V2, G> branch1, SBGraphTraversal<V, V2, G> branch2);

    WsEdgeTraverser<T, V, G> copy();

    WsEdgeTraverser<T, V, G> deduplicate();

    <S extends SBGraph> WsEdgeTraverser<T, V, G> doFunction(Function<V, S> function);

    WorkspaceConfigurable<T, V, G> doTraversal(WorkspaceTraversal<V, V, G> traversal);
    
    WsEdgeTraverser<T, V, G> filter(Predicate<V> predicate);

    <X extends WorkspaceTraversal> WsEdgeTraverser<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits, boolean inclusive);

    <X extends WorkspaceTraversal> WsEdgeTraverser<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits);

    <X extends WorkspaceTraversal> WsEdgeTraverser<T, V, G> loop(int iterations, X traversal, boolean emit);

    <V2> WorkspaceConfigurable<T, V2, G> select(String... labels);

    <V2> WorkspaceConfigurable<T, V2, G> select(Class<V2> clazz, String... label);


    //G toGraph();
    
    
    WsNodeTraverser<T, ? extends SBValued, G> v();

    WsNodeTraverser<T, ? extends SBValued, G> v(SBDirection direction);

    <V2 extends SBValued> WsNodeTraverser<T, V2, G> v(SBDirection direction, Class<V2> clazz);
    
}
