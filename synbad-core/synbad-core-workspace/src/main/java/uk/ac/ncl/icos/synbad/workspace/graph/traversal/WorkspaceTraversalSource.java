/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.net.URI;
import java.util.Arrays;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceTraversalSource <N extends SBValued, E extends SBEdge, G extends SBWorkspaceGraph> implements SBGraphTraversalSource<N, E, G> {

        private final G graph;
 
        public WorkspaceTraversalSource (G graph) {
            this.graph = graph;
        }

        @Override
        public WsNodeTraverser<N, N, G> v(N... objs) {
            WorkspaceTraversal<N, N, G> trav = new WorkspaceTraversal<>(graph);
            trav = trav.asConfigurable().addPipe(new SBSourcePipe<>(trav));
            trav.asConfigurable().addInput(Arrays.stream(objs)
                .map(n -> (SBTraverser<N>)new DefaultActionTraverser<>(n)).iterator());
            return trav;
        }

        public <T extends N> WsNodeTraverser<T, T, G> v(URI objId, Class<T> objClass) {
            T obj = graph.getWorkspace().getObject(objId, objClass, graph.getContexts());
            WorkspaceTraversal<T, T, G> trav = new WorkspaceTraversal<>(graph);
            trav = trav.asConfigurable().addPipe(new SBSourcePipe<>(trav));
            trav.asConfigurable().addInput(new DefaultActionTraverser<>(obj));
            return trav;
        }

        @Override
        public WsEdgeTraverser<E, E, G> e( E... edges) {
            WorkspaceTraversal<E, E, G> trav = new WorkspaceTraversal<>(graph);
            trav = trav.asConfigurable().addPipe(new SBSourcePipe<>(trav));
            trav.asConfigurable().addInput(Arrays.stream(edges)
                .map(e -> (SBTraverser<E>)new DefaultActionTraverser<>(e)).iterator());
            return trav;
        }
    }
    
