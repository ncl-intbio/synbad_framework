/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import java.util.List;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;

/**
 *
 * @author owengilfellon
 */
public interface SBCompoundRule<G> {

    public void apply(G g, List<SBEvent> e);


    /*
    public static class Rules<N extends SBIdentified, E extends SBEdge> implements SBRewriterRule<N, E, SBHGraph<N, E>> {

        private final List<SBRewriterRule<N,E,SBHGraph<N, E>>> rules;
        
        public Rules(List<SBRewriterRule<N,E,SBHGraph<N, E>>> rules) {
            this.rules = new ArrayList<>(rules);
        }

        @Override
        public void apply(SBHGraph<N, E> g) {
            rules.stream().forEach((rule) -> {
                rule.apply(g);
            });
        } 

        @Override
        public void apply(SBHGraph<N, E> g, N object) {
            rules.stream().forEach((rule) -> {
                rule.apply(g, object);
            });
        }
        
        
    }
    
    public static class HierarchyRule<N extends SBIdentified, E extends SBEdge> implements SBRewriterRule<N, E, SBHGraph<N, E>> {

        private final Collection<URI> hierarchyEdges;
        
        public HierarchyRule() {
            this.hierarchyEdges = Arrays.asList(
                URI.create(SynBadTerms2.SbolComponent.hasComponent),
                URI.create(SynBadTerms2.SbolComponent.hasSequenceAnnotation),
                URI.create(SynBadTerms2.SbolModule.hasFunctionalComponent),
                URI.create(SynBadTerms2.SbolModule.hasInteraction),
                URI.create(SynBadTerms2.SbolModule.hasModule));
        }
        
        public HierarchyRule(Collection<String> hierarchyEdges) {
            this.hierarchyEdges = hierarchyEdges.stream().map(s -> URI.create(s)).collect(Collectors.toSet());
        }

        @Override
        public void apply(SBHGraph<N, E> g) {
            Set<E> edgeSet = g.edgeSet().stream()
                .filter(e -> hierarchyEdges.contains(e.getEdge()))
                .collect(Collectors.toSet());
            for(E edge: edgeSet) {
                N from = g.getEdgeSource(edge);
                N  to = g.getEdgeTarget(edge);
                g.setParent(from, to);
            }
            
            for(E edge: edgeSet) {
                N from = g.getEdgeSource(edge);
                g.contract(from);
            }
        }

        @Override
        public void apply(SBHGraph<N, E> g, N object) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
        
    }
    
 
    public static class MergeToIntoFrom<N extends SBIdentified, E extends SBEdge> implements SBRewriterRule<N, E, SBHGraph<N, E>> {
       
        private final Collection<String> edges;
        
        public MergeToIntoFrom() {
            this.edges = Arrays.asList(
                SynBadTerms2.Sbol.definedBy,
                "http://www.synbad.org/definition");
        }
        
        public MergeToIntoFrom(Collection<String> hierarchyEdges) {
            this.edges = new HashSet<>(hierarchyEdges);  
        }
        
        @Override
        public void apply(SBHGraph<N, E> g) {

            new HGraphSBTraversal<>(g)
                    .v().as("FROM")
                    .e(SBDirection.OUT, edges.toArray(new String[]{}))
                    .v().setParent("FROM").forEachRemaining(v -> System.out.println("Set parent of " + v));
        }

        @Override
        public void apply(SBHGraph<N, E> g, N object) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
        
        
    } */
  
}
