/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBValidationRule {
    
    public String getValidationCode();
    
    public String getFailureMessage();
    
    public boolean validate(SBWorkspaceGraph graph, SBValued object);
    
}
