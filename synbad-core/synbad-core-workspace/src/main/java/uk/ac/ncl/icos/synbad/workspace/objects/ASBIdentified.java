/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;

/**
 * ASBIdentified is the root class for representing all domain objects within
 SynBad. They are identified. They provide SynBadEdges, which represent
 * predicates between registered SynBadObjects.
 * 
 * @author owengilfellon
 */
public abstract class ASBIdentified extends ASBValued implements SBIdentified {

    transient protected static final SBDataDefManager dataDefinitionManager = SBDataDefManager.getManager();
    private static final long serialVersionUID = -794133892554193894L;
    private final SBIdentity objectIdentity;

    public ASBIdentified(SBIdentity identity, SBWorkspace workspace, SBValueProvider provider) {
        super(identity.getIdentity(), workspace, provider);
        this.objectIdentity = identity;
    }

    @Override
    public String getPrefix() {
        return objectIdentity.getUriPrefix();
    }

    @Override
    public String getDisplayId() {
        return objectIdentity.getDisplayID();
    }
    
    @Override
    public String getVersion() {
        return objectIdentity.getVersion();
    }
    
    @Override
    public String getName() {
        return getValues(SynBadTerms.SbolIdentified.hasName, String.class).stream()
                .filter(s -> !s.isEmpty())
                .findFirst().orElse(getDisplayId());
    }
    
    @Override
    public URI getPersistentId() {
        return getValues(SynBadTerms.SbolIdentified.hasPersistentIdentity, URI.class).iterator().next();
    }

    @Override
    public URI getWasDerivedFrom() {
        return getValues(SynBadTerms.SbolIdentified.wasDerivedFrom, URI.class)
                .stream().findFirst().orElse(null);
    }

    @Override
    public String getDescription() {
       return getValues(SynBadTerms.SbolIdentified.hasDescription, String.class).stream()
                .findFirst().orElse("");
    }
    
    public Collection<Type> getSbolTypes() {
        //System.out.println("Getting roles of: " + getIdentity().toASCIIString());
        Set<Type> roles = getValues(SynBadTerms.Sbol.type).stream()
            .filter(s -> s.isURI())
            .filter(s -> dataDefinitionManager.containsDefinition(Type.class, s.asURI().toASCIIString()))
            .map(s-> dataDefinitionManager.getDefinition(Type.class, s.asURI().toASCIIString()))
            .collect(Collectors.toSet());
        //System.out.println(roles.size());
        return roles;
    }
    
    public Collection<Role> getSbolRoles() {
        //System.out.println("Getting roles of: " + getIdentity().toASCIIString());
        Set<Role> roles = getValues(SynBadTerms.Sbol.role).stream()
            .filter(s -> s.isURI())
            .filter(s -> dataDefinitionManager.containsDefinition(Role.class, s.asURI().toASCIIString()))
            .map(s-> dataDefinitionManager.getDefinition(Role.class, s.asURI().toASCIIString()))
            .collect(Collectors.toSet());
        //System.out.println(roles.size());
        return roles;
    }
/*
    @Override
    public void setDescription(String description) {
        apply(new SetValueAction(objectIdentity.getIdentity(), SynBadTerms.SbolIdentified.hasDescription, SBValue.parseValue(description), getType(), getWorkspace(), values.getContexts()));
    }

    @Override
    public void setName(String name) {
        apply(new SetValueAction(objectIdentity.getIdentity(), SynBadTerms.SbolIdentified.hasName, SBValue.parseValue(name), getType(),  getWorkspace(), values.getContexts()));
    }

    @Override
    public void setWasDerivedFrom(URI derivedFrom) {
        apply(new SetValueAction(objectIdentity.getIdentity(), SynBadTerms.SbolIdentified.wasDerivedFrom, SBValue.parseValue(derivedFrom), getType(), getWorkspace(), values.getContexts()));
    }

    public void apply(SBAction command) {
        ws.perform(command);
    }*/

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(SBIdentified.class.isAssignableFrom(obj.getClass())))
            return false;

        SBIdentified otherObj = (SBIdentified) obj;

        URI[] contexts1 = getContexts();
        URI[] contexts2 = otherObj.getContexts();

        if(!otherObj.getIdentity().equals(getIdentity()))
            return false;
        
        if(contexts1.length != contexts2.length)
            return false;
        
        return Arrays.equals(contexts1, contexts2);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(getIdentity());
        for(URI uri : getContexts()) {
            hash = hash + Objects.hashCode(uri);
        }
        return hash;
    }

    @Override
    public boolean is(String rdfType) {
        if(rdfType.isEmpty() || SBIdentityHelper.getURI(rdfType) == null)
            return false;
        
        
        return getValues(SynBadTerms.Rdf.hasType).stream()
                .filter(v -> v.isURI())
                .map(v -> v.asURI().toASCIIString())
                .anyMatch(s -> s.equals(rdfType));
        
    }
}
