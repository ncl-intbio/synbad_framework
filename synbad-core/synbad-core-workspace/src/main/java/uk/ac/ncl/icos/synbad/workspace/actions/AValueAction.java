/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.domain.SBPredicateValue;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

/**
 *
 * @author owengilfellon
 */
public class AValueAction extends ADomainAction<SBValue> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AValueAction.class);
    private final URI object;
    private final URI predicate;
    private final SBValue value;
    private final URI ownerType;
    private final SBIdentityFactory f;

    public AValueAction(SBGraphEventType type, URI owner, URI ownerType, URI predicate, SBValue value, SBWorkspace workspace, URI[] contexts) {
        super(type, value, workspace.getIdentity(), contexts);
        this.predicate = predicate;
        this.value = value;
        this.object = owner;
        this.ownerType = ownerType;
        this.f = workspace.getIdentityFactory();
    }

    @Override
    public Collection<SBEvent> getEvents(boolean undo) {

        SBGraphEventType t = type;

        if (undo) {
            switch (t) {
                case ADDED:
                    t = SBGraphEventType.REMOVED;
                    break;
                case REMOVED:
                    t = SBGraphEventType.ADDED;
            }
        }

        SBIdentity objectId = f.getIdentity(object);
        Class c = SBObjectFactoryManager.get().getClassFromType(ownerType);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("{}: {}:{}:{}", type, objectId.getDisplayID(), predicate, value);
        return Arrays.asList(
            new DefaultSBGraphEvent<>(
                t,
                SBGraphEntityType.VALUE,
                new SBPredicateValue(predicate, value),
                SBValue.class,
                workspace,
                SBWorkspace.class,
                object,
                c,
                getContexts()));
    }
    
    @Override
    public String toString() {
        return type + " Value: " + object.toASCIIString();
    }
}
