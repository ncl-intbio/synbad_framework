/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WorkspaceTraversalSource;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBLocalEdgeProvider;

/**
 * Provides read-only SBGraph-based access to an SBWorkspace. Dispatches graph-based
 * events from the workspace.
 * @author owengilfellon
 */
public class SBWorkspaceGraph implements SBGraph<SBValued, SynBadEdge>, SBSubscriber, Closeable {

    private static final Logger logger = LoggerFactory.getLogger(SBWorkspaceGraph.class);
    private static final long serialVersionUID = 8784464836513718593L;
    private final SBWorkspace ws;

    private final SBIdentity workspaceId;
    private final URI[] contexts;
    private final DefaultSBDispatcher eventService;
    
    public SBWorkspaceGraph(SBWorkspace ws, URI[] contexts) {
        this.ws = ws;
        this.workspaceId = ws.getIdentityFactory().getIdentity(ws.getIdentity());
        this.eventService = new DefaultSBDispatcher();
        //this.ws.getDispatcher().subscribe(new SBEventFilter.DefaultFilter(), this);
        this.contexts = contexts;
    }

    /**
     * 
     * @return the workspace that backs this graph
     */
    public SBWorkspace getWorkspace() {
        return ws;
    }
    
    @Override
    public void close()  {
        eventService.close();
        ws.getDispatcher().unsubscribe(this);
    }
    
    public URI[] getContexts() {
        return this.contexts.length > 0 ? this.contexts : ws.getContextIds();
    }

    @Override
    public boolean containsEdge(SBValued from, SBValued to) {
        return getCursor(from, getContexts()).getEdges(SBDirection.OUT).stream().anyMatch(e -> ws.getEdgeTarget(e, getContexts()).getIdentity().equals(to.getIdentity()));
    }

    @Override
    public SynBadEdge getEdge(SBValued from, SBValued to) {
        return getCursor(from, getContexts()).getEdges(SBDirection.OUT).stream()
                .filter(e -> ws.getEdgeTarget(e, getContexts()).getIdentity().equals(to.getIdentity()))
                .findFirst().orElse(null);
    }

    @Override
    public Set<SynBadEdge> getAllEdges(SBValued node) {
        SBCursor<SBValued, SynBadEdge> c = getCursor(node, getContexts());
        return Stream.concat(c.getEdges(SBDirection.IN).stream(),
            c.getEdges(SBDirection.OUT).stream()).collect(Collectors.toSet());
    }

    @Override
    public Set<SynBadEdge> getAllEdges(SBValued sourceNode, SBValued targetNode) {
        return getCursor(sourceNode, getContexts()).getEdges(SBDirection.OUT).stream()
                .filter(e -> ws.getEdgeTarget(e, getContexts()).getIdentity()
                    .equals(targetNode.getIdentity())).collect(Collectors.toSet());
    }

    @Override
    public SBValued getEdgeSource(SynBadEdge edge) {
        return ws.getObject(edge.getFrom(), SBValued.class, contexts);
    }

    @Override
    public SBValued getEdgeTarget(SynBadEdge edge) {
        return ws.getObject(edge.getTo(), SBValued.class, contexts);
    }

    @Override
    public List<SynBadEdge> incomingEdges(SBValued node) {
        return new ArrayList(getCursor(node, getContexts()).getEdges(SBDirection.IN));
    }

    @Override
    public List<SynBadEdge> outgoingEdges(SBValued node) {
        return new ArrayList(getCursor(node, getContexts()).getEdges(SBDirection.OUT));
    }

    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        eventService.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        eventService.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return eventService.getListeners();
    }

    @Override
    public WorkspaceTraversalSource<SBValued, SynBadEdge, SBWorkspaceGraph> getTraversal() {
        return new WorkspaceTraversalSource<>(this);
    }

    @Override
    public void onEvent(SBEvent e) {
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {
            eventService.publish(e);
        }
    }

    private SBCursor<SBValued, SynBadEdge> getCursor(SBValued startingNode, URI[] contexts) {

        return new DefaultSBCursor<>(startingNode, new SBLocalEdgeProvider<SBValued, SynBadEdge>(){

            @Override
            public SBValued getEdgeSource(SynBadEdge edge) {
                return ws.getEdgeSource(edge, contexts);
            }

            @Override
            public SBValued getEdgeTarget(SynBadEdge edge) {
                return ws.getEdgeTarget(edge, contexts);
            }

            @Override
            public List<SynBadEdge> incomingEdges(SBValued node) {
                return ws.incomingEdges(node, contexts).stream()
                        .filter(e -> node.getIdentity().equals(getEdgeTarget(e).getIdentity()))
                        .collect(Collectors.toList());
            }

            @Override
            public List<SynBadEdge> outgoingEdges(SBValued node) {
                return ws.outgoingEdges(node, contexts).stream()
                        .filter(e -> node.getIdentity().equals(getEdgeSource(e).getIdentity()))
                        .collect(Collectors.toList());
            }
        });
    }


    @Override
    public SBCursor<SBValued, SynBadEdge> createCursor(SBValued startNode) {
       return getCursor(startNode, getContexts());
    }

    @Override
    public String toString() {
        return "WSGraph[ ".concat(workspaceId.getDisplayID()).concat(" ]");
    }
}
