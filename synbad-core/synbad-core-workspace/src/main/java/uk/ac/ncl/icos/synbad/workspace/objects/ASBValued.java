/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.objects;

import org.apache.jena.ext.com.google.common.collect.Streams;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;

/**
 * SynBadObject is the root class for representing all domain objects within
 * SynBad. They are identified. They provide SynBadEdges, which represent
 * predicates between registered SynBadObjects.
 * 
 * @author owengilfellon
 */
public abstract class ASBValued implements SBValued {

    transient protected static final SBDataDefManager dataDefinitionManager = SBDataDefManager.getManager();
    private static final long serialVersionUID = 5874662565469052187L;
    private final URI objectIdentity;
    protected final SBValueProvider values;
    transient protected SBWorkspace ws;

    public ASBValued(URI identity, SBWorkspace workspace, SBValueProvider provider) {
       
        if(identity == null )
            throw new NullPointerException("Identity cannot be null");
        
        if(workspace == null)
            throw new NullPointerException("Workspace identity cannot be null");
                
        if(provider == null)
            throw new NullPointerException("Value provider cannot be null");
        
        this.objectIdentity = identity;
        this.values = provider;
        this.ws = workspace;
    }

    /**
     *
     * @return a list of outgoing edges to objects owned by this object
     */
    public Set<URI> getOwnedEdges() {
        return Arrays.stream(getClass().getMethods())
                .map(m -> m.getAnnotation(OwnedEdge.class))
                .map(OwnedEdge::id)
                .map(URI::create)
                .collect(Collectors.toSet());
    }

    protected <T extends SBValued> T getOutgoingObject(String edge, Class<T> clazz) {
        return getOutgoingObjects(edge, clazz).stream().findFirst().orElse(null);
    }

    protected <T extends SBValued> Set<T> getOutgoingObjects(String edge, Class<T> clazz) {
        return ws.outgoingEdges(this, edge,  clazz, getContexts()).stream()
                .map(e -> ws.getObject(ws.getEdgeTarget(e, getContexts()).getIdentity(), clazz, getContexts()))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    protected <T extends SBValued> T getIncomingObject(String edge, Class<T> clazz) {
        return getIncomingObjects(edge, clazz).stream().findFirst().orElse(null);
    }

    protected <T extends SBValued> Set<T> getIncomingObjects(String edge, Class<T> clazz) {
        return ws.incomingEdges(this, edge,  clazz, getContexts()).stream()
                .map(e -> ws.getObject(ws.getEdgeSource(e, getContexts()).getIdentity(), clazz, getContexts()))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    @Override
    public URI getIdentity() {
        return objectIdentity;
    }

    @Override
    public URI[] getContexts() {
        return values.getContexts();
    }
    
    public URI[] getSystemContexts() {
        return ws.getSystemContextIds(getContexts());
     }

    private Set<SBValue> getUrisByEdge(String predicate) {
        return ws.outgoingEdges(this, getContexts()).stream()
                .filter(e -> e.getEdge().toASCIIString().equals(predicate))
                .map(SBEdge.DefaultEdge::getTo)
                .map(SBValue::parseValue)
                .collect(Collectors.toSet());
    }

    @Override
    public SBValue getValue(String predicate) {
        return getValues(predicate).stream().findFirst().orElse(null);
    }
    
    @Override
    public <T> Collection<T> getValues(String predicate, Class<T> clazz) {
        return getValues(predicate).stream().filter(v ->
            clazz.isAssignableFrom(v.getValue().getClass())
        ).map(v -> clazz.cast(v.getValue())).collect(Collectors.toSet());
    }
    
    @Override
    public Collection<SBValue> getValues(String predicate) {
        return Streams.concat(values.getValues(predicate).stream(), getUrisByEdge(predicate).stream()).collect(Collectors.toSet());
    }
    
    @Override
    public Set<String> getPredicates() {
        return values.getPredicates();
    }
    
    public void apply(SBAction command) {
        ws.perform(command);
    }


    @Override
    public String toString() {
        return getIdentity().toASCIIString(); // !getName().isEmpty() ? getName() : getDisplayId();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(ASBValued.class.isAssignableFrom(obj.getClass())))
            return false;

        ASBValued otherObj = (ASBValued) obj;
        
        // TODO: Include Contexts in equality checks?
        
        URI[] contexts1 = getContexts();
        URI[] contexts2 = otherObj.getContexts();
        
        
        if(!otherObj.getIdentity().equals(getIdentity()))
            return false;
        
        if(contexts1.length != contexts2.length)
            return false;
        
        return Arrays.equals(contexts1, contexts2);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(getIdentity());
        for(URI uri : getContexts()) {
            hash = hash + Objects.hashCode(uri);
        }
        return hash;
    }

    @Override
    public SBWorkspace getWorkspace() {
        return ws;
    }

    @Override
    public boolean is(String rdfType) {
        if(rdfType.isEmpty() || SBIdentityHelper.getURI(rdfType) == null)
            return false;
        
        return values.getValues(SynBadTerms.Rdf.hasType).stream()
                .filter(SBValue::isURI)
                .map(v -> v.asURI().toASCIIString())
                .anyMatch(s -> s.equals(rdfType));
        
    }

    @Override
    public <T extends SBValued> Optional<T> as(Class<T> clazz) {
        if(clazz.isAssignableFrom(getClass()))
            return Optional.of(clazz.cast(this));
        
        return Optional.ofNullable(ws.getObject(getIdentity(), clazz, getContexts()));
    }
    

}
