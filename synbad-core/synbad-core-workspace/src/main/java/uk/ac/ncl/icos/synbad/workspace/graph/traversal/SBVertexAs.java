/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.graph.traversal;

import java.util.Collection;
import java.util.Collections;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBVertexAs<T, V> extends DefaultSBTraversalPipe<T, V>{

    private static final Logger logger =  LoggerFactory.getLogger(SBVertexAs.class);
    private final Class<V> clazz;
    
    public SBVertexAs(SBTraversal traversal, Class<V> clazz) {
        super(traversal);
        this.clazz = clazz;
    }
    
    protected SBVertexAs(SBTraversal parentTraversal, SBVertexAs<T, V> traversal) {
        super(parentTraversal, traversal);
        this.clazz = traversal.clazz;
    }
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        
        logger.debug("Processing: [{}]", traverser.toString());
        
        SBTraverser<T> t = traverser.duplicate();
        T obj = t.pop();
        
        if(obj == null) { 
            logger.error("Traverser's object is null!");
            return Collections.EMPTY_SET;
        }
        else if (!SBValued.class.isAssignableFrom(obj.getClass())) {
            logger.error("Object does not inherit from SBValued: [{}:{}]", obj, obj.getClass());
            return Collections.EMPTY_SET;
        }
        else if (!SBValued.class.isAssignableFrom(clazz)) {
            logger.error("Class does not inherit from SBValued: [{}]", clazz.getSimpleName());
            return Collections.EMPTY_SET;
        } 

        SBWorkspace ws = ((SBValued)obj).getWorkspace();
        
        if(ws == null) {
            logger.error("No workspace found for: [{}]", ((SBValued)obj).getWorkspace());
            return Collections.EMPTY_SET;
        }
        
        // We've checked that the clazz extends SBValued and know clazz is Class<V>, so this is safe
        
        SBValued valued = ws.getObject(((SBValued)obj).getIdentity(), (Class<? extends SBValued>) clazz, ((SBValued)obj).getContexts());
        if(valued == null) {
            logger.debug("Could not retrieve as {}: [{}]", clazz.getSimpleName(), valued);
            return Collections.EMPTY_SET;
        }
       
        return Collections.singleton(t.push((V)valued));
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBVertexAs<>(parentTraversal, this);
    }

    
    

}
