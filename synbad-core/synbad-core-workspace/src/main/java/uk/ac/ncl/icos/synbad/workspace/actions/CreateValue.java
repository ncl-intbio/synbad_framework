/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.SynBadAction;

/**
 *
 * @author owengilfellon
 */
@SynBadAction(type = SBGraphEventType.ADDED, clazz = SBValue.class)
public class CreateValue extends AValueAction {

    public CreateValue(URI identity, String predicate, SBValue v, URI ownerType, SBWorkspace workspace, URI[] contexts) {
        super(SBGraphEventType.ADDED, 
            identity,
            ownerType,
            SBIdentityHelper.getURI(predicate),
            v,
            workspace,
            contexts);
    }
    
}
