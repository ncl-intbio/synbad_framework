/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.events;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public abstract class ASBWorkspaceSubscriber implements SBWorkspaceSubscriber {

    private static final Logger logger = LoggerFactory.getLogger(ASBWorkspaceSubscriber.class);

    @Override
    public void onEvent(SBEvent e) {
        
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {

            SBGraphEvent evt = (SBGraphEvent) e;
                    
            if(null != evt.getGraphEntityType())
                switch (((SBGraphEvent)e).getGraphEntityType()) {
                    case EDGE:
                        onEdgeEvent(evt);
                        break;
                    case NODE:
                        onNodeEvent(evt);
                        break;
                    case VALUE:
                        onValueEvent(evt);
                        break;
                    default:
                        break;
            }
        }
    }

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> evt) { }

    @Override
    public void onNodeEvent(SBGraphEvent<URI> evt) { }

    @Override
    public void onValueEvent(SBGraphEvent<SBValue> evt) { }
}
