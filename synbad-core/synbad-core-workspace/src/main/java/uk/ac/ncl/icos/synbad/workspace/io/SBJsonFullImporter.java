/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.apache.commons.io.IOUtils;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
//import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author Owen
 */
public class SBJsonFullImporter implements SBWorkspaceImporter {

    /**
     * Populates a workspace from a json encoded WorkspaceJson provided as an
     * InputStream.
     * @param os
     * @param workspace
     * @param contexts Is ignored, as they are provided by WorkspaceJson  
     */
    @Override
    public void importWs(InputStream os, SBWorkspace workspace, URI[] contexts) {
        
        try {
            String json = IOUtils.toString(os, "UTF-8");
            SBWorkspaceExport.WorkspaceJson export = new Gson().fromJson(json, SBWorkspaceExport.WorkspaceJson.class);
            for(SBWorkspaceExport.ContextJsonPair pair : export.getContexts()) {
                URI uri = SBIdentityHelper.getURI(pair.getUri());
                JsonElement jsonElement = pair.getJson();
                InputStream inputStream = IOUtils.toInputStream(jsonElement.toString(), "UTF-8");
                SBJsonImporter importer = new SBJsonImporter();
                importer.importWs(inputStream, workspace, new URI[] { uri });
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    public void importWs(InputStream os, SBWorkspace workspace) {
        importWs(os, workspace, null);
    }
    
    /**
     * Imports a set of workspaces, as provided by a JSON-encoded SBWorkspaceExport
     * using an InputStream
     * @param os 
     */
    /*
    public void importWs(InputStream os) {
        try {
            String json = IOUtils.toString(os, "UTF-8");
            SBWorkspaceExport export = new Gson().fromJson(json, SBWorkspaceExport.class);
            for(SBWorkspaceExport.WorkspaceJson workspace : export.getWorkspaces()) {
                URI wsUri = SBIdentityHelper.getURI(workspace.getUri());
                SBWorkspace ws = Lookup.getDefault().lookup(SBWorkspaceManager.class).getWorkspace(wsUri);
                for(SBWorkspaceExport.ContextJsonPair pair : workspace.getContexts()) {
                    URI uri = SBIdentityHelper.getURI(pair.getUri());
                    JsonElement jsonElement = pair.getJson();
                    InputStream inputStream = IOUtils.toInputStream(jsonElement.toString(), "UTF-8");
                    SBJsonImporter importer = new SBJsonImporter();
                    importer.importWs(inputStream, ws, new URI[] { uri });
                }
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }*/
}
