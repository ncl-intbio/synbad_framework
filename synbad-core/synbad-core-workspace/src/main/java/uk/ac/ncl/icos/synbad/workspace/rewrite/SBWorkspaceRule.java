/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.rewrite;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author Owen
 */
public interface SBWorkspaceRule extends SBRewriterRule<SBIdentified, SynBadEdge, SBWorkspace>{
    
}
