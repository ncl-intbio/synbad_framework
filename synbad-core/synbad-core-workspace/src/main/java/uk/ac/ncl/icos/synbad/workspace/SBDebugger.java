/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SBDebugger {
    
    public static void printSbIdentified(SBIdentified identified, OutputStream os) {
        PrintWriter writer = new PrintWriter(os);
        writer.println();
        writer.println("============================================");
        writer.println(identified.getDisplayId());
        writer.println("============================================");
        
        SBWorkspace ws = identified.getWorkspace();
        
        for(String predicate : identified.getPredicates() ) {
            Collection<SBValue> values = identified.getValues(predicate);
            for(SBValue v : values) {
                if(!(v.isURI() && ws.containsIdentity(v.asURI(), ws.getContextIds()))) {
                    URI p = URI.create(predicate);
                    writer.printf("%-25s: %s\n", p.getPath() + (p.getFragment() != null ? "#" + p.getFragment() : ""), v);
                }
            }
        }
        
        
        writer.println("------------------  Edges - In  ------------------");
        
        List<URI> allContexts = Arrays.asList(ws.getContextIds());
        URI[] sysCtxs = ws.getSystemContextIds(ws.getContextIds());
        if(sysCtxs != null)
            allContexts.addAll(Arrays.asList());
        URI[] allCtxs = allContexts.toArray(new URI[]{});
        
        ws.incomingEdges(identified, allCtxs).stream().forEach(e -> {
            SBIdentified from = ws.getObject(e.getFrom(), SBIdentified.class, allCtxs);
            SBIdentified   to = ws.getObject(e.getTo(), SBIdentified.class, allCtxs);
             URI p = e.getEdge();
            writer.printf("%-25s: %-25s: %-25s\n", from,  p.getPath() + (p.getFragment() != null ? "#" + p.getFragment() : ""), to);
        });
        
        writer.println("------------------  Edges - Out  ------------------");
        
        ws.outgoingEdges(identified, allCtxs).stream().forEach(e -> {
            SBIdentified from = ws.getObject(e.getFrom(), SBIdentified.class, allCtxs);
            SBIdentified   to = ws.getObject(e.getTo(), SBIdentified.class, allCtxs);
              URI p = e.getEdge();
            writer.printf("%-25s: %-25s: %-25s\n", from,  p.getPath() + (p.getFragment() != null ? "#" + p.getFragment() : ""), to);
        });
        
        writer.flush();
    }
}
