/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.io;

import com.google.gson.JsonElement;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Owen
 */
public class SBWorkspaceExport {
    
    private List<WorkspaceJson> workspaces;

    public List<WorkspaceJson> getWorkspaces() {
        return workspaces;
    }

    public void setWorkspaces(List<WorkspaceJson> workspaces) {
        this.workspaces = workspaces;
    }
    
    public static class WorkspaceJson{

        private String uri;
        private List<ContextJsonPair> contexts;

        public void setContexts(List<ContextJsonPair> contexts) {
            this.contexts = contexts;
        }

        public List<ContextJsonPair> getContexts() {
            return contexts;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }
    }

    public static class ContextJsonPair {
        
        private String uri;
        private JsonElement json;

        public JsonElement getJson() {
            return json;
        }

        public String getUri() {
            return uri;
        }

        public void setJson(JsonElement json) {
            this.json = json;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }
    }
}
