/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.actions;


import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public abstract class AEdgeAction extends ADomainAction<SynBadEdge> {

    private static final Logger LOGGER  = LoggerFactory.getLogger(AEdgeAction.class);

    public AEdgeAction(SBGraphEventType type, SynBadEdge object, URI workspace, URI[] contexts) {
        super(type, object, workspace, contexts);
        
    }

    @Override
    public Collection<SBEvent>  getEvents(boolean undo) {
      
        SBGraphEventType t = type;
        if(undo) {
            switch(type) {
            case ADDED :
                t = SBGraphEventType.REMOVED;
                break;
            case REMOVED :
                t = SBGraphEventType.ADDED;    
            }
        }

       //LOGGER.debug("{}: {}", t, object);
        
        return Arrays.asList(new DefaultSBGraphEvent(t, SBGraphEntityType.EDGE, object, object.getClass(), workspace, SBWorkspace.class, null, null, getContexts()));
  
    }
    
    @Override
    public String toString() {
        return type + " Edge: " + object.getEdge().toASCIIString();
    }
}

