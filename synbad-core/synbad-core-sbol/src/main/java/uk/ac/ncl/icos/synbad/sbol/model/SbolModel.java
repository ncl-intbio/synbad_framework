/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.model;

import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.model.ASBModel;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * @author owengilfellon
 */
public class SbolModel extends ASBModel<SBIdentified, SbolHView> {
    
    private static final Logger logger = LoggerFactory.getLogger(SbolModel.class);

    public SbolModel(SBWorkspace workspace, ModuleDefinition rootNode) {
        super(workspace, rootNode);
    }

    public SbolModel(SBWorkspace workspace, ModuleDefinition rootNode, NamespaceBinding namespace) {
        super(workspace, rootNode, namespace);
    }

    @Override
    public ModuleDefinition getRootNode() {
        return (ModuleDefinition)super.getRootNode(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected SbolHView constructView() {
        return new SbolHViewFactory().getView(getRootNode(), this);
    }

    /*
    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(SbolModel.class.isAssignableFrom(obj.getClass())))
            return false;

        return super.equals(obj);
    }

    @Override
    public int hashCode() {
     
        return super.hashCode();
    }
    */
    

}