/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Model.TYPE)
public class Model extends ASBIdentified implements TopLevel{
    
    public static final String TYPE = "http://sbols.org/v2#Model";

    public Model(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    public URI getSource() {
        return getValue(SynBadTerms.SbolModel.source).asURI();
    }
    
    public URI getLanguage() {
        return getValue(SynBadTerms.SbolModel.language).asURI();
    }
    
    public URI getFramework() {
        return getValue(SynBadTerms.SbolModel.framework).asURI();
    }
 
    @Override
    public URI getType() {
        return URI.create(TYPE);
    }
    
    public static SBAction createModel( URI identity, URI source, URI language, URI framework, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace, contexts)
            .createObject(identity, URI.create(Model.TYPE), null, null)
            .createValue(identity,URI.create(SynBadTerms.SbolModel.source), SBValue.parseValue(source), URI.create(Model.TYPE))
            .createValue(identity,URI.create(SynBadTerms.SbolModel.language), SBValue.parseValue(language), URI.create(Model.TYPE))
            .createValue(identity,URI.create(SynBadTerms.SbolModel.framework), SBValue.parseValue(framework), URI.create(Model.TYPE))
                .build();
    }
}
