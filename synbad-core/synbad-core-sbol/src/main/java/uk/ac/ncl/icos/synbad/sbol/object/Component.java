/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;

import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Component.TYPE, xmlParent = ComponentDefinition.class)
public class Component extends ComponentInstance implements ChildObject {
    
    public static final String TYPE = "http://sbols.org/v2#Component";
    
    public Component(SBIdentity identity, SBWorkspace wsIdentity, SBValueProvider values) {
        super(identity, wsIdentity, values);
    }
    
    @Override
    public ComponentDefinition getParent() {
        return getIncomingObject(SynBadTerms.SbolComponent.hasComponent, ComponentDefinition.class);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public static SBAction createComponent(URI identity, URI parent, URI definition, SbolAccess access, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace, contexts)
            .createObject(identity, SBIdentityHelper.getURI(Component.TYPE), parent, SBIdentityHelper.getURI(ComponentDefinition.TYPE))
            .createEdge(parent, SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasComponent), identity)
            .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.definedBy), definition)
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasAccess), SBValue.parseValue(access.getUri()), SBIdentityHelper.getURI(Component.TYPE))
            .addAction(new CreateEdge(parent, SynBadTerms.SynBadSystem.owns, identity, workspace.getIdentity(), workspace.getSystemContextIds(contexts))).build();
    }
        
    
    public static SBAction createComponent(URI identity, ComponentDefinition parent, ComponentDefinition definition, SbolAccess access, SBWorkspace workspace, URI[] contexts) {
        return createComponent(identity, parent.getIdentity(), definition.getIdentity(), access, workspace, contexts);
    }
    
    public static SBIdentity getComponentIdentity(SBIdentity parentComponentDefId, SBIdentity definitionComponentDefId) {
        return SBIdentity.getIdentity(definitionComponentDefId.getUriPrefix(), parentComponentDefId.getDisplayID(), "c_" + definitionComponentDefId.getDisplayID(), parentComponentDefId.getVersion());
    }

}
