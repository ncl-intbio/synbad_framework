/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = MapsTo.TYPE , xmlParent = ComponentInstance.class)
public class MapsTo extends ASBIdentified {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MapsTo.class);
    public static final String TYPE = "http://sbols.org/v2#MapsTo";

    public MapsTo(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    public SbolInstance getOwner() {
        return getIncomingObjects( SynBadTerms.SbolMapsTo.hasMapsTo, SbolInstance.class)
                .stream().findFirst().orElse(null);
    }
    
    public MapsToRefinement getRefinement() {
        URI uri = getValue(SynBadTerms.SbolMapsTo.hasRefinement).asURI();
        MapsToRefinement refinement = dataDefinitionManager.getDefinition(MapsToRefinement.class, uri.toASCIIString());
        return refinement;
    }
    
    public SbolInstance getRemoteInstance() {
        return getOutgoingObjects(SynBadTerms.SbolMapsTo.hasRemoteInstance, SbolInstance.class)
                .stream().findFirst().orElse(null);
    }
    
    public SbolInstance getLocalInstance() {
        return getOutgoingObjects(SynBadTerms.SbolMapsTo.hasLocalInstance, SbolInstance.class)
                .stream().findFirst().orElse(null);
    }
    
    public static SBAction createMapsTo(URI identity, SbolInstance owner, MapsToRefinement refinement, SbolInstance remoteInstance,  SbolInstance localInstance, URI[] contexts) {

       return new DefaultSBDomainBuilder(owner.getWorkspace(), contexts)
            .createObject(identity, SBIdentityHelper.getURI(MapsTo.TYPE), owner.getIdentity(), owner.getType())
            .createEdge(owner.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasMapsTo), identity)
            .createValue(identity,SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasRefinement), SBValue.parseValue(refinement.getRefinement()), SBIdentityHelper.getURI(MapsTo.TYPE))
            .createEdge(identity,SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasRemoteInstance), remoteInstance.getIdentity())
            .createEdge(identity,SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasLocalInstance), localInstance.getIdentity())
           .build("CreateMapsTo ["+ identity.toASCIIString() + "]");
    }

   public static SBAction createMapsTo(URI identity, URI owner, URI ownerType, MapsToRefinement refinement, URI remoteInstance,  URI localInstance, SBWorkspace workspaceId, URI[] contexts) {
  
       return new DefaultSBDomainBuilder(workspaceId, contexts)
            .createObject(identity, SBIdentityHelper.getURI(MapsTo.TYPE), owner, ownerType)
            .createEdge(owner, SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasMapsTo), identity)
            .createValue(identity,SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasRefinement), SBValue.parseValue(refinement.getRefinement()), SBIdentityHelper.getURI(MapsTo.TYPE))
            .createEdge(identity,SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasRemoteInstance), remoteInstance)
            .createEdge(identity,SBIdentityHelper.getURI(SynBadTerms.SbolMapsTo.hasLocalInstance), localInstance)
            .build("CreateMapsTo ["+ identity.toASCIIString() + "]");

    }

    public static SBIdentity getMapsToIdentity(SBIdentity owner, SBIdentity local, SBIdentity remote, MapsToRefinement refinement) {

        String r = "";

        switch(refinement) {
            case useLocal:
                r = "useLocal";
                break;
            case useRemote:
                r = "useRemote";
                break;
            case verifyIdentical:
                r = "sameAs";
                break;
        }


        String subjectName = SBIdentityHelper.getParentChildDisplayId(local);
        String objectName = SBIdentityHelper.getParentChildDisplayId(remote);

        return SBIdentity.getIdentity(
                owner.getUriPrefix(),
                owner.getDisplayID(),
                "mt_" + subjectName + "_" + r + "_" + objectName,
                owner.getVersion());
    }
}
