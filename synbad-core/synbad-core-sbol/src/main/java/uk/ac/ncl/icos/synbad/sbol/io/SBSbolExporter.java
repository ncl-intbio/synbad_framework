/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import org.apache.jena.riot.RDFFormat;
import org.openide.util.Exceptions;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfFormat;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.io.SBWorkspaceExporter;

/**
 * Writes workspaces to Sbol in XML format, using libSBOLj
 * @author Owen
 */
public class SBSbolExporter implements SBWorkspaceExporter {
    
    private final Logger LOGGER = LoggerFactory.getLogger(SBSbolExporter.class);
 
    @Override
    public void export(OutputStream os, SBWorkspace workspace, URI[] contexts) {

        SBRdfService service = workspace.getRdfService();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        service.export(out, SBRdfFormat.XML, contexts);
        SBOLReader.setKeepGoing(true); 
        SBOLDocument document = null;
        
        try {
            document = SBOLReader.read(new ByteArrayInputStream(out.toByteArray()));
        } catch (SBOLValidationException ex) {
            LOGGER.error("Invalid SBOL found while exporting workspace", ex);
        } catch (IOException ex) {
            LOGGER.error("IO Exception thrown", ex);
        } catch (SBOLConversionException ex) {
            LOGGER.error("Sbol Conversion Exception thrown", ex);
        }
        
        if(SBOLReader.getNumErrors()>0)
            SBOLReader.getErrors().stream().forEach(LOGGER::debug);

        if(document == null)
            return;
        
        LOGGER.debug("Created SBOL Document: [ {} ]", document);

        SBOLWriter.setKeepGoing(true);

        try {
            SBOLWriter.write(document, os);
            os.flush();
        } catch (SBOLConversionException ex) {
            LOGGER.error("Sbol Conversion Exception thrown", ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        LOGGER.debug("Wrote SBOL Document to: [ {} ]", os);
    }

}
