
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObjectAndDependants;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

/**
 *
 * @author owengilfellon
 */
public abstract class ComponentInstance extends ASBIdentified implements SbolInstance<ComponentDefinition> {

    public ComponentInstance(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    public SbolAccess getAccesss() {
        return getValues(SynBadTerms.SbolComponent.hasAccess,
            URI.class).stream()
            .map(v -> SBDataDefManager.getManager().getDefinition(SbolAccess.class, v.toASCIIString()))
            .findFirst().orElse(null);
    }
     
    public MapsTo createMapsTo( SBIdentity identity, MapsToRefinement refinement, ComponentInstance remoteInstance, ComponentInstance localInstance) {
        ws.perform(MapsTo.createMapsTo(identity.getIdentity(), this, refinement, remoteInstance, localInstance, getContexts()));
        return ws.getObject(identity.getIdentity(), MapsTo.class, values.getContexts());
    }
    
    public void removeMapsTo(URI mapsToIdentity) {
        ws.perform(new RemoveObjectAndDependants(mapsToIdentity, getIdentity(), getWorkspace(), getContexts()));
    }

    @Override
    public Collection<Role> getSbolRoles() {
        return getDefinition().map(ASBIdentified::getSbolRoles).orElse(Collections.EMPTY_LIST);
    }

    @Override
    public Collection<Type> getSbolTypes() {
        return getDefinition().map(ASBIdentified::getSbolTypes).orElse(Collections.EMPTY_LIST);
    }
    
    
    @OwnedEdge(id = SynBadTerms.SbolMapsTo.hasMapsTo, clazz = MapsTo.class)
    public Set<MapsTo> getMapsTo() {
        return getOutgoingObjects(SynBadTerms.SbolMapsTo.hasMapsTo,  MapsTo.class);
    }
    
    public MapsTo getMapsTo(URI mapsToIdentity) {
        return getMapsTo().stream()
            .filter(m -> m.getIdentity().equals(mapsToIdentity))
            .findFirst().orElse(null);
    }

    @Override
    public Optional<ComponentDefinition> getDefinition() {
        return getOutgoingObjects(SynBadTerms.Sbol.definedBy, ComponentDefinition.class).stream().findFirst();
    }

}
