/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.actions.SetValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Sequence.TYPE) 
public class Sequence extends ASBIdentified implements TopLevel {
    
    public static final String TYPE = "http://sbols.org/v2#Sequence";
    
    public Sequence(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public String getElements() {
        return getValue(SynBadTerms.SbolSequence.hasElements).asString();
    }
    
    public URI getEncoding() {
        return getValue(SynBadTerms.SbolSequence.hasEncoding).asURI();
    }
    
    public void setElements(String elements) {
        apply(new SetValue(getIdentity(), SynBadTerms.SbolSequence.hasElements, SBValue.parseValue(elements), getType(), ws, getContexts()));
    }

    public void setEncoding(URI encoding) {
        apply(new SetValue(getIdentity(), SynBadTerms.SbolSequence.hasEncoding, SBValue.parseValue(encoding), getType(), ws, getContexts()));
    }
    
    public static SBAction createSequence(URI identity, String elements, String encoding, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace, contexts)
            .createObject(identity, SBIdentityHelper.getURI(Sequence.TYPE), null, null)
            .createValue(identity,SBIdentityHelper.getURI(SynBadTerms.SbolSequence.hasEncoding), SBValue.parseValue(elements), SBIdentityHelper.getURI(Sequence.TYPE))
            .createValue(identity,SBIdentityHelper.getURI(SynBadTerms.SbolSequence.hasElements), SBValue.parseValue(encoding), SBIdentityHelper.getURI(Sequence.TYPE))
            .build("CreateSeq ["+ identity.toASCIIString() + "]");
    }
    
}
