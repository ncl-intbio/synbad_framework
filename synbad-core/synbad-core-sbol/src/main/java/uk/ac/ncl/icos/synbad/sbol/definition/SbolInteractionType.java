/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.definition;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;

/**
 *
 * @author owengilfellon
 */
public enum SbolInteractionType implements SBDataDef, Type {
    
    INHIBITION(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000169")),
    STIMULATION(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000170")),  
    BIOCHEMICAL_REACTION(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000176")),  
    NON_COVALENT_BINDING(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000177")),  
    DEGRADATION(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000179")),  
    GENETIC_PRODUCTION(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000589")),  
    CONTROL(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000168"));
    
    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private SbolInteractionType(URI uri) {
        this.uri = uri;
    }  
}
