/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.rule;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.rewrite.AEdgeFromEdgeRule;
import uk.ac.ncl.icos.synbad.workspace.rewrite.SBWorkspaceRule;

/**
 *
 * @author Owen
 */
@ServiceProvider(service = SBWorkspaceRule.class)
public class AddSbolDependencyStatements extends AEdgeFromEdgeRule {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSbolDependencyStatements.class);
    
    private final List<String> predicates = Arrays.asList(
        SynBadTerms.Sbol.definedBy,
        SynBadTerms.SbolMapsTo.hasLocalInstance,
        SynBadTerms.SbolMapsTo.hasRemoteInstance,
        SynBadTerms.SbolSequenceConstraint.hasObject,
        SynBadTerms.SbolSequenceConstraint.hasSubject//,
        //SynBadTerms2.SbolInteraction.participant
    );

    @Override
    public List<String> getPredicates() {
        return predicates;
    }

    @Override
    public SynBadEdge getEdgeToAdd(SynBadEdge edge) {
        return new SynBadEdge(URI.create(SynBadTerms.SynBadSystem.dependantOn), edge.getFrom(), edge.getTo(), edge.getContexts());
    }
}
