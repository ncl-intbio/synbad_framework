/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Collection.TYPE)
public class Collection extends ASBIdentified implements TopLevel {

    public static final String TYPE = "http://sbols.org/v2#Collection";
    
    public Collection(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }
    
    public List<SBValued> getMembers() {
        return ws.outgoingEdges(this, SynBadTerms.SbolCollecton.hasMember, SBValued.class, values.getContexts())
            .stream().map(e -> ws.getEdgeTarget(e, getContexts())).collect(Collectors.toList());
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

}
