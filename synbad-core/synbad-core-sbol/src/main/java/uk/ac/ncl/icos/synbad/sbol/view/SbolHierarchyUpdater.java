/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.view;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewEdge;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewObject;
import uk.ac.ncl.icos.synbad.tree.ITree;
import uk.ac.ncl.icos.synbad.tree.ITreeNode;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.view.SBWritableView;
import uk.ac.ncl.icos.synbad.view.AViewUpdater;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.view.SBViewUpdater;

/**
 *
 * @author owengilfellon
 */
public final class SbolHierarchyUpdater<
        T extends SBGraph<SBIdentified, SynBadEdge>,
        V extends SBWritableView<
                SbolViewObject,
                SbolViewEdge>> extends AViewUpdater<T, V> implements SBWorkspaceSubscriber, SBViewUpdater<T, V> {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(SbolHierarchyUpdater.class);

    private static final List<String> HIERARCHY_PREDICATES = Arrays.asList(SynBadTerms.Sbol.definedBy,
        SynBadTerms.SbolComponent.hasComponent,
        SynBadTerms.SbolModule.hasFunctionalComponent,
        SynBadTerms.SbolModule.hasInteraction,
        SynBadTerms.SbolModule.hasModule
    );
    
    private final String[] predicates;

    private final V target;
    private final SBIdentified root;
    private final SBWorkspaceGraph graph;
    private final SBWorkspace ws;

    public SbolHierarchyUpdater(T source, V target, SBIdentified root) {
        super(source, target, root);
        this.target = target;
        this.root = root;
        this.ws = root.getWorkspace();
        this.graph =  new SBWorkspaceGraph(ws, root.getContexts());
        this.graph.subscribe(new SBEventFilter.DefaultFilter(), this);
        this.predicates = HIERARCHY_PREDICATES.toArray(new String[] {});
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing tree on construction: [ {} ]", root);
        processTree(SBGraphEventType.ADDED, getTreeFromNode(root));
    }
    
    public SbolHierarchyUpdater(T source, V target, SBIdentified root, String... predicates) {
       super(source, target, root);
        this.target = target;
        this.root = root;
        this.ws = root.getWorkspace();
        this.graph =  new SBWorkspaceGraph(ws, root.getContexts());
        this.graph.subscribe(new SBEventFilter.DefaultFilter(), this);
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing tree on construction: [ {} ]", root);
        if(predicates.length > 0)
            this.predicates = predicates;
        else
            this.predicates = HIERARCHY_PREDICATES.toArray(new String[] {});
        processTree(SBGraphEventType.ADDED, getTreeFromNode(root));
    }

    @Override
    public void close() {
        this.graph.close();
    }
   
    public void processTree(SBGraphEventType type, ITree<Object> tree) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing tree: [{}:{}]", type.getType(), tree.getRootNode().getData());
        Iterator<Object> it = tree.iterator();
        SynBadEdge edge = null;
        
        while(it.hasNext()) {

                Object o = it.next();

                if(SBIdentified.class.isAssignableFrom(o.getClass())) {

                    // For each vafViewObject, add ViewObjectWrapper

                    SbolViewObject viewTo = new SbolViewObject((SBIdentified) o, target);
                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("Adding N: [ " + viewTo + " ] ");
                    
                    if(type == SBGraphEventType.ADDED)
                        target.addNode(viewTo);
                    else if(type == SBGraphEventType.REMOVED)
                        target.removeNode(viewTo);

                    // If an edge is waiting to be added

                    if(edge != null && type == SBGraphEventType.ADDED) {

                        ITreeNode<Object> edgeNode = tree.findNode(edge);

                        // From should already be added

                        SBIdentified vo_from = (SBIdentified)tree
                                .findNode((SBIdentified)edgeNode.getParent().getData())
                                .getData();

                        // Add edge between from and to

                        if(LOGGER.isDebugEnabled())
                            LOGGER.debug("Adding E: [ " + edge + " ] ");

                        for(SbolViewObject viewFrom : target.findViewNodes(vo_from)) {
                            target.addEdge(viewFrom, viewTo, new SbolViewEdge(edge, viewFrom, viewTo, target));
                        }

                        edge = null;
                    }
                } else if (SynBadEdge.class.isAssignableFrom(o.getClass())) {
                    edge = (SynBadEdge) o;
                } 
            }
    }
    
    
    @Override
    public void onValueEvent(SBGraphEvent<SBValue> e) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: V [ {} ] " + e.getClass());
    }
    
    public void processNode(URI uri, SBGraphEventType type) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing node: [{}:{}]", uri.toASCIIString(), type.getType());
        SBIdentified node = ws.getObject(uri, SBIdentified.class, root.getContexts());
        ITree<Object> tree = getTreeFromNode(node);
        processTree(type, tree);
    }

    @Override
    public void onNodeEvent(SBGraphEvent<URI> e) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: N [ {} ]",  e.getData().toASCIIString());
        URI nodeIdentity = e.getData();
        
        // If nodes already exists don't need to traverse tree (unless is root node,
        // which is instantiated in graph without hierarchy traversal)
        
        if(!target.findObjects(nodeIdentity).isEmpty() && !root.getIdentity().equals(nodeIdentity) )
            return;
        
        processNode(nodeIdentity, (SBGraphEventType)e.getType());        
    }

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> e) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: E [ {} ]",  e.getData());
        URI fromIdentity = e.getData().getFrom();
        URI toIdentity = e.getData().getTo();
        
        // Nodes should already be in graph
        
        if(target.findObjects(toIdentity).isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Does not exist in target: N [ {} ] ", toIdentity.toASCIIString());
            return;
        } else if(target.findObjects(fromIdentity).isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Does not exist in target: N [ {} ] ", fromIdentity.toASCIIString());
            return;
        }
        
        if(!target.findObjects(fromIdentity).isEmpty() && !root.getIdentity().equals(fromIdentity) )
            return;
        
        SBIdentified from = ws.getObject(e.getData().getFrom(), SBIdentified.class, root.getContexts());
        ITree<Object> tree = getTreeFromNode(from);
        processTree((SBGraphEventType)e.getType(), tree);

    }
    
    @Override
    public void onEvent(SBEvent e) {
        
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {

            SBGraphEvent evt = (SBGraphEvent) e;
                    
            if(null != evt.getGraphEntityType())
                switch (((SBGraphEvent)e).getGraphEntityType()) {
                    case EDGE:
                        onEdgeEvent(evt);
                        break;
                    case NODE:
                        onNodeEvent(evt);
                        break;
                    case VALUE:
                        onValueEvent(evt);
                        break;
                    default:
                        break;
            }
        }
    }

    @Override
    public Collection<String> getHierarchyPredicates() {
        return HIERARCHY_PREDICATES;
    }
}
