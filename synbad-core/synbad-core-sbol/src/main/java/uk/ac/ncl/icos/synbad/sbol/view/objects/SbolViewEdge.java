/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.view.objects;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class SbolViewEdge extends DefaultSBViewEdge<SbolViewObject, SBIdentified, SynBadEdge> {
    
    public SbolViewEdge(SynBadEdge edge, SbolViewObject from, SbolViewObject to, SBView view) {
        super(edge, from, to, view);
    }

    public SbolViewEdge(SynBadEdge edge, SbolViewObject from, SbolViewObject to, long id, SBView view) {
        super(edge, from, to, id, view);
    }
    
    
    
}
