/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.action;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.actions.SBActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Location;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;

/**
 *
 * @author owen
 */
public interface SBSbolBuilder extends SBActionBuilder {

    <T extends SBSbolBuilder> T addModelAction(URI modelIdentity, ModuleDefinition owner);

    <T extends SBSbolBuilder> T  addParticipation(URI interaction, URI participation);

    <T extends SBSbolBuilder> T  createComponent(URI identity, URI parent, URI definition, SbolAccess access);

    <T extends SBSbolBuilder> T  createComponent(URI identity, ComponentDefinition parent, ComponentDefinition definition, SbolAccess access);

    <T extends SBSbolBuilder> T  createComponentDefinition(URI identity, Type[] types, Role[] roles);

    <T extends SBSbolBuilder> T  createCut(URI identity, int at, URI parent);

    <T extends SBSbolBuilder> T  createFuncComponent(URI identity, ComponentDefinition definition, ModuleDefinition parent, SbolDirection direction, SbolAccess access);

    <T extends SBSbolBuilder> T  createInteraction(URI identity, SbolInteractionType[] types, ModuleDefinition owner, URI[] contexts);

    // ================================
    //            Commands
    // ================================
    <T extends SBSbolBuilder> T  createMapsTo(URI identity, SbolInstance owner, MapsToRefinement refinement, SbolInstance remoteInstance, SbolInstance localInstance);

    <T extends SBSbolBuilder> T  createMapsTo(URI identity, URI owner, URI ownerType, MapsToRefinement refinement, URI remoteInstance, URI localInstance);

    <T extends SBSbolBuilder> T  createModel(URI identity, URI source, URI language, URI framework);

    // Components
    <T extends SBSbolBuilder> T  createModule(URI identity, ModuleDefinition definition, ModuleDefinition parent);

    <T extends SBSbolBuilder> T  createModuleDefinition(URI identity);

    // Interactions
    <T extends SBSbolBuilder> T  createParticipation(URI identity, URI interaction, URI functionalComponent, SbolInteractionRole role);

    <T extends SBSbolBuilder> T  createRange(URI identity, int start, int end);

    // ================================
    //             Actions
    // ================================
    <T extends SBSbolBuilder> T  createSequence(URI identity, String elements, String encoding);

    <T extends SBSbolBuilder> T  createSequenceAnnotation(URI identity, ComponentDefinition owner, Component component, Location... locations);

    <T extends SBSbolBuilder> T  createSequenceConstraint(URI identity, ComponentDefinition owner, Component subject, ConstraintRestriction restriction, Component object);

    <T extends SBSbolBuilder> T  createSequenceConstraint(URI identity, URI owner, URI subject, ConstraintRestriction restriction, URI object);
    
}
