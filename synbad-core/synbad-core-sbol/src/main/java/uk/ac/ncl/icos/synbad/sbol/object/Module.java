/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveObjectAndDependants;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Module.TYPE, xmlParent = ModuleDefinition.class)
public class Module extends ASBIdentified implements SbolInstance<ModuleDefinition>, ChildObject {

    public static final String TYPE = "http://sbols.org/v2#Module";
    
    public Module(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    @Override
    public ModuleDefinition getParent() {
          return getIncomingObject(SynBadTerms.SbolModule.hasModule, ModuleDefinition.class);
    }

    @Override
    public Optional<ModuleDefinition> getDefinition() {
        return getOutgoingObjects(SynBadTerms.Sbol.definedBy, ModuleDefinition.class).stream().findFirst();
    }

    @OwnedEdge(id = SynBadTerms.SbolMapsTo.hasMapsTo, clazz = MapsTo.class)
    public Set<MapsTo> getMapsTo() {
        return getOutgoingObjects(SynBadTerms.SbolMapsTo.hasMapsTo, MapsTo.class);
    }

    @Override
    public Collection<Role> getSbolRoles() {
        return getDefinition().map(md -> md.getSbolRoles()).orElse(Collections.EMPTY_LIST);
    }

    @Override
    public Collection<Type> getSbolTypes() {
        return getDefinition().map(md -> md.getSbolTypes()).orElse(Collections.EMPTY_LIST);
    }

    public MapsTo createMapsTo( SBIdentity identity, MapsToRefinement refinement, ComponentInstance remoteInstance, ComponentInstance localInstance) {
        ws.perform(MapsTo.createMapsTo(identity.getIdentity(), this, refinement, remoteInstance, localInstance, getContexts()));
        return ws.getObject(identity.getIdentity(), MapsTo.class, values.getContexts());
    }

    public void removeMapsTo(URI mapsToIdentity) {
        ws.perform(new RemoveObjectAndDependants(mapsToIdentity, getIdentity(), getWorkspace(), getContexts()));
    }

    public static SBAction createModule(URI identity, ModuleDefinition definition, ModuleDefinition parent,  URI[] contexts) {
        return new DefaultSBDomainBuilder( parent.getWorkspace(), contexts)
                .createObject(identity, SBIdentityHelper.getURI(Module.TYPE), parent.getIdentity(), SBIdentityHelper.getURI(ModuleDefinition.TYPE))
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.definedBy), definition.getIdentity())
                .createEdge(parent.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolModule.hasModule), identity)
                .build("CreateModule ["+ identity.toASCIIString() + "]");
    }
    
    public static SBAction createModule(URI identity, URI definition, URI parent, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace, contexts)
                .createObject(identity, SBIdentityHelper.getURI(Module.TYPE), parent, SBIdentityHelper.getURI(ModuleDefinition.TYPE))
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.definedBy), definition)
                .createEdge(parent, SBIdentityHelper.getURI(SynBadTerms.SbolModule.hasModule), identity)
                .build("CreateModule ["+ identity.toASCIIString() + "]");
    }
    
    public static SBIdentity getModuleIdentity(SBIdentity parentModuleDef, SBIdentity definitionModuleDef) {
            return SBIdentity.getIdentity(definitionModuleDef.getUriPrefix(), parentModuleDef.getDisplayID(), "m_" + definitionModuleDef.getDisplayID(), definitionModuleDef.getVersion());
    }
    
        
        /*
        public boolean validate() {
            SBWorkspace ws = SBWorkspaceManager.getManager().getWorkspace(wsId);
            ModuleDefinition def = ws.getObject(parentIdentity, ModuleDefinition.class, contexts);
            return true;
            //return noLoopsInHierarchy(ws, def, def);
        }
        
        public boolean noLoopsInHierarchy(SBWorkspace ws, SBValued def, SBValued o) {
            
            Set<SBValued> objs = new SBWorkspaceGraph(ws).getTraversal().v(def)
                .e(SBEdgeDirection.IN, SynBadTerms.Sbol.definedBy, SynBadTerms.SbolModule.hasModule)
                .v(SBEdgeDirection.IN).toSet();
 
            if(objs.stream().anyMatch(ob -> ob.getIdentity().equals(def.getIdentity())))
                return false;

            for(SBValued ob : objs) {
                if(noLoopsInHierarchy(ws, def, ob))
                    return false;
            }
            
            return true;
            
        }*/

        /*@Override
        public void onPerformed(SBDispatcher service, boolean undo) {
            
            SBWorkspaceManager m = SBWorkspaceManager.getManager();
            SBWorkspace ws = m.getWorkspace(wsId);
           // if(!noLoopsInHierarchy(ws, ws.getObject(parentIdentity, ModuleDefinition.class), ws.getObject(parentIdentity, ModuleDefinition.class)))
           //     throw new IllegalArgumentException("Cannot be loops in dependencies");

            super.onPerformed(service, undo);
        }*/
    }
    
    

