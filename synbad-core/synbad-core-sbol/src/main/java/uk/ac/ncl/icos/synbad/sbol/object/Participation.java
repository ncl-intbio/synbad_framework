/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Participation.TYPE, xmlParent = Interaction.class)
public class Participation extends ASBIdentified {

    private static final Logger LOGGER = LoggerFactory.getLogger(Participation.class);
    public static final String TYPE = "http://sbols.org/v2#Participation";
    
    public Participation(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public SbolInteractionRole getRole() {
        return dataDefinitionManager.getDefinition(SbolInteractionRole.class, getValue(SynBadTerms.Sbol.role).asURI().toASCIIString());
    }
    
    public Interaction getInteraction() {
        return getIncomingObject(SynBadTerms.SbolInteraction.hasParticipation, Interaction.class);
    }

    public Interaction getParent() {
        return getIncomingObject(SynBadTerms.SbolInteraction.hasParticipation, Interaction.class);
    }
    
    public FunctionalComponent getParticipant() {
        return getOutgoingObject(SynBadTerms.SbolInteraction.participant, FunctionalComponent.class);
    }

    public static SBAction createParticipation(URI identity, URI interaction, URI participant, SbolInteractionRole role, SBWorkspace workspaceId, URI[] contexts ) {
        DefaultSBDomainBuilder sb = new DefaultSBDomainBuilder(workspaceId,  contexts)
                    .createObject(identity, SBIdentityHelper.getURI(Participation.TYPE), interaction,  SBIdentityHelper.getURI(Interaction.TYPE))
                    .createEdge(interaction, SBIdentityHelper.getURI(SynBadTerms.SbolInteraction.hasParticipation), identity)
                    .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolInteraction.participant), participant)
                    .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.role), SBValue.parseValue(role.getUri()), SBIdentityHelper.getURI(Participation.TYPE));
        
        return sb.build("CreatePar ["+ identity.toASCIIString() + "]");
    }

   public static SBIdentity getParticipationIdentity(SBIdentity parent, SBIdentity participant) {
        String displayId = "par_" + participant.getDisplayID();
        URI parId = SBIdentityHelper.getURI(parent.getPersistentId() + "/" + displayId + (parent.getVersion().isEmpty() ? "" : "/" + parent.getVersion()));

        try {
            SBIdentity id = SBIdentity.getIdentity(parId);
            LOGGER.trace("Got Participation ID: {}", id.getIdentity().toASCIIString());
            return id;
        } catch (SBIdentityException e) {
            return null;
        }
    }
}
