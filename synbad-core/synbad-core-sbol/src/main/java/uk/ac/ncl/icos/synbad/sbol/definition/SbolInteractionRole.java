/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.definition;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;

/**
 *
 * @author owengilfellon
 */
public enum SbolInteractionRole implements SBDataDef, Role {
    
    INHIBITOR(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000020")),
    INHIBITED(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000642")),
    STIMULATOR(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000459")),  
    STIMULATED(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000643")),  
    REACTANT(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000010")),  
    PRODUCT(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000011")),  
    PROMOTER(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000598")),  
    MODIFIER(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000019")),  
    MODIFIED(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000644")),  
    TEMPLATE(URI.create("http://identifiers.org/biomodels.sbo/SBO:0000645"));
    
    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private SbolInteractionRole(URI uri) {
        this.uri = uri;
    }  
}
