/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import java.util.Set;

import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.workspace.actions.RemoveEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = Interaction.TYPE)
public class Interaction extends ASBIdentified implements ChildObject {
    
    public static final String TYPE = "http://sbols.org/v2#Interaction";

    public Interaction(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @OwnedEdge(id = SynBadTerms.SbolInteraction.hasParticipation, clazz = Participation.class)
    public Set<Participation> getParticipants() {
        return getOutgoingObjects(SynBadTerms.SbolInteraction.hasParticipation, Participation.class);
    }

    @Override
    public ModuleDefinition getParent() {
        return getIncomingObject(SynBadTerms.SbolModule.hasInteraction, ModuleDefinition.class);
    }

    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    public void addParticipation(Participation participation, URI[] contexts) {
        apply(Interaction.addParticipation(this.getIdentity(), participation.getIdentity(), getWorkspace(), getContexts()));
    }

    public void removeParticipation(Participation participation, URI[] contexts) {
        apply(Interaction.removeParticipation(getWorkspace(), this.getIdentity(), participation, contexts));
    }
    
    public static SBAction createInteraction(URI identity, java.util.Collection<SbolInteractionType> types, ModuleDefinition owner, URI[] contexts) {

        DefaultSBDomainBuilder sb = new DefaultSBDomainBuilder(owner.getWorkspace(), contexts)
                    .createObject(identity, SBIdentityHelper.getURI(Interaction.TYPE), owner.getIdentity(), SBIdentityHelper.getURI(ModuleDefinition.TYPE))
                    .createEdge(owner.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolModule.hasInteraction), identity);
        
        for(SbolInteractionType type : types ) {
            sb = sb.createValue(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.type), SBValue.parseValue(type.getUri()), SBIdentityHelper.getURI(Interaction.TYPE));
        }

        return sb.build("CreateInteraction ["+ identity.toASCIIString() + "]");
    }

    
     public static SBAction addParticipation(URI interaction, URI participation, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace, contexts)
            .createEdge(interaction, 
                SBIdentityHelper.getURI(SynBadTerms.SbolInteraction.hasParticipation), 
                participation)
            .addAction(new CreateEdge(interaction, SynBadTerms.SynBadSystem.dependantOn, participation, 
                    workspace.getIdentity(), workspace.getSystemContextIds(contexts))).build();
    }
    
    public static SBAction removeParticipation(SBWorkspace ws, URI interaction, Participation participation, URI[] contexts){

         return new DefaultSBDomainBuilder(participation.getWorkspace(), contexts)
            .removeEdge(interaction, 
                SBIdentityHelper.getURI(SynBadTerms.SbolInteraction.hasParticipation), 
                participation.getIdentity())
            .addAction(new RemoveEdge(interaction, SynBadTerms.SynBadSystem.owns, participation.getIdentity(),
                    participation.getWorkspace().getIdentity(), ws.getSystemContextIds(contexts)))
             .addAction(new RemoveEdge(interaction, SynBadTerms.SynBadSystem.dependantOn, participation.getIdentity(), 
                    participation.getWorkspace().getIdentity(), ws.getSystemContextIds(contexts))).build();
    }  
  
}
