/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.algorithm;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultSBGraph;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Location;
import uk.ac.ncl.icos.synbad.sbol.object.Location.Cut;
import uk.ac.ncl.icos.synbad.sbol.object.Location.Range;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceAnnotation;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceConstraint;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class ComponentOrderer {
    
    private static final Logger logger = LoggerFactory.getLogger(ComponentOrderer.class);

    private static Map<Component, Integer> getComponentToIndexMap(Set<Component> components) {
        BiMap<Component, Integer> indexComponentMap = HashBiMap.create();
        if(components == null || components.isEmpty())
            return indexComponentMap;

        Component c = components.iterator().next();
        SBWorkspace ws = c.getWorkspace();
        // map each component to it's location
        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(ws, c.getContexts())) {
            // map each component to it's location
            
            components.stream().forEach((Component node) -> {
                Integer location = graph.getTraversal().v(node)
                        .e(SBDirection.IN, SynBadTerms.SbolSequenceAnnotation.positionsComponent)
                        .v(SBDirection.IN, SequenceAnnotation.class).getResult().streamVertexes()
                        .filter(n -> SequenceAnnotation.class.isAssignableFrom(n.getClass()))
                        .map((SBValued v) -> {
                            logger.debug("Processing {}", v);
                            Location l = ((SequenceAnnotation)v).getLocation();
                            int i = -1;
                            if(Range.class.isAssignableFrom(l.getClass()))
                                i = ((Range)l).getStart();
                            else if(Cut.class.isAssignableFrom(l.getClass()))
                                i = ((Cut)l).getCutAt();
                            return i > -1 ? new Integer(i) : null;
                        }).findFirst().orElse(null);
                
                if(location != null) {
                    if(!indexComponentMap.containsKey(location))
                        indexComponentMap.put(node, location);
                    else {
                        int existingIndex = indexComponentMap.get(node);
                        logger.warn("{} is referenced by two SequenceAnnotations ({} and {}), positioning at {}", node, existingIndex, location, existingIndex);
                    }
                }
            });
        }
        
        return indexComponentMap;
    }
   
    
    private static List<Component> getOrderedComponentsBySequenceAnnotation(Set<Component> components) {
        BiMap<Component, Integer> indexComponentMap = (BiMap<Component, Integer>)getComponentToIndexMap(components);
        List<Integer> orderedIndexes = new ArrayList<>(indexComponentMap.values());
        Collections.sort(orderedIndexes);
        List<Component> orderedByIndex = orderedIndexes.stream().map(index -> indexComponentMap.inverse().get(index)).collect(Collectors.toList());
        return orderedByIndex;
    }
    
    private static Boolean hasUpstream(SBGraph<SBIdentified, SynBadEdge> graph, SBIdentified currentNode, SBIdentified soughtNode) {
        
        if(currentNode.getIdentity().equals(soughtNode.getIdentity()))
            return true;
        
        List<SynBadEdge> incomingEdges = graph.incomingEdges(currentNode);
        
        if(incomingEdges.isEmpty())
            return false;
        
        if(incomingEdges.stream().anyMatch(e -> e.getFrom().equals(soughtNode.getIdentity())))
            return true;
        
        return incomingEdges.stream().map(e -> graph.getEdgeSource(e)).anyMatch(n -> hasUpstream(graph, n, soughtNode));
    }
    
    private static DefaultSBGraph<SBIdentified, SynBadEdge> getConstraintsGraph(Set<Component> components) {
        
        Component c = components.iterator().next();
        SBWorkspace ws = c.getWorkspace();

        DefaultSBGraph<SBIdentified, SynBadEdge> constraintsGraph = new DefaultSBGraph<>();
        try (SBWorkspaceGraph graph = new SBWorkspaceGraph(ws, c.getContexts())) {
            
            components.stream().forEach((Component node) -> {
                
                if(!constraintsGraph.containsNode(node))
                    constraintsGraph.addNode(node);
                
                Set<SBIdentified> preceded = graph.getTraversal().v(node)
                        .e(SBDirection.IN, SynBadTerms.SbolSequenceConstraint.hasSubject).v(SBDirection.IN, SequenceConstraint.class)
                        .filter(n -> n.getRestriction() == ConstraintRestriction.PRECEDES)
                        .e(SBDirection.OUT, SynBadTerms.SbolSequenceConstraint.hasObject).v(SBDirection.OUT, SBIdentified.class)
                        .getResult().streamVertexes().map(o -> (SBIdentified)o)
                        .collect(Collectors.toSet());

                preceded.stream().forEach((SBIdentified p) -> {
                    if(hasUpstream(constraintsGraph, node, p)) {
                        logger.warn("Cyclical constraints found when processing {} and {}, ignoring {}", node.getDisplayId(), p.getDisplayId(), p.getDisplayId());
                    } else {
                        if(!constraintsGraph.containsNode(p))
                            constraintsGraph.addNode(p);
                        constraintsGraph.addEdge(node, p, new SynBadEdge(ConstraintRestriction.PRECEDES.getUri(), node.getIdentity(), p.getIdentity(), node.getContexts()));
                    }
                });
                
            });
        }
        
        return constraintsGraph;
    }

    
    public static boolean precedes(Component a, Component b) {
        
        if(!a.getParent().getIdentity().equals(b.getParent().getIdentity()))
            return false;
        
        ComponentDefinition componentDefinition = a.getParent();

        Comparator<Component> comparator = new ComponentOrderComparator(componentDefinition);
        List<Component> ordered = componentDefinition.getComponents().stream().sorted(comparator).collect(Collectors.toList());
        return ordered.indexOf(a) < ordered.indexOf(b);
    }
    
    public static class ComponentOrderComparator implements Comparator<Component> {
        private final SBWorkspace workspace;
        
        private final BiMap<Component, Integer> orderedByIndex;
        //private final Set<Component> unOrderedByIndex;
        private final DefaultSBGraph<SBIdentified, SynBadEdge> orderedByConstraint;

        public ComponentOrderComparator(ComponentDefinition definition) {
            workspace =  definition.getWorkspace();
           
            Set<Component> components = definition.getComponents();
            orderedByIndex = (BiMap<Component, Integer>) getComponentToIndexMap(components);
            //unOrderedByIndex = new HashSet<>(components);
            //unOrderedByIndex.removeAll(orderedByIndex.keySet());
            orderedByConstraint = getConstraintsGraph(components);
        }
        
        private Integer getLastUpstreamIndex(Component c) {
            if(orderedByIndex.containsKey(c))
                return orderedByIndex.get(c);
            
            if(!orderedByConstraint.containsNode(c) || orderedByConstraint.incomingEdges(c).isEmpty())
                return null;
            
            Integer  max;
            try (SBWorkspaceGraph graph = new SBWorkspaceGraph(workspace, c.getContexts())) {
                max = orderedByConstraint.incomingEdges(c).stream().map(e -> graph.getEdgeSource(e))
                        .filter(n -> n!= null && orderedByIndex.containsKey(n))
                        .map(n -> orderedByIndex.get(n)).max(Comparator.naturalOrder()).orElse(null);
            }
            return max;
        }
        
        @Override
        public int compare(Component o1, Component o2) {
     
            if(orderedByIndex.containsKey(o1) && orderedByIndex.containsKey(o2)) {
                int o1Index = orderedByIndex.get(o1);
                int o2Index = orderedByIndex.get(o2);
                if(o1Index > o2Index)
                    return -1;
                else if (o1Index == o2Index)
                    return 0;
                else if (o1Index < o2Index)
                    return 1;
            }

            Integer lastUpstreamIndex1 = getLastUpstreamIndex(o1);
            
            if(lastUpstreamIndex1 != null) {
                Integer lastUpstreamIndex2 = getLastUpstreamIndex(o2);
                if( lastUpstreamIndex2 != null) {
                    if(lastUpstreamIndex1 > lastUpstreamIndex2)
                       return -1;
                    else if(lastUpstreamIndex1.equals(lastUpstreamIndex2))
                        return 0;
                    else if(lastUpstreamIndex1 < lastUpstreamIndex2)
                        return 1;
                }
            }

            if(hasUpstream(orderedByConstraint, o2, o1))
                return -1;
            else if (hasUpstream(orderedByConstraint, o1, o2))
                return 1;

            
            // Indexed components precede non placed components.
            
            if(orderedByIndex.containsKey(o1) && !orderedByIndex.containsKey(o2))
                return 1;
            else if(!orderedByIndex.containsKey(o1) && orderedByIndex.containsKey(o2))
                return -1;
            else return 0;

        }
    }
}
