/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.definition;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefProvider;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider( service = SBDataDefProvider.class)
public class SbolDefinitionProvider implements SBDataDefProvider {

    @Override
    public void addDefinitions(SBDataDefManager manager) {

        // Add defined terms

        for(SbolAccess access : SbolAccess.values()) {
            manager.addDefinition(access);
        }
        
        for(SbolDirection direction : SbolDirection.values()) {
            manager.addDefinition(direction);
        }
        
        for(SbolInteractionRole role : SbolInteractionRole.values()) {
            manager.addDefinition(role);
        }
        
         for(SbolInteractionType type : SbolInteractionType.values()) {
            manager.addDefinition(type);
        }
        
        
        for(MapsToRefinement refinement : MapsToRefinement.values()) {
            manager.addDefinition(refinement);
        }
        
        //manager.addSynonym(MapsToRefinement.useLocal.getRefinement(), SBPortDirection.IN);
        //manager.addSynonym(MapsToRefinement.useRemote.getRefinement(), SBPortDirection.OUT);
        //manager.addSynonym(MapsToRefinement.verifyIdentical.getRefinement(), SBPortDirection.OUT);
    }
}

