/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.algorithm.ComponentOrderer;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.events.ASBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 * ComponentDefinition represents a structural element in an engineered
 * system.
 * @author owengilfellon
 */
@DomainObject(rdfType = ComponentDefinition.TYPE)
public class ComponentDefinition extends ASBIdentified implements TopLevel{

    private static final Logger LOGGER = LoggerFactory.getLogger(ComponentDefinition.class);
    public static final String TYPE = "http://sbols.org/v2#ComponentDefinition";
    private  List<Component> orderedComponents = new ArrayList<>();
    private boolean dirtyComponentList = true;
    
    public ComponentDefinition(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }

    @OwnedEdge(id = SynBadTerms.SbolComponent.hasComponent, clazz = Component.class)
    public Set<Component> getComponents() {
        return getOutgoingObjects(SynBadTerms.SbolComponent.hasComponent, Component.class);
    }
    
     public List<Component> getOrderedComponents() {
        if(dirtyComponentList) {
            Set<Component> components = getComponents();
            if(components.isEmpty()) {
                LOGGER.trace("{} has no components", getDisplayId());
                return Collections.EMPTY_LIST;
            }
            Comparator<Component> comparator = new ComponentOrderer.ComponentOrderComparator(this);
            this.orderedComponents = getComponents().stream().sorted(comparator).collect(Collectors.toList());
           // dirtyComponentList = false;
        }
        return orderedComponents;
    }

    public Set<Sequence> getSequences() {
        return getOutgoingObjects(SynBadTerms.SbolComponent.hasSequence, Sequence.class);
    }

    @OwnedEdge(id = SynBadTerms.SbolComponent.hasSequenceAnnotation, clazz = SequenceAnnotation.class)
    public Set<SequenceAnnotation> getSequenceAnnotations() {
        return getOutgoingObjects(SynBadTerms.SbolComponent.hasSequenceAnnotation, SequenceAnnotation.class);
    }

    @OwnedEdge(id = SynBadTerms.SbolComponent.hasSequenceConstraint, clazz = SequenceConstraint.class)
    public Set<SequenceConstraint> getSequenceConstraints() {
        return getOutgoingObjects(SynBadTerms.SbolComponent.hasSequenceConstraint, SequenceConstraint.class);
    }

    public List<ASBIdentified> getChildren() {
        List<ASBIdentified> children = new ArrayList<>();
        children.addAll(getComponents());
        return children;
    }

    public Component createComponent(SBIdentity identity, ComponentDefinition definition, SbolAccess access) {
        ws.perform(Component.createComponent(identity.getIdentity(), this, definition, access, getWorkspace(), getContexts()));
        return ws.getObject(identity.getIdentity(), Component.class, getContexts());
    }
    
    public Sequence createSequence(SBIdentity identity, URI encoding, String elements) {
        ws.perform(Sequence.createSequence(identity.getIdentity(), elements, encoding.toASCIIString(), ws, getContexts()));
        return ws.getObject(identity.getIdentity(), Sequence.class, getContexts());
    }

    public SequenceAnnotation createSequenceAnnotation(SBIdentity identity, Component component, Location... locations) {
        ws.perform(SequenceAnnotation.createSequenceAnnotation(identity.getIdentity(), this, component, getContexts(), locations));
        return ws.getObject(identity.getIdentity(), SequenceAnnotation.class, getContexts());
    }

    public SequenceConstraint createSequenceConstraint(SBIdentity identity, Component subject, ConstraintRestriction restriction, Component object) {
        ws.perform(SequenceConstraint.createSequenceConstraint(identity.getIdentity(), this, subject, restriction, object, getContexts()));
        return ws.getObject(identity.getIdentity(), SequenceConstraint.class, getContexts());
    }

    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
 
    class OrderedComponentUpdater extends ASBWorkspaceSubscriber {
        
        private final URI identity;

        public OrderedComponentUpdater() {
            this.identity = ComponentDefinition.this.getIdentity();
        }
        
        @Override
        public void onNodeEvent(SBGraphEvent<URI> evt) {
            dirtyComponentList = true;
        }

        @Override
        public String toString() {
            return "ComponentOrderUpdater[ ".concat(ComponentDefinition.this.getDisplayId()).concat(" ]");
        }

        @Override
        public boolean equals(Object obj) {
            
            if(obj == null)
                return false;
            
            if(obj == this)
                return true;
            
            if(!(obj instanceof OrderedComponentUpdater))
                return false;
            
            OrderedComponentUpdater u = (OrderedComponentUpdater) obj;
            
            return u.identity.equals(getIdentity());
        } 

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 83 * hash + Objects.hashCode(this.identity);
            return hash;
        }
    }
    
    public static SBAction createComponentDefinition(URI identity, java.util.Collection<Type> types, java.util.Collection<Role> roles, SBWorkspace workspace, URI[] contexts) {
        DefaultSBDomainBuilder sb = new DefaultSBDomainBuilder(workspace, contexts);
                
        for(Type type : types) {
            sb = sb.createValue(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.type), SBValue.parseValue(type.getUri()), SBIdentityHelper.getURI(ComponentDefinition.TYPE));
        }
        
        for(Role role : roles) {
            sb = sb.createValue(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.role), SBValue.parseValue(role.getUri()), SBIdentityHelper.getURI(ComponentDefinition.TYPE));
        }
       
        //sb.setEvent(new DefaultSBGraphEventWithSrc(DefaultEventType.ADDED, GraphEntityType.NODE, identity, ComponentDefinition.class, workspace, SBWorkspace.class));
        return sb.createObject(identity, SBIdentityHelper.getURI(ComponentDefinition.TYPE), null, null).build("CreateCD ["+ identity.toASCIIString() + "]");
    }
    
      public static SBAction addSequence(URI sequence, ComponentDefinition owner, URI[] contexts) {
        return new CreateEdge(owner.getIdentity(), SynBadTerms.SbolComponent.hasSequence, sequence, owner.getWorkspace().getIdentity(), contexts);
    }

}
