/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.validation;

import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.object.TopLevel;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.ValidationRule;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WorkspaceTraversal;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBValidationRule;
import uk.ac.ncl.icos.synbad.workspace.SBValidationRule;

/**
 *
 * @author owengilfellon
 */
public class SbolValidation {

    private static final Logger logger = LoggerFactory.getLogger(SbolValidation.class);

    @ServiceProvider(service = SBValidationRule.class)
    @ValidationRule
    public static class Sbol10204 extends DefaultSBValidationRule {
        public Sbol10204() {
            super("sbol-10204",
                "displayId should be a string, consisting only of alphanumeric or underscore characters. Must not begin with a digit",
                new WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph>()
                        .as(SBIdentified.class)
                        
                        // allow only failures - if does not have display id, or if display id is invalid
                        
                        .filter(identified -> identified.getDisplayId().isEmpty() || !identified.getDisplayId().matches("^[^0-9][A-Za-z0-9_]*$"))
                        .as(SBValued.class),     
                c -> c.isEmpty());
        }
    }
    
    @ServiceProvider(service = SBValidationRule.class)
    @ValidationRule
    public static class Sbol10206 extends DefaultSBValidationRule {
        public Sbol10206() {
            super("sbol-10206",
                "version is OPTIONAL. Must be a string, composed of alphanumeric characters, underscores, hyphens, or periods. Must begin with digit.",
                new WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph>()
                    .as(SBIdentified.class)
                    .filter(identified -> !identified.getVersion().isEmpty() && !identified.getVersion().matches("^[0-9][A-Za-z0-9_\\-\\.]*$"))
                    .as(SBValued.class),
                c -> c.isEmpty());
        }
    }
    
    @ServiceProvider(service = SBValidationRule.class)
    @ValidationRule
    public static class Sbol10208 extends DefaultSBValidationRule {
        public Sbol10208() {
            super("sbol-10208",
                "wasDerivedFrom is optional and must be a URI",
                new WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph>()
                     .filter(o -> o.getValue(SynBadTerms.SbolIdentified.wasDerivedFrom) != null && o.getValue(SynBadTerms.SbolIdentified.wasDerivedFrom).isURI()),
                c -> c.isEmpty());
        }
    }
    
    @ServiceProvider(service = SBValidationRule.class)
    @ValidationRule
    public static class Sbol10209 extends DefaultSBValidationRule {
        public Sbol10209() {
            super("sbol-10209",
                "wasDerivedFrom should not refer to an object's own identity",
                new WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph>()
                    .as(SBIdentified.class)
                    .filter(identified -> identified.getWasDerivedFrom() != null && identified.getWasDerivedFrom().equals(identified.getIdentity()))
                    .as(SBValued.class),
                c -> c.isEmpty());
        }
    }    /**/
    
    @ServiceProvider(service = SBValidationRule.class)
    @ValidationRule
    public static class Sbol10216 extends DefaultSBValidationRule {

        public Sbol10216() {
            super("sbol-10216",
                "The persistentIdentity property of a compliant TopLevel object "
                        + "is REQUIRED and MUST contain a URI that ends with a "
                        + "delimiter (’/’, ’#’, or ’:’) followed by the displayId "
                        + "of the TopLevel object",
                new WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph>()
                    .as(TopLevel.class)
                    .filter(identified -> {
                        
                        if(identified.getPersistentId() == null) {
                            logger.error("[{}]:persistentIdentity is missing", identified);
                            return true;
                        }

                        String displayId = SBIdentityHelper.getDisplayIdFromId(identified.getPersistentId());

                        if(!displayId.equals(identified.getDisplayId())) {
                            logger.error("displayId [{}] is not in persistentIdentity [{}]", identified.getDisplayId(), identified.getPersistentId().toASCIIString());
                            return true;
                        }
                        
                        return false;
                    }).as(SBValued.class),
                c -> c.isEmpty());
        }
    } 
    
    /*
    @ServiceProvider(service = SBValidationRule.class)
    @ValidationRule
    public static class Sbol10217 extends DefaultValidationRule {
        public Sbol10217() {
            super("sbol-10217",
                "The persistentIdentity property of a compliant Identified object that "
                        + "is not also a TopLevel object is REQUIRED and MUST contain a URI "
                        + "that begins with the persistentIdentity of the compliant object’s "
                        + "parent and is immediately followed by a delimiter (’/’, ’#’, or ’:’) "
                        + "and the displayId of the compliant object.",
                new WorkspaceTraversal<SBValued, SBValued, SBWorkspaceGraph>()
                    .as(ChildObject.class)
                    .filter(identified -> {
                        
                        if(identified.getPersistentId() == null) {
                            logger.error("[{}]:persistentId is missing", identified);
                            return true;
                        }

    
                        String parentDisplayId = SBIdentityHelper.getParentDisplayIdFromChildPersistentId(identified.getPersistentId());
                        String childDisplayId = SBIdentityHelper.getChildDisplayIdFromChildPersistentId(identified.getPersistentId());
          
                        if(parentDisplayId == null || parentDisplayId.isEmpty()) {
                            logger.error("Could not get parent displayId from: [{}]",  identified.getPersistentId().toASCIIString());
                            return true;
                        }
                        
                        if(childDisplayId == null || childDisplayId.isEmpty()) {
                            logger.error("Could not get child displayId from: [{}]",  identified.getPersistentId().toASCIIString());
                            return true;
                        }
                        

                        if(!parentDisplayId.equals(identified.getParent().getDisplayId())) {
                            logger.error("First section [{}] should be [{}] in [{}]",  parentDisplayId, identified.getParent().getDisplayId(), identified.getPersistentId().toASCIIString() );
                            return true;
                        }

                        if(!childDisplayId.equals(identified.getDisplayId())) {
                            logger.error("Second section [{}] should be [{}] in [{}]",  childDisplayId, identified.getDisplayId(), identified.getPersistentId().toASCIIString());
                            return true;
                        }

                        return false;
                    }).as(SBValued.class),
                c -> c.isEmpty());
        }
    } */


}
