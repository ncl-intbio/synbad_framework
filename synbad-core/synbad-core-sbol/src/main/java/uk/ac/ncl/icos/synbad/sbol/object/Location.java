/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.actions.SetValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
public abstract class Location extends ASBIdentified {

    public Location(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public URI getOrientation() {
        SBValue v = getValue(SynBadTerms.SbolLocation.hasOrientation);
        return v.isURI() ? v.asURI() : null;
    }
    
    public void setOrientation(URI orientation) {
         apply(new SetValue(getIdentity(), SynBadTerms.SbolLocation.hasOrientation, SBValue.parseValue(orientation), getType(), ws, values.getContexts()));
    }
    /*
    @Override
    public SequenceAnnotation getParent() {
        return ws.incomingEdges(this, SynBadTerms.SbolComponent.hasSequenceAnnotation, SequenceAnnotation.class, getContexts())
            .stream().filter(e -> e != null).map(e -> ws.getEdgeSource(e)).map(o -> (SequenceAnnotation)o).findFirst().orElse(null);
    }*/
 
    
    // ================================
    //             Range
    // ================================
    
    @DomainObject(rdfType = Location.Range.TYPE)
    public static final class Range extends Location {
        
        public static final String TYPE = "http://sbols.org/v2#Range";

        public Range(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
            super(identity, workspaceIdentity, values);
        }
        
        public int getStart() {
            SBValue v = getValue(SynBadTerms.SbolLocation.rangeStart);
            return v.isInt() ? v.asInt() : -1;
        }
        
        public int getEnd() {
            SBValue v = getValue(SynBadTerms.SbolLocation.rangeEnd) ;
            return v.isInt() ? v.asInt() : -1;
        }  
        
        @Override
        public URI getType() {
            return SBIdentityHelper.getURI(TYPE);
        }
        
       
        
        
    
    }

    
    // ================================
    //             Cut
    // ================================
    
    @DomainObject(rdfType = Location.Cut.TYPE)
    public static class Cut extends Location {

        public static final String TYPE = "http://sbols.org/v2#Cut";
        
        public Cut(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
            super(identity, workspaceIdentity, values);
        }
        
        public int getCutAt() {
            SBValue v = getValue(SynBadTerms.SbolLocation.cutAt) ;
            return v.isInt() ? v.asInt() : -1;
        }
        
         @Override
        public URI getType() {
            return SBIdentityHelper.getURI(TYPE);
        }

    }
    
    
    
    public static SBAction createRange(URI identity, int start, int end, SBWorkspace workspace, URI[] contexts) {

        return new DefaultSBDomainBuilder(workspace, contexts)
            .createObject(identity, SBIdentityHelper.getURI(Location.Range.TYPE), null, null)
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolLocation.rangeStart), SBValue.parseValue(start), SBIdentityHelper.getURI(Range.TYPE))
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolLocation.rangeEnd), SBValue.parseValue(end), SBIdentityHelper.getURI(Range.TYPE)).build();           
    
    }


    public static SBAction createCut(URI identity, int at,  SBWorkspace workspace, URI[] contexts) {

        return new DefaultSBDomainBuilder(workspace, contexts)
            .createObject(identity, SBIdentityHelper.getURI(Location.Cut.TYPE), null, null)
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolLocation.cutAt), SBValue.parseValue(at), SBIdentityHelper.getURI(Cut.TYPE)).build();           
    
    }
}
