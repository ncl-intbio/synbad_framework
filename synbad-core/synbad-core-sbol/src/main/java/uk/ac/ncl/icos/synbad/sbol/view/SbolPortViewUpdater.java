/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.view;

/**
 *
 * @author owengilfellon
 */
public final class SbolPortViewUpdater {/*<
        V extends DefaultSbolPView> 
        extends AViewUpdater<SBWorkspaceGraph, V> implements SBWorkspaceSubscriber {
        
    private static final Logger logger = LoggerFactory.getLogger(SbolPortViewUpdater.class);

    private static final List<String> HIERARCHY_PREDICATES = Arrays.asList(
        SynBadTerms2.Sbol.definedBy,
        SynBadTerms2.SbolComponent.hasComponent,
        SynBadTerms2.SbolModule.hasInteraction,
        SynBadTerms2.SbolModule.hasFunctionalComponent,
        SynBadTerms2.SbolModule.hasInteraction,
        SynBadTerms2.SbolModule.hasModule,
        SynBadTerms2.SbolMapsTo.hasMapsTo
    );
    
    private static final List<String> PORT_PREDICATES = Arrays.asList(
       
    );
    
    private static final List<String> WIRE_PREDICATES = Arrays.asList(
        SynBadTerms2.SbolMapsTo.hasLocalInstance,
        SynBadTerms2.SbolMapsTo.hasRemoteInstance
    );
    
    //private final V target;
    //private final SBIdentified root
    
    private final Set<TempEdge> edgesToAdd = new HashSet<TempEdge>();
    private final Set<SBIdentified> deferred = new HashSet<>();
    private final SBWorkspace ws;

    public SbolPortViewUpdater(V target, SBIdentified root, SBWorkspace workspace) {
        super(new SBWorkspaceGraph(workspace, root.getContexts()), target, root);
        this.ws = workspace;
        getSourceGraph().subscribe(new SBEventFilter.DefaultFilter(), this);
        logger.debug("Processing tree on construction: [ {} ]", root.getDisplayId());
        processTree(DefaultEventType.ADDED, getTreeFromNode(root));
    }

    @Override
    public void close() {
        this.getSourceGraph().close();
        this.edgesToAdd.clear();
        this.deferred.clear();
    }
    
    @Override
    public Collection<String> getHierarchyPredicates() {
        return HIERARCHY_PREDICATES;
    }

    @Override
    public void onEvent(SBEvent e) {
        
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {

            SBGraphEvent evt = (SBGraphEvent) e;
                    
            if(null != evt.getGraphEntityType())
                switch (((SBGraphEvent)e).getGraphEntityType()) {
                    case EDGE:
                        onEdgeEvent(evt);
                        break;
                    case NODE:
                        onNodeEvent(evt);
                        break;
                    case VALUE:
                        onValueEvent(evt);
                        break;
                    default:
                        break;
            }
        }
    }
    
    
    
    private void createObjectAndAddToOwners(SBIdentified object) {
        Collection<DefaultViewIdentified<SBIdentified>> ownerNodes = getOwnerNodes(object);
        
        if(ownerNodes.isEmpty())
            logger.debug("No owner nodes found: [ {} ]", object.getDisplayId());
        
        for(DefaultViewIdentified<SBIdentified> node : ownerNodes) {
            DefaultViewIdentified<SBIdentified> instanceNode = new DefaultViewIdentified<>(object, target);
            logger.debug("Adding [ {} ] to [ {} ]",instanceNode.getDisplayId(),  node);
            target.addNode((SBViewComponent<SBIdentified>)instanceNode, (SBViewComponent<SBIdentified>)node);
        } 
    }
    
    private void createComponentAndAddToOwners(Component object) {
        Collection<DefaultViewIdentified<SBIdentified>> ownerNodes = getOwnerNodes(object);
        
        if(ownerNodes.isEmpty())
            logger.debug("No owner nodes found: [ {} ]", object.getDisplayId());
        
        List<Component> orderedComponents = object.getDefinition().getOrderedComponents();
        
        for(DefaultViewIdentified<SBIdentified> node : ownerNodes) {
            DefaultViewIdentified<SBIdentified> instanceNode = new DefaultViewIdentified<>(object, target);
            logger.debug("Adding [ {} ] to [ {} ]",instanceNode.getDisplayId(),  node);
            
            int index = 0;
            
            for(DefaultViewIdentified<? extends SBIdentified> child : target.getChildren((SBViewComponent<SBIdentified>)node)) {
                if(orderedComponents.indexOf((Component)child.getObject()) < orderedComponents.indexOf(object) ) {
                    index ++;
                }
            }
            
            target.addNode((SBViewComponent<SBIdentified>)instanceNode, (SBViewComponent<SBIdentified>)node, index);
        } 
    }
    
    private void removeObjectFromOwners(SBIdentified object) {
        Collection<DefaultViewIdentified<SBIdentified>> ownerNodes = getOwnerNodes(object);
        
        if(ownerNodes.isEmpty())
            logger.debug("No owner nodes found: [ {} ]", object.getDisplayId());
        
        for(DefaultViewIdentified<SBIdentified> node : ownerNodes) {
            SBViewComponent<? extends SBIdentified> toRemove = target.getChildren((SBViewComponent<SBIdentified>)node).stream()
                .filter(n -> n.getObject().getIdentity().equals(object.getIdentity()))
                .findFirst().orElse(null);
            logger.debug("Removing [ {} ]", object.getDisplayId());
            if(toRemove != null)
                target.removeNode((SBViewComponent<SBIdentified>)toRemove);
        } 
    }

    // ========================================================================
    //                          Tree methods
    // ========================================================================

    public void processTree(DefaultEventType type, ITree<Object> tree) {
        logger.debug("Processing tree: [{}:{}]", type.getType(), tree.getRootNode().getData());
        Iterator<Object> it = tree.iterator();
       // SynBadEdge edge = null;
        
        while(it.hasNext()) {

            Object o = it.next();
              
            if(SBIdentified.class.isAssignableFrom(o.getClass())) {

                SBIdentified identified = (SBIdentified) o;
                
                logger.debug("Processing: [ {} ]", identified.getDisplayId());

                if(ws.containsObject(identified.getIdentity(), Module.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), Module.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), Interaction.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), Interaction.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), FunctionalComponent.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), FunctionalComponent.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), Component.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), Component.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), MapsTo.class, identified.getContexts()))
                    deferred.add(ws.getObject(identified.getIdentity(), MapsTo.class, identified.getContexts()));
                
            } else if(SynBadEdge.class.isAssignableFrom(o.getClass())) {          
                SynBadEdge edge = (SynBadEdge) o;          
                SBIdentified from = ws.getObject(edge.getFrom(), SBIdentified.class, root.getContexts());
                SBIdentified to = ws.getObject(edge.getFrom(), SBIdentified.class, root.getContexts());      
                edgesToAdd.add(new TempEdge(from, to, edge.getEdge())); 
                logger.debug("Found edge: [ {} ]", edge.getEdge().toASCIIString());
            }
        }
        
        deferred.stream().forEach(d -> {
                if(ws.containsObject(d.getIdentity(), MapsTo.class, d.getContexts()))
                    processInstance(type, ws.getObject(d.getIdentity(), MapsTo.class, d.getContexts()));

                else if(ws.containsObject(d.getIdentity(), Participation.class, d.getContexts()))
                    processInstance(type, ws.getObject(d.getIdentity(), Participation.class, d.getContexts()));
        
        });
        deferred.clear();
    }
    
    
    // ========================================================================
    //                      Object processing
    // ========================================================================
    
    private void processInstance(DefaultEventType type, Module module) {
        logger.debug("Processing module: [ {} ]", module.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(module);
                break;
            case REMOVED:
                removeObjectFromOwners(module);
                break;
        }
        
    }
    
    private void processInstance(DefaultEventType type, Component component) {
        logger.debug("Processing component: [ {} ]", component.getDisplayId());

        switch(type) {
            case ADDED:
                createComponentAndAddToOwners(component);
                break;
            case REMOVED:
                removeObjectFromOwners(component);
                break;
        }
        
    }
    
    private void processInstance(DefaultEventType type, FunctionalComponent component) {
        
        logger.debug("Processing functional component: [ {} ]", component.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(component);
                break;
            case REMOVED:
                removeObjectFromOwners(component);
                break;
        }
        
        
    }
    
    private void processInstance(DefaultEventType type, Interaction interaction) {
        logger.debug("Processing interaction: [ {} ]", interaction.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(interaction);
                Set<Participation> participations = interaction.getParticipants();
                participations.stream().forEach(deferred::add);
                break;
            case REMOVED:
                removeObjectFromOwners(interaction);
                break;
        }
    }
    
    private void processInstance(DefaultEventType type, Participation participation) {
        logger.debug("Processing participation: [ {} ]", participation.getDisplayId());

        switch(type) {
            case ADDED:
               
                    SbolInteractionRole role = participation.getRole();
                    FunctionalComponent participant = participation.getParticipant();   
                    Interaction interaction = (Interaction)getSourceGraph().getTraversal()
                            .v(participation).e(SBDirection.IN, SynBadTerms2.SbolInteraction.hasParticipation)
                            .v(SBDirection.IN, Interaction.class)
                            .getResult().streamVertexes().findFirst().orElse(null);
           
                    // Add edge from interaction to participant, with label = role   
                    
                    target.findObjects(interaction.getIdentity()).forEach(interactionVo -> {
                    SBViewComponent<? extends SBIdentified> participantVo = target.getChildren(target.getParent(interactionVo)).stream().filter(vo -> vo.getIdentity().equals(participant.getIdentity())).findFirst().orElse(null);
   
                    target.addEdge((SBViewComponent<SBIdentified>)interactionVo, (SBViewComponent<SBIdentified>)participantVo, 
                            new DefaultViewEdge(
                                    new SynBadEdge(
                                        role.getUri(),
                                        interaction.getIdentity(), 
                                        participant.getIdentity()), 
                                    (SBViewComponent<SBIdentified>)interactionVo, 
                                    (SBViewComponent<SBIdentified>)participantVo,
                                    target));

                });
                    
                break;
            case REMOVED:
                //removeObjectFromOwners;
                break;
        }
    }
    

    private void processInstance(DefaultEventType type, MapsTo mapsTo) {
        
        logger.debug("Processing MapsTo: [ {} ]", mapsTo.getDisplayId());

        switch(type) {
            case ADDED:
               // createObjectAndAddToOwners(mapsTo);
                
                SbolInstance owner = mapsTo.getOwner();
                SbolInstance remote = mapsTo.getRemoteInstance();
                SbolInstance local = mapsTo.getLocalInstance();
                
                // add edge between remote and local
                
                target.findObjects(owner.getIdentity()).forEach(n -> {
                    SBViewComponent remoteVo = target.getChildren(n).stream().filter(nRemote -> nRemote.getIdentity().equals(remote.getIdentity())).findFirst().orElse(null);
                    SBViewComponent localVo = target.getChildren(target.getParent(n)).stream().filter(nLocal -> nLocal.getIdentity().equals(local.getIdentity())).findFirst().orElse(null);
                    SBViewPort<MapsTo> vPort = new SBViewPort<>(mapsTo, null, target);
                    switch(mapsTo.getRefinement()) {
                        case useLocal:
                            target.addPort(vPort, (SBViewComponent<SBIdentified>)n, PortDirection.IN, Collections.EMPTY_SET);                            
//                            target.addEdge(vPort, remoteVo, new SBViewEdge<>(new SynBadEdge(mapsTo.getRefinement().getUri(), mapsTo.getIdentity(), remote.getIdentity()), vPort, remoteVo, target));
//                            target.addEdge(localVo, vPort, new SBViewEdge<>(new SynBadEdge(mapsTo.getRefinement().getUri(), local.getIdentity(),  mapsTo.getIdentity()), localVo, vPort,  target));
                            break;
                        case useRemote:
                            target.addPort(vPort, (SBViewComponent<SBIdentified>)n, PortDirection.OUT, Collections.EMPTY_SET);              
//                            target.addEdge(remoteVo, vPort, new SBViewEdge<>(new SynBadEdge(mapsTo.getRefinement().getUri(), remote.getIdentity(), mapsTo.getIdentity()), remoteVo, vPort,  target));
//                            target.addEdge(vPort, localVo,  new SBViewEdge<>(new SynBadEdge(mapsTo.getRefinement().getUri(),  mapsTo.getIdentity(), local.getIdentity()),  vPort, localVo,  target));
                           
                            break;
                        case verifyIdentical:
//                            target.addEdge(remoteVo, vPort, new SBViewEdge<>(new SynBadEdge(mapsTo.getRefinement().getUri(), remote.getIdentity(), mapsTo.getIdentity()), remoteVo, vPort,  target));
//                            target.addEdge(vPort, localVo,  new SBViewEdge<>(new SynBadEdge(mapsTo.getRefinement().getUri(),  mapsTo.getIdentity(), local.getIdentity()),  vPort, localVo,  target));
                            break;
                    }
                });

                break;
            case REMOVED:
                target.findPortNodes(mapsTo).stream().forEach(p -> target.removePort(p, (SBViewComponent<SBIdentified>)target.getPortOwner(p)));
                break;   
        }
    }
    
    private void processInstance(DefaultEventType type, SBIdentified instance) {
        
        if(instance == null) 
        {
            logger.debug("Instance is null");
            return;
        }
        
        logger.debug("Processing non-Svp instance: [{}:{}]", instance.getClass().getSimpleName(), instance.getDisplayId());

        return;
        
    }
    
    // ========================================================================
    //                          Event methods
    // ========================================================================
    
     
    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> e) {

        logger.debug("Received event: E [ {} ]",  e.getData());
        URI fromIdentity = e.getData().getFrom();
        URI toIdentity = e.getData().getTo();
        
        // Nodes should already be in graph
        
        if(target.findObjects(toIdentity).isEmpty()) {
            logger.trace("Does not exist in target: N [ {} ] ", toIdentity.toASCIIString());
            return;
        } else if(target.findObjects(fromIdentity).isEmpty()) {
            logger.trace("Does not exist in target: N [ {} ] ", fromIdentity.toASCIIString());
            return;
        }
        
        if(!target.findObjects(fromIdentity).isEmpty() && !root.getIdentity().equals(fromIdentity) )
            return;
        
        SBIdentified from = ws.getObject(e.getData().getFrom(), SBIdentified.class, root.getContexts());

        ITree<Object> tree = getTreeFromNode(from);
        processTree((DefaultEventType)e.getType(), tree);

    }
    
    @Override
    public void onValueEvent(SBGraphEvent<SBValue> e) {
        logger.debug("Received event: V [ {} ] " + e.getClass());
    }
    
    // ========================================================================
    //                          SBOL helper methods
    // ========================================================================

    private Set<SBIdentified> getSbolOwnerOfInstance(SBIdentified instance) {

       SBTraversalResult<SBIdentified, SynBadEdge> result = getSourceGraph().getTraversal().v(instance)
            .e(SBDirection.IN, 
                    SynBadTerms2.SbolComponent.hasComponent,
                    SynBadTerms2.SbolModule.hasFunctionalComponent,
                    SynBadTerms2.SbolModule.hasInteraction,
                    SynBadTerms2.SbolModule.hasModule)
            .v(SBDirection.IN, SBIdentified.class)
            .getResult();

       Stream<SBIdentified> stream = result.streamVertexes();
       SBIdentified ownerDefinitions =  stream.findFirst().orElse(null);
 
        // if owner is root, add to root
       
       if(ownerDefinitions == null) {
           logger.warn("Owner definition could not be found");
           return Collections.EMPTY_SET;
       }
       
       if(ownerDefinitions.equals(root)) {
           logger.debug("Found ModuleDef owner [ {} ] of instance [ {} ]",ownerDefinitions, instance);
           return Collections.singleton(ownerDefinitions);
       }
       
       // otherwise, add to instances of definition in graph
       
       Set<SBIdentified> ownerInstances = getSourceGraph().getTraversal().v(ownerDefinitions)
            .e(SBDirection.IN, SynBadTerms2.Sbol.definedBy)
            .v(SBDirection.IN)
            .filter(i -> {
                //logger.debug("FILTERING: {}", i);
                boolean b = !target.findObjects(i.getIdentity()).isEmpty();;
                //logger.debug("Is in view? {}: [ {} ] ", b, i);
                return b;
            }).getResult().streamVertexes().map(v -> (SBIdentified) v).collect(Collectors.toSet());
       
       //logger.debug("Found {} owners of instance in view: [ {} ]", ownerInstances.size(), instance);
       
        return ownerInstances;
    }
    
    private SBIdentified getSbolDefinitionofInstanceWithSvpDefInView(SBIdentified instance) {

        
       SBIdentified def= (SBIdentified) getSourceGraph().getTraversal()
                        .v(instance)
                        .e(SBDirection.OUT, SynBadTerms2.Sbol.definedBy)
                        .v(SBDirection.OUT, SBIdentified.class).as("definition")
                        .getResult().streamVertexes().findFirst().orElse(null);
        logger.debug("Found SbolDef [ {} ] for instance [ {} ]", def, instance);
        return def; 
    }
    
    private SBIdentified getSvpDefOfInteractionInView(SBIdentified instance) {
        SBIdentified svpDef = getSourceGraph().getTraversal()
                        .v(instance)
                        .e(SBDirection.IN, SynBadTerms2.SbolModule.hasInteraction)
                        .v(SBDirection.IN, SBIdentified.class).as("interaction")
                        .e(SBDirection.IN, SynBadTerms2.SynBadEntity.extensionOf)
                        .v(SBDirection.IN, SBIdentified.class).as("definition")
                        .getResult().streamVertexes().map(v -> (SBIdentified) v).findFirst().orElse(null);
         logger.debug("Found SvpDef [ {} ] of interaction: [ {} ]", svpDef, instance);
         return svpDef;
    }
    
    private boolean isFunctionalComponentOfSvmTu(SBIdentified instance) {
        boolean b = (getSourceGraph().getTraversal()
                    .v(instance)
                    .e(SBDirection.OUT, SynBadTerms2.SynBadModule.instancedTuOf)
                    .v(SBDirection.OUT).getResult().stream().count() > 0);
        logger.debug("isFunctionalComponentTu? [ {} ]", b);
        return b;
    }
    
    boolean isInstanceNode(SBIdentified instance, SBIdentified def){
        boolean b =  def.getIdentity().equals(root.getIdentity()) ||
                SbolInstance.class.isAssignableFrom(instance.getClass()) ||
                Interaction.class.isAssignableFrom(instance.getClass()) ||
                FunctionalComponent.class.isAssignableFrom(instance.getClass());
        
        logger.debug("IsInstanceNode? [ {}:{} ]", b, instance);
        return b;        
    }
    
    public Collection<DefaultViewIdentified<SBIdentified>>  getOwnerNodes(SBIdentified instance) {
        
        logger.debug("getOwnerNodes() of: [ {} ]", instance);

        Set<SBIdentified> ownerDefinitons = getSbolOwnerOfInstance(instance);
        Collection<DefaultViewIdentified<SBIdentified>> ownerNodes = new HashSet<>();
        
        if(ownerDefinitons != null) {
            for(SBIdentified owner : ownerDefinitons) {
                Collection<DefaultViewIdentified<SBIdentified>> objs = target.findObjects(owner.getIdentity(), SBIdentified.class);
                ownerNodes.addAll(objs);
            }
        } else {
            logger.debug("No owners found for: [ {} ]", instance);
        }
        
        return ownerNodes;
    }

    
    
    class TempEdge {

        private final SBIdentified from;
        private final SBIdentified to;
        private final URI label;
        
        public TempEdge(SBIdentified from, SBIdentified to, URI label) {
            this.from = from;
            this.to = to;
            this.label = label;
        }
        
    }*/
}
