/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.action;

import java.net.URI;
import java.util.Arrays;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.workspace.actions.ASBActionBuilder;
import uk.ac.ncl.icos.synbad.api.actions.SBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Location;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.Model;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.sbol.object.Sequence;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceAnnotation;
import uk.ac.ncl.icos.synbad.sbol.object.SequenceConstraint;

/**
 *
 * @author owengilfellon
 */
public class SbolActionBuilder extends ASBActionBuilder implements  SBDomainBuilder, SBSbolBuilder {

    protected final DefaultSBDomainBuilder builder;

    public SbolActionBuilder(SBWorkspace workspaceId, URI[] contexts) {
        super(workspaceId, contexts);
        builder = new DefaultSBDomainBuilder(workspaceId, contexts);
    }
    
    @Override
    public int getSize() {
        return builder.getSize();
    }
    
    
    @Override
    public boolean isEmpty() {
        return builder.isEmpty();
    }

    @Override
    public SBAction build(String label) {
        return builder.build(label);
    }

    @Override
    public SBAction build() {
        return builder.build();
    }

    @Override
    public boolean isBuilt() {
        return builder.isBuilt();
    }

    // Top Level
    
    
    public SbolActionBuilder addAction(SBAction action) {
        if(!isBuilt())
            builder.addAction(action);
        return this;
    }
    
    @Override
    public SbolActionBuilder createComponentDefinition(URI identity, Type[] types, Role[] roles) {
        if(!isBuilt())
            builder.addAction(ComponentDefinition.createComponentDefinition(identity, Arrays.asList(types), Arrays.asList(roles), getWorkspace(), getContexts()));
        return this;
    }
    

    @Override
    public SbolActionBuilder createModuleDefinition( URI identity) {
        if(!isBuilt())
            builder.addAction(ModuleDefinition.createModuleDefinition(identity, getWorkspace(), getContexts()));
        return this;
    }
   
    
    // Components
    
    @Override
    public SbolActionBuilder createModule(URI identity, ModuleDefinition definition, ModuleDefinition parent) {
        if(!isBuilt())
            builder.addAction(Module.createModule(identity, definition, parent,  getContexts()));
        return this;       
    }
      
    @Override
    public SbolActionBuilder createComponent(URI identity, URI parent, URI definition, SbolAccess access) {
        if(!isBuilt())
            builder.addAction(Component.createComponent(identity, parent, definition, access, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createComponent(URI identity, ComponentDefinition parent, ComponentDefinition definition, SbolAccess access) {
        if(!isBuilt())
            builder.addAction(Component.createComponent(identity, parent, definition, access, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createFuncComponent(URI identity, ComponentDefinition definition, ModuleDefinition parent, SbolDirection direction, SbolAccess access) {
        if(!isBuilt())
            builder.addAction(FunctionalComponent.createFuncComponent(identity, definition, parent, access, direction, getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createInteraction(URI identity, SbolInteractionType[] types, ModuleDefinition owner, URI[] contexts) {
        if(!isBuilt())
            builder.addAction(Interaction.createInteraction(identity, Arrays.asList(types), owner, getContexts()));
        return this;
    }
    

    // Interactions
    
    @Override
    public SbolActionBuilder createParticipation(URI identity, URI interaction, URI functionalComponent, SbolInteractionRole role) {
        if(!isBuilt())
            builder.addAction(Participation.createParticipation(identity, interaction, functionalComponent, role, getWorkspace(), getContexts()));
        return this;
    }
    
    
    @Override
    public SbolActionBuilder addParticipation(URI interaction, URI participation) {
        if(!isBuilt())
            builder.addAction(Interaction.addParticipation(interaction, participation, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createRange(URI identity, int start, int end) {
        if(!isBuilt())
            builder.addAction(Location.createRange(identity, start, end, getWorkspace(), getContexts()));
        return this;
    }


    @Override
    public SbolActionBuilder createCut(URI identity, int at, URI parent) {
        if(!isBuilt())
            builder.addAction(Location.createCut(identity, at,  getWorkspace(), getContexts()));
        return this;
    }


    
      // ================================
    //            Commands
    // ================================

    @Override
   public SbolActionBuilder createMapsTo(URI identity, SbolInstance owner, MapsToRefinement refinement, SbolInstance remoteInstance,  SbolInstance localInstance) {
         if(!isBuilt())
            builder.addAction(MapsTo.createMapsTo(identity, owner, refinement, remoteInstance, localInstance, getContexts()));
        return this;
    }
   
    @Override
   public SbolActionBuilder createMapsTo(URI identity, URI owner,  URI ownerType, MapsToRefinement refinement, URI remoteInstance,  URI localInstance) {
         if(!isBuilt())
            builder.addAction(MapsTo.createMapsTo(identity, owner, ownerType, refinement, remoteInstance, localInstance, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createModel( URI identity, URI source, URI language, URI framework) {
       if(!isBuilt())
            builder.addAction(Model.createModel(identity, source, language, framework, ws, getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder addModelAction(URI modelIdentity, ModuleDefinition owner) {
        if(!isBuilt())
            builder.addAction(ModuleDefinition.addModel(modelIdentity, owner, modelIdentity, getContexts()));
        return this;
    }
     
    // ================================
    //             Actions
    // ================================
    
    @Override
    public SbolActionBuilder createSequence(URI identity, String elements, String encoding) {
        if(!isBuilt())
            builder.addAction(Sequence.createSequence(identity, elements, encoding, getWorkspace(), getContexts()));
        return this;
    }
    
        
    
    @Override
    public SbolActionBuilder createSequenceAnnotation(URI identity, ComponentDefinition owner, Component component, Location... locations) {
        if(!isBuilt())
            builder.addAction(SequenceAnnotation.createSequenceAnnotation(identity, owner, component, getContexts(), locations));
        return this;
    }
    
    
    @Override
    public SbolActionBuilder createSequenceConstraint(URI identity, ComponentDefinition owner, Component subject, ConstraintRestriction restriction, Component object) {
        if(!isBuilt())
            builder.addAction(SequenceConstraint.createSequenceConstraint(identity, owner, subject, restriction, object, getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createSequenceConstraint(URI identity, URI owner, URI subject, ConstraintRestriction restriction, URI object) {
        if(!isBuilt())
            builder.addAction(SequenceConstraint.createSequenceConstraint(identity, owner, subject, restriction, object, getWorkspace(), getContexts()));
        return this;
    }
    
    @Override
    public SbolActionBuilder createObject(URI identity, URI rdfType, URI ownerId, URI ownerType) {
        if(!isBuilt())
            builder.createObject(identity, rdfType, ownerId, ownerType);
        return this;
    }
    
    
    public SbolActionBuilder removeObject(URI identity, URI rdfType, URI ownerId, URI ownerType) {
        if(!isBuilt())
            builder.removeObject(identity, rdfType, ownerId, ownerType);
        return this;
    }
     
    public SbolActionBuilder removeObjectAndDependants(URI identity, URI ownerId) {
        if(!isBuilt())
            builder.removeObjectAndDependants(identity, ownerId);
        return this;
    }
    
    public SbolActionBuilder createEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.createEdge(source, predicate, target);
        return this;
    }
    
    public SbolActionBuilder removeEdge(URI source, URI predicate, URI target) {
        if(!isBuilt())
            builder.removeEdge(source, predicate, target);
        return this;
    }
    
    public SbolActionBuilder createValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.createValue(source, predicate, value,  sourceType);
        return this;
    }
    
    public SbolActionBuilder removeValue(URI source, URI predicate, SBValue value, URI sourceType) {
        if(!isBuilt())
            builder.removeValue(source, predicate, value, sourceType);
        return this;
    }

    @Override
    public SbolActionBuilder duplicateObject(URI identity, URI[] fromContexts, boolean incrementIds) {
        if(!isBuilt())
            builder.duplicateObject(identity, fromContexts, incrementIds);
        return this;
    }


    
}
