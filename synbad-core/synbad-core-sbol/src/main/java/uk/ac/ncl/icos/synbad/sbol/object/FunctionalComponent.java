/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = FunctionalComponent.TYPE, xmlParent = ModuleDefinition.class)
public class FunctionalComponent extends ComponentInstance implements ChildObject {
    
    public static final String TYPE = "http://sbols.org/v2#FunctionalComponent";

    public FunctionalComponent(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    public SbolDirection getDirection() {
        return getValues(SynBadTerms.SbolFunctionalComponent.hasDirection,
            URI.class).stream()
            .map(s -> {
                if(s.equals(SBIdentityHelper.getURI("http://sbolstandard.org/v2#out")))
                   return SbolDirection.OUT;
                else if (s.equals(SBIdentityHelper.getURI("http://sbolstandard.org/v2#in")))
                    return SbolDirection.IN;
                else if (s.equals(SBIdentityHelper.getURI("http://sbolstandard.org/v2#inout")))
                    return SbolDirection.INOUT;
                else return SbolDirection.NONE;})
            .findFirst().orElse(null);
    }
    
    @Override
    public ModuleDefinition getParent() {
        return getIncomingObject(SynBadTerms.SbolModule.hasFunctionalComponent, ModuleDefinition.class);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    public static SBAction createFuncComponent(URI identity, URI definition, URI parent, SbolAccess access, SbolDirection direction, SBWorkspace workspaceId, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspaceId, contexts)
            .createObject(identity, SBIdentityHelper.getURI(FunctionalComponent.TYPE), parent, SBIdentityHelper.getURI(ModuleDefinition.TYPE))
            .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.definedBy), definition)
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasAccess), SBValue.parseValue(access.getUri()), SBIdentityHelper.getURI(FunctionalComponent.TYPE))
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolFunctionalComponent.hasDirection), SBValue.parseValue(direction.getUri()), SBIdentityHelper.getURI(FunctionalComponent.TYPE))
            .createEdge(parent, SBIdentityHelper.getURI(SynBadTerms.SbolModule.hasFunctionalComponent), identity)
            .build("CreateFC ["+ identity.toASCIIString() + "]");
    
    }
    
    public static SBAction createFuncComponent(URI identity, ComponentDefinition definition, ModuleDefinition parent, SbolAccess access, SbolDirection direction, URI[] contexts) {
        return new DefaultSBDomainBuilder(definition.getWorkspace(), contexts)
            .createObject(identity, SBIdentityHelper.getURI(FunctionalComponent.TYPE), parent.getIdentity(), SBIdentityHelper.getURI(ModuleDefinition.TYPE))
            .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.Sbol.definedBy), definition.getIdentity())
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasAccess), SBValue.parseValue(access.getUri()), SBIdentityHelper.getURI(FunctionalComponent.TYPE))
            .createValue(identity, SBIdentityHelper.getURI(SynBadTerms.SbolFunctionalComponent.hasDirection), SBValue.parseValue(direction.getUri()), SBIdentityHelper.getURI(FunctionalComponent.TYPE))
            .createEdge(parent.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolModule.hasFunctionalComponent), identity)
            .build("CreateFC ["+ identity.toASCIIString() + "]");
    
    }
    
     public static SBIdentity getFunctionalComponentIdentity(SBIdentity parentModuleDefId, SBIdentity definitionComponentDefId) {
            return SBIdentity.getIdentity(parentModuleDefId.getUriPrefix(), parentModuleDefId.getDisplayID(), "fc_" + definitionComponentDefId.getDisplayID(), parentModuleDefId.getVersion());
    }
}
