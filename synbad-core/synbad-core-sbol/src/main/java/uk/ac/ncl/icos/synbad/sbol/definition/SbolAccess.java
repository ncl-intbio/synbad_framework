/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.definition;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;

/**
 *
 * @author owengilfellon
 */
public enum SbolAccess implements SBDataDef {
    
    PUBLIC(URI.create("http://sbols.org/v2#public")), 
    PRIVATE(URI.create("http://sbols.org/v2#private"));

    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private SbolAccess(URI uri) {
        this.uri = uri;
    }   

}
