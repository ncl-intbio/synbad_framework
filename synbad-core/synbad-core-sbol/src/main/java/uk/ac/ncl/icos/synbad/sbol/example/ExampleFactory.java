package uk.ac.ncl.icos.synbad.sbol.example;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author owengilfellon
 */
public class ExampleFactory {
    
    
    private final static Logger logger = LoggerFactory.getLogger(ExampleFactory.class);
    private static final String contexts = "http://www.synbad.org/subtilin_reporter_context";
    private static final URI[] contextsUris;
    static {     
        logger.debug("Parsing contexts");
        URI uri;
        try {
            uri = new URI(contexts);
        } catch (URISyntaxException ex) {
            uri = null;
           logger.error("Could not create context: [{}]", contexts);
        }
        contextsUris = new URI[] {uri};
    }
    
    public static ModuleDefinition getSubtilinReporter(SBWorkspace ws) {

            logger.info("Creating example model  - subtilin receiver");
            
            String PREFIX = "http://www.synbad.org";
            String VERSION = "1.0";
            
            logger.info("Creating identities...");
            
            // Module Defs
            
            ws.createContext(contextsUris[0]);
            
            SBIdentity md_receiverAndReporter = ws.getIdentityFactory().getIdentity(PREFIX, "md_ReceiverAndReporter", VERSION);
            SBIdentity md_subtilinReceiver = ws.getIdentityFactory().getIdentity(PREFIX, "md_SubtilinReceiver", VERSION);
            SBIdentity md_reporter = ws.getIdentityFactory().getIdentity(PREFIX, "md_Reporter", VERSION);
            
            // Composite DNA
            
            SBIdentity cd_subtilinReceiverDna = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SubtilinReceiver_DNA", VERSION);
            SBIdentity cd_reporterDna = ws.getIdentityFactory().getIdentity(PREFIX, "cd_Reporter_DNA", VERSION);
            
            // Composite DNA instances
            
            SBIdentity subtilinReceiverDna_Id = ws.getIdentityFactory().getIdentity(PREFIX,md_subtilinReceiver.getDisplayID(), "fc_subtilinReceiver_DNA", VERSION);
            SBIdentity reporterDna_Id = ws.getIdentityFactory().getIdentity(PREFIX,md_reporter.getDisplayID(), "fc_reporter_DNA", VERSION);
            
            // Species definitions
            
            SBIdentity cd_subtilin = ws.getIdentityFactory().getIdentity(PREFIX, "cd_Subtilin", VERSION);
            SBIdentity cd_spak_protein = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SpaK_protein", VERSION);
            SBIdentity cd_spar_protein = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SpaR_protein", VERSION);
            SBIdentity cd_spak_p_protein = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SpaK_P_protein", VERSION);
            SBIdentity cd_spar_p_protein = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SpaR_P_protein", VERSION);
            SBIdentity cd_gfp_protein = ws.getIdentityFactory().getIdentity(PREFIX, "cd_GFP_protein", VERSION);
            
            // DNA definitions
            
            SBIdentity cd_pspark = ws.getIdentityFactory().getIdentity(PREFIX, "cd_PSpaRK", VERSION);
            SBIdentity cd_rbs_spak = ws.getIdentityFactory().getIdentity(PREFIX, "cd_RBS_SpaK", VERSION);
            SBIdentity cd_spak = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SpaK", VERSION);
            SBIdentity cd_rbs_spar = ws.getIdentityFactory().getIdentity(PREFIX, "cd_RBS_SpaR", VERSION);
            SBIdentity cd_spar = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SpaR", VERSION);
            SBIdentity cd_ter1 = ws.getIdentityFactory().getIdentity(PREFIX, "cd_SubtilinReceiverTerminator", VERSION);
            SBIdentity cd_pspas = ws.getIdentityFactory().getIdentity(PREFIX, "cd_PspaS", VERSION);
            
            SBIdentity cd_promoter = ws.getIdentityFactory().getIdentity(PREFIX, "cd_Promoter", VERSION);
            
            SBIdentity cd_rbs_spas = ws.getIdentityFactory().getIdentity(PREFIX, "cd_RBS_SpaS", VERSION);
            SBIdentity cd_gfp = ws.getIdentityFactory().getIdentity(PREFIX, "cd_GFP_rrnb", VERSION);
            SBIdentity cd_ter2 = ws.getIdentityFactory().getIdentity(PREFIX, "cd_ReporterTerminator", VERSION);
            
            // Module instance definitions
            
            SBIdentity m_subtilinReceiver = ws.getIdentityFactory().getIdentity(PREFIX, md_receiverAndReporter.getDisplayID(), "m_SubtilinReceiver", VERSION);
            SBIdentity m_reporter = ws.getIdentityFactory().getIdentity(PREFIX, md_receiverAndReporter.getDisplayID(), "m_Reporter", VERSION);
            
            // Component instance definitions
            
            SBIdentity c_pspark = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_PSpaRK", VERSION);
            SBIdentity c_rbs_spak = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_RBS_SpaK", VERSION);
            SBIdentity c_spak = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_SpaK", VERSION);
            SBIdentity c_rbs_spar = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_RBS_SpaR", VERSION);
            SBIdentity c_spar = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_SpaR", VERSION);
            SBIdentity c_ter1 = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_SubtilinReceiverTerminator", VERSION);
            SBIdentity c_pspas = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "c_PspaS", VERSION);
            SBIdentity c_rbs_spas = ws.getIdentityFactory().getIdentity(PREFIX, cd_reporterDna.getDisplayID(), "c_RBS_SpaS", VERSION);
            SBIdentity c_gfp = ws.getIdentityFactory().getIdentity(PREFIX, cd_reporterDna.getDisplayID(), "c_GFP_rrnb", VERSION);
            SBIdentity c_ter2 = ws.getIdentityFactory().getIdentity(PREFIX, cd_reporterDna.getDisplayID(), "c_ReporterTerminator", VERSION);
            
            // Sequence constraints
            
            SBIdentity pspark_precedes_rbs_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "pspark_precedes_rbs", VERSION);
            SBIdentity rbs_precedes_spak_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "rbs_precedes_spak", VERSION);
            SBIdentity spak_precedes_rbs_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "spak_precedes_rbs", VERSION);
            SBIdentity rbs_precedes_spar_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "rbs_precedes_spar", VERSION);
            SBIdentity spar_precedes_ter_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "spar_precedes_ter", VERSION);
            SBIdentity ter_precedes_pspas_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_subtilinReceiverDna.getDisplayID(), "ter_precedes_pspas", VERSION);
            SBIdentity rbs_precedes_gfp_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_reporterDna.getDisplayID(), "rbs_precedes_gfp", VERSION);
            SBIdentity gfp_precedes_ter_id = ws.getIdentityFactory().getIdentity(PREFIX, cd_reporterDna.getDisplayID(), "gfp_precedes_ter", VERSION);
            
            // Functional components
            
            SBIdentity fc_subtilin_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_subtilin", VERSION);
            SBIdentity fc_spak_dna_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_spak_dna", VERSION);
            SBIdentity fc_spak_protein_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_spak_protein", VERSION);
            SBIdentity fc_spak_p_protein_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_spak_p_protein", VERSION);
            SBIdentity fc_spar_dna_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_spar_dna", VERSION);
            SBIdentity fc_spar_protein_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_spar_protein", VERSION);
            SBIdentity fc_spar_p_protein_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_spar_p_protein", VERSION);
            SBIdentity fc_pspas_id = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "fc_pspas", VERSION);
            SBIdentity fc_rr_pspas_id = ws.getIdentityFactory().getIdentity(PREFIX, md_receiverAndReporter.getDisplayID(), "fc_pspas", VERSION);
            SBIdentity fc_r_promoter_id = ws.getIdentityFactory().getIdentity(PREFIX, md_reporter.getDisplayID(), "fc_promoter", VERSION);
            SBIdentity fc_gfp_dna_id = ws.getIdentityFactory().getIdentity(PREFIX, md_reporter.getDisplayID(), "fc_GFP_dna", VERSION);
            SBIdentity fc_gfp_protein_id = ws.getIdentityFactory().getIdentity(PREFIX, md_reporter.getDisplayID(), "fc_GFP_protein", VERSION);
            
            // Participations
            
            SBIdentity spak_production_participants = ws.getIdentityFactory().getIdentity(PREFIX, "i_spak_production_participants", VERSION);
            SBIdentity spak_degradation_participants = ws.getIdentityFactory().getIdentity(PREFIX, "i_spak_degradation_participants", VERSION);
            SBIdentity spak_p_degradation_participants = ws.getIdentityFactory().getIdentity(PREFIX, "i_spak_p_degradation_participants", VERSION);
            SBIdentity spak_p_dephos_participants = ws.getIdentityFactory().getIdentity(PREFIX, "i_spak_p_dephosphorylation_participants", VERSION);
            
            SBIdentity spar_production_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_spar_production_participants", VERSION);
            SBIdentity spar_degradation_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_spar_degradation_participants", VERSION);
            SBIdentity spar_p_degradation_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_spar_p_degradation_participants", VERSION);
            SBIdentity spar_p_dephos_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_spar_p_dephosphorylation_participants", VERSION);
            
            SBIdentity gfp_production_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_GFP_production_participants", VERSION);
            SBIdentity subtilin_phos_spak_participants = ws.getIdentityFactory().getIdentity(PREFIX, "i_subtilin_phos_spak_participants", VERSION);
            SBIdentity i_spak_p_phos_spar_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_spak_p_phos_spar_participants", VERSION);
            SBIdentity i_spar_p_act_pspas_participants= ws.getIdentityFactory().getIdentity(PREFIX, "i_spar_p_act_pspas_participants", VERSION);
            
            // Interactions
            
            SBIdentity i_spak_production = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spak_production", VERSION);
            SBIdentity i_spak_degradation = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spak_degradation", VERSION);
            
            SBIdentity i_spak_p_phos_spar = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spak_p_phos_spar", VERSION);
            SBIdentity i_spak_p_degradation = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spak_p_degradation", VERSION);
            SBIdentity i_spak_p_dephos = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spak_p_production", VERSION);
            
            
            SBIdentity i_spar_production = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spar_production", VERSION);
            SBIdentity i_spar_degradation = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spar_degradation", VERSION);
            SBIdentity i_spar_p_degradation = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spar_p_degradation", VERSION);
            SBIdentity i_spar_p_dephos = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spar_p_production", VERSION);
            
            
            
            
            
            SBIdentity i_gfp_production = ws.getIdentityFactory().getIdentity(PREFIX, md_reporter.getDisplayID(), "i_GFP_production", VERSION);
            SBIdentity i_subtilin_phos_spak = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_subtilin_phos_spak_production", VERSION);
            
            SBIdentity i_spar_p_act_pspas = ws.getIdentityFactory().getIdentity(PREFIX, md_subtilinReceiver.getDisplayID(), "i_spar_p_act_pspas", VERSION);
            

            
            // Create Definitions
            
            logger.info("Constructing objects...");
            
            
            SBAction createDefinitions = new SbolActionBuilder(ws, contextsUris)
                    .createModuleDefinition(md_receiverAndReporter.getIdentity())
                    .createModuleDefinition(md_subtilinReceiver.getIdentity())
                    .createModuleDefinition(md_reporter.getIdentity())
                    .createComponentDefinition(cd_subtilin.getIdentity(), new Type[]{ ComponentType.SmallMolecule }, new Role[]{  })
                    .createComponentDefinition(cd_subtilinReceiverDna.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ })
                    .createComponentDefinition(cd_reporterDna.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Generic})
                    .createComponentDefinition(cd_pspark.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Promoter})
                    .createComponentDefinition(cd_rbs_spak.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.RBS})
                    .createComponentDefinition(cd_spak.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.CDS})
                    .createComponentDefinition(cd_spak_protein.getIdentity(), new Type[]{  ComponentType.Protein }, new Role[]{ })
                    .createComponentDefinition(cd_spak_p_protein.getIdentity(), new Type[]{  ComponentType.Protein }, new Role[]{})
                    
                    .createComponentDefinition(cd_rbs_spar.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.RBS})
                    .createComponentDefinition(cd_spar.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.CDS})
                    .createComponentDefinition(cd_spar_protein.getIdentity(), new Type[]{  ComponentType.Protein }, new Role[]{ })
                    .createComponentDefinition(cd_spar_p_protein.getIdentity(), new Type[]{  ComponentType.Protein }, new Role[]{ })
                    
                    .createComponentDefinition(cd_ter1.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Terminator})
                    .createComponentDefinition(cd_pspas.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Promoter})
                    .createComponentDefinition(cd_promoter.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Promoter})
                    .createComponentDefinition(cd_rbs_spas.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.RBS})
                    .createComponentDefinition(cd_gfp.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.CDS})
                    .createComponentDefinition(cd_gfp_protein.getIdentity(), new Type[]{  ComponentType.Protein }, new Role[]{ })
                    .createComponentDefinition(cd_ter2.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Terminator})
                    .build("CreateSubtilinReporter ["+ md_receiverAndReporter.getDisplayID() + "]");
            
            ws.perform(createDefinitions);
            
            // Instance Identities
            
            logger.info("Creating modules...");
            
            // Instantiate modules
            
            ModuleDefinition root = ws.getObject(md_receiverAndReporter.getIdentity(), ModuleDefinition.class, contextsUris);
            Module subtilinReceiver = root.createModule(m_subtilinReceiver, ws.getObject(md_subtilinReceiver.getIdentity(), ModuleDefinition.class, contextsUris));
            Module reporter = root.createModule(m_reporter, ws.getObject(md_reporter.getIdentity(), ModuleDefinition.class, contextsUris));
            
            // Instantiate DNA 1
            
            logger.info("Creating components...");
            
            ComponentDefinition subtilinReceiverDna = ws.getObject(cd_subtilinReceiverDna.getIdentity(), ComponentDefinition.class, contextsUris);
            Component pspark = subtilinReceiverDna.createComponent(c_pspark, ws.getObject(cd_pspark.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component rbs_spak = subtilinReceiverDna.createComponent(c_rbs_spak, ws.getObject(cd_rbs_spak.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component spak = subtilinReceiverDna.createComponent(c_spak, ws.getObject(cd_spak.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component rbs_spar = subtilinReceiverDna.createComponent(c_rbs_spar, ws.getObject(cd_rbs_spar.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component spar = subtilinReceiverDna.createComponent(c_spar, ws.getObject(cd_spar.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component ter1 = subtilinReceiverDna.createComponent(c_ter1, ws.getObject(cd_ter1.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component pspas = subtilinReceiverDna.createComponent(c_pspas, ws.getObject(cd_pspas.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            
            ComponentDefinition reporterDna = ws.getObject(cd_reporterDna.getIdentity(), ComponentDefinition.class, contextsUris);
            Component rbs_spas = reporterDna.createComponent(c_rbs_spas, ws.getObject(cd_rbs_spas.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component gfp = reporterDna.createComponent(c_gfp, ws.getObject(cd_gfp.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component ter2 = reporterDna.createComponent(c_ter2, ws.getObject(cd_ter2.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            
            ComponentDefinition promoter = ws.getObject(cd_promoter.getIdentity(), ComponentDefinition.class, contextsUris);
            
            
            logger.info("Creating sequence constraints...");
            
            subtilinReceiverDna.createSequenceConstraint(pspark_precedes_rbs_id, pspark, ConstraintRestriction.PRECEDES, rbs_spak);
            subtilinReceiverDna.createSequenceConstraint(rbs_precedes_spak_id, rbs_spak, ConstraintRestriction.PRECEDES, spak);
            subtilinReceiverDna.createSequenceConstraint(spak_precedes_rbs_id, spak, ConstraintRestriction.PRECEDES, rbs_spar);
            subtilinReceiverDna.createSequenceConstraint(rbs_precedes_spar_id, rbs_spar, ConstraintRestriction.PRECEDES, spar);
            subtilinReceiverDna.createSequenceConstraint(spar_precedes_ter_id, spar, ConstraintRestriction.PRECEDES, ter1);
            subtilinReceiverDna.createSequenceConstraint(ter_precedes_pspas_id, ter1, ConstraintRestriction.PRECEDES, pspas);
            
            // Instantiate DNA 2
            
            reporterDna.createSequenceConstraint(rbs_precedes_gfp_id, rbs_spas, ConstraintRestriction.PRECEDES, gfp);
            reporterDna.createSequenceConstraint(gfp_precedes_ter_id, gfp, ConstraintRestriction.PRECEDES, ter2);
            
            // Instantiate Interactions
            
            
            ModuleDefinition subtilinReceiverDef = subtilinReceiver.getDefinition().get();
            ModuleDefinition reporterDef = reporter.getDefinition().get();
            
            logger.info("Creating functional components...");
            
            FunctionalComponent fc_subtilinReceiver_Dna = subtilinReceiverDef.createFunctionalComponent(subtilinReceiverDna_Id, subtilinReceiverDna,SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_reporter_Dna = reporterDef.createFunctionalComponent(reporterDna_Id, reporterDna,SbolDirection.INOUT, SbolAccess.PUBLIC);
 
            FunctionalComponent fc_subtilin = subtilinReceiverDef.createFunctionalComponent(fc_subtilin_id, ws.getObject(cd_subtilin.getIdentity(), ComponentDefinition.class, contextsUris),SbolDirection.INOUT, SbolAccess.PUBLIC);
            
            FunctionalComponent fc_spak_dna = subtilinReceiverDef.createFunctionalComponent(fc_spak_dna_id, spak.getDefinition().get(),SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_spak_protein = subtilinReceiverDef.createFunctionalComponent(fc_spak_protein_id, ws.getObject(cd_spak_protein.getIdentity(), ComponentDefinition.class, contextsUris),SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_spak_p_protein = subtilinReceiverDef.createFunctionalComponent(fc_spak_p_protein_id, ws.getObject(cd_spak_p_protein.getIdentity(), ComponentDefinition.class, contextsUris),SbolDirection.INOUT, SbolAccess.PUBLIC);
           
            FunctionalComponent fc_spar_dna = subtilinReceiverDef.createFunctionalComponent(fc_spar_dna_id, spar.getDefinition().get(),SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_spar_protein = subtilinReceiverDef.createFunctionalComponent(fc_spar_protein_id, ws.getObject(cd_spar_protein.getIdentity(), ComponentDefinition.class, contextsUris),SbolDirection.INOUT,  SbolAccess.PUBLIC);
            FunctionalComponent fc_spar_p_protein = subtilinReceiverDef.createFunctionalComponent(fc_spar_p_protein_id, ws.getObject(cd_spar_p_protein.getIdentity(), ComponentDefinition.class, contextsUris),SbolDirection.INOUT, SbolAccess.PUBLIC);
            
            FunctionalComponent fc_pspas_dna = subtilinReceiverDef.createFunctionalComponent(fc_pspas_id, pspas.getDefinition().get(),SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_rr_pspas_dna = root.createFunctionalComponent(fc_rr_pspas_id, pspas.getDefinition().get(),SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_promoter_dna = reporterDef.createFunctionalComponent(fc_r_promoter_id, promoter,SbolDirection.INOUT, SbolAccess.PUBLIC);
            
            
            FunctionalComponent fc_gfp_dna = reporterDef.createFunctionalComponent(fc_gfp_dna_id, gfp.getDefinition().get(),SbolDirection.INOUT, SbolAccess.PUBLIC);
            FunctionalComponent fc_gfp_protein = reporterDef.createFunctionalComponent(fc_gfp_protein_id, ws.getObject(cd_gfp_protein.getIdentity(), ComponentDefinition.class, contextsUris),SbolDirection.INOUT, SbolAccess.PUBLIC);
            
            
                        // Maps to
            
            SBIdentity mt_spakToSpak = ws.getIdentityFactory().getIdentity(SBIdentityHelper.getURI(fc_subtilinReceiver_Dna.getPersistentId() + "/spak_c_maps_to_spak_fc/" + VERSION));
            SBIdentity mt_sparToSpar = ws.getIdentityFactory().getIdentity(SBIdentityHelper.getURI(fc_subtilinReceiver_Dna.getPersistentId() + "/spar_c_maps_to_spar_fc/" + VERSION));
            SBIdentity mt_r_pspasToPromoter = ws.getIdentityFactory().getIdentity(SBIdentityHelper.getURI(reporter.getPersistentId() + "/r_pspas_fc_maps_to_promoter_fc/" + VERSION));
            SBIdentity mt_pspasTorPspas = ws.getIdentityFactory().getIdentity(SBIdentityHelper.getURI(subtilinReceiver.getPersistentId() + "/pspas_fc_maps_to_r_pspas_fc/" + VERSION));
            SBIdentity mt_gfpToGfp = ws.getIdentityFactory().getIdentity(SBIdentityHelper.getURI(fc_reporter_Dna.getPersistentId() + "/gfp_c_maps_to_gfp_fc/" + VERSION));
            
            
            //
            logger.info("Creating interactions...");
            
            Interaction subtilin_phos_spak = subtilinReceiverDef.createInteraction(i_subtilin_phos_spak, Arrays.asList(SbolInteractionType.CONTROL, SbolInteractionType.BIOCHEMICAL_REACTION), Arrays.asList(fc_subtilin, fc_spak_protein, fc_spak_p_protein), Arrays.asList(SbolInteractionRole.MODIFIER, SbolInteractionRole.MODIFIED, SbolInteractionRole.PRODUCT));
            Interaction spak_phos_spar = subtilinReceiverDef.createInteraction(i_spak_p_phos_spar, Arrays.asList(SbolInteractionType.CONTROL, SbolInteractionType.BIOCHEMICAL_REACTION), Arrays.asList(fc_spak_p_protein, fc_spar_protein, fc_spar_p_protein), Arrays.asList(SbolInteractionRole.MODIFIER, SbolInteractionRole.MODIFIED, SbolInteractionRole.PRODUCT));
   
            Interaction spakProduction = subtilinReceiverDef.createInteraction(i_spak_production, Arrays.asList(SbolInteractionType.CONTROL, SbolInteractionType.BIOCHEMICAL_REACTION), Arrays.asList(fc_spak_dna, fc_spak_protein), Arrays.asList(SbolInteractionRole.TEMPLATE, SbolInteractionRole.PRODUCT));
            Interaction spakDegradation = subtilinReceiverDef.createInteraction(i_spak_degradation, Arrays.asList(SbolInteractionType.DEGRADATION), Arrays.asList(fc_spak_protein), Arrays.asList(SbolInteractionRole.REACTANT));
            Interaction spakPDegradation = subtilinReceiverDef.createInteraction(i_spak_p_degradation, Arrays.asList(SbolInteractionType.DEGRADATION),Arrays.asList(fc_spak_p_protein), Arrays.asList(SbolInteractionRole.REACTANT));
            Interaction spakPDephos = subtilinReceiverDef.createInteraction(i_spak_p_dephos, Arrays.asList(SbolInteractionType.CONTROL, SbolInteractionType.BIOCHEMICAL_REACTION), Arrays.asList(fc_spak_p_protein, fc_spak_protein), Arrays.asList(SbolInteractionRole.REACTANT, SbolInteractionRole.PRODUCT));
            
            Interaction sparProduction = subtilinReceiverDef.createInteraction(i_spar_production, Arrays.asList(SbolInteractionType.GENETIC_PRODUCTION),Arrays.asList(fc_spar_dna, fc_spar_protein), Arrays.asList(SbolInteractionRole.TEMPLATE, SbolInteractionRole.PRODUCT));
            Interaction sparDegradation = subtilinReceiverDef.createInteraction(i_spar_degradation, Arrays.asList(SbolInteractionType.DEGRADATION),Arrays.asList(fc_spar_protein), Arrays.asList(SbolInteractionRole.REACTANT));
            Interaction sparPDegradation = subtilinReceiverDef.createInteraction(i_spar_p_degradation, Arrays.asList(SbolInteractionType.DEGRADATION),Arrays.asList(fc_spar_p_protein), Arrays.asList(SbolInteractionRole.REACTANT));
            Interaction sparPDephos = subtilinReceiverDef.createInteraction(i_spar_p_dephos, Arrays.asList(SbolInteractionType.CONTROL, SbolInteractionType.BIOCHEMICAL_REACTION), Arrays.asList(fc_spar_p_protein, fc_spar_protein), Arrays.asList(SbolInteractionRole.REACTANT, SbolInteractionRole.PRODUCT));
            
            
            Interaction spar_act_pspas = subtilinReceiverDef.createInteraction(i_spar_p_act_pspas, Arrays.asList(SbolInteractionType.STIMULATION, SbolInteractionType.CONTROL),Arrays.asList(fc_spar_protein, fc_pspas_dna), Arrays.asList(SbolInteractionRole.STIMULATOR, SbolInteractionRole.PROMOTER));
            
            Interaction gfpProduction = reporterDef.createInteraction(i_gfp_production, Arrays.asList(SbolInteractionType.GENETIC_PRODUCTION),Arrays.asList(fc_gfp_dna, fc_gfp_protein), Arrays.asList(SbolInteractionRole.TEMPLATE, SbolInteractionRole.PRODUCT));

            // Instantiate Maps To
            
            logger.info("Creating mapsTo...");

            fc_subtilinReceiver_Dna.createMapsTo(mt_spakToSpak, MapsToRefinement.verifyIdentical, spak, fc_spak_dna);
            fc_subtilinReceiver_Dna.createMapsTo(mt_sparToSpar, MapsToRefinement.verifyIdentical, spar, fc_spar_dna);
            fc_reporter_Dna.createMapsTo(mt_gfpToGfp, MapsToRefinement.verifyIdentical, gfp, fc_gfp_dna);
            reporter.createMapsTo(mt_r_pspasToPromoter, MapsToRefinement.useLocal,  fc_promoter_dna, fc_rr_pspas_dna);
            subtilinReceiver.createMapsTo(mt_pspasTorPspas, MapsToRefinement.verifyIdentical,  fc_pspas_dna, fc_rr_pspas_dna);
            
            logger.info("Returning example model...");
            
            return root;

    }
    
    
    public static ModuleDefinition getSimpleModel(){

            String PREFIX = "http://www.synbad.org";
            String VERSION = "1.0";
            String workspaceUri = PREFIX + "/simpleModelWorkspace";
            SBWorkspace ws = new DefaultSBWorkspace(URI.create(workspaceUri));
            ws.createContext(contextsUris[0]);
            logger.info("Creating example model - simple model...");
            
            // Module Defs
            
            SBIdentity md_root = ws.getIdentityFactory().getIdentity(PREFIX, "md_Root", VERSION);
            SBIdentity md_subRoot = ws.getIdentityFactory().getIdentity(PREFIX,  "md_subRoot", VERSION);
            SBIdentity cd_gfp_parent = ws.getIdentityFactory().getIdentity(PREFIX, "cd_GFP_Parent", VERSION);
            SBIdentity cd_gfp = ws.getIdentityFactory().getIdentity(PREFIX, "cd_GFP_rrnb", VERSION);
            
            SBIdentity m_subRoot = ws.getIdentityFactory().getIdentity(PREFIX, md_root.getDisplayID(), "m_subRoot", VERSION);
            SBIdentity fc_gfp_parent = ws.getIdentityFactory().getIdentity(PREFIX, md_subRoot.getDisplayID(),  "fc_gfp_parent", VERSION);
            SBIdentity c_gfp1 = ws.getIdentityFactory().getIdentity(PREFIX ,  cd_gfp_parent.getDisplayID(),  "c_GFP_rrnb1", VERSION);
            SBIdentity c_gfp2 = ws.getIdentityFactory().getIdentity(PREFIX,   cd_gfp_parent.getDisplayID(),  "c_GFP_rrnb2", VERSION);
            SBIdentity gfp1_precedes_gfp2 = ws.getIdentityFactory().getIdentity(PREFIX, "gfp1_precedes_gfp2", VERSION);
            
            SBAction createDefinitions = new SbolActionBuilder(ws, contextsUris)
                    .createModuleDefinition(md_root.getIdentity())
                    .createModuleDefinition(md_subRoot.getIdentity())
                    .createComponentDefinition( cd_gfp.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.Generic})
                    .createComponentDefinition( cd_gfp_parent.getIdentity(), new Type[]{ ComponentType.DNA}, new Role[]{ ComponentRole.CDS})
                    .build("CreateSimpleModel ["+ md_root.getDisplayID() + "]");
            
            ws.perform(createDefinitions);
            
            // Instantiate modules
            
            ModuleDefinition root = ws.getObject(md_root.getIdentity(), ModuleDefinition.class, contextsUris);
            ModuleDefinition sub_root = ws.getObject(md_subRoot.getIdentity(), ModuleDefinition.class, contextsUris);
            root.createModule(m_subRoot, sub_root);
            
            ComponentDefinition gfpParent = ws.getObject(cd_gfp_parent.getIdentity(), ComponentDefinition.class, contextsUris);
            
            Component gfp_1 = gfpParent.createComponent(c_gfp1, ws.getObject(cd_gfp.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            Component gfp_2 = gfpParent.createComponent(c_gfp2, ws.getObject(cd_gfp.getIdentity(), ComponentDefinition.class, contextsUris), SbolAccess.PUBLIC);
            
            gfpParent.createSequenceConstraint(gfp1_precedes_gfp2, gfp_1, ConstraintRestriction.PRECEDES, gfp_2);
            
            sub_root.createFunctionalComponent(fc_gfp_parent, gfpParent,SbolDirection.INOUT, SbolAccess.PUBLIC);
            
            logger.info("Returning example model...");
            
            return root;

    }
    
}
