/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.io;

import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdfxml.xmlinput.JenaReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.io.AJenaImporter;

/**
 * Writes workspaces to Sbol in XML format, using libSBOLj
 * @author Owen
 */
public class SBSbolImporter extends AJenaImporter {
    
    private final Logger logger = LoggerFactory.getLogger(SBSbolImporter.class);
    
    @Override
    public void importWs(InputStream os, SBWorkspace workspace, URI[] contexts) {
       
        logger.debug("Exporting RDF for contexts...");
        Arrays.asList(contexts).stream().map(u -> u.toASCIIString()).forEach(s -> logger.debug("\t...{}", s));

        for(URI uri : contexts) {
            Model importedModel = ModelFactory.createDefaultModel();
            new JenaReader().read(importedModel, os, uri.toASCIIString());
            List<SBStatement> statements = getStatementsFromModel(null, null, null, importedModel);
            workspace.getRdfService().addStatements(statements, uri);
        }
    }
}
