/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.sbol.model.SbolModel;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewEdge;
import uk.ac.ncl.icos.synbad.view.DefaultSBHView;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewObject;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class SbolHView extends DefaultSBHView<SbolViewObject, SbolViewEdge>{

    private final SbolModel model;
    private static final Logger logger = LoggerFactory.getLogger(SbolHView.class);
    
    public SbolHView(SBIdentified obj, SbolModel model) {
        super(obj);
        this.model = model;
        addUpdater(new SbolHViewUpdater(this, getRoot(), model.getWorkspace()));
    }

    @Override
    public SbolModel getModel() {
        return model;
    }
    
    @Override
    protected SbolViewObject createViewObject(SbolViewObject object) {

        SbolViewObject obj;
        
        if(graph != null && graph.containsNode(object)) {
            obj = object;
            logger.debug("createViewObject() - returning existing object: [ {} ]", obj);
        } else {
            obj = new SbolViewObject(object.getObject(), object.getId(), this);
            logger.debug("createViewObject() - returning new object: [ {} ]", obj);
        }
        
        return obj;
    }

    @Override
    protected SbolViewEdge createViewEdge(SbolViewEdge edge) {
        
        if(graph != null && graph.containsEdge(edge)) {
            logger.debug("createViewEdge() - returning existing edge: [ {} ]", edge);
            return edge;   
        }
        
        SbolViewObject from = ( SbolViewObject)edge.getFrom();
        SbolViewObject to = ( SbolViewObject)edge.getTo();
        
        if(graph != null)
            from = (SbolViewObject)createViewObject(from);
        
        if(graph != null)
            to = (SbolViewObject)createViewObject(to);
        
        SbolViewEdge viewEdge = new SbolViewEdge(edge.getEdge(), from, to, edge.getId(), this);
        
        logger.debug("createViewEdge() - returning new edge: [ {} ]", viewEdge);
        return viewEdge;
    }
   
    @Override
    protected <V extends SBValued> SbolViewObject createViewObject(V object) {
        
        logger.debug("createViewObject() - Returning new object for [ {} ]", object);
        
        if(SBIdentified.class.isAssignableFrom(object.getClass()))
            return new SbolViewObject((SBIdentified)object, this);
        
        return null;
    }

    @Override
    protected <E extends SBEdge> SbolViewEdge createViewEdge(E edge, SbolViewObject from, SbolViewObject to) {
        
        if(!SynBadEdge.class.isAssignableFrom(edge.getClass()))
            return null;
        
        SbolViewObject fromToUse = (SbolViewObject)from;
        SbolViewObject toToUse = (SbolViewObject)to;
        
        if(graph != null )
            fromToUse = (SbolViewObject)createViewObject(fromToUse);
        
        if(graph != null)
            toToUse = (SbolViewObject)createViewObject(toToUse);
        
        SbolViewEdge viewEdge =  new SbolViewEdge((SynBadEdge)edge, fromToUse, toToUse, this);
        logger.debug("createViewEdge() - returning new edge for [ {} ]", edge);
        
        return viewEdge;
    }

}