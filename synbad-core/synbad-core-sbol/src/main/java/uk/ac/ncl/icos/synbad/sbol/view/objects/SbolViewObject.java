/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.view.objects;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;

/**
 *
 * @author owengilfellon
 */
public class SbolViewObject extends DefaultSBViewIdentified<SBIdentified>  {
  
    public SbolViewObject(SBIdentified obj, long id, SBView view) {
        super(obj, id, view);
    }

    public SbolViewObject(SBIdentified obj, SBView view) {
        super(obj, view);
    }

}
