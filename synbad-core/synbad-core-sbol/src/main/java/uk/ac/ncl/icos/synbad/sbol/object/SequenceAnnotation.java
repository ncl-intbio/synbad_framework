/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = SequenceAnnotation.TYPE, xmlParent = ComponentDefinition.class)
public class SequenceAnnotation extends ASBIdentified implements ChildObject {
    
    public static final String TYPE = "http://sbols.org/v2#SequenceAnnotation";

    public SequenceAnnotation(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }
    
    @OwnedEdge(id = SynBadTerms.SbolSequenceAnnotation.hasLocation, clazz = Location.class)
    public Location getLocation() {
         return getOutgoingObject(SynBadTerms.SbolSequenceAnnotation.hasLocation, Location.class);
    }
    
    public Component getComponent() {
        return getOutgoingObject(SynBadTerms.SbolSequenceAnnotation.positionsComponent, Component.class);
    }
    
    @Override
    public ComponentDefinition getParent() {
        return getOutgoingObject(SynBadTerms.SbolComponent.hasSequenceAnnotation, ComponentDefinition.class);
    }
    
    public static SBAction createSequenceAnnotation(URI identity, ComponentDefinition owner, Component component, URI[] contexts, Location... locations) {

         DefaultSBDomainBuilder ab = new DefaultSBDomainBuilder(owner.getWorkspace(), contexts)
                .createObject(identity, SBIdentityHelper.getURI(SequenceAnnotation.TYPE), owner.getIdentity(), SBIdentityHelper.getURI(ComponentDefinition.TYPE))
                .createEdge(owner.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasSequenceAnnotation), identity)
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceAnnotation.positionsComponent), component.getIdentity())
                .addAction(new CreateEdge(identity, SynBadTerms.SynBadSystem.dependantOn, component.getIdentity(), owner.getWorkspace().getIdentity(), owner.getWorkspace().getSystemContextIds(contexts)));

        for(Location location : locations) {
            ab.createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceAnnotation.hasLocation), location.getIdentity()); 
            
        }
        
        return ab.build("CreateSeqAnn ["+ identity.toASCIIString() + "]");
    }
}
