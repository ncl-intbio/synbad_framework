/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionType;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.OwnedEdge;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

/**
 * ModuleDefinition represents a grouping of functional elements in an
 * engineered system.
 * @author owengilfellon
 */
@DomainObject(rdfType = ModuleDefinition.TYPE)
public class ModuleDefinition extends ASBIdentified implements TopLevel {

    public static final String TYPE = "http://sbols.org/v2#ModuleDefinition";
    private static final Logger logger = LoggerFactory.getLogger(ModuleDefinition.class);
   
    public ModuleDefinition(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    @OwnedEdge(id = SynBadTerms.SbolModule.hasFunctionalComponent, clazz = FunctionalComponent.class)
    public Set<FunctionalComponent> getFunctionalComponents() {
        return getOutgoingObjects(SynBadTerms.SbolModule.hasFunctionalComponent, FunctionalComponent.class);
    }

    @OwnedEdge(id = SynBadTerms.SbolModule.hasModule, clazz = Module.class)
    public Set<Module> getModules() {
        return getOutgoingObjects(SynBadTerms.SbolModule.hasModule, Module.class);
    }

    @OwnedEdge(id = SynBadTerms.SbolModule.hasInteraction, clazz = Interaction.class)
    public Set<Interaction> getInteractions() {
        return getOutgoingObjects(SynBadTerms.SbolModule.hasInteraction, Interaction.class);
    }

    public FunctionalComponent createFunctionalComponent(SBIdentity identity, ComponentDefinition definition, SbolDirection direction, SbolAccess access) {
        ws.perform(FunctionalComponent.createFuncComponent(identity.getIdentity(), definition, this, access, direction,  getContexts()));
        return ws.getObject(identity.getIdentity(), FunctionalComponent.class, getContexts());
    }

    public Module createModule(SBIdentity identity, ModuleDefinition definition) {
        ws.perform(Module.createModule(identity.getIdentity(), definition, this,  getContexts()));
        return ws.getObject(identity.getIdentity(), Module.class, getContexts());
    }

    public Interaction createInteraction(SBIdentity identity, java.util.Collection<SbolInteractionType> type, List<FunctionalComponent> participants, List<SbolInteractionRole> roles) {
        
        if(participants.size() != roles.size()) {
            logger.error("Cannot create interaction - participants and roles are not the same size.");
            return null;
        }
        
        ws.perform(Interaction.createInteraction(identity.getIdentity(), type, this, getContexts()));
        
        int index = 0;
        for(FunctionalComponent c :  participants) {
            SbolInteractionRole role = roles.get(index);
            ws.perform(Participation.createParticipation(SBIdentityHelper.getURI(identity.getPersistentId().toASCIIString() + "/participation_" + ++index + "/" + identity.getVersion()), 
                    identity.getIdentity(),
                    c.getIdentity(),
                    role, getWorkspace(),
                    getContexts()));
        }
        return ws.getObject(identity.getIdentity(), Interaction.class, getContexts());
    }

    public static SBAction createModuleDefinition( URI identity, SBWorkspace workspace, URI[] contexts) {
        return new DefaultSBDomainBuilder(workspace, contexts)
                .createObject(identity, SBIdentityHelper.getURI(ModuleDefinition.TYPE), null, null)
                .build("CreateModDef ["+ identity.toASCIIString() + "]");
    }

    public static SBAction addModel(URI modelIdentity, ModuleDefinition owner, URI workspace, URI[] contexts) {
        return new CreateEdge(owner.getIdentity(), SynBadTerms.SbolModule.hasModel, modelIdentity, workspace, contexts);
    }
}
