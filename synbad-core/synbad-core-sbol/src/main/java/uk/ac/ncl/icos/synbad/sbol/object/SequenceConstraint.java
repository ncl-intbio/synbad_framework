/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.net.URI;
import java.util.Objects;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityHelper;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owengilfellon
 */
@DomainObject(rdfType = SequenceConstraint.TYPE, xmlParent = ComponentDefinition.class)
public class SequenceConstraint extends ASBIdentified implements ChildObject {
 
    public static final String TYPE = "http://sbols.org/v2#SequenceConstraint";
   
    public SequenceConstraint(SBIdentity identity, SBWorkspace workspaceIdentity, SBValueProvider values) {
        super(identity, workspaceIdentity, values);
    }
    
    @Override
    public URI getType() {
        return SBIdentityHelper.getURI(TYPE);
    }

    public Component getSubject() {
        return getOutgoingObject(SynBadTerms.SbolSequenceConstraint.hasSubject, Component.class);
    }

    public Component getObject() {
        return getOutgoingObject(SynBadTerms.SbolSequenceConstraint.hasObject, Component.class);
    }

    public ConstraintRestriction getRestriction() {
        return getValues(SynBadTerms.SbolSequenceConstraint.hasRestriction).stream()
                .filter(v -> v.isURI())
                .map(v ->  ConstraintRestriction.fromString(v.asURI().toASCIIString()))
                .findFirst().orElse(null);
    }
    
    @Override
    public ComponentDefinition getParent() {
        return getIncomingObject(SynBadTerms.SbolComponent.hasSequenceConstraint, ComponentDefinition.class);
    }
    
    public static SBAction createSequenceConstraint(URI identity, ComponentDefinition owner, Component subject, ConstraintRestriction restriction, Component object, URI[] contexts) {

        return new DefaultSBDomainBuilder(owner.getWorkspace(), contexts)
                .createObject(identity, SBIdentityHelper.getURI(SequenceConstraint.TYPE), owner.getIdentity(), SBIdentityHelper.getURI(ComponentDefinition.TYPE))
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceConstraint.hasRestriction), restriction.getUri())
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceConstraint.hasObject), object.getIdentity())
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceConstraint.hasSubject), subject.getIdentity())
                .createEdge(owner.getIdentity(), SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasSequenceConstraint), identity)
                .build("CreateSeqCon ["+ identity.toASCIIString() + "]");
    
    }
    
    public static SBAction createSequenceConstraint(URI identity, URI owner, URI subject, ConstraintRestriction restriction, URI object, SBWorkspace workspaceId, URI[] contexts) {

        return new DefaultSBDomainBuilder(workspaceId, contexts)
                .createObject(identity, SBIdentityHelper.getURI(SequenceConstraint.TYPE), owner, SBIdentityHelper.getURI(ComponentDefinition.TYPE))
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceConstraint.hasRestriction), restriction.getUri())
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceConstraint.hasObject), object)
                .createEdge(identity, SBIdentityHelper.getURI(SynBadTerms.SbolSequenceConstraint.hasSubject), subject)
                .createEdge(owner, SBIdentityHelper.getURI(SynBadTerms.SbolComponent.hasSequenceConstraint), identity)
                .build("CreateSeqCon ["+ identity.toASCIIString() + "]");
    
    }
    
    public static SBIdentity getSequenceConstraintIdentity(SBIdentity owner, SBIdentity subject, SBIdentity object, ConstraintRestriction restriction) {

            String r = "";
            
            switch(restriction) {
                case PRECEDES:
                    r = "precedes";
                    break;
                case OPPOSITE_ORIENTATION_AS:
                    r = "opposite_orientation_as";
                    break;
                case SAME_ORIENTATION_AS:
                    r = "same_orientation_as";
                    break;
            }

            String subjectName = SBIdentityHelper.getChildDisplayIdFromId(subject.getIdentity());
            String objectName = SBIdentityHelper.getChildDisplayIdFromId(object.getIdentity());
            
            return SBIdentity.getIdentity(
                    owner.getUriPrefix(), 
                    owner.getDisplayID(),
                    "sc_" + subjectName + "_" + r + "_" + objectName,
                    owner.getVersion());

    }
    

}
