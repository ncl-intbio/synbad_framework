/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.definition;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;

/**
 *
 * @author owengilfellon
 */
public enum SbolDirection implements SBDataDef {
    
    IN(URI.create("http://sbols.org/v2#in")),
    OUT(URI.create("http://sbols.org/v2#out")),
    INOUT(URI.create("http://sbols.org/v2#inout")),  
    NONE(URI.create("http://sbols.org/v2#none"));
    
    private final URI uri;

    @Override
    public URI getUri() {
        return uri;
    }

    private SbolDirection(URI uri) {
        this.uri = uri;
    }  
}
