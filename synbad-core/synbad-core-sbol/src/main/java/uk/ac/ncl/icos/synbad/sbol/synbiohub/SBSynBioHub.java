/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.synbiohub;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;

import org.sbolstandard.core2.SBOLConversionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.synbiohub.frontend.IdentifiedMetadata;
import org.synbiohub.frontend.SynBioHubException;
import org.synbiohub.frontend.SynBioHubFrontend;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;

/**
 *
 * @author owengilfellon
 */
public class SBSynBioHub {
    
    private final Logger LOGGER = LoggerFactory.getLogger(SBSynBioHub.class);
    public void x() throws SBOLConversionException {
        
        SBWorkspaceManager m = new DefaultSBWorkspaceManagerMem();
        SBWorkspace ws = m.getWorkspace(URI.create("http://www.synbad.org/synbiohubtest"));
        SynBioHubFrontend fe = new SynBioHubFrontend("https://synbiohub.org/");
        
        try {
            LOGGER.debug("CD: " + fe.getCount("ComponentDefinition"));
            
            ArrayList<IdentifiedMetadata> l = fe.getMatchingComponentDefinitionMetadata(
                    null, 
                    Collections.singleton(ComponentRole.Promoter.getUri()), 
                    Collections.singleton(ComponentType.DNA.getUri()),
                    null, 0, 50);
            LOGGER.debug("CDS (DNA): " + l.size());
            for(IdentifiedMetadata md : l) {
                LOGGER.debug("\tCDS: {} | {}", md.getName(), md.getDescription());
                
                
                
//                SBOLWriter.setKeepGoing(true);
//                SBOLWriter.write(fe.getSBOL(URI.create(md.getUri())), System.out);
            }
        
        } catch (SynBioHubException ex) {
            LOGGER.error("Could not get count", ex);
        }
    }
}
