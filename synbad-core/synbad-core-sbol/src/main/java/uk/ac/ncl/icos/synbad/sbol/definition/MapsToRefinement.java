/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.definition;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;

/**
 *
 * @author owengilfellon
 */
public enum MapsToRefinement implements SBDataDef{ 
        
        useLocal("http://sbols.org/v2#useLocal"), 
        useRemote("http://sbols.org/v2#useRemote"), 
        verifyIdentical("http://sbols.org/v2#verifyIdentical");
    
        private final URI refinement;
    
        private MapsToRefinement(String refinement) {
            this.refinement = URI.create(refinement);
        }

        public String getRefinement() {
            return refinement.toASCIIString();
        }

        @Override
        public URI getUri() {
            return refinement;
        }
    }
