/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.object;

import java.util.Optional;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;

/**
 *
 * @author owengilfellon
 */
public interface SbolInstance<T extends SBIdentified> extends SBIdentified, ChildObject {
    
    public Optional<T> getDefinition();
    
}
