/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.model;

import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.view.SBViewFactory;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewObject;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = SBViewFactory.class)
public class SbolHViewFactory implements SBViewFactory<SbolModel, ModuleDefinition, SbolHView>{

    private static final Logger LOGGER = LoggerFactory.getLogger(SbolHViewFactory.class);

    @Override
    public SbolHView getView(ModuleDefinition rootNode, SbolModel model) {  
        SbolHView view = new SbolHView(rootNode, model);
        SBWorkspaceGraph g = new SBWorkspaceGraph(model.getWorkspace(), rootNode.getContexts());
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Constructed view: {}", view.getClass().getSimpleName());
        return view;
    }

    @Override
    public Class<SbolModel> getModelClass() {
        return SbolModel.class;
    }

    @Override
    public Class<ModuleDefinition> getRootNodeClass() {
        return ModuleDefinition.class;
    }

    @Override
    public Class<SbolHView> getViewClass() {
        return SbolHView.class;
    }
    
}
