package uk.ac.ncl.icos.synbad.sbol.model;

import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.model.ASBModelFactory;
import uk.ac.ncl.icos.synbad.model.SBModelFactory;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;

@ServiceProvider(service = SBModelFactory.class)
public class SbolModelProvider extends ASBModelFactory<SbolModel, ModuleDefinition> {

    public SbolModelProvider() {
        super(SbolModel.class, ModuleDefinition.class);
    }

    @Override
    public SbolModel getModel(ModuleDefinition rootNode) {

        return new SbolModel(rootNode.getWorkspace(), rootNode);
    }
}
