/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.view;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.*;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphSubscriber;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBGraphTraversal;
import uk.ac.ncl.icos.synbad.sbol.definition.MapsToRefinement;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolInteractionRole;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Interaction;
import uk.ac.ncl.icos.synbad.sbol.object.MapsTo;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.Participation;
import uk.ac.ncl.icos.synbad.sbol.object.SbolInstance;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewEdge;
import uk.ac.ncl.icos.synbad.sbol.view.objects.SbolViewObject;
import uk.ac.ncl.icos.synbad.tree.ITree;
import uk.ac.ncl.icos.synbad.view.SBHWritableView;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.view.AViewUpdater;

/**
 *
 * @author owengilfellon
 */
public final class SbolHViewUpdater<V extends SBHWritableView<SbolViewObject, SbolViewEdge>> 
        extends AViewUpdater<SBWorkspaceGraph, V> implements SBGraphSubscriber<URI, SynBadEdge> {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(SbolHViewUpdater.class);

    private static final List<String> HIERARCHY_PREDICATES = Arrays.asList(
        SynBadTerms.Sbol.definedBy,
      SynBadTerms.SbolComponent.hasComponent,
        SynBadTerms.SbolModule.hasInteraction,
        SynBadTerms.SbolModule.hasFunctionalComponent,
//        SynBadTerms.SbolModule.hasInteraction,
        SynBadTerms.SbolModule.hasModule,
       SynBadTerms.SbolMapsTo.hasMapsTo,
       SynBadTerms.SbolInteraction.hasParticipation
    );

    private static final List<String> WIRE_PREDICATES = Arrays.asList(SynBadTerms.SbolMapsTo.hasLocalInstance,
        SynBadTerms.SbolMapsTo.hasRemoteInstance
    );

    private final SBWorkspace ws;
    private static final SBDataDefManager defManager = SBDataDefManager.getManager();
    private final Set<TempEdge> edgesToAdd = new HashSet<TempEdge>();
    private final Set<SBIdentified> deferred = new HashSet<>();

    public SbolHViewUpdater(V target, SBIdentified root, SBWorkspace ws) {
        super(new SBWorkspaceGraph(ws, root.getContexts()), target, root);
        this.ws = ws;
        getSourceGraph().subscribe(new SBEventFilter.DefaultFilter(), this);
        LOGGER.info("Processing tree on construction: [ {} ]", root.getDisplayId());
        processTree(SBGraphEventType.ADDED, getTreeFromNode(root));
    }

    @Override
    public void close() {
       getSourceGraph().close();
    }

    private void createObjectAndAddToOwners(SBIdentified object) {
        Collection<SbolViewObject> ownerNodes = getOwnerNodes(object);
        
        if(ownerNodes.isEmpty())
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("No owner nodes found: [ {} ]", object.getDisplayId());
        
        for(SbolViewObject node : ownerNodes) {
            SbolViewObject instanceNode = new SbolViewObject(object, target);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Adding [ {} ] to [ {} ]",instanceNode.getDisplayId(),  node);
            target.addNode(instanceNode, node);
        } 
    }
    
    private void removeObjectFromOwners(SBIdentified object) {
        Collection<SbolViewObject> ownerNodes = getOwnerNodes(object);
        
        if(ownerNodes.isEmpty())
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("No owner nodes found: [ {} ]", object.getDisplayId());
        
        for(SbolViewObject node : ownerNodes) {
            SbolViewObject toRemove = target.getAllChildren(node).stream()
                .filter(n -> n.getObject().getIdentity().equals(object.getIdentity()))
                .findFirst().orElse(null);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Removing [ {} ]", object.getDisplayId());
            if(toRemove != null)
                target.removeNode(toRemove);
        } 
    }

    // ========================================================================
    //                          Tree methods
    // ========================================================================
    
 
    @Override
    public void onEvent(SBEvent e) {
        
        LOGGER.info("GRAPH EVENT!");
        
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {

            SBGraphEvent evt = (SBGraphEvent) e;
                    
            if(null != evt.getGraphEntityType())
                switch (((SBGraphEvent)e).getGraphEntityType()) {
                    case EDGE:
                        onEdgeEvent(evt);
                        break;
                    case NODE:
                        onNodeEvent(evt);
                        break;
                    case VALUE:
                        onValueEvent(evt);
                        break;
                    default:
                        break;
            }
        }
    }
    
    public void processTree(SBGraphEventType type, ITree<Object> tree) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing tree: [{}:{}]", type.getType(), tree.getRootNode().getData());
        Iterator<Object> it = tree.iterator();
       // SynBadEdge edge = null;
        
        while(it.hasNext()) {

            Object o = it.next();
              
            if(SBIdentified.class.isAssignableFrom(o.getClass())) {

                SBIdentified identified = (SBIdentified) o;

                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Processing: [ {} ]", identified.getDisplayId());

                if(ws.containsObject(identified.getIdentity(), Module.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), Module.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), Interaction.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), Interaction.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), FunctionalComponent.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), FunctionalComponent.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), Component.class, identified.getContexts()))
                    processInstance(type, ws.getObject(identified.getIdentity(), Component.class, identified.getContexts()));

                else if(ws.containsObject(identified.getIdentity(), MapsTo.class, identified.getContexts()))
                    deferred.add(ws.getObject(identified.getIdentity(), MapsTo.class, identified.getContexts()));
                
                else if(ws.containsObject(identified.getIdentity(), Participation.class, identified.getContexts()))
                    deferred.add(ws.getObject(identified.getIdentity(), Participation.class, identified.getContexts()));
        
            } else if(SynBadEdge.class.isAssignableFrom(o.getClass())) {          
                SynBadEdge edge = (SynBadEdge) o;         
                SBIdentified from = ws.getObject(edge.getFrom(), SBIdentified.class, root.getContexts());
                SBIdentified to = ws.getObject(edge.getTo(), SBIdentified.class, root.getContexts());      
                edgesToAdd.add(new TempEdge(from, to, edge.getEdge()));
                if(LOGGER.isDebugEnabled())
                    LOGGER.debug("Found edge: [ {} ]", edge.getEdge().toASCIIString());
            }
        }
        
        deferred.stream().forEach(d -> {
                if(ws.containsObject(d.getIdentity(), MapsTo.class, d.getContexts()))
                    processInstance(type, ws.getObject(d.getIdentity(), MapsTo.class, d.getContexts()));

                else if(ws.containsObject(d.getIdentity(), Participation.class, d.getContexts()))
                    processInstance(type, ws.getObject(d.getIdentity(), Participation.class, d.getContexts()));
        
        });
        
        deferred.clear();
        
        edgesToAdd.stream().forEach(e ->{

            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Doing edge: {}", e.label.toASCIIString());
        
            Collection<SbolViewObject> fromCollection = target.findObjects(e.from.getIdentity());
            Collection<SbolViewObject> toCollection = target.findObjects(e.to.getIdentity());
           
            for(SbolViewObject from : fromCollection) {
                for(SbolViewObject to : toCollection) {
                    if(LOGGER.isDebugEnabled())
                        LOGGER.debug("Adding edge: {} -> {}", from.getDisplayId(), to.getDisplayId());
                    target.addEdge(from, to,
                        new SbolViewEdge(
                            new SynBadEdge(
                                e.label,
                                e.from.getIdentity(), 
                                e.to.getIdentity(), from.getContexts()), 
                            from, 
                            to,
                            target));
                }
            }
        });
        
        edgesToAdd.clear();
    }
    
    
    // ========================================================================
    //                      Object processing
    // ========================================================================
    
    private void processInstance(SBGraphEventType type, Module module) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing module: [ {} ]", module.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(module);
                break;
            case REMOVED:
                removeObjectFromOwners(module);
                break;
        }
    }
    
    private void processInstance(SBGraphEventType type, Component component) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing component: [ {} ]", component.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(component);
                break;
            case REMOVED:
                removeObjectFromOwners(component);
                break;
        }
    }
    
    private void processInstance(SBGraphEventType type, FunctionalComponent component) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing functional component: [ {} ]", component.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(component);
                break;
            case REMOVED:
                removeObjectFromOwners(component);
                break;
        }
    }
    
    private void processInstance(SBGraphEventType type, Interaction interaction) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing interaction: [ {} ]", interaction.getDisplayId());

        switch(type) {
            case ADDED:
                createObjectAndAddToOwners(interaction);
                Set<Participation> participations = interaction.getParticipants();
                participations.stream().forEach(deferred::add);
                break;
            case REMOVED:
                removeObjectFromOwners(interaction);
                break;
        }
    }
    
    private void processInstance(SBGraphEventType type, Participation participation) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing participation: [ {} ]", participation.getDisplayId());

        switch(type) {
            case ADDED:
                    SbolInteractionRole role = participation.getRole();
                    FunctionalComponent participant = participation.getParticipant();
                    Interaction interaction = (Interaction)source.getTraversal()
                            .v(participation).e(SBDirection.IN, SynBadTerms.SbolInteraction.hasParticipation)
                            .v(SBDirection.IN, Interaction.class)
                            .getResult().streamVertexes().findFirst().orElse(null);
                    
                    // Add edge from interaction to participant, with label = role
                    
                    target.findObjects(interaction.getIdentity()).forEach(interactionVo -> {
         
                        SbolViewObject participantVo = target.getAllChildren(target.getParent(interactionVo))
                                .stream().filter(vo -> vo.getIdentity().equals(participant.getIdentity()))
                                .findFirst().orElse(null);
                        
                        SbolViewObject owner = target.getParent(interactionVo);
                        
                        // TODO: add Participation node
                        // TODO: add edges from interaction to participation and participation to object

                        target.addEdge(interactionVo, participantVo, 
                            new SbolViewEdge(
                                new SynBadEdge(
                                        URI.create(SynBadTerms.SbolInteraction.participant),
                                    interactionVo.getIdentity(), 
                                    participant.getIdentity(),
                                    owner.getContexts()), 
                                (SbolViewObject)interactionVo, 
                                (SbolViewObject)participantVo,
                                target));
                });
                    
                break;
            case REMOVED:
                //removeObjectFromOwners;
                break;
        }
    }
    
    private void processInstance(SBGraphEventType type, MapsTo mapsTo) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing MapsTo: [ {} ]", mapsTo.getDisplayId());

        switch(type) {
            case ADDED:
               // createObjectAndAddToOwners(mapsTo);
                
                SbolInstance owner = mapsTo.getOwner();
                SbolInstance remote = mapsTo.getRemoteInstance();
                SbolInstance local = mapsTo.getLocalInstance();
                
                // add edge between remote and local
                
                getTargetGraph().findObjects(owner.getIdentity()).forEach(n -> {
                    SbolViewObject remoteVo = getTargetGraph().getAllChildren(n).stream().filter(nRemote -> nRemote.getIdentity().equals(remote.getIdentity())).findFirst().orElse(null);
                    SbolViewObject localVo = getTargetGraph().getAllChildren(target.getParent(n)).stream().filter(nLocal -> nLocal.getIdentity().equals(local.getIdentity())).findFirst().orElse(null);

                   if(localVo == null) {
                       LOGGER.error("Could not get local VO from {}", owner.getDisplayId());
                   }
                   if(remoteVo == null) {
                       LOGGER.error("Could not get remote VO from {}", owner.getDisplayId());
                   }
                  
                   if(localVo != null && remoteVo != null) {
                       if(mapsTo.getRefinement() == MapsToRefinement.useLocal) {
                            getTargetGraph().addEdge(remoteVo, localVo, new SbolViewEdge(new SynBadEdge(mapsTo.getRefinement().getUri(),   remote.getIdentity(), local.getIdentity(), owner.getContexts()), remoteVo, localVo, target));
                        } else {
                            getTargetGraph().addEdge(localVo, remoteVo, new SbolViewEdge(new SynBadEdge(mapsTo.getRefinement().getUri(),   local.getIdentity(), remote.getIdentity(), owner.getContexts()), localVo, remoteVo, target));
                        }
                   }
                });

                break;
            case REMOVED:
                //removeObjectFromOwners(mapsTo);
                break;   
        }
    }

    // ========================================================================
    //                          Event methods
    // ========================================================================

    @Override
    public void onNodeEvent(SBGraphEvent<URI> e) {}

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> e) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: E [ {} ]",  e.getData());
        URI fromIdentity = e.getData().getFrom();
        URI toIdentity = e.getData().getTo();
        
        // Nodes should already be in graph
        
        if(target.findObjects(toIdentity).isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Does not exist in target: N [ {} ] ", toIdentity.toASCIIString());
            return;
        } else if(target.findObjects(fromIdentity).isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Does not exist in target: N [ {} ] ", fromIdentity.toASCIIString());
            return;
        }
        
        if(!target.findObjects(fromIdentity).isEmpty() && !root.getIdentity().equals(fromIdentity) )
            return;
        
        SBIdentified from = ws.getObject(e.getData().getFrom(), SBIdentified.class, root.getContexts());
        ITree<Object> tree = getTreeFromNode(from);
        processTree((SBGraphEventType)e.getType(), tree);
    }
    
    public void onValueEvent(SBGraphEvent<SBValue> e) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: V [ {} ] " + e.getClass());
    }
    
    // ========================================================================
    //                          SBOL helper methods
    // ========================================================================

    protected ITree<Object> getTreeFromNode(SBValued identified) {

        SBGraphTraversal<SBValued, SBValued, SBWorkspaceGraph> t = (source.getTraversal()).v(identified);

        // Follow out edges that have specified predicates, skipping duplicates

        t = t.loop(o -> true, new DefaultSBGraphTraversal<SBValued, SBEdge, SBWorkspaceGraph>(source)
                .e(SBDirection.OUT, getHierarchyPredicates().toArray(new String[]{}))
                .v(SBDirection.OUT), true, false);

        return (ITree<Object>)t.getResult().getTree();
    }

    private Set<SBIdentified> getSbolOwnerOfInstance(SBIdentified instance) {

       SBTraversalResult<SBIdentified, SynBadEdge> result = source.getTraversal().v(instance)
            .e(SBDirection.IN, 
                    SynBadTerms.SbolComponent.hasComponent,
                    SynBadTerms.SbolModule.hasFunctionalComponent,
                    SynBadTerms.SbolModule.hasInteraction,
                    SynBadTerms.SbolModule.hasModule)
            .v(SBDirection.IN, SBIdentified.class)
            .getResult();

       Stream<SBIdentified> stream = result.streamVertexes();
       SBIdentified ownerDefinitions =  stream.findFirst().orElse(null);
 
        // if owner is root, add to root
       
       if(ownerDefinitions == null) {
           LOGGER.warn("Owner definition could not be found");
           return Collections.EMPTY_SET;
       }
       
       if(ownerDefinitions.equals(root)) {
           if(LOGGER.isDebugEnabled())
               LOGGER.debug("Found ModuleDef owner [ {} ] of instance [ {} ]",ownerDefinitions, instance);
           return Collections.singleton(ownerDefinitions);
       }
       
       // otherwise, add to instances of definition in graph
       
       Set<SBIdentified> ownerInstances = source.getTraversal().v(ownerDefinitions)
            .e(SBDirection.IN, SynBadTerms.Sbol.definedBy)
            .v(SBDirection.IN)
            .filter(i -> !target.findObjects(i.getIdentity()).isEmpty())
               .getResult().streamVertexes().map(v -> (SBIdentified) v).collect(Collectors.toSet());

        return ownerInstances;
    }

    public Collection<SbolViewObject>  getOwnerNodes(SBIdentified instance) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("getOwnerNodes() of: [ {} ]", instance);

        Set<SBIdentified> ownerDefinitons = getSbolOwnerOfInstance(instance);
        Collection<SbolViewObject> ownerNodes = new HashSet<>();
        
        if(ownerDefinitons != null) {
            for(SBIdentified owner : ownerDefinitons) {
                Collection<SbolViewObject> objs = target.findObjects(owner.getIdentity());
                ownerNodes.addAll(objs);
            }
        } else {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("No owners found for: [ {} ]", instance);
        }
        
        return ownerNodes;
    }

    @Override
    public Collection<String> getHierarchyPredicates() {
        return HIERARCHY_PREDICATES;
    }
  
    class TempEdge {

        private final SBIdentified from;
        private final SBIdentified to;
        private final URI label;
        
        public TempEdge(SBIdentified from, SBIdentified to, URI label) {
            this.from = from;
            this.to = to;
            this.label = label;
        }
    }
}
