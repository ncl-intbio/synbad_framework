/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.tests;

import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.datadefinition.types.Role;
import uk.ac.ncl.icos.synbad.datadefinition.types.Type;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.action.SbolActionBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;

/**
 *
 * @author owengilfellon
 */
public class ContextTests  {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ContextTests.class);
    
    private static final URI WORKSPACE_URI = UriHelper.synbad.namespacedUri("contextTests");
    
    private static final URI CONTEXT_1 = UriHelper.synbad.namespacedUri("context1");
    private static final URI CONTEXT_2 = UriHelper.synbad.namespacedUri("context2");
    
    private static final URI CD1_ID = UriHelper.synbad.namespacedUri("cd1");
    private static final URI CD2_ID = UriHelper.synbad.namespacedUri("cd2");
    private static final URI CD3_ID = UriHelper.synbad.namespacedUri("cd3");
    
    private static final URI C2_ID = UriHelper.synbad.namespacedUri("c2");
    private static final URI C3_ID = UriHelper.synbad.namespacedUri("c3");
    
    private static final URI SC_ID = UriHelper.synbad.namespacedUri("sc");

    private static final String PREFIX = "http://www.synbad.org";
    private static final String VERSION = "1.0";
    
    private SBWorkspace ws;

    @BeforeClass
    public static void setUpClass() { }
    
    @AfterClass
    public static void tearDownClass() { }
    
    @Before
    public void setUp() {
        this.ws = new DefaultSBWorkspace(WORKSPACE_URI);
    }
    
    @After
    public void tearDown() {
        this.ws.shutdown();
       ws = null;
    }
    
    private void createModelInContext(URI context) {
        
        LOGGER.debug("Creating model in context: {}", context.toASCIIString());
        
        ws.createContext(context);
        
        SbolActionBuilder b = new SbolActionBuilder(ws, new URI[]{ context });
        b.createComponentDefinition(
                CD1_ID, 
                new Type[] { ComponentType.DNA }, 
                new Role[] { ComponentRole.Gene})
            .createComponentDefinition(
                CD2_ID, 
                new Type[] { ComponentType.DNA }, 
                new Role[] { ComponentRole.Promoter})
            .createComponentDefinition(
                CD3_ID, 
               new Type[] { ComponentType.DNA }, 
               new Role[] { ComponentRole.CDS})
            .createComponent(C2_ID, CD1_ID, CD2_ID, SbolAccess.PUBLIC)
            .createComponent(C3_ID, CD1_ID, CD3_ID, SbolAccess.PUBLIC)
            .createSequenceConstraint(SC_ID, CD1_ID, C2_ID, ConstraintRestriction.PRECEDES, C3_ID);
        ws.perform(b.build());
    }
    
    @Test
    public void testContextEncapsulation() {
        
        createModelInContext(CONTEXT_1);
        createModelInContext(CONTEXT_2);
        
        Assert.assertEquals("Incorrect number of contexts in workspace", 2, ws.getContextIds().length);
        
        // Check context 1
        
        ComponentDefinition c1_cd1 = ws.getObject(CD1_ID, ComponentDefinition.class, new URI[]{ CONTEXT_1 });
        Assert.assertNotNull("ComponentDefinition 1 in Context 1 should not be null", c1_cd1);
        
        Component c1_c2 = ws.getObject(C2_ID, Component.class, new URI[]{ CONTEXT_1 });
        Assert.assertNotNull("Component 2 in Context 1 should not be null", c1_c2);
        ComponentDefinition c1_cd2 = c1_c2.getDefinition().get();
        Assert.assertNotNull("ComponentDefinition 2 in Context 1 should not be null", c1_cd2);
        Component c1_c3 = ws.getObject(C3_ID, Component.class, new URI[]{ CONTEXT_1 });
        Assert.assertNotNull("Component 3 in Context 1 should not be null", c1_c3);
        ComponentDefinition c1_cd3 = c1_c3.getDefinition().get();
        Assert.assertNotNull("ComponentDefinition 3 in Context 1 should not be null", c1_cd3);
        
        Assert.assertEquals("Incorrect number of contexts in c3 definition", 1, c1_cd3.getContexts().length);
        
        SBWorkspaceGraph g1 = new SBWorkspaceGraph(ws, new URI[]{ CONTEXT_1 });
        
        Assert.assertEquals("Incorrect number of contexts in graph 1", 1, g1.getContexts().length);
        
        long c1_componentCount = g1.getTraversal().v(c1_cd1)
                .e(SBDirection.OUT, SynBadTerms.SbolComponent.hasComponent)
                .v(SBDirection.OUT).getResult().stream().count();
        
        Assert.assertEquals("Should be 2 components via traversal in context 1", 2, c1_componentCount);
        
        // Check context 2
        
        ComponentDefinition c2_cd1 = ws.getObject(CD1_ID, ComponentDefinition.class, new URI[]{ CONTEXT_2 });
        Assert.assertNotNull("ComponentDefinition 1 in Context 2 should not be null", c2_cd1);

        Component c2_c2 = ws.getObject(C2_ID, Component.class, new URI[]{ CONTEXT_2 });
        Assert.assertNotNull("Component 2 in Context 2 should not be null", c2_c2);
        Component c2_c3 = ws.getObject(C3_ID, Component.class, new URI[]{ CONTEXT_2 });
        Assert.assertNotNull("Component 3 in Context 2 should not be null", c2_c3);
        ComponentDefinition c2_cd2 = c2_c2.getDefinition().get();
        Assert.assertNotNull("ComponentDefinition 2 in Context 2 should not be null", c2_cd2);
        ComponentDefinition c2_cd3 = c2_c3.getDefinition().get();
        Assert.assertNotNull("ComponentDefinition 3 in Context 2 should not be null", c2_cd3);
        
        Assert.assertEquals("Incorrect number of contexts in c3 definition", 1, c2_cd3.getContexts().length);
        
        SBWorkspaceGraph g2 = new SBWorkspaceGraph(ws, new URI[]{ CONTEXT_2 });
        
        Assert.assertEquals("Incorrect number of contexts in graph 2", 1, g2.getContexts().length);
        
        long c2_componentCount = g2.getTraversal().v(c2_cd1)
                .e(SBDirection.OUT, SynBadTerms.SbolComponent.hasComponent)
                .v(SBDirection.OUT).getResult().stream().count();
        
        Assert.assertEquals("Should be 2 components via traversal in context 2", 2, c2_componentCount);

        g1.close();
        g2.close();
        
    }
    
    @Test
    public void testDuplication() {
        
        createModelInContext(CONTEXT_1);
        ws.createContext(CONTEXT_2);
        
        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(ws, new URI[]{ CONTEXT_2 });
        b.duplicateObject(CD1_ID, new URI[]{ CONTEXT_1 }, false);
        ws.perform(b.build());

        // Check context 2
        
        ComponentDefinition c2_cd1 = ws.getObject(CD1_ID, ComponentDefinition.class, new URI[]{ CONTEXT_2 });
        Assert.assertNotNull("ComponentDefinition 1 in Context 2 should not be null", c2_cd1);

        Component c2_c2 = ws.getObject(C2_ID, Component.class, new URI[]{ CONTEXT_2 });
        Assert.assertNotNull("Component 2 in Context 2 should not be null", c2_c2);
        Component c2_c3 = ws.getObject(C3_ID, Component.class, new URI[]{ CONTEXT_2 });
        Assert.assertNotNull("Component 3 in Context 2 should not be null", c2_c3);
        ComponentDefinition c2_cd2 = c2_c2.getDefinition().get();
        Assert.assertNotNull("ComponentDefinition 2 in Context 2 should not be null", c2_cd2);
        ComponentDefinition c2_cd3 = c2_c3.getDefinition().get();
        Assert.assertNotNull("ComponentDefinition 3 in Context 2 should not be null", c2_cd3);
        
        Assert.assertEquals("Incorrect number of contexts in c3 definition", 1, c2_cd3.getContexts().length);
        
        SBWorkspaceGraph g2 = new SBWorkspaceGraph(ws, new URI[]{ CONTEXT_2 });
        
        Assert.assertEquals("Incorrect number of contexts in graph 2", 1, g2.getContexts().length);
        
        long c2_componentCount = g2.getTraversal().v(c2_cd1)
                .e(SBDirection.OUT, SynBadTerms.SbolComponent.hasComponent)
                .v(SBDirection.OUT).getResult().stream().count();
        
        Assert.assertEquals("Should be 2 components via traversal in context 2", 2, c2_componentCount);

        
    }
}
