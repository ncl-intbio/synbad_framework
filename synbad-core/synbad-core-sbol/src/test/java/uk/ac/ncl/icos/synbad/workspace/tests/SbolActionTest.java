/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.tests;

import com.google.common.collect.ObjectArrays;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.datadefinition.types.ConstraintRestriction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.actions.DefaultSBDomainBuilder;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolAccess;
import uk.ac.ncl.icos.synbad.sbol.definition.SbolDirection;
import uk.ac.ncl.icos.synbad.sbol.object.*;
import uk.ac.ncl.icos.synbad.sbol.object.Location.Cut;
import uk.ac.ncl.icos.synbad.sbol.object.Location.Range;
import uk.ac.ncl.icos.synbad.workspace.actions.SetValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.openide.util.Lookup;

/**
 *
 * @author owengilfellon
 */
public class SbolActionTest {

    private static final URI WORKSPACE_ID = UriHelper.synbad.namespacedUri("commandTests");

    private static final String PREFIX = "http://www.synbad.org";
    private static final String VERSION = "1.0";

    private static final URI[] CONTEXTS = new URI[]{URI.create("http://www.synbad.org/flow_tests/1.0")};
    private static final URI[] EMPTY_CONTEXTS = new URI[]{URI.create("http://www.synbad.org/flow_tests_empty/1.0")};

    private static int START = 1;
    private static int END = 100;
    private static int CUT = 70;

    private static final Logger LOGGER = LoggerFactory.getLogger(SbolActionTest.class);

    private SBWorkspace ws;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ws = new DefaultSBWorkspace(WORKSPACE_ID);
        ws.createContext(CONTEXTS[0]);
        ws.createContext(EMPTY_CONTEXTS[0]);
    }

    @After
    public void tearDown() {
        ws.shutdown();
        ws = null;
    }

  //  @Test
    public void testNameAndDescriptionActions() throws SBIdentityException {
        SBIdentity cdId1 = ws.getIdentityFactory().getIdentity(PREFIX, "cd1", VERSION);
        ws.perform(ComponentDefinition.createComponentDefinition(cdId1.getIdentity(),
                Collections.singleton(ComponentType.DNA),
                Collections.singleton(ComponentRole.Promoter),
                ws,
                CONTEXTS));
        


        ComponentDefinition cd1 = ws.getObject(cdId1.getIdentity(), ComponentDefinition.class, CONTEXTS);
        assert (cd1.getVersion().equals(VERSION));
        assert (cd1.getIdentity().equals(URI.create("http://www.synbad.org/cd1/1.0")));

        // Test methods
      /*  cd1.setName("ChangeByMethod");
        assert (cd1.getName().equals("ChangeByMethod"));
        cd1.setDescription("ChangeByMethod");
        assert (cd1.getDescription().equals("ChangeByMethod"));
*/
        // Test helper actions
        ws.perform(new SetValue(cdId1.getIdentity(), SynBadTerms.SbolIdentified.hasName, SBValue.parseValue("ChangeBySetNameCommand"), cd1.getType(), ws, CONTEXTS));
        assert (cd1.getName().equals("ChangeBySetNameCommand"));
        ws.perform(new SetValue(cdId1.getIdentity(), SynBadTerms.SbolIdentified.hasDescription, SBValue.parseValue("ChangeBySetDescriptionCommand"), cd1.getType(), ws, CONTEXTS));
        assert (cd1.getDescription().equals("ChangeBySetDescriptionCommand"));
    }

    @Test
    public void testComponentDefinitionActions() throws SBIdentityException {

        // Create identity for module and component, instantiate Object creation
        // actions and issue to workspace.
        SBIdentity cdId1 = ws.getIdentityFactory().getIdentity(PREFIX, "cd1", VERSION);
        SBIdentity cdId2 = ws.getIdentityFactory().getIdentity(PREFIX, "cd2", VERSION);
        SBIdentity cdId3 = ws.getIdentityFactory().getIdentity(PREFIX, "cd3", VERSION);
        SBIdentity seq1Id = ws.getIdentityFactory().getIdentity(PREFIX, "sequence1", VERSION);
        SBIdentity seq2Id = ws.getIdentityFactory().getIdentity(PREFIX, "sequence2", VERSION);
        SBIdentity seq3Id = ws.getIdentityFactory().getIdentity(PREFIX, "sequence3", VERSION);
        SBIdentity c1Id = ws.getIdentityFactory().getIdentity(PREFIX, cdId1.getDisplayID(), "component1", VERSION);
        SBIdentity c2Id = ws.getIdentityFactory().getIdentity(PREFIX, cdId1.getDisplayID(), "component2", VERSION);
        SBIdentity location1Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentInstance1_Location", VERSION);
        SBIdentity location2Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentInstance2_Location", VERSION);
        SBIdentity annotation1Identity = ws.getIdentityFactory().getIdentity(PREFIX, cdId1.getDisplayID(), "componentInstance1_Annotation", VERSION);
        SBIdentity annotation2Identity = ws.getIdentityFactory().getIdentity(PREFIX, cdId1.getDisplayID(), "componentInstance2_Annotation", VERSION);
        SBIdentity constraintIdentity = ws.getIdentityFactory().getIdentity(PREFIX, cdId1.getDisplayID(), "componentInstance1_2Constraint", VERSION);

        ws.perform(ComponentDefinition.createComponentDefinition(cdId1.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.Promoter), ws, CONTEXTS));
        ws.perform(ComponentDefinition.createComponentDefinition(cdId2.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.RBS), ws, CONTEXTS));
        ws.perform(ComponentDefinition.createComponentDefinition(cdId3.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.CDS), ws, CONTEXTS));

        ComponentDefinition cd1 = ws.getObject(cdId1.getIdentity(), ComponentDefinition.class, CONTEXTS);
        ComponentDefinition cd2 = ws.getObject(cdId2.getIdentity(), ComponentDefinition.class, CONTEXTS);
        ComponentDefinition cd3 = ws.getObject(cdId3.getIdentity(), ComponentDefinition.class, CONTEXTS);

        assert (cd1 != null);
        assert (cd2 != null);
        assert (cd3 != null);

        Assert.assertNull(ws.getObject(cdId1.getIdentity(), ComponentDefinition.class, EMPTY_CONTEXTS));
        Assert.assertNull(ws.getObject(cdId2.getIdentity(), ComponentDefinition.class, EMPTY_CONTEXTS));
        Assert.assertNull(ws.getObject(cdId3.getIdentity(), ComponentDefinition.class, EMPTY_CONTEXTS));

        ws.perform(Sequence.createSequence(seq1Id.getIdentity(), "AAAAAAAAAAAAAAAATTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html", ws, CONTEXTS));
        ws.perform(Sequence.createSequence(seq2Id.getIdentity(), "AAAAAAAAAAAAAAAA", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html", ws, CONTEXTS));
        ws.perform(Sequence.createSequence(seq3Id.getIdentity(), "TTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html", ws, CONTEXTS));

        Sequence seq1 = ws.getObject(seq1Id.getIdentity(), Sequence.class, CONTEXTS);
        Sequence seq2 = ws.getObject(seq2Id.getIdentity(), Sequence.class, CONTEXTS);
        Sequence seq3 = ws.getObject(seq3Id.getIdentity(), Sequence.class, CONTEXTS);

        assert (seq1 != null);
        assert (seq2 != null);
        assert (seq3 != null);

        ws.perform(ComponentDefinition.addSequence(seq1.getIdentity(), cd1, CONTEXTS));
        ws.perform(ComponentDefinition.addSequence(seq2.getIdentity(), cd2, CONTEXTS));
        ws.perform(ComponentDefinition.addSequence(seq3.getIdentity(), cd3, CONTEXTS));
        ws.perform(Component.createComponent(c1Id.getIdentity(), cd1, cd2, SbolAccess.PUBLIC, ws, CONTEXTS));
        ws.perform(Component.createComponent(c2Id.getIdentity(), cd1, cd3, SbolAccess.PUBLIC, ws, CONTEXTS));

        Component c1 = ws.getObject(c1Id.getIdentity(), Component.class, CONTEXTS);
        Component c2 = ws.getObject(c2Id.getIdentity(), Component.class, CONTEXTS);

        assert (cd1.getSequences().contains(seq1));
        assert (cd2.getSequences().contains(seq2));
        assert (cd3.getSequences().contains(seq3));
        assert (ws.outgoingEdges(cd1, SynBadTerms.SbolComponent.hasSequence, Sequence.class, CONTEXTS).size() == 1);
        assert (ws.outgoingEdges(cd2, SynBadTerms.SbolComponent.hasSequence, Sequence.class, CONTEXTS).size() == 1);
        assert (ws.outgoingEdges(cd3, SynBadTerms.SbolComponent.hasSequence, Sequence.class, CONTEXTS).size() == 1);
        assert (ws.outgoingEdges(cd1, ws.getContextIds()).size() == 3);
        assert (ws.outgoingEdges(cd1, SynBadTerms.SbolComponent.hasComponent, Component.class, CONTEXTS).size() == 2);
        assert (c1 != null);
        assert (c2 != null);
        assert (cd1.getComponents().contains(c1));
        assert (cd1.getComponents().contains(c2));
        assert (ws.outgoingEdges(cd1, SynBadTerms.SbolComponent.hasComponent, Component.class, CONTEXTS).size() == 2);

        ws.perform(Location.createRange(location1Identity.getIdentity(), START, END, ws, CONTEXTS));
        ws.perform(Location.createCut(location2Identity.getIdentity(), CUT, ws, CONTEXTS));

        Location location1 = ws.getObject(location1Identity.getIdentity(), Location.class, CONTEXTS);
        Location location2 = ws.getObject(location2Identity.getIdentity(), Location.class, CONTEXTS);

        assert (location1 != null);
        assert (location2 != null);

        ws.perform(SequenceAnnotation.createSequenceAnnotation(annotation1Identity.getIdentity(), cd1, c1, CONTEXTS, location1));
        ws.perform(SequenceAnnotation.createSequenceAnnotation(annotation2Identity.getIdentity(), cd1, c2, CONTEXTS, location2));

        SequenceAnnotation ann1 = ws.getObject(annotation1Identity.getIdentity(), SequenceAnnotation.class, CONTEXTS);
        SequenceAnnotation ann2 = ws.getObject(annotation2Identity.getIdentity(), SequenceAnnotation.class, CONTEXTS);

        assert (ann1.getLocation().is(Range.TYPE));
        assert (ann2.getLocation().is(Cut.TYPE));

        assert (((Range) ann1.getLocation()).getStart() == START);
        assert (((Range) ann1.getLocation()).getEnd() == END);
        assert (((Cut) ann2.getLocation()).getCutAt() == CUT);

        ws.perform(SequenceConstraint.createSequenceConstraint(constraintIdentity.getIdentity(), cd1, c1, ConstraintRestriction.PRECEDES, c2, CONTEXTS));
        SequenceConstraint c = ws.getObject(constraintIdentity.getIdentity(), SequenceConstraint.class, CONTEXTS);
       // c.setDescription("Constraint:- C precedes D in B");

        assert (c != null);

        DefaultSBDomainBuilder b = new DefaultSBDomainBuilder(ws, CONTEXTS)
                .removeObjectAndDependants(cdId1.getIdentity(), null)
                .removeObjectAndDependants(cdId2.getIdentity(), null)
                .removeObjectAndDependants(cdId3.getIdentity(), null)
                .removeObjectAndDependants(seq1Id.getIdentity(), null)
                .removeObjectAndDependants(seq2Id.getIdentity(), null)
                .removeObjectAndDependants(seq3Id.getIdentity(), null)
                ;

        ws.perform(b.build());
        if (!ws.getObjects(SBIdentified.class, CONTEXTS).isEmpty()) {
            ws.getObjects(SBIdentified.class, CONTEXTS).stream().map(o -> (ASBIdentified) o).forEach(System.out::println);
        }

        //  ws.getRdfService().getStatements(null, null, null).stream().forEach(System.out::println);
        Collection<SBIdentified> i = ws.getObjects(SBIdentified.class, ObjectArrays.concat(ws.getSystemContextIds(), ws.getContextIds(), URI.class));
        Assert.assertEquals(0, i.size());

    }

    @Test
    public void testCreateObjects() throws SBIdentityException {

        // Create identity for module and component, instantiate Object creation
        // actions and issue to workspace.
        SBIdentity moduleIdentity = ws.getIdentityFactory().getIdentity(PREFIX, "rootModule", VERSION);
        SBIdentity compDef1Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentDefinition", VERSION);
        SBIdentity compDef2Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentDefinition2", VERSION);
        SBIdentity compDef3Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentDefinition3", VERSION);
        SBIdentity sequence1Identity = ws.getIdentityFactory().getIdentity(PREFIX, "sequence1", VERSION);
        SBIdentity sequence2Identity = ws.getIdentityFactory().getIdentity(PREFIX, "sequence2", VERSION);
        SBIdentity compInstanceIdentity = ws.getIdentityFactory().getIdentity(PREFIX, compDef1Identity.getDisplayID(), "B_instance", VERSION);
        SBIdentity compInstance1Identity = ws.getIdentityFactory().getIdentity(PREFIX, compDef1Identity.getDisplayID(), "C_instance", VERSION);
        SBIdentity compInstance2Identity = ws.getIdentityFactory().getIdentity(PREFIX, compDef1Identity.getDisplayID(), "D_instance", VERSION);
        SBIdentity location1Identity = ws.getIdentityFactory().getIdentity(PREFIX, compInstance1Identity.getDisplayID() + "_rangeLocation", VERSION);
        SBIdentity location2Identity = ws.getIdentityFactory().getIdentity(PREFIX, compInstance2Identity.getDisplayID() + "_rangeLocation", VERSION);
        SBIdentity annotation1Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentInstance1_Annotation", VERSION);
        SBIdentity annotation2Identity = ws.getIdentityFactory().getIdentity(PREFIX, "componentInstance2_Annotation", VERSION);
        SBIdentity constraintIdentity = ws.getIdentityFactory().getIdentity(PREFIX, compDef1Identity.getDisplayID(), "componentInstance1_2Constraint", VERSION);

        ws.perform(ModuleDefinition.createModuleDefinition(moduleIdentity.getIdentity(), ws, CONTEXTS));
        ws.perform(ComponentDefinition.createComponentDefinition(compDef1Identity.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.Promoter), ws, CONTEXTS));
        ws.perform(ComponentDefinition.createComponentDefinition(compDef2Identity.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.RBS), ws, CONTEXTS));
        ws.perform(ComponentDefinition.createComponentDefinition(compDef3Identity.getIdentity(), Collections.singleton(ComponentType.DNA), Collections.singleton(ComponentRole.CDS), ws, CONTEXTS));
        ws.perform(Sequence.createSequence(sequence1Identity.getIdentity(), "AAAAAAAAAAAAAAAA", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html", ws, CONTEXTS));
        ws.perform(Sequence.createSequence(sequence2Identity.getIdentity(), "TTTTTTTTTTTTTTTT", "http://www.chem.qmul.ac.uk/iubmb/misc/naseq.html", ws, CONTEXTS));

        // Retrieve created objects
        ModuleDefinition mod = ws.getObject(moduleIdentity.getIdentity(), ModuleDefinition.class, CONTEXTS);
//        mod.setName("A");
//        mod.setDescription("The root module definition");

        ComponentDefinition comDef1 = ws.getObject(compDef1Identity.getIdentity(), ComponentDefinition.class, CONTEXTS);
        assertNotNull("Component Definition 1 should not be null", comDef1);
//        comDef1.setName("B");
//        comDef1.setDescription("A component definition that contains two child components");

        ComponentDefinition comDef2 = ws.getObject(compDef2Identity.getIdentity(), ComponentDefinition.class, CONTEXTS);
        assertNotNull("Component Definition 2 should not be null", comDef2);
//        comDef2.setName("C");
//        comDef2.setDescription("A component definition");

        ComponentDefinition comDef3 = ws.getObject(compDef3Identity.getIdentity(), ComponentDefinition.class, CONTEXTS);
        assertNotNull("Component Definition 3 should not be null", comDef3);
//        comDef3.setName("D");
//        comDef3.setDescription("A component definition");

        Sequence seq1 = ws.getObject(sequence1Identity.getIdentity(), Sequence.class, CONTEXTS);
        Sequence seq2 = ws.getObject(sequence2Identity.getIdentity(), Sequence.class, CONTEXTS);
        assertNotNull("Sequence 1 should not be null", seq1);
        assertNotNull("Sequence 2 should not be null", seq2);

        // Instantiate ComponentDefinition as Components in ComponentDefinition
        ws.perform(FunctionalComponent.createFuncComponent(compInstanceIdentity.getIdentity(), comDef1, mod, SbolAccess.PUBLIC, SbolDirection.INOUT, CONTEXTS));
        ws.perform(Component.createComponent(compInstance1Identity.getIdentity(), comDef1, comDef2, SbolAccess.PUBLIC, ws, CONTEXTS));
        ws.perform(Component.createComponent(compInstance2Identity.getIdentity(), comDef1, comDef3, SbolAccess.PUBLIC, ws, CONTEXTS));

        FunctionalComponent fc = ws.getObject(compInstanceIdentity.getIdentity(), FunctionalComponent.class, CONTEXTS);
//        fc.setDescription("Instance of B in ModuleDefinition A");

        Component comp1 = ws.getObject(compInstance1Identity.getIdentity(), Component.class, CONTEXTS);
//        comp1.setDescription("Instance of C in ComponentDefinition B");

        Component comp2 = ws.getObject(compInstance2Identity.getIdentity(), Component.class, CONTEXTS);
//        comp2.setDescription("Instance of D in ComponentDefinition B");

        assertNotNull("Functional component should not be null", fc);
        assertNotNull("Component 1 should not be null", comp1);
        assertNotNull("Component 2 should not be null", comp2);

        ws.perform(Location.createRange(location1Identity.getIdentity(), 1, 100, ws, CONTEXTS));
        ws.perform(Location.createCut(location2Identity.getIdentity(), 70, ws, CONTEXTS));

        Location location1 = ws.getObject(location1Identity.getIdentity(), Location.class, CONTEXTS);
        Location location2 = ws.getObject(location2Identity.getIdentity(), Location.class, CONTEXTS);

        assertNotNull("Location 1 should not be null", location1);
        assertNotNull("Location 2 should not be null", location2);

        ws.perform(SequenceAnnotation.createSequenceAnnotation(annotation1Identity.getIdentity(), comDef1, comp1, CONTEXTS, location1));
        ws.perform(SequenceAnnotation.createSequenceAnnotation(annotation2Identity.getIdentity(), comDef1, comp2, CONTEXTS, location2));

        SequenceAnnotation ann1 = ws.getObject(annotation1Identity.getIdentity(), SequenceAnnotation.class, CONTEXTS);
        SequenceAnnotation ann2 = ws.getObject(annotation2Identity.getIdentity(), SequenceAnnotation.class, CONTEXTS);

        assertNotNull("Annotation 1 should not be null", ann1);
        assertNotNull("Annotation 2 should not be null", ann2);

        assertTrue("Component of annotation 1 should be component 1", ann1.getComponent().equals(comp1));
        assertTrue("Component of annotation 2 should be component 2", ann2.getComponent().equals(comp2));

        assertTrue("Location of annotation 1 should be location 1", ann1.getLocation().equals(location1));
        assertTrue("Location of annotation 2 should be location 2", ann2.getLocation().equals(location2));

        ws.perform(SequenceConstraint.createSequenceConstraint(constraintIdentity.getIdentity(), comDef1, comp1, ConstraintRestriction.PRECEDES, comp2, CONTEXTS));
        SequenceConstraint c = ws.getObject(constraintIdentity.getIdentity(), SequenceConstraint.class, CONTEXTS);

        assertNotNull("Sequence constraint should not be null", c);
        assertTrue("Constraint subject should be component 1", c.getSubject().equals(comp1));
        assertTrue("Constraint object should be component 2", c.getObject().equals(comp2));
    }

    private void debugSynBadObject(ASBIdentified o) {
        System.out.println("===============================================");
        System.out.println(o.getName().isEmpty() ? o.getDisplayId() : o.getName());
        System.out.println("===============================================");
        System.out.println(o.getIdentity().toASCIIString());
        System.out.println(o.getPersistentId().toASCIIString());
        System.out.println(o.getDisplayId());
        System.out.println(o.getDescription());

        for (String p : o.getPredicates()) {
            for (SBValue v : o.getValues(p)) {
                System.out.println(" - " + p + " : " + v);
            }

        }

        ws.incomingEdges(o, ws.getContextIds()).stream().forEach(e -> System.out.println(" I: " + e.getEdge().toASCIIString() + " from " + ws.getEdgeSource(e, ws.getContextIds())));
        ws.outgoingEdges(o, ws.getContextIds()).stream().forEach(e -> System.out.println(" O: " + e.getEdge().toASCIIString() + " to " + ws.getEdgeTarget(e, ws.getContextIds())));
    }

}
