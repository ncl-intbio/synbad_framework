/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.tests;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import org.sbolstandard.core2.AccessType;
import org.sbolstandard.core2.Component;
import org.sbolstandard.core2.ComponentDefinition;
import org.sbolstandard.core2.DirectionType;
import org.sbolstandard.core2.FunctionalComponent;
import org.sbolstandard.core2.RefinementType;
import org.sbolstandard.core2.RestrictionType;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLDocument;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidate;
import org.sbolstandard.core2.SBOLValidationException;
import org.sbolstandard.core2.SBOLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.sbol.io.SBSbolExporter;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SbolImportExportTest {
    
    private static final URI WORKSPACE_URI = UriHelper.synbad.namespacedUri("sbolImportExport");
    private static final Logger LOGGER = LoggerFactory.getLogger(SbolImportExportTest.class);
    
    private ModuleDefinition def;
    private SBWorkspace ws;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ws = new DefaultSBWorkspace(WORKSPACE_URI);
        def = ExampleFactory.getSubtilinReporter(ws);
    }
    
    @After
    public void tearDown() {
       ws.shutdown();
       ws = null;
       def = null;
    }
    
    
    /**
     * Test of doExport method, of class SBWorkspaceExporter.
     */
//    @Test
//    public void testToSystemOut() throws SBIdentityException, IOException {
//        new SBSbolExporter().export(System.out, ws, ws.getContextIds());
//        new SBJsonExporter().export(System.out, ws, ws.getContextIds());
//        new SBJsonFullExporter().export(System.out, ws, null);
//    }

    /**
     * Test of createEntity method, of class Workspace.
     */
    @Test
    public void testSBOL() throws SBOLValidationException, SBOLConversionException, IOException {
       
        LOGGER.debug("Writing SBOL document of SVP Example to .xml file...");
        
        File f2 = new File("sbolExport.xml");
        if(!f2.exists())
            f2.delete();
        f2.createNewFile();
        
        FileOutputStream os = new FileOutputStream(f2);
        SBSbolExporter exporter = new SBSbolExporter();
        exporter.export(os, ws, ws.getContextIds());
        os.close();
        
        SBOLReader.setCompliant(true);
        SBOLReader.setKeepGoing(true);
        
        SBOLDocument d = SBOLReader.read(f2);
        
        if(SBOLReader.getNumErrors() > 0) {
            SBOLReader.getErrors().stream().forEach(LOGGER::error);
        }

        LOGGER.debug("Validating exported XML file...");
        
        SBOLValidate.clearErrors();
        SBOLValidate.validateSBOL(d, true, true, true);
        SBOLValidate.getErrors().stream().forEach(LOGGER::warn);
        
    }
    
     /**
     * Test of createEntity method, of class Workspace.
     */
    @Test
    public void testSBOL2() throws SBOLValidationException, SBOLConversionException, IOException {
        
        LOGGER.debug("Creating SBOL document...");
        
        SBOLDocument document = new SBOLDocument();
        ComponentDefinition tu = document.createComponentDefinition(
                "http://www.synbad.org", "tu", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        tu.addRole(ComponentRole.Generic.getUri());
        
        ComponentDefinition promoter = document.createComponentDefinition(
                "http://www.synbad.org", "promoter", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        promoter.addRole(ComponentRole.Promoter.getUri());
        
        ComponentDefinition rbs = document.createComponentDefinition(
                "http://www.synbad.org", "rbs", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        rbs.addRole(ComponentRole.RBS.getUri());
        
        ComponentDefinition cds = document.createComponentDefinition(
                "http://www.synbad.org", "cds", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        cds.addRole(ComponentRole.CDS.getUri());
        
        ComponentDefinition terminator = document.createComponentDefinition(
                "http://www.synbad.org", "terminator", "1.0",
                new HashSet<>(Collections.singletonList(ComponentType.DNA.getUri())));
        terminator.addRole(ComponentRole.Terminator.getUri());
        
        Component tuPromoter = tu.createComponent("promoterInstance", AccessType.PUBLIC, promoter.getPersistentIdentity());
        Component tuRBS = tu.createComponent("rbsInstance", AccessType.PUBLIC, rbs.getPersistentIdentity());
        Component tuCDS = tu.createComponent("cdsInstance", AccessType.PUBLIC, cds.getPersistentIdentity());
        Component tuTer = tu.createComponent("terminatorInstance", AccessType.PUBLIC, terminator.getPersistentIdentity());

        tu.createSequenceConstraint("cons1", RestrictionType.PRECEDES, tuPromoter.getPersistentIdentity(), tuRBS.getPersistentIdentity());
        tu.createSequenceConstraint("cons2", RestrictionType.PRECEDES, tuRBS.getPersistentIdentity(), tuCDS.getPersistentIdentity());
        tu.createSequenceConstraint("cons3", RestrictionType.PRECEDES, tuCDS.getPersistentIdentity(), tuTer.getPersistentIdentity());
        
        org.sbolstandard.core2.ModuleDefinition module = document.createModuleDefinition("http://www.synbad.org", "module", "1.0");

        FunctionalComponent tuInstance = module.createFunctionalComponent("tuInstance", AccessType.PUBLIC, tu.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent promoterInstance = module.createFunctionalComponent("promoterInstance", AccessType.PUBLIC, promoter.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent rbsInstance = module.createFunctionalComponent("rbsInstance", AccessType.PUBLIC, rbs.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent cdsInstance = module.createFunctionalComponent("cdsInstance", AccessType.PUBLIC, cds.getPersistentIdentity(), DirectionType.INOUT);
        FunctionalComponent terInstance = module.createFunctionalComponent("terInstance", AccessType.PUBLIC, terminator.getPersistentIdentity(), DirectionType.INOUT);

        tuInstance.createMapsTo("maps1", RefinementType.VERIFYIDENTICAL, promoterInstance.getPersistentIdentity(), tuPromoter.getPersistentIdentity());
        tuInstance.createMapsTo("maps2", RefinementType.VERIFYIDENTICAL, rbsInstance.getPersistentIdentity(), tuRBS.getPersistentIdentity());
        tuInstance.createMapsTo("maps3", RefinementType.VERIFYIDENTICAL, cdsInstance.getPersistentIdentity(), tuCDS.getPersistentIdentity());
        tuInstance.createMapsTo("maps4", RefinementType.VERIFYIDENTICAL, terInstance.getPersistentIdentity(), tuTer.getPersistentIdentity());
        
        LOGGER.debug("Validating SBOL...");
        
        SBOLValidate.validateSBOL(document, true, true, true);
        SBOLValidate.getErrors().stream().forEach(LOGGER::warn);
       
        LOGGER.debug("Loading from .xml file...");
        
        String path = "sbol1.xml";
        path = URLEncoder.encode(path, "UTF-8");
        File f = new File(path);
        if(!f.exists())
            f.delete();
        f.createNewFile();

        SBOLWriter.write(document, f);
        ws.getRdfService().addRdf(f, ws.getContextIds());
        
        LOGGER.debug("Writing back to .xml file...");
       
        String path2 = "sbol2.xml";
        path2 = URLEncoder.encode(path2, "UTF-8");
        File f2 = new File(path2);
        
        if(!f2.exists())
            f2.delete();
        f2.createNewFile();
        
        FileOutputStream os = new FileOutputStream(f2);
        SBSbolExporter exporter = new SBSbolExporter();
        exporter.export(os, ws, ws.getContextIds());
        os.close();
        SBOLDocument d = SBOLReader.read(f2);
        
        LOGGER.debug("Validating exported XML file...");
        
        SBOLValidate.clearErrors();
        SBOLValidate.validateSBOL(d, true, true, true);
        SBOLValidate.getErrors().stream().forEach(LOGGER::warn);
      
    }
}
