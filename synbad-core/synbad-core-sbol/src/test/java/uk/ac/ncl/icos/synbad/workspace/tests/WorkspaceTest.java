/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.tests;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.JFrame;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawlerObserver;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBCrawler;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.sbol.model.SbolModel;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.FunctionalComponent;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.sbol.visualisation.SbolGraphStreamPanel;
import uk.ac.ncl.icos.synbad.view.SBHView;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBHTraversal;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBIdentified;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceTest  {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkspaceTest.class);
    private static final URI WORKSPACE_URI = UriHelper.synbad.namespacedUri("sbolWorkspaceTest");

    private static final String PREFIX = "http://www.synbad.org";
    private static final String VERSION = "1.0";
    
    private SBWorkspaceManager m;
    private SBWorkspace ws;
    private ModuleDefinition def;

    private static SBIdentity md_receiverAndReporter;
    private static SBIdentity md_subtilinReceiver;
    private static SBIdentity md_reporter;

    public WorkspaceTest() {
    }
    
    public static void main(String[] args) {
        WorkspaceTest test = new WorkspaceTest();
        test.setUp();
        test.renderModel();
        test.tearDown();
    }
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        ws = m.getWorkspace(WORKSPACE_URI);
        def = ExampleFactory.getSubtilinReporter(ws);    
        

        md_receiverAndReporter  = ws.getIdentityFactory().getIdentity(URI.create(PREFIX + "/md_ReceiverAndReporter/" + VERSION));
        md_subtilinReceiver  = ws.getIdentityFactory().getIdentity(URI.create(PREFIX + "/md_SubtilinReceiver/" + VERSION));
        md_reporter  = ws.getIdentityFactory().getIdentity(URI.create(PREFIX + "/md_Reporter/" + VERSION));

    }
    
    @After
    public void tearDown() {
       m.closeWorkspace(WORKSPACE_URI);
       ws = null;
       def = null;
       m = null;
    }
    
    @Test
    public void testGetIdentities() {
        Set<SBIdentity> identities = ws.getIdentities(ws.getContextIds());
        assert(identities.contains(md_receiverAndReporter));
        assert(identities.contains(md_subtilinReceiver));
        assert(identities.contains(md_reporter));        
    }

    @Test
    public void testContainsIdentity() {
        Set<SBIdentity> identities = ws.getIdentities(ws.getContextIds());
        assert(identities.stream()
               .filter(i -> ws.containsIdentity(i.getIdentity(), ws.getContextIds()))
               .count() == identities.size());
    }

    @Test
    public void testModuleDefinition() { 
        assertTrue("Root node in example model should have functional components", !def.getFunctionalComponents().isEmpty());
        def.getFunctionalComponents().stream().forEach(fc -> {
            assertTrue("Functional components should be FunctionalComponent.class",FunctionalComponent.class.isAssignableFrom(fc.getClass()));
            assertNotNull("Functional components should have a definition", fc.getDefinition().get());
            assertTrue("Functional component definition should be ComponentDefinition", ComponentDefinition.class.isAssignableFrom(fc.getDefinition().get().getClass()));
        });
        
        assertTrue("Root node in example model should have modules", !def.getModules().isEmpty());
        def.getModules().stream().forEach(module -> {
            assertTrue("Module should be Module.class", Module.class.isAssignableFrom(module.getClass()));
            assertNotNull("Module should have a definition", module.getDefinition().get());
            assertTrue("Module definition should be a ModuleDefinition", ModuleDefinition.class.isAssignableFrom(module.getDefinition().get().getClass()));
        });
        
        Collection<ModuleDefinition> mds = ws.getObjects(ModuleDefinition.class, ws.getContextIds());
        assertTrue("Example model should have ComponentDefinitions" , !mds.isEmpty());
        mds.stream().forEach(md -> {
            Set<Module> modules = md.getModules();
            for(Module module :modules) {
                
                assertNotNull("Module needs a definition", module.getDefinition().get());
            }
        });
    }
    
    @Test
    public void testComponentDefinition() {
        Collection<ComponentDefinition> cds = ws.getObjects(ComponentDefinition.class, ws.getContextIds());
        assertTrue("Example model should have ComponentDefinitions" , !cds.isEmpty());
        cds.stream().forEach(cd -> {
            Set<Component> components = cd.getComponents();
            for(Component component : components) {
                assertTrue("Component parent not correct", component.getParent().equals(cd));
            }
            
            if(!components.isEmpty()) {
               // System.out.println("Ordered components ----> ");
                
               cd.getOrderedComponents().forEach((Component c) -> LOGGER.debug(c.getDisplayId()));
            }
            
        });
        
        Set<URI> childUris = ws.getObjects(ComponentDefinition.class, ws.getContextIds()).stream()
                .flatMap(m -> m.getChildren().stream())
                .map(ASBIdentified::getIdentity).collect(Collectors.toSet());
        
        Set<URI> componentUris = ws.getObjects(ComponentDefinition.class, ws.getContextIds()).stream()
                .flatMap(m -> m.getComponents().stream())
                .map(ASBIdentified::getIdentity).collect(Collectors.toSet());

        assertTrue("Children of ComponentDefinitions should contain all Components", childUris.containsAll(componentUris));

         ws.getObjects(Component.class, ws.getContextIds()).stream().forEach(
            c -> {
                assertTrue("Component should have definition", c.getDefinition().get() != null);
                assertTrue("Component definition should be a ComponentDefinition", ws.containsObject(c.getDefinition().get().getIdentity(), ComponentDefinition.class, ws.getContextIds()));
            }
        );
    }

    @Test
    public void testSetNameDerivedFromDescription() {  

        //Arrays.asList(ws.getContextIds()).stream().map(u -> u.toASCIIString()).forEach(System.out::println);
        
/*        ws.getObjects(ModuleDefinition.class, ws.getContextIds()).stream().forEach(
            o -> {
                o.setName(o.getDisplayId() + "_newName");
                assertTrue("Name is not correct", o.getName().equals(o.getDisplayId() + "_newName"));

                URI derivedFrom = URI.create("http://www.synbad.org/anotherModuleDef");
                o.setWasDerivedFrom(derivedFrom);
                assertTrue("Derived from does not equals " + derivedFrom.toASCIIString(), o.getWasDerivedFrom().equals(derivedFrom));
                
                String original = o.getDescription();
                o.setDescription(original + "!");
                assertTrue("Description does not equals " + original + "!", o.getDescription().equals(original + "!"));
            
                SBValue v = SBValue.parseValue("Property Value");
                o.setValue("http://www.synbad.org/myproperty", v);
                assertTrue("Value for http://www.synbad.org/myproperty does not equal " + v, o.getValue("http://www.synbad.org/myproperty").equals(v));
            }     
        );*/
    }

    // ============================================================
    //                         Objects
    // ============================================================

    @Test
    public void testGetObject() {
        ws.getObjects(ModuleDefinition.class, ws.getContextIds()).stream().forEach(
            o -> {
                assert (ws.getObject(o.getIdentity(), ModuleDefinition.class, ws.getContextIds()) != null);
            }
        );
    }

    @Test
    public void testContainsObject() {

        for(SBObjectFactory<? extends SBValued> f : SBObjectFactoryManager.get().getFactories()) {

            ws.getObjects(f.getEntityClass(), ws.getContextIds()).stream().map(m -> (SBValued) m).forEach(
                    o -> ws.containsObject(o.getIdentity(), o.getClass(), ws.getContextIds())
            );
        }
    }
    
    // ============================================================
    //                          Edges
    // ============================================================

    @Test
    public void testGetEdges() {
        
        ModuleDefinition root = ws.getObject(md_receiverAndReporter.getIdentity(), ModuleDefinition.class, ws.getContextIds());

        for(SBObjectFactory<? extends SBValued> f : SBObjectFactoryManager.get().getFactories()) {

            ws.getObjects(f.getEntityClass(), ws.getContextIds()).stream()
                .map(object -> ws.outgoingEdges(object, ws.getContextIds()))
                .flatMap(edges -> edges.stream())
                .forEach(
                    edge -> { 
                        
                    }
            );
        }
    }
    
    @Test
    public void testGetEdgesWithPredicate() { 
        for(SBObjectFactory<? extends SBValued> f : SBObjectFactoryManager.get().getFactories()) {
            ws.getObjects(f.getEntityClass(), ws.getContextIds()).stream()
                .map(m -> (SBIdentified) m)
                .forEach(
                    o -> {
                        List<SynBadEdge> outgoing = ws.outgoingEdges(o, ws.getContextIds());
                        outgoing.stream().forEach(e-> { 
                            SBValued source = ws.getEdgeSource(e, ws.getContextIds());
                            SBValued target = ws.getEdgeTarget(e, ws.getContextIds());
                            assert(source.getIdentity().equals(o.getIdentity()));
                            assert(!target.getIdentity().equals(o.getIdentity()));
                        });
                        List<SynBadEdge> incoming = ws.incomingEdges(o, ws.getContextIds());
                        incoming.stream().forEach(e-> {
                            assert(!ws.getEdgeSource(e, ws.getContextIds()).getIdentity().equals(o.getIdentity()));
                            assert(ws.getEdgeTarget(e, ws.getContextIds()).getIdentity().equals(o.getIdentity()));
                        }); 
                    }
                );
        }
    }
    
    // ============================================================
    //                          Traversal
    // ============================================================
    
    @Test
    public void testCrawler() {
        ModuleDefinition system = ws.getObject(URI.create("http://www.synbad.org/md_ReceiverAndReporter/1.0"), ModuleDefinition.class, ws.getContextIds());
        SBWorkspaceGraph g = new SBWorkspaceGraph(ws, ws.getContextIds());
        SBCursor<SBValued, SynBadEdge> cursor = g.createCursor(system);
        DefaultSBCrawler<SBValued, SynBadEdge> crawler = new DefaultSBCrawler<>();
        crawler.addObserver(new SBCrawlerObserver.SBCrawlerObserverAdapter<SBValued, SynBadEdge, SBCursor<SBValued, SynBadEdge>>() {

            @Override
            public void onInstance(SBValued instance, SBCursor<SBValued, SynBadEdge> cursor) {

            }

            @Override
            public void onEdge(SynBadEdge edge) {

            }
            
        });

        crawler.run(cursor);
    }
    
    /**
     * Test of nodeSet method, of class SBWorkspaceGraph.
     */
    //@Test
    public void renderModel() {

        JFrame frame = new JFrame("Visualisation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
     
        LOGGER.debug("Creating view");
        
        SbolModel model = new  SbolModel(ws, def);
        SbolHView view = model.getMainView();
        
        SbolGraphStreamPanel panel = new SbolGraphStreamPanel();
        panel.setView(view);
        frame.getContentPane().add(panel, c);
        frame.pack();
        frame.setVisible(true);
        panel.run();
        
        LOGGER.debug("Adding updater");
        
        view.getTraversal().v(view.getRoot())
            .loop(p -> true, new DefaultSBHTraversal<>((SBHView)view).children(), true, false)
            .getResult().stream().forEach(i -> LOGGER.debug(i.toString()));
    }

    private void debugSynBadObject(SBIdentified o) {
        System.out.println("===============================================");
        System.out.println(o.getName());
        System.out.println("===============================================");
        System.out.println(o.getIdentity().toASCIIString());
        System.out.println(o.getPersistentId().toASCIIString());
        System.out.println(o.getDisplayId());
        System.out.println(o.getDescription());

        for(String p : o.getPredicates()) {
            for(SBValue v : o.getValues(p)) {
                System.out.println(" - " + p + " : " + v);
            }
        }
        
        ws.incomingEdges(o, ws.getContextIds()).stream().forEach(e -> System.out.println(" I: " + e.getEdge() + " from " + ws.getEdgeSource(e, ws.getContextIds())));
        ws.outgoingEdges(o, ws.getContextIds()).stream().forEach(e -> System.out.println(" O: " + e.getEdge() + " to " + ws.getEdgeTarget(e, ws.getContextIds())));
    }
}
