/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.tests;

import java.net.URI;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.sbol.object.Component;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.Module;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

/**
 *
 * @author owengilfellon
 */
public class WorkspaceTraversalTest  {
    
    private final static URI WORKSPACE_URI = UriHelper.synbad.namespacedUri("workspaceGraph");
    private final static SBWorkspaceManager MANAGER = Lookup.getDefault().lookup(SBWorkspaceManager.class);
    private final static Logger LOGGER = LoggerFactory.getLogger(WorkspaceTraversalTest.class);
    
    private SBWorkspace ws;
    private ModuleDefinition def;

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ws = MANAGER.getWorkspace(WORKSPACE_URI);
        def = ExampleFactory.getSubtilinReporter(ws);
    }
    
    @After
    public void tearDown() {
       MANAGER.closeWorkspace(WORKSPACE_URI);
       ws = null;
    }

    
    public void testTraversal() {
        
        SBWorkspaceGraph g = new SBWorkspaceGraph(ws, def.getContexts());
        Collection<SBIdentified> objects = ws.getObjects(SBIdentified.class, ws.getContextIds());
       
        SBTraversalResult<SBIdentified, SynBadEdge> r = g.getTraversal()
                .v(def)
                .e(SBDirection.OUT, SynBadTerms.SbolComponent.hasComponent)
                .v(SBDirection.OUT).getResult();
        
        LOGGER.debug("Found {} components of root:", r.streamVertexes().count());
        
        Set<Component> c = r.streamVertexes().filter(v -> Component.class.isAssignableFrom(v.getClass()))
                .map(v -> (Component)v).collect(Collectors.toSet());
        
        c.stream().map(v->v.toString()).forEach(LOGGER::debug);
            
        // ComponentDefinitions have Components
        
        assertTrue("ComponentDefinitions should have components", g.getTraversal().v(objects.toArray(new SBIdentified[]{})).has(SynBadTerms.Rdf.hasType, ComponentDefinition.TYPE)
                .e(SBDirection.OUT, SynBadTerms.SbolComponent.hasComponent)
                .v(SBDirection.IN).getResult().streamVertexes().count() > 0);
        
        // ModuleDefinitions have Modules
        
        assertTrue("ModuleDefinitions should have modules", g.getTraversal().v(objects.toArray(new SBIdentified[]{})).has(SynBadTerms.Rdf.hasType, ModuleDefinition.TYPE)
                .e(SBDirection.OUT, SynBadTerms.SbolModule.hasModule)
                .v(SBDirection.IN).getResult().streamVertexes().count() > 0);

        // Modules do not have Modules
        
        assertTrue("Modules should not have modules", g.getTraversal().v(objects.toArray(new SBIdentified[]{})).has(SynBadTerms.Rdf.hasType, Module.TYPE)
                .e(SBDirection.OUT, SynBadTerms.SbolModule.hasModule)
                .v(SBDirection.IN).getResult().streamVertexes().count() == 0);
        
    }

}
