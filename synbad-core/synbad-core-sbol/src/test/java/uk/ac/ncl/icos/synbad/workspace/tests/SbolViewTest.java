/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.tests;

import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.sbol.model.SbolModel;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.sbol.view.SbolHView;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentityException;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspace;

/**
 *
 * @author owengilfellon
 */
public class SbolViewTest  {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SbolViewTest.class);
    private static final URI WORKSPACE_URI = UriHelper.synbad.namespacedUri("sbolWorkspaceTest");

    private static final String PREFIX = "http://www.synbad.org";
    private static final String VERSION = "1.0";
    
    private SBWorkspace ws;
    private ModuleDefinition def;
    private SbolModel model;

    private static SBIdentity md_receiverAndReporter;
    private static SBIdentity md_subtilinReceiver;
    private static SBIdentity md_reporter;

    public SbolViewTest() {
        try {
            md_receiverAndReporter  = SBIdentity.getIdentity(URI.create(PREFIX + "/md_ReceiverAndReporter/" + VERSION));
            md_subtilinReceiver  = SBIdentity.getIdentity(URI.create(PREFIX + "/md_SubtilinReceiver/" + VERSION));
            md_reporter  = SBIdentity.getIdentity(URI.create(PREFIX + "/md_Reporter/" + VERSION));
        } catch (SBIdentityException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ws = new DefaultSBWorkspace(WORKSPACE_URI);
        def = ExampleFactory.getSubtilinReporter(ws);  
        model = new SbolModel(ws, def);
    }
    
    @After
    public void tearDown() {
        ws.shutdown();
       ws = null;
       def = null;
       model = null;
    }
    
    @Test
    public void testGetIdentities() {
        ModuleDefinition receiverAndReporter = ws.getObject(md_receiverAndReporter.getIdentity(), ModuleDefinition.class, ws.getContextIds());
        assert(model.getRootNode().equals(receiverAndReporter));
        SbolHView view = model.getCurrentView();
        assert(view != null);
        assert(model.getRootNode().equals(view.getRoot().getObject()));
        assert(!view.findObjects(ModuleDefinition.class).isEmpty());
        
    }
    

    private void debugSynBadObject(SBIdentified o) {
        System.out.println("===============================================");
        System.out.println(o.getName());
        System.out.println("===============================================");
        System.out.println(o.getIdentity().toASCIIString());
        System.out.println(o.getPersistentId().toASCIIString());
        System.out.println(o.getDisplayId());
        System.out.println(o.getDescription());

        for(String p : o.getPredicates()) {
            for(SBValue v : o.getValues(p)) {
                System.out.println(" - " + p + " : " + v);
            }
        }
        
        ws.incomingEdges(o, ws.getContextIds()).stream().forEach(e -> System.out.println(" I: " + e.getEdge() + " from " + ws.getEdgeSource(e, ws.getContextIds())));
        ws.outgoingEdges(o, ws.getContextIds()).stream().forEach(e -> System.out.println(" O: " + e.getEdge() + " to " + ws.getEdgeTarget(e, ws.getContextIds())));
    }
}
