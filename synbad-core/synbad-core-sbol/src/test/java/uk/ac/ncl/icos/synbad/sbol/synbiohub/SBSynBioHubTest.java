/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.sbol.synbiohub;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openide.util.Lookup;
import org.sbolstandard.core2.SBOLConversionException;
import org.sbolstandard.core2.SBOLReader;
import org.sbolstandard.core2.SBOLValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.synbiohub.frontend.IdentifiedMetadata;
import org.synbiohub.frontend.SynBioHubException;
import org.synbiohub.frontend.SynBioHubFrontend;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentRole;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.sbol.example.ExampleFactory;
import uk.ac.ncl.icos.synbad.sbol.io.SBSbolImporter;
import uk.ac.ncl.icos.synbad.sbol.object.ComponentDefinition;
import uk.ac.ncl.icos.synbad.sbol.object.ModuleDefinition;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owengilfellon
 */
public class SBSynBioHubTest {
    
    private static final URI WORKSPACE_URI = UriHelper.synbad.namespacedUri("sbolImportExport");
    private static final SBWorkspaceManager MANAGER = Lookup.getDefault().lookup(SBWorkspaceManager.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(SBSynBioHubTest.class);
    
    private ModuleDefinition def;
    private SBWorkspace ws;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ws = MANAGER.getWorkspace(WORKSPACE_URI);
        def = ExampleFactory.getSubtilinReporter(ws);
    }
    
    @After
    public void tearDown() {
       MANAGER.closeWorkspace(WORKSPACE_URI);
       ws = null;
       def = null;
    }
    
    private static String getUrlContents(String theUrl)
    {
    StringBuilder content = new StringBuilder();

    // many of these calls can throw exceptions, so i've just
    // wrapped them all in one try/catch statement.
    try
    {
      // create a url object
      URL url = new URL(theUrl);

      // create a urlconnection object
      URLConnection urlConnection = url.openConnection();

      // wrap the urlconnection in a bufferedreader
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

      String line;

      // read from the urlconnection via the bufferedreader
      while ((line = bufferedReader.readLine()) != null)
      {
        content.append(line + "\n");
      }
      bufferedReader.close();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    return content.toString();
  }
    
    
    /**
     * Test of doExport method, of class SBWorkspaceExporter.
     */
//    @Test
//    public void testToSystemOut() throws SBIdentityException, IOException {
//        new SBSbolExporter().export(System.out, ws, ws.getContextIds());
//        new SBJsonExporter().export(System.out, ws, ws.getContextIds());
//        new SBJsonFullExporter().export(System.out, ws, null);
//    }

    /**
     * Test of createEntity method, of class Workspace.
     */
    //@Test
    public void testSBOL() throws SBOLValidationException, SBOLConversionException, IOException {
      SynBioHubFrontend fe = new SynBioHubFrontend("https://synbiohub.org/");
        
        try {
            LOGGER.debug("CD: " + fe.getCount("ComponentDefinition"));
            ArrayList<IdentifiedMetadata> c = fe.getRootCollectionMetadata();
            for(IdentifiedMetadata md : c) {
                LOGGER.debug("\tC: {} | {}", md.getName(), md.getDescription());
            }
            
            ArrayList<IdentifiedMetadata> l = fe.getMatchingComponentDefinitionMetadata(
                    null, 
                    Collections.singleton(ComponentRole.RBS.getUri()), 
                    Collections.singleton(ComponentType.DNA.getUri()),
                    null, 0, 50);
            LOGGER.debug("CDS (DNA): " + l.size());
            for(IdentifiedMetadata md : l) {
                LOGGER.debug("\tCDS: {} | {}", md.getName(), md.getDescription());
                OutputStream f = new ByteArrayOutputStream();
                
                SBOLReader.setKeepGoing(true);
                String file = getUrlContents(md.getUri() + "/sbol");
                        
                InputStream is2 = new ByteArrayInputStream(file.getBytes());
//                ;
//                
//                SBOLWriter.write(//fe.getSBOL(URI.create(md.getUri())), f);
//                       SBOLReader.read(is2) , f);
//                ByteInputStream is = new  ByteInputStream(f.getBytes(), f.getCount());
                //ws.getRdfService().addRdf(is2, ws.getContextIds());
                
                SBSbolImporter importer = new SBSbolImporter();
                importer.importWs(is2, ws, new URI[]{URI.create("http://synbad.org/context")});
                ComponentDefinition d = ws.getObject(URI.create(md.getUri()), ComponentDefinition.class, ws.getContextIds());
                if(d!= null) {
                    //SBDebugger.printSbIdentified(d, System.out);
                }
                
            }
        } catch (SynBioHubException ex) {
            LOGGER.error("Could not get count", ex);
        }
      
    }
    /**
     * Test of x method, of class SBSynBioHub.
     */
   // @Test
    public void testX() {
       
    }
    
}
