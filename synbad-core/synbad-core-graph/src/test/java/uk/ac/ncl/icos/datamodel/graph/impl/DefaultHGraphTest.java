/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author owengilfellon
 */
public class DefaultHGraphTest {
    
    public DefaultHGraphTest() {
        
        
    }
    
       
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
/*
    private void printTree(SBHGraph<String, String> graph) {
        
        SBHIterator<String> i = graph.hierarchyIterator("root");
        StringBuilder sb = new StringBuilder();
        while(i.hasNext()) {

            String s = i.next();
            sb.append(graph.isVisible(s) ? "*" : " ");
            sb.append(i.getDepth());
            for(int n = 0; n < i.getDepth(); n++) {
                sb.append("\t");
                
            }
            sb.append(s);
            sb.append(" E{ ");
            graph.incomingEdges(s).stream().forEach(e ->
                sb.append(e));
            
            sb.append(" | ");
            graph.outgoingEdges(s).stream().forEach(e ->
                sb.append(e));
            sb.append(" }\n");
        }
        
        System.out.println(sb.toString());        
    }
    
    public SBHGraph<String, String> createGraph() {
        
        SBHGraph<String, String> graph = new DefaultHGraph<>("root");

        graph.addNode("1", "root");
            graph.addNode("1.1", "1");
            graph.addNode("1.2", "1");
                graph.addNode("1.2.1", "1.2");
                graph.addNode("1.2.2", "1.2");
            graph.addNode("1.3", "1");
            graph.addNode("1.4", "1");
        graph.addNode("2", "root");
            graph.addNode("2.1", "2");
            graph.addNode("2.2", "2");
            graph.addNode("2.3", "2");
            
        graph.addEdge("1.1", "1.2", "1.1 -> 1.2");
        graph.addEdge("1.2.1", "1.1", "1.2.1 -> 1.1");
        graph.addEdge("2", "1.2.2", "2 -> 1.2.2");
        
        return graph;
    }
    
    public SBHGraph<String, String> createGraph2() {
        
        SBHGraph<String, String> graph = new DefaultHGraph<>("root");

        graph.addNode("a", "root");
            graph.addNode("aa", "a");
            graph.addNode("ab", "a");
                graph.addNode("aba", "ab");
                graph.addNode("abb", "ab");
                    graph.addNode("abba", "abb");
                        graph.addNode("abbaa", "abba");
                        graph.addNode("abbab", "abba");
                        graph.addNode("abbac", "abba");
                    graph.addNode("abbb", "abb");
            graph.addNode("ac", "a");
            graph.addNode("ad", "a");
        graph.addNode("b", "root");
            graph.addNode("ba", "b");
            graph.addNode("bb", "b");
            graph.addNode("bc", "b");
            
        graph.addEdge("aa", "ab", "aa -> ab");
        graph.addEdge("aba", "aa", "aba -> aa");
        graph.addEdge("b", "abb", "b -> abb");
        
        return graph;
    }
    
    @Test
    public void testExpandAndContraction() {
        
       
        SBHGraph<String, String> graph = createGraph2(); 
        
        assert(graph.getDescendants("a").size() == 11);
        System.out.println("ab: " + graph.getDescendants("ab").size());

        assert(graph.getDescendants("ab").size() == 7);
        assert(graph.getDescendants("ac").isEmpty());
        
        assert(graph.getChildren("a").size() == 4);
        assert(graph.getChildren("b").size() == 3);
        assert(graph.getChildren("aa").isEmpty());
        assert(graph.getChildren("ab").size() == 2);
        assert(graph.getChildren("abb").size() == 2);
        assert(graph.getChildren("abba").size() == 3);
        assert(graph.getChildren("abbaa").isEmpty());
        
        graph.setParent("ac", "abb");

        assert(graph.getDescendants("a").size() == 11);
        assert(graph.getDescendants("ab").size() == 1);
        assert(graph.getDescendants("ac").size() == 6);
        
        assert(graph.getChildren("ab").size() == 1);
        assert(graph.getChildren("ac").size() == 1);
        
        graph.setParent("a", "abb");
        assert(graph.getChildren("ac").isEmpty());
        assert(graph.getChildren("a").size() == 5);
        
        
        assert(graph.getDescendants("a").size() == 11);
        assert(graph.getDescendants("ab").size() == 1);
        assert(graph.getDescendants("ac").isEmpty());
    }

    @Test
    public void testGetDescendantsWithContraction() {
        SBHGraph<String, String> graph = createGraph();

        int descendants = 11;
        
        assert(graph.getDescendants("root").size() == descendants);
        //printTree(graph);
        
        // removes 1.2.1 and 1.2.2
        
        graph.contract("1.2");
        //printTree(graph);
        assert(graph.getDescendants("root").size() == descendants - 2);
        assert(!graph.isVisible("1.2.1"));
        assert(!graph.isVisible("1.2.2"));
        
        
        // removes 1.1, 1.2, 1.3, 1.4

        graph.contract("1");
        assert(graph.getDescendants("root").size() == descendants - 6);
        assert(!graph.isVisible("1.2.1"));
        assert(!graph.isVisible("1.2.2"));
        assert(!graph.isVisible("1.1"));
        assert(!graph.isVisible("1.2"));
        assert(!graph.isVisible("1.3"));
        assert(!graph.isVisible("1.4"));
        //printTree(graph);
        
        graph.contract("2");
        assert(graph.getDescendants("root").size() == descendants - 9);
        //printTree(graph);        

        // no effect as 1 is still contracted
        
        graph.expand("2");
        assert(graph.getDescendants("root").size() == descendants - 6);
        //printTree(graph);        
        
        
        graph.expand("1.2");
        assert(graph.getDescendants("root").size() == descendants - 6);
        //printTree(graph);
        // adds all descendants of 1, including the previously expanded 1.2
        
        graph.expand("1");
        assert(graph.getDescendants("root").size() == descendants);
        //printTree(graph);
    }
 */   
}
