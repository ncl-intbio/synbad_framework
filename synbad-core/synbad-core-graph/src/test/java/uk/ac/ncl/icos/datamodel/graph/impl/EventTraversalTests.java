/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;


import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.event.DefaultSBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.event.pipes.DefaultSBEventPipeline;
import uk.ac.ncl.icos.synbad.event.pipes.DefaultSBEventPipelineSubscriberSource;
import uk.ac.ncl.icos.synbad.event.pipes.SBEventPipeline;

/**
 *
 * @author owengilfellon
 */
public class EventTraversalTests {

    private static Logger LOGGER = LoggerFactory.getLogger(EventTraversalTests.class);
    Collection<SBEvent> events = Arrays.asList(
        
    );


    public EventTraversalTests() { }
    
       
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    SBEventFilter getEvenFilter() {
        return (SBEvent event) -> {
            int i = Integer.parseInt(event.getData().toString());
            return i % 2 == 0;
        };
    }
    
    SBEventFilter getFilter(String... data) {
        return (SBEvent event) -> Stream.of(data).anyMatch(s -> event.getData().equals(s));
    }
    
    SBEventPipeline<SBEvent, SBEvent> getLabeller(String label) {
        return new DefaultSBEventPipeline<SBEvent, SBEvent>().as(label);
    }
    
    

    @Test
    public void testPulling() {
        
        SBEventPipeline<SBEvent, SBEvent> pipeline = 
            new DefaultSBEventPipelineSubscriberSource().push(
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "A"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "B"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "C"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "D"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "E"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "F"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "G"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "H"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "I"),
                new DefaultSBEvent<>(SBGraphEventType.ADDED, "J"));
        
        pipeline
            .doTraversal(getFilter("A", "B"), getLabeller("1"))
            .doTraversal(getFilter("C", "D"), getLabeller("2"))
            .doTraversal(getFilter("A", "C"), getLabeller("3"))
            .hasLabel("3")
            .subscribe(e -> LOGGER.trace(e.toString()))
            .forEachRemaining(e-> LOGGER.debug(e.toString()));
    }
    
    @Test
    public void testPushing() throws InterruptedException {
  
        SBDispatcher dispatcher = new DefaultSBDispatcher();

        SBEventPipeline<SBEvent, SBEvent> pipeline = 
            new DefaultSBEventPipelineSubscriberSource()
                .subscribe(dispatcher)
                .doTraversal(getFilter("1", "3", "5", "7", "9"), getLabeller("ODD"))
                .doTraversal(getEvenFilter(), getLabeller("EVEN"))
                .hasLabel("EVEN")
                .subscribe(e-> LOGGER.debug(e.toString()));

        for(int i = 0; i < 20; i++) {
            dispatcher.publish(new DefaultSBEvent<>(SBGraphEventType.ADDED, i + ""));
        }
    }
}
