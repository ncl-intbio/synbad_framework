/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.datamodel.graph.impl;


import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultSBGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import org.junit.*;

/**
 *
 * @author owengilfellon
 */
public class GraphTraversalTests {

    SBGraph<SBValued, SBEdge<SBValued, URI>> graph;
    SBValued a = new MockSBValued(URI.create("http://www.synbad.org/a"));
    SBValued b = new MockSBValued(URI.create("http://www.synbad.org/b"));
    SBValued c = new MockSBValued(URI.create("http://www.synbad.org/c"));
    SBValued d = new MockSBValued(URI.create("http://www.synbad.org/d"));
    SBValued e = new MockSBValued(URI.create("http://www.synbad.org/e"));
    SBValued f = new MockSBValued(URI.create("http://www.synbad.org/f"));
    SBValued g = new MockSBValued(URI.create("http://www.synbad.org/g"));
    SBValued h = new MockSBValued(URI.create("http://www.synbad.org/h"));

    URI pred1 = URI.create("http://www.synbad.org/predicate1");
    URI pred2 = URI.create("http://www.synbad.org/predicate2");

    public GraphTraversalTests() { }
    
       
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

        DefaultSBGraph<SBValued, SBEdge<SBValued, URI>> graph = new DefaultSBGraph<>();

        graph.addNode(a);
        graph.addNode(b);
        graph.addNode(c);
        graph.addNode(d);
        graph.addNode(e);
        graph.addNode(f);
        graph.addNode(g);
        graph.addNode(h);


        graph.addEdge(a, b, new SBEdge.DefaultEdge<>(pred1, a, b));
        graph.addEdge(b, c, new SBEdge.DefaultEdge<>(pred1, b, c));
        graph.addEdge(b, d, new SBEdge.DefaultEdge<>(pred2, b, d));
        graph.addEdge(c, e, new SBEdge.DefaultEdge<>(pred1, c, e));
        graph.addEdge(d, e, new SBEdge.DefaultEdge<>(pred1, d, e));
        graph.addEdge(e, f, new SBEdge.DefaultEdge<>(pred1, e, f));
        graph.addEdge(f, g, new SBEdge.DefaultEdge<>(pred1, f, g));
        graph.addEdge(f, h, new SBEdge.DefaultEdge<>(pred2, f, h));
        graph.addEdge(g, a, new SBEdge.DefaultEdge<>(pred1, g, a));
        graph.addEdge(h, a, new SBEdge.DefaultEdge<>(pred1, h, a));
        this.graph = graph;
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testTraversal() {

        SBTraversalResult<SBValued, SBEdge<SBValued, URI>> r1 = graph.getTraversal().v(a).as("A")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("B").getResult();

        Assert.assertEquals(1, r1.getTraverserCount());
        Assert.assertEquals(1, r1.getMatches().size());

        SBTraversalResult<SBValued, SBEdge<SBValued, URI>> r2 = graph.getTraversal().v(a).as("A")
            .e(SBDirection.OUT).v(SBDirection.OUT).as("B")
            .e(SBDirection.OUT, pred1.toASCIIString()).v(SBDirection.OUT).as("C")
            .e(SBDirection.OUT).v(SBDirection.OUT).as("E")
            .e(SBDirection.OUT).v(SBDirection.OUT).as("F").getResult();

        Assert.assertEquals("No branch should result in one path and therefore one match",1, r2.getMatches().size());
        Assert.assertEquals("No branch should result in one path and therefore one match",1, r2.getTraverserCount());

        SBTraversalResult<SBValued, SBEdge<SBValued, URI>> r3 = graph.getTraversal().v(a).as("A")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("B")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("CD")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("E")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("F").getResult();

         Assert.assertEquals("One branch should result in two paths and therefore two matches", 2, r3.getMatches().size());
        Assert.assertEquals("One branch should result in two paths and therefore two matches", 2, r3.getTraverserCount());

        SBTraversalResult<SBValued, SBEdge<SBValued, URI>> r4 = graph.getTraversal().v(a).as("A")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("B")
                .e(SBDirection.OUT, pred1.toASCIIString()).v(SBDirection.OUT).as("C")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("E")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("F")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("GH").getResult();

        r4.getMatches();
        Assert.assertEquals("One branch should result in two paths and therefore two matches",2, r4.getMatches().size());
        Assert.assertEquals("One branch should result in two paths and therefore two matches",2, r4.getTraverserCount());

        SBTraversalResult<SBValued, SBEdge<SBValued, URI>> r5 = graph.getTraversal().v(a).as("A")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("B")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("CD")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("E")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("F")
                .e(SBDirection.OUT).v(SBDirection.OUT).as("GH")
                .e(SBDirection.OUT).v(SBDirection.OUT).getResult();

        Assert.assertEquals("Two branches should result in four paths and therefore four matches", 4, r5.getMatches().size());
        Assert.assertEquals("Two branches should result in four paths and therefore four matches",4, r5.getTraverserCount());
//        Assert.assertTrue("Should have visited all vertices and edges", graphEquals(graph, r5.toGraph()));
//        r5.getMatches().forEach(sg -> {
//            Assert.assertEquals("Subgraphs should all have the same number of nodes", 6, sg.nodeSet().size());
//        });
    }


//    public boolean graphEquals(SBGraph g1, SBGraph g2) {
//    
//        return g1.nodeSet().containsAll(g2.nodeSet())  && g2.nodeSet().containsAll(g1.nodeSet()) &&
//                g1.edgeSet().containsAll(g2.edgeSet()) && g2.edgeSet().containsAll(g1.edgeSet());
//    }

    class MockSBValued implements SBValued {

        private final URI identity;

        MockSBValued(URI identity) {
            this.identity = identity;
        }

        @Override
        public String toString() {
            return identity.getPath().replaceAll("/", "");
        }

        @Override
        public URI getType() {
            return URI.create("http://www.synbad.org/mockSbValued");
        }

        @Override
        public URI[] getContexts() {
            return new URI[] { URI.create("http://www.synbad.org/context") };
        }

        @Override
        public SBWorkspace getWorkspace() {
            return null;
        }

        @Override
        public URI getIdentity() {
            return identity;
        }

        @Override
        public Set<String> getPredicates() {
            return Collections.EMPTY_SET;
        }

        @Override
        public SBValue getValue(String predicate) {
            return null;
        }

        @Override
        public Collection<SBValue> getValues(String predicate) {
            return Collections.EMPTY_SET;
        }

        @Override
        public <T> Collection<T> getValues(String predicate, Class<T> clazz) {
            return Collections.EMPTY_SET;
        }

       /* @Override
        public void setValue(String predicate, SBValue v) {

        }

        @Override
        public void addValue(String predicate, SBValue v) {

        }

        @Override
        public void removeValues(String predicate) {

        }

        @Override
        public void removeValue(String predicate, SBValue v) {

        }*/

        @Override
        public boolean is(String rdfType) {
            return rdfType.equals(getType().toASCIIString());
        }

        

        @Override
        public boolean equals(Object obj) {
             if(obj == null)
                 return false;
             if(obj == this)
                 return true;
             if(!(obj instanceof MockSBValued))
                 return false;
             return identity.equals(((MockSBValued)obj).identity);
        }

        @Override
        public int hashCode() {
            return identity.hashCode();
        }

        @Override
        public <T extends SBValued> Optional<T> as(Class<T> clazz) {
            return Optional.empty();
        }
    }




}
