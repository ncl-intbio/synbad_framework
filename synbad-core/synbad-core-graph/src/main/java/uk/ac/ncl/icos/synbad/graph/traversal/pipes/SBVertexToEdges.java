/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBLocalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBVertexToEdges<T extends SBValued> extends DefaultSBTraversalPipe<T, SBEdge>{

    private static final Logger logger =  LoggerFactory.getLogger(SBVertexToEdges.class);
    
    private final SBDirection direction;
    private final List<String> predicates;
    
    public SBVertexToEdges(SBTraversal traversal, SBDirection direction, String... edgePredicates) {
        super(traversal);
        this.predicates = Arrays.asList(edgePredicates);
        this.direction = direction;
    }
    
    protected SBVertexToEdges(SBTraversal traversal, SBVertexToEdges<T> vertexToEdges) {
        super(traversal, vertexToEdges);
        this.direction = vertexToEdges.direction;
        this.predicates = vertexToEdges.predicates.stream().collect(Collectors.toList());
    }
    
    @Override
    public Collection<SBTraverser<SBEdge>> processTraverser(SBTraverser<T> traverser) {
        if(traverser.get() == null) {
           logger.error("Traverser object is null, traversing {}: {}", direction, predicates.stream().reduce("", (s1, s2) -> s1 + ", " +  s2));
           return Collections.EMPTY_SET;
        }

        //assert(SBValued.class.isAssignableFrom(traverser.get().getClass()));
        
       // logger.trace("Processing " + traverser);
        
        Set<SBTraverser<SBEdge>> processed =new HashSet<>();
        
        SBLocalEdgeProvider<SBValued, SBEdge> view = ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();
        Stream<? extends SBEdge> edges = null;
        
        // if no direction is provided, then we use all edges
        
        if(direction == null) {
            //System.out.println("Direction is null");
            edges = Stream.concat(
                    view.incomingEdges(traverser.get()).stream(),
                    view.outgoingEdges(traverser.get()).stream());
        }
        else {
            switch(direction) {
                case IN : edges = view.incomingEdges(traverser.get()).stream();
                    break;
                case OUT : edges = view.outgoingEdges(traverser.get()).stream();
                    break;
            }
        }
        
        Set<SBEdge> edgeSet = null;
        if(edges!=null)
            edgeSet = edges.map(e -> (SBEdge)e).collect(Collectors.toSet());       
        

        if(edgeSet!=null) {
            
            // If predicates are specified, filter out non-matching edges
            
            Set<SBEdge> filtered = edgeSet.stream()
                    .filter(e -> predicates.isEmpty() || predicates.contains(((SBEdge<URI, URI>)e).getEdge().toASCIIString()))
                    .map(e -> (SBEdge) e).collect(Collectors.toSet());

            // if there is only one matching edge, move to edge
            
            //logger.trace("Filtered {} out of {} edges", edgeSet.size() - filtered.size(), edgeSet.size());
            
            if (!filtered.isEmpty() && filtered.size() == 1)
                processed.add(traverser.push(filtered.iterator().next()));
            
            // if there is more than one match, duplicate a traverser for each.
            
            else if (!filtered.isEmpty()){
                Iterator<SBEdge> i = filtered.iterator();
                while(i.hasNext()) {
                    processed.add(traverser.duplicate().push(i.next()));
                }
                
            }
        } else {
           // logger.trace("No edges to process");
        }
        
        //processed.stream().forEach(p -> logger.trace("Returning: [ {} ]", p));
        
        return processed;

    }

    @Override
    public SBTraversalPipe<T, SBEdge> copy(SBTraversal parentTraversal) {
        return new SBVertexToEdges<>(parentTraversal, this);
    }

}
