/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.filter;

import java.net.URI;
import java.util.Objects;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithParent;

/**
 *
 * @author owen
 */
public class SBFilterConstructor {
    
    private Class parentClazz = null;
    private URI parentIdentity = null;
    private Class childClazz = null;
    private URI childIdentity = null;
    private Class eventClazz = null;

    public SBFilterConstructor setChildClazz(Class childClazz) {
        this.childClazz = childClazz;
        return this;
    }

    public SBFilterConstructor setChildIdentity(URI childIdentity) {
        this.childIdentity = childIdentity;
        return this;
    }

    /**
     *
     * @param parentClazz
     * @return true if the parentClazz is assignable from the event's parent class
     */
    public SBFilterConstructor setParentClazz(Class parentClazz) {
        this.parentClazz = parentClazz;
        return this;
    }

    public SBFilterConstructor setParentIdentity(URI parentIdentity) {
        this.parentIdentity = parentIdentity;
        return this;
    }

    /**
     *
     * @param eventClazz
     * @return true if the eventClazz is assignable from the event class
     */
    public SBFilterConstructor setEventClazz(Class eventClazz) {
        this.eventClazz = eventClazz;
        return this;
    }

    public SBEventFilter build() {
        return new SBConstructedFilter(parentClazz, parentIdentity, childClazz, childIdentity, eventClazz);
    }
    
    class SBConstructedFilter implements SBEventFilter {
     
        private final Class parentClazz;
        private final URI parentIdentity;
        private final Class childClazz;
        private final URI childIdentity;
        private final Class eventClazz;

         public SBConstructedFilter(Class parentClazz, URI parentIdentity, Class childClazz, URI childIdentity, Class eventClazz) {
            this.parentClazz = parentClazz;
            this.parentIdentity = parentIdentity;
            this.childClazz = childClazz;
            this.childIdentity = childIdentity;
            this.eventClazz = eventClazz;
        }

        @Override
        public boolean test(SBEvent event) {

            if(parentIdentity != null || parentClazz != null) {
                if(!SBEvtWithParent.class.isAssignableFrom(event.getClass()))
                    return false;

                SBEvtWithParent evt = (SBEvtWithParent)event;

                if(evt.getParentClass() != null && parentClazz != null) {
                    if(!parentClazz.isAssignableFrom(evt.getParentClass()))
                        return false;
                }

                if(evt.getParent() != null && parentIdentity != null) {
                    if(!parentIdentity.equals(evt.getParent()))
                        return false;
                }   
            }

            if(childClazz  != null) {
                if(!childClazz.isAssignableFrom(event.getDataClass()))
                    return false;
            }

            if(childIdentity != null) {
                if(!URI.class.isAssignableFrom(event.getDataClass()) || !childIdentity.equals(event.getData()))
                    return false;
            }

            if(eventClazz  != null) {
                if(!eventClazz.isAssignableFrom(event.getClass()))
                    return false;
            }

            return true;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;

            if (obj == this)
                return true;

            if (!(obj instanceof SBConstructedFilter))
                return false;

            SBConstructedFilter r = (SBConstructedFilter) obj;

            if( (childClazz != null && !childClazz.equals(r.childClazz)) ||
                    (childClazz == null && r.childClazz != null))
                return false;

            if((childIdentity != null && !childIdentity.equals(r.childIdentity)) ||
                   (childIdentity == null &&  r.childIdentity != null))
                return false;

            if((parentClazz != null && !parentClazz.equals(r.parentClazz)) ||
                    (parentClazz == null && r.parentClazz != null))
                return false;

            if((parentIdentity != null && !parentIdentity.equals(r.parentIdentity)) ||
                    (parentIdentity == null && r.parentIdentity != null ))
                return false;

            if((eventClazz != null && !eventClazz.equals(r.eventClazz)) || 
                    (eventClazz == null && r.eventClazz != null))
                return false;

            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            if(this.parentClazz != null)
                hash = 37 * hash + Objects.hashCode(this.parentClazz);
            if(this.parentIdentity != null)
                hash = 37 * hash + Objects.hashCode(this.parentIdentity);
            if(this.childClazz != null)
                hash = 37 * hash + Objects.hashCode(this.childClazz);
            if(this.childIdentity != null) 
                hash = 37 * hash + Objects.hashCode(this.childIdentity);
            if(this.eventClazz != null)
                hash = 37 * hash + Objects.hashCode(this.eventClazz);
            return hash;
        }
    }
}


