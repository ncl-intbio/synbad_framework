/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;


import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBCrawler<N extends SBValued, E extends SBEdge, C extends SBCursor<N, E>> extends SBCrawlerObserver<N, E, C> {
    
    public void addObserver(SBCrawlerObserver<N,E,C> listener);
    
    public void removeObserver(SBCrawlerObserver<N,E,C> listener);
    
    public void run(C cursor);

    void processNode(C cursor, Set<E> seenEdges);
    
    void processEdge(C cursor, Set<E> seenEdges, E edge);
    
    public abstract class SBCrawlerAdapter<N extends SBValued, E extends SBEdge, C extends SBCursor<N, E>> implements SBCrawler<N, E, C> {
        
        private static final Logger logger = LoggerFactory.getLogger(SBCrawlerAdapter.class.getSimpleName());
        private final Set<SBCrawlerObserver<N, E, C>> listeners = new HashSet<>();
    

        @Override
        public void addObserver(SBCrawlerObserver<N, E, C> listener) {
            listeners.add(listener);
            logger.debug("Added listener: [ {} ]", listener);
        }

        @Override
        public void removeObserver(SBCrawlerObserver<N, E, C> listener) {
            listeners.remove(listener);
            logger.debug("Removed listener: [ {} ]", listener);
        }

        @Override
        public void onEdge(E edge) {
            
            logger.debug("On edge: [ {} ]", edge);
            
            for(SBCrawlerObserver<N, E, C> listener : listeners) {
                listener.onEdge(edge);
            }
        }

        @Override
        public void onEnded() {
            logger.debug("On ended");
            for(SBCrawlerObserver<N, E, C> listener : listeners) {
                listener.onEnded();
            }
        }

        @Override
        public void onInstance(N instance, C cursor) {
            logger.debug("On instance: [ {} ]", instance);
            for(SBCrawlerObserver<N, E, C> listener : listeners) {
                listener.onInstance(instance, cursor);
            }
        }

        @Override
        public boolean terminate() {

            boolean terminate = false;

            for(SBCrawlerObserver<N, E, C> listener : listeners) {
                if(listener.terminate())
                    terminate = true;
            }

            return terminate;
        }

        @Override
        public boolean vetoEdge(E edge) {

            boolean veto = false;

            for(SBCrawlerObserver<N, E, C> listener : listeners) {
                if(listener.vetoEdge(edge)) {
                    logger.debug("Vetoing edge: [ {} ]", edge);
                    veto = true;
                }
                    
            }

            return veto;
        }
        
    }

}
