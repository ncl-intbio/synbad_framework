/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.cursor;

import java.util.Collection;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 * A cursor maintains a stack of positions that the cursor has held within a
 * graph.
 * @author owengilfellon
 * @param <N> The base type of objects within the graph.
 * @param <E> The base type of edges within the graph.
 */
public interface SBCursor<N, E> {

    /**
     * 
     * @return the list of objects that this cursor has been positioned at.
     */
    public Collection<N> getPath();

    /**
     * 
     * @return the current object that this cursor is positioned at.
     */
    public N getCurrentPosition();

    /**
     * 
     * @return removes and returns the current object this cursor is positioned
     * at, returning the cursor to it's previous position if there is one available.
     */
    public N pop();

    /**
     * Clears the current path, and places the cursor at any object that exists 
     * within the graph.
     * @param instance 
     */
    public void placeAt(N instance);

    /**
     * Places the cursor at any object that exists within the graph, preserving
     * the current path.
     * @param instance 
     */
    public void moveTo(N instance);

    /**
     * Moves the cursor to an adjacent object via the provided edge. Throws an
     * exception if the provided edge is not incident to the current object.
     * @param edge 
     * @throws IllegalArgumentException if the provided edge is not incident to
     * the current object.
     */
    public void moveVia(E edge);

    /**
     * 
     * @param direction
     * @return all edges incident to the current object in the provided direction.
     * If the direction is OUT, returns all edges of which this node is the source.
     * If the direction is IN, returns all edges of which this node is the target.
     */
    public Collection<E> getEdges(SBDirection direction);
    
    /**
     * 
     * @param edge
     * @return the source of the provided edge.
     */
    public N getEdgeSource(E edge);
    
    /**
     * 
     * @param edge
     * @return the target of the provided edge.
     */
    public N getEdgeTarget(E edge);
    
    
}
