/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;

/**
 * A SBTraversalSource initiates a traversal from an initial list of nodes
 * or edges.
 * 
 * 
 * @author owengilfellon
 */
public interface SBHTraversalSource<N, E,  G extends SBHGraph> extends SBGraphTraversalSource<N, E, G>{
    
    
    /**
     * A traversal beginning at the provided objects.

     * @param objs
     * @return 
     */
    public SBHTraversal<N, N, G> v(N... objs);
    
    /**
     * A traversal beginning at the provided edges.

     * @param edges
     * @return 
     */
    public SBHTraversal<E, E, G> e(E... edges);
    
    
    
    
  
}
