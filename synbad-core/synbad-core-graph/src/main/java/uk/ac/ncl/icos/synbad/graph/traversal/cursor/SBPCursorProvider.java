/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.cursor;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBPCursorProvider<N, P, E> extends SBHCursorProvider<N, E> {
    
    @Override
    SBPCursor<N, P, E> createCursor(N startNode);
    
}
