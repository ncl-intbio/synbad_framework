/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owen
 */
public interface WritablePGraph<
        N,
        P,
        E> extends SBPGraph<N, P, E> {
    
    // mutator
    
    public boolean addNode(N instance, N parent);

    public boolean addNode(N instance, N parent, int index);
    
    public boolean removeNode(N node);

    public boolean removeAllNodes(Collection<? extends N> nodes);
    
    
    public boolean addEdge(N from, N to, E edge);
    
    public boolean addEdgeByPort(P from, P to, E edge);
    
    /**
     * Removes non-proxy edges. Proxy edges are calculated automatically.
     *
     * @param edge
     * @return
     */
    public boolean removeEdge(E edge);
    
    public boolean removeAllEdges(Collection<? extends E> edges);

    public E removeEdge(N sourceNode, N targetNode);
    
    public Set<E> removeAllEdges(N sourceNode, N targetNode);
    
     public boolean addPort(P portNode, N ownerNode, SBPortDirection direction, Set<String> constraints);

    public boolean removePort(P portNode, N ownerNode);
    
    public boolean setProxy(P proxy, P proxied);
    
    public boolean removeProxy(P proxy, P proxied);
}
