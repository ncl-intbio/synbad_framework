/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraverser;
import java.util.Arrays;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;
import uk.ac.ncl.icos.synbad.graph.SBPGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPTraversalSource;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBPTraversalSource<N, P, E, G extends SBPGraph<N, P, E>> implements SBPTraversalSource<N, P,E,  G> {

    private final G graph;

    public DefaultSBPTraversalSource(G graph) {
        this.graph = graph;
    }

    @Override
    public SBPTraversal<N, N, G> v(N... objs) {

        DefaultSBPTraversal<N, N, G> trav = new DefaultSBPTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(objs)
                //.filter(n -> !graph.findNodes(n.getData().getIdentity()).isEmpty())
            .map(n -> (SBTraverser<N>)new DefaultSBTraverser<>(n)).iterator());
        return trav;

    }
    
    @Override
    public SBPTraversal<P, P, G> p(P... ports) {

        DefaultSBPTraversal<P, P, G> trav = new DefaultSBPTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(ports)
                //.filter(n -> !graph.findNodes(n.getData().getIdentity()).isEmpty())
            .map(n -> (SBTraverser<P>)new DefaultSBTraverser<>(n)).iterator());
        
        return trav;

    }

    @Override
    public SBPTraversal<E, E, G> e(E... edges) {

       DefaultSBPTraversal<E, E, G> trav = new DefaultSBPTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(edges)
                .map(e -> (SBTraverser<E>)new DefaultSBTraverser<>(e)).iterator());
        
        return trav;

    }
    
    
}
