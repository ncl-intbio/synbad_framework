/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.api.domain.SBEntity;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBTraverser<N> implements SBTraverser<N> {
        
    protected final Map<String, Set<SBTraverser.SBTraverserPosition>> labelledEntities;
    protected final Stack<SBTraverser.SBTraverserPosition> positionStack;

    public DefaultSBTraverser(N start) {
        positionStack = new Stack<>();
        labelledEntities = new HashMap<>();
        positionStack.push(new SBTraverser.SBTraverserPosition(start));
    }

    public DefaultSBTraverser(Collection<SBTraverser.SBTraverserPosition> stack, Map<String, Set<SBTraverser.SBTraverserPosition>> labelledEntities) {
        positionStack = new Stack<>();
        positionStack.addAll(stack);
        this.labelledEntities = labelledEntities;
    }

    @Override
    public Collection<SBTraverser.SBTraverserPosition> getPath() {
        return positionStack.stream().collect(Collectors.toList());
    }

    @Override
    public N pop() {
        return (N)positionStack.pop().getEntity();
    }
    
    @Override
    public N get() {
        return (N)positionStack.peek().getEntity();
    }

    @Override
    public <E> SBTraverser<E> push(E instance) {
        positionStack.push(new SBTraverser.SBTraverserPosition(instance));
        return (SBTraverser<E>) this;
    }

    @Override
    public Set<String> getLabels() {
        return positionStack.peek().getLabels();
    }

    @Override
    public Collection<?> getByLabel(String label) {
        return !labelledEntities.containsKey(label) ? new HashSet<>() : labelledEntities.get(label).stream().map(c -> c.getEntity()).collect(Collectors.toSet());
    }

    @Override
    public void addLabel(String label) {
        SBTraverser.SBTraverserPosition<? extends SBEntity> pos = positionStack.peek();
        pos.addLabel(label);
        if(!labelledEntities.containsKey(label))
            labelledEntities.put(label, new HashSet<>());
        labelledEntities.get(label).add(pos);

    }

    @Override
    public SBTraverser<N> duplicate() {
        Stack<SBTraverser.SBTraverserPosition> newStack = new Stack<>();
        newStack.addAll(positionStack.stream().map(cp -> new SBTraverser.SBTraverserPosition(cp.getEntity(), cp.getLabels())).collect(Collectors.toList()));
        Map<String, Set<SBTraverser.SBTraverserPosition>> newlabelledEntities = new HashMap<>();
        for(String key : labelledEntities.keySet()) {
            newlabelledEntities.put(key, new HashSet<>());
            for(SBTraverser.SBTraverserPosition c : labelledEntities.get(key)) {
                newlabelledEntities.get(key).add(c);
            }
        }

        return new DefaultSBTraverser<>(newStack, newlabelledEntities);
    }

  /*  @Override
    public String toString() {
        if(get() != null) 
            return "Traverser[".concat(get().toString()).concat("]");
        else
            return "Traverser[NONE]";
    }*/
}