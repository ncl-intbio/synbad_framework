/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.tree;

import java.io.Serializable;
import java.util.List;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;


/**
 *
 * @author owengilfellon
 */
public interface ITreeNode<T> extends Serializable {
    
    public ITree<T> getTree();
    
    public void setData(T data);
    public T getData();

    public void setParent(ITreeNode<T> parent);
    public ITreeNode<T> getParent();
    
    public boolean setChildren(List<ITreeNode<T>> children);
    public boolean addChild(ITreeNode<T> child);
    public void addChild(int index, ITreeNode<T> child);
    public List<ITreeNode<T>> getChildren();
    
    public boolean hasSiblings();
    
    public int getHeight();
    public int getDepth();

    public ITreeNode<T> duplicate();
    
    public void alert(SBEvent e);
    

}
