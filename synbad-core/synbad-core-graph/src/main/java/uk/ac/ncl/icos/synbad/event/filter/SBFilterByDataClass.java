/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.filter;

import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import java.util.Collection;
import java.util.HashSet;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;

/**
 *
 * @author Owen
 */
 public class SBFilterByDataClass implements SBEventFilter
 {

    private final Collection<Class> clazz;

    public SBFilterByDataClass(Class clazz) {
        this.clazz = new HashSet<>();
        this.clazz.add(clazz);
    }

    public SBFilterByDataClass(Collection<Class> clazz) {
        this.clazz = new HashSet<>(clazz);
    }

    @Override
    public boolean test(SBEvent event) {
        return clazz.stream().anyMatch(c -> c.isAssignableFrom(event.getDataClass()));
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (obj == this)
            return true;

        if (!(obj instanceof SBFilterByDataClass))
            return false;

       SBFilterByDataClass r = (SBFilterByDataClass) obj;

        return clazz.containsAll(r.clazz) && r.clazz.size() == clazz.size();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash;
        hash = clazz.stream().map((c) -> c.hashCode()).reduce(hash, Integer::sum);
        return hash;
    }
    
    
}
