/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.Map;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursorProvider;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPTraversalSource;

/**
 *
 * @author owengilfellon
 */
public interface SBPGraph<N, P, E> extends SBHGraph<N, E>, SBPCursorProvider<N, P, E> {

    boolean containsPort(P port);
    
    boolean isProxied(P port);
    
    P getProxy(P port);

    boolean isProxy(P port);
    
    P getProxied(P port);
    
    Set<P> portSet();
    
    Set<P> getPorts(N node);

    N getPortOwner(P port);
    
    N getEdgeOwner(E edge);

    boolean containsEdgeByPort(P from, P to);

    Set<E> getAllEdgesByPort(P port);

    Set<E> getAllEdgesByPort(P sourcePort, P targetPort);

    P getEdgeSourcePort(E edge);

    P getEdgeTargetPort(E edge);

    Map<P, P> getConnectablePorts(N from, N to);

    boolean hasLowestCommonAncestor(N from, N to);
    
    @Override
    SBPTraversalSource<N, P, E, ? extends SBPGraph> getTraversal();
}
