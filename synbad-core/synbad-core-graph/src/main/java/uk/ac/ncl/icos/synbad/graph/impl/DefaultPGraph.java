/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.impl;

import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBPCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPTraversalSource;
import java.io.Serializable;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHIterator;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBPTraversalSource;
import uk.ac.ncl.icos.synbad.api.event.SBEvtWithParent;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.SBGlobalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGlobalNodeProvider;
import uk.ac.ncl.icos.synbad.graph.SBPGraph;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.graph.WritablePGraph;

/**
 * @author owengilfellon
 * @param <N> Vertex base class
 * @param <E> Edge base class
 * @param <P> Port base class
 */
public class DefaultPGraph<N, P, E> implements WritablePGraph<N, P, E>, SBGlobalEdgeProvider<N, E>, SBGlobalNodeProvider<N>, SBSubscriber {

    private static final long serialVersionUID = -2896802789663470746L;
    private final Map<E, WireRecord> wireRecords;
    private final Map<P, PortRecord> portRecords;
    private final Map<N, Set<P>> danglingProxies;
    
    private final SBDispatcher dispatcher;
    
    private final DefaultHGraph<N, E> graph;
    
    private static final Logger LOGGER =  LoggerFactory.getLogger(DefaultPGraph.class);

    public DefaultPGraph(N root) {
        graph = new DefaultHGraph<>(root);
        this.portRecords = new HashMap<>();
        this.danglingProxies = new HashMap<>();
        this.dispatcher = new DefaultSBDispatcher();
        
        // we proxy the events related to contracting / expanding nodes
        
        graph.subscribe(new SBEventFilter.DefaultFilter(), this);
        this.wireRecords = new HashMap<>();
    }
    
    
     private String getPrettyName(Object node) {
        if (SBIdentified.class.isAssignableFrom(node.getClass()))
          return ((SBIdentified)node).getDisplayId();
        else if (URI.class.isAssignableFrom(node.getClass()))
            return ((URI)node).getFragment();
        else
            return node.toString();
    }
    

    // ============================================================
    //                          Ports
    // ============================================================
    
    private void registerPort(N owner, P port, SBPortDirection direction, Set<String> portConstraints) {
        if(!portRecords.containsKey(port)) {
            PortRecord pr = new PortRecord(owner, port, direction);
            portConstraints.stream().forEach(pr::addConstraint);
            portRecords.put(port, pr);
        }
    }
    
    private void deregisterPort(P port) {
        portRecords.remove(port);
    }
    
    private void registerWire(N owner, E wire, P fromPort, P toPort) {
        if(!portRecords.containsKey(fromPort) || !portRecords.containsKey(toPort)) 
            return;
        
        portRecords.get(fromPort).addWire(wire);
        portRecords.get(toPort).addWire(wire);
        wireRecords.put(wire, new WireRecord(owner, wire, fromPort, toPort));
    }
    
    private void deregisterWire(E wire) {

        WireRecord wr = wireRecords.get(wire);
        
        if(wr == null)
            return;
        
        P fromPort = wr.getFrom();
        P toPort = wr.getTo();
        
        if(!portRecords.containsKey(fromPort) || !portRecords.containsKey(toPort))
            return;
        
        portRecords.get(fromPort).removeWire(wire);
        portRecords.get(toPort).removeWire(wire);
        wireRecords.remove(wire);
    }

    
    @Override
    public boolean addNode(N instance, N parent, int index) {
        
        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        if(parent == null)
            throw new NullPointerException("parent cannot be null");
        if(!containsNode(parent))
            throw new NullPointerException("parent is not in graph");
        if(containsNode(instance))
            return false;
        if(!graph.addNode(instance, parent, index))
            return false;

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added {} to {} at index {}", getPrettyName(instance), getPrettyName(parent), index);
        //contract(instance);
        dispatchNodeEvent(SBGraphEventType.ADDED, parent, instance);
        if(isVisible(instance)) 
            dispatchNodeEvent(SBGraphEventType.VISIBLE, parent, instance);
        return true;
    }

    @Override
    public boolean addNode(N instance, N parent) {
        
        if(instance == null) {
            LOGGER.warn("instance cannot be null");
            return false;
        } else if(parent == null) {
            LOGGER.warn("parent cannot be null");
            return false;
        } else if(!containsNode(parent)) {
            LOGGER.warn("parent [ {} ] is not in graph", getPrettyName(parent));
            return false;
        } else if(containsNode(instance)) {
            LOGGER.warn("instance [ {} ] is already in graph", getPrettyName(instance));
            return false;
        }
        
        return addNode(instance, parent, getChildren(parent).size());
    }

     @Override
    public boolean addPort(P portNode, N ownerNode, SBPortDirection direction, Set<String> portConstraints) {
        
        registerPort(ownerNode, portNode, direction, portConstraints);
        connectLowerPortToHigherProxies(portNode, graph.getParent(ownerNode)); 

        if(isProxy(portNode))
            storeDanglingProxy(portNode, ownerNode);

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added {} port to {}", getPrettyName(portNode), getPrettyName(ownerNode));

        dispatchPortEvent(SBGraphEventType.ADDED, ownerNode, portNode);
        if(isVisible(ownerNode))
            dispatchPortEvent(SBGraphEventType.VISIBLE, ownerNode, portNode);
        
        return true;
    }
    
    @Override
    public boolean addEdge(N from, N to, E edge) {
       LOGGER.warn("Tried to add edge without specifying ports: [ {} ]", edge);
        return false;
    }
    
    @Override
    public boolean addEdgeByPort(P from, P to, E edge) {

        if(from == null) {
            LOGGER.warn("from port is null for {}", edge.toString());
            return false;
        }

        if(to == null) {
            LOGGER.warn("to port is null for {}", edge.toString());
            return false;
        }
        
        if(!portRecords.containsKey(from)) {
            LOGGER.warn("Could not find record for {}", from.toString());
            return false;
        }
        
        if(!portRecords.containsKey(to)) {
            LOGGER.warn("Could not find record for {}", to.toString());
            return false;
        }
        
        N fromOwner = portRecords.get(from).getOwner();
        N toOwner = portRecords.get(to).getOwner();
        
        if(!graph.addEdge(fromOwner, toOwner, edge)) {
            LOGGER.warn("Could not add edge by port: {}: {} to {}", getPrettyName(edge), getPrettyName(from), getPrettyName(to));
        }
            
        registerWire(toOwner, edge, from, to);

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added edge by port: {}: {} to {}", getPrettyName(edge), getPrettyName(from), getPrettyName(to));
 
        dispatchEdgeEvent(SBGraphEventType.ADDED, edge);
        
        if(isVisible(fromOwner) && isVisible(toOwner))
            dispatchEdgeEvent(SBGraphEventType.VISIBLE, edge);
        
        return true;
    }
   
     @Override
    public boolean removeNode(N node) {
    
        N parent = graph.getParent(node);
        
        if(!graph.removeNode(node))
            return false;

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Removed {}", getPrettyName(node));

        dispatchNodeEvent(SBGraphEventType.REMOVED, parent, node);
        
        return true;
    }
    
    

    @Override
    public boolean removeAllNodes(Collection<? extends N> nodes) {
       boolean changed = false;
        
        for(N node : nodes) {
            if(removeNode(node)) {
                changed = true;
            }
        }
        
        return changed;
    }
    
    @Override
    public boolean removePort(P portNode, N ownerNode) {
        //removeNode(portNode);

        Set<E> edges = portRecords.get(portNode).getWires();
        removeAllEdges(edges);

        deregisterPort(portNode);
        
        if(isProxy(portNode) && ownerNode != graph.getRoot())
            removeDanglingProxies(portNode, graph.getParent(ownerNode));
        
        //LOGGER.trace("Removed {} port from {}", getPrettyName(portNode), getPrettyName(ownerNode));

        dispatchPortEvent(SBGraphEventType.REMOVED, ownerNode, portNode);
        
        return true;
    }
    
    
    
    @Override
    public E removeEdge(N sourceNode, N targetNode) {
        E edge = getEdge(sourceNode, targetNode);
        return (edge != null && removeEdge(edge)) ? edge : null;
    }
    
    @Override
    public boolean removeEdge(E edge) {

        boolean removed = graph.removeEdge(edge);

        deregisterWire(edge);

        if(removed) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Removed {}", getPrettyName(edge));
            dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
        }

        return removed;
    }

    @Override
    public boolean removeAllEdges(Collection<? extends E> edges) {
        boolean b = true;
        
        for(E edge : edges) {
            if(!removeEdge(edge))
                b = false;
        }
        
        return b;
    }

    @Override
    public Set<E> removeAllEdges(N sourceNode, N targetNode) {
       return getAllEdges(sourceNode, targetNode).stream().filter(e -> removeEdge(e)).collect(Collectors.toSet());
    }
    
    // =========================================================================
    //                          Event dispatch
    // =========================================================================  
    
    public void dispatchNodeEvent(SBEventType type, N parent, N node) {
        SBEvent evt = new DefaultSBGraphEvent<>(type, SBGraphEntityType.NODE, node, this, parent, null);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch N evt: [ {} ]", evt);
        dispatcher.publish(evt);
    }
    
    public void dispatchPortEvent(SBEventType type, N owner, P node) {
         SBEvent evt = new DefaultSBGraphEvent<>(type, SBGraphEntityType.PORT, node, this, owner, null);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch P evt: [ {} ]", evt);
        dispatcher.publish(evt);
    }
    
    public void dispatchEdgeEvent(SBEventType type, E edge) {
        SBEvent evt = new DefaultSBGraphEvent<>(type, SBGraphEntityType.EDGE, edge, this, null, null);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch E evt: [ {} ]", evt);
        dispatcher.publish(evt);  
    }

    @Override
    public void onEvent(SBEvent e) {
        
        if(e.getType() != SBGraphEventType.HIDDEN && e.getType() != SBGraphEventType.VISIBLE) {
            return;
        }

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Relaying evt: [ {} ]", e);

        // object has just been expanded or collapse - dispatch an appropriate event
        
        if(SBValued.class.isAssignableFrom(e.getDataClass())) {
            N obj = (N)e.getData();
        
            if(SBEvtWithParent.class.isAssignableFrom(e.getClass())) {
                dispatchNodeEvent(e.getType(), (N)((SBEvtWithParent)e).getParent(), (N)e.getData());

                // for each of the object's ports, dispatch event

                for (P port : getPorts(obj)) {
                    dispatchPortEvent(e.getType(), obj, port);
                }
            }
        } else if (SBEdge.class.isAssignableFrom(e.getDataClass())) {
            dispatchEdgeEvent(e.getType(), (E)e.getData());
        }
        
        
        
        
    }
    
    // ==================================================================
    
    
    @Override
    public boolean containsPort(P port) {
        return portRecords.containsKey(port);
    }

    
    
    @Override
    public boolean isProxied(P port) {
        return getProxy(port) != null;
    }

    @Override
    public boolean isProxy(P port) {
        if(!portRecords.containsKey(port))
            return false;
        return portRecords.get(port).isProxy();
    }

    @Override
    public boolean setProxy(P proxy, P proxied) {
        if(!portRecords.containsKey(proxy) || !portRecords.containsKey(proxied)) {
            return false;
        }
        
        portRecords.get(proxy).setProxied(proxied);
        return true;
    }
    
    @Override
    public boolean removeProxy(P proxy, P proxied) {
        if(!portRecords.containsKey(proxy) || !portRecords.containsKey(proxied)) {
            return false;
        }
        
        portRecords.get(proxy).removeProxied(proxied);
        return true;
    }
    
    
    @Override
    public Set<P> getPorts(N node) {
        return portRecords.values().stream()
                .filter(p -> p.getOwner().equals(node))
                .map(p -> p.getPort())
                .collect(Collectors.toSet());
    }

    @Override
    public N getPortOwner(P port) {
        if(!portRecords.containsKey(port))
            return null;
        
        return portRecords.get(port).getOwner();
    }

   
    @Override
    public Map<P, P> getConnectablePorts(N from, N to) {
        Map<P, P> fromTo = new HashMap<>();

        for(P outPort : getPorts(from)) {
            for(P inPort : getPorts(to)) {    
                if(isConnectable(outPort, inPort)) {
                    fromTo.put(outPort, inPort);
                }
            }
        }
        
        return fromTo;
    }

    @Override
    public boolean hasLowestCommonAncestor(N from, N to) {
        return getLowestCommonAncestor(from, to) != null;
    }

    @Override
    public SBPTraversalSource<N, P, E, SBPGraph<N, P, E>> getTraversal() {
        return new DefaultSBPTraversalSource<>(this);
    }

    private void connectLowerPortToHigherProxies(P port, N ownersParent) {
       
        if(!danglingProxies.containsKey(ownersParent))
            return;
        
        Set<P> added = new HashSet<>();
        
        for(P danglingProxy : danglingProxies.get(ownersParent)) {
            /*
            ProxyPort proxyPort = (ProxyPort)danglingProxy.getData().getValue();
 */
           // if(proxyPort.getProxied() == port.getDefinition()) {
                /*
                E viewEdge = new E(danglingProxy, port, SignalEdgeFactory.proxies(
                            "wire_" + graph.edgeSet().size(), 
                            danglingProxy.getData(),
                            port.getData()), model);
                    addEdge(viewEdge);
                    added.add(danglingProxy);*/
          //  }   
        }
     
        for(P instance : added) {
        } 
    }
    
    
    public boolean isConnectable(P from, P to) {
        if(!portRecords.containsKey(from) || !portRecords.containsKey(to))
            return false;
            
        if(portRecords.get(from).getDirection() != SBPortDirection.OUT
                || portRecords.get(to).getDirection() != SBPortDirection.IN)
            return false;
        
        if(!portRecords.get(to).getPortConstraints().containsAll(portRecords.get(from).getPortConstraints()) 
                || !portRecords.get(from).getPortConstraints().containsAll(portRecords.get(to).getPortConstraints()))
            return false;
        
        return true;        
    }
    
    private void storeDanglingProxy(P proxyPort, N owner) {

        if(proxyPort == null)
            throw new NullPointerException("proxy cannot be null");
        
        if(owner == null)
            throw new NullPointerException("owner cannot be null");
            
        if(danglingProxies.get(owner) == null) 
            danglingProxies.put(owner, new HashSet<>());
        
        danglingProxies.get(owner).add(proxyPort);

    }
    
    private void removeDanglingProxies(P proxyPort, N owner) {

        if(proxyPort == null)
            throw new NullPointerException("proxy cannot be null");
        
        if(owner == null)
            throw new NullPointerException("owner cannot be null");

        if(danglingProxies.get(owner) == null) 
           return;

        danglingProxies.get(owner).remove(proxyPort);
        
        if(danglingProxies.get(owner).isEmpty())
            danglingProxies.remove(owner);
    }
    
    // ============================================================
    //                          Wires
    // ============================================================
    
         @Override
    public N getEdgeOwner(E edge) {
        return wireRecords.get(edge).getOwner();
    }

   

    @Override
    public boolean containsEdgeByPort(P from, P to) {
        N fromOwner = portRecords.get(from).getOwner();
        N toOwner = portRecords.get(to).getOwner();
        return graph.getAllEdges(fromOwner, toOwner).stream().anyMatch((E e) -> {
            WireRecord wr = wireRecords.get(e);
            return wr.getFrom().equals(from) && wr.getTo().equals(to);
        });
    }

    @Override
    public Set<E> getAllEdgesByPort(P port) {
        if(!portRecords.containsKey(port))
            return Collections.EMPTY_SET;
        
        return portRecords.get(port).getWires();
    }

    @Override
    public Set<E> getAllEdgesByPort(P from, P to) {
        N fromOwner = portRecords.get(from).getOwner();
        N toOwner = portRecords.get(to).getOwner();
        return graph.getAllEdges(fromOwner, toOwner).stream().filter((E e) -> {
            WireRecord wr = wireRecords.get(e);
            return wr.getFrom().equals(from) && wr.getTo().equals(to);
        }).collect(Collectors.toSet());
    }

    @Override
    public P getEdgeSourcePort(E edge) {
        if(!wireRecords.containsKey(edge))
            return null;
        return wireRecords.get(edge).getFrom();
    }

    @Override
    public P getEdgeTargetPort(E edge) {
        if(!wireRecords.containsKey(edge))
            return null;
        return wireRecords.get(edge).getTo();
    }


    // ============================================================
    //                          Others
    // ============================================================
    

    private boolean areSibilings(N instance1, N instance2) {
        return graph.getParent(instance1) == graph.getParent(instance2);
    }

    //@Override
    private N getLowestCommonAncestor(N from, N to) {
        
        Map<P, P> portPairs = getConnectablePorts(from, to);
        if(portPairs.size() != 1)
            return null;
        
        N commonParent = null;
        N ancestor = graph.getParent(from);
        P tempFrom = portPairs.keySet().iterator().next();
        P tempTo = portPairs.get(tempFrom);

        // follow proxies, not parents
        
        while(commonParent == null && ancestor!=null && tempFrom != null) {
            if(graph.isDescendant(ancestor, to))
                commonParent = ancestor;
            
            tempFrom = getProxy(tempFrom);
            if(tempFrom != null)
                ancestor = graph.getParent(getPortOwner(tempFrom));
        }
        
        if(commonParent!=null)
            return null;
        
        ancestor = null;
        
        while(ancestor != commonParent && tempTo != null) {

            tempTo = getProxy(tempTo);
            if(tempTo != null)
                ancestor = graph.getParent(getPortOwner(tempTo));
        }

        if(ancestor != commonParent)
            return null;
        
        return ancestor;
    }

    @Override
    public boolean isVisible(N node) {
        return graph.isVisible(node);
    }

    
    
    
    @Override
    public boolean containsEdge(N from, N to, E edge) {
        return graph.containsEdge(from, to, edge);
    }

    @Override
    public Set<E> getAllEdges() {
        return graph.getAllEdges();
    }
    
    

    @Override
    public N getRoot() {
        return graph.getRoot();
    }

    @Override
    public boolean isDescendant(N node, N descendant) {
        return graph.isDescendant(node, descendant);
    }

    @Override
    public boolean hasSiblings(N node) {
        return graph.hasSiblings(node);
    }

    @Override
    public List<N> getSiblings(N node) {
        return graph.getSiblings(node);
    }

    @Override
    public boolean isAncestor(N node, N ancestor) {
        return graph.isAncestor(node, ancestor);
    }

    @Override
    public void setParent(N parent, N node) {
        graph.setParent(parent, node);
    }

    @Override
    public SBHIterator<N> hierarchyIterator(N instance) {
        return graph.hierarchyIterator(instance);
    }

    @Override
    public Set<N> nodeSet() {
        return graph.nodeSet();
    }

    @Override
    public boolean containsNode(N node) {
        return graph.containsNode(node);
    }


    
    @Override
    public boolean containsEdge(N from, N to) {
        return graph.containsEdge(from, to);
    }
    
    
    @Override
    public E getEdge(N from, N to) {
        return graph.getEdge(from, to);
    }
    
    @Override
    public Set<E> getAllEdges(N node) {
        return graph.getAllEdges(node);
    }

    @Override
    public Set<E> getAllEdges(N sourceNode, N targetNode) {
        return graph.getAllEdges(sourceNode, targetNode);
    }

    @Override
    public Set<E> edgeSet() {
        return graph.edgeSet();
    }

    @Override
    public boolean containsEdge(E edge) {
        return graph.containsEdge(edge);
    }

    @Override
    public N getEdgeSource(E edge) {
       return graph.getEdgeSource(edge);
    }

    @Override
    public N getEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }

    @Override
    public List<E> incomingEdges(N node) {
        return graph.incomingEdges(node);
    }

    @Override
    public List<E> outgoingEdges(N node) {
        return graph.outgoingEdges(node);
    }

    @Override
    public void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }

    @Override
    public N getOriginalEdgeSource(E edge) {
        return graph.getOriginalEdgeSource(edge);
    }

    @Override
    public N getOriginalEdgeTarget(E edge) {
        return graph.getOriginalEdgeTarget(edge);
    }
    
    @Override
    public List<N> getAllChildren(N node) {
        return graph.getAllChildren(node);
    }

    @Override
    public List<N> getChildren(N node) {
        return graph.getChildren(node);
    }

    @Override
    public List<N> getAllDescendants(N node) {
        return graph.getAllDescendants(node);
    }

    @Override
    public List<N> getDescendants(N node) {
        return graph.getDescendants(node);
    }

    @Override
    public N getParent(N node) {
        return graph.getParent(node);
    }

    @Override
    public int getDepth(N node) {
        return graph.getDepth(node);
    }

    @Override
    public void expand(N node) {
        graph.expand(node);
    }

    @Override
    public void contract(N node) {
        graph.contract(node);
    }

    @Override
    public boolean isContracted(N node) {
        return graph.isContracted(node);
    }

    @Override
    public Set<P> portSet() {
        return portRecords.keySet();
    }

   @Override
    public P getProxy(P port) {
        return portRecords.values().stream()
                .filter(p -> p.isProxy())
                .filter(p -> p.getProxied().equals(port))
                .map(p -> p.getPort())
                .findFirst().orElse(null);
    }

    @Override
    public P getProxied(P port) {
        return portRecords.get(port).getProxied();
    }

    @Override
    public SBPCursor<N,P, E> createCursor(N startNode) {
        return new DefaultSBPCursor<>(startNode, this);
    }

    @Override
    public String toString() {
        return "P-Graph[ ".concat(getRoot().toString()).concat(" ]");
    }

    
    /*
    @Override
    public void onMessage(WorkspaceEvent e) {
        
        // Instances
        
        if(e instanceof WorkspaceEvent.InstanceEvent) {
            
            WorkspaceEvent.InstanceEvent evt = (WorkspaceEvent.InstanceEvent) e;
            
            if(evt.getData().getIdentity().equals(getRoot().getData().getIdentity()))
                return;
            
            Set<N> parentNodes = getViewNodes(evt.getSource());
            
            if(parentNodes.isEmpty())
                    return;
            
            if(e.getEventType() == EventType.ADD) {
                for(N parentNode : getViewNodes(evt.getSource())) {
                    
                    if(!expandedNodes.contains(parentNode)) 
                        expand(parentNode);
                    
                    N node = new N(evt.getData(), model);
                    
                    addInstance( node,
                        parentNode, 
                        evt.getIndex());
                   
                    expand(node);
                    
                }
            }
            
            else if (e.getEventType() == EventType.REMOVE) {
                Set<N> viewNodes = new HashSet<>(getViewNodes(evt.getData()));
                for(N viewNode : viewNodes) {
                    
                    if(expandedNodes.contains(viewNode))
                        contract(viewNode);
                    removeInstance(viewNode);
                    
                  
                }
            }    
        } 
        
        // Ports do not work....

        // Ports
        
        else if (e instanceof WorkspaceEvent.PortEvent) {
   
            WorkspaceEvent.PortEvent evt = (WorkspaceEvent.PortEvent) e;
            
            if(e.getEventType() == EventType.ADD)
                
                for(N ownerNode : getViewNodes(evt.getSource())) {
                    EntityInstance instance = ((EntityInstance)ownerNode.getData());
                    addPort(new P(instance.getPortByDefinition(evt.getData().getIdentity()), model), ownerNode);
                }
            
            else if (e.getEventType() == EventType.REMOVE) {
                
                for(N ownerNode : getViewNodes(evt.getSource())) {
                    for(P portNode : getPorts(ownerNode)) {
                        if(portNode.getData().getValue().getIdentity().equals(evt.getData().getIdentity())) {
                            removePort(portNode, ownerNode);
                        }
                    }
                }
            }
        }
        
        // Wires
        
        else if (e instanceof WorkspaceEvent.WireEvent) {

            WorkspaceEvent.WireEvent evt = (WorkspaceEvent.WireEvent) e;
  
            if(e.getEventType() == EventType.ADD)
                
                for(N node : getViewNodes(evt.getSource())) {
                    
                    if(!expandedNodes.contains(node))
                        expand(node);
                    
                    addWire(node, evt.getData());
                }
            
            else if(e.getEventType() == EventType.REMOVE)
                
                for(N node : getViewNodes(evt.getSource())) {
                    removeWire(node, evt.getData());
                }
        }   
    }*/  
    
    protected final class PortRecord implements Serializable {
        
        private final SBPortDirection direction;
        private final N owner;
        private final P port;
        private P proxied;
        private final Set<E> wires;
        private final Set<String> portConstraints;
 
        public PortRecord(N owner, P port, SBPortDirection direction) {
            this.port = port;
            this.owner = owner;
            this.portConstraints = new HashSet<>();
            this.wires = new HashSet<>();
            this.direction = direction;
        }

        public P getPort() {
            return port;
        }
        
        public N getOwner() {
            return owner;
        }

        public SBPortDirection getDirection() {
            return direction;
        }
        
        public void addConstraint(String constraint) {
            portConstraints.add(constraint);
        }

        public Set<String> getPortConstraints() {
            return portConstraints;
        }
        
        public boolean isProxy() {
            return proxied != null;
        }
        
        public P getProxied() {
            return proxied;
        }
        
        public boolean addWire(E wire) {
            return wires.add(wire);
        }
        
        public boolean removeWire(E wire) {
            return wires.remove(wire);
        }
        
        public Set<E> getWires() {
            return wires;
        }

        public void setProxied(P proxied) {
            this.proxied = proxied;
        }
        
        public void removeProxied(P proxied) {
            if(this.proxied.equals(proxied))
                this.proxied = null;
        }
        
        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof DefaultPGraph.PortRecord))
                return false;
            
            PortRecord portRecord = (PortRecord) obj;
            return portRecord.getPort().equals(this.getPort());
        }

        @Override
        public int hashCode() {
            return getPort().hashCode();
        }
    }
    
    protected final class WireRecord implements Serializable {
        
        private final N owner;
        private final E wire;
        private final P from;
        private final P to;
 
        public WireRecord(N owner, E wire, P from, P to) {
            this.owner = owner;
            this.wire = wire;
            this.from = from;
            this.to = to;
        }

        public E getWire() {
            return wire;
        }

        public P getFrom() {
            return from;
        }

        public P getTo() {
            return to;
        }

        public N getOwner() {
            return owner;
        }
        
        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof DefaultPGraph.WireRecord))
                return false;
            
            WireRecord wireRecord = (WireRecord) obj;
            return wireRecord.getOwner().equals(getOwner()) &&
                    wireRecord.getFrom().equals(getFrom()) &&
                    wireRecord.getTo().equals(getTo()) &&
                    wireRecord.getOwner().equals(getOwner());
        }

        @Override
        public int hashCode() {
            return 7 * owner.hashCode() *
                    wire.hashCode() *
                    from.hashCode() * 
                    to.hashCode();
        }
    }
}
