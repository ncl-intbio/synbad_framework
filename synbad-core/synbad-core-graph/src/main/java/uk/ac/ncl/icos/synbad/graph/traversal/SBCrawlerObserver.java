package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;


/**
 *
 * @author owengilfellon
 */
public interface SBCrawlerObserver<N extends SBValued, E extends SBEdge, C extends SBCursor<N, E>> {

    public void onEdge(E edge);

    public void onEnded();

    public void onInstance(N instance, C cursor);

    public boolean terminate();

    public boolean vetoEdge(E edge);
    
    
    public abstract class SBCrawlerObserverAdapter<N extends SBValued, E extends SBEdge, C extends SBCursor<N, E>> implements SBCrawlerObserver<N, E, C>{

        @Override
        public void onEdge(E edge) { }

        @Override
        public void onEnded() { }

        @Override
        public void onInstance(N instance, C cursor) { }

        @Override
        public boolean terminate() { return false; }

        @Override
        public boolean vetoEdge(E edge) { return false; }
        
    }
}
