/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.api.domain.SBEntity;

/**
 *
 * @author owengilfellon
 */
public interface SBSubGraph<N extends SBValued, E extends SBEdge> extends SBGraph<N, E> {

    public Set<String> getLabels();
    public Set<SBEntity> getLabelledEntities(String label);
    public Set<N> getLabelledVertices(String label);
    public Set<E> getLabelledEdges(String label);

}
