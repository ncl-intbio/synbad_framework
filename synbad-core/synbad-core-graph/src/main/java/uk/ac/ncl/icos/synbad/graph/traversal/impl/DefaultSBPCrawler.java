/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawlerObserver;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPCrawler;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPCrawlerListener;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBPCrawler<N extends SBValued, P extends SBValued, E extends SBEdge>  extends SBPCrawler.SBCrawlerAdapter<N, E, SBPCursor<N,P, E>> implements SBPCrawlerListener<N, P, E, SBPCursor<N, P, E>>{
    
    private final Set<SBPCrawlerListener<N, P, E, SBPCursor<N,P,E>>> listeners = new HashSet<>();

    @Override
    public void run(SBPCursor<N, P, E> cursor) {
        processNode(cursor, new HashSet<>());
        onEnded(); 
    }
    
    @Override
    public void onEdge(E edge) {
        for(SBCrawlerObserver<N, E, SBPCursor<N,P, E>> listener : listeners) {
            listener.onEdge(edge);
        }
    }

    @Override
    public void onEnded() {
        for(SBCrawlerObserver<N, E, SBPCursor<N, P, E>> listener : listeners) {
            listener.onEnded();
        }
    }
    

    @Override
    public void processNode(SBPCursor<N,P,E> cursor, Set<E> seenEdges) {

        if(terminate()) {
            return;
        }
        
        onInstance(cursor.getCurrentPosition(), cursor);
        
        for(P port : cursor.getPorts()) {
            
            for(E edge : cursor.getEdgesViaPort(port)) {
                
                onPort(port, cursor);
                
            }
            
             
            
        
            for (E edge : cursor.getEdgesViaPort(port)) {
                if(!seenEdges.contains(edge)) {
                    processEdge(cursor, seenEdges, edge);
                }       
            }
        }

        
        for(N node : cursor.getChildren()) {
            if(!hasVisited(cursor, node))
                processNode(cursor, seenEdges);
        }
    }

    
    @Override
    public void processEdge(SBPCursor<N, P, E> cursor, Set<E> seenEdges, E edge) {
        
        
        seenEdges.add(edge);
        
        if(vetoEdge(edge))
            return;

        onEdge(edge);

        // TODO: Should work for to->from as well as from->to
        
        P port = cursor.getOwner(cursor.getEdgeSourcePort(edge)).equals(cursor.getCurrentPosition()) ? cursor.getEdgeTargetPort(edge) : cursor.getEdgeSourcePort(edge);
        
        if(hasVisited(cursor, cursor.getOwner(port)))
            return;
        
        
        onPort(port, cursor);

        cursor.moveTo(cursor.getOwner(port));
        processNode(cursor, seenEdges);
    }
    
       private boolean hasVisited(SBPCursor<N,P, E> cursor,Object instance) {
        for(N i : cursor.getPath()) {
            if(i.equals(instance)) {
                return true;
            }
                
        }
        return false;
    }

    @Override
    public void onPort(P instance, SBPCursor<N, P, E> cursor) {
        for(SBPCrawlerListener<N, P, E, SBPCursor<N,P, E>> listener : listeners) {
            listener.onPort(instance, cursor);
        }
    }




}
