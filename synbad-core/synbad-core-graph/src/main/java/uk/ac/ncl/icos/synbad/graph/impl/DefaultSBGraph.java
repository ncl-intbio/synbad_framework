 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.impl;

import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBCursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.jgrapht.graph.DirectedMultigraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.SBGlobalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGlobalNodeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBGraphTraversalSource;
import uk.ac.ncl.icos.synbad.graph.SBWritableGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;

 /**
 *
 * @author owengilfellon
 */

public class DefaultSBGraph<N, E> implements SBWritableGraph<N, E>, SBGlobalEdgeProvider<N, E>, SBGlobalNodeProvider<N> {

    private final DirectedMultigraph<N, E> graph; 
    private final SBDispatcher dispatcher;
    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultSBGraph.class);
    
    public DefaultSBGraph() {
        Class edgeClass = Object.class;
        this.graph = new DirectedMultigraph<>(edgeClass);
        this.dispatcher = new DefaultSBDispatcher();
    }
    
    private DefaultSBGraph(DirectedMultigraph<N, E> graph) {
        this.graph = graph;
        this.dispatcher = new DefaultSBDispatcher();
    }
    
    public void dispatchNodeEvent(SBGraphEventType type, N node) {
        dispatcher.publish(new DefaultSBGraphEvent(type, SBGraphEntityType.NODE, node, this,  null,null));
    }
    
    public void dispatchEdgeEvent(SBGraphEventType type, E edge) {
        dispatcher.publish(new DefaultSBGraphEvent(type, SBGraphEntityType.NODE, edge, this, null, null));
    }

    @Override
    public boolean addNode(N node) {
        if(!graph.addVertex(node))
            return false;
        
        dispatchNodeEvent(SBGraphEventType.ADDED, node);
        
        return true;
    }

    
    @Override
    public boolean addEdge(N sourceNode, N targetNode, E edge) {
        /*   */
        if(sourceNode.equals(targetNode))
            return false;
        
        if(!graph.containsVertex(sourceNode)) {
            //logger.error("Graph does not contain source [ {} ] of [ {} ]", sourceNode, edge);
            return false;
        }
        
        if(!graph.containsVertex(targetNode)) {
           // LOGGER.error("Graph does not contain target [ {} ] of [ {} ]", targetNode, edge);
            return false;
        }
            
     
        if(!graph.addEdge(sourceNode, targetNode, edge))
            return false;
        
        dispatchEdgeEvent(SBGraphEventType.ADDED, edge);
        
        return true;
    }
    
    @Override
    public Set<E> getAllEdges(N sourceNode, N targetNode) {
        return graph.getAllEdges(sourceNode, targetNode);
    }


    @Override
    public N getEdgeSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public N getEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }

    @Override
    public boolean containsEdge(E edge) {
        return graph.containsEdge(edge);
    }

    @Override
    public boolean containsNode(N node) {
        return graph.containsVertex(node);
    }

    @Override
    public Set<E> edgeSet() {
        return graph.edgeSet();
    }

    @Override
    public Set<E> getAllEdges(N node) {
        return graph.edgesOf(node);
    }

    @Override
    public List<E> incomingEdges(N node) {
        return graph.containsVertex(node) ? new ArrayList<>(graph.incomingEdgesOf(node)) : new ArrayList<>();
    }


    @Override
    public List<E> outgoingEdges(N node) {
        return graph.containsVertex(node) ? new ArrayList<>(graph.outgoingEdgesOf(node)) : new ArrayList<>();
    }

    @Override
    public E removeEdge(N sourceNode, N targetNode) {
        E edge  = graph.removeEdge(sourceNode, targetNode);
        if(edge == null)
            return null;
        
        dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
        return edge;
    }

    @Override
    public boolean removeEdge(E edge) {
        if(!graph.removeEdge(edge))
            return false;
        
        dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
        return true;
    }

    @Override
    public boolean removeNode(N node) {
        if(!graph.removeVertex(node))
            return false;
        
        dispatchNodeEvent(SBGraphEventType.REMOVED, node);
        return true;
    }

    @Override
    public Set<N> nodeSet() {
        return graph.vertexSet();
    }

    @Override
    public boolean containsEdge(N sourceNode, N targetNode) {
        return graph.containsEdge(sourceNode, targetNode);
    }

    @Override
    public boolean removeAllEdges(Collection<? extends E> edges) {
        return graph.removeAllEdges(edges);
    }

    @Override
    public Set<E> removeAllEdges(N sourceNode, N targetNode) {
        Set<E> edges = graph.removeAllEdges(sourceNode, targetNode);
        
        for(E edge : edges) {
            dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
        }
        
        return edges;
    }

    @Override
    public boolean removeAllNodes(Collection<? extends N> nodes) {
        if( !graph.removeAllVertices(nodes))
            return false;
        
        for(N node : nodes) {
            dispatchNodeEvent(SBGraphEventType.REMOVED, node);
        }
        
        return true;
    }
    
    
    @Override
    public E getEdge(N from, N to) {
        return graph.getEdge(from, to);
    }

    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }

    @Override
    public SBCursor<N, E> createCursor(N startNode) {
        return new DefaultSBCursor<>(startNode, this);
    }

     @Override
     public SBGraphTraversalSource<N, E, SBGraph<N, E>> getTraversal() {
         return new DefaultSBGraphTraversalSource<>(this);
     }

     @Override
    public String toString() {
        return "Graph";
    }

}