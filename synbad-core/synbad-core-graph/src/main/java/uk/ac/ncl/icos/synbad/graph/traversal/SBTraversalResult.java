/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.tree.ITree;
import uk.ac.ncl.icos.synbad.api.domain.SBEntity;

/**
 *
 * @author owengilfellon
 */
public interface SBTraversalResult<N extends SBValued, E extends SBEdge> {

    /**
     * Returns the set of subgraphs, with each traverser returning a subgraph created from the vertices and edges that
     * it has traversed.
     * @return
     */
    public Set<SBSubGraph<N, E>> getMatches();
     // ==============================
    //      Collection methods
    // ==============================

    /**
     * Returns a map of labels to vertices that are derived from the provided class.
     * @param clazz
     * @param <E>
     * @return
     */
    public <E> Map<String, Set<E>> getLabelledEntities(Class<E> clazz);

    /**
     * Returns the set of all entities that have the provided label and are derived from the provided class.
     * @param clazz
     * @param label
     * @param <E>
     * @return
     */
    public <E> Set<E> getLabelledEntities(Class<E> clazz, String label);

    /**
     * A stream of the final positions of all traversers.
     * @return
     */
    public Stream<SBEntity> stream();

    /**
     * A stream of the final vertex positions of all traversers.
     * @return
     */
    public Stream<N> streamVertexes();

    /**
     * A stream of the final edge positions of all traversers.
     * @return
     */
    public Stream<E> streamEdges();
    
    /**
     * A tree constructed from the paths of all traversers. The positions in the
     * path alternate between nodes and edges, and as such, the levels hierarchy
     * also alternate between nodes and edges.
     * @return 
     */
    public ITree<?> getTree();

    /**
     * The number of traversers created as part of the traversal.
     * @return
     */
    public int getTraverserCount();

    /**
     * Create a single graph from the paths of all traversers.
     * @return
     */
    public SBGraph<N, E> toGraph();
    
}
