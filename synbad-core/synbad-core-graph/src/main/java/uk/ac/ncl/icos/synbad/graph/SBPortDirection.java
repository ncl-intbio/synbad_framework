/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.net.URI;
import uk.ac.ncl.icos.synbad.datadefinition.SBDataDef;
import uk.ac.ncl.icos.synbad.datadefinition.SBNamedDef;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;

/**
 *
 * @author owengilfellon
 */
public enum SBPortDirection implements SBDataDef {

    IN(UriHelper.synbad.namespacedUri("in")),
    OUT(UriHelper.synbad.namespacedUri("out"));
   
    private final URI uri;
    
    @Override
    public URI getUri() {
        return uri;
    }

    private SBPortDirection(URI uri) {
        this.uri = uri;
    }  

    public static SBPortDirection fromString(String signalDirection) {
        if (signalDirection.equals("Input") || signalDirection.equals("IN") || signalDirection.equals("Modifier") || signalDirection.equals(IN.getUri().toASCIIString())) {
            return IN;
        } else if (signalDirection.equals("Output") || signalDirection.equals("OUT") || signalDirection.equals(OUT.getUri().toASCIIString())) {
            return OUT;
        } 
        return null;
    }
    
}
