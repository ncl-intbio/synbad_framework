/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.SBPGraph;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 * A SBTraversalSource initiates a traversal from an initial list of nodes
 * or edges.
 * 
 * 
 * @author owengilfellon
 */
public interface SBPTraversalSource<N, P, E,  G extends SBPGraph> extends SBHTraversalSource<N, E, G> {
    
    
    /**
     * A traversal beginning at the provided objects
     * @param objs
     * @return 
     */
    @Override
    public SBPTraversal<N, N, G> v(N... objs);
    
    /**
     * A traversal beginning at the provided edges
     * @param edges
     * @return 
     */
    @Override
    public SBPTraversal<E, E, G> e(E... edges);
    
    /**
     * A traversal beginning at the provided ports
     * @param ports
     * @return 
     */
    public SBPTraversal<P, P, G> p(P... ports);
    
    
    
    
  
}
