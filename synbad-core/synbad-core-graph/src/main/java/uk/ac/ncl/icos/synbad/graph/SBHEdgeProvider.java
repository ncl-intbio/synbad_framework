/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.List;
import uk.ac.ncl.icos.synbad.graph.SBLocalEdgeProvider;

/**
 *
 * @author owengilfellon
 */
public interface SBHEdgeProvider<N, E> extends SBLocalEdgeProvider<N, E> {
    
    public N getOriginalEdgeSource(E edge);
            
    public N getOriginalEdgeTarget(E edge);
    
    /**
     * Returns visible and non-visible children of a node
     * @param node
     * @return 
     */
    public List<N> getAllChildren(N node);
    
    /**
     * Returns visible children of a node
     * @param node
     * @return 
     */
    public List<N> getChildren(N node);
    
    /**
     * Returns visible and non-visible descendants of a node
     * @param node
     * @return 
     */
    public List<N> getAllDescendants(N node);
    
    /**
     * Returns visible descendants of a node
     * @param node
     * @return 
     */
    public List<N> getDescendants(N node);

    public N getParent(N node);

    public int getDepth(N node);
    
    public void expand(N node);
    
    public void contract(N node);
    
    public boolean isContracted(N node);
    
   // public boolean isVisible(N node);
  
}
