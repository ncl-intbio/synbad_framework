/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;


/**
 * An SBTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface SBTraversal<T, V> {
    
 

    
    // ==============================
    //      Mutator methods
    // ==============================

    public SBTraversal<T, V> copy();
    
    // ==============================
    //        Configuration
    // ==============================
    
    // Use of a extended interface for concealing construction methods inspired
    // by TinkerPops traversal code. 
    
    public SBTraversal.Configurable<T, V> asConfigurable();

    public interface Configurable<T, V> extends SBTraversal<T, V> {

        public <E> SBTraversal<?, E> addPipe(SBTraversalPipe<?, E> step);

        public SBTraversalPipe<T, ?> getInputPipe();

        public SBTraversalPipe<?, V> getOutputPipe();

        public List<SBTraversalPipe> getPipes();
 
        // ============

        public void addInput(SBTraverser<T> input);

        public void addInput(Iterator<SBTraverser<T>> input);
     }
    
}
