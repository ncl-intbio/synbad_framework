/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Consumer;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;

import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBDoFunctionOnFiltered<T> extends DefaultSBTraversalPipe<T, T>{

    private final Consumer<T> consumer;
    private final SBEventFilter filter;
    private final boolean passThrough;
    
    public SBDoFunctionOnFiltered(SBTraversal parentTraversal, SBEventFilter filter, boolean passThrough, Consumer<T> consumer) {
        super(parentTraversal);
        this.consumer = consumer;
        this.filter = filter;
        this.passThrough = passThrough;
        
    }
    
    protected SBDoFunctionOnFiltered(SBTraversal parentTraversal, SBDoFunctionOnFiltered<T> doTraversal) {
        super(parentTraversal, doTraversal);
        this.consumer = doTraversal.consumer;
        this.filter = doTraversal.filter;
        this.passThrough = doTraversal.passThrough;
    }

    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {
        
        T current = traverser.get();
        
        if(SBEvent.class.isAssignableFrom(current.getClass()) && filter.test((SBEvent)current))  {
            consumer.accept(current);
        }

        return passThrough ? Collections.singleton(traverser) : Collections.EMPTY_LIST;
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBDoFunctionOnFiltered<>(parentTraversal, this);
    }
    
}
