/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public abstract class ASBBlockingPipe<T, V>  implements SBTraversalPipe<T, V> {

    private static final Logger LOGGER =  LoggerFactory.getLogger(ASBBlockingPipe.class);

    protected final SBCacheIterator<SBTraverser<T>> traversers;
    protected final SBCacheIterator<SBTraverser<T>> collectedTraversers;
    protected final SBCacheIterator<SBTraverser<V>> processedTraversers;

    private SBTraversalPipe<?, T> prevPipe;
    private SBTraversalPipe<V, ?> nextPipe;
    private final SBTraversal traversal;

    public ASBBlockingPipe(SBTraversal traversal) {
        this.traversers = new SBCacheIterator<>();
        this.collectedTraversers = new SBCacheIterator<>();
        this.traversal = traversal;
        this.processedTraversers = new SBCacheIterator<>();
    }

    protected ASBBlockingPipe(SBTraversal traversal, ASBBlockingPipe<T, V> pipe) {
        this.traversers = pipe.traversers.copy();
        this.processedTraversers = pipe.processedTraversers.copy();
        this.traversal =  traversal;
        this.collectedTraversers = pipe.collectedTraversers.copy();
    }

    @Override
    public SBTraversal getParentTraversal() {
        return traversal;
    }
       
    private void push() {
        if(getNextPipe() == null)
            return;

        while(hasNext()) {
            SBTraverser<V> t = next();
            getNextPipe().addInput(t);
        }
    }

    @Override
    public void addInput(SBTraverser<T> traverser) {
        this.traversers.add(traverser);
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Added Input: " + traverser);
    }

    @Override
    public void addInput(Iterator<SBTraverser<T>> traversers) {
        this.traversers.add(traversers);
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Added Inputs: " + traversers);
    }

    @Override
    public void setPreviousPipe(SBTraversalPipe<?, T> pipe) {
        prevPipe = pipe;
    }

    @Override
    public SBTraversalPipe<?, T> getPreviousPipe() {
        return prevPipe;
    }

    @Override
    public void setNextPipe(SBTraversalPipe<V, ?> pipe) {
        this.nextPipe = pipe;
    }

    @Override
    public SBTraversalPipe<V, ?> getNextPipe() {
        return nextPipe;
    }

    @Override
    public boolean hasNext() {

        if(processedTraversers.hasNext()) {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Has Next: TRUE");
            return true;
        }

        Iterator<SBTraverser<T>> collectSource = (prevPipe == null) ? traversers : prevPipe; 

        while(collectSource.hasNext()) {
            collectedTraversers.add(collectSource.next());
        }

        //  otherwise process the next result (or return null);

        Collection<SBTraverser<T>> toProcess = new ArrayList<>();
        while(collectedTraversers.hasNext()) {
            toProcess.add(collectedTraversers.next());
        }

        processedTraversers.add(processTraverser(toProcess).iterator());

        boolean b = processedTraversers.hasNext();

        if(LOGGER.isDebugEnabled()) {
            if(b)
                LOGGER.debug("Has Next: TRUE");
            else
                LOGGER.debug("Has Next: FALSE");
        }

        return b;
    }


    /**
     * If single traverser return and null. If an iterator 
     * @return 
     */
    @Override
    public SBTraverser<V> next() {


        //  If hasNext has been called, then return the processed traverser,

        if(processedTraversers.hasNext()) {
            SBTraverser<V> t = processedTraversers.next();
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Next: ".concat(t.toString()));
            return t;
        }
        //  otherwise process the next result (or return null);

        Iterator<SBTraverser<T>> collectSource = (prevPipe == null) ? traversers : prevPipe; 

        while(collectSource.hasNext()) {
            collectedTraversers.add(collectSource.next());
        }

        //  otherwise process the next result (or return null);

        Collection<SBTraverser<T>> toProcess = new ArrayList<>();
        while(collectedTraversers.hasNext()) {
            toProcess.add(collectedTraversers.next());
        }

        processedTraversers.add(processTraverser(toProcess).iterator());

        if(processedTraversers.hasNext()) {
            SBTraverser<V> t = processedTraversers.next();
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Next: ".concat(t.toString()));
            return t;
        }

        LOGGER.error("Next: NULL");
        return null;
    }

    /**
     * This method is intended to be implemented by Pipe implementations. It
     * defines the mapping a single input traverser and 0-* output traversers.
     * @param traverser
     * @return 
     */
    public abstract Collection<SBTraverser<V>> processTraverser(Collection<SBTraverser<T>> traverser);

}