/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.function.Function;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.SBWritableGraph;

/**
 *
 * @author owengilfellon
 */
public abstract class GraphFunction<V, G extends SBWritableGraph> implements Function<V, G> {

    protected final G graph;

    public GraphFunction(G graph) {
        this.graph = graph;
    }

    public static class AddNode<V extends SBValued, G extends SBWritableGraph> extends GraphFunction<V, G>{

        public AddNode(G graph) {
            super(graph);
        }

        @Override
        public G apply(V t) {
            graph.addNode(t);
            return graph;
        }
    }
    
    public static class AddEdge<V extends SBEdge, G extends SBWritableGraph, G2 extends SBGraph> extends GraphFunction<V, G>{

        protected final G2 sourceGraph;
        
        public AddEdge(G graph, G2 sourceGraph) {
            super(graph);
            this.sourceGraph = sourceGraph;
        }

        @Override
        public G apply(V t) {
            graph.addEdge((SBValued)sourceGraph.getEdgeSource(t), (SBValued)sourceGraph.getEdgeTarget(t), t);
            return graph;
        }
    }
}
