
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBSelectPipe<T, V> extends DefaultSBTraversalPipe<T, V>{

    private final List<String> labels;
    private final Class<T> clazz;
    
    public SBSelectPipe(SBTraversal traversal, String... labels) {
        super(traversal);
        this.labels = Arrays.asList(labels);
        this.clazz = null;
    }
    
    public SBSelectPipe(SBTraversal traversal, Class<T> clazz, String... labels) {
        super(traversal);
        this.labels = Arrays.asList(labels);
        this.clazz = clazz;
    }
    
    protected SBSelectPipe(SBTraversal traversal, SBSelectPipe<T, V> pipe) {
        super(traversal, pipe);
        this.labels = pipe.labels.stream().collect(Collectors.toList());
        this.clazz = pipe.clazz;
    }
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        Collection<SBTraverser<V>> processed = new ArrayList<>();
        for(String label : labels) {
            traverser.getByLabel(label).stream().filter(b -> clazz == null || clazz.isAssignableFrom(b.getClass())).map((b) -> {
                SBTraverser t = traverser.duplicate();
                t.push(b);
                return t;
            }).forEach((t) -> {
                processed.add(t);
            });
        }
        return processed;
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBSelectPipe<>(parentTraversal, this);
    }

    
    
    

}
