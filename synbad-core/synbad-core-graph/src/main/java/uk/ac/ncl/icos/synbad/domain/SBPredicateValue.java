/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.domain;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;

import java.net.URI;

/**
 *
 * @author owengilfellon
 */
public class SBPredicateValue {
    
    private final URI predicate;
    private final SBValue value;

    public SBPredicateValue(URI predicate, SBValue value) {
        this.predicate = predicate;
        this.value = value;
    }

    public URI getPredicate() {
        return predicate;
    }

    public SBValue getValue() {
        return value;
    }

    @Override
    public String toString() {
        return predicate.toASCIIString() + ": " + value.toString();
    }
    
    
    
}
