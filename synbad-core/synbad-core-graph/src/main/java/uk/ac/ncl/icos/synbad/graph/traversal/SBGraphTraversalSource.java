/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.SBGraph;

/**
 * A SBTraversalSource initiates a traversal from an initial list of nodes
 * or edges.
 * 
 * 
 * @author owengilfellon
 */
public interface SBGraphTraversalSource<N, E, G extends SBGraph> {
    
    
    /**
     * A traversal beginning at the provided objects.
     * @param <T>
     * @param objs
     * @return 
     */
    public SBGraphTraversal<N, N, G> v(N... objs);
    
    /**
     * A traversal beginning at the provided edges.
     * @param <E>
     * @param edges
     * @return 
     */
    public SBGraphTraversal<E, E, G> e(E... edges);
    
    
    
    
  
}
