/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import java.util.function.Function;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;

/**
 * An SBTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface SBHTraversal<T, V, G extends SBHGraph> extends SBGraphTraversal<T, V, G> {

    
    <V extends SBValued> SBHTraversal<T, V, G> expand();
      
    <V extends SBValued> SBHTraversal<T, V, G> contract();

    
//    <V2 extends SBValued> SBHTraversal<T, V2, G> addE(Class<V2> clazz, String label, String predicate);

    <E> SBHTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe);

//    <V2 extends SBValued> HNodeTraversal<T, V2, G> addV(V2 v);

    SBHTraversal<T, V, G> as(String label);

    SBHTraversal<T, V, G> hasLabel(String... label);

    SBHTraversal<T, V, G> hasNotLabel(String... label);

    <V2> SBHTraversal<T, V2, G> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2);

    SBHTraversal<T, V, G> copy();

    SBHTraversal<T, V, G> deduplicate();

    <S extends SBGraph> SBHTraversal<T, V, G> doFunction(Function<V, S> function);

    SBHTraversal<T, V, G> doTraversal(SBTraversal<V, V> traversal);

    public <T extends SBValued, V2 extends SBEdge> SBHTraversal<V, V2, G> e();
    
    public <V2 extends SBEdge> SBHTraversal<T, V2, G> e(SBDirection direction);
    
    public <V2 extends SBEdge> SBHTraversal<T, V2, G> e(String... predicates);
    
    public <V2 extends SBEdge> SBHTraversal<T, V2, G> e(SBDirection direction, String... predicates);
    
    public <V2 extends SBEdge> SBHTraversal<T, V2, G> e(String[] inPredicates, String[] outpredicates);

    
    <T extends SBEdge, V2 extends SBValued> SBHTraversal<T, V2, G> v();
    
    <T extends SBEdge, V2 extends SBValued> SBHTraversal<T, V2, G> v(SBDirection direction);
    
    <V2 extends SBValued> SBHTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz);


    SBHTraversal<T, V, G> filter(Predicate<V> predicate);

    SBHTraversal<T, V, G> has(String key, Object value, Object... additionalValues);

    SBHTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues);

    SBHTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits, boolean inclusive);

    SBHTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits);

    SBHTraversal<T, V, G> loop(int iterations, SBTraversal<?, V> traversal, boolean emit);

    <V2> SBHTraversal<T, V2, G> select(String... labels);

    <V2> SBHTraversal<T, V2, G> select(Class<V2> clazz, String... label);

    SBHTraversal<T, V, G> setParent(String label);

    G toGraph();

    
    SBHTraversal<T, ? extends SBValued, G> children();
    
    SBHTraversal<T, ? extends SBValued, G> children(Predicate<SBValued> predicate);
    
    SBHTraversal<T, ? extends SBValued, G> parent();
    
    SBHTraversal<T, ? extends SBValued, G> parent(Predicate<SBValued> predicate);

    @Override
    HGraphConfigurable<T, V, G> asConfigurable();

    
    
    
    public interface HGraphConfigurable<T, V, G extends SBHGraph> extends Configurable<T, V, G>, SBHTraversal<T, V, G> {

    }
    

}
