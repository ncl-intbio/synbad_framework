/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Stream;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBHasLabel<V> extends DefaultSBTraversalPipe<V, V>{

    private final String[] label;
    private final boolean not;

    public SBHasLabel(SBTraversal traversal, boolean not, String[] label) {
        super(traversal);
        this.label = label;
        this.not = not;
    }

    protected SBHasLabel(SBTraversal traversal, SBHasLabel<V> hasLabel) {
        super(traversal, hasLabel);
        this.label = hasLabel.label;
        this.not = hasLabel.not;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {

        if(traverser.get() == null)
            return Collections.EMPTY_SET;

        boolean b = not ?
                Stream.of(label).parallel().noneMatch(l -> traverser.getByLabel(l).contains(traverser.get())) :
                Stream.of(label).parallel().anyMatch(l -> traverser.getByLabel(l).contains(traverser.get()));

        return b ? Collections.singleton(traverser) : Collections.EMPTY_SET;
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBHasLabel<>(parentTraversal, this);
    }
}
