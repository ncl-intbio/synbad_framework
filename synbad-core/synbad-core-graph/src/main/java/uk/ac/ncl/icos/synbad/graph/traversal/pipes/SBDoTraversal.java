/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBDoTraversal<T> extends DefaultSBTraversalPipe<T, T>{

    private SBTraversal traversal;

    public SBDoTraversal(SBTraversal parentTraversal, SBTraversal traversal) {
        super(parentTraversal);
        this.traversal = traversal.copy();
        if(SBGraphTraversal.class.isAssignableFrom(traversal.getClass()) && 
                SBGraphTraversal.class.isAssignableFrom(getParentTraversal().getClass()))
            ((SBGraphTraversal)(this.traversal)).asConfigurable().setGraph(
                ((SBGraphTraversal)(getParentTraversal())).asConfigurable().getGraph());
    }
    
    protected SBDoTraversal(SBTraversal parentTraversal, SBDoTraversal<T> doTraversal) {
        super(parentTraversal, doTraversal);
        this.traversal = doTraversal.traversal.copy();
        if(SBGraphTraversal.class.isAssignableFrom(traversal.getClass()) && 
                SBGraphTraversal.class.isAssignableFrom(getParentTraversal().getClass()))
            ((SBGraphTraversal)(this.traversal)).asConfigurable().setGraph(
                ((SBGraphTraversal)(getParentTraversal())).asConfigurable().getGraph());
    }

    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {

        SBTraversal t = traversal.copy();
        t.asConfigurable().addInput(traverser);
        SBTraversalPipe<?, T> output = t.asConfigurable().getOutputPipe();
        List<SBTraverser<T>> processed = new ArrayList<>();
        while(output.hasNext()) {
            processed.add(output.next());
        }
        return processed;
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBDoTraversal<>(parentTraversal, this);
    }
    
}
