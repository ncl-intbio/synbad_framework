/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.List;

/**
 * Cursors operate on SBEdgeProviders, and therefore can operate over simpler
 * structures than an entire graph.
 * @author owengilfellon
 * @param <N>
 * @param <E> 
 */
public interface SBLocalEdgeProvider<N, E> {
    
    public N getEdgeSource(E edge);
            
    public N getEdgeTarget(E edge);
    
    List<E> incomingEdges(N node);
    
    List<E> outgoingEdges(N node);
}
