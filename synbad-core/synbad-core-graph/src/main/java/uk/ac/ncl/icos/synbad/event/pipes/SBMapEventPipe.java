/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 *
 * @author owengilfellon
 */
public class SBMapEventPipe<T, V> extends DefaultSBTraversalPipe<T, V>{

    private final Function<T, V> mapFunction;
    private final SBEventFilter filter;

    public SBMapEventPipe(SBTraversal parentTraversal, SBEventFilter filter, Function<T, V> mapFunction) {
        super(parentTraversal);
        this.mapFunction = mapFunction;
        this.filter = filter;
    }

    protected SBMapEventPipe(SBTraversal parentTraversal, SBMapEventPipe<T, V> doTraversal) {
        super(parentTraversal, doTraversal);
        this.filter = doTraversal.filter;
        this.mapFunction = doTraversal.mapFunction;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        
        T current = traverser.get();
        
        if(SBEvent.class.isAssignableFrom(current.getClass()) && filter.test((SBEvent)current))  {
            traverser.push(mapFunction.apply(current));
        }
        return Collections.singleton((SBTraverser<V>) traverser);
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBMapEventPipe<>(parentTraversal, this);
    }
    
}
