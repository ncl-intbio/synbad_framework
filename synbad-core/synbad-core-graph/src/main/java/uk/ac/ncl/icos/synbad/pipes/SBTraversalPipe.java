/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.Iterator;


/**
 * SBTraversers are retrieved using iterator methods. 
 * 
 * @author owengilfellon
 */
public interface SBTraversalPipe<T, V> extends Iterator<SBTraverser<V>>, Cloneable {

    public void addInput(SBTraverser<T> traverser);
    
    public void addInput(Iterator<SBTraverser<T>> traversers);
    
    // =======
    
    public void setPreviousPipe(SBTraversalPipe<?, T> pipe);
    
    public SBTraversalPipe<?, T> getPreviousPipe();

    public void setNextPipe(SBTraversalPipe<V, ?> pipe);
    
    public SBTraversalPipe<V, ?> getNextPipe();
    
    public SBTraversal getParentTraversal();
    
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal);
    
    
    
   
    
}
