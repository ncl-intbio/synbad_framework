/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBChoose<T, V> extends DefaultSBTraversalPipe<T, V>{

    private final Predicate<T> predicate;
    private final SBTraversal<T, V> branch1;
    private final SBTraversal<T, V> branch2;
    
    public SBChoose(SBTraversal traversal, Predicate<T> predicate, SBTraversal<T, V> branch1, SBTraversal<T, V> branch2) {
        super(traversal);
        this.predicate = predicate;
        this.branch1 = branch1;
        this.branch2 = branch2;
    }

    public SBChoose(SBTraversal parentTraversal, SBChoose<T, V> choose) {
        super(parentTraversal, choose);
        this.predicate = choose.predicate;
        this.branch1 = choose.branch1.copy();
        this.branch2 = choose.branch2.copy();
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        
        
        Collection<SBTraverser<V>> processed = new ArrayList<>();
        SBTraversal<T, V> t = predicate.test(traverser.get()) ? branch1.copy() : branch2.copy();
        t.asConfigurable().addInput(traverser);
        SBTraversalPipe<?, V> output = t.asConfigurable().getOutputPipe();
        while(output.hasNext())
            processed.add(output.next());
        
        return processed;
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBChoose<>(parentTraversal, this);
    }

    

    
    
}
