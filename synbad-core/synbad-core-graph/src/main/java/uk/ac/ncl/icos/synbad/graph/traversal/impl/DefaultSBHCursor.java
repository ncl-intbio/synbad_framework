/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;
import uk.ac.ncl.icos.synbad.graph.SBHEdgeProvider;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBHCursor<N, E> extends DefaultSBCursor<N,E> implements SBHCursor<N, E> {

    final SBHEdgeProvider<N,  E> graph;
    
    public DefaultSBHCursor(N startNode, SBHEdgeProvider<N,  E> graph) {
        super(startNode, graph);
        this.graph = graph;
    }

    @Override
    public Collection<E> getEdges(SBDirection direction) {
        switch(direction) {
            case IN :
                return graph.incomingEdges(positionStack.peek().getNode()).stream().collect(Collectors.toSet());
            case OUT :
                return graph.outgoingEdges(positionStack.peek().getNode()).stream().collect(Collectors.toSet());
        }
        
        return new HashSet<>();
    }
    
    /*
    @Override
    public Collection<E> getProxyEdges(SBEdgeDirection direction) {
        switch(direction) {
            case IN :
                return graph.incomingProxyEdges(positionStack.peek().getNode());
            case OUT :
                return graph.outgoingProxyEdges(positionStack.peek().getNode());
        }
        
        return new HashSet<>();
    }*/


    @Override
    public Collection<N> getChildren() {
        return graph.getChildren(getCurrentPosition());
    }

    @Override
    public N getParent() {   
        return graph.getParent(getCurrentPosition());
    }

    @Override
    public int getDepth() {
        return  graph.getDepth(positionStack.peek().getNode());
    }

    @Override
    public void expand() {
         graph.expand(positionStack.peek().getNode());
    }

    @Override
    public void collapse() {
         graph.contract(positionStack.peek().getNode());
    }

    @Override
    public N getOriginalEdgeSource(E e) {
       return graph.getEdgeSource(e);
    }

    @Override
    public N getOriginalEdgeTarget(E e) {
        return graph.getEdgeTarget(e);
    }



}
