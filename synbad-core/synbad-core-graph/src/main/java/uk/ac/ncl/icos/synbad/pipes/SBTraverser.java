/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public interface SBTraverser<N>  {
    
    public Collection<SBTraverserPosition> getPath();

    public N get();
    
    public N pop();
   
    public <E> SBTraverser<E> push(E instance);

    public SBTraverser<N> duplicate();
    
    // handle labelling of entities
    
    public Set<String> getLabels();
    
    public Collection<?> getByLabel(String label);

    public void addLabel(String label);
    

    public static class SBTraverserPosition<N> {

        private final N entity;
        private Set<String> labels;
        
        public SBTraverserPosition(N entity) {
            this.labels = new HashSet<>();
            this.entity = entity;
        }
        
        public SBTraverserPosition(N entity, Set<String> labels) {
            this.entity = entity;
            this.labels = labels;
        }

        public N getEntity() {
            return entity;
        }
        
        public void addLabel(String label) {
            this.labels.add(label);
        }
        
        public Set<String> getLabels() {
            return labels;
        }
    }

   
}
