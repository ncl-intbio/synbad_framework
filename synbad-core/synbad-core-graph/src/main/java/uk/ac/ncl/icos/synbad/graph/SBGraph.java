/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursorProvider;
import java.io.Serializable;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEventSource;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;

/**
 * The base graph interface for all graphs and views within SynBad. This graph
 * is a directed property graph, and all edges are labelled.
 * @author owengilfellon
 * @param <N> The base type of the objects in the graph.
 * @param <E> The base type of the edges in the graph.
 */
public interface SBGraph<N, E>
        extends SBLocalEdgeProvider<N, E>, 
                SBCursorProvider<N, E>, 
                SBEventSource, 
                Serializable {


    // ==========================
    //          Edges
    // ==========================

    /**
     * 
     * @param from the source of the edge
     * @param to the target of the edge
     * @return true, if this graph contains at least one edge between the
     * provided objects.
     */
    boolean containsEdge(N from, N to);

    /**
     * 
     * @param from the source of the edge
     * @param to the target of the edge
     * @return a single edge that exists between the two provided objects.
     */
    E getEdge(N from, N to);

    /**
     * 
     * @param node
     * @return all edges either to or from the provided object.
     */
    Set<E> getAllEdges(N node);

    /**
     * 
     * @param sourceNode
     * @param targetNode
     * @return all edges between the source object and the target object.
     */
    Set<E> getAllEdges(N sourceNode, N targetNode);

    /**
     * 
     * @return A traversal source, which allows the construction of a traversal
     * over this graph.
     */
    SBGraphTraversalSource<N, E, ? extends SBGraph> getTraversal();

}