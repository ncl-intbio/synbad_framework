/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.cursor;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 * 
 * @author owengilfellon
 */
public interface SBCursorProvider<N, E>  {
    
    /**
     * 
     * @param startNode
     * @return A cursor, that maintains a position and path within and through 
     * a graph.
     */
    SBCursor<N, E> createCursor(N startNode);
    
}
