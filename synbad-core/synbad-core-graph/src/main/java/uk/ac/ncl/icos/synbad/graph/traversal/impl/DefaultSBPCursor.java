/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursor;
import uk.ac.ncl.icos.synbad.graph.SBPGraph;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBPCursor<N, P, E> implements SBPCursor<N, P, E>
{


    protected final Stack<CursorPosition<N>> positionStack = new Stack();
    protected final SBPGraph<N, P, E> graph;

    public DefaultSBPCursor(N startNode, SBPGraph<N,P,E> graph) {
        this.graph = graph;
        placeAt(startNode);
    }

    @Override
    public Collection<N> getPath() {

        List<N> path = new ArrayList<>();

        for(Iterator<CursorPosition<N>> i = positionStack.iterator(); i.hasNext();) {
            path.add(i.next().instance);
        }

        return path;
    }


    @Override
    public N getCurrentPosition() {
        return positionStack.peek().getNode();
    }

    @Override
    public N pop() {
        return positionStack.pop().getNode();
    }


    @Override
    public void placeAt(N instance) { 
        positionStack.clear();
        positionStack.push(new CursorPosition(instance));
     }


    @Override
    public void moveTo(N instance) {
        // if // graph contains edge between this and that....

        positionStack.push(new CursorPosition(instance));
    }


    @Override
    public void moveVia(E edge) {
        
        P from  = graph.getEdgeSourcePort(edge);
        N fromOwner = graph.getPortOwner(from);
        P to = graph.getEdgeTargetPort(edge);
        N toOwner = graph.getPortOwner(to);

        if(getCurrentPosition().equals(fromOwner)) {
           positionStack.add(new CursorPosition(toOwner)); 
        } else if(getCurrentPosition().equals(toOwner)) {
           positionStack.add(new CursorPosition(fromOwner)); 
        } else {
           throw new IllegalStateException(edge.toString() + " is not incident to " + getCurrentPosition().toString());
        }
    }

    @Override
    public Collection<E> getEdges(SBDirection direction) {
        switch(direction) {

            case OUT:
                return graph.outgoingEdges(positionStack.peek().getNode());
            case IN:
                return graph.incomingEdges(positionStack.peek().getNode());

        }

        return new HashSet<>();

    }

    @Override
    public N getEdgeSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public N getEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }

    @Override
    public Collection<E> getEdgesViaPort(P port) {
        return graph.getAllEdgesByPort(port);
    }

    @Override
    public N getOwner(P port) {    
        return graph.getPortOwner(port);
    }

    @Override
    public N getParent() {
        return graph.getParent(getCurrentPosition());
    }

    @Override
    public Collection<N> getChildren() {
        return graph.getChildren(getCurrentPosition());
    }

    @Override
    public Collection<P> getPorts() {
        return graph.getPorts(getCurrentPosition());
    }

    @Override
    public int getDepth() {
        return graph.getDepth(getCurrentPosition());
    }

    @Override
    public void expand() {
         graph.expand(positionStack.peek().getNode());
    }

    @Override
    public void collapse() {
        graph.contract(positionStack.peek().getNode());
    }

    @Override
    public boolean hasProxy(P port) {
        return graph.isProxied(port);
    }

    @Override
    public boolean hasProxied(P port) {
        return graph.isProxy(port);
    }

    @Override
    public P getProxy(P port) {
        return graph.getProxy(port);
    }

    @Override
    public P getProxied(P port) {
        return graph.getProxied(port);
    }

    @Override
    public P getEdgeSourcePort(E edge) {
        return graph.getEdgeSourcePort(edge);
    }

    @Override
    public P getEdgeTargetPort(E edge) {
        return graph.getEdgeTargetPort(edge);
    }

    @Override
    public N getOriginalEdgeSource(E edge) {
        return graph.getOriginalEdgeSource(edge);
    }

    @Override
    public N getOriginalEdgeTarget(E edge) {
        return graph.getOriginalEdgeTarget(edge);
    }

    public static class CursorPosition<N> {

        private final N instance;

        public CursorPosition(N instance) {
            this.instance = instance;
        }

        public N getNode() {
            return instance;
        }
    }



}
