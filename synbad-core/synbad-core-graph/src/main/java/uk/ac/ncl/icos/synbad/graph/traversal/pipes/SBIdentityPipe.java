/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;

import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBIdentityPipe<V> extends DefaultSBTraversalPipe<V, V>{

    public SBIdentityPipe(SBTraversal traversal) {
        super(traversal);
       
    }
    
    protected SBIdentityPipe(SBTraversal traversal, SBIdentityPipe<V> identityPipe) {
        super(traversal, identityPipe);
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {
        return Collections.singleton(traverser);
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBIdentityPipe(parentTraversal, this);
    }

   
    
    
    

}
