/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;


/**
 * A SBTraversalSource initiates a traversal from an initial list of nodes
 * or edges.
 * 
 * @author owengilfellon
 */
public interface SBEventPipelineSource<N extends SBEvent> {

    /**
     * Creates an {@link SBEventPipeline} initialised with the provided {@link SBEvent}s.
     * @param events The {@link SBEvent}s that will be passed into the {@link SBEventPipeline}.
     * @return An {@link SBEventPipeline} initialised with the provided {@link SBEvent}s.
     */
    public SBEventPipeline<?, N> push(N... events);

}
