/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import java.util.function.Consumer;
import uk.ac.ncl.icos.synbad.pipes.*;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterByPredicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBWhile;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHasLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBChoose;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSelectPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBAddLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDoTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBLoop;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDeduplicate;

import java.util.function.Function;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
/**
 *
 * @author owengilfellon
 */

    
    
public class DefaultSBEventPipeline<T, V> extends ASBTraversal<T, V> implements SBEventPipeline.Configurable<T, V>{

    public DefaultSBEventPipeline() {
        super();
    }
    
    protected DefaultSBEventPipeline(DefaultSBEventPipeline<T, V> traversal) {
        super();
    }

    @Override
    public <V2> SBEventPipeline<T, V2> map(SBEventFilter predicate, Function<T, V2> mapper) {
       return addPipe(new SBMapEventPipe<>(this, predicate, mapper));
    }

    @Override
    public DefaultSBEventPipeline<T, V> as(String label) {
        return addPipe(new SBAddLabel(this, label));
    }

    @Override
    public DefaultSBEventPipeline<T, V> hasLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, false, label));
    }

    @Override
    public DefaultSBEventPipeline<T, V> hasNotLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, true, label));
    }

    @Override
    public DefaultSBEventPipeline<T, V> filter(Predicate<V> predicate) {
        return (DefaultSBEventPipeline<T, V>)addPipe(new SBFilterByPredicate<>(this, predicate));
    }

    @Override
    public SBEventPipeline<T, V> filter(SBEventFilter predicate) {
        return (DefaultSBEventPipeline<T, V>)addPipe(new SBFilterByPredicate<>(this, predicate));
    }
    

    @Override
    public <V2> DefaultSBEventPipeline<T, V2> select(String... labels) {
        return (DefaultSBEventPipeline<T, V2>)addPipe(new SBSelectPipe<>(this, labels));
    }

    @Override
    public <V2> DefaultSBEventPipeline<T, V2> select(Class<V2> clazz, String... label) {
        return (DefaultSBEventPipeline<T, V2>)addPipe(new SBSelectPipe<>(this, clazz, label));
    }
    
    @Override
    public <V2> DefaultSBEventPipeline<T, V2> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2) {
        return (DefaultSBEventPipeline<T, V2>)addPipe(new SBChoose<>(this,predicate, branch1, branch2));
    }

    @Override
    public DefaultSBEventPipeline<T, V> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits, boolean inclusive) {
        return (DefaultSBEventPipeline<T, V>)addPipe(new SBWhile<>(this,predicate, doWhileTrue, emit, revisits, inclusive));
    } 
    
    
    @Override
    public DefaultSBEventPipeline<T, V> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits) {
        return (DefaultSBEventPipeline<T, V>)addPipe(new SBWhile<V>(this,predicate, doWhileTrue, emit, revisits));
    }

    @Override
    public DefaultSBEventPipeline<T, V> loop(int iterations, SBTraversal<?, V> traversal, boolean emit) {
        return (DefaultSBEventPipeline<T, V>)addPipe(new SBLoop<>(this, traversal, iterations));
    }

    @Override
    public SBEventPipeline<T, V> deduplicate() {
        return (DefaultSBEventPipeline<T, V>)addPipe(new SBDeduplicate<>(this));
    }

    /*
        ============================================================
            Internal methods
        ============================================================
    */

    @Override
    public <E> DefaultSBEventPipeline<T, E> addPipe(SBTraversalPipe<?, E> nextPipe) {
        
        if(getPipes().size() > 0) {
            SBTraversalPipe previousPipe = getPipes().get(getPipes().size() - 1);
            previousPipe.setNextPipe(nextPipe);
            nextPipe.setPreviousPipe(previousPipe);
        }

        getPipes().add(nextPipe);

        return (DefaultSBEventPipeline<T, E>)this;
    }

    @Override
    public SBEventPipeline.Configurable<T, V> asConfigurable() {
        return (SBEventPipeline.Configurable<T,V>) this;
    }


    @Override
    public DefaultSBEventPipeline<T, V> copy() {
        final DefaultSBEventPipeline<T, V> copy = new DefaultSBEventPipeline<>(this);
        getPipes().stream().map(p -> p.copy(copy)).forEach(copy::addPipe);
        return copy;
    }

    @Override
    public DefaultSBEventPipeline<T, V> doTraversal(SBTraversal<V, V> traversal) {
        return addPipe(new SBDoTraversal<>(this, traversal));
    }

    @Override
    public SBEventPipeline<T, V> doTraversal(SBEventFilter predicate, boolean passThrough, SBTraversal<V, V> traversal) {
        return addPipe(new SBDoTraversalOnFiltered<>(this, predicate, passThrough, traversal));
    }
    
    @Override
    public SBEventPipeline<T, V> doTraversal(SBEventFilter predicate, SBTraversal<V, V> traversal) {
        return doTraversal(predicate, true, traversal);
    }

    @Override
    public SBEventPipeline<T, V> doSideEffect(SBEventFilter predicate, boolean passThrough, Consumer<V> consumer) {
        return addPipe(new SBDoFunctionOnFiltered<>(this, predicate, passThrough, consumer));
    }

    @Override
    public SBEventPipeline<T, V> doSideEffect(SBEventFilter predicate, Consumer<V> consumer) {
        return doSideEffect(predicate, true, consumer);
    }
    
    
    @Override
    public Runnable run() {
        return () -> {
            while(true) {
                try {
                    Thread.sleep(1000);
                    System.out.println("Doing batch ---->");
                    while(hasNext()) {
                        System.out.println("  " + next());
                    }
                } catch (InterruptedException ex) {
                    
                }
            }
        };
    }

    @Override
    public SBEventPipeline<T, V> subscribe(SBSubscriber subscriber) {
        return addPipe(new SBSubscribePipe<>(this, subscriber));
    }
    
    @Override
    public SBEventPipeline<T, V> subscribe(SBEventFilter filter, SBSubscriber subscriber) {
        return addPipe(new SBSubscribePipe<>(this, subscriber, filter));
    }
}


