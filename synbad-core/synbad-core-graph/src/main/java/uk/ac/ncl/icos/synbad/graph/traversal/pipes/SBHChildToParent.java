/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBHChildToParent<T extends SBValued, V extends SBValued> extends DefaultSBTraversalPipe<T, V>{

    private static final Logger logger =  LoggerFactory.getLogger(SBHChildToParent.class);
    private final Predicate<T> predicate;
     
    public SBHChildToParent(SBTraversal traversal) {
        super(traversal);
        predicate = null;
    }
    
    public SBHChildToParent(SBTraversal traversal, Predicate<T> predicate) {
        super(traversal);
        this.predicate = predicate; 
    }
    
    protected SBHChildToParent(SBTraversal parentTraversal, SBHChildToParent<T, V> traversal) {
        super(parentTraversal, traversal);
        this.predicate = traversal.predicate;
    }
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        
        logger.debug("Processing " + traverser.toString());
        
        assert(SBValued.class.isAssignableFrom(traverser.get().getClass()));
        
        SBGraph view = ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();
        
        if(SBHGraph.class.isAssignableFrom(view.getClass())) {
            T parent = ((SBHGraph<T, ?>)view).getParent(traverser.get());
            
            if(predicate == null || predicate.test(parent))
                return Collections.singleton((SBTraverser<V>)traverser.push(parent));
        }
        
        return Collections.EMPTY_SET;
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBHChildToParent<>(parentTraversal, this);
    }
}
