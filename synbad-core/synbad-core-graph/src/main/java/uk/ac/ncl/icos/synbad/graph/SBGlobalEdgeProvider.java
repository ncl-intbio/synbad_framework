/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.Set;

/**
 * Cursors operate on SBEdgeProviders, and therefore can operate over simpler
 * structures than an entire graph.
 * @author owengilfellon
 * @param <N>
 * @param <E> 
 */
public interface SBGlobalEdgeProvider<N, E> {
    
    /**
     * 
     * @return all edges in this graph.
     */
    Set<E> edgeSet();

    /**
     * 
     * @param edge
     * @return true, if this edge contains the provided edge.
     */
    boolean containsEdge(E edge);
    
}
