/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursor;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBPCrawler<N extends SBValued, P extends SBValued, E extends SBEdge, C extends SBPCursor<N, P, E>>
                    extends SBHCrawler<N, E, C>, SBPCrawlerListener<N, P, E, C> { 

}
