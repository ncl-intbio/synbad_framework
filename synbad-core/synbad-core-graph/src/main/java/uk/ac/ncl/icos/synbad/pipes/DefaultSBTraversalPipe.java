/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.Collection;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public abstract class DefaultSBTraversalPipe<T, V>  implements SBTraversalPipe<T, V> , Cloneable{

        private static final Logger logger =  LoggerFactory.getLogger(DefaultSBTraversalPipe.class);
        
        protected final SBCacheIterator<SBTraverser<T>> traversers;
        protected final SBCacheIterator<SBTraverser<V>> processedTraversers;
        
        private SBTraversalPipe<?, T> prevPipe;
        private SBTraversalPipe<V, ?> nextPipe;
        private final SBTraversal traversal;
       
        public DefaultSBTraversalPipe(SBTraversal traversal) {
            this.traversers = new SBCacheIterator<>();
            this.processedTraversers = new SBCacheIterator<>();
            this.traversal = traversal;
        }
        
        protected DefaultSBTraversalPipe(SBTraversal traversal, DefaultSBTraversalPipe<T, V> pipe) {
            this.traversers = pipe.traversers.copy();
            this.processedTraversers = pipe.processedTraversers.copy();
            this.traversal =  traversal;
        }

 
        @Override
        public SBTraversal getParentTraversal() {
            return traversal;
        }


        @Override
        public void addInput(SBTraverser<T> traverser) {
            if(traverser == null) {
                logger.warn("Cannot add null traverser");
                return;
            }
            
            this.traversers.add(traverser);
        }

        @Override
        public void addInput(Iterator<SBTraverser<T>> traversers) {
            if(traversers == null) {
                logger.warn("Cannot add null traversers");
                return;
            }
            this.traversers.add(traversers);
        }

        @Override
        public void setPreviousPipe(SBTraversalPipe<?, T> pipe) {
            prevPipe = pipe;
        }

        @Override
        public SBTraversalPipe<?, T> getPreviousPipe() {
            return prevPipe;
        }

        @Override
        public void setNextPipe(SBTraversalPipe<V, ?> pipe) {
            this.nextPipe = pipe;
        }

        @Override
        public SBTraversalPipe<V, ?> getNextPipe() {
            return nextPipe;
        }

        @Override
        public boolean hasNext() {
            
            // Need to call processTraverser on iterator and check to see if 
            // there are any results. If so, store and return true, else false
            

            if(processedTraversers.hasNext()) {
                 //logger.trace("Has Next: TRUE");
                 return true;
            }
                
            // If there is no previous pipe, traversers are supplied,
            // otherwise they come from previous pipe.
            
            Iterator<SBTraverser<T>> toProcess = (prevPipe == null) ? traversers : prevPipe; 
            
            if(toProcess == null) {
                logger.error("toProcess is null");
                return false;
            }
            
            while(toProcess.hasNext()) {
                
                Collection<SBTraverser<V>> processed = processTraverser(toProcess.next());
                if(!processed.isEmpty())
                {
                    
                    // the next traversers are added to the cache
                    
                    if(processed.size() == 1)
                        processedTraversers.add(processed.iterator().next());
                    else 
                        processedTraversers.add(processed.iterator());
                    
                 //   logger.trace("Has Next: TRUE");
                    return true;
                }
            }
           
          //  logger.trace("Has Next: FALSE");
            
            return false;        
        }
        
        
        /**
         * If single traverser return and null. If an iterator 
         * @return 
         */
        @Override
        public SBTraverser<V> next() {
      
            //  If hasNext has been called, then return the processed traverser,

            if(processedTraversers.hasNext()) {
               return processedTraversers.next();
            }
            
            //  otherwise process the next result (or return null);
  
            Iterator<SBTraverser<T>> toProcess = (prevPipe == null) ? traversers : prevPipe; 
            
            if(toProcess == null) {
                logger.error("toProcess is null");
            }

            while(toProcess.hasNext()) {
                
                Collection<SBTraverser<V>> processed = processTraverser(toProcess.next());
                if(!processed.isEmpty())
                {
                    if(processed.size() == 1)
                        processedTraversers.add(processed.iterator().next());
                    else 
                        processedTraversers.add(processed.iterator());
                    
                    return processedTraversers.next();

                }
            }
            
            logger.error("Next: NULL");
            return null;  
        }

        
        /**
         * This method is intended to be implemented by Pipe implementations. It
         * defines the mapping a single input traverser and 0-* output traversers.
         * @param traverser
         * @return 
         */
        public abstract Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser);

    }
