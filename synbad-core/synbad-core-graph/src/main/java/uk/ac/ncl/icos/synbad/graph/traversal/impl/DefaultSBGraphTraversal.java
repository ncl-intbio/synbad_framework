/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import uk.ac.ncl.icos.synbad.pipes.ASBTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterByPredicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByNotValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToEdges;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBWhile;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHasLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBChoose;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSelectPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBGraphSideEffect;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBAddLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBEdgeToVertex;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDoTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBLoop;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDeduplicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToInOutEdges;
import java.util.function.Function;
import java.util.function.Predicate;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal.Configurable;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
/**
 *
 * @author owengilfellon
 */

    
    
public class DefaultSBGraphTraversal<T, V, G extends SBGraph> extends ASBTraversal<T, V> implements Configurable<T, V, G>{

    private DefaultSBTraversalResult result;
    protected G graph;
    
    public DefaultSBGraphTraversal() {
        super();
    }
    
    public DefaultSBGraphTraversal(G graph) {
        super();
        this.graph = graph;
    }

    protected DefaultSBGraphTraversal(DefaultSBGraphTraversal<T, V, G> traversal) {
        super();
        this.graph = traversal.getGraph();
    }
    
    @Override
    public G getGraph() {
        return graph;
    }

    @Override
    public void setGraph(G graph) {
        this.graph = graph;
    }

    
    @Override
    public DefaultSBGraphTraversal<T, ? extends SBEdge, G> e() {
        return addPipe(new SBVertexToEdges(this, null));
    }

    @Override
    public DefaultSBGraphTraversal<T, ? extends SBEdge, G> e(String... predicates) {
        return addPipe(new SBVertexToEdges(this, null, predicates));
    }

    @Override
    public DefaultSBGraphTraversal<T, ? extends SBEdge, G> e(SBDirection direction) {
        return addPipe(new SBVertexToEdges(this, direction));
    }

    @Override
    public DefaultSBGraphTraversal<T, ? extends SBEdge, G> e(SBDirection direction, String... predicates) {
        return addPipe(new SBVertexToEdges(this, direction, predicates));
    }


    
    @Override
    public DefaultSBGraphTraversal<T, ? extends SBEdge, G> e(String[] inPredicates, String[] outpredicates) {
        return addPipe(new SBVertexToInOutEdges<>(this, inPredicates, outpredicates));
    }

    @Override
    public DefaultSBGraphTraversal<T, ? extends SBValued, G> v() {
        return addPipe(new SBEdgeToVertex(this));
    }
    
    @Override
    public DefaultSBGraphTraversal<T, ? extends SBValued, G> v(SBDirection direction) {
        return addPipe(new SBEdgeToVertex(this, direction));
    }

    @Override
    public <V2 extends SBValued> SBGraphTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz) {
         return addPipe(new SBEdgeToVertex(this, direction, clazz));
    }
    
    @Override
    public DefaultSBGraphTraversal<T, V, G> has(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByValues(this, key, value, additionalValues));
    }

    @Override
    public DefaultSBGraphTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByNotValues(this, key, value, additionalValues));
    }

    @Override
    public DefaultSBGraphTraversal<T, V, G> as(String label) {
        return addPipe(new SBAddLabel(this, label));
    }

    @Override
    public DefaultSBGraphTraversal<T, V, G> hasLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, false, label));
    }

    @Override
    public DefaultSBGraphTraversal<T, V, G> hasNotLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, true, label));
    }

    @Override
    public DefaultSBGraphTraversal<T, V, G> filter(Predicate<V> predicate) {
        return (DefaultSBGraphTraversal<T, V, G>)addPipe(new SBFilterByPredicate<>(this, predicate));
    }

    @Override
    public <V2> DefaultSBGraphTraversal<T, V2, G> select(String... labels) {
        return (DefaultSBGraphTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, labels));
    }

    @Override
    public <V2> DefaultSBGraphTraversal<T, V2, G> select(Class<V2> clazz, String... label) {
        return (DefaultSBGraphTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, clazz, label));
    }
    
    @Override
    public <V2> DefaultSBGraphTraversal<T, V2, G> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2) {
        return (DefaultSBGraphTraversal<T, V2, G>)addPipe(new SBChoose<>(this,predicate, branch1, branch2));
    }

    @Override
    public <X extends SBGraphTraversal> DefaultSBGraphTraversal<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits, boolean inclusive) {
        return (DefaultSBGraphTraversal<T, V, G>)addPipe(new SBWhile<>(this,predicate, doWhileTrue, emit, revisits, inclusive));
    } 
    
    
    @Override
    public <X extends SBGraphTraversal> DefaultSBGraphTraversal<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits) {
        return (DefaultSBGraphTraversal<T, V, G>)addPipe(new SBWhile<V>(this,predicate, doWhileTrue, emit, revisits));
    }

    @Override
    public <X extends SBGraphTraversal> DefaultSBGraphTraversal<T, V, G> loop(int iterations, X traversal, boolean emit) {
        return (DefaultSBGraphTraversal<T, V, G>)addPipe(new SBLoop<>(this, traversal, iterations));
    }

    @Override
    public SBGraphTraversal<T, V, G> deduplicate() {
        return (DefaultSBGraphTraversal<T, V, G>)addPipe(new SBDeduplicate<>(this));
    }
 
//    @Override
//    public <V2 extends SBIdentified> SBTraversal<T, V2, G> addV(V2 v) {
//        return (DefaultSBTraversal<T, V2, G>)addPipe(new SBAddVertex<T, V2, G>(this, v));
//    }
//
//    @Override
//    public <V2 extends SBIdentified> DefaultSBTraversal<T, V2, G> addE(Class<V2> clazz, String label, String predicate) {
//        return (DefaultSBTraversal<T, V2, G>)addPipe(new SBAddEdge<T, V2, G>(this, clazz, label, predicate));
//    }
    
    
    @Override
    public <S extends SBGraph> SBGraphTraversal<T, V, G> doFunction(Function<V, S> function) {
        return (SBGraphTraversal<T, V, G>)addPipe(new SBGraphSideEffect<>(this, function));
    }

    @Override
    public <N extends SBValued, E extends SBEdge> SBTraversalResult<N, E> getResult() {
        if(result == null)
            result = new DefaultSBTraversalResult(this);
        
        return result;
    }
    
    
    
    
    
    /*
        ============================================================
            Internal methods
        ============================================================
    */
    

    @Override
    public <E> DefaultSBGraphTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe) {
        if(getPipes().size() > 0) {
            SBTraversalPipe previousPipe = getPipes().get(getPipes().size() - 1);
            previousPipe.setNextPipe(nextPipe);
            nextPipe.setPreviousPipe(previousPipe);
        }

        getPipes().add(nextPipe);

        return (DefaultSBGraphTraversal<T, E, G>)this;
    }

    @Override
    public SBGraphTraversal.Configurable<T, V, G> asConfigurable() {
        return (SBGraphTraversal.Configurable<T,V, G>) this;
    }


    @Override
    public DefaultSBGraphTraversal<T, V, G> copy() {
        final DefaultSBGraphTraversal<T, V, G> copy = new DefaultSBGraphTraversal<>(this);
        getPipes().stream().map(p -> p.copy(copy)).forEach(copy::addPipe);
        return copy;
    }

    @Override
    public DefaultSBGraphTraversal<T, V, G> doTraversal(SBTraversal<V, V> traversal) {
        return addPipe(new SBDoTraversal<>(this, traversal));
    }



}


