/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import java.util.Arrays;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversalSource;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBHTraversalSource<N,  E, G extends SBHGraph<N,  E>> implements SBHTraversalSource<N, E,  G> {

    private final G graph;

    public DefaultSBHTraversalSource(G graph) {
        this.graph = graph;
    }

    @Override
    public SBHTraversal<N, N, G> v(N... objs) {
        DefaultSBHTraversal<N, N, G> trav = new DefaultSBHTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(objs)
            .map(n -> (SBTraverser<N>)new DefaultSBTraverser<>(n)).iterator());
        return trav;
    }

    @Override
    public SBHTraversal<E, E, G> e(E... edges) {
       DefaultSBHTraversal<E, E, G> trav = new DefaultSBHTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(edges)
            .map(e -> (SBTraverser<E>)new DefaultSBTraverser<>(e)).iterator());
        return trav;
    }
}
