/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBSetPipe<V> extends DefaultSBTraversalPipe<V, V>{

    private final String key;
    private final List<SBValue> values;
    
    public SBSetPipe(SBTraversal traversal, String key, SBValue value, SBValue... additionalValues) {
        super(traversal);
        this.key = key;
        values = new ArrayList();
        values.add(value);
        if(additionalValues != null && additionalValues.length > 0 )
            values.addAll(Arrays.asList(additionalValues));
    }
    
    protected SBSetPipe(SBTraversal traversal, SBSetPipe<V> pipe) {
        super(traversal, pipe);
        this.key = pipe.key;
        this.values = pipe.values.stream().collect(Collectors.toList());
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {
        
        Collection<SBTraverser<V>> toReturn = Collections.singleton(traverser);
        
        V v = traverser.get();
        
        if(!SBValued.class.isAssignableFrom(v.getClass())) {
            return toReturn;
        }
        
       /* values.stream().forEach((value) -> {
            ((SBValued)v).setValue(key, value);
        });
        */
        return toReturn;
        
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBSetPipe<>(parentTraversal, this);
    }

    
    

}
