package uk.ac.ncl.icos.synbad.tree;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;

import java.util.ArrayList;
import java.util.List;

import uk.ac.ncl.icos.synbad.event.DefaultSBEvent;

public class ADataModelTreeNode<T> implements ITreeNode<T> {

        private static final long serialVersionUID = 9219052584353097840L;
        private ITree<T> tree;
        private T data;
        private ITreeNode<T> parent = null;
        private final List<ITreeNode<T>> children = new ArrayList<>();
        private boolean expanded;
        //private final SBDispatcher dispatcher;

        public ADataModelTreeNode(ITree<T> tree, T data) {
         //   this.dispatcher = new DefaultEventService();
            this.expanded = true;
            this.data = data;
            this.tree = tree;
            
        }

        @Override
        public ITree<T> getTree() {
            return tree;
        }

        @Override
        public void setData(T data) {
            this.data = data;
            alert(new DefaultSBEvent<>(SBGraphEventType.MODIFIED,  data, tree, this, null));
        }

        @Override
        public T getData() {
            return data;
        }

        @Override
        public void setParent(ITreeNode<T> parent) {
            
            boolean removeFromParent = getParent() != null && getParent().getChildren().contains(this);
            boolean addToParent = parent != null && !parent.getChildren().contains(this);
            
            if(removeFromParent)
                getParent().getChildren().remove(this);
            if(addToParent)
                parent.addChild(this);
            
            this.parent = parent;
        }

        @Override
        public ITreeNode<T> getParent() {
            return parent;
        }

        @Override
        public boolean setChildren(List<ITreeNode<T>> children) {
            children.clear();
            for(ITreeNode<T> child : children) {
                addChild(child);
            }

            return true;
        }

        @Override
        public boolean addChild(ITreeNode<T> child) {
            boolean b = children.add(child);
            child.setParent(this);
            alert(new DefaultSBEvent<>(SBGraphEventType.ADDED, child, tree, this, null));
            return b;
        }

        @Override
        public void addChild(int index, ITreeNode<T> child) {
            children.add(index, child);
            child.setParent(this);
            alert(new DefaultSBEvent<>(SBGraphEventType.ADDED, child, tree, this, null));
        }

        @Override
        public List<ITreeNode<T>> getChildren() {
            return children;
        }

        @Override
        public int getHeight() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int getDepth() {
            int depth = 0;
            ITreeNode<T> parent = getParent();
            while(parent != null) {
                depth++;
                parent = parent.getParent();
            }
            return depth;
        }


        @Override
        public ITreeNode<T> duplicate() {
            ITreeNode<T> duplicate = new ADataModelTreeNode<>(tree, data);
            //duplicate.setData(data);
            duplicate.setChildren(children);
            duplicate.setParent(parent);
            return duplicate;
        }

        @Override
        public boolean hasSiblings() {
            if (parent == null) {
                return false;
            }
            return parent.getChildren().size() > 1;
        }
        
        public boolean getIsExpanded() {
            return expanded;
        }
        
        public void setIsExpanded(boolean expanded) {
            this.expanded = expanded;
        }


        @Override
        public void alert(SBEvent e)
        {
            if(parent != null)
                parent.alert(e);
            else
                tree.alert(e);
        }

        @Override
        public String toString() {
            return getData().toString();
        }
        
        
        
    }