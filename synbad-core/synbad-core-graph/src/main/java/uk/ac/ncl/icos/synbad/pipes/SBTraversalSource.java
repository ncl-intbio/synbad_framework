/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;


/**
 * A SBTraversalSource initiates a traversal from an initial list of nodes
 * or edges.
 * 
 * 
 * @author owengilfellon
 */
public interface SBTraversalSource<N> {
    
    
    /**
     * A traversal beginning at the provided objects.
     * @param <T>
     * @param objs
     * @return 
     */
    public SBTraversal<?, N> push(N... objs);
    
  
}
