/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBFilterVertexByValues<T extends SBValued, V extends SBValued> extends DefaultSBTraversalPipe<V, V>{

    private final String key;
    private final Collection<SBValue> values;

    public SBFilterVertexByValues(SBTraversal traversal, String key, Object value, Object... additionalvalues) {
        super(traversal);
        this.key = key;
        this.values = new HashSet<>();
        this.values.add(SBValue.parseValue(value));
        for(Object o : additionalvalues) {
            values.add(SBValue.parseValue(o));
        }
    }
    
    protected SBFilterVertexByValues(SBTraversal traversal, SBFilterVertexByValues<T, V> valuesPipe) {
        super(traversal, valuesPipe);
        this.key = valuesPipe.key;
        this.values = new HashSet<>(valuesPipe.values);
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {

        for(SBValue v : this.values) {
            if(traverser.get().getValues(key).contains(v))
                return Collections.singleton(traverser);
        }
        
        return Collections.EMPTY_SET;
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBFilterVertexByValues<>(parentTraversal, this);
    }
}
