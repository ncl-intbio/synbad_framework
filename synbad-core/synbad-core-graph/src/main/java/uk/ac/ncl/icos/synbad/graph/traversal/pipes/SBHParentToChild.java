/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBHParentToChild<T extends SBValued, V extends SBValued> extends DefaultSBTraversalPipe<T, V>{

    private static final Logger logger =  LoggerFactory.getLogger(SBHParentToChild.class);
    private final Predicate<T> predicate; 
    
    
    public SBHParentToChild(SBHTraversal traversal) {
        super(traversal);
        this.predicate = null;
    }
    
    public SBHParentToChild(SBHTraversal traversal, Predicate<T> predicate) {
        super(traversal);
        this.predicate = predicate;
    }
    
    protected SBHParentToChild(SBTraversal parentTraversal, SBHParentToChild<T, V> traversal) {
        super(parentTraversal, traversal);
        this.predicate = traversal.predicate;
    }
    
    
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        
        logger.debug("Processing " + traverser.toString());
        
        assert(SBValued.class.isAssignableFrom(traverser.get().getClass()));
        
        SBHGraph<T, ?> view = (SBHGraph<T, ?>) ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();
        
        return view.getChildren(traverser.get()).stream()
                .filter(child -> predicate == null ? true : predicate.test(child))
                .map(t -> (SBTraverser<V>)traverser.duplicate().push(t))
                .collect(Collectors.toSet());
        
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBHParentToChild<>(parentTraversal, this);
    }

    
    

}
