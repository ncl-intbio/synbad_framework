/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBWhile<T> extends DefaultSBTraversalPipe<T, T> {
    
    private final Predicate<T> predicate;
    private final SBTraversal<?, T> doWhileTrue;
    private final boolean emit;
    private final boolean revisit;
    private final boolean inclusive;
    
    public SBWhile(SBTraversal traversal, Predicate<T> predicate, SBTraversal<?, T> doWhileTrue) {
        super(traversal);
        this.predicate = predicate;
        this.doWhileTrue = doWhileTrue;
        this.emit = false;
        this.revisit = false;
        this.inclusive = false;
    }
    
    public SBWhile(SBTraversal traversal, Predicate<T> predicate, SBTraversal<?, T> doWhileTrue, boolean emit) {
        super(traversal);
        this.predicate = predicate;
        this.doWhileTrue = doWhileTrue;
        this.emit = emit;
        this.revisit = false;
        this.inclusive = false;
    }
    
    public SBWhile(SBTraversal traversal, Predicate<T> predicate, SBTraversal<?, T> doWhileTrue, boolean emit, boolean revisit) {
        super(traversal);
        this.predicate = predicate;
        this.doWhileTrue = doWhileTrue;
        this.emit = emit;
        this.revisit = revisit;
        this.inclusive = false;
    }
    
    public SBWhile(SBTraversal traversal, Predicate<T> predicate, SBTraversal<?, T> doWhileTrue, boolean emit, boolean revisit, boolean inclusive) {
        super(traversal);
        this.predicate = predicate;
        this.doWhileTrue = doWhileTrue;
        this.emit = emit;
        this.revisit = revisit;
        this.inclusive = inclusive;
    }
    
    protected SBWhile(SBTraversal traversal, SBWhile<T> whilePipe) {
        super(traversal, whilePipe);
        this.predicate = whilePipe.predicate;
        this.doWhileTrue = whilePipe.doWhileTrue.copy();
        this.emit = whilePipe.emit;
        this.revisit = whilePipe.revisit;
        this.inclusive = whilePipe.inclusive;
    }
   
    private Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser, Set<T> visited) {
     
        SBTraversal t = doWhileTrue.copy();
        t.asConfigurable().addInput(traverser);
        SBTraversalPipe<?, T> output = t.asConfigurable().getOutputPipe();
        Collection<SBTraverser<T>> toReturn = new ArrayList<>();
        
        // If returning nodes at each iteration of the loop, add to toReturn
        
        if(emit) {
            toReturn.add(traverser);
        }

        visited.add(traverser.get());
        
        // If there are traversers output from the loop

        while(output.hasNext()) {
            SBTraverser<T> next = output.next();
            if(predicate.test(next.get()) && (revisit || !visited.contains(next.get()))) {
                toReturn.addAll(processTraverser(next, visited));
            } else if (!emit) {
                toReturn.add(traverser);
                if(inclusive) {
                    toReturn.add(next);
                }
            }
        }
        
        return toReturn;
    }
    
    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {   
        // Passes in current traverser and returns the result of looping the doWhileTrue traversal
        return processTraverser(traverser, new HashSet<>());
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBWhile<>(parentTraversal, this);
    }
}
