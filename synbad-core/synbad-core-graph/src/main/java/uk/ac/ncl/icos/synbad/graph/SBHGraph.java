/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursorProvider;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHIterator;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversalSource;

/**
 * The base graph interface for hierarchical graphs and views. This graph
 * is a directed property graph, and all edges are labelled. Additionally, all
 * nodes objects can have children, placing them in a hierarchical structure.
 * 
 * @author owengilfellon
 * @param <N> The base type of the objects in the graph.
 * @param <E> The base type of the edges in the graph.
 */
public interface SBHGraph<N,  E> extends SBGraph<N, E>, SBHEdgeProvider<N, E>, SBHCursorProvider<N, E>{

    /**
     * Returns true if no ancestors are contracted.
     *
     * @param node
     * @return
     */
    public boolean isVisible(N node);

    // ================= Edges =================

    /**
     * 
     * @param from
     * @param to
     * @param edge
     * @return returns true if the provided edge exists between the two
     * provided objects.
     */
    public boolean containsEdge(N from, N to, E edge);

    /**
     * 
     * @return All edges in the graph, whether they are visible are not.
     */
    public Set<E> getAllEdges();

    // ================= Hierarchy =================
    
    /**
     * 
     * @return the root object of this graph.
     */
    public N getRoot();

    /**
     * 
     * @param node
     * @param descendant
     * @return true if the provided node is an ancestor of the descendant object.
     */
    public boolean isDescendant(N node, N descendant);

    /**
     * 
     * @param node
     * @return true if the provided object's parent has children in
     * addition to the provided object.
     */
    public boolean hasSiblings(N node);

    /**
     * 
     * @param node
     * @return all objects that are also children of the provided object's
     * parent.
     */
    public List<N> getSiblings(N node);

    /**
     * Is this redundant?
     * @param node
     * @param ancestor
     * @return true if the provided node is an descendant of the ancestor object.
     */
    public boolean isAncestor(N node, N ancestor);

    /**
     * Adds the provided object as a child of the provided parent.
     * @param parent
     * @param node the child to be added to the parent.
     */
    public void setParent(N parent, N node);

    // ================= Iteration =================
    
    /**
     * 
     * @param instance
     * @return an depth-first iterator across the hierarchical structure of 
     * this graph.
     */
    public SBHIterator<N> hierarchyIterator(N instance);

    /**
     * 
     * @return A traversal source, which allows the construction of a traversal
     * over this graph.
     */
    @Override
    SBHTraversalSource<N, E, ? extends SBHGraph> getTraversal();

}
