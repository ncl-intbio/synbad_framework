/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.tree;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;


/**
 *
 * @author owengilfellon
 */
public interface ITree<T>  extends Serializable {

    ITreeNode<T> getRootNode();
    ITreeNode<T> findNode(T data);
    Set<ITreeNode<T>> findAllNodes(T data);
    void setRootNode(ITreeNode<T> rootNode);
    Iterator<T> iterator();
    void alert(SBEvent e);
    


}
