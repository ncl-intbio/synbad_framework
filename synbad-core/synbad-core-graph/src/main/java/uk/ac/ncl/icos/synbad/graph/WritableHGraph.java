/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owen
 */
public interface WritableHGraph<N, E> extends SBHGraph<N, E> {
    
    // mutator
    
    boolean addNode(N instance, N parent);

    boolean addNode(N instance, N parent, int index);
    
    boolean removeNode(N node);

    boolean removeAllNodes(Collection<? extends N> nodes);
    
    boolean addEdge(N from, N to, E edge);
    
    /**
     * Removes non-proxy edges. Proxy edges are calculated automatically.
     *
     * @param edge
     * @return
     */
    boolean removeEdge(E edge);
    
    boolean removeAllEdges(Collection<? extends E> edges);

    E removeEdge(N sourceNode, N targetNode);
    
    Set<E> removeAllEdges(N sourceNode, N targetNode);
    
}
