/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;


/**
 * An SBTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface SBEventPipeline<T, V> extends SBTraversal<T, V>, Iterator<V> {
    
    // Event-specific methods
    
    SBEventPipeline<T, V> filter(SBEventFilter predicate);
    
    /**
     * Conditionally performs a traversal on events that are accepted by an
     * event filter.
     * @param predicate event filter
     * @param passThrough if false, events that fail the filter are discarded. If
     * true, then events that fail the filter are passed through the pipe without
     * any further processing
     * @param traversal to be performed if event filter returns true
     * @return the {@link SBEventPipeline} for further method chaining
     */

    SBEventPipeline<T, V> doTraversal(SBEventFilter predicate, boolean passThrough, SBTraversal<V, V> traversal);
    
    /**
     * Conditionally performs a traversal on events that are accepted by an
     * event filter. Events that are not accepted are passed through without
     * any further processing.
     * @param predicate event filter
     * @param traversal to be performed if event filter returns true
     * @return the {@link SBEventPipeline} for further method chaining
     */
    SBEventPipeline<T, V> doTraversal(SBEventFilter predicate, SBTraversal<V, V> traversal);
    
    
    /**
     * Conditionally performs a function on events that are accepted by an
     * event filter.
     * @param predicate event filter
     * @param passThrough if false, events that fail the filter are discarded. If
     * true, then events that fail the filter are passed through the pipe without
     * any further processing
     * @return the {@link SBEventPipeline} for further method chaining
     */

    SBEventPipeline<T, V> doSideEffect(SBEventFilter predicate, boolean passThrough, Consumer<V> consumer);
    
    /**
     * Conditionally performs a function on events that are accepted by an
     * event filter. Events that are not accepted are passed through without
     * any further processing.
     * @param predicate event filter
     * @return the {@link SBEventPipeline} for further method chaining
     */
    SBEventPipeline<T, V> doSideEffect(SBEventFilter predicate, Consumer<V> consumer);
    
    
    Runnable run();
     
    // Overriding existing methods

    <V2> SBEventPipeline<T, V2> map(SBEventFilter predicate, Function<T,V2> mapper);
    
    <V2> SBEventPipeline<T, V2> select(String... label);
    
    <V2> SBEventPipeline<T, V2> select(Class<V2> clazz, String... label);
    
    <V2> SBEventPipeline<T, V2> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2);
    
    SBEventPipeline<T, V> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits, boolean inclusive);
    
    SBEventPipeline<T, V> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits);
    
    SBEventPipeline<T, V> loop(int iteratons, SBTraversal<?, V> traversal, boolean emit);
    
    SBEventPipeline<T, V> filter(Predicate<V> predicate);
    
    SBEventPipeline<T, V> deduplicate();
    
    SBEventPipeline<T, V> as(String label);

    SBEventPipeline<T, V> hasLabel(String... label);

    SBEventPipeline<T, V> hasNotLabel(String... label);
    
    SBEventPipeline<T, V> doTraversal(SBTraversal<V, V> traversal);

    @Override
    SBEventPipeline<T, V> copy();
    
    /**
     * Dispatches all events to the provided subscriber.
     * @param subscriber receives all events that pass through this stage
     * of the pipeline
     * @return the {@link SBEventPipeline} for further method chaining
     */
    SBEventPipeline<T, V> subscribe(SBSubscriber subscriber);
    
    /**
     * Dispatches filtered events to the provided subscriber.
     * @param filter an {@link SBEventFilter} to determine which events are 
     * dispatched to the provided subscriber
     * @param subscriber receives filtered events that pass through this stage
     * of the pipeline
     * @return the {@link SBEventPipeline} for further method chaining
     */
    SBEventPipeline<T, V> subscribe(SBEventFilter filter, SBSubscriber subscriber);
    
  
    // ==============================
    //        Configuration
    // ==============================
    
    // Use of a extended interface for concealing construction methods inspired
    // by TinkerPops traversal code. 
    
    @Override
    SBEventPipeline.Configurable<T, V> asConfigurable();

    interface Configurable<T, V> extends SBTraversal.Configurable<T, V>, SBEventPipeline<T, V> {


        @Override
        <E> SBEventPipeline<?, E> addPipe(SBTraversalPipe<?, E> step);

     }
    
}
