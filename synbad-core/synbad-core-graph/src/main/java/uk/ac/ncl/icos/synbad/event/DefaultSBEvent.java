/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event;

import uk.ac.ncl.icos.synbad.api.event.*;

import java.net.URI;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBEvent<D, S, P> implements SBEvent<D>, SBEvtWithSrc<S>, SBEvtWithParent<P>, SBEvtWithContext {

    private final URI[] contexts;
    private final SBEventType type;
    
    private final D data;
    private final Class dataClass;
    
    private final S source;
    private final Class sourceClass;
    
    private final P parent;
    private final Class parentClass;
    
    public DefaultSBEvent(
            SBEventType type, 
            D data) {
        this(type, data, null, null, null);
    }
    
    public DefaultSBEvent(
            SBEventType type, 
            D data, 
            S source, 
            P parent, 
            URI[] contexts) {
        this(type, data, data.getClass(), 
                source, source != null ? source.getClass() : null, 
                parent, parent != null ? parent.getClass() : null, 
                contexts);
    }

    public DefaultSBEvent(
            SBEventType type,
            D data,
            Class dataClass,
            S source,
            Class sourceClass,
            P parent,
            Class parentClass,
            URI[] contexts) {
        this.type = type;
        this.data = data;
        this.dataClass = dataClass;
        this.source = source;
        this.sourceClass = sourceClass;
        this.parent = parent;
        this.parentClass = parentClass;
        this.contexts = contexts;
    }

    @Override
    public URI[] getContexts() {
        return contexts;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder(type.toString());
        b.append(" - ").append(data.toString()).append(":").append(dataClass.getSimpleName());
        if(source != null)
            b.append("[").append(source).append(":").append(sourceClass.getSimpleName()).append("]");
        return b.toString();
    }

    @Override
    public SBEventType getType() {
        return type;
    }

    @Override
    public D getData() {
        return data;
    }

    @Override
    public Class getDataClass() {
        return dataClass;
    }

    @Override
    public S getSource() {
        return source;
    }

    @Override
    public Class getSourceClass() {
        return sourceClass;
    }

    @Override
    public P getParent() {
        return parent;
    }

    @Override
    public Class getParentClass() {
        return parentClass;
    }
    
    
}
