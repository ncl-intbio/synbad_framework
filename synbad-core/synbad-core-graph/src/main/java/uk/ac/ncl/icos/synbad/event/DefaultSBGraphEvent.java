/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBGraphEvent<D, S, P> extends DefaultSBEvent<D, S, P> implements SBGraphEvent<D> {

    private final SBGraphEntityType graphEntityType;

    public DefaultSBGraphEvent(
            SBGraphEntityType graphEntityType, 
            SBEventType type,
            D data) {
        super(type, data);
        this.graphEntityType = graphEntityType;
    }
    
    public DefaultSBGraphEvent(
            SBEventType type, 
            SBGraphEntityType graphType, 
            D data, 
            S source, 
            P parent, 
            URI[] contexts) {
        super(type, data, source, parent, contexts);
        this.graphEntityType = graphType;
        
    }

    public DefaultSBGraphEvent(
            SBEventType type, 
            SBGraphEntityType graphType, 
            D data, 
            Class dataClass, 
            S source, 
            Class sourceClass, 
            P parent, 
            Class parentClass, 
            URI[] contexts) {
        super(type, data, dataClass, source, sourceClass, parent, parentClass, contexts);
        this.graphEntityType = graphType;
    }
    
    @Override
    public String toString() {

        StringBuilder b = new StringBuilder(getType().toString());

        b.append("\t").append(getDataClass().getSimpleName()).append(":").append(getData().toString());

        if(getParent() != null) {
            b.append(" P[").append(getParent());
            if(getParentClass() != null) {
                b.append(":").append(getParentClass().getSimpleName());
            } else {
                b.append("! ! !");
            }
            b.append("]");
        }

        if(getSource() != null)
            b.append(" S[").append(getSource()).append(":").append(getSourceClass().getSimpleName()).append("]");

        return b.toString();
    }

    @Override
    public SBGraphEntityType getGraphEntityType() {
        return graphEntityType;
    }

}
