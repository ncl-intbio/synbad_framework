/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import uk.ac.ncl.icos.synbad.pipes.*;
import java.util.Arrays;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBEventPipelineSource<N extends SBEvent> implements SBEventPipelineSource<N> {


    public DefaultSBEventPipelineSource() {
    }

    @Override
    public SBEventPipeline<N, N> push(N... objs) {
        DefaultSBEventPipeline<N, N> trav = new DefaultSBEventPipeline<>();
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(objs)
            .map(n -> (SBTraverser<N>)new DefaultSBTraverser<>(n)).iterator());
        return trav;
    }

}
