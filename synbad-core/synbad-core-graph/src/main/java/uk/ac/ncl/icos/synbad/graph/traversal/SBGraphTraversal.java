/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import java.util.Iterator;
import java.util.function.Function;
import java.util.function.Predicate;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;

/**
 * An SBTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface SBGraphTraversal<T, V, G extends SBGraph> extends SBTraversal<T, V>, Iterator<V> {
    
    // ==============================
    //      Traversal methods
    // ==============================
    
    public <T extends SBValued, V2 extends SBEdge> SBGraphTraversal<V, V2, G> e();
    
    public <V2 extends SBEdge> SBGraphTraversal<T, V2, G> e(SBDirection direction);
    
    public <V2 extends SBEdge> SBGraphTraversal<T, V2, G> e(String... predicates);
    
    public <V2 extends SBEdge> SBGraphTraversal<T, V2, G> e(SBDirection direction, String... predicates);
    
    public <V2 extends SBEdge> SBGraphTraversal<T, V2, G> e(String[] inPredicates, String[] outpredicates);

    
    public <T extends SBEdge, V2 extends SBValued> SBGraphTraversal<T, V2, G> v();
    
    public <T extends SBEdge, V2 extends SBValued> SBGraphTraversal<T, V2, G> v(SBDirection direction);
    
    public <V2 extends SBValued> SBGraphTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz);

    
    public <V2> SBGraphTraversal<T, V2, G> select(String... label);
    
    public <V2> SBGraphTraversal<T, V2, G> select(Class<V2> clazz, String... label);
    
    public <V2> SBGraphTraversal<T, V2, G> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2);

    public <X extends SBGraphTraversal> SBGraphTraversal<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits, boolean inclusive);
    
    public <X extends SBGraphTraversal> SBGraphTraversal<T, V, G> loop(Predicate<V> predicate, X doWhileTrue, boolean emit, boolean revisits);
    
    public <X extends SBGraphTraversal> SBGraphTraversal<T, V, G> loop(int iteratons, X traversal, boolean emit);
    
    public SBGraphTraversal<T, V, G> filter(Predicate<V> predicate);

    public SBGraphTraversal<T, V, G> deduplicate();
    
    public SBGraphTraversal<T, V, G> has(String key, Object value, Object... additionalValues);
    
    public SBGraphTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues);
    
    public SBGraphTraversal<T, V, G> as(String label);

    public SBGraphTraversal<T, V, G> hasLabel(String... label);

    public SBGraphTraversal<T, V, G> hasNotLabel(String... label);
    
    public SBGraphTraversal<T, V, G> doTraversal(SBTraversal<V, V> traversal);

    public <S extends SBGraph> SBGraphTraversal<T, V, G> doFunction(Function<V, S> function);

    
    // ==============================
    //      Mutator methods
    // ==============================

    public <N extends SBValued, E extends SBEdge> SBTraversalResult<N, E> getResult();
    
    @Override
    public SBGraphTraversal<T, V, G> copy();
    
    // ==============================
    //        Configuration
    // ==============================
    
    // Use of a extended interface for concealing construction methods inspired
    // by TinkerPops traversal code. 
    
    @Override
    public SBGraphTraversal.Configurable<T, V, G> asConfigurable();

    public interface Configurable<T, V, G extends SBGraph> extends SBTraversal.Configurable<T, V>, SBGraphTraversal<T, V, G> {
        
        public G getGraph();
        
        public void setGraph(G graph);

        @Override
        public <E> SBGraphTraversal<?, E, G> addPipe(SBTraversalPipe<?, E> step);

     }
    
}
