/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBLoop<T> extends DefaultSBTraversalPipe<T, T>{

    private static final Logger logger =  LoggerFactory.getLogger(SBLoop.class);
    
    private final SBTraversal doWhileTrue;
    private final int loops;
    private final boolean emit;
    
    public SBLoop(SBTraversal traversal, SBTraversal doWhileTrue, int loops) {
        super(traversal);
        this.doWhileTrue = doWhileTrue;
        this.emit = false;
        this.loops = loops;
    }
    
    public SBLoop(SBTraversal traversal, SBTraversal doWhileTrue, int loops, boolean emit) {
        super(traversal);
        this.doWhileTrue = doWhileTrue;
        this.emit = emit;
        this.loops = loops;
    }
    
    protected SBLoop(SBTraversal traversal, SBLoop<T> loop) {
        super(traversal, loop);
        this.doWhileTrue = loop.doWhileTrue.copy();
        this.loops = loop.loops;
        this.emit = loop.emit;
    }
   
    private Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser, int iteration) {

        if(iteration >= loops)
            return Collections.singleton(traverser);
        
        Collection<SBTraverser<T>> processed = new ArrayList<>();
        SBTraversal t = doWhileTrue.copy();
        t.asConfigurable().addInput(traverser);
        SBTraversalPipe<?, T> output = t.asConfigurable().getOutputPipe();
        while(output.hasNext()) {
            processed.addAll(processTraverser(output.next(), (iteration + 1)));
        }

        return processed;
    }
    
    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {
        logger.debug("Processing " + traverser);
        return processTraverser(traverser, 0);  
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBLoop<>(parentTraversal, this);
    }

}
