/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.cursor;


import java.util.Collection;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBPCursor<N,  P, E> extends SBHCursor<N, E> {

    public Collection<N> getPath();

    public N getCurrentPosition();

    public N pop();

    public void placeAt(N instance);

    public void moveTo(N instance);

    public void moveVia(E edge);

    public Collection<E> getEdges(SBDirection direction);
    
    public N getEdgeSource(E edge);
    
    public N getEdgeTarget(E edge);
    
    public N getParent();
    
    public Collection<N> getChildren();
    
    public int getDepth();

    public void expand();
    
    public void collapse();
    
    public Collection<P> getPorts();
    
    public Collection<E> getEdgesViaPort(P port);
    
    public boolean hasProxy(P port);
   
    public boolean hasProxied(P port);
    
    public P getProxy(P port);
   
    public P getProxied(P port);
    
    public N getOwner(P port);
    
    public P getEdgeSourcePort(E edge);
    
    public P getEdgeTargetPort(E edge);
    
    
}
