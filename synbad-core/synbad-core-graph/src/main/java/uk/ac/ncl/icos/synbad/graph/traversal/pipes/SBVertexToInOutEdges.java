/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBLocalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBVertexToInOutEdges<T extends SBValued> extends DefaultSBTraversalPipe<T, SBEdge>{

    private static final Logger logger =  LoggerFactory.getLogger(SBVertexToInOutEdges.class);
    
    private final List<String> inPredicates;
    private final List<String> outPredicates;
    
    public SBVertexToInOutEdges(SBTraversal traversal, String[] inPredicates, String[] outPredicates) {
        super(traversal);
        this.inPredicates = Arrays.asList(inPredicates);
        this.outPredicates =  Arrays.asList(outPredicates);
    }
    
    protected SBVertexToInOutEdges(SBTraversal traversal, SBVertexToInOutEdges<T> vertexToEdges) {
        super(traversal, vertexToEdges);
        this.inPredicates = vertexToEdges.inPredicates.stream().collect(Collectors.toList());
        this.outPredicates = vertexToEdges.outPredicates.stream().collect(Collectors.toList());
    }
    
    @Override
    public Collection<SBTraverser<SBEdge>> processTraverser(SBTraverser<T> traverser) {

        SBLocalEdgeProvider<SBValued, SBEdge> view = ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();

        return  Stream.concat(
            view.incomingEdges(traverser.get()).stream().parallel()
                .filter(e -> inPredicates.isEmpty() || inPredicates.contains(((SBEdge<URI, URI>)e).getEdge().toASCIIString())),
            view.outgoingEdges(traverser.get()).stream().parallel()
                .filter(e -> outPredicates.isEmpty() || outPredicates.contains(((SBEdge<URI, URI>)e).getEdge().toASCIIString())))
            .parallel().map(e -> traverser.duplicate().push(e))
            .collect(Collectors.toSet());
    }

    @Override
    public SBTraversalPipe<T, SBEdge> copy(SBTraversal parentTraversal) {
        return new SBVertexToInOutEdges<>(parentTraversal, this);
    }

    
    
    

}
