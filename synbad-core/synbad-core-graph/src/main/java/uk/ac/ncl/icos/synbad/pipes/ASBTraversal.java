/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
 public abstract class ASBTraversal<T, V> implements SBTraversal.Configurable<T, V>, Iterator<V> {

    protected final List<SBTraversalPipe> pipes;

    public ASBTraversal() {
        this.pipes = new ArrayList<>();
    }

    @Override
    public boolean hasNext() {
        return getOutputPipe().hasNext();
    }

    @Override
    public V next() {
        return getOutputPipe().next().get();
    }

    @Override
    public List<SBTraversalPipe> getPipes() {
        return pipes;
    }

    @Override
    public SBTraversalPipe<T, ?> getInputPipe() {
        return !pipes.isEmpty() ? pipes.get(0) : null;
    }

    @Override
    public SBTraversalPipe<?, V> getOutputPipe() {
        return !pipes.isEmpty() ? pipes.get(pipes.size() - 1) : null;
    }
    
    private void push() {
    }

    @Override
    public  void addInput(SBTraverser<T> input) {
        getInputPipe().addInput(input);
    }

    @Override
    public void addInput(Iterator<SBTraverser<T>> input) {
        getInputPipe().addInput(input);
    }
 
}
