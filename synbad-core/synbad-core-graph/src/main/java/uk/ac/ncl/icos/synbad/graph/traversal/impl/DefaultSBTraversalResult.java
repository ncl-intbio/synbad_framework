/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultSBGraph;
import uk.ac.ncl.icos.synbad.tree.ADataModelTreeNode;
import uk.ac.ncl.icos.synbad.tree.ATree;
import uk.ac.ncl.icos.synbad.tree.ITree;
import uk.ac.ncl.icos.synbad.tree.ITreeNode;
import uk.ac.ncl.icos.synbad.api.domain.SBEntity;
import uk.ac.ncl.icos.synbad.graph.traversal.SBSubGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBTraversalResult<N extends SBValued, E extends SBEdge> implements SBTraversalResult<N,E>{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBTraversalResult.class);
    private final SBGraphTraversal.Configurable traversal;
    private final List<SBEntity> list = new ArrayList<>();
    private final ITree<SBEntity> tree = new ATree<>();
    private final Set<SBSubGraph<N, E>> subgraphs = new HashSet<>();
    private final SBGraph<N, E> sourceGraph;
    
    private final List<SBTraverser> traversers = new ArrayList<>();

    private boolean isProcessed = false;
    
    public <V extends SBEntity,T extends SBEntity, G extends SBGraph<N, E>> DefaultSBTraversalResult(SBGraphTraversal.Configurable<V, T, G> traversal) {
        this.traversal = traversal;
        this.sourceGraph = traversal.getGraph();
    }
    
    private <V extends SBEntity,T extends SBEntity, G extends SBGraph<N, E>> boolean processResult(SBGraphTraversal.Configurable<V, T, G> traversal) {

        if(isProcessed) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Result has already been processed");
            return false;
        } else if(!traversal.hasNext()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Nothing to process");
            return false;
        }

        while(traversal.getOutputPipe().hasNext()) {
         //   LOGGER.debug("Processing traverser {}", ++traverserCount);
            SBTraverser t = traversal.getOutputPipe().next();
            traversers.add(t);
        }
        
        isProcessed = true;
        
        return true;

    }

    @Override
    public int getTraverserCount() {
        processResult(traversal);
        return traversers.size();
    }
    
    
    
    private boolean processSubGraph(SBTraverser traverser) {
    
        Collection<SBTraverser.SBTraverserPosition> path = traverser.getPath();
        Iterator<SBTraverser.SBTraverserPosition> it = path.iterator();
        Map<String, Set<SBEntity>> labels = new HashMap<>();
        Set<N> nodes = new HashSet<>();
        Set<E> edges = new HashSet<>();

        while(it.hasNext()) {

            SBTraverser.SBTraverserPosition p = it.next();
            SBEntity obj = (SBEntity) p.getEntity();
            Set<String> nodeLabels = p.getLabels();
            
            if(SBValued.class.isAssignableFrom(obj.getClass()))  {
                nodes.add((N)obj);
            //    LOGGER.debug("Added subgraph node: [ {} ]", obj);
            }
            else if(SBEdge.class.isAssignableFrom(obj.getClass())) {
                edges.add((E)obj);
            //    LOGGER.debug("Added subgraph edge: [ {} ]", obj);
            }
            
            for(String nl : nodeLabels) {
                if(!labels.containsKey(nl)) {
                    labels.put(nl, new HashSet<>());
                }
                
                labels.get(nl).add(obj);
            }
        }
        
         SBSubGraph<N, E> subGraph = new DefaultSBSubGraph<>(nodes, edges, labels, sourceGraph);
         subgraphs.add(subGraph);
        
       
        return true;
    }
    
    private boolean processTree(ITree<SBEntity> tree, SBTraverser traverser) {

        Collection<SBTraverser.SBTraverserPosition> path = traverser.getPath();
      //  LOGGER.debug("Processing tree of depth {}", path.size());
        Iterator<SBTraverser.SBTraverserPosition> it = path.iterator();
        ITreeNode<SBEntity> parent = null;

        while(it.hasNext()) {

            SBTraverser.SBTraverserPosition c = it.next();

            // If this is the first path, we'll need to set the tree's root node

            if ( parent == null ) {   
                if(tree.getRootNode() == null) {
       //             LOGGER.debug("...setting root node: " + (SBEntity)c.getEntity());
                    tree.setRootNode(new ADataModelTreeNode<>(tree, (SBEntity)c.getEntity()));
                }
                parent = tree.getRootNode();
            }

            // If we are past the root node

            else  {

                // Check to see if we need to add...

                ITreeNode<SBEntity> existing = null;
                for ( ITreeNode<SBEntity> child : parent.getChildren() ) {
                    if(child.getData().equals(c.getEntity())) {
                        existing = child;
                    }
                } 

                if(existing != null) {
                   // LOGGER.debug("...using existing node: " + existing.getData());
                    parent = existing;
                } else {
     //               LOGGER.debug("...adding new node: " + c.getEntity());
                    ITreeNode<SBEntity> n = new ADataModelTreeNode<>(tree, (SBEntity)c.getEntity());
                    parent.addChild(n);
                    parent = n;
                }

            }

        }

        return true;
    }
    
    

    @Override
    public <E> Map<String, Set<E>> getLabelledEntities(Class<E> clazz) {

        processResult(traversal);
        
        if(subgraphs.isEmpty()) {
            for(SBTraverser t : traversers) {
                processSubGraph(t);
            }
        }
        
        Map<String, Set<E>> entities = new HashMap<>();
        
        subgraphs.stream().forEach(g ->  {
            
            for(String label : g.getLabels()) {
                g.getLabelledEntities(label).stream().filter(b -> clazz.isAssignableFrom(b.getClass())).map(b-> (E)b).forEach(e -> {
                
                    if(!entities.containsKey(label))
                        entities.put(label,new HashSet<>());
                    
                    entities.get(label).add(e);
                });
            }
        });

        return entities;
    }
    
    @Override
    public <E> Set<E> getLabelledEntities(Class<E> clazz, String label) {
        
        processResult(traversal);

        if(subgraphs.isEmpty()) {
            for(SBTraverser t : traversers) {
                processSubGraph(t);
            }
        }
        
        Set<E> entities = new HashSet<>();
        
        subgraphs.stream().forEach(g ->  {
            
            g.getLabelledEntities(label).stream().filter(b -> clazz.isAssignableFrom(b.getClass())).map(b-> (E)b).forEach(e -> {

                entities.add(e);
            });
     
        });

        return entities;
    }
    
    @Override
    public Set<SBSubGraph<N, E>> getMatches() {
        
        processResult(traversal);
        
        if(subgraphs.isEmpty()) {
            for(SBTraverser t : traversers) {
                processSubGraph(t);
            }
        }
        return subgraphs;
    }

    @Override
    public ITree<SBEntity> getTree() {
        
        processResult(traversal);
        
        if(tree.getRootNode() == null) {
            for(SBTraverser t : traversers) {
                processTree(tree, t);
            }
        }
        
        return tree;
    }

    @Override
    public Stream<SBEntity> stream() {
        
        processResult(traversal);
        
        if(list.isEmpty()) {
            for(SBTraverser t : traversers) {
                list.add((SBEntity)t.get());
            }
            
        }
        
        return list.stream();
    }

    @Override
    public Stream<N> streamVertexes() {
        return stream().filter(b -> SBValued.class.isAssignableFrom(b.getClass())).map(n -> (N)n);
    }

    @Override
    public Stream<E> streamEdges() {
       return stream().filter(b -> SBEdge.class.isAssignableFrom(b.getClass())).map(e -> (E)e);
    }

    @Override
    public SBGraph<N, E> toGraph() {
        
        DefaultSBGraph<N, E> completeGraph = new DefaultSBGraph<>();

        for(SBTraverser<?> t : traversers) {
            for(SBTraverser.SBTraverserPosition cp : t.getPath()) {
                SBEntity next = (SBEntity) cp.getEntity();
                if(SBValued.class.isAssignableFrom(next.getClass())) {
                    completeGraph.addNode((N)next);
                } else if (SBEdge.class.isAssignableFrom(next.getClass())) {
                    N source = sourceGraph.getEdgeSource((E)next);
                    N target = sourceGraph.getEdgeTarget((E)next);
                    if(source != null && target != null)
                        completeGraph.addEdge(source, target, (E)next);
                } else {
                    LOGGER.warn("{} is not an SBValued or SBEdge", next.toString());
                }
            }
        }

        return completeGraph;
    }

}
