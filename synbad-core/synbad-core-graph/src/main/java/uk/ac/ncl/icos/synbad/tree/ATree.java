package uk.ac.ncl.icos.synbad.tree;

import com.google.common.collect.HashMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;

import java.util.*;

public class ATree<T> implements ITree<T> {

        private static final long serialVersionUID = 7849440874544428642L;
        private final HashMultimap<T, ITreeNode<T>> map = HashMultimap.create();
        private ITreeNode<T> rootNode = null;
        private static Logger logger = LoggerFactory.getLogger(ATree.class);

        public ATree() {
        }

        public ATree(ITree<T> tree, ITreeNode<T> root) {
            ITreeNode<T> rootNode = new ADataModelTreeNode<>(this, root.getData());
            setRootNode(rootNode);
            addDescendants(tree, root.getData());
        }
        
        private void addDescendants(ITree<T> tree, T node) {
            
            Set<ITreeNode<T>> allNodeA = tree.findAllNodes(node);
            ITreeNode<T> nodeB = findAllNodes(node).stream().findFirst().orElse(null);

            for(ITreeNode<T> nodeA : allNodeA) {
                for(ITreeNode<T> childA : nodeA.getChildren()) {
                    nodeB.addChild(new ADataModelTreeNode<>(tree, childA.getData()));
                    addDescendants(tree, childA.getData());
                }
            }
        }

        @Override
        public ITreeNode<T> getRootNode() {
            return rootNode;
        }


        @Override
        public ITreeNode<T> findNode(T data) {
            Set<ITreeNode<T>> nodes =  findAllNodes(data);
            if(nodes.isEmpty())
                return null;
            return nodes.iterator().next();
        }

        @Override
        public Set<ITreeNode<T>> findAllNodes(T data) {
            return map.get(data);
        }

        @Override
        public void setRootNode(ITreeNode<T> rootNode) {
            this.rootNode = rootNode;
            map.put(rootNode.getData(), rootNode);
//            rootNode.subscribe(new SBEventFilter.DefaultFilter(), this);
        }
        
//        @Override
//        public void registerNode(ITreeNode<T> node) {
//            map.put(node.getData(), node);
//        }
//        
//        @Override
//        public void deregisterNode(ITreeNode<T> node) {
//            map.remove(node.getData(), node);
//        }

        @Override
        public void alert(SBEvent event) {
            if(event.getType() == SBGraphEventType.ADDED) {
                addNodeChildren(rootNode);
                //alert(event);
            }  else if(event.getType() == SBGraphEventType.REMOVED) {
                removeNodeChildren(rootNode);
                //alert(event);
            } 
        }
        
        private void addNodeChildren(ITreeNode<T> node) {  

            for(ITreeNode<T> child : node.getChildren()) {
                map.put(child.getData(), child);
                addNodeChildren(child);
            }
        }
        
        private void removeNodeChildren(ITreeNode<T> node) {
            for(ITreeNode<T> child : node.getChildren()) {
                map.remove(child.getData(), node);
                removeNodeChildren(child);
            }
        }

        @Override
        public Iterator<T> iterator() {
            return new PreOrderIterator(rootNode);
        }
        

//        public Iterator<T> iterator(T node) {
//            return new PreOrderIterator(findNode(node));
//        }

    class PreOrderIterator implements Iterator<T> {

        private final Set<ITreeNode<T>> explored = new HashSet<>();
        private final ITreeNode<T> rootNode;
        private ITreeNode<T> currentNode = null;
        private int depth = 0;
        private final Logger logger = LoggerFactory.getLogger(PreOrderIterator.class);

        PreOrderIterator(ITreeNode<T> node) {
            rootNode = node;
        }

        public int getDepth()
        {
            return depth;
        }

        @Override
        public boolean hasNext() {

            if(currentNode == null && rootNode != null){
                //logger.trace("Has Next: TRUE: Has root node");
                return true;
            }

            if(currentNode!=null) {
                if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                    //logger.trace("Has Next: TRUE: Has unexplored children");
                    return true;
                }

                if(currentNode.hasSiblings()) {
                    if(!explored.containsAll(currentNode.getParent().getChildren())){
                        //logger.trace("Has Next: TRUE: Has unexplored siblings");
                        return true;
                    }
                }

                ITreeNode<T> tempNode = currentNode;

                while(tempNode.getParent() != null) {

                    tempNode = tempNode.getParent();

                    if(!explored.containsAll(tempNode.getChildren())) {
                        //logger.trace("Has Next: TRUE: Has ancestor with unexplored children");
                        return true;
                    }
                }
            }

            //logger.trace("Has Next: FALSE");
            return false;
        }

        @Override
        public T next() {

            if(currentNode == null && rootNode != null){
                currentNode = rootNode;
                explored.add(currentNode);
                //logger.trace("Next: " + currentNode.getData());
                return currentNode.getData();
            }

            if(currentNode!=null)
            {
                if(!currentNode.getChildren().isEmpty() && !explored.containsAll(currentNode.getChildren())){
                    ListIterator<ITreeNode<T>> it = currentNode.getChildren().listIterator();
                    while(it.hasNext())
                    {
                        ITreeNode<T> n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            explored.add(currentNode);
                            depth++;
                            //logger.trace("Next: " + n.getData());
                            return n.getData();
                        }
                    }
                }

                if(currentNode.hasSiblings())
                {
                    if(!explored.containsAll(currentNode.getParent().getChildren())){
                        ListIterator<ITreeNode<T>> it = currentNode.getParent().getChildren().listIterator();
                        while(it.hasNext())
                        {
                            ITreeNode<T> n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                explored.add(currentNode);
                                //logger.trace("Next: " + currentNode.getData());
                                return n.getData();
                            }
                        }
                    }
                }

                ITreeNode<T> tempNode = currentNode;

                while(tempNode.getParent()!=null){

                    tempNode = tempNode.getParent();


                    if(!explored.containsAll(tempNode.getChildren())) {
                        depth--;
                        ListIterator<ITreeNode<T>> it = tempNode.getChildren().listIterator();
                        while(it.hasNext())
                        {
                            ITreeNode<T> n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                explored.add(currentNode);
                                //logger.trace("Next: " + currentNode.getData());
                                return n.getData();
                            }
                        }
                    }
                }
            }

            //logger.trace("Next: null");
            return null;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }

}
    
