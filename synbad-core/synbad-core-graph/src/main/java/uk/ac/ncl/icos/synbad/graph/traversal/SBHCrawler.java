/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBHCrawler<N extends SBValued, E extends SBEdge, C extends SBHCursor<N, E>>
                    extends SBCrawler<N, E, C>, SBCrawlerObserver<N, E, C> { 

}
