/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;

/**
 *
 * @author owengilfellon
 */
public class SBTraversalPipeException extends Exception {

    private final SBTraversalPipe pipe;
    
    public SBTraversalPipeException(SBTraversalPipe pipe, String message) {
        super(message);
        this.pipe = pipe;
    }

    @Override
    public String toString() {
        return "! " + pipe + ": "+ getMessage();
    }

}
