/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPTraversal.PGraphConfigurable;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBAddPort<T,  V extends SBValued> extends DefaultSBTraversalPipe<T, V>{

    private final V port;
    private final SBPortDirection direction;
    
    public SBAddPort(SBTraversal<T, ?> traversal, V port, SBPortDirection direction) {
        super(traversal);
        this.port = port;
        this.direction = direction;
    }
    
    public SBAddPort(SBTraversal<T, ?> traversal, SBAddPort<T, V> addPort) {
        super(traversal, addPort);
        this.port = addPort.port;
        this.direction = addPort.direction;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        SBGraph graph =((PGraphConfigurable<?, T, ?>)getParentTraversal())
                .asConfigurable()
                .getGraph();
        
        T currentNode = traverser.get();
        
        if(SBValued.class.isAssignableFrom(currentNode.getClass()) 
                //&& graph.containsNode((SBValued)currentNode)
                ) 
            return Collections.EMPTY_SET;
        
        //graph.addPort(port, (SBValued)currentNode, direction, Collections.EMPTY_SET);
 
        return Collections.singleton(traverser.push(port));
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBAddPort<>(parentTraversal, this);
    }
}
