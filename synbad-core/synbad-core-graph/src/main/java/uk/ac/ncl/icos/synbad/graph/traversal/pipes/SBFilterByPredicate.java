/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBFilterByPredicate<V> extends DefaultSBTraversalPipe<V, V>{

    private final Predicate<V> predicate;

    public SBFilterByPredicate(SBTraversal traversal, Predicate<V> predicate) {
        super(traversal);
        this.predicate = predicate;
    }
    
    protected SBFilterByPredicate(SBTraversal traversal, SBFilterByPredicate<V> filterByPredicate) {
        super(traversal, filterByPredicate);
        this.predicate = filterByPredicate.predicate;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {

        if(traverser.get() == null)
            return Collections.EMPTY_SET;
        
        return predicate.test(traverser.get()) ?
                Collections.singleton(traverser) : 
                Collections.EMPTY_SET;
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBFilterByPredicate<>(parentTraversal, this);
    }
}
