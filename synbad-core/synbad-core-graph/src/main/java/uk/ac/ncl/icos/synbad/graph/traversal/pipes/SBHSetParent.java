/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBHSetParent<V> extends DefaultSBTraversalPipe<V, V>{
    
    private final String label;
    
    public SBHSetParent(SBTraversal traversal, String label) {
        super(traversal);
        this.label = label;
    }
    
    protected SBHSetParent(SBTraversal traversal, SBHSetParent<V> setParent) {
        super(traversal, setParent);
        this.label = setParent.label;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {

        if(SBHGraph.class.isAssignableFrom(((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph().getClass())) {
           traverser.getByLabel(label).stream()
                .filter(b -> SBValued.class.isAssignableFrom(b.getClass()))
                .map(b -> (SBValued)b)
                .forEach(b -> {
                   // System.out.println("Setting parent of " + traverser.get() + ": " + b);
                    if(!traverser.get().equals(b))
                        ((SBHGraph)((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph())
                                .setParent(b, (SBValued)traverser.get());
            }); 
        }
        
        

        return Collections.singleton(traverser);
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBHSetParent<>(parentTraversal, this);
    }

   
    
    
}
