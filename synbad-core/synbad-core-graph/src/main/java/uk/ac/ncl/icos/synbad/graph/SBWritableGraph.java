/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owen
 */
public interface SBWritableGraph<N, E> extends SBGraph<N, E> {
    
    // mutator
    
    public boolean addNode(N node);
    
    public boolean removeNode(N node);

    public boolean removeAllNodes(Collection<? extends N> nodes);
    
    
    public boolean addEdge(N from, N to, E edge);
    
    public boolean removeEdge(E edge);

    public boolean removeAllEdges(Collection<? extends E> edges);

    public E removeEdge(N sourceNode, N targetNode);
    
    public Set<E> removeAllEdges(N sourceNode, N targetNode);
    
}
