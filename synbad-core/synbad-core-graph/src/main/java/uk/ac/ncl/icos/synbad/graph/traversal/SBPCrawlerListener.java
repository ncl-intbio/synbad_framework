package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursor;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;


/**
 *
 * @author owengilfellon
 */
public interface SBPCrawlerListener<N extends SBValued, P extends SBValued, E extends SBEdge, C extends SBPCursor<N, P, E>> extends SBCrawlerObserver<N, E, C>{

    public void onPort(P instance, C cursor);

    public abstract class SBPCrawlerListenerAdapter<N extends SBValued, P extends SBValued, E extends SBEdge, C extends SBPCursor<N, P, E>> extends SBCrawlerObserverAdapter<N, E, C> implements SBPCrawlerListener<N, P, E, C>{

        @Override
        public void onPort(P instance, C cursor) { }

    }
}
