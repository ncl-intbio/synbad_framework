/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import uk.ac.ncl.icos.synbad.graph.SBPGraph;
import java.util.function.Function;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;

/**
 *
 * @author owengilfellon
 */
public interface SBPTraversal<T, V, G extends SBPGraph> extends SBHTraversal<T,V,G> {

//    <V2 extends SBValued> SBPTraversal<T, V2, G> addE(Class<V2> clazz, String label, String predicate);
//
//    <V2 extends SBValued> DefaultSBPTraversal<T, V2, G> addP(V2 v, SBPortDirection direction);

    <E> SBPTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe);

//    <V2 extends SBValued> SBPTraversal<T, V2, G> addV(V2 v);

    SBPTraversal<T, V, G> as(String label);

    SBPTraversal<T, V, G> hasLabel(String... label);

    SBPTraversal<T, V, G> hasNotLabel(String... label);

    <V2> SBPTraversal<T, V2, G> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2);

    <V extends SBValued> SBPTraversal<T, V, G> contract();

    SBPTraversal<T, V, G> copy();

    SBPTraversal<T, V, G> deduplicate();

    <S extends SBGraph> SBPTraversal<T, V, G> doFunction(Function<V, S> function);

    SBPTraversal<T, V, G> doTraversal(SBTraversal<V, V> traversal);

    public <T extends SBValued, V2 extends SBEdge> SBPTraversal<V, V2, G> e();
    
    public <V2 extends SBEdge> SBPTraversal<T, V2, G> e(SBDirection direction);
    
    public <V2 extends SBEdge> SBPTraversal<T, V2, G> e(String... predicates);
    
    public <V2 extends SBEdge> SBPTraversal<T, V2, G> e(SBDirection direction, String... predicates);
    
    public <V2 extends SBEdge> SBPTraversal<T, V2, G> e(String[] inPredicates, String[] outpredicates);

    
    <T extends SBEdge, V2 extends SBValued> SBPTraversal<T, V2, G> v();
    
    <T extends SBEdge, V2 extends SBValued> SBPTraversal<T, V2, G> v(SBDirection direction);
    
    <V2 extends SBValued> SBPTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz);

    
    
    <V extends SBValued> SBPTraversal<T, V, G> expand();

    SBPTraversal<T, V, G> filter(Predicate<V> predicate);

    SBPTraversal<T, V, G> has(String key, Object value, Object... additionalValues);

    SBPTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues);

    SBPTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits, boolean inclusive);
    
    SBPTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits);

    SBPTraversal<T, V, G> loop(int iterations, SBTraversal<?, V> traversal, boolean emit);

    SBPTraversal<T, ? extends SBValued, G> p();

    SBPTraversal<T, ? extends SBValued, G> p(SBPortDirection direction);

    SBPTraversal<T, ? extends SBValued, G> proxied();

    SBPTraversal<T, ? extends SBValued, G> proxy();

    <V2> SBPTraversal<T, V2, G> select(String... labels);

    <V2> SBPTraversal<T, V2, G> select(Class<V2> clazz, String... label);

    SBPTraversal<T, V, G> setParent(String label);

    G toGraph();


    
    SBPTraversal<T, ? extends SBValued, G> children();
    
    SBPTraversal<T, ? extends SBValued, G> children(Predicate<SBValued> predicate);
    
    SBPTraversal<T, ? extends SBValued, G> parent();
    
    SBPTraversal<T, ? extends SBValued, G> parent(Predicate<SBValued> predicate);
    
    @Override
    PGraphConfigurable<T, V, G> asConfigurable();
    
    public interface PGraphConfigurable<T, V, G extends SBPGraph> extends HGraphConfigurable<T, V, G>, SBPTraversal<T, V, G>{

    }
    
}
