/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBAddLabel<T> extends DefaultSBTraversalPipe<T, T>{

    private final String label;
    
    public SBAddLabel(SBTraversal traversal, String label) {
        super(traversal);
        this.label = label;
    }
    
    protected SBAddLabel(SBTraversal traversal, SBAddLabel addLabel) {
        super(traversal, addLabel);
        this.label = addLabel.label;
    }

    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {
        traverser.addLabel(label);
        return  Collections.singleton(traverser);
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBAddLabel<>(parentTraversal, label);
    }

   
    
    
    

}
