/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.impl;


import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBHCursor;
import java.io.Serializable;
import java.net.URI;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHIterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.SBGlobalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGlobalNodeProvider;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.graph.WritableHGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversalSource;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBHTraversalSource;

/**
 * 
 * @author owengilfellon
 */
public class DefaultHGraph<N, E> implements WritableHGraph<N, E>, SBGlobalEdgeProvider<N, E>, SBGlobalNodeProvider<N> {

    private static final long serialVersionUID = 563632964209819888L;
    private final Logger LOGGER = LoggerFactory.getLogger(DefaultHGraph.class);
    private final DefaultCompoundGraph<N, E> graph;
    private final Map<N, NodeRecord<N>> nodeRecords;
    private final Map<E, EdgeRecord> edgeRecords;
    private final SBDispatcher dispatcher;

    public DefaultHGraph(N root) {
        this.graph = new DefaultCompoundGraph<>(root);
        this.dispatcher = new DefaultSBDispatcher();
        this.nodeRecords = new HashMap<>();
        this.edgeRecords = new HashMap<>();
        registerNode(new NodeRecord<>(root));
    }
    
    
    private String getPrettyName(Object node) {
        if (SBIdentified.class.isAssignableFrom(node.getClass()))
          return ((SBIdentified)node).getDisplayId();
        else if (URI.class.isAssignableFrom(node.getClass()))
            return ((URI)node).getFragment();
        else
            return node.toString();
    }
    
    
    // =========================================================================
    //                Internal record management
    // =========================================================================  
    
  
    private void registerNode(NodeRecord<N> record) {
        record.setIsRetracted(true);
        if(!nodeRecords.containsKey(record.getNode()))
            nodeRecords.put(record.getNode(), record);
    }
    
    private void deregisterNode(N node) {
        if(nodeRecords.containsKey(node))
            nodeRecords.remove(node);
    }
    
    private void registerEdge(EdgeRecord record) {
        if(!edgeRecords.containsKey(record.getEdge()))
            edgeRecords.put(record.getEdge(), record);
    }
    
    private void deregisterEdge(E edge) {
        if(edgeRecords.containsKey(edge))
             edgeRecords.remove(edge);
    }
    
    // =========================================================================
    //                         Nodes
    // =========================================================================   

    
    
    @Override
    public boolean addNode(N instance, N parent, int index) {

        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        if(parent == null)
            throw new NullPointerException("parent cannot be null");
        if(!containsNode(parent))
            throw new NullPointerException("parent is not in graph");
        if(containsNode(instance))
            return false;

        if(!graph.addNode(instance, parent, index))
            return false;

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added {} to {} at index {}", getPrettyName(instance), getPrettyName(parent), index);
        registerNode(new NodeRecord<>(instance));
       // contract(parent);
        dispatchNodeEvent(SBGraphEventType.ADDED, parent, instance);
        if(isVisible(instance))
            dispatchNodeEvent(SBGraphEventType.VISIBLE, parent, instance);
        return true;
    }

     @Override
    public boolean addNode(N instance, N parent) {
        
        if(instance == null) {
            LOGGER.warn("instance cannot be null");
            return false;
        } else if(parent == null) {
            LOGGER.warn("parent cannot be null");
            return false;
        } else if(!containsNode(parent)) {
            LOGGER.warn("parent [ {} ] is not in graph", getPrettyName(parent));
            return false;
        } else if(containsNode(instance)) {
            LOGGER.warn("instance [ {} ] is already in graph", getPrettyName(instance));
            return false;
        }

        return addNode(instance, parent, graph.getChildren(parent).size());
    }
    
    /*
    @Override
    public boolean addNode(N node) {
        return addNode(node, getRoot());
    }
   */
    @Override
    public boolean addEdge(N from, N to, E edge) {
        
        if(!graph.addEdge(from, to, edge))
            return false;
        
        registerEdge(new EdgeRecord(edge));
        
        // resolve source and target as required...

        edgeRecords.get(edge).resolveSource();
        edgeRecords.get(edge).resolveTarget();

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Added {}", edge);
        dispatchEdgeEvent(SBGraphEventType.ADDED, edge);
        
        if(isVisible(from) && isVisible(to))
            dispatchEdgeEvent(SBGraphEventType.VISIBLE, edge);
        
        return true;
    }
    
    @Override
    public boolean removeNode(N node) {
        if(node == null)
            throw new NullPointerException("Node cannot be null");

        if(!containsNode(node))
            return false;

        N parent = graph.getParent(node);
        
        Set<E> edges = getAllEdges(node);
        
        if(graph.removeNode(node)) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Removed {} from {}", getPrettyName(node), getPrettyName(parent));
            deregisterNode(node);
            dispatchNodeEvent(SBGraphEventType.REMOVED, parent, node);
        }

        for(E edge : edges) {
            removeEdge(edge);
        }

        return false;
    }
    
    @Override
    public boolean removeEdge(E edge) {

        if(graph.removeEdge(edge)) {
            deregisterEdge(edge);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Removed {}", edge);
            dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
            return true;
        }

        return false;
    }
    
    
    @Override
    public void expand(N node) {
       
        
        NodeRecord<N> record = nodeRecords.get(node);
        
        

        if(!record.isRetracted()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Is not retracted: [ {} ]", node);
            return;
        }
            

        // use nodeDelta to identify changes in visibility of descendents
        
        Map<N, Boolean> nodeDelta = new HashMap<>();
        List<N> descendants = graph.getDescendants(node);
        
        descendants.stream().forEach(n -> {   
            nodeDelta.put(n, isVisible(n)); }
        );

        // Resolve any proxy edges to their's node's new lowest visible ancestor

        Set<E> edgesToResolve = Stream.concat(
                Stream.of(node),
                descendants.stream())
            .flatMap(n -> getAllEdges(n).stream())
            .collect(Collectors.toSet());

        record.setIsRetracted(false);
        
        descendants.stream().forEach(n -> {   
                
            boolean isVisible = isVisible(n);
            
            if(nodeDelta.get(n).equals(isVisible))
                nodeDelta.remove(n);
        });

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Expanding {} and {} descendants", getPrettyName(node), (nodeDelta.size() > 0 ? nodeDelta.size() : 0));
        
        // Dispatch event for any descendants that are now visible

        List<DefaultSBGraphEvent> edgeEvts = edgesToResolve.stream().flatMap(edge -> resolveEdge(edge).stream()).collect(Collectors.toList());
        
        
        nodeDelta.keySet().stream().forEach(n -> {
            
            // if has changes and *was* visible
            
            if(nodeDelta.get(n))
                dispatchNodeEvent(SBGraphEventType.HIDDEN, getParent(n), n);
            
            // if has changes and *was not* visible
            
            else
                dispatchNodeEvent(SBGraphEventType.VISIBLE, getParent(n), n);
        });
        
        edgeEvts.stream().forEach(this::dispatchEdgeEvent);

    }
    
    @Override
    public void contract(N node) {
        

        NodeRecord<N> record = nodeRecords.get(node);
        
        if(record.isRetracted()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Is already contracted: [ {} ]", node);
            return;
        }
            

        // use nodeDelta to identify changes in visibility of descendents
        
        Map<N, Boolean> nodeDelta = graph.getDescendants(node).stream()
                .collect(Collectors.toMap(k -> k, v -> isVisible(v)));

        // If node is visible, need to recalculate edges of it and it's descendents
        
        Set<E> edgesToResolve = Stream.concat(
                Stream.of(node),
                graph.getDescendants(node).stream())
            .flatMap(n -> getAllEdges(n).stream())
            .collect(Collectors.toSet());
        
        record.setIsRetracted(true);
     
        // If node visibility has not changes upon retraction, remove from delta
        
        graph.getDescendants(node).stream()
                .filter(n -> nodeDelta.get(n).equals(isVisible(n)))
                .forEach(n -> { 
                    nodeDelta.remove(n);});

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Contracting {} and {} descendants", getPrettyName(node), (nodeDelta.size() > 0 ? nodeDelta.size() : 0));
        
        List<DefaultSBGraphEvent> edgeEvts = edgesToResolve.stream().flatMap(edge -> resolveEdge(edge).stream()).collect(Collectors.toList());

        nodeDelta.keySet().stream().forEach(n -> {

            // If it was visible, is now hidden
            
            if(nodeDelta.get(n))
                dispatchNodeEvent(SBGraphEventType.HIDDEN, getParent(n), n);
            
            // If was hidden, is now visible
            
            else
                dispatchNodeEvent(SBGraphEventType.VISIBLE, getParent(n), n);
        });
        
        edgeEvts.stream().forEach(this::dispatchEdgeEvent);
    }
    
    private List<DefaultSBGraphEvent> resolveEdge(E e) {

        if(e == null) {
            throw new NullPointerException("Edge to resolve cannot be null");
        }
        
        List<DefaultSBGraphEvent> edgeEvts = new ArrayList<>();
        
        EdgeRecord r = edgeRecords.get(e);

        if(r == null) {
            return edgeEvts;
            // throw new NullPointerException("Cannot retrieve edge record for " + e.toString());
        }
        
        // Get currently resolved source and target
        
        N oldResolvedSource = r.getResolvedSource();
        N oldResolvedTarget = r.getResolvedTarget();
        
        // If either of the source or target's lowest visible ancestors have changed
        
        if(lowestVisibleAncestor(graph.getEdgeSource(e)) != oldResolvedSource ||
            lowestVisibleAncestor(graph.getEdgeTarget(e)) != oldResolvedTarget) {
            
            // If the edge is currently visible, remove it
            
            if(isEdgeVisible(e))
                edgeEvts.add(new DefaultSBGraphEvent<>(SBGraphEventType.HIDDEN, SBGraphEntityType.EDGE, e, this, null, null));

            
            // Resolve the edge and targets
            
            r.resolveTarget();
            r.resolveSource();
            
            // If the edge is now visible, add it
            
            if(isEdgeVisible(e))
               edgeEvts.add(new DefaultSBGraphEvent<>(SBGraphEventType.VISIBLE, SBGraphEntityType.EDGE, e, this, null, null));
        }
        
        return edgeEvts;
        
    }
    
    @Override
    public void setParent(N parent, N node) {
        N currentParent = graph.getParent(node);
        graph.setParent(parent, node);
        dispatchNodeEvent(SBGraphEventType.REMOVED, currentParent, node);
        dispatchNodeEvent(SBGraphEventType.ADDED, parent, node);
        getAllEdges(node).stream().forEach(e -> {
            EdgeRecord edge =  edgeRecords.get(e);
            edge.resolveSource();
            edge.resolveTarget();
        });
    }
    
    
    // =========================================================================
    //                          Event dispatch
    // =========================================================================  
    
    public void dispatchNodeEvent(SBEventType type, N parent, N node) {
        SBEvent evt = new DefaultSBGraphEvent<>(type, SBGraphEntityType.NODE, node, this, parent, null);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch N evt: [ {} ]", evt);
        dispatcher.publish(evt);

    }
    
    public void dispatchEdgeEvent(SBEventType type, E edge) {
        SBEvent evt = new DefaultSBGraphEvent<>(type, SBGraphEntityType.EDGE, edge, this, null, null);
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch E evt: [ {} ]", evt);
        dispatcher.publish(evt);  
    }

     public void dispatchEdgeEvent(DefaultSBGraphEvent evt) {
         if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch E evt: [ {} ]", evt);
         dispatcher.publish(evt);
    }
     
    
    // =============================================================

    @Override
    public boolean containsNode(N node) {
        return graph.containsNode(node);
    }
    
    @Override
    public Set<N> nodeSet() {
        return graph.nodeSet();
    }

    @Override
    public boolean removeAllNodes(Collection<? extends N> nodes) {
        
        boolean changed = false;
        
        for(N node : nodes) {
            if(removeNode(node)) {
                changed = true;
            }
        }
        
        return changed;
    } 

    // =========================================================================
    //                                Edges
    // =========================================================================    

    @Override
    public Set<E> getAllEdges() {
        return edgeRecords.keySet();
    }
    
    
    
    @Override
    public Set<E> edgeSet() {
        return edgeRecords.values().stream()
            .filter(e -> isEdgeVisible(e.getEdge()))
            .map(EdgeRecord::getEdge).collect(Collectors.toSet());
    }

    @Override
    public boolean containsEdge(E edge) {
        return edgeRecords.containsKey(edge);
    }
    
    
    @Override
    public boolean containsEdge(N from, N to, E edge) {

        if(!edgeRecords.containsKey(edge))
            return false;
        
        return getEdgeSource(edge).equals(from) 
                && getEdgeTarget(edge).equals(to);

    }
    
    @Override
    public boolean containsEdge(N from, N to) {

        for(E edge : nodeRecords.get(from).getResolvedOutgoing()) {
            if(getEdgeTarget(edge).equals(to))
                return true;
        }
        
        return false;

    }

    @Override
    public List<E> incomingEdges(N node) {
        return new ArrayList<>(nodeRecords.get(node).getResolvedIncoming());
    }

    @Override
    public List<E> outgoingEdges(N node) {
        return new ArrayList<>(nodeRecords.get(node).getResolvedOutgoing());
    }
    
    
    @Override
    public E getEdge(N from, N to) {
        return getAllEdges(from, to).stream().findAny().orElse(null);
    }

    @Override
    public boolean removeAllEdges(Collection<? extends E> edges) {
        boolean b = true;
        
        for(E edge : edges) {
            if(!removeEdge(edge))
                b = false;
        }
        
        return b;
    }

    @Override
    public E removeEdge(N sourceNode, N targetNode) {
        E edge = getEdge(sourceNode, targetNode);
        return (edge != null && removeEdge(edge)) ? edge : null;
    }

    @Override
    public Set<E> removeAllEdges(N sourceNode, N targetNode) {
        return getAllEdges(sourceNode, targetNode).stream().filter(e -> removeEdge(e)).collect(Collectors.toSet());
    }
    
    // =========================================================================
    //                               Proxy Edges
    // =========================================================================  
    

    public boolean hasProxyEdge(E edge) {

        boolean sourceProxied = !graph.getEdgeSource(edge).equals(
                edgeRecords.get(edge).getResolvedSource());
        boolean targetProxied = !graph.getEdgeTarget(edge).equals(
                edgeRecords.get(edge).getResolvedTarget());

       return sourceProxied || targetProxied;

    }

    
    @Override
    public N getEdgeSource(E edge) {
        return edgeRecords.get(edge).getResolvedSource();
    }
    
    @Override
    public N getEdgeTarget(E edge) {
        return edgeRecords.get(edge).getResolvedTarget();
    }

    @Override
    public N getOriginalEdgeSource(E edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public N getOriginalEdgeTarget(E edge) {
        return graph.getEdgeTarget(edge);
    }
    
    
    // =========================================================================
    //                               Hierarchy
    // =========================================================================  
    
    
    @Override
    public N getRoot() {
        return graph.getRoot();
    }
    
    @Override
    public boolean hasSiblings(N node) {
        return graph.hasSiblings(node);
    }

    @Override
    public List<N> getSiblings(N node) {
        return graph.getSiblings(node);
    }
    
    @Override
    public boolean isAncestor(N node, N ancestor) {
        return graph.isAncestor(node, ancestor);
    }
    
    private N lowestVisibleAncestor(N node) {

        // Stores current iterated node as we ascend the hierarchy
        
        N currentRecord = node;
       
        // Stores lowest visible ancestor
        
        N lowestVisibleAncestor = currentRecord;
        
        // Traverse the hierarchy
        
        while(graph.getParent(currentRecord) != null){
            currentRecord = graph.getParent(currentRecord);
            
            if(nodeRecords.get(currentRecord).isRetracted())
                lowestVisibleAncestor = currentRecord;
        }
        
        return lowestVisibleAncestor;
    }
    
    private boolean isEdgeVisible(E edge) {
        
        // if both source and targets resolve to same node, then no

        EdgeRecord r = edgeRecords.get(edge);
        
        if(r == null)
            return false;
   
        return !r.getResolvedSource().equals(r.getResolvedTarget());
        
    }   

    @Override
    public boolean isVisible(N node) {

        N currentRecord = node;

        // Traverse hierarchy
        
        while(graph.getParent(currentRecord) != null){
            currentRecord = graph.getParent(currentRecord);
            
            // If an ancestor is retracted, node is not visible
            
            if(nodeRecords.get(currentRecord).isRetracted())
                return false;
        }
        
        return true;
    }
    


    @Override
    public boolean isContracted(N node) {
        return nodeRecords.get(node).isRetracted();
    }
    
    
    
    @Override
    public N getParent(N instance) {
        if(instance == null)
            throw new NullPointerException("instance cannot be null");
        
        if(!containsNode(instance))
            throw new NullPointerException(instance + " not in graph");
        
        return graph.getParent(instance);
    }

    @Override
    public List<N> getChildren(N instance) {
         return graph.getChildren(instance).stream()
                .filter(this::isVisible).collect(Collectors.toList());
        
    }

    @Override
    public List<N> getAllChildren(N node) {
        return graph.getChildren(node);
    }
    
    
    
    public boolean isLeaf(N node) {
        return getChildren(node).isEmpty();
    }

    @Override
    public boolean isDescendant(N ancestor, N descendant) {
        
        if(ancestor == null)
            throw new NullPointerException("ancestor cannot be null");
        
        if(descendant == null)
            throw new NullPointerException("descendant cannot be null");
        
        return graph.isDescendant(ancestor, descendant);
    }
    

    @Override
    public List<N> getDescendants(N instance) {
        return graph.getDescendants(instance).stream()
                .filter(this::isVisible).collect(Collectors.toList());
    }


    @Override
    public List<N> getAllDescendants(N instance) {
        return graph.getDescendants(instance);
    }
  
    @Override
    public int getDepth(N node) {
        return graph.getDepth(node);
    }
    
    
    @Override
    public Set<E> getAllEdges(N node) {
        return Stream.concat(
            incomingEdges(node).stream(),
            outgoingEdges(node).stream()).collect(Collectors.toSet()); 
    }
    
    
    @Override
    public Set<E> getAllEdges(N o1, N o2) {
        return Stream.concat(
            incomingEdges(o1).stream(),
            outgoingEdges(o1).stream()).filter(e -> 
                        getEdgeSource(e).equals(o1) && getEdgeTarget(e).equals(o2) ||
                        getEdgeTarget(e).equals(o1) && getEdgeSource(e).equals(o2))
            .collect(Collectors.toSet());

    }
 
    
    // =========================================================================
    //                               Iteration
    // =========================================================================  



    public SBHIterator<N> visibleIterator(N instance) {
        return new HierarchyIterator(this, instance);
    }
    
    
    @Override
    public SBHIterator<N> hierarchyIterator(N instance) {
        return graph.hierarchyIterator(instance);
    }

    @Override
    public SBHTraversalSource<N, E, SBHGraph<N, E>> getTraversal() {
        return new DefaultSBHTraversalSource<>(this);
    }


    // =========================================================================
    //                          Graph listeners
    // =========================================================================  
    
    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }

    @Override
    public SBHCursor<N, E> createCursor(N startNode) {
        return new DefaultSBHCursor<>(startNode, this);
    }

    @Override
    public String toString() {
        return getRoot().toString() + " H-Graph";
    }



    // =========================================================================
    //             Internal classes for storing node and edge info
    // =========================================================================  
    
    protected final class NodeRecord<T> implements  Serializable{
        
        private final T node;
        private boolean isRetracted;
        private final Set<E> resolvedIncoming;
        private final Set<E> resolvedOutgoing;
 
        public NodeRecord(T node) {
            this.node = node;
            this.isRetracted = false;
            this.resolvedIncoming = new HashSet<>();
            this.resolvedOutgoing = new HashSet<>();
        }

        public T getNode() {
            return node;
        }

        public boolean isRetracted() {
            return isRetracted;
        }

        public void setIsRetracted(boolean isRetracted) {
            this.isRetracted = isRetracted;
        }
        
        public void addResolvedIncoming(E edge) {
            this.resolvedIncoming.add(edge);
        }
        
        public void removeResolvedIncoming(E edge) {
            this.resolvedIncoming.remove(edge);
        }
        
        public void addResolvedOutgoing(E edge) {
            this.resolvedOutgoing.add(edge);
        }
        
        public void removeResolvedOutgoing(E edge) {
            this.resolvedOutgoing.remove(edge);
        }

        public Set<E> getResolvedIncoming() {
            return resolvedIncoming;
        }

        public Set<E> getResolvedOutgoing() {
            return resolvedOutgoing;
        }
        
        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof NodeRecord))
                return false;
            
            NodeRecord nodeRecord = (NodeRecord) obj;
            return nodeRecord.getNode().equals(this.getNode());
        }

        @Override
        public int hashCode() {
            return getNode().hashCode();
        }
    }
    
    protected final class EdgeRecord implements Serializable {
        
        private final E edge;
        private N resolvedSource;
        private N resolvedTarget;

        public EdgeRecord(E edge) {
            this.edge = edge;
            this.resolvedSource = graph.getEdgeSource(edge);
            this.resolvedTarget = graph.getEdgeTarget(edge);
        }

        public E getEdge() {
            return edge;
        }

        public N getResolvedSource() {
            return resolvedSource;
        }

        public N getResolvedTarget() {
            return resolvedTarget;
        }

        public void resolveSource() {
            nodeRecords.get(getResolvedSource()).removeResolvedOutgoing(edge);
            this.resolvedSource  = lowestVisibleAncestor(graph.getEdgeSource(edge));
            nodeRecords.get(getResolvedSource()).addResolvedOutgoing(edge);
        }

        public void resolveTarget() {
            nodeRecords.get(getResolvedTarget()).removeResolvedIncoming(edge);
            this.resolvedTarget  = lowestVisibleAncestor(graph.getEdgeTarget(edge));
            nodeRecords.get(getResolvedTarget()).addResolvedIncoming(edge);
        }

        @Override
        public String toString() {
            return "ER: " + edge + "\n   rSource: " + resolvedSource + " rTarget: " + resolvedTarget;
        }
    }
    
    // =========================================================================
    //                          Iterator
    // =========================================================================  

    public final class HierarchyIterator implements SBHIterator<N> {

        private final LinkedList<N> list = new LinkedList<>();
        private final Set<N> explored = new HashSet<>();
        private final N rootNode;
        private N currentNode = null;
        private final DefaultHGraph<N, E> graph;
        
        private int index = -1;
        private int depth = 0;

        public HierarchyIterator(DefaultHGraph<N, E> graph, N rootNode) {
            this.rootNode = rootNode;
            list.add(rootNode);
            this.graph = graph;
        }

        @Override
        public int getDepth()
        {
            if( currentNode == null)
                return 0;
            return graph.getDepth(currentNode);
        }

        @Override
        public boolean hasNext() {

            if(currentNode == null && rootNode != null){
                return true;
            }

            if(currentNode!=null) {
     
                
                if(!graph.getChildren(currentNode).isEmpty() &&
                        !explored.containsAll(graph.getChildren(currentNode)) &&
                        !isContracted(currentNode)){
                    return true;
                }

                if(graph.hasSiblings(currentNode)) {
                    if(!explored.containsAll(graph.getSiblings(currentNode))){
                        return true;
                    }     
                }

                if(currentNode.equals(rootNode)) {
                    return false;
                }

                N tempNode = currentNode;
                
                while(graph.getParent(tempNode)!=null && !tempNode.equals(rootNode)) {
                    tempNode = graph.getParent(tempNode);
                    if(!explored.containsAll(graph.getChildren(tempNode))) {
                        return true;
                    }
                }  
                
                
            }
            
            

            return false;
        }

        @Override
        public N next() {

            // If we dont have a current node, set current node to root node
            
            if(currentNode == null && rootNode != null){
                currentNode = rootNode;
                
                if(!explored.contains(currentNode)) {
                    list.add(currentNode);
                    explored.add(currentNode);
                    index++;
                }

                return currentNode;
            }
            
            // If we have a current node

            if(currentNode!=null)
            {
                
                // first, if current node is unretracted, navigate through
                // unexplored children
                
                if(!graph.getChildren(currentNode).isEmpty() &&
                        !explored.containsAll(graph.getChildren(currentNode)) &&
                        !isContracted(currentNode)){
                    ListIterator<N> it = graph.getChildren(currentNode).listIterator();
                    index++;
                    
                    while(it.hasNext())
                    {
                        N n = it.next();

                        if(!explored.contains(n))
                        {
                            currentNode = n;
                            list.add(currentNode);
                            explored.add(currentNode);
                            depth++;
                            
                            return n;
                        }
                    }
                }
                
                // if there are no unexplored children, then move on to siblings

                if(graph.hasSiblings(currentNode))
                {
                    if(!explored.containsAll(graph.getSiblings(currentNode))){
                        ListIterator<N> it = graph.getSiblings(currentNode).listIterator();
                        while(it.hasNext())
                        {
                            N n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                list.add(currentNode);
                                explored.add(currentNode);
                                index++;
                                return n;
                            }
                        }
                    }                   
                }
             
                // if there are no siblings, then retrace hierarchy to nearest
                // ancestor with unexplored children
                
               N tempNode = currentNode;

                while(graph.getParent(tempNode) !=null && !tempNode.equals(rootNode)){

                    tempNode = graph.getParent(tempNode);


                    if(!explored.containsAll(graph.getChildren(tempNode))) {
                        depth--;
                        ListIterator<N> it = graph.getChildren(tempNode).listIterator();
                        while(it.hasNext())
                        {
                            N n = it.next();

                            if(!explored.contains(n))
                            {
                                currentNode = n;
                                list.add(currentNode);
                                explored.add(currentNode);
                                index++;
                                return n;
                            }
                        }
                    }
                }
            }

            return null;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public N previous() {
            index--;
            return list.get(index);
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void set(N e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void add(N e) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
}
