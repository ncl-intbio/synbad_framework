/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.HashSet;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.SBLocalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBEdgeToVertex<T extends SBEdge, V extends SBValued> extends DefaultSBTraversalPipe<T, V>{

    private final SBDirection direction;
    private final Class<? extends V> clazz;
    
    public SBEdgeToVertex(SBTraversal traversal) {
        super(traversal);
        this.direction = null;
        this.clazz = null;
    }
    
    protected SBEdgeToVertex(SBTraversal parentTraversal, SBEdgeToVertex<T, V> traversal) {
        super(parentTraversal, traversal);
        this.direction = traversal.direction;
        this.clazz = traversal.clazz;
    }
    
    public SBEdgeToVertex(SBTraversal traversal, SBDirection direction) {
        super(traversal);
        this.direction = direction;
        this.clazz = null;
    }
    
    public SBEdgeToVertex(SBTraversal traversal, SBDirection direction, Class<? extends V> clazz) {
        super(traversal);
        this.direction = direction;
        this.clazz = clazz;
    }
    
    private Collection<SBTraverser<V>> addIfIs(Collection<SBTraverser<V>> coll, SBTraverser<T> traverser, V obj, Class<? extends V> clazz) {
        if(clazz == null || clazz.isAssignableFrom(obj.getClass()))
            coll.add(traverser.duplicate().push(obj));
        return coll;
    }
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {

         SBLocalEdgeProvider<V, SBEdge> view = ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();
         Collection<SBTraverser<V>> processed = new HashSet<>();
         
        if(null == direction) {
            V source = view.getEdgeSource(traverser.get());
            V target = view.getEdgeTarget(traverser.get());
            addIfIs(processed, traverser, source, clazz);
            addIfIs(processed, traverser, target, clazz);
        } else switch (direction) {
            case IN: 
                V source = view.getEdgeSource(traverser.get());
                addIfIs(processed, traverser, source, clazz);
                break;
            default:
                V target = view.getEdgeTarget(traverser.get());
                addIfIs(processed, traverser, target, clazz);
                break;
        }
        
        return processed;
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBEdgeToVertex<>(parentTraversal, this);
    }
}
