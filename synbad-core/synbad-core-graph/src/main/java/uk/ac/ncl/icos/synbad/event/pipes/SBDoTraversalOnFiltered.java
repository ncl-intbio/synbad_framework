/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;

import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBDoTraversalOnFiltered<T> extends DefaultSBTraversalPipe<T, T>{

    private final SBTraversal traversal;
    private final SBEventFilter filter;
    private final boolean passThrough;
    
    public SBDoTraversalOnFiltered(SBTraversal parentTraversal, SBEventFilter filter, boolean passThrough, SBTraversal traversal) {
        super(parentTraversal);
        this.traversal = traversal.copy();
        this.filter = filter;
        this.passThrough = passThrough;
        
    }
    
    protected SBDoTraversalOnFiltered(SBTraversal parentTraversal, SBDoTraversalOnFiltered<T> doTraversal) {
        super(parentTraversal, doTraversal);
        this.traversal = doTraversal.traversal.copy();
        this.filter = doTraversal.filter;
        this.passThrough = doTraversal.passThrough;
    }

    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {
        
        T current = traverser.get();
        
        if(!SBEvent.class.isAssignableFrom(current.getClass()) || !filter.test((SBEvent)current))  {
            return passThrough ? Collections.singleton(traverser) : Collections.EMPTY_LIST;
        }
           
        SBTraversal t = traversal.copy();
        t.asConfigurable().addInput(traverser);
        SBTraversalPipe<?, T> output = t.asConfigurable().getOutputPipe();
        List<SBTraverser<T>> processed = new ArrayList<>();
        while(output.hasNext()) {
            SBTraverser<T> next = output.next();
            processed.add(next);
        }
        return processed;
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBDoTraversalOnFiltered<>(parentTraversal, this);
    }
    
}
