/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph;

import java.util.Set;

/**
 *
 * @author owengilfellon
 */
public interface SBGlobalNodeProvider<N> {
    
    
    // ==========================
    //          Nodes
    // ==========================
    
    // global
    
    /**
     * 
     * @return all objects stored in this graph.
     */
    Set<N> nodeSet();
    
    /**
     * 
     * @param node
     * @return True, if this graph contains the provided object
     */
    boolean containsNode(N node);
    
}
