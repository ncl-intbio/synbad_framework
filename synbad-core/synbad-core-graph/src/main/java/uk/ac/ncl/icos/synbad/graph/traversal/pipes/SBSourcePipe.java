/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Iterator;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBSourcePipe<T> extends DefaultSBTraversalPipe<T, T>{

    private final boolean push;
    
    public SBSourcePipe(SBTraversal traversal) {
        this(traversal, false);
    }
    
    public SBSourcePipe(SBTraversal traversal, boolean push) {
        super(traversal);
        this.push = push;
    }

    public SBSourcePipe(SBTraversal traversal, SBSourcePipe<T> pipe) {
        super(traversal, pipe);
        this.push = pipe.push;
    }
    
    private void push() {
        
        SBTraversalPipe pipe = this;
        
        while(pipe.getNextPipe() != null)
            pipe = pipe.getNextPipe();
        
        if(pipe != this) {
            while(pipe.hasNext()) {
                pipe.next();
            }
        }
        
    }

    @Override
    public void addInput(Iterator<SBTraverser<T>> traversers) {
        super.addInput(traversers); //To change body of generated methods, choose Tools | Templates.
        if(push) push();
    }

    @Override
    public void addInput(SBTraverser<T> traverser) {
        super.addInput(traverser); //To change body of generated methods, choose Tools | Templates.
        if(push) push();
    }
    
    
    
    @Override
    public boolean hasNext() {
        return traversers.hasNext();
    }

    @Override
    public SBTraverser<T> next() {
        return traversers.next();
    }

    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {
        return null;
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBSourcePipe<>(parentTraversal, this);
    }

}
