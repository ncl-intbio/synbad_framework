/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraverser;
import java.util.Arrays;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBGraphTraversalSource<N, E, G extends SBGraph<N, E>> implements SBGraphTraversalSource<N, E, G> {

    private final G graph;

    public DefaultSBGraphTraversalSource(G graph) {
        this.graph = graph;
    }

    @Override
    public DefaultSBGraphTraversal<N, N, G> v(N... objs) {
        DefaultSBGraphTraversal<N, N, G> trav = new DefaultSBGraphTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(objs)
                //.filter(n -> !graph.findNodes(n.getData().getIdentity()).isEmpty())
                .map(n -> (SBTraverser<N>)new DefaultSBTraverser<>(n)).iterator());
        return trav;

    }

    @Override
    public DefaultSBGraphTraversal<E, E, G> e(E... edges) {

        DefaultSBGraphTraversal<E, E, G> trav = new DefaultSBGraphTraversal<>(graph);
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(edges)
                .map(e -> (SBTraverser<E>)new DefaultSBTraverser<>(e)).iterator());
        return trav;

    }

}
