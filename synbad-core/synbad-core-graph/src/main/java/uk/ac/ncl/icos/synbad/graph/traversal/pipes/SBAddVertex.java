/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBAddVertex<T,  V extends SBValued> extends DefaultSBTraversalPipe<T, V>{

    private final V vertex;
    
    public SBAddVertex(SBTraversal<T, ?> traversal, V vertex) {
        super(traversal);
        this.vertex = vertex;
    }
    
    public SBAddVertex(SBTraversal<T, ?> traversal, SBAddVertex<T, V> addVertex) {
        super(traversal, addVertex);
        this.vertex = addVertex.vertex;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        SBGraph graph = ((SBGraphTraversal.Configurable<?, T, ?>)getParentTraversal()
                .asConfigurable())
                .getGraph();
        
//        if(SBValued.class.isAssignableFrom(vertex.getClass()))
//            graph.addNode(vertex);
 
        return Collections.singleton(traverser.push(vertex));
    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBAddVertex<>(parentTraversal, this);
    }

    
    

}
