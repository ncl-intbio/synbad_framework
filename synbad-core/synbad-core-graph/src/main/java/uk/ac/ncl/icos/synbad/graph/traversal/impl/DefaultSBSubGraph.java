/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultSBGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBSubGraph;
import uk.ac.ncl.icos.synbad.api.domain.SBEntity;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBSubGraph<N extends SBValued, E extends SBEdge> extends DefaultSBGraph<N, E> implements SBSubGraph<N, E>{

    private final Map<String, Set<SBEntity>> labels;
    private final SBGraph<N,E> graph;
    private final static Logger LOGGER = LoggerFactory.getLogger(DefaultSBSubGraph.class);
    
    public DefaultSBSubGraph(Set<N> nodes, Set<E> edges, Map<String, Set<SBEntity>> labels, SBGraph<N, E> sourceGraph) {
        this.graph = sourceGraph;
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Creating subgraph from {} nodes and {} edges", nodes.size(), edges.size());
        nodes.stream().forEach(this::addNode);
        for(E edge : edges) {
                addEdge(graph.getEdgeSource(edge), graph.getEdgeTarget(edge), edge);
        }
        this.labels = labels;
    }

    @Override
    public Set<SBEntity> getLabelledEntities(String label) {
        return labels.containsKey(label) ? labels.get(label) : Collections.EMPTY_SET;
    }

    @Override
    public Set<N> getLabelledVertices(String label) {
        return getLabelledEntities(label).stream().filter(e -> SBValued.class.isAssignableFrom(e.getClass())).map(n -> (N)n).collect(Collectors.toSet());
    }

    @Override
    public Set<E> getLabelledEdges(String label) {
        return getLabelledEntities(label).stream().filter(e -> SBEdge.class.isAssignableFrom(e.getClass())).map(e -> (E)e).collect(Collectors.toSet());
    }

    @Override
    public Set<String> getLabels() {        
        return labels.keySet();
    }

    
   
}
