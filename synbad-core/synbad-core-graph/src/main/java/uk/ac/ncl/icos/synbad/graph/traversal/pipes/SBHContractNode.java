    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;
import uk.ac.ncl.icos.synbad.graph.SBGraph;

import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBHContractNode<V> extends DefaultSBTraversalPipe<V, V>{
    
    public SBHContractNode(SBTraversal traversal) {
        super(traversal);
    }
    
    protected SBHContractNode(SBTraversal traversal, SBHContractNode<V> contractNode) {
        super(traversal, contractNode);
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {
        
        SBGraph graph = ((SBGraphTraversal)getParentTraversal()).asConfigurable().getGraph();
        
        if(SBHGraph.class.isAssignableFrom(graph.getClass()))
            ((SBHGraph)graph).contract(traverser.get());
        return Collections.singleton(traverser);
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBHContractNode<>(parentTraversal, this);
    }

    
    
    
}
