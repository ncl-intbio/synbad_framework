/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import uk.ac.ncl.icos.synbad.pipes.ASBBlockingPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBDeduplicate<V> extends ASBBlockingPipe<V, V>{

    private final Collection<V> filtered;
    
    public SBDeduplicate(SBTraversal traversal) {
        super(traversal);
        filtered = new HashSet<>();
    }

    public SBDeduplicate(SBTraversal traversal, SBDeduplicate<V> deduplicate) {
        super(traversal, deduplicate);
        this.filtered = deduplicate.filtered;
    }
    
    @Override
    public Collection<SBTraverser<V>> processTraverser(Collection<SBTraverser<V>> traverser) {
        
        List<SBTraverser<V>> processed = new ArrayList();
        Set<V> nodes = new HashSet<>();
        for(SBTraverser<V> t : traverser) {
            if(!nodes.contains(t.get())) {
                nodes.add(t.get());
                processed.add(t);
            }
        }
        return processed;
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBDeduplicate<>(parentTraversal, this);
    }

}
