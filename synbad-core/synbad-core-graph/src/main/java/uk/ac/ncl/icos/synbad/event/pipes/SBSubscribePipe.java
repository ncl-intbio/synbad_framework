/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event.pipes;

import java.util.Arrays;
import java.util.Collection;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;

import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBSubscribePipe<T> extends DefaultSBTraversalPipe<T, T>{

    private final SBSubscriber subscriber;
    private final SBEventFilter filter;
    
    public SBSubscribePipe(SBTraversal parentTraversal, SBSubscriber subscriber) {
        this(parentTraversal, subscriber, new SBEventFilter.DefaultFilter());
    }
    
    public SBSubscribePipe(SBTraversal parentTraversal, SBSubscriber subscriber, SBEventFilter filter) {
        super(parentTraversal);
        this.subscriber = subscriber;
        this.filter = filter;
    }
    
    protected SBSubscribePipe(SBTraversal parentTraversal, SBSubscribePipe<T> subscribePipe) {
        super(parentTraversal, subscribePipe);
        this.subscriber = subscribePipe.subscriber;
        this.filter = subscribePipe.filter;
    }

    @Override
    public Collection<SBTraverser<T>> processTraverser(SBTraverser<T> traverser) {
        
        T current = traverser.get();
        
        if(SBEvent.class.isAssignableFrom(current.getClass()) && filter.test((SBEvent)current))  {
            subscriber.onEvent((SBEvent)current);
        }
           
        return Arrays.asList(traverser);
    }

    @Override
    public SBTraversalPipe<T, T> copy(SBTraversal parentTraversal) {
        return new SBSubscribePipe<>(parentTraversal, this);
    }
    
}
