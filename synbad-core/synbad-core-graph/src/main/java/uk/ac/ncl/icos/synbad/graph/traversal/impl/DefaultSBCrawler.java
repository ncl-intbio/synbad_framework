/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;


import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawler;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBCrawler<N extends SBValued, E extends SBEdge> extends SBCrawler.SBCrawlerAdapter<N, E, SBCursor<N,E>> {
    
    private static final Logger logger = LoggerFactory.getLogger(DefaultSBCrawler.class);
    
    @Override
    public void run(SBCursor<N, E> cursor) {
        processNode(cursor, new HashSet<>());
        onEnded(); 
    }
  
    @Override
    public void processNode(SBCursor<N, E> cursor, Set<E> seenEdges) {

        logger.debug("Processing node: [ {} ]", cursor.getCurrentPosition());
        
        if(terminate()) {
            return;
        }
        
        onInstance(cursor.getCurrentPosition(), cursor);
        
        for (E edge : cursor.getEdges(SBDirection.OUT)) {
            if(!seenEdges.contains(edge)) {
                processEdge(cursor, seenEdges, edge);
            }       
        }

        for (E edge : cursor.getEdges(SBDirection.IN)) {
            if(!seenEdges.contains(edge))
                processEdge(cursor, seenEdges, edge);
        }

    }
    
    @Override
    public void processEdge(SBCursor<N, E> cursor, Set<E> seenEdges, E edge) {

        logger.debug("Processing edge: [ {} ]", edge);
        
        
        seenEdges.add(edge);
        
        if(vetoEdge(edge))
            return;
        
        onEdge(edge);
        
        // TODO: Should work for to->from as well as from->to
        
        if(hasVisited(cursor, cursor.getEdgeTarget(edge)))
            return;

        cursor.moveTo(cursor.getEdgeTarget(edge));
        processNode(cursor, seenEdges);

    }
    
    private boolean hasVisited(SBCursor<N, E> cursor,Object instance) {
        for(N i : cursor.getPath()) {
            if(i.equals(instance)) {
                return true;
            }
                
        }
        return false;
    }
}
