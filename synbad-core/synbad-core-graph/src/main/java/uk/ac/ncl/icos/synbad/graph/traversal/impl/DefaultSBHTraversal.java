/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterByPredicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByNotValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToEdges;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBWhile;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHasLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBChoose;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSelectPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBGraphSideEffect;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBAddLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBEdgeToVertex;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDoTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBLoop;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDeduplicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToInOutEdges;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHExpandNode;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHSetParent;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHChildToParent;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHContractNode;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHParentToChild;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultHGraph;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon

    */
    
public class DefaultSBHTraversal<T, V, G extends SBHGraph> extends DefaultSBGraphTraversal<T, V, G> implements SBHTraversal<T, V, G>, SBHTraversal.HGraphConfigurable<T, V, G> {

    private static final Logger logger = LoggerFactory.getLogger(DefaultSBHTraversal.class);
    
    public DefaultSBHTraversal(G graph) {
        super(graph);
    }

    protected DefaultSBHTraversal(DefaultSBHTraversal<T, V, G> traversal) {
        super(traversal);
    }

    @Override
    public HGraphConfigurable<T, V, G> asConfigurable() {
        return (HGraphConfigurable<T, V, G>)this;
    }

    
    @Override
    public DefaultSBHTraversal<T, V, G> setParent(String label) {
        return addPipe(new SBHSetParent<>(this, label));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> contract() {
        return addPipe(new SBHContractNode<>(this));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> expand() {
        return addPipe(new SBHExpandNode<>(this));
    }
        
    @Override
    public <E> DefaultSBHTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe) {
        if(getPipes().size() > 0) {
            SBTraversalPipe previousPipe = getPipes().get(getPipes().size() - 1);
            previousPipe.setNextPipe(nextPipe);
            nextPipe.setPreviousPipe(previousPipe);
        }

        getPipes().add(nextPipe);

        return (DefaultSBHTraversal<T, E, G>)this;
    }
    
    @Override
    public G toGraph() {
       DefaultHGraph<SBValued, SBEdge> outputGraph = null;
       Set<SBEdge> edges = new HashSet<>();

       while(getOutputPipe().hasNext()) {
           SBTraverser<V> t = getOutputPipe().next();
           
           if(!(SBValued.class.isAssignableFrom(t.get().getClass()))) {
                logger.error("Cannot create graph from traversal starting with Edge");
                return null;
           }
              
           
           if(outputGraph == null)
               outputGraph = new DefaultHGraph<>((SBValued)t.get());
           
           
           for(SBTraverser.SBTraverserPosition entity : t.getPath()) {
               if(SBEdge.class.isAssignableFrom(entity.getEntity().getClass()))
                    edges.add((SBEdge)entity.getEntity());
                else if(SBValued.class.isAssignableFrom(entity.getEntity().getClass()))
                    outputGraph.addNode(outputGraph.getRoot(), (SBValued)entity.getEntity());
           }               
       }

       for(SBEdge edge : edges) {
           outputGraph.addEdge((SBValued)graph.getEdgeSource(edge), (SBValued)graph.getEdgeTarget(edge), edge);
       }

       return (G)outputGraph;
    }

     @Override
    public DefaultSBHTraversal<T, ? extends SBEdge, G> e() {
        return addPipe(new SBVertexToEdges(this, null));
    }

    @Override
    public DefaultSBHTraversal<T, ? extends SBEdge, G> e(String... predicates) {
        return addPipe(new SBVertexToEdges(this, null, predicates));
    }

    @Override
    public DefaultSBHTraversal<T, ? extends SBEdge, G> e(SBDirection direction) {
        return addPipe(new SBVertexToEdges(this, direction));
    }

    @Override
    public DefaultSBHTraversal<T, ? extends SBEdge, G> e(SBDirection direction, String... predicates) {
        return addPipe(new SBVertexToEdges(this, direction, predicates));
    }

    
    @Override
    public DefaultSBHTraversal<T, ? extends SBEdge, G> e(String[] inPredicates, String[] outpredicates) {
        return addPipe(new SBVertexToInOutEdges(this, inPredicates, outpredicates));
    }
    
    

    @Override
    public DefaultSBHTraversal<T, ? extends SBValued, G> v() {
        return addPipe(new SBEdgeToVertex(this));
    }
    
    @Override
    public DefaultSBHTraversal<T, ? extends SBValued, G> v(SBDirection direction) {
        return addPipe(new SBEdgeToVertex(this, direction));
    }

    @Override
    public <V2 extends SBValued> DefaultSBHTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz) {
         return addPipe(new SBEdgeToVertex(this, direction, clazz));
    }
    
    @Override
    public DefaultSBHTraversal<T, V, G> has(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByValues(this, key, value, additionalValues));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByNotValues(this, key, value, additionalValues));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> as(String label) {
        return addPipe(new SBAddLabel(this, label));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> hasLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, false, label));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> hasNotLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, true, label));
    }


    @Override
    public DefaultSBHTraversal<T, V, G> filter(Predicate<V> predicate) {
        return (DefaultSBHTraversal<T, V, G>)addPipe(new SBFilterByPredicate<>(this, predicate));
    }

    @Override
    public <V2> DefaultSBHTraversal<T, V2, G> select(String... labels) {
        return (DefaultSBHTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, labels));
    }

    @Override
    public <V2> DefaultSBHTraversal<T, V2, G> select(Class<V2> clazz, String... label) {
        return (DefaultSBHTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, clazz, label));
    }
    
    @Override
    public <V2> DefaultSBHTraversal<T, V2, G> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2) {
        return (DefaultSBHTraversal<T, V2, G>)addPipe(new SBChoose<>(this,predicate, branch1, branch2));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits, boolean inclusive) {
        return (DefaultSBHTraversal<T, V, G>)addPipe(new SBWhile<>(this,predicate, doWhileTrue, emit, revisits, inclusive));
    }

    
    
    @Override
    public DefaultSBHTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits) {
        return (DefaultSBHTraversal<T, V, G>)addPipe(new SBWhile<>(this,predicate, doWhileTrue, emit, revisits));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> loop(int iterations, SBTraversal<?, V> traversal, boolean emit) {
        return (DefaultSBHTraversal<T, V, G>)addPipe(new SBLoop<>(this, traversal, iterations));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> deduplicate() {
        return (DefaultSBHTraversal<T, V, G>)addPipe(new SBDeduplicate<>(this));
    }
 
//    @Override
//    public <V2 extends SBValued> DefaultSBHTraversal<T, V2, G> addV(V2 v) {
//        return (DefaultSBHTraversal<T, V2, G>)addPipe(new SBAddVertex<T, V2, G>(this, v));
//    }
//
//    @Override
//    public <V2 extends SBValued> DefaultSBHTraversal<T, V2, G> addE(Class<V2> clazz, String label, String predicate) {
//        return (DefaultSBHTraversal<T, V2, G>)addPipe(new SBAddEdge<T, V2, G>(this, clazz, label, predicate));
//    }
    
    @Override
    public DefaultSBHTraversal<T, V, G> doTraversal(SBTraversal<V, V> traversal) {
        return addPipe(new SBDoTraversal<>(this, traversal));
    }

    @Override
    public DefaultSBHTraversal<T, V, G> copy() {
        final DefaultSBHTraversal<T, V, G> copy = new DefaultSBHTraversal<>(this);
        getPipes().stream().map(p -> p.copy(copy)).forEach(copy::addPipe);
        return copy;
    }
    
    

    @Override
    public <S extends SBGraph> DefaultSBHTraversal<T, V, G> doFunction(Function<V, S> function) {
        return (DefaultSBHTraversal<T, V, G>)addPipe(new SBGraphSideEffect<>(this, function));
    }

    @Override
    public SBHTraversal<T, ? extends SBValued, G> children() {
         return addPipe(new SBHParentToChild<>(this));
    }

    @Override
    public SBHTraversal<T, ? extends SBValued, G> children(Predicate<SBValued> predicate) {
        return addPipe(new SBHParentToChild<>(this, predicate));
    }

    @Override
    public SBHTraversal<T, ? extends SBValued, G> parent() {
        return addPipe(new SBHChildToParent<>(this));
    }

    @Override
    public SBHTraversal<T, ? extends SBValued, G> parent(Predicate<SBValued> predicate) {
        return addPipe(new SBHParentToChild<>(this, predicate));
    }

    

    
    

}


