/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.impl;

import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterByPredicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBFilterVertexByNotValues;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToEdges;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBWhile;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHasLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBChoose;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSelectPipe;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBGraphSideEffect;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBAddLabel;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBEdgeToVertex;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDoTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBLoop;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBDeduplicate;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBVertexToInOutEdges;
import uk.ac.ncl.icos.synbad.graph.SBPortDirection;
import uk.ac.ncl.icos.synbad.graph.SBPGraph;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultHGraph;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.SBPTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHContractNode;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHExpandNode;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHSetParent;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHChildToParent;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBHParentToChild;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */

public class DefaultSBPTraversal<T, V, G extends SBPGraph> extends DefaultSBGraphTraversal<T, V, G> implements SBPTraversal<T, V, G>, SBPTraversal.PGraphConfigurable<T, V, G> {

    private static final Logger logger = LoggerFactory.getLogger(DefaultSBPTraversal.class);
    
    public DefaultSBPTraversal(G graph) {
        super(graph);
    }

    protected DefaultSBPTraversal(DefaultSBPTraversal<T, V, G> traversal) {
        super(traversal);
    }

    @Override
    public PGraphConfigurable<T, V, G> asConfigurable() {
        return (PGraphConfigurable<T, V, G>)this;
    }

    @Override
    public SBPTraversal<T, V, G> setParent(String label) {
        return addPipe(new SBHSetParent<>(this, label));
    }

    @Override
    public <V extends SBValued> DefaultSBPTraversal<T, V, G> contract() {
        return addPipe(new SBHContractNode<>(this));
    }

    @Override
    public <V extends SBValued> DefaultSBPTraversal<T, V, G> expand() {
        return addPipe(new SBHExpandNode<>(this));
    }
        
    @Override
    public <E> DefaultSBPTraversal<T, E, G> addPipe(SBTraversalPipe<?, E> nextPipe) {
        if(getPipes().size() > 0) {
            SBTraversalPipe previousPipe = getPipes().get(getPipes().size() - 1);
            previousPipe.setNextPipe(nextPipe);
            nextPipe.setPreviousPipe(previousPipe);
        }

        getPipes().add(nextPipe);

        return (DefaultSBPTraversal<T, E, G>)this;
    }
    
    @Override
    public G toGraph() {
       DefaultHGraph<SBValued, SBEdge> outputGraph = null;
       Set<SBEdge> edges = new HashSet<>();

       while(getOutputPipe().hasNext()) {
           SBTraverser<V> t = getOutputPipe().next();
           
           if(!(SBValued.class.isAssignableFrom(t.get().getClass()))) {
                logger.error("Cannot create graph from traversal starting with Edge");
                return null;
           }
              
           
           if(outputGraph == null)
               outputGraph = new DefaultHGraph<>((SBValued)t.get());
           
           
           for(SBTraverser.SBTraverserPosition entity : t.getPath()) {
               if(SBEdge.class.isAssignableFrom(entity.getEntity().getClass()))
                    edges.add((SBEdge)entity.getEntity());
                else if(SBValued.class.isAssignableFrom(entity.getEntity().getClass()))
                    outputGraph.addNode(outputGraph.getRoot(), (SBValued)entity.getEntity());
           }               
       }

       for(SBEdge edge : edges) {
           outputGraph.addEdge((SBValued)graph.getEdgeSource(edge), (SBValued)graph.getEdgeTarget(edge), edge);
       }

       return (G)outputGraph;
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBEdge, G> e() {
        return addPipe(new SBVertexToEdges(this, null));
    }
    
    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> p() {
        return addPipe(new SBVertexToEdges(this, null));
    }
    
    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> p(SBPortDirection direction) {
        return addPipe(new SBVertexToEdges(this, null));
    }
    
    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> proxy() {
        return addPipe(new SBVertexToEdges(this, null));
    }
    
    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> proxied() {
        return addPipe(new SBVertexToEdges(this, null));
    }
    
    @Override
    public DefaultSBPTraversal<T, ? extends SBEdge, G> e(String... predicates) {
        return (DefaultSBPTraversal<T, SBEdge, G>)addPipe(new SBVertexToEdges(this, null, predicates));
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBEdge, G> e(SBDirection direction) {
        return addPipe(new SBVertexToEdges(this, direction));
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBEdge, G> e(SBDirection direction, String... predicates) {
        return addPipe(new SBVertexToEdges(this, direction, predicates));
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBEdge, G> e(String[] inPredicates, String[] outPredicates) {
        return addPipe(new SBVertexToInOutEdges<>(this, inPredicates, outPredicates));
    }

    
    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> v() {
        return addPipe(new SBEdgeToVertex(this));
    }
    
    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> v(SBDirection direction) {
        return addPipe(new SBEdgeToVertex(this, direction));
    }

    @Override
    public <V2 extends SBValued> DefaultSBPTraversal<T, V2, G> v(SBDirection direction, Class<V2> clazz) {
         return addPipe(new SBEdgeToVertex(this, direction, clazz));
    }
    
    @Override
    public DefaultSBPTraversal<T, V, G> has(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByValues(this, key, value, additionalValues));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> hasNot(String key, Object value, Object... additionalValues) {
        return addPipe(new SBFilterVertexByNotValues(this, key, value, additionalValues));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> as(String label) {
        return addPipe(new SBAddLabel(this, label));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> hasLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, false, label));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> hasNotLabel(String... label) {
        return addPipe(new SBHasLabel<>(this, true, label));
    }


    @Override
    public DefaultSBPTraversal<T, V, G> filter(Predicate<V> predicate) {
        return (DefaultSBPTraversal<T, V, G>)addPipe(new SBFilterByPredicate<>(this, predicate));
    }
    
    @Override
    public <V2> DefaultSBPTraversal<T, V2, G> select(String... labels) {
        return (DefaultSBPTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, labels));
    }

    @Override
    public <V2> DefaultSBPTraversal<T, V2, G> select(Class<V2> clazz, String... label) {
        return (DefaultSBPTraversal<T, V2, G>)addPipe(new SBSelectPipe<>(this, clazz, label));
    }
    
    @Override
    public <V2> DefaultSBPTraversal<T, V2, G> choose(Predicate<V> predicate, SBTraversal<V, V2> branch1, SBTraversal<V, V2> branch2) {
        return (DefaultSBPTraversal<T, V2, G>)addPipe(new SBChoose<>(this,predicate, branch1, branch2));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits, boolean inclusive) {
        return (DefaultSBPTraversal<T, V, G>)addPipe(new SBWhile<V>(this,predicate, doWhileTrue, emit, revisits, inclusive));
    }

    @Override
    public  DefaultSBPTraversal<T, V, G> loop(Predicate<V> predicate, SBTraversal<?, V> doWhileTrue, boolean emit, boolean revisits) {
        return (DefaultSBPTraversal<T, V, G>)addPipe(new SBWhile<V>(this,predicate, doWhileTrue, emit, revisits));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> loop(int iterations, SBTraversal<?, V> traversal, boolean emit) {
        return (DefaultSBPTraversal<T, V, G>)addPipe(new SBLoop<>(this, traversal, iterations));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> deduplicate() {
        return (DefaultSBPTraversal<T, V, G>)addPipe(new SBDeduplicate<>(this));
    }
 
//    @Override
//    public <V2 extends SBValued> DefaultSBPTraversal<T, V2, G> addV(V2 v) {
//        return (DefaultSBPTraversal<T, V2, G>)addPipe(new SBAddVertex<T, V2, G>(this, v));
//    }
//
//    @Override
//    public <V2 extends SBValued> DefaultSBPTraversal<T, V2, G> addE(Class<V2> clazz, String label, String predicate) {
//        return (DefaultSBPTraversal<T, V2, G>)addPipe(new SBAddEdge<T, V2, G>(this, clazz, label, predicate));
//    }
    
    @Override
    public DefaultSBPTraversal<T, V, G> doTraversal(SBTraversal<V, V> traversal) {
        return addPipe(new SBDoTraversal<>(this, traversal));
    }

    @Override
    public DefaultSBPTraversal<T, V, G> copy() {
        final DefaultSBPTraversal<T, V, G> copy = new DefaultSBPTraversal<>(this);
        getPipes().stream().map(p -> p.copy(copy)).forEach(copy::addPipe);
        return copy;
    }

    @Override
    public <S extends SBGraph> DefaultSBPTraversal<T, V, G> doFunction(Function<V, S> function) {
        return (DefaultSBPTraversal<T, V, G>)addPipe(new SBGraphSideEffect<>(this, function));
    }

//    @Override
//    public <V2 extends SBValued> DefaultSBPTraversal<T, V2, G> addP(V2 v, SBPortDirection direction) {
//        return (DefaultSBPTraversal<T, V2, G>)addPipe(new SBAddPort<T, V2, G>(this, v, direction));
//    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> children() {
        return addPipe(new SBHParentToChild<>(this));
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> children(Predicate<SBValued> predicate) {
        return addPipe(new SBHParentToChild<>(this, predicate));
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> parent() {
        return addPipe(new SBHChildToParent<>(this));
    }

    @Override
    public DefaultSBPTraversal<T, ? extends SBValued, G> parent(Predicate<SBValued> predicate) {
        return addPipe(new SBHChildToParent<>(this, predicate));
    }

}


