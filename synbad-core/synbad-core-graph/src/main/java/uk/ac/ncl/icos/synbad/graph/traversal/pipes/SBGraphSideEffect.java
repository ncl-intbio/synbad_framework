/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 * 
 * @author owengilfellon
 */
public class SBGraphSideEffect<V, G1 extends SBGraph, G2 extends SBGraph> extends DefaultSBTraversalPipe<V, V> {

    private final Function<V, G2> function;

    public SBGraphSideEffect(SBTraversal traversal, Function<V, G2> function) {
        super(traversal);
        this.function = function;
    }
    
    protected SBGraphSideEffect(SBTraversal traversal, SBGraphSideEffect<V, G1, G2> contractNode, Function<V, G2> function) {
        super(traversal, contractNode);
        this.function = function;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<V> traverser) {
        function.apply(traverser.get());
        return Collections.singleton(traverser);
    }

    @Override
    public SBTraversalPipe<V, V> copy(SBTraversal parentTraversal) {
        return new SBGraphSideEffect<>((getParentTraversal()), function);
    }
    
}
