/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.event;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBDispatcher implements SBDispatcher {

    private static final long serialVersionUID = 2197125473114782184L;
    private final List<SBSubscriber> subscribers = new LinkedList<>();
    transient private  Map<SBSubscriber, Set<SBEventFilter>> records2 = new WeakHashMap<>();
    private final Multimap<SBEventFilter, SubscriberRecord> records = HashMultimap.create();
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBDispatcher.class);
    private Queue<SBEvent> queuedEvents = new LinkedList<>();
    private boolean pushingEvents = false;

    private final boolean CACHING = true;


    @Override
    public List<SBSubscriber> getListeners() {
        return records.values().stream().map(r -> r.subscriber.get())
                .filter(s -> s!= null).collect(Collectors.toList());
    }

    private void writeObject(ObjectOutputStream out) throws IOException
    {
        out.defaultWriteObject();
        out.writeObject(new HashMap<>(records2));
    }

    private void readObject(ObjectInputStream in) throws IOException
    {
        try {
            in.defaultReadObject();
            this.records2 = new WeakHashMap<>((Map<SBSubscriber, Set<SBEventFilter>>)in.readObject());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        records.clear();
    }

    private void doEvent(SBEvent event) {
        List<SBSubscriber> filtered = new ArrayList<>();
        for(SBEventFilter filter : new ArrayList<>(records.keySet())) {
            if(filter.test(event)) {
                filtered.addAll(records.get(filter).stream()
                        .map(r -> r.subscriber.get())
                        .filter(s -> s != null)
                        .collect(Collectors.toList()));
            }
        }

        if(filtered.isEmpty())
            return;

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Publishing to {} subscribers: {}", filtered.size(), event);

        for(SBSubscriber subscriber : filtered) {
            subscriber.onEvent(event);
        }
    }

    private  void pushEvents() {
        if(!pushingEvents) {

            pushingEvents = true;

            while(!queuedEvents.isEmpty()) {
                SBEvent e = queuedEvents.poll();
                doEvent(e);
            }

            pushingEvents = false;
        }
    }

    @Override
    public void publish(Collection<SBEvent> events) {
        if(CACHING) {
            queuedEvents.addAll(events);
            pushEvents();
        } else {
            events.forEach(this::doEvent);
        }

    }

    @Override
    public  void publish(SBEvent event) {
        if(CACHING) {
            queuedEvents.add(event);
            pushEvents();
        } else {
            doEvent(event);
        }
    }

    @Override
    public synchronized <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
       
        if(!records2.containsKey(handler))
            records2.put(handler, new HashSet<>());
        
        records2.get(handler).add(filter);
        records.put(filter, new SubscriberRecord(handler.getClass(), filter, handler));

        subscribers.add(handler);

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("[ {} ] subscribed with [ {} ] ({} total subscribers)", handler, filter.getClass().getSimpleName(), records2.size());
    }

    @Override
    public synchronized <T> void unsubscribe(SBSubscriber handler) {
        
        if(!records2.containsKey(handler)) {
            return;
        }
        
        Set<SBEventFilter> filters = records2.get(handler);
        
        for(SBEventFilter filter: filters) {
            records.remove(filter, handler);
        }

        subscribers.remove(handler);
        
        records2.remove(handler);

        if(LOGGER.isTraceEnabled())
            LOGGER.trace("[ {} ] unsubscribed ({} total subscribers)", handler,  records2.size());

    }

    class SubscriberRecord implements Serializable {
        
        final Class<?> clazz;
        final SBEventFilter filter;
        transient  WeakReference<SBSubscriber> subscriber;

        public SubscriberRecord(Class<?> clazz, SBEventFilter filter, SBSubscriber subscriber) {
            this.clazz = clazz;
            this.filter = filter;
            this.subscriber = new WeakReference<>(subscriber);
        }

        private void writeObject(ObjectOutputStream out) throws IOException
        {
            out.defaultWriteObject();
            out.writeObject(subscriber.get());
        }

        private void readObject(ObjectInputStream in) throws IOException
        {
            try {
                in.defaultReadObject();
                this.subscriber = new WeakReference<>((SBSubscriber) in.readObject());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null)
                return false;
            
            if (obj == this)
                return true;
            
            if (!(obj instanceof SubscriberRecord))
                return false;
            
            SubscriberRecord r = (SubscriberRecord) obj;

            if(r.clazz != null && !r.clazz.equals(clazz))
                return false;
            
            SBSubscriber s1 = r.subscriber.get();
            SBSubscriber s2 = subscriber.get();
            
            
            
            if((s1 == null || s2 == null) && s1 != s2)
                return false;
                
            return r.filter.equals(filter) && ((s1 == null && s2 == null) || s1.equals(s2));
        }

        @Override
        public int hashCode() {
            return 7 * 
               (clazz == null ? 27 : clazz.hashCode()) + 
               filter.hashCode() + 
               (subscriber.get() == null ? 0 : subscriber.get().hashCode());
        }
    }
} 
