/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.pipes;

import java.util.Collection;
import java.util.Collections;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;

/**
 *
 * @author owengilfellon
 */
public class SBAddEdge<T, V> extends DefaultSBTraversalPipe<T, V>{

    private final String label;
    private final String predicate;
    private final Class<V> clazz;
    
    public SBAddEdge(SBTraversal traversal, Class<V> clazz, String label, String predicate) {
        super(traversal);
        this.label = label;
        this.clazz = clazz;
        this.predicate = predicate;
    }
    
    protected SBAddEdge(SBTraversal traversal, SBAddEdge<T, V> addEdge) {
        super(traversal, addEdge);
        this.label = addEdge.label;
        this.clazz = addEdge.clazz;
        this.predicate = addEdge.predicate;
    }

    @Override
    public Collection<SBTraverser<V>> processTraverser(SBTraverser<T> traverser) {
        
        T t = traverser.get();
        
        if(!clazz.isAssignableFrom(t.getClass()) && SBValued.class.isAssignableFrom(clazz)) {
            return Collections.EMPTY_SET;
        }
        
        V from = clazz.cast(t);

        SBGraph graph = ((SBGraphTraversal.Configurable<?, T, ?>)getParentTraversal()
                .asConfigurable())
                .getGraph();

        V to = traverser.getByLabel(label).stream()
                .filter(b -> clazz.isAssignableFrom(b.getClass()))
                .map(b -> clazz.cast(b))
                .findFirst().orElse(null);
        
        if(to != null)
             return Collections.EMPTY_SET;
        
        SBValued fromObj = SBValued.class.cast(from);
        SBValued toObj = SBValued.class.cast(to);
        
       // graph.addEdge(fromObj, toObj, new SBEdge.DefaultEdge(URI.create(predicate), fromObj.getIdentity(), toObj.getIdentity()));
        return Collections.singleton(traverser.push(to));

    }

    @Override
    public SBTraversalPipe<T, V> copy(SBTraversal parentTraversal) {
        return new SBAddEdge<>(parentTraversal, this);
    }


}
