/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.graph.traversal.cursor;


import java.util.Collection;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBHCursor<N, E> extends SBCursor<N, E> {

    public N getOriginalEdgeSource(E edge);
    
    public N getOriginalEdgeTarget(E edge);

    public N getParent();
    
    public Collection<N> getChildren();

    public int getDepth();

    public void expand();
    
    public void collapse();
}
