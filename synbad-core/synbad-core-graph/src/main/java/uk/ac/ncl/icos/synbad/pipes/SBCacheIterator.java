/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.pipes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author owengilfellon
 */
public class SBCacheIterator<T> implements Iterator<T> {
    
    private int itemIndex = 0;
    private List<T> items = new ArrayList<>();

    public boolean add(Iterator<? extends T> iterator) {
        while(iterator.hasNext()) {
            items.add(iterator.next());
        }
        return true;
    }
    
    public boolean add(T item) {
        return items.add(item);
    }
    
    @Override
    public boolean hasNext() {
        return items.size() > 0 && items.size() > (itemIndex);
    }

    @Override
    public T next() {
        if(items.size() <= (itemIndex - 1))
            throw new NullPointerException("Iterator has no next object");
        return items.get(itemIndex++);
    }
    
    public void reset() {
        this.items.clear();
        this.itemIndex = 0;
    }
    
    public SBCacheIterator<T> copy() {
        SBCacheIterator<T> i = new SBCacheIterator<>();
        i.itemIndex = itemIndex;
        i.items = items.stream().collect(Collectors.toList());
        return i;
    }
    
}
