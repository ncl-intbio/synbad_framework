/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.graph.SBGraph;

import java.util.Collection;

/**
 *
 * @author owengilfellon
 */
public interface SBViewUpdater<T extends SBGraph, V extends SBGraph>   {

    void close();

    T getSourceGraph();

    V getTargetGraph();

    Collection<String> getHierarchyPredicates();

}
