/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.pipes;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;


/**
 * A SBTraversalSource initiates a traversal from an initial list of nodes
 * or edges.
 * 
 * @author owengilfellon
 */
public interface SBViewPipelineSource<N extends SBEvent> {

    public SBViewPipeline<?, N> push(N... events);

}
