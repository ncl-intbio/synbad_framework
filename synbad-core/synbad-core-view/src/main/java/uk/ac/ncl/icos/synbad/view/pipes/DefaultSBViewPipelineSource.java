/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.pipes;

import uk.ac.ncl.icos.synbad.pipes.*;
import java.util.Arrays;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBViewPipelineSource<N extends SBEvent> implements SBViewPipelineSource<N> {


    public DefaultSBViewPipelineSource() {
    }

    @Override
    public SBViewPipeline<N, N> push(N... objs) {
        DefaultSBViewPipeline<N, N> trav = new DefaultSBViewPipeline<>();
        trav = trav.addPipe(new SBSourcePipe<>(trav));
        trav.asConfigurable().addInput(Arrays.stream(objs)
            .map(n -> (SBTraverser<N>)new DefaultSBTraverser<>(n)).iterator());
        return trav;
    }

}
