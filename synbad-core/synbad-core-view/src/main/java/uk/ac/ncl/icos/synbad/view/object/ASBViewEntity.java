/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.view.helpers.SBViewEntityIdGenerator;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public abstract class ASBViewEntity<T> implements SBViewEntity<T> {

    private static final long serialVersionUID = 1056412621625602654L;
    private final SBView view;
    private final long id;

    public ASBViewEntity(SBView view) {
        this.view = view;
        this.id = SBViewEntityIdGenerator.getGenerator().next();
    }

    public ASBViewEntity(SBView view, long id) {
        this.view = view;
        this.id = id;
    }

    @Override
    public SBWorkspace getWorkspace() {
        return view.getModel().getWorkspace();
    }

    @Override
    public SBView getView() {
        return view;
    }

    @Override
    public long getId() {
        return id;
    }
}