package uk.ac.ncl.icos.synbad.model;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEventSource;

import java.io.Serializable;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * SynBad data model is based on SBOL2.0, and captures entities such as  modules,
 * components and interactions. Entities are created and instantiated using the
 * data model. Properties can be added to entities to capture additional information.
 *
 * Represents the model defined by a root object and related objects as defined by
 * a Main View. Additional views can be created based on the main view.
 * 
 * @author owengilfellon
 */
public interface SBModel<N extends SBValued> extends SBEventSource, Serializable {

    SBWorkspace getWorkspace();
    
    N getRootNode();
    
    void close();

    Long getId();

}
