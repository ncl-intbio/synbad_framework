/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.SBTraversalResult;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WorkspaceTraversal;
import uk.ac.ncl.icos.synbad.workspace.graph.traversal.WsNodeTraverser;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewValued;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceSubscriber;
import uk.ac.ncl.icos.synbad.graph.SBWritableGraph;

/**
 *
 * @author owengilfellon
 */
public final class FlatUpdater<
        T extends SBWritableGraph<SBValued, SynBadEdge>, 
        V extends SBWritableView<SBViewValued<SBValued>, SBViewEdge<SBViewValued<SBValued>, SBValued, SynBadEdge>>>
            extends AViewUpdater<T, V> implements SBWorkspaceSubscriber {
        
    private static final Logger LOGGER = LoggerFactory.getLogger(FlatUpdater.class);

    private static final List<String> HIERARCHY_PREDICATES = Arrays.asList(SynBadTerms.Sbol.definedBy,
        SynBadTerms.SbolComponent.hasComponent,
        SynBadTerms.SbolModule.hasFunctionalComponent,
        SynBadTerms.SbolModule.hasInteraction,
        SynBadTerms.SbolModule.hasModule
    );
    
    private final String[] predicates;
    private final SBValued root;
    private final SBWorkspaceGraph graph;
    private final SBWorkspace ws;

    public FlatUpdater(T source, V target, SBValued root) {
        super(source, target, root);
        this.root = root;
        this.ws = root.getWorkspace();
        this.graph = new SBWorkspaceGraph(ws, root.getContexts());
        this.graph.subscribe(new SBEventFilter.DefaultFilter(), this);
        this.predicates = HIERARCHY_PREDICATES.toArray(new String[] {});
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing graph on construction: [ {} ]", root);
        processGraph(SBGraphEventType.ADDED, root);
    }
    
    public FlatUpdater(T source, V target, SBValued root, String... predicates) {
        super(source, target, root);
        this.root = root;
        this.ws = this.root.getWorkspace();
        this.graph = new SBWorkspaceGraph(ws, root.getContexts());
        this.graph.subscribe(new SBEventFilter.DefaultFilter(), this);
        if(predicates.length > 0)
            this.predicates = predicates;
        else
            this.predicates = HIERARCHY_PREDICATES.toArray(new String[] {});
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing graph on construction: [ {} ]", root);
        processGraph(SBGraphEventType.ADDED, root);
    }
 
    @Override
    public void close() {
        this.graph.close();
    }

    @Override
    public Collection<String> getHierarchyPredicates() {
        return HIERARCHY_PREDICATES;
    }
    
    public void processGraph(SBGraphEventType type, SBValued identified) {
        WsNodeTraverser<SBValued, SBValued, SBWorkspaceGraph> t = graph.getTraversal()
                .v(identified)
                .loop(o -> true, new WorkspaceTraversal<>(graph)
                    .e(SBDirection.OUT, predicates)
                    .v(SBDirection.OUT), true, true);
        
        SBTraversalResult<SBValued, SynBadEdge> result = t.getResult();

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Got result from {} traversers", result.getTraverserCount());
        
        result.streamVertexes().map(v -> new DefaultSBViewValued<SBValued>(v, target)).forEach(target::addNode);
        for(SynBadEdge e : result.streamEdges().collect(Collectors.toSet())) {
            SBViewValued<SBValued> from = target.findObjects(e.getFrom()).iterator().next();
            SBViewValued<SBValued> to = target.findObjects(e.getTo()).iterator().next();
            target.addEdge(from, to, new DefaultSBViewEdge(e, from, to, target));
        }
    }
    
    @Override
    public void onValueEvent(SBGraphEvent<SBValue> e) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: V [ {} ] " + e.getClass());
    }
    
    public void processNode(URI uri, SBGraphEventType type) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Processing node: [{}:{}]", uri.toASCIIString(), type.getType());
        SBValued node = ws.getObject(uri, SBValued.class, root.getContexts());
        if(type == SBGraphEventType.ADDED)
            target.addNode(new DefaultSBViewValued<>(node, target));
    }

    @Override
    public void onEvent(SBEvent e) {
        
        if(SBGraphEvent.class.isAssignableFrom(e.getClass())) {

            SBGraphEvent evt = (SBGraphEvent) e;
                    
            if(null != evt.getGraphEntityType())
                switch (((SBGraphEvent)e).getGraphEntityType()) {
                    case EDGE:
                        onEdgeEvent(evt);
                        break;
                    case NODE:
                        onNodeEvent(evt);
                        break;
                    case VALUE:
                        onValueEvent(evt);
                        break;
                    default:
                        break;
            }
        }
    }
    
    @Override
    public void onNodeEvent(SBGraphEvent<URI> e) {
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: N [ {} ]",  e.getData().toASCIIString());
        URI nodeIdentity = e.getData();
        
        // If nodes already exists don't need to traverse tree (unless is root node,
        // which is instantiated in graph without hierarchy traversal)
        
        if(!target.findObjects(nodeIdentity).isEmpty() && !root.getIdentity().equals(nodeIdentity) )
            return;
        
        //target.addNode(null)
        //processNode(nodeIdentity, (SBGraphEventType)e.getType());        
    }

    @Override
    public void onEdgeEvent(SBGraphEvent<SynBadEdge> e) {

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Received event: E [ {} ]",  e.getData());
        URI fromIdentity = e.getData().getFrom();
        URI toIdentity = e.getData().getTo();
        
        // Nodes should already be in graph
        
        if(target.findObjects(toIdentity).isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Does not exist in target: N [ {} ] ", toIdentity.toASCIIString());
            return;
        } else if(target.findObjects(fromIdentity).isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Does not exist in target: N [ {} ] ", fromIdentity.toASCIIString());
            return;
        }
        
        if(!target.findObjects(fromIdentity).isEmpty() && !root.getIdentity().equals(fromIdentity) )
            return;
    }
}
