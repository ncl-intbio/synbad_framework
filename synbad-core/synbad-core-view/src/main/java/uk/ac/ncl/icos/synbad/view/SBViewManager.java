/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.view.SBView;

/**
 *
 * @author owengilfellon
 */
public class SBViewManager<V extends SBView> implements Serializable, AutoCloseable {

    private static final long serialVersionUID = -4296287465337312760L;
    private final V mainView;
    private final Set<V> createdViews = new HashSet<>();
    private V currentView;
    private static final Logger LOGGER = LoggerFactory.getLogger(SBViewManager.class);

    public SBViewManager(V mainView) {
        this.mainView = mainView;
        this.currentView = mainView;
    }
    
    @Override
    public void close() {
        LOGGER.info("Closing {}", getCurrentView().getId());
        mainView.close();
        createdViews.forEach(v -> v.close());
        currentView = null;
    }

    // main view, maintains all nodes as visible
    
    /**
     * Returns the main view - a view which can not be removed.
     * @return 
     */
    public V getMainView() {
        return mainView;
    }

    /**
     * Returns the current view, by default, the main view.
     * @return 
     */
    public V getCurrentView() {
        return currentView;
    }
    
    /**
     * Sets the current view to another view.
     * @param view A View created by this view manager.
     * @return 
     */
    public boolean setCurrentView(V view) {
        if(createdViews.stream().map(v -> v.getId())
                .noneMatch(id -> id.equals(view.getId())))
            return false;
            
        this.currentView = view;
        return true;
    }
   
    // created views only
    
    public Collection<V> getViews() {
        return new HashSet<>(createdViews);
    }
    
    public boolean addView(V view) {
        if(createdViews.stream().map(v -> v.getId())
                .anyMatch(id -> id.equals(view.getId())))
            return false;
        
        return createdViews.add(view);
    }
    
    /**
     * Removes a view from the view manager. If the removed view is the current
     * view, the current view is set to the main view.
     * @param view
     * @return 
     */
    public boolean removeView(V view) {
        
        if(createdViews.contains(view) && currentView.getId().equals(view.getId())) {
            currentView = mainView;
        }
        
        return createdViews.remove(view);
    }
}
