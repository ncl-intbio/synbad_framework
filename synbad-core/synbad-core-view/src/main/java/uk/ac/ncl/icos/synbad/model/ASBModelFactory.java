/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.model;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;

/**
 *
 * @author owen
 */
public abstract class ASBModelFactory<M extends SBModel, N extends SBIdentified> implements SBModelFactory<M, N> {

    private final Class<M> modelClazz;
    private final Class<N> rootNodeClazz;

    public ASBModelFactory(Class<M> modelClazz, Class<N> rootNodeClazz) {
        this.modelClazz = modelClazz;
        this.rootNodeClazz = rootNodeClazz;
    }

    @Override
    public Class<M> getModelClass() {
        return modelClazz;
    }

    @Override
    public Class<N> getRootNodeClass() {
        return rootNodeClazz;
    }

}
