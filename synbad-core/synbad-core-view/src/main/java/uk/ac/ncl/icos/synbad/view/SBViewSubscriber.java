/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphSubscriber;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;

/**
 *
 * @author owengilfellon
 */
public interface SBViewSubscriber extends SBGraphSubscriber<DefaultSBViewIdentified, SBViewEdge>{

    public void onValueEvent(SBGraphEvent<SBValue> e);
 
}
