/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.model;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.stream.Collectors;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owen
 */
@ServiceProvider(service = SBModelManager.class)
public class DefaultSBModelFactory implements SBModelManager {
    
    private final Multimap<Class<? extends SBModel>, SBModelFactory<? extends SBModel, ? extends SBValued>> modelToFactory;
    private final Multimap<Class<? extends SBValued>, SBModelFactory<? extends SBModel, ? extends SBValued>> rootToFactory;

    public DefaultSBModelFactory() {
        this.modelToFactory = HashMultimap.create();
        this.rootToFactory = HashMultimap.create();
        
        Lookup.getDefault().lookupAll(SBModelFactory.class).stream().forEach(mf -> {
            this.modelToFactory.put(mf.getModelClass(), mf);
            this.rootToFactory.put(mf.getRootNodeClass(), mf);
        });
    }

    @Override
    public <R extends SBValued> Collection<SBModelFactory<? extends SBModel, R>> getProviders(Class<R> rootNode) {
        return rootToFactory.get(rootNode).stream()
                .map(f -> (SBModelFactory<? extends SBModel, R>)f)
                .collect(Collectors.toSet());
    }
    
    @Override
    public <T extends SBModel, V extends SBValued> T createModel(Class<T> modelClazz, V rootNode) {
        SBModelFactory<T, V> f =  modelToFactory.get(modelClazz).stream()
            .filter(factory -> factory.getRootNodeClass().isAssignableFrom(rootNode.getClass()))
            .map(factory -> (SBModelFactory<T, V>) factory)
            .findFirst().orElse(null);

        if(f == null)
            return null;

        T model = f.getModel(rootNode);
        
        if(model == null)
            return null;

        return model;
    }
}
