package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;

public interface SBViewTransformer<T extends SBGraph> extends SBTraversal<T, T>, SBWorkspaceService {
    
    

    
}
