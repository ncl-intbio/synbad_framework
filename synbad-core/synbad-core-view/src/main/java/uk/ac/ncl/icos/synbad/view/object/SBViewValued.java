/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 * Represents objects within an SBView. A single object can be instantiated 
 * multiple times within the same view.
 * @param <O> The type of the object instanced in the view by this view object.
 * @author owengilfellon
 */
public interface SBViewValued<O extends SBValued> extends SBValued, SBViewEntity<O> {
    
    /**
     * @return The main object represented by this view object.
     */
    O getObject();

    
    /**
     * 
     * @param clazz
     * @return True if the main object is, or is a subclass of, the provided class.
     */
    boolean isClass(Class<? extends SBValued> clazz);

    /**
     * 
     * @param <T> The type of the object that is returned.
     * @param clazz
     * @return The current object, cast to a subclass of SBViewValued. If this
     * object is not an instance of the provided class, returns null.
     */
    <T extends SBViewValued> T getAsSubclass(Class<T> clazz);

    /**
     * 
     * @param <T> The type of the main object instanced by this object.
     * @param clazz
     * @return  A generic version of this object, typed according to the provided
     * class, if it's wrapped object is a subclass of it. Otherwise returns null.
     */
    <T extends O> SBViewValued<T> asViewObj(Class<T> clazz);
}
