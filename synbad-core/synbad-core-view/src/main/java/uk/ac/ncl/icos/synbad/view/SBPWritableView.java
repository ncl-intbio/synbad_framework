/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.graph.WritablePGraph;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 * Wraps around a graph instance, allowing read only access to the graph...
 * @author owengilfellon
 */
public interface SBPWritableView<
        W extends SBViewValued, 
        X extends SBViewEdge, 
        Y extends SBViewPort> extends SBPView<W, X, Y>, WritablePGraph<W, Y, X> {

}
