/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.pipes;

import uk.ac.ncl.icos.synbad.pipes.*;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;

/**
 *
 * @author owengilfellon
 */

    
    
public class DefaultSBViewPipeline<T, V> extends ASBTraversal<T, V> implements SBViewPipeline.Configurable<T, V>{

    public DefaultSBViewPipeline() {
        super();
    }
    
    protected DefaultSBViewPipeline(DefaultSBViewPipeline<T, V> traversal) {
        super();
    }
    
    /*
        ============================================================
            Internal methods
        ============================================================
    */

    @Override
    public <E> DefaultSBViewPipeline<T, E> addPipe(SBTraversalPipe<?, E> nextPipe) {
        
        if(getPipes().size() > 0) {
            SBTraversalPipe previousPipe = getPipes().get(getPipes().size() - 1);
            previousPipe.setNextPipe(nextPipe);
            nextPipe.setPreviousPipe(previousPipe);
        }

        getPipes().add(nextPipe);

        return (DefaultSBViewPipeline<T, E>)this;
    }

    @Override
    public SBViewPipeline.Configurable<T, V> asConfigurable() {
        return (SBViewPipeline.Configurable<T,V>) this;
    }


    @Override
    public DefaultSBViewPipeline<T, V> copy() {
        final DefaultSBViewPipeline<T, V> copy = new DefaultSBViewPipeline<>(this);
        getPipes().stream().map(p -> p.copy(copy)).forEach(copy::addPipe);
        return copy;
    }

}


