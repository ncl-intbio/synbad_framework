/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.net.URI;
import java.util.Collection;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBHCursor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultHGraph;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.SBGlobalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGlobalNodeProvider;
import uk.ac.ncl.icos.synbad.graph.SBHGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHIterator;
import uk.ac.ncl.icos.synbad.graph.traversal.SBHTraversalSource;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBHTraversalSource;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;

/**
 *
 * @author owengilfellon
 */
public abstract class DefaultSBHView<W extends SBViewValued, X extends SBViewEdge> 
        extends AView<W, X > implements SBHWritableView<W, X>, SBGlobalEdgeProvider<W, X>, SBGlobalNodeProvider<W>, SBSubscriber {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBHView.class);
    private final W rootNode;
    protected final DefaultHGraph<W, X> graph;

    abstract protected W createViewObject(W object); 

    abstract protected X createViewEdge(X edge);

    abstract protected <V extends SBValued> W createViewObject(V object); 

    abstract protected <E extends SBEdge> X createViewEdge(E edge, W from, W to);

    public DefaultSBHView(SBValued rootNode) {
        super();
        this.rootNode = createViewObject(rootNode);
        this.graph = new DefaultHGraph<>(this.rootNode);
        registerNode(this.rootNode);
        graph.subscribe(new SBEventFilter.DefaultFilter(), this);
    }
    
    private String getPrettyName(Object node) {
        if (SBIdentified.class.isAssignableFrom(node.getClass()))
            return ((SBIdentified)node).getDisplayId();
        else if (DefaultSBViewEdge.class.isAssignableFrom(node.getClass())) {
           DefaultSBViewEdge edge = ((DefaultSBViewEdge)node);
           return getPrettyName(edge.getEdge().getEdge()) + ": " + getPrettyName(edge.getFrom()) + " -> " + getPrettyName(edge.getTo());
        } else if (URI.class.isAssignableFrom(node.getClass()))
            return ((URI)node).getFragment();
        else
            return node.toString();
    }
    
    // ===============================================
      
    @Override
    public boolean removeAllEdges(Collection<? extends X> edges) {
        
        boolean b = true;
        
        for(X edge : edges) {
            if(!removeEdge(edge))
                b = false;
        }
        
        return b;
    }

    @Override
    public Set<X> removeAllEdges(W sourceNode, W targetNode) {
        Set<X> edges = new HashSet<>() ;
        for(X e: getAllEdges(sourceNode, targetNode)) {
            if(removeEdge(e))
                edges.add(e);
        }
        
        return edges;
    }

    @Override
    public boolean removeAllNodes(Collection<? extends W> nodes) {
        
        boolean b = true;
        
        for(W o : nodes) {
            if(!removeNode(o))
                b = false;
        }
        
        return b;
    }
    @Override
    public boolean addNode(W instance, W parent, int index) {
        
        if(parent == null)
            return false;

        W c = createViewObject(instance);
        
        if(graph.containsNode(c))
            return false;
        
        // is the node expanded or not?
        W p = parent.getView().equals(this) ? parent : createViewObject(parent);
        
        if(graph.addNode(c, p, index)) {
            registerNode(c);
            dispatchNodeEvent(SBGraphEventType.ADDED, c, p);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Added {} to {} at index {}", getPrettyName(c), getPrettyName(p), index);
            if(isVisible(p))
                 dispatchNodeEvent(SBGraphEventType.VISIBLE, c, p);
            return true;
        } else {
            LOGGER.warn("Could not add: [ {} ] to [ {} ]", c, p);
            return false;
        }
    }

    @Override
    public boolean addNode(W child, W parent) {

        if(parent == null)
            return false;
        
        return addNode(child, parent, getChildren(parent).size());

    }

    /*
       @Override
    public boolean addNode(W node) {
        W c = createViewObject(node);
        addNode( c, rootNode);
        return true;
    }*/
    
    
    
    @Override
    public boolean removeNode(W object) {
        
        W o = object.getView().equals(this) ? object : createViewObject(object);

        if(!graph.containsNode(o))
            return false;
        
        // is the node expanded or not?
        if(!graph.removeNode(o)) 
            return false;
        
        deregisterNode(o);

        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Removed {}", getPrettyName(o));
            
        
        dispatchNodeEvent(SBGraphEventType.REMOVED, object);
        return true;
    }
    
     
    @Override
    public boolean addEdge(W from, W to, X edge) {
        
        W f = from.getView().equals(this) ? from : createViewObject(from);
        W t = to.getView().equals(this) ? to :createViewObject(to);
        
        /*if(!graph.isVisible(f)) {
            LOGGER.warn("Cannot add edge {} - source not visible: [ {} ]", edge, f);
            return false;
        } else if ( !graph.isVisible(t)) {
            LOGGER.warn("Cannot add edge {} - target not visible: [ {} ]", edge, t);
            return false;    
        }*/
           
        X e = createViewEdge(edge);
        
        if(graph.addEdge(f, t, e)) {
            registerEdge(e);
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Added {}", getPrettyName(e));
            dispatchEdgeEvent(SBGraphEventType.ADDED, e);
            if(isVisible(f) && isVisible(t))
                 dispatchEdgeEvent(SBGraphEventType.VISIBLE, edge);
            return true;
        }
        return false;
    }
    
    
    @Override
    public X removeEdge(W sourceNode, W targetNode) {
        X e =  graph.getEdge(sourceNode, targetNode);
        
        if(e == null)
            return null;

        removeEdge(e);
        return e;

    }

    

    @Override
    public boolean removeEdge(X edge) {

        X e = createViewEdge(edge);
        
        if(e == null || !isEdgeVisible(e))
            return false;

        if(!graph.removeEdge(e))
            return false;
        
        deregisterEdge(e);
        if(LOGGER.isDebugEnabled())
            LOGGER.debug("Removed {}", getPrettyName(e));
        dispatchEdgeEvent(SBGraphEventType.REMOVED, e);

        return true;
    }
    
    // ===============================================
    
    @Override
    public void onEvent(SBEvent e) {
        if(e.getType() ==  SBGraphEventType.VISIBLE || 
                e.getType() == SBGraphEventType.HIDDEN) {
            if(LOGGER.isDebugEnabled())
                LOGGER.debug("Relaying evt: " + e);
            dispatcher.publish(e);
        }
    }
    
     public void dispatchNodeEvent(SBEventType type, W node, W parent) {
        SBEvent e =  new DefaultSBGraphEvent(
                type, SBGraphEntityType.NODE,
                node, node.getObject().getClass(), 
                getId(), SBHView.class, 
                parent, parent.getObject().getClass(),
                node.getContexts());
         if(LOGGER.isDebugEnabled())
             LOGGER.debug("Dispatch N evt: [ {} ]", e);
        dispatcher.publish(e);
    }
     
    @Override
     public void dispatchEdgeEvent(SBEventType type, X edge) {
        SBEvent e =  new DefaultSBGraphEvent<>(
                type, SBGraphEntityType.EDGE, 
                edge, edge.getClass(), 
                getId(), SBHView.class,
                null, null,
                ((W)edge.getFrom()).getContexts());
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Dispatch E evt: {}", e);
        dispatcher.publish(e);
    }
    
    @Override
    public Set<W> nodeSet() {
        return graph.nodeSet();
    }

    @Override
    public Set<X> edgeSet() {
        return graph.getAllEdges();
    }

    @Override
    public SBHCursor<W, X> createCursor(W startNode) {
        return new DefaultSBHCursor<>(startNode, this);
    }

    
    
    private boolean isEdgeVisible(X e) {
        
        W source = getEdgeSource(e);
        W target = getEdgeTarget(e);
        
        return !source.equals(target) 
                && graph.isVisible(source) 
                && graph.isVisible(target);
    }

    @Override
    public List<X> incomingEdges(W node) {
        
//        if(!graph.isVisible(node))
//            return Collections.EMPTY_LIST;
        
        return graph.incomingEdges(node).stream()
                .filter(e -> isEdgeVisible(e))
                .collect(Collectors.toList());
    }

    @Override
    public List<X> outgoingEdges(W node) {
        return graph.outgoingEdges(node).stream()
                .filter(e -> isEdgeVisible(e))
                .collect(Collectors.toList());
    }


    @Override
    public W getEdgeSource(X edge) {
        W obj = graph.getEdgeSource(edge);
        return graph.isVisible(obj) ? obj : null;
    }

    @Override
    public W getEdgeTarget(X edge) {
        W obj = graph.getEdgeTarget(edge);
        return graph.isVisible(obj) ? obj : null;
    }
    
    @Override
    public W getOriginalEdgeSource(X edge) {
        return graph.getOriginalEdgeSource(edge);
    }

    @Override
    public W getOriginalEdgeTarget(X edge) {
        return graph.getOriginalEdgeTarget(edge);
    }

    @Override
    public void expand(W node) {
      //  if(graph.isVisible(node))
            graph.expand(node);
    }

    @Override
    public void contract(W node) {
       // if(graph.isVisible(node))
            graph.contract(node);
    }

    @Override
    public boolean isContracted(W node) {
        return graph.isContracted(node);
    }
    
    @Override
    public W getParent(W node) {
        W obj = graph.getParent(node);
        if(obj == null)
            return null;
        //return graph.isVisible(obj) ? obj : null;
        return obj;
    }


    public void setParent(W parent, W node) {
        //if(graph.isVisible(parent)) {
            
            graph.setParent(parent, node);
        //}
            
    }

    @Override
    public List<W> getAllChildren(W node) {
        return graph.getAllChildren(node);
    }

    @Override
    public List<W> getChildren(W node) {
        return graph.getChildren(node);
    }

    @Override
    public List<W> getDescendants(W node) {
       return graph.getDescendants(node);
    }

    @Override
    public List<W> getAllDescendants(W node) {
        return graph.getAllDescendants(node);
    }

    @Override
    public int getDepth(W node) {
        /*if(!graph.isVisible(node))
            return 0;*/
        return graph.getDepth(node);
    }



    @Override
    public Set<X> getAllEdges(W o1, W o2) {
        return graph.getAllEdges(o1, o2);
    }
    
    @Override
    public W getRoot() {
        return rootNode;
    }
    
    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }

    @Override
    public boolean isVisible(W node) {
        return graph.isVisible(node);
    }

    @Override
    public boolean containsEdge(W from, W to, X edge) {
        return graph.containsEdge(from, to, edge);
    }

    @Override
    public Set<X> getAllEdges() {
        return graph.getAllEdges();
    }

    @Override
    public boolean isDescendant(W node, W descendant) {
        return graph.isDescendant(node, descendant);
    }

    @Override
    public boolean hasSiblings(W node) {
        return graph.hasSiblings(node);
    }

    @Override
    public List<W> getSiblings(W node) {
        return graph.getSiblings(node);
    }

    @Override
    public boolean isAncestor(W node, W ancestor) {
        return graph.isAncestor(node, ancestor);
    }

    @Override
    public SBHIterator<W> hierarchyIterator(W instance) {
        return graph.hierarchyIterator(instance);
    }

    @Override
    public boolean containsNode(W node) {
        return graph.containsNode(node);
    }

    @Override
    public boolean containsEdge(W from, W to) {
        return graph.containsEdge(from, to);
    }

    @Override
    public X getEdge(W from, W to) {
        return graph.getEdge(from, to);
    }

    @Override
    public Set<X> getAllEdges(W node) {
        return graph.getAllEdges();
    }

    @Override
    public boolean containsEdge(X edge) {
       return graph.containsEdge(edge);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[ " + getPrettyName(rootNode) + " ] ";
    }

    @Override
    public SBHTraversalSource<W, X, ? extends SBHGraph> getTraversal() {
        return new DefaultSBHTraversalSource<>(this);
    }

    
    
    

}
