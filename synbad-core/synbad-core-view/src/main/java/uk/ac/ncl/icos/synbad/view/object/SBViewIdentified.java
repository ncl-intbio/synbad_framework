/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;

/**
 * Represents objects within an SBView. A single object can be instantiated 
 * multiple times within the same view.
 * @param <O> The type of the object instanced in the view by this view object.
 * @author owengilfellon
 */
public interface SBViewIdentified<O extends SBIdentified> extends SBIdentified, SBViewValued<O> {
    

}
