/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.pipes;

import java.util.Arrays;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBEventSource;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.graph.traversal.pipes.SBSourcePipe;
import uk.ac.ncl.icos.synbad.pipes.DefaultSBTraverser;
import uk.ac.ncl.icos.synbad.pipes.SBTraverser;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBViewPipelineSubscriberSource implements SBViewPipelineSource<SBEvent>, SBSubscriber {

    DefaultSBViewPipeline<SBEvent, SBEvent> trav = new DefaultSBViewPipeline<>();
    
    public DefaultSBViewPipelineSubscriberSource() {
        trav = trav.addPipe(new SBSourcePipe<>(trav, true));
    }

    public SBViewPipeline<SBEvent,SBEvent> get() {
        return trav;
    }
    
    @Override
    public SBViewPipeline<SBEvent,SBEvent> push(SBEvent... objs) {
        trav.asConfigurable().addInput(Arrays.stream(objs)
            .map(n -> (SBTraverser<SBEvent>)new DefaultSBTraverser<>(n)).iterator());
        return trav;
    }
    
    public SBViewPipeline<SBEvent,SBEvent> subscribe(SBEventSource source) {
        source.subscribe(new SBEventFilter.DefaultFilter(), this);
        return trav;
    }
    
    @Override
    public void onEvent(SBEvent e) {
        trav.asConfigurable().addInput(new DefaultSBTraverser<>(e));
    }
}
