/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.view.SBView;

import java.net.URI;

/**
 *
 * @author owengilfellon
 * @param <O>
 */
public class DefaultSBViewIdentified<O extends SBIdentified> extends DefaultSBViewValued<O> implements SBViewIdentified<O> {

    public DefaultSBViewIdentified(O obj, long id, SBView view) {
        super(obj, id, view);
    }

    public DefaultSBViewIdentified(O obj, SBView view) {
        super(obj, view);
    }
    
    @Override
    public String toString() {
        return getObject().getName();
    }

    @Override
    public String getPrefix() {
        return getObject().getPrefix();
    }

    @Override
    public String getDisplayId() {
        return getObject().getDisplayId();
    }

    @Override
    public String getVersion() {
        return getObject().getVersion();
    }

    @Override
    public String getName() {
        return getObject().getName();
    }

    @Override
    public URI getPersistentId() {
        return getObject().getPersistentId();
    }

    @Override
    public URI getWasDerivedFrom() {
        return getObject().getWasDerivedFrom();
    }

    @Override
    public String getDescription() {
        return getObject().getDescription();
    }
}
