/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.helpers.SBViewIdGenerator;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.event.SBEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.event.DefaultSBGraphEvent;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 */
public abstract class AView<W extends SBViewValued, X extends SBViewEdge> implements SBView<W, X> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AView.class);
    private static final long serialVersionUID = -2703578688400880434L;

    private final Map<Class<? extends SBValued>, Set<W>> instanceClazzToViewObject;
    private final Map<URI, Set<W>> instanceIdentityToViewObjects;
    private final Map<Object, Set<X>> instanceEdgeToViewEdges;
    
    private final long id;
    transient private List<SBViewUpdater> updaters;
    protected final SBDispatcher dispatcher;

    @Override
    public Long getId() {
        return id;
    }

    public AView() {
        this.instanceIdentityToViewObjects = new HashMap<>();
        this.instanceEdgeToViewEdges = new HashMap<>();
        this.instanceClazzToViewObject = new HashMap<>();
        this.id = SBViewIdGenerator.getGenerator().next();
        this.dispatcher = new DefaultSBDispatcher();
        this.updaters = new ArrayList<>();
    }

    @Override
    public void close() {
        this.dispatcher.close();
        this.updaters.stream().forEach(u -> u.close());
        this.updaters.clear();
        this.instanceEdgeToViewEdges.clear();
        this.instanceIdentityToViewObjects.clear();
        this.instanceClazzToViewObject.clear();
    }
    
    protected void registerNode(W node) {
        URI instanceId = node.getObject().getIdentity();
        
        if(!instanceIdentityToViewObjects.containsKey(instanceId))
            instanceIdentityToViewObjects.put(instanceId, new HashSet<>());
        
        instanceIdentityToViewObjects.get(instanceId).add(node);
        
        if(!instanceClazzToViewObject.containsKey(node.getObject().getClass()))
            instanceClazzToViewObject.put(node.getObject().getClass(), new HashSet<>());
        
        instanceClazzToViewObject.get(node.getObject().getClass()).add(node);
    }

    protected void deregisterNode(W node) {
        URI instanceId = node.getObject().getIdentity();

        if(instanceIdentityToViewObjects.containsKey(instanceId)) {
            instanceIdentityToViewObjects.get(instanceId).remove(node);

            if(instanceIdentityToViewObjects.get(instanceId).isEmpty())
                instanceIdentityToViewObjects.remove(instanceId);
        }

        if(instanceClazzToViewObject.containsKey(node.getObject().getClass())) {
            instanceClazzToViewObject.get(node.getObject().getClass()).remove(node);

            if(instanceClazzToViewObject.isEmpty())
                instanceClazzToViewObject.remove(node.getObject().getClass());
        }
    }
    
    public void registerEdge(X edge) {
        
        if(!instanceEdgeToViewEdges.containsKey(edge.getEdge())) {
            instanceEdgeToViewEdges.put(edge.getEdge(), new HashSet<>());
        };
        
        instanceEdgeToViewEdges.get(edge.getEdge()).add(edge);
    }

    protected void deregisterEdge(X edge) {

        if(instanceEdgeToViewEdges.containsKey(edge.getEdge())) {
            instanceEdgeToViewEdges.get(edge.getEdge()).remove(edge);

            if(instanceEdgeToViewEdges.get(edge.getEdge()).isEmpty())
                instanceEdgeToViewEdges.remove(edge.getEdge());
        }
    }

    @Override
    public Set<W> findObjects(URI identity) {
        Set<W> nodes = instanceIdentityToViewObjects.get(identity);
        return nodes == null ? new HashSet<>() : nodes;
    }

    @Override
    public <T extends SBValued> Collection<W> findObjects(Class<T> clazz) {
        return instanceClazzToViewObject.keySet().stream()
                .filter(c -> clazz.isAssignableFrom(c)).flatMap(c -> instanceClazzToViewObject.get(c).stream())
                //.map(o -> clazz.cast(o))
                .collect(Collectors.toSet());
    }
  
    @Override
    public <T extends SBValued> Collection<W> findObjects(URI identity, Class<T> clazz) {
        return instanceIdentityToViewObjects.get(identity) != null ?
                instanceClazzToViewObject.keySet().stream()
                    .filter(c -> clazz.isAssignableFrom(c))
                    .flatMap(c -> instanceClazzToViewObject.get(c).stream())
                    .filter(o -> o.getIdentity().equals(identity))
                    .filter(o -> clazz.isAssignableFrom(o.getObject().getClass()))
                    .collect(Collectors.toSet())
                : Collections.EMPTY_SET;
    }

    @Override
    public <T extends SBValued> Collection<W> findViewNodes(T instance) {
        return instanceIdentityToViewObjects.get(instance.getIdentity()) != null ?
               instanceIdentityToViewObjects.get(instance.getIdentity())
                .stream()
                .filter(n -> n.getObject() != null)
                .map(n -> (W) n).collect(Collectors.toSet())
        : Collections.EMPTY_SET;
    }

    /*
    @Override
    public Set<X> findViewEdges(E edge) {
        Set<X> edges = new HashSet<>(instanceEdgeToViewEdges.get(edge.getEdge()));
        return  edges;
    }*/

    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }
    
    public void dispatchNodeEvent(SBEventType type, W node) {
        SBEvent e =  new DefaultSBGraphEvent<>(type, SBGraphEntityType.NODE, node, node.getClass(), getId(), SBView.class, null, null, node.getContexts());
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Publish Node Event: {} ", e);
        dispatcher.publish(e);
    }
    
    public void dispatchEdgeEvent(SBEventType type, X edge) {
        SBEvent e =  new DefaultSBGraphEvent<>(type, SBGraphEntityType.EDGE, edge, edge.getClass(), getId(), SBView.class, null, null, ((W)edge.getFrom()).getContexts());
        LOGGER.trace("Publish Edge Event: {}", e);
        dispatcher.publish(e);
    }

    @Override
    public void addUpdater(SBViewUpdater updater) {
        // Serialization fudge
        if(updaters == null)
            updaters = new ArrayList<>();
        LOGGER.debug("Added updater: {}", updater.getClass().getSimpleName());
        this.updaters.add(updater);
        //this.subscribe(new SBEventFilter.DefaultFilter(), updater);
    }

    @Override
    public void removeUpdater(SBViewUpdater updater) {
        LOGGER.debug("Removed updater: {}", updater.getClass().getSimpleName());
        this.updaters.remove(updater);
       // this.unsubscribe(updater);
    }
    
    @Override
    public SBCursor<W, X> createCursor(W startNode) {
         return new DefaultSBCursor<>(startNode, this);
    }
    
    @Override
    public String toString() {
        return "DefaultSBView[ " + id +  " ]";
    }
}
