/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.SBView;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBViewEdge<N extends SBViewValued<O>, O extends SBValued, E extends SBEdge> extends ASBViewEntity<E> implements SBViewEdge<N, O, E> {
    
    private static final long serialVersionUID = 7243066494066019759L;
    private final E edge;
    private final N from;
    private final N to;

    public  DefaultSBViewEdge(E edge, N from, N to, SBView view) {
        super(view);
        this.edge = edge;
        this.from = from;
        this.to = to;
    }
    
    public DefaultSBViewEdge(E edge, N from, N to, long id, SBView view) {
        super(view, id);
        this.edge = edge;
        this.from = from;
        this.to = to;
    }
    
    @Override
    public String toString() {
        return edge.getEdge().toString();
    }

    @Override
    public N getFrom() {
        return from;
    }

    @Override
    public N getTo() {
        return to;
    }

    @Override
    public E getEdge() {
        return edge;
    }
    
  
}
