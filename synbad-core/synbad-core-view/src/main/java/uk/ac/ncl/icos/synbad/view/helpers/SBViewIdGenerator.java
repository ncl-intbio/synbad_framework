/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.helpers;

import java.util.Iterator;


/**
 *
 * @author owengilfellon
 */
public class SBViewIdGenerator implements Iterator<Long> {
    
    private static SBViewIdGenerator generator;
    private long currentId;
    
    private SBViewIdGenerator() {
        currentId = 0;
    }
    
    public static SBViewIdGenerator getGenerator() {
        if(generator == null)
            generator = new SBViewIdGenerator();
        
        return generator;
    }
    
    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Long next() {
        return currentId++;
    }
}
