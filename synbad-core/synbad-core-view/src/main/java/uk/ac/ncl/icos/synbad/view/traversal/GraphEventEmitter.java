/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawlerObserver;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public class GraphEventEmitter<N extends SBValued, E extends SBEdge, C extends SBCursor<N, E>> implements SBCrawlerObserver<N,E,C> {

    String s = "";
    

    @Override
    public void onEdge(E edge) {
        System.out.println(s + "   " + edge.toString());
    }

    @Override
    public void onEnded() {

    };

    @Override
    public void onInstance(N instance, C cursor) {
        System.out.println(instance.toString());
    }

    @Override
    public boolean terminate() { 
        return false; }

    @Override
    public boolean vetoEdge(E edge) {
       return false;
    }

}
