/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.view.SBView;

import java.io.Serializable;
import uk.ac.ncl.icos.synbad.api.domain.SBEntity;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 * Represents objects and edges instanced within an SBView. A single object or
 * edge can be instantiated multiple times within the same view. This is a common
 * interface that is extended by SBViewValued, SBViewPort and SBViewEdge, and
 * represents all instanced entities.
 * @param <T> The type the object instanced by this entity.
 * @author owengilfellon
 */
public interface SBViewEntity<T> extends SBEntity, Serializable {

    SBWorkspace getWorkspace();

    /**
     * 
     * @param <V> The type of the view.
     * @return The view to which this view entity belongs.
     */
    public <V extends SBView> V getView();
    
    /**
     * 
     * @return The identifier for the view entity, which is unique to all
     * entities belonging to the same view.
     */
    public long getId();
    
    
}
