/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.graph.WritableHGraph;
import uk.ac.ncl.icos.synbad.view.SBHView;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 * Wraps around a graph instance, allowing read only access to the graph...
 * @author owengilfellon
 */
public interface SBHWritableView<W extends SBViewValued, X extends SBViewEdge> extends SBHView<W, X>, WritableHGraph<W, X> {

}
