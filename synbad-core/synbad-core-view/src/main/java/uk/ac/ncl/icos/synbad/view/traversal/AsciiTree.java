/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.traversal;

import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawlerObserver;
import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;
import uk.ac.ncl.icos.synbad.view.SBHView;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 */
public class AsciiTree<C extends SBHCursor<SBViewValued<SBValued>, SBViewEdge<SBViewValued<SBValued>, SBValued, SBEdge>>> extends SBCrawlerObserver.SBCrawlerObserverAdapter<SBViewValued<SBValued>, SBViewEdge<SBViewValued<SBValued>, SBValued, SBEdge>, C> {
         

    StringBuilder sb = new StringBuilder();

    @Override
    public void onInstance(SBViewValued<SBValued> instance, C cursor) {
        super.onInstance(instance, cursor); //To change body of generated methods, choose Tools | Templates.

        for (int i = 0; i< cursor.getDepth(); i++) {
          sb.append("   ");
        }

        sb.append(!((SBHView)instance.getView()).isContracted(instance) ? "+ " : "- ");
        if(SBIdentified.class.isAssignableFrom(instance.getObject().getClass())) {
           sb.append(!((SBIdentified)instance.getObject()).getName().isEmpty() ? ((SBIdentified)instance.getObject()).getName() : ((SBIdentified)instance.getObject()).getDisplayId());

        }
        sb.append(" ")
        .append(cursor.getEdges(SBDirection.IN).size()).append(", ")
        .append(cursor.getEdges(SBDirection.OUT).size());
        
        if(cursor.getParent() != null) {
            sb.append(", P: ").append(cursor.getParent());
        }
                
        sb.append(", C: ").append(cursor.getChildren().size()).append("\n");
        
        /*
        for(Object predicate : instance.getPredicates()) {

            for(Object v : instance.getValues(predicate.toString())) {
                for (int i = 0; i< cursor.getDepth() +1; i++) {
                    sb.append("   ");
                }
                sb.append(predicate).append(": ").append(v).append("\n");
            }
        }*/
        
       // sb.append("\n");
    }

    @Override
    public  void onEdge(SBViewEdge edge) {
        super.onEdge(edge);
    }

    @Override
    public void onEnded() {
        System.out.println(sb.toString());
    }     /* */  

}