/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.model;

import uk.ac.ncl.icos.synbad.view.SBViewManager;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.util.Collection;
import java.util.List;

import org.openide.util.Lookup;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.helpers.SBViewIdGenerator;
import uk.ac.ncl.icos.synbad.datadefinition.UriHelper;
import uk.ac.ncl.icos.synbad.event.DefaultSBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import uk.ac.ncl.icos.synbad.api.event.SBEventFilter;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.event.SBSubscriber;
import uk.ac.ncl.icos.synbad.view.SBView;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * @author owengilfellon
 */
public abstract class ASBModel
        <N extends SBValued,
        V extends SBView> implements SBModelWithView<N, V> {

    private static final long serialVersionUID = -206878057936024383L;
    transient private  SBDispatcher dispatcher;
    transient protected  SBWorkspace ws;

    private final URI workspaceIdentity;
    private final N rootNode;
    private final long id;
    private final SBViewManager<V> viewManager;

    public ASBModel(SBWorkspace workspace, N rootNode) {
        this(workspace, rootNode, UriHelper.synbad);
    }

    @Override
    public SBWorkspace getWorkspace() {
        return ws;
    }
    
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void close() {
        dispatcher.getListeners().stream().forEach(dispatcher::unsubscribe);
        viewManager.close();
    }

    private void writeObject(ObjectOutputStream out) throws IOException
    {
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException
    {
        try {
            in.defaultReadObject();
            this.ws = Lookup.getDefault().lookup(SBWorkspaceManager.class).getWorkspace(workspaceIdentity);
            this.dispatcher = new DefaultSBDispatcher();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ASBModel(SBWorkspace workspace, N rootNode, NamespaceBinding namespace) {
        assert(workspace != null);
        this.workspaceIdentity = workspace.getIdentity();
        this.id = SBViewIdGenerator.getGenerator().next();
        this.ws = workspace;
        this.rootNode = rootNode;
        this.dispatcher = new DefaultSBDispatcher();
        this.viewManager = new SBViewManager<>(constructView());
    }
    
    @Override
    public <T> void subscribe(SBEventFilter filter, SBSubscriber handler) {
        dispatcher.subscribe(filter, handler);
    }

    @Override
    public <T> void unsubscribe(SBSubscriber subscriber) {
        dispatcher.unsubscribe(subscriber);
    }

    @Override
    public List<SBSubscriber> getListeners() {
        return dispatcher.getListeners();
    }
    
    @Override
    public N getRootNode() {
        return rootNode;
    }
    
    @Override
    public V getMainView() {
         return viewManager.getMainView();
    }

    @Override
    public V getCurrentView() {
        return viewManager.getCurrentView();
    }
    
    protected abstract V constructView();

    @Override
    public V createView() {
        V view = constructView();
        viewManager.addView(view);
        return view;
    }
    
    @Override
    public boolean setCurrentView(V view) {
        return viewManager.setCurrentView(view);
    }

    @Override
    public Collection<V> getViews() {
        return viewManager.getViews();
    }

    @Override
    public boolean removeView(V view) {
        return viewManager.removeView(view);
    }

} 
