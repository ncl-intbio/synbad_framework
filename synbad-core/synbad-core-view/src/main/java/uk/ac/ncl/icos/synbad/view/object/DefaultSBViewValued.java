/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import java.net.URI;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.view.SBView;

/**
 * Wraps an SBObject. Equality is derived from the wrapped object, and the view
 * object's id, allowing multiple instances of an SBObject to be included in
 * a view.
 * @author owengilfellon
 */
public class DefaultSBViewValued<O extends SBValued> extends ASBViewEntity<O> implements SBValued, SBViewValued<O> {

    private final O obj;

    public DefaultSBViewValued(O obj, long id, SBView view) {
        super(view, id);
        this.obj = obj;
    }

    public DefaultSBViewValued(O obj, SBView view) {
        super(view);
        this.obj = obj;
    }
 
    @Override
    public O getObject() {
        return obj;
    }
    
    @Override
    public boolean isClass(Class<? extends SBValued> clazz) {
        return clazz.isAssignableFrom(getObject().getClass());
    }

    @Override
    public <T extends SBViewValued> T getAsSubclass(Class<T> clazz) {
        return clazz.isAssignableFrom(getClass()) ? clazz.cast(this) : null;
    }

    @Override
    public <T extends O> SBViewValued<T> asViewObj(Class<T> clazz) {
        return isClass(clazz) ? (SBViewValued<T>) this : null;
    }
    
    @Override
    public <T extends SBValued> Optional<T> as(Class<T> clazz) {
        return obj.as(clazz);
    }

    @Override
    public URI getType() {
        return getObject().getType();
    }

    @Override
    public SBValue getValue(String predicate) {
        return getObject().getValue(predicate);
    }

    @Override
    public Collection<SBValue> getValues(String predicate) {
        return getObject().getValues(predicate);
    }

    @Override
    public <T> Collection<T> getValues(String predicate, Class<T> clazz) {
        return getObject().getValues(predicate, clazz);
    }

    @Override
    public Set<String> getPredicates() {
        return getObject().getPredicates();
    }

    @Override
    public URI getIdentity() {
        return getObject().getIdentity();
    }

    @Override
    public SBWorkspace getWorkspace() {
        return getObject().getWorkspace();
    }

    @Override
    public URI[] getContexts() {
        return getObject().getContexts();
    }

    @Override
    public boolean is(String rdfType) {
        return getObject().is(rdfType);
    }

    
    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(DefaultSBViewValued.class.isAssignableFrom(obj.getClass())))
            return false;

        DefaultSBViewValued node = (DefaultSBViewValued) obj;
        return node.getId() == getId() && node.getObject().equals(getObject());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.obj);
        hash = 53 * hash + Objects.hashCode(getId());
        return hash;
    }

    
}