/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.model.SBModel;

/**
 *
 * @author owengilfellon
 */
public interface SBViewFactory<M extends SBModel, N extends SBIdentified, V extends SBView> {
    
    V getView(N rootNode, M model);
    
    Class<M> getModelClass();
    
    Class<N> getRootNodeClass();
    
    Class<V> getViewClass();
}
