/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.graph.SBDirection;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBGraphTraversal;
import uk.ac.ncl.icos.synbad.tree.ITree;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversal;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;

/**
 *
 * @author owengilfellon
 */
public abstract class AViewUpdater<T extends SBGraph, V extends SBView>  implements SBViewUpdater<T, V> {

    protected final T source;
    protected final V target;
    protected final SBValued root;

    public AViewUpdater(T source, V target, SBValued root) {
        this.root = root;
        this.source = source;
        this.target = target;
    }

    @Override
    public T getSourceGraph() {
        return source;
    }

    @Override
    public V getTargetGraph() {
        return target;
    }
    
    protected ITree<Object> getTreeFromNode(SBValued identified) {

        SBGraphTraversal<SBValued, SBValued, T> t = (( SBGraphTraversalSource<SBValued, SBEdge, T>)source.getTraversal()).v(identified);

        // Follow out edges that have specified predicates, skipping duplicates

        t = t.loop(o -> true, new DefaultSBGraphTraversal<SBValued, SBEdge, T>(source)
                    .e(SBDirection.OUT, getHierarchyPredicates().toArray(new String[]{}))
                    .v(SBDirection.OUT), true, false);

        return (ITree<Object>)t.getResult().getTree();
    }

}
