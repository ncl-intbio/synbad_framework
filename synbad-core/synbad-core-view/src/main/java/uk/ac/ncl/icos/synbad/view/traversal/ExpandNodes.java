/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.traversal;

import uk.ac.ncl.icos.synbad.graph.traversal.SBCrawlerObserver;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.DefaultSBViewIdentified;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBHCursor;

/**
 *
 * @author owengilfellon
 */
public class ExpandNodes extends SBCrawlerObserver.SBCrawlerObserverAdapter<DefaultSBViewIdentified, DefaultSBViewEdge, SBHCursor<DefaultSBViewIdentified, DefaultSBViewEdge>> {

    @Override
    public void onInstance(DefaultSBViewIdentified instance, SBHCursor<DefaultSBViewIdentified, DefaultSBViewEdge> cursor) {
        super.onInstance(instance, cursor); //To change body of generated methods, choose Tools | Templates.
        cursor.expand(); 
    }
    
}
