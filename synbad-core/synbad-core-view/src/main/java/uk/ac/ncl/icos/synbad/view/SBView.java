/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.io.Serializable;
import java.net.URI;
import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

import uk.ac.ncl.icos.synbad.model.SBModel;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.graph.SBGraph;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 * Wraps around a graph instance, allowing read only access to the graph...
 * @author owengilfellon
 */
public interface SBView<W extends SBViewValued, X extends SBViewEdge> extends SBGraph<W, X>, Serializable {

    Long getId();
    
    void close();
    
    <M extends SBModel> M getModel();
    
    void addUpdater(SBViewUpdater updater);
    
    void removeUpdater(SBViewUpdater updater);

    /**
     * Finds all instances that have the specified identity
     * @param identity
     * @return 
     */
    Set<W> findObjects(URI identity);
    
    /**
     * Find all instances with the specified identity that wrap an object
     * of the provided class
     * @param <T>
     * @param identity
     * @param clazz
     * @return 
     */
    <T extends SBValued> Collection<W> findObjects(URI identity, Class<T> clazz);

    /**
     * Find all instances that wrap objects of the provided class
     * @param <T>
     * @param clazz
     * @return 
     */
    <T extends SBValued> Collection<W> findObjects(Class<T> clazz);

    /**
     * Find all instances of the provided object
     * @param <T>
     * @param instance
     * @return 
     */
    <T extends SBValued> Collection<W> findViewNodes(T instance);

}
