/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBCursor;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEventType;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBCursor;
import uk.ac.ncl.icos.synbad.graph.traversal.impl.DefaultSBGraphTraversalSource;
import uk.ac.ncl.icos.synbad.model.DefaultSBModel;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewIdentified;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceGraph;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.graph.traversal.SBGraphTraversalSource;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBView<W extends SBViewValued, X extends SBViewEdge> extends AFlatView<W, X> {
    
    private final Logger logger = LoggerFactory.getLogger(DefaultSBView.class);
    private final List<SBViewUpdater<SBWorkspaceGraph,
            SBView<
            SBViewIdentified<SBIdentified>, 
            SBViewEdge<SBViewIdentified<SBIdentified>, SBIdentified,  SynBadEdge>>>> updaters;

    private final SBWorkspaceGraph g;
    private final DefaultSBModel m;
    
    public DefaultSBView(SBIdentified rootNode, DefaultSBModel model) {
        g = new SBWorkspaceGraph(model.getWorkspace(), rootNode.getContexts());
        this.m = model;
        this.updaters = new ArrayList<>();
       // SbolHierarchyUpdater u = new SbolHierarchyUpdater(g, this, rootNode);
       // g.subscribe(new SBEventFilter.DefaultFilter(), u);
      //  addUpdater(u);
    }
    
    @Override
    public DefaultSBModel getModel() {
        return m;
    }
    
    @Override
    public boolean addNode(W node) {
        if(!super.addNode(node))
            return false;
        dispatchNodeEvent(SBGraphEventType.ADDED, node);
        return true;
    }

    @Override
    public boolean addEdge(W sourceNode, W targetNode, X edge) {
        if(!super.addEdge(sourceNode, targetNode, edge))
            return false;
        dispatchEdgeEvent(SBGraphEventType.ADDED, edge);
        return true;
    }

    @Override
    public boolean removeEdge(X edge) {
        if(super.removeEdge(edge))
            return false;
        dispatchEdgeEvent(SBGraphEventType.REMOVED, edge);
        return true;
    }

    @Override
    public X removeEdge(W sourceNode, W targetNode) {
        X e = getEdge(sourceNode, targetNode);
        if(!super.removeEdge(e))
            return null;
        
        dispatchEdgeEvent(SBGraphEventType.REMOVED, e);
        
        return e;
        
    }

    @Override
    public boolean removeNode(W node) {
        if(!super.removeNode(node))
            return false;
        dispatchNodeEvent(SBGraphEventType.ADDED, node);
        return true;
    }

    
    @Override
    public SBCursor<W, X> createCursor(W startNode) {
         return new DefaultSBCursor<>(startNode, this);
    }
    
    @Override
    public String toString() {
        return "DefaultSBView[ N: " + nodeSet().size() + " E: " + edgeSet().size() + " ]";
    }

    @Override
    public SBGraphTraversalSource<W, X, SBView<W, X>> getTraversal() {
         return new DefaultSBGraphTraversalSource<>(this);
    }
}
