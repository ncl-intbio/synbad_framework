/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.model;

import java.util.Collection;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owen
 */
public interface SBModelManager {

    <R extends SBValued> Collection<SBModelFactory<? extends SBModel, R>> getProviders(Class<R> rootNode);

    <T extends SBModel, V extends SBValued> T createModel(Class<T> modelClazz, V rootNode);

}
