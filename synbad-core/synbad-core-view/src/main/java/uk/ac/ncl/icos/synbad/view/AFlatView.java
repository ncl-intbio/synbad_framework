/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.graph.SBGlobalEdgeProvider;
import uk.ac.ncl.icos.synbad.graph.SBGlobalNodeProvider;
import uk.ac.ncl.icos.synbad.graph.impl.DefaultSBGraph;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 *
 * @author owengilfellon
 */
public abstract class AFlatView< W extends SBViewValued, X extends SBViewEdge> extends AView<W, X> implements SBWritableView<W, X>, SBGlobalEdgeProvider<W, X>, SBGlobalNodeProvider<W> {

    private static final Logger logger = LoggerFactory.getLogger(AFlatView.class);
    private final DefaultSBGraph<W, X> graph = new DefaultSBGraph<>();
    
    @Override
    public boolean addNode(W node) {
        
        logger.debug("Adding node " + node);
        
        boolean b = graph.addNode(node);
        if(!b) return b;
        
        registerNode(node);
        
        //dispatchNodeEvent(DefaultEventType.ADDED, node);
        
        return b;
    }
   
    @Override
    public boolean removeNode(W node) {
        
        logger.debug("Removing node " + node);
        
        boolean b = graph.removeNode(node); //To change body of generated methods, choose Tools | Templates.
        if(!b) return b;
        
        // THis wasn't here -> is there a reason?
        //deregisterNode(node);

        return b;
    }

    @Override
    public boolean addEdge(W sourceNode, W targetNode, X edge) {
        
        logger.debug("Adding edge " + edge);
        
        boolean b = graph.addEdge(sourceNode, targetNode, edge); //To change body of generated methods, choose Tools | Templates.
        if(!b) return b;
        
        registerEdge(edge);

        return b;
    }

    @Override
    public boolean removeEdge(X edge) {
        
        logger.debug("Removing edge " + edge);
        
        boolean b = graph.removeEdge(edge); //To change body of generated methods, choose Tools | Templates.
        if(!b) return b;
        
        deregisterEdge(edge);

        return b;
    }

    @Override
    public boolean removeAllEdges(Collection<? extends X> edges) {
        
        boolean b = true;
        
        for(X edge : edges) {
            if(!removeEdge(edge))
                b = false;
        }
        
        return b;
    }

    @Override
    public Set<X> removeAllEdges(W sourceNode, W targetNode) {
        Set<X> edges = new HashSet<>() ;
        for(X e: getAllEdges(sourceNode, targetNode)) {
            if(removeEdge(e))
                edges.add(e);
        }
        
        return edges;
    }

    @Override
    public boolean removeAllNodes(Collection<? extends W> nodes) {
        
        boolean b = true;
        
        for(W o : nodes) {
            if(!removeNode(o))
                b = false;
        }
        
        return b;
    }

    @Override
    public X removeEdge(W sourceNode, W targetNode) {
        X edge = graph.removeEdge(sourceNode, targetNode);
        logger.debug("Removing edge " + edge);
        if(edge!=null) {
            deregisterEdge(edge);
        }
        
        return edge;
    }

    

    @Override
    public boolean containsEdge(W from, W to) {
        return graph.containsEdge(from, to);
    }

    @Override
    public X getEdge(W from, W to) {
        return graph.getEdge(from, to);
    }

    @Override
    public Set<X> getAllEdges(W node) {
        return edgeSet();
    }

    @Override
    public boolean containsEdge(X edge) {
        return graph.containsEdge(edge);
    }

    @Override
    public Set<W> nodeSet() {
        return graph.nodeSet();
    }

    @Override
    public boolean containsNode(W node) {
        return graph.containsNode(node);
    }

    @Override
    public Set<X> getAllEdges(W sourceNode, W targetNode) {
        return graph.getAllEdges(sourceNode, targetNode);
    }

    @Override
    public Set<X> edgeSet() {
        return graph.edgeSet();
    }

    @Override
    public W getEdgeSource(X edge) {
        return graph.getEdgeSource(edge);
    }

    @Override
    public W getEdgeTarget(X edge) {
        return graph.getEdgeTarget(edge);
    }

    @Override
    public List<X> incomingEdges(W node) {
        return graph.incomingEdges(node);
    }

    @Override
    public List<X> outgoingEdges(W node) {
        return graph.outgoingEdges(node);
    }
//
//    @Override
//    public SBTraversalSource<W, X, SBView<W, X>> getTraversal() {
//        return new GraphTraversalSource<>(this);
//    }
    
    
}
