/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.model.DefaultSBModel;
import org.openide.util.lookup.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;

/**
 *
 * @author owengilfellon
 */
@ServiceProvider(service = SBViewFactory.class)
public class DefaultSBViewFactory implements SBViewFactory<DefaultSBModel, SBIdentified, DefaultSBView>{

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBViewFactory.class);
    
    @Override
    public DefaultSBView getView(SBIdentified rootNode, DefaultSBModel model) {  
      //  SBViewIdentified obj = new DefaultSBViewIdentified(rootNode, view);
        DefaultSBView view = new DefaultSBView(rootNode, model);
        LOGGER.debug("Constructed view: {}", view.getClass().getSimpleName());
        return view;
    }

    @Override
    public Class<DefaultSBModel> getModelClass() {
        return DefaultSBModel.class;
    }

    @Override
    public Class<SBIdentified> getRootNodeClass() {
        return SBIdentified.class;
    }

    @Override
    public Class<DefaultSBView> getViewClass() {
        return DefaultSBView.class;
    }  
}
