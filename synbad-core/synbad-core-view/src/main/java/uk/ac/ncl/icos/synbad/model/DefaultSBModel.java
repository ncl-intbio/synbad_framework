/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.view.DefaultSBView;
import uk.ac.ncl.icos.synbad.view.DefaultSBViewFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.intbio.core.datatree.NamespaceBinding;

/**
 *
 * @author owengilfellon
 */
public class DefaultSBModel extends ASBModel<SBIdentified, DefaultSBView>  {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSBModel.class);
    
    public DefaultSBModel(SBWorkspace workspace, SBIdentified rootNode) {
        super(workspace, rootNode);
    }

    public DefaultSBModel(SBWorkspace workspace, SBIdentified rootNode, NamespaceBinding namespace) {
        super(workspace, rootNode, namespace);
    }

    @Override
    protected DefaultSBView constructView() {
        return new DefaultSBViewFactory().getView(getRootNode(), this);
    }


}