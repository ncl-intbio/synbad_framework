/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.model;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owengilfellon
 */
public interface SBModelFactory<M extends SBModel, N extends SBValued> {
    
    M getModel(N rootNode);
    
    Class<M> getModelClass();
    
    Class<N> getRootNodeClass();
    
}
