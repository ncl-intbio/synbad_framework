/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.object.SBViewEdge;
import uk.ac.ncl.icos.synbad.view.object.SBViewPort;
import uk.ac.ncl.icos.synbad.graph.traversal.cursor.SBPCursor;
import uk.ac.ncl.icos.synbad.graph.SBPGraph;
import uk.ac.ncl.icos.synbad.view.object.SBViewValued;

/**
 * 
 * @author owengilfellon
 */
public interface SBPView<W extends SBViewValued, X extends SBViewEdge, Y extends SBViewPort> 
            extends SBHView<W, X>, SBPGraph<W, Y, X> {
    
    public <P extends SBValued> Set<Y> findPortNodes(P port);    

    @Override
    public SBPCursor<W, Y, X> createCursor(W startNode);
    
}
