/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.event.*;
import uk.ac.ncl.icos.synbad.datadefinition.SynBadTerms;
import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;
import uk.ac.ncl.icos.synbad.domain.SBPredicateValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.event.pipes.DefaultSBEventPipeline;
import uk.ac.ncl.icos.synbad.event.pipes.DefaultSBEventPipelineSource;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.actions.SBCommandManager;
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;

import java.net.URI;
import java.util.*;

/**
 * TODO: Apply actions to a model / view
 */
public class SBModelCommander implements SBCommandManager {

    private final static Logger logger = LoggerFactory.getLogger(SBModelCommander.class);
    private final SBDispatcher eventService;
    private final List<SBAction> actions;
    private int currentIndex;
    private SBWritableView model;

    private SBObjectFactoryManager factory;

    public SBModelCommander(SBWritableView model, SBDispatcher dispatcher) {
        this.actions = new LinkedList<>();
        this.currentIndex = -1;
        this.model = model;
        this.eventService = dispatcher;
    }

    @Override
    public synchronized void perform(SBAction action) {

        // If there are re-dos, clear them

        if(hasRedo()) {
            logger.debug("Clearing redos");
            Iterator<SBAction> i = actions.listIterator(currentIndex + 1);
            while(i.hasNext()) {
                i.next();
                i.remove();
            }
        }

        actions.add(action);
        logger.trace("Performed action [{}]: {}", currentIndex, action.getClass().getSimpleName());
        SBAction toPerform = actions.get(++currentIndex);
        Collection<SBEvent> events = toPerform.perform(this);
        eventService.publish(events);
    }

    @Override
    public synchronized void perform(Collection<SBEvent> events) {
        new DefaultSBEventPipelineSource<>()
            .push(events.toArray(new SBEvent[]{}))
            .doTraversal(new DefaultSBEventPipeline<SBEvent, SBEvent>()
                .doSideEffect(getFilter(SBGraphEntityType.NODE), this::processObject)
                .doSideEffect(getFilter(SBGraphEntityType.EDGE), this::processEdge)
                .doSideEffect(getFilter(SBGraphEntityType.VALUE), this::processValue)
                .doSideEffect(getFilter(SBGraphEntityType.PORT), this::processObject))
            .forEachRemaining(e -> {});
    }

    private void processObject(SBEvent e) {
        
        if(!SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            logger.error("No Contexts: {}", e);
            return;
        }
        
        SBGraphEvent<URI> event = (SBGraphEvent<URI>) e;
        URI[] contexts = ((SBEvtWithContext)event).getContexts();
        
        if(factory.getFactoryFromClass(event.getDataClass()) == null)
            return;

        URI type = factory.getTypeFromClass(event.getDataClass());

        SBStatement s = new SBStatement(
                event.getData().toASCIIString(), 
                SynBadTerms.Rdf.hasType,
                SBValue.parseValue(type));

        if (e.getType() == SBGraphEventType.ADDED) {
            //model.addNode(new SB)
        }
        else if (e.getType() == SBGraphEventType.REMOVED) {

        }
    }
    
    private void processEdge(SBEvent e) {
        
        if(!SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            logger.error("No Contexts: {}", e);
            return;
        }
        
        SBGraphEvent<SynBadEdge> event = (SBGraphEvent<SynBadEdge>) e;
        
        SBStatement s = new SBStatement(
                event.getData().getFrom().toASCIIString(),
                event.getData().getEdge().toASCIIString(), 
                SBValue.parseValue(event.getData().getTo().toASCIIString()));
        URI[] contexts = ((SBEvtWithContext)event).getContexts();

        if (e.getType() == SBGraphEventType.ADDED) {

        } else if (e.getType() == SBGraphEventType.REMOVED) {

        }
    }
    
    private void processValue(SBEvent e) {
        if(!SBEvtWithContext.class.isAssignableFrom(e.getClass())) {
            logger.error("No Contexts: {}", e);
            return;
        }
        
        SBGraphEvent<SBPredicateValue> event = (SBGraphEvent<SBPredicateValue>) e;
        SBPredicateValue value = event.getData();
        
        URI parent = (URI)((SBEvtWithParent)e).getParent();
        URI[] contexts = ((SBEvtWithContext)event).getContexts();
        SBStatement s = new SBStatement(
                parent.toASCIIString(), 
                value.getPredicate().toASCIIString(),
                value.getValue());

        if(e.getType() == SBGraphEventType.ADDED) {

        } else if(e.getType() == SBGraphEventType.REMOVED) {

        }
    }
 

    @Override
    public boolean hasUndo() {
        return currentIndex >= 0;
    }

    @Override
    public boolean hasRedo() {
        return currentIndex < actions.size()-1;
    }

    @Override
    public synchronized void undo() {
        if(hasUndo()) {
            SBAction a = actions.get(currentIndex--);
            logger.debug("Undoing action {}: {} ", currentIndex + 1, a.getClass().getSimpleName());
            perform(a.getEvents(true));
        }
    }

    @Override
    public synchronized void redo() {
        if(hasRedo()) {
            SBAction a = actions.get(++currentIndex);
            logger.debug("Redoing action {}: {} ", currentIndex, a.getClass().getSimpleName());
            perform(a.getEvents(false));
        }
    }

    private SBEventFilter getFilter(SBGraphEntityType entityType) {
        return (SBEvent event) -> SBGraphEvent.class.isAssignableFrom(event.getClass()) && ((SBGraphEvent)event).getGraphEntityType() == entityType;
    }

}
