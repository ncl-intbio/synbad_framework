package uk.ac.ncl.icos.synbad.model;

import java.util.Collection;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.view.SBView;

/**
 *
 * Represents a model defined by a root object and related objects as defined by
 * a view. By default, the main view is used, although the view used can be specified.
 * 
 * @author owengilfellon
 */
public interface SBModelWithView<N extends SBValued, V extends SBView> extends SBModel<N> {

    /**
     * Returns the main view. The main view is a view which is created with the
     * model, and can not be removed.
     * @return 
     */
    V getMainView();
    
     /**
     * Returns the current view. By default, the main view is used as the current
     * view. The current view can be set to another view created by the model.
     * @return 
     */
     V getCurrentView();
    
    /**
     * Sets the current view to another view created by the model.
     * @param view A View created by the model.
     * @return 
     */
    boolean setCurrentView(V view);
    
    /**
     * Create a new, independent view.
     * @return 
     */
    V createView();
    
    /**
     * Returns a collection of all views created by the model, excluding the
     * main view.
     * @return 
     */
    Collection<V> getViews();
 
    /**
     * Removed a view created by the model. If the current view is removed, then
     * the current view is set to the main view. The main view cannot be removed.
     * @param view
     * @return 
     */
    boolean removeView(V view);
    
}