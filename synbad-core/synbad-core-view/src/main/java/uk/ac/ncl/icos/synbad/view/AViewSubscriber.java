/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view;

import uk.ac.ncl.icos.synbad.api.event.SBEvent;
import uk.ac.ncl.icos.synbad.api.event.SBGraphEvent;

/**
 *
 * @author owengilfellon
 */
public abstract class AViewSubscriber implements SBViewSubscriber {

    @Override
    public void onEvent(SBEvent e) {
        
        if(!SBGraphEvent.class.isAssignableFrom(e.getClass()))
            return;
        
        SBGraphEvent evt = (SBGraphEvent) e;
        
        if(null != evt.getGraphEntityType())
            switch (evt.getGraphEntityType()) {
            case EDGE:
                onEdgeEvent(evt);
                break;
            case NODE:
                onNodeEvent(evt);
                break;
            case VALUE:
                onValueEvent(evt);
                break;
            default:
                break;
        }
    }

}
