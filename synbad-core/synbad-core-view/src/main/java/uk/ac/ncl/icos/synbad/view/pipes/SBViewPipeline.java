/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.pipes;

import uk.ac.ncl.icos.synbad.pipes.SBTraversalPipe;
import uk.ac.ncl.icos.synbad.pipes.SBTraversal;
import java.util.Iterator;


/**
 * An SBTraversal represents a whole traversal. Vertexes Can be retrieved by iteration, or
 * by specialist collection methods.
 * 
 * Each traversal has an input traversal - which may supply 1-* vertexes. 
 * 
 * Calling methods - for example, filter, adds a Step, which itself inherits traversal,
 * and returns itself allowing chaining of methods together.
 * 
 * @author owengilfellon
 */
public interface SBViewPipeline<T, V> extends SBTraversal<T, V>, Iterator<V> {
    
    @Override
    public SBViewPipeline<T, V> copy();
   
  
    // ==============================
    //        Configuration
    // ==============================
    
    // Use of a extended interface for concealing construction methods inspired
    // by TinkerPops traversal code. 
    
    @Override
    public SBViewPipeline.Configurable<T, V> asConfigurable();

    public interface Configurable<T, V> extends SBTraversal.Configurable<T, V>, SBViewPipeline<T, V> {


        @Override
        public <E> SBViewPipeline<?, E> addPipe(SBTraversalPipe<?, E> step);

     }
    
}
