/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.helpers;

import java.util.Iterator;


/**
 *
 * @author owengilfellon
 */
public class SBViewEntityIdGenerator implements Iterator<Long> {
    
    private static SBViewEntityIdGenerator generator;
    private long currentId;
    
    private SBViewEntityIdGenerator() {
        currentId = 0;
    }
    
    public static SBViewEntityIdGenerator getGenerator() {
        if(generator == null)
            generator = new SBViewEntityIdGenerator();
        
        return generator;
    }
    
    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Long next() {
        return currentId++;
    }
}
