/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.view.object;

import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 * Represents objects within an SBView that are instanced by a different object
 * within the SBOL data model.
 * @param <O> The type of the object instanced in the view.
 * @param <N> The type of the object that instances the object in the SBOL data model.
 * @author owengilfellon
 */
public interface SBViewInstanced<O extends SBValued, N extends SBValued> extends SBValued, SBViewEntity<O> {

    N getInstancedBy();
    
}
