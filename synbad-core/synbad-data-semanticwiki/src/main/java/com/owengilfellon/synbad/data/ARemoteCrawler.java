/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data;

import com.owengilfellon.synbad.data.semanticwiki.obj.Subject;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owen
 * @param <T> The type of identifier
 */
public abstract class ARemoteCrawler<T> implements SBRemoteCrawler<T> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ARemoteCrawler.class);
    protected SBWorkspace workspace;
    private Collection<T> parsedIds = new HashSet<>();

    @Override
    public void setWorkspace(SBWorkspace workspace) {
        if(this.workspace != null) {
            LOGGER.warn("Workspace is already set in {}: {}", getClass().getSimpleName(), this.workspace.getIdentity());
            return;
        }

        this.workspace = workspace;
    }

    @Override
    public SBWorkspace getWorkspace() {
        return this.workspace == null ? null : this.workspace;
    }

    @Override
    public void close() {
        this.workspace = null;
        this.parsedIds.clear();
    }

    protected Collection<T> getParsedIds() {
        return parsedIds;
    }
    
    @Override
    public List<? extends SBValued> crawl(T startingUri, int maxCount, URI... contexts) {
        
        LOGGER.trace("Crawling {} subjects from {}", maxCount, startingUri);
        
        Queue<T> queue = new LinkedList<>(Arrays.asList(startingUri));
        int count = 0;
        List<SBValued> subjects = new LinkedList<>();
        
        while(!queue.isEmpty() && count < maxCount) {
            SBValued s = resolveHelper(queue, contexts);
            if(s != null && s.is(Subject.TYPE)) {
                subjects.add(s);
                count++;
            }
        }
        
        return subjects;
    }
    
    private SBValued resolveHelper(Queue<T> queue,  URI[] contexts) {

        if(queue.isEmpty()) {
            LOGGER.trace("...queue is empty, returning...");
            return null;
        }
            
        T identifier = queue.remove();

        if(parsedIds.contains(identifier))
            return null;

        SBValued s = resolve(SBValued.class, identifier, contexts);

        if(s == null) {
            LOGGER.warn("Could not retrieve obj of: {}", identifier);
            return null;
        }
        
        parsedIds.add(identifier);
        
        updateQueue(queue, s, contexts);
        
        return s;

    }
    
  
    protected abstract Queue<T> updateQueue(Queue<T> queue, SBValued s, URI[] contexts);
    
}
