/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data;

import java.net.URI;
import java.util.List;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspaceService;

/**
 *
 * @author owengilfellon
 */
public interface SBRemoteCrawler<T> extends SBWorkspaceService {
    
    public <V extends SBValued> V resolve(Class<V> clazz, T identifier, URI... contexts);
    
    public List<? extends SBValued> crawl(T startingIdentifier, int maxCount, URI... contexts);
    
}
