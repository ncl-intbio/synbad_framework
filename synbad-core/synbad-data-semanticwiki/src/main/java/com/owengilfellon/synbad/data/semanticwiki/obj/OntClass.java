/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data.semanticwiki.obj;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owen
 */
@DomainObject(rdfType = OntClass.TYPE)
public class OntClass extends SemanticWikiObj {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(OntClass.class);
    public static final String TYPE = "http://www.w3.org/2002/07/owl#Class";

    public OntClass(URI identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }

    @Override
    public URI getType() {
        return URI.create(TYPE);
    }
    
    public Set<URI> getInstanceUris() {
        Set<URI> subclasses =  
            ws.incomingEdges(this, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", Subject.class, getContexts()).stream()
            .map(e -> e.getFrom())
            .collect(Collectors.toSet());
        return subclasses;
    }
    
    public Set<URI> getSuperClassUris() {
        Set<URI> subclasses =  
            ws.outgoingEdges(this, "http://www.w3.org/2000/01/rdf-schema#subClassOf", OntClass.class, getContexts()).stream()
            .map(e -> e.getTo())
            .collect(Collectors.toSet());
        return subclasses;
    }
    
    public Set<URI> getSubclassUris() {
        Set<URI> subclasses =  
            ws.incomingEdges(this, "http://www.w3.org/2000/01/rdf-schema#subClassOf", OntClass.class, getContexts()).stream()
            .map(e -> e.getFrom())
            .collect(Collectors.toSet());
        
        return subclasses;
    }
    
    public Set<Subject> getInstances() {
        Set<Subject> parentclasses =  getInstanceUris().stream()
            .map(uri -> crawler.resolve(Subject.class, uri, getContexts()))
            .collect(Collectors.toSet());
        return parentclasses;
    }
    
    public Set<OntClass> getSuperClasses() {
        Set<OntClass> parentclasses =  getSuperClassUris().stream()
            .map(uri -> crawler.resolve(OntClass.class, uri, getContexts()))
            .collect(Collectors.toSet());
        return parentclasses;
    }
    
    public Set<OntClass> getAllSuperClasses() {
        return getAllSuperClasses(new HashSet<>());
    }
    
    public Set<OntClass> getAllSubClasses() {
        return getAllSubClasses(new HashSet<>());
    }
    
    private Set<OntClass> getAllSuperClasses(Set<OntClass> set) {
        Set<OntClass> superClasses = getSuperClasses();
        for(OntClass clazz : superClasses) {
            if(!set.contains(clazz)) {
                //LOGGER.debug("Adding {}", clazz.getLabel());
                    set.add(clazz);
                    clazz.getAllSuperClasses(set);
            }
        }
        
        return set;
    }
    
    private Set<OntClass> getAllSubClasses(Set<OntClass> set) {
        Set<OntClass> subclasses = getSubclasses();
        for(OntClass clazz : subclasses) {
            if(!set.contains(clazz)) {
                //LOGGER.debug("Adding {}", clazz.getLabel());
                    set.add(clazz);
                    clazz.getAllSubClasses(set);
            }
        }
        
        return set;
    }
    
    public Set<OntClass> getSubclasses() {
        Set<OntClass> subclasses =  getSubclassUris().stream()
            .map(uri -> crawler.resolve(OntClass.class, uri, getContexts()))
            .collect(Collectors.toSet());
        return subclasses;
    }

}
