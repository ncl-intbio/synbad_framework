/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data.semanticwiki.obj;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.api.workspace.annotations.DomainObject;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;

/**
 *
 * @author owen
 */
@DomainObject(rdfType = Subject.TYPE)
public class Subject extends SemanticWikiObj {
    
    public static final String TYPE = "http://semantic-mediawiki.org/swivt/1.0#Subject";

    public Subject(URI identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
    }

    @Override
    public URI getType() {
        return URI.create(TYPE);
    }
    
   
    
    public URI getIsDefinedBy() {
        return getValue("http://www.w3.org/2000/01/rdf-schema#isDefinedBy").asURI();
    }
    
       public String getImage() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AHas_image");
        return v == null ? "" : v.asString();
    }
    
    public String getDisplayTitle() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3ADisplay_title_of");
        return v == null ? "" : v.asString();
    }
    
    public Set<OntClass> getClasses() {
        Set<OntClass> classes =  
            ws.outgoingEdges(this, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", OntClass.class, getContexts()).stream()
            .map(e -> e.getTo())
            .map(uri -> crawler.resolve(OntClass.class, uri, getContexts()))
            .collect(Collectors.toSet());
        return classes;
    }
    
    public Set<OntClass> getSuperClasses() {
        Set<OntClass> classes =  
            ws.outgoingEdges(this, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", OntClass.class, getContexts()).stream()
            .map(e -> e.getTo())
            .map(uri -> crawler.resolve(OntClass.class, uri, getContexts()))
            .collect(Collectors.toSet());
        return classes;
    }
    
    /*
    
    public Collection<String> getAffiliation() {
        Collection<SBValue> values = getValues("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AAffiliation");
        return values.stream().map(v -> v.asString()).collect(Collectors.toSet());
    }
    
    public Collection<String> getFeaturing() {
        Collection<SBValue> values = getValues("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AFeaturing");
        return values.stream().map(v -> v.asString()).collect(Collectors.toSet());
    }
    
    public String getFirstAppearance() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AFirst_appearance");
        return v == null ? "" : v.asString();
    }
    */
 
    /*
    public String getSpecies() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3ASpecies");
        return v == null ? "" : v.asString();
    }
    
    public String getMother() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AMother");
        return v == null ? "" : v.asString();
    }
    
    public String getFather() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AFather");
        return v == null ? "" : v.asString();
    }
    
    public String getJob() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AJob");
        return v == null ? "" : v.asString();
    }
    
    public String getActor() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AActor");
        return v == null ? "" : v.asString();
    }
    
    public String getDoctor() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3ADoctor");
        return v == null ? "" : v.asString();
    }
    
    public String getEpisodeCount() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3AEpcount");
        return v == null ? "" : v.asString();
    }
    
    public String getSeason() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3ASeason");
        return v == null ? "" : v.asString();
    }
    
    public String getSeasonSerialNumber() {
        SBValue v = getValue("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3ASeason_serial_number");
        return v == null ? "" : v.asString();
    }*/
    

    
    /*
    
    public Collection<String> getSecondaryDoctors() {
        Collection<SBValue> values = getValues("http://tardis.wikia.com/wiki/Special:URIResolver/Property-3ASecondary_doctor");
        return values.stream().map(v -> v.asString()).collect(Collectors.toSet());
    }*/

}
