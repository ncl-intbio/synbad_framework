/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data.semanticwiki;

import com.owengilfellon.synbad.data.SBRemoteCrawler;
import com.owengilfellon.synbad.data.semanticwiki.obj.Subject;
import java.net.URI;
import java.util.List;

/**
 *
 * @author owen
 */
public interface SemanticWikiCrawler extends SBRemoteCrawler<URI> {

    @Override
    public List<Subject> crawl(URI startingIdentifier, int maxCount, URI... contexts);

    
}
