/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data.semanticwiki;

import com.owengilfellon.synbad.data.ARemoteCrawler;
import com.owengilfellon.synbad.data.semanticwiki.obj.Subject;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;

/**
 *
 * @author owen
 */
public class DefaultSemanticWikiCrawler extends ARemoteCrawler<URI> implements SemanticWikiCrawler{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSemanticWikiCrawler.class);
    
    @Override
    public <T extends SBValued> T resolve(Class<T> clazz, URI uri, URI... contexts) {
        
        URI objectId = getObjectUriFromEscaped(uri.toASCIIString());
        
        if(!getParsedIds().contains(objectId)) {
            LOGGER.trace("Resolving... {}", objectId.toASCIIString());
            addRdfFromUri(uri, contexts);
        }
        
        T s = workspace.getObject(objectId, clazz, contexts);
        
        if(s == null) {
            LOGGER.warn("Did not successfully retrieve object: {}", objectId.toASCIIString());
            return null;
        }
        
        getParsedIds().add(objectId);
        return s;
    } 
    
    @Override
    public List<Subject> crawl(URI startingUri, int maxCount, URI... contexts) {
        return super.crawl(startingUri, maxCount, contexts)
                .stream().map(s -> (Subject)s)
                .collect(Collectors.toList());
        
    }
    
    private void addRdfFromUri(URI uri, URI[] contextId) {
        
        URI toResolve = getRdfUriFromIdUri(uri.toASCIIString());
        
        if(toResolve == null) {
            LOGGER.warn("Could not get RDF URI from {}", uri.toASCIIString());
            return;
        }

        URL url;
        try {
            url = new URL(toResolve.toASCIIString());
        } catch (MalformedURLException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return;
        }
        try (InputStream s = url.openStream()) {
            workspace.getRdfService().addRdf(s, contextId);
        }
        catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
    }
    
    private URI getObjectUriFromEscaped(String uri) {
       
        String updatedUri = uri;
        
        if(updatedUri.contains("Special:ExportRDF"))
            updatedUri = updatedUri.replace("Special:ExportRDF", "Special:URIResolver");
        else if(!updatedUri.contains("Special:URIResolver"))
            updatedUri = updatedUri.replace("wiki/", "wiki/Special:URIResolver/");
        
        if(updatedUri.contains("?xmlmime=rdf"))
            updatedUri = updatedUri.replace("?xmlmime=rdf", "");
        
        return URI.create(updatedUri);
        
    }
    
    private URI getRdfUriFromIdUri(String uri) {
        
        String updatedUri = uri;
        
        if(updatedUri.contains("-3A"))
            updatedUri = updatedUri.replace("-3A", ":");
        
        if(updatedUri.contains("-2D"))
            updatedUri = updatedUri.replace("-2D", "-");
        
        if(updatedUri.contains("-26"))
            updatedUri = updatedUri.replace("-26", "%26");
        
        if(updatedUri.contains("Special:URIResolver"))
            updatedUri = updatedUri.replace("Special:URIResolver", "Special:ExportRDF");
        else if(!updatedUri.contains("Special:ExportRDF"))
            updatedUri = updatedUri.replace("wiki/", "wiki/Special:ExportRDF/");

        if(updatedUri.contains("Special:ExportRDF"))
            updatedUri = updatedUri.concat("?xmlmime=rdf");
        
        LOGGER.trace("Returning {} from {}", updatedUri, uri);
        return URI.create(updatedUri);
    }

    @Override
    protected Queue<URI> updateQueue(Queue<URI> queue, SBValued s, URI[] contexts) {
        if(s.is(Subject.TYPE)) {
            workspace.incomingEdges(s, contexts ).stream().forEach(e -> queue.add(e.getFrom()));
            workspace.outgoingEdges(s, contexts ).stream().forEach(e -> queue.add(e.getTo()));
            queue.addAll(s.getPredicates().stream()
                .flatMap(p -> s.getValues(p).stream())
                .filter(SBValue::isURI)
                .map(SBValue::asURI)
                .filter(v -> getParsedIds().contains(v))
                .collect(Collectors.toSet()));
        }
        return queue;
    }
}
