/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data.semanticwiki.obj;

import com.owengilfellon.synbad.data.semanticwiki.DefaultSemanticWikiCrawler;
import com.owengilfellon.synbad.data.semanticwiki.SemanticWikiCrawler;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBValueProvider;
import uk.ac.ncl.icos.synbad.workspace.objects.ASBValued;

/**
 *
 * @author owen
 */
public abstract class SemanticWikiObj extends ASBValued {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SemanticWikiObj.class);
    protected final SemanticWikiCrawler crawler;

    public SemanticWikiObj(URI identity, SBWorkspace workspaceIdentity, SBValueProvider provider) {
        super(identity, workspaceIdentity, provider);
        this.crawler = new DefaultSemanticWikiCrawler();
        this.crawler.setWorkspace(workspaceIdentity);
    }
    
    public String getLabel() {
        SBValue label = getValue("http://www.w3.org/2000/01/rdf-schema#label");
        return label == null ? "" : label.asString();
    }
    
    protected URI addRdfFromUri(URI uri ) {
        URI toResolve = getRdfUriFromIdUri(uri.toASCIIString());
        
        if(toResolve == null) {
            LOGGER.warn("Could not get RDF URI from {}", uri.toASCIIString());
            return null;
        }

        URL url;
        try {
            url = new URL(toResolve.toASCIIString());
        } catch (MalformedURLException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
        try (InputStream s = url.openStream()) {
            ws.getRdfService().addRdf(s, getContexts());
        }
        catch (IOException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        }
        
        URI toReturn = uri.toASCIIString().contains("Category:") ? 
                URI.create(uri.toASCIIString().replace("Category:", "Category-3A")) :
                uri;
        
        return toReturn;
    }
    
    protected URI getRdfUriFromIdUri(String uri) {
        
        String updatedUri = uri;
        
        if(updatedUri.contains("-3A"))
            updatedUri = updatedUri.replace("-3A", ":");
        
        if(updatedUri.contains("-2D"))
            updatedUri = updatedUri.replace("-2D", "-");
        
        if(updatedUri.contains("Special:URIResolver"))
            updatedUri = updatedUri.replace("Special:URIResolver", "Special:ExportRDF");

        if(updatedUri.contains("Special:ExportRDF"))
            updatedUri = updatedUri.concat("?xmlmime=rdf");
        
        LOGGER.trace("Returning {} from {}", updatedUri, uri);
        return URI.create(updatedUri);
    }
    
}
