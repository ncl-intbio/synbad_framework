/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.workspace.impl;

import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerDisk;
import com.owengilfellon.synbad.data.semanticwiki.obj.Subject;
import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import uk.ac.ncl.icos.synbad.rdf.DefaultSBRdfDiskService;
import uk.ac.ncl.icos.synbad.workspace.actions.CreateObject;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owen
 */
public class SBWorkspaceManagerDiskImplTest {

    private final String BASE_URL = "http://www.owengilfellon.com/";
    private final URI WORKSPACE = URI.create(BASE_URL + "workspace");
    private final URI CONTEXT_1 = URI.create(BASE_URL + "context1");
    private final URI CONTEXT_2 = URI.create(BASE_URL + "context2");
    private final URI OBJ = URI.create(BASE_URL + "obj");
    
    public SBWorkspaceManagerDiskImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {

    }


    /**
     * Test of getWorkspace method, of class SBWorkspaceManagerDiskImpl.
     */
    @Test
    public void testGetWorkspace() {
        
        DefaultSBWorkspaceManagerDisk m1 = new DefaultSBWorkspaceManagerDisk();
        SBWorkspace ws = m1.getWorkspace(WORKSPACE);
//        ws.createContext(CONTEXT_1);
//        ws.createContext(CONTEXT_2);
        if(!DefaultSBRdfDiskService.class.isAssignableFrom(ws.getRdfService().getClass()))
            return;

        ws.perform(new CreateObject(
                OBJ, 
                Subject.TYPE, 
                null, 
                null, 
                ws,
                new URI[] { CONTEXT_1 }));
        Subject obj = ws.getObject(OBJ, Subject.class, new URI[] { CONTEXT_1 });
        
        Assert.assertNotNull("Object should not be null", obj);
        
        int contexts_1 = ws.getContextIds().length;
        int sys_contexts_1 = ws.getSystemContextIds(ws.getContextIds()).length;
        m1.closeWorkspace(WORKSPACE);

        DefaultSBWorkspaceManagerDisk m2 = new DefaultSBWorkspaceManagerDisk();
        SBWorkspace ws2 = m2.getWorkspace(WORKSPACE);
        int contexts_2 = ws2.getContextIds().length;
        int sys_contexts_2 = ws2.getSystemContextIds(ws2.getContextIds()).length;
        m2.closeWorkspace(WORKSPACE);

        
        Assert.assertEquals("Contexts of workspace should be same when retrieved from disk", contexts_1, contexts_2);
        Assert.assertEquals("System Contexts of workspace should be same when retrieved from disk", sys_contexts_1, sys_contexts_2);
        
    }

    
}
