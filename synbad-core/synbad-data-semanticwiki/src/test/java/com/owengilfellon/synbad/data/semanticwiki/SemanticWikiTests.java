/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.owengilfellon.synbad.data.semanticwiki;

import com.owengilfellon.synbad.data.semanticwiki.obj.OntClass;
import com.owengilfellon.synbad.data.semanticwiki.obj.Subject;
import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.netbeans.junit.MockServices;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;
import uk.ac.ncl.icos.synbad.workspace.DefaultSBWorkspaceManagerMem;

/**
 *
 * @author owengilfellon
 */
public class SemanticWikiTests {
    
    private static final String CRAWL_START = "Iris_Wildthyme_(series)";
    private static final String CRAWL_START_2 = "Faction_Paradox";
    private static final String CRAWL_START_3 = "Jago_-26_Litefoot";
    private static final String CRAWL_START_4 = "Bernice_Summerfield";

    private static final int MULTIPLE_CRAWL_LENGTH = 1;
    
    private static final String CAT_SPIN_OFF = "Category-3ADoctor_Who_spin-2Doffs";
    private static final String CAT_FACTION_PARADOX = "Category-3AFP_series";
    private static final String CAT_BIG_FINISH_SPINOFF = "Category-3ABig_Finish_audio_spin-2Doff_series";
    private static final String JAGO_AND_LITEFOOT = "Category-3AJago_-26_Litefoot_audio_series";
    
    
    private static final URI workspaceId = URI.create("http://www.owengilfellon.com/wikiTest");
    private static final URI contextId = URI.create("http://www.owengilfellon.com/context1");
    
    private static Logger LOGGER = LoggerFactory.getLogger(SemanticWikiTests.class);
    
    private static final int ONTOLOGY_MAX_DEPTH = 100;
    
    private SBWorkspaceManager m;
    private SBWorkspace result;
    
    private static final Collection<URI> parsedIds = new HashSet<>();

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        MockServices.setServices(DefaultSBWorkspaceManagerMem.class);
        
        m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        result = m.getWorkspace(workspaceId);
        //result.createContext(contextId);
    }
    
    @After
    public void tearDown() {
        m.closeWorkspace(workspaceId);
    }

    /**
     * Test of getWorkspace method, of class SBWorkspaceManager.
     */
   // @Test
    public void testCrawl() {
        LOGGER.debug("---testCrawl---");
        SemanticWikiCrawler crawler = new DefaultSemanticWikiCrawler();
        crawler.setWorkspace(result);
        doCrawl(crawler, CRAWL_START, MULTIPLE_CRAWL_LENGTH);
       // subjects.stream().forEach(s -> printSubject(result, s));
   }
    
    
     /**
     * Test of getWorkspace method, of class SBWorkspaceManager.
     */
   // @Test
    public void testMultipleCrawls() {
        LOGGER.debug("---testMultipleCrawls---");
        SemanticWikiCrawler crawler = new DefaultSemanticWikiCrawler();
        crawler.setWorkspace(result);
        Collection<SBStatement> s1 = doCrawl(crawler, CRAWL_START_2, MULTIPLE_CRAWL_LENGTH);
        Collection<SBStatement> s2 = doCrawl(crawler, CRAWL_START_3, MULTIPLE_CRAWL_LENGTH);
        Collection<SBStatement> s3 = doCrawl(crawler, CRAWL_START_4, MULTIPLE_CRAWL_LENGTH);
//        
//        Set<Statement> s1s2 = new HashSet(s2);
//        s1s2.removeAll(s1);
//        
//        Set<Statement> s2s3 = new HashSet(s3);
//        s2s3.removeAll(s2);
//        
//        System.out.println("S1: " + s1.size());
//        System.out.println("S2: " + s2.size());
//        System.out.println("S3: " + s3.size());
//        System.out.println("S1S2: " + s1s2.size());
//        System.out.println("S2S3: " + s2s3.size());
//        Sets.difference(s2, s1).stream().forEach(s -> System.out.println("Diff S1|S2 " + s));
//        Sets.difference(s3, s2).stream().forEach(s -> System.out.println("Diff S2|S3 " + s));
    }
    
    private Collection<SBStatement> doCrawl(SemanticWikiCrawler crawler, String start, int size) {
        List<Subject> subjects = crawler.crawl(URI.create("http://tardis.wikia.com/wiki/Special:URIResolver/" + start),  
                size, contextId);
        int returnedSubjects = subjects.size();
        int allObjects = result.getObjects(SBValued.class, new URI[] { contextId }).size();
        int allSubjects = result.getObjects(Subject.class, new URI[] { contextId }).size();
        int statements = result.getRdfService().getStatements(null, null, null, contextId).size();
        LOGGER.debug("Returned {} subjects from {}, of {} subjects from {} objects and {} statements",
                returnedSubjects, start, allSubjects, allObjects, statements);
        return result.getRdfService().getStatements(null, null, null, contextId);
     //  subjects.stream().forEach(s -> printSubject(result, s));
    }
    
    /**
     * Test of getWorkspace method, of class SBWorkspaceManager.
     */
    //@Test
    public void testGetOntClasses() {
        LOGGER.debug("---testGetOntClasses---");
        SemanticWikiCrawler crawler = new DefaultSemanticWikiCrawler();
        crawler.setWorkspace(result);
        OntClass c = crawler.resolve(OntClass.class,  URI.create("http://tardis.wikia.com/wiki/Special:URIResolver/" + JAGO_AND_LITEFOOT), contextId);
        LOGGER.debug("\t- {}", c.getLabel());
//        c.getAllSuperClasses().forEach(sc -> {
//            LOGGER.debug("\t\t+ {}", sc.getIdentity());
//        });
//        c.getSubclasses().forEach(sc -> {
//            LOGGER.debug("\t\t- {}", sc.getIdentity());
//        });
    }
    
    /**
     * Test of getWorkspace method, of class SBWorkspaceManager.
     */
    //@Test
    public void testGetAllSuperClasses() {
        LOGGER.debug("---testGetAllSuperClasses---");
        SemanticWikiCrawler crawler  = new DefaultSemanticWikiCrawler();
        crawler.setWorkspace(result);
        Subject subject = crawler.resolve(Subject.class,  URI.create("http://tardis.wikia.com/wiki/Special:URIResolver/" + CRAWL_START), contextId);
        LOGGER.debug("{}", subject.getLabel());
//        subject.getClasses().stream().forEach(c -> {
//            LOGGER.debug("\t- {}", c.getLabel());
//            c.getAllSuperClasses().forEach(sc -> {
//                LOGGER.debug("\t\t+ {}", sc.getIdentity());
//            });
//            c.getAllSubClasses().forEach(sc -> {
//                LOGGER.debug("\t\t- {}", sc.getIdentity());
//            });
//        });
    }
    
    public static void printSubject(SBWorkspace workspace, Subject subject) {
            LOGGER.debug(subject.getLabel());
   
            if(!subject.getImage().isEmpty())
                LOGGER.debug("\tImage: {}", subject.getImage());

            if(!subject.getDisplayTitle().isEmpty())
                LOGGER.debug("\tDisplay Title: {}", subject.getDisplayTitle());
            
            for(String predicate : subject.getPredicates()) {
                for(SBValue value : subject.getValues(predicate)) {
                   LOGGER.debug("\tV: {}: {}", predicate, value); 
            }
                
            workspace.incomingEdges(subject, new URI[] { contextId })
                    .stream().forEach(e -> LOGGER.debug("\tI: {}", e));
            workspace.outgoingEdges(subject, new URI[] { contextId })
                    .stream().forEach(e -> LOGGER.debug("\tO: {}", e));
            
       
//            subject.getClasses().stream().forEach(c -> {
//                LOGGER.debug("\tClass {}: [ {} instances ]", c.getLabel(), c.getInstanceUris().size());
//                c.getSubclasses().stream().forEach(sc ->  { LOGGER.debug("\t\tC: {} [ {} instances ]", sc.getLabel(), sc.getInstanceUris().size());});
//            });
//
            
        }
    }
}
