/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

import java.util.List;


/**
 *
 * @author owengilfellon
 */
public interface SBEventSource {
    
    <T> void subscribe(SBEventFilter filter, SBSubscriber handler);
    
    <T> void unsubscribe(SBSubscriber subscriber);
   
    List<SBSubscriber> getListeners();
    
}
