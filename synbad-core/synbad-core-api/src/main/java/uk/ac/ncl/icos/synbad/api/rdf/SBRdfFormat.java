package uk.ac.ncl.icos.synbad.api.rdf;

public enum SBRdfFormat {
     JSON,
     XML
}
