/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import java.io.Serializable;
import java.net.URI;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public final class SBIdentity implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SBIdentity.class);
    private static final long serialVersionUID = -76740547222402651L;
    
    private final URI identity;
    private final URI persistentId;
    private final String uriPrefix;
    private final String displayId;
    private final String version;

    public SBIdentity(String uriPrefix, String displayId, URI identity) {
        this.identity = identity;
        this.persistentId = SBIdentityHelper.getURI(SBIdentityHelper.getPersistentIdFromId(identity));
        this.uriPrefix = uriPrefix;
        this.displayId = displayId;
        this.version = null;
    }

    private SBIdentity(String uriPrefix, String displayId, URI identity, URI persistentId) {
        this.identity = identity;
        this.persistentId = persistentId;
        this.uriPrefix = uriPrefix;
        this.displayId = displayId;
        this.version = null;
    }
    
    public SBIdentity(String uriPrefix, String displayId, String version, URI identity) {
        this.identity = identity;
        this.persistentId = SBIdentityHelper.getURI(SBIdentityHelper.getPersistentIdFromId(identity));
        this.uriPrefix = uriPrefix;
        this.displayId = displayId;
        this.version = version;
    }

    private SBIdentity(String uriPrefix, String displayId, String version, URI identity, URI persistentId) {
        this.identity = identity;
        this.persistentId = persistentId;
        this.uriPrefix = uriPrefix;
        this.displayId = displayId;
        this.version = version;
    }
   
    public static SBIdentity getIdentity(String prefix, String displayId) {
        return new SBIdentity(prefix, displayId, SBIdentityHelper.getURI(prefix + "/" + displayId));
    }

    public static SBIdentity getIdentity(String prefix, String displayId, String version)  {
        return new SBIdentity(prefix, displayId, version, SBIdentityHelper.getURI(prefix + "/" + displayId + "/" + version));
    }
    
    public static SBIdentity getIdentity(String prefix, String parentDisplayId, String displayId, String version)  {
        String persistentId = prefix + "/" + parentDisplayId + "/" +  displayId;
        return new SBIdentity(prefix, parentDisplayId + "/" +  displayId, version, SBIdentityHelper.getURI(persistentId + "/" + version), SBIdentityHelper.getURI(persistentId));
    }
    
    public static SBIdentity getIdentity(URI identity) throws SBIdentityException {

        String prefix = SBIdentityHelper.getPrefixFromId(identity);
        String displayId = SBIdentityHelper.getDisplayIdFromId(identity);
        URI persistentId = SBIdentityHelper.getURI(SBIdentityHelper.getPersistentIdFromId(identity));
        String version = SBIdentityHelper.getVersionFromId(identity);

        if(prefix == null) {
            LOGGER.error("Prefix is null: [{}]", identity.toASCIIString());
            throw new SBIdentityException("Prefix cannot be null");
        }

        if(displayId == null) {
             LOGGER.error("DisplayId is null: [{}]", identity.toASCIIString());
             throw new SBIdentityException("DisplayId cannot be null");
        }

        if(version != null && !version.isEmpty()) {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Constructing identity: {}/{}/{}", prefix, displayId, version);
           return new SBIdentity(prefix, displayId, version, identity, persistentId);
        } else {
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Constructing identity: {}/{}", prefix, displayId);
           return new SBIdentity(prefix, displayId, identity, persistentId);
        }
    }

    public URI getIdentity() {
        return identity;
    }

    public URI getPersistentId() {
        return persistentId;
    }

    public String getUriPrefix() {
        return uriPrefix;
    }

    public String getDisplayID() {
        return displayId;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return getDisplayID();
    }
    
    

    @Override
        public boolean equals(Object obj) {
            if(obj == null)
                return false;
            if(obj == this)
                return true;
            if(!(obj instanceof SBIdentity))
                return false;
            
            SBIdentity id = (SBIdentity) obj;
            return id.getIdentity().equals(getIdentity());
        }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.identity);
        return hash;
    }
}
