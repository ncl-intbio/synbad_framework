/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.rdf;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Collection;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;

/**
 * RdfService abstracts an RDF triple store.
 * 
 * @author owengilfellon
 */
public interface SBRdfService {

    //SBIdentityFactory getIdentityFactory();
    
    //URI getWorkspaceId();
    
    // ========= Loading and unloading files ==========
    
    boolean addRdf(InputStream stream, URI... contexts);
    
    boolean addRdf(File f, URI... contexts);

    boolean export(OutputStream out, SBRdfFormat format, URI... contexts);

    boolean removeRdf(File f);
    
    URI[] getContexts();
    
    void createContext(URI context);
    
    void removeContext(URI context);
    
    // ========== Accessors =============

    Collection<SBStatement> getStatements(String subject, String predicate, SBValue object, URI... contexts);

    Collection<String> getPredicates(String subject, URI... contexts);

    Set<SBValue> getValues(String subject, String predicate, URI... contexts);
    
    SBValue getValue(String subject, String predicate, URI... contexts);
    
    // ======== Mutators ========
    
    boolean addStatements(Collection<SBStatement> statement, URI... contexts);
    
    boolean removeStatements(Collection<SBStatement> statement, URI... contexts);

    void shutdown();
    
}
