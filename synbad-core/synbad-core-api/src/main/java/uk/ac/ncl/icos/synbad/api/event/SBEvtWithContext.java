/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

import java.net.URI;

/**
 *
 * @author owengilfellon
 */
public interface SBEvtWithContext {
    
    public URI[] getContexts();
    
}
