/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

/**
 * Base interface for all events in SynBad. 
 * 
 * @author owengilfellon
 */
public interface SBEvent<D> {

    public <T extends SBEventType> T getType();
    
    public D getData();
    
    public Class getDataClass();

}



