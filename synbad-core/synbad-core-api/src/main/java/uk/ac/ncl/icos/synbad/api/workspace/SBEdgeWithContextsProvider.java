/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.workspace;

import java.net.URI;
import java.util.List;

/**
 *
 * @author owengilfellon
 */
public interface SBEdgeWithContextsProvider<N, E> {
    
    public N getEdgeSource(E edge, URI[] contexts);
            
    public N getEdgeTarget(E edge, URI[] contexts);
    
    List<E> incomingEdges(N node, URI[] contexts);
    
    List<E> outgoingEdges(N node, URI[] contexts);
}
