/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.workspace;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.net.URI;

/**
 *
 * @author owen
 */
public interface SBWorkspaceService {
    
    public void setWorkspace(SBWorkspace workspace);
    
    public SBWorkspace getWorkspace();
    
    public void close();
    
}
