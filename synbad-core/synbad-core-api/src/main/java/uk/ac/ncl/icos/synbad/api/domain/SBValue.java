/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.io.Serializable;
import java.net.URI;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import javax.xml.namespace.QName;
import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class SBValue implements Serializable {

    private static final long serialVersionUID = -3152948577861547870L;
    private final Object value;
    private boolean isDouble = false;
    private boolean isBoolean = false;
    private boolean isInt = false;
    private boolean isString = false;
    private boolean isURI = false;
    private boolean isQName = false;
    
    private static final String DOUBLE_REGEX = "^(\\d+)(\\.)(\\d+)([eE][-+]?[0-9]+)?$";
    private static final String INT_REGEX = "^[-]?\\d+$";
    private static final String ID_REGEX = "^([a-z0-9+.-]+:\\/\\/?[\\da-z\\.-]+\\.[a-z\\.]{2,6})(\\/)(([\\w-_]*)|(([\\w-_]+)([#\\/:]?))*?([\\w-_]+)?)(([\\/#:])+([\\d][\\d\\w\\.-]*))?$";

    private static final Predicate<String> DOUBLE_PATTERN = Pattern.compile(DOUBLE_REGEX).asPredicate();
    private static final Predicate<String> INT_PATTERN = Pattern.compile(INT_REGEX).asPredicate();
    private static final Predicate<String> ID_PATTERN = Pattern.compile(ID_REGEX).asPredicate();
    
    private static final UrlValidator URL_VALIDATOR = new UrlValidator(); 
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SBValue.class);
    
    private static LoadingCache<String, SBValue> uriCache = CacheBuilder.newBuilder()
        .expireAfterAccess(20, TimeUnit.MINUTES)
        .build(
            new CacheLoader<String, SBValue>() {
                @Override
                public SBValue load(String s) {
                   if(s.isEmpty()) {
                        return new SBValue(s);
                   } else if (s.equalsIgnoreCase("true") || s.equalsIgnoreCase("false")) {
                       return new SBValue(Boolean.parseBoolean(s));
                   } else if(INT_PATTERN.test(s)) {
                       try {
                            return new SBValue(Integer.parseInt(s));
                       } catch(NumberFormatException ex) {
                           LOGGER.warn(ex.getLocalizedMessage());
                       }
                   } else if(DOUBLE_PATTERN.test(s)) {
                       try {
                            return new SBValue(Double.parseDouble(s));
                       } catch(NumberFormatException ex) {
                           LOGGER.warn(ex.getLocalizedMessage());
                       }
                   } else  {
                       try {
                            URI uri = SBIdentityHelper.getURI(s);
                            if(uri.isAbsolute())
                                return new SBValue(uri);
                       } catch ( IllegalArgumentException | NullPointerException  ex) {
                           // ignore this...
                       }
                   }
                   return new SBValue(s);
                }
            }
        );


    SBValue(boolean value) {
        this.value = value;
        this.isBoolean = true;
    }
    
    SBValue(double value) {
        this.value = value;
        this.isDouble = true;
    }
    
    SBValue(int value) {
        this.value = value;
        this.isInt = true;
    }
    
    SBValue(String value) {
        this.value = value;
        this.isString = true;
    }
    
    SBValue(URI value) {
        this.value = value;
        this.isURI = true;
    }
    
    SBValue(QName value) {
        this.value = value;
        this.isQName = true;
    }
    
    public boolean asBoolean() {
        return (boolean) value;
    }
    
    public double asDouble() {
        return (double) value;
    }
        
    public int asInt() {
        return (int) value;
    }
            
    public String asString() {
        if(value instanceof String)
            return (String) value;
        else
            return value.toString();
    }
                
    public URI asURI() {
        return (URI) value;
    }
    
    public QName asQName() {
        return (QName) value;
    }

    public boolean isBoolean() {
        return isBoolean;
    }

    public boolean isDouble() {
        return isDouble;
    }

    public boolean isInt() {
        return isInt;
    }

    public boolean isQName() {
        return isQName;
    }

    public boolean isString() {
        return isString;
    }

    public boolean isURI() {
        return isURI;
    }

    public Object getValue() {
        return value;
    }
    
    public static SBValue parseValue(Object value) {
     
        if(value == null)
            throw new NullPointerException("Cannot create an SBValue from a null object");

        if(value instanceof URI)
            return new SBValue((URI)value);
        
        if(value instanceof Boolean)
            return new SBValue((Boolean)value);
        
        if(value instanceof Integer)
            return new SBValue((Integer)value);
        
        if(value instanceof Double)
            return new SBValue((Double)value);
        
        if(value instanceof QName)
            return new SBValue((QName)value);
        
        String s = (value instanceof String) ? (String)value : value.toString();
        
        try {
            return uriCache.get(s);
        } catch (ExecutionException ex) {

        }
        
        return new SBValue(s);
        
    }
 

    @Override
    public String toString() {
        if(isURI)
            return asURI().toASCIIString();
        else
            return value.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SBValue))
            return false;
        SBValue v = (SBValue) obj;
        return v.getValue().equals(value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(value);
        return hash;
    }

}
