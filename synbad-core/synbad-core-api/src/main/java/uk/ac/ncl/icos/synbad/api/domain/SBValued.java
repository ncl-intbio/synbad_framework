    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.io.Serializable;
import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;


/**
 * The base interface for all domain objects within SynBad. These domain objects
 * are uniquely identified by a URI, and owned by a single workspace. They can be
 * located in one or more contexts in that workspace, again identified by URIs,
 * and own zero-to-many predicate-value pairs.
 * 
 * @author owengilfellon
 */
public interface SBValued extends SBEntity, Serializable {
    
    /**
     * 
     * @return The URI that uniquely identifies this object.
     */
    public URI getIdentity();
    
    /**
     * 
     * @return The <code>rdf:type</code> of this object.
     */
    public URI getType();
    
    /**
     * 
     * @param rdfType the <code>rdf:type</code> to check
     * @return True, if the provided string matches the <code>rdf:type</code> of
     * this object.
     */
    public boolean is(String rdfType);
    
    /**
     * 
     * @return the identifier of the Workspace that this object belongs to.
     */
    public SBWorkspace getWorkspace();
    
    /**
     * 
     * @return the contexts that this object belongs to.
     */
    public URI[] getContexts();
    
    /**
     * 
     * @return all predicates of statements of which this object is the subject.
     */
    public Set<String> getPredicates();
    
    /**
     * 
     * @param predicate
     * @return a single value from the statements of which this object is the
     * subject and the provided string is the predicate
     */
    public SBValue getValue(String predicate);
    
    /**
     * 
     * @param predicate
     * @return all values from the statements of which this object is the
     * subject and the provided string is the predicate
     */
    public Collection<SBValue> getValues(String predicate);
    
    /**
     * 
     * @param <T> the type of the value
     * @param predicate
     * @param clazz the class of the value. The supported types are: 
     * <code>Double</code>, <code>Boolean</code>, <code>Integer</code>, 
     * <code>String</code>, <code>URI</code> and <code>QName</code>.
     * @return all values of the provided type, from the statements of which
     * this object is the subject and the provided string is the predicate
     */
    public <T> Collection<T> getValues(String predicate, Class<T> clazz);

    
    <T extends SBValued> Optional<T> as(Class<T> clazz);
}
