/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import java.net.URI;

/**
 * The base interface for all domain objects within SynBad that derive from the 
 * SBOL <code>Identified</code> class. As per <code>SBValued</code>, these domain objects
 * are uniquely identified by a URI, and owned by a single workspace. They can be
 * located in one or more contexts in that workspace, again identified by URIs,
 * and own zero-to-many predicate-value pairs.
 * 
 * @author owengilfellon
 */
public interface SBIdentified extends SBValued {

    /**
     * 
     * @return the prefix portion of the identity, excluding the trailing divider.
     */
    String getPrefix();
    
    /**
     * 
     * @return the display ID portion of the identity, excluding any trailing
     * divider.
     */
    String getDisplayId();
    
    /**
     * 
     * @return the version portion of the identity.
     */
    String getVersion();
    
    /**
     * 
     * @return the user-friendly name of this object.
     */
    String getName();
    
    /**
     * 
     * @return the identity of this object, without the version.
     */
    URI getPersistentId();

    /**
     * 
     * @return the identity of the object that this object was derived from. This
     * value is optional, and therefore may return null.
     */
    URI getWasDerivedFrom();

    /**
     * 
     * @return the free-text description of this object.
     */
    String getDescription();
    
}
