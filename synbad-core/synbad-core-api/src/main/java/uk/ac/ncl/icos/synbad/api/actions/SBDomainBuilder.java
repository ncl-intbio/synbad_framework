/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.actions;

import java.net.URI;

import uk.ac.ncl.icos.synbad.api.domain.SBValue;

/**
 *
 * @author owen
 */
public interface SBDomainBuilder extends SBActionBuilder {

    <T extends SBDomainBuilder> T addAction(SBAction action);
    
    <T extends SBDomainBuilder> T createEdge(URI source, URI predicate, URI target);

    <T extends SBDomainBuilder> T createObject(URI identity, URI rdfType, URI parentIdentity, URI parentRdfType);

    <T extends SBDomainBuilder> T createValue(URI source, URI predicate, SBValue value, URI sourceType);

    <T extends SBDomainBuilder> T duplicateObject(URI identity, URI[] fromContexts, boolean incrementIds);

    <T extends SBDomainBuilder> T removeEdge(URI source, URI predicate, URI target);

    <T extends SBDomainBuilder> T removeObject(URI identity, URI type, URI ownerIdentity, URI ownerType);

    <T extends SBDomainBuilder> T removeObjectAndDependants(URI identity, URI ownerIdentity);

    <T extends SBDomainBuilder> T removeValue(URI source, URI predicate, SBValue value, URI sourceType);
    
}
