/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;


import uk.ac.ncl.icos.synbad.api.domain.SBGraphEntityType;

/**
 *
 * @author owengilfellon
 */
public interface SBGraphEvent<D> extends SBEvent<D> {
    
    public SBGraphEntityType getGraphEntityType();

    
}
