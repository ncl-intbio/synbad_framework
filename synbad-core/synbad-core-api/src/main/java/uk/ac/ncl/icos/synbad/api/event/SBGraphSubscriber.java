/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

/**
 *
 * @author owengilfellon
 */
public interface SBGraphSubscriber<N, E> extends SBSubscriber {
    
    public void onNodeEvent(SBGraphEvent<N> e);
    
    public void onEdgeEvent(SBGraphEvent<E> e);

}
