/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;


import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author owengilfellon
 */
public interface SBDispatcher extends SBEventSource, Serializable {


    void publish(Collection<SBEvent> events);

    void publish(SBEvent event);

    void close();
    
}
