/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.workspace;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;

/**
 *
 * @author owengilfellon
 */
public interface SBIdentityFactory {
    
    public SBIdentity getUniqueIdentity(SBIdentity identity);
    
    public SBIdentity getIdentity(String prefix, String displayId);
    
    public SBIdentity getIdentity(String prefix, String displayId, String version);
    
    public SBIdentity getNextIdentity(String prefix, String displayId, String version);
    
    public SBIdentity getIdentity(String prefix, String parentDisplayId, String displayId, String version);
    
    public SBIdentity getNextIdentity(String prefix, String parentDisplayId, String displayId, String version);

    public SBIdentity getNextIdentity(SBIdentity identity);

    public SBIdentity getIdentity(URI identity);
    
}
