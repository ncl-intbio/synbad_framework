/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.rdf;

import java.net.URI;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

/**
 *
 * @author owengilfellon
 */
public interface SBEntityIdentifier {

    boolean test(String t, SBWorkspace u, URI[] contexts);
    
    class IdentifyByType implements SBEntityIdentifier {

         private final SBValue type;

         public IdentifyByType(URI type, Class clazz) {
             this.type = SBValue.parseValue(type);
         }

         @Override
         public boolean test(String t, SBWorkspace u, URI[] contexts) {
             if(t == null || t.isEmpty() || u == null)
                 return false;
             return !u.getRdfService().getStatements(t, "http://www.w3.org/1999/02/22-rdf-syntax-ns#type", type, contexts).isEmpty();
         }
     } 
}

