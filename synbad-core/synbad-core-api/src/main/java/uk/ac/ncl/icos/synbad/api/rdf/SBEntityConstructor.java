/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.rdf;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.net.URI;

/**
 *
 * @author owengilfellon
 */

public interface SBEntityConstructor<T> {

    public T apply(String t, SBWorkspace u, URI[] contexts);

}
    

