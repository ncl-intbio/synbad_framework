/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.actions;

import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;

import java.net.URI;

/**
 * Subclasses of {@link SBActionBuilder} should define methods that simplify
 * {@link SBAction} creation, and that return the same type of builder so method
 * calls can be chained. Once one or
 * more actions have been created using such methods, {@link #build()} is called
 * to retrieve a single action that encapsulates them.
 *
 * @author owengilfellon
 */
public interface SBActionBuilder {


    SBAction build(String label);

    SBAction build();
    
    boolean isBuilt();
    
    boolean isEmpty();
    
    SBWorkspace getWorkspace();
    
    URI[] getContexts();
    
    int getSize();

    <T extends SBActionBuilder> T getAs(Class<T> clazz);

}
