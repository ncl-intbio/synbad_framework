/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import uk.ac.ncl.icos.synbad.api.domain.SBEdge;
import java.net.URI;

/**
 *
 * @author owengilfellon
 */
public class SynBadEdge extends SBEdge.DefaultEdge<URI, URI> {

    private static final long serialVersionUID = -1852183708934804588L;
    private String string;
    private URI[] contexts;
    
    public SynBadEdge(URI uri, URI from, URI to, URI[] contexts) {
        super(uri, from, to);
        this.contexts = contexts;
    }

    public URI[] getContexts() {
        return contexts;
    }
    
    @Override
    public String toString() {
        if(string == null)
            string = "[" + getFrom().toASCIIString() + "] - " + getEdge().toASCIIString() + " -> [" + getTo().toASCIIString() + "]";
        return string;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
