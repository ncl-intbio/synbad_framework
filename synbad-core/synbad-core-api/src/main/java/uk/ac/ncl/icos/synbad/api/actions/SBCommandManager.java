/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.actions;

import java.util.Collection;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;

/**
 * SBCommandManager manages changes to state as a result of actions and events.
 * 
 * @author owengilfellon
 */
public interface SBCommandManager {
    
    void perform(SBAction action);
    
    void perform(Collection<SBEvent> events);;

    boolean hasUndo();
    
    boolean hasRedo();
    
    void undo();
    
    void redo();
    
}