/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.actions;

import java.net.URI;
import java.util.Collection;
import uk.ac.ncl.icos.synbad.api.event.SBEvent;

/**
 * Actions allow intended changes of state to be captured. 
 * 
 * Actions are initially performed using the {@link #perform(SBCommandManager) } 
 * method. This method should pass the events that capture the execution of the action
 * to the {@link SBCommandManager} {@code commander}.
 * 
 * Events can subsequently be retrieved using {@link #getEvents(boolean)}.
 * 
 * @author owengilfellon
 */
public interface SBAction {
    
    enum SBActionType {
        ADD,
        REMOVE,
        MODIFY
    }
    
    /**
     * 
     * @param commander the {@link SBCommandManager} that will execute the action
     * @return events that capture the execution of this action
     */
    Collection<SBEvent> perform(SBCommandManager commander);

    /**
     * 
     * @param undo
     * @return 
     */
    Collection<SBEvent> getEvents(boolean undo);
    
    URI[] getContexts();
}
