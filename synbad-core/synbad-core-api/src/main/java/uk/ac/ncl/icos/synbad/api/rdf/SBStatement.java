/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.rdf;

import java.util.Objects;
import uk.ac.ncl.icos.synbad.api.domain.SBValue;

/**
 *
 * @author owengilfellon
 */
public class SBStatement {

    private final String subject;
    private final String predicate;
    private final SBValue object;

    public SBStatement(String subject, String predicate, SBValue object) {
        
        if(subject == null)
            throw new NullPointerException("Subject cannot be null");
        
        if(predicate == null)
            throw new NullPointerException("Predicate cannot be null");
        
        if(object == null)
            throw new NullPointerException("Object cannot be null");
        
        this.subject = subject;
        this.predicate = predicate;
        this.object = object;
    }

    public String getSubject() {
        return subject;
    }

    public SBValue getObject() {
        return object;
    }

    public String getPredicate() {
        return predicate;
    }

    @Override
    public String toString() {
        return subject + " : " + predicate + " : " + object;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(!(obj instanceof SBStatement))
            return false;
        
        SBStatement s = (SBStatement) obj;
        return s.getSubject().equals(getSubject()) &&
                s.getPredicate().equals(getPredicate()) &&
                s.getObject().equals(getObject());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.subject);
        hash = 37 * hash + Objects.hashCode(this.predicate);
        hash = 37 * hash + Objects.hashCode(this.object);
        return hash;
    }
}
