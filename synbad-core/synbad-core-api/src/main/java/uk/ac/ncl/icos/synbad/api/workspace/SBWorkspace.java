/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.workspace;

import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SynBadEdge;
import uk.ac.ncl.icos.synbad.api.rdf.SBRdfService;
import uk.ac.ncl.icos.synbad.api.event.SBDispatcher;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.domain.SBValued;
import uk.ac.ncl.icos.synbad.api.rdf.SBStatement;

/**
 * The main interface for interacting with collections of valued objects and edges,
 * derived from RDF statements. An object represents multiple statements about 
 * a single subject that has an <code>rdf:type</code> recognised by a registered
 * factory.
 * 
 * @author owengilfellon
 */
public interface SBWorkspace extends SBEdgeWithContextsProvider<SBValued, SynBadEdge> {
     
    /**
     * 
     * @return the unique identifier of the workspace.
     */
    URI getIdentity();
    
    /**
     * @return the contexts present in the workspace. Workspaces can be thought
     * of as named graphs, and can be used to organise statements in the workspace.
     */
    URI[] getContextIds();
    
    /**
     * @param contexts the contexts for which to retrieve the associated system
     * context IDs.
     * @return the systems contexts used to store internal statements.
     */
    URI[] getSystemContextIds(URI... contexts);

    URI[] getDefaultContextIds(URI... systemContexts);


    void createContext(URI context);
   
    void removeContext(URI context);
    
    /**
     * Removes all subscribers, closes the RDF service and then closes the workspace.
     */
    void shutdown();
    
    // ============ Identities ============
    
    /**
     * 
     * @param identity
     * @param contexts
     * @return true if the workspace contains an object with the provided identity
     * in the provided contexts. An object in this case requires an <code>rdf:type</code>
     * statement, but does not require a registered domain object factory.
     */
    boolean containsIdentity(URI identity, URI[] contexts);

    /**
     * 
     * @param contexts
     * @returnan all identity objects for objects in the workspace in the 
     * provided contexts. An object in this case requires an <code>rdf:type</code>
     * statement, but does not require a registered domain object factory.
     */
    Set<SBIdentity> getIdentities(URI[] contexts);

    /**
     * 
     * @param persistentIdentity
     * @param contexts
     * @return all identity objects for objects in the workspace with the 
     * provided persistent identity in the provided contexts. An object in this
     * case requires an <code>rdf:type</code> statement, but does not require a
     * registered domain object factory.
     */
    Set<SBIdentity> getIdentities(URI persistentIdentity, URI[] contexts);

    // ============ Objects =============

     /**
      * 
      * @param <T> 
      * @param identity
      * @param clazz
      * @param contexts
      * @return true if the workspace contains an object of class <code>T</code>
      * with the provided identity in the provided contexts.
      */
     <T extends SBValued> boolean containsObject(URI identity, Class<T> clazz, URI[] contexts);
    
    
    Collection<Class<? extends SBValued>> getObjectClasses();
    
    /**
     * 
     * @param identity
     * @param contexts
     * @return all domain object classes that the provided identity in the provided
     * contexts can be parsed to.
     */
    Collection<Class<? extends SBValued>> getObjectClasses(URI identity, URI[] contexts);
    
    /**
     * 
     * @param <T>
     * @param identity
     * @param clazz
     * @param contexts
     * @return an object of the provided class, if the identity in the provided
     * contexts can be parsed to that class. Otherwise, returns null.
     */
    <T extends SBValued> T getObject(URI identity, Class<T> clazz, URI[] contexts);
    
    /**
     * 
     * @param <T>
     * @param clazz
     * @param contexts
     * @return All entities in the workspace that can be parsed into the provided
     * domain object class.
     */
    <T extends SBValued> Collection<T> getObjects(Class<T> clazz, URI[] contexts);
    
    /**
     * 
     * @param <T>
     * @param identity
     * @param clazz
     * @param contexts
     * @return true, if there exists statements with the provided subject in the
     * workspace in the provided contexts, that can be parsed into the provided
     * domain object class.
     */
    <T extends SBValued> boolean isObject(URI identity, Class<T> clazz, URI[] contexts);
    
    boolean isEdge(SBStatement statement, URI[] contexts);

    // ============ Graph methods ============
    
    /**
     * 
     * @param <T> the type of the edge's source
     * @param <V> the type of the edge's target
     * @param subject identifies the source object of the edges.
     * @param predicate identified the predicate of the edges. May be null, which
     * is treated as a wildcard.
     * @param objectClass identified the class, or superclass, of the target of
     * the edge. To retrieve edges to all potential objects, can pass in
     * <code>SBValued.class</code>
     * @param contexts the contexts from which to retrieve the edges
     * @return 
     */
    <T extends SBValued, V extends SBValued> Collection<SynBadEdge> outgoingEdges(T subject, String predicate, Class<V> objectClass, URI[] contexts);

    /**
     * 
     * @param <V> the type of the edge's source
     * @param target identifies the target object of the edges.
     * @param predicate identified the predicate of the edges. May be null, which
     * is treated as a wildcard.
     * @param objectClass identified the class, or superclass, of the source of
     * the edge. To retrieve edges to all potential objects, can pass in
     * <code>SBValued.class</code>
     * @param contexts the contexts from which to retrieve the edges
     * @return 
     */
    <V extends SBValued> List<SynBadEdge> incomingEdges(SBValued target, String predicate, Class<V> objectClass, URI[] contexts);

    // ============ Commands and Events ============
    
    
    void perform(SBAction command);
    
    void undo();
    
    void redo();
    
    // ============ Services ============
    
    SBDispatcher getDispatcher();
    
    SBRdfService getRdfService();
    
    //public SBWorkspaceValidator getValidator();
    
    SBIdentityFactory getIdentityFactory();
  

}
