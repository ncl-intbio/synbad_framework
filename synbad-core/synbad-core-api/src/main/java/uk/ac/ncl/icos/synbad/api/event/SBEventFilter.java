/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

import java.io.Serializable;
import java.util.function.Predicate;

/**
 *
 * @author owengilfellon
 */
public interface SBEventFilter extends Serializable, Predicate<SBEvent> {
    
    public boolean test(SBEvent event);
    
    public static class DefaultFilter implements SBEventFilter {

        @Override
        public boolean test(SBEvent event) {
            return true;
        }

        @Override
        public boolean equals(Object obj) {
             if(obj == null)
                 return false;
             
             if(obj == this)
                 return true;
             
             if(obj instanceof DefaultFilter)
                 return true;
             
             return false;
        }

        @Override
        public int hashCode() {
            int hash = 912389128;
            return hash;
        }
        
    }
    
}
