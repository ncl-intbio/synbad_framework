/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

/**
 *
 * @author owengilfellon
 */
public class SBIdentityException extends Exception{

    public SBIdentityException(String message) {
        super(message);
    }

    public SBIdentityException(Throwable cause) {
        super(cause);
    }

    public SBIdentityException(String message, Throwable cause) {
        super(message, cause);
    }

}
