/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author owengilfellon
 */
public interface SBSubscriber {
    
    public void onEvent(SBEvent e);

    public class MessageLogger implements SBSubscriber {
        
        private static final Logger logger = LoggerFactory.getLogger(MessageLogger.class);

        @Override
        public void onEvent(SBEvent e) {
            logger.info(e.toString());
        }
    }
}
