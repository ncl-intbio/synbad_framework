/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author owengilfellon
 */
public class SBIdentityHelper {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SBIdentityHelper.class);
    
    // [1]: ns [2]: nsSeperator [3]:fullDisplayId [6]:displayIdOfParent [7]: childSeperator [8]:displayIdOfChild [10]: versionSeperator [11]: version
   
    private static final String ID_REXGEX = "^([a-z0-9+.-]+:\\/\\/?[\\da-z\\.-]+\\.[a-z\\.]{2,6})(\\/)(([\\w-_]*)|(([\\w-_]+)([#\\/:]?))*?([\\w-_]+)?)(([\\/#:])+([\\d][\\d\\w\\.-]*))?$";
    private static final Pattern ID_PATTERN = Pattern.compile(ID_REXGEX);
    
    private static final String VERSION_REGEX = "([\\d-A-Za-z]+[-|.])?([\\d]+)($|[-A-Za-z]+)$";
    private static final Pattern VERSION_PATTERN = Pattern.compile(VERSION_REGEX);
    
    // SBIdentity pattern that specifically captures an incrementing displayId (e.g. _123)
    
    private static final String INC_DISPLAYID_REGEX = "^([a-zA-Z0-9/\\-_]+)?(?:_([\\d]+))$";
    private static final Pattern INC_DISPLAYID_PATTERN = Pattern.compile(INC_DISPLAYID_REGEX);
    
    private static final LoadingCache<String, URI> uriCache = CacheBuilder.newBuilder()
        .expireAfterAccess(20, TimeUnit.MINUTES)
        .build(
            new CacheLoader<String, URI>() {
                @Override
                public URI load(String s) throws Exception {
                    try {
                        return new URI(s);
                    } catch(URISyntaxException ex) {
                        throw ex;
                    }
                }
            }
        );
    
    public static URI getURI(String s) {
        try {
            return uriCache.get(s);
        } catch (ExecutionException ex) {
           // LOGGER.error("Could not get URI: {}", s, ex);
            return null;
        }
    }

    public static boolean isValidId(URI id) {
        return ID_PATTERN.matcher(id.toASCIIString()).matches();
    }
    
    public static boolean isValidId(String id) {
        return ID_PATTERN.matcher(id).matches();
    }
    
    public static boolean isValidChildPersistentId(URI id) {
        return !getChildDisplayIdFromId(id).isEmpty();
    }
    
    public static String getBaseDisplayIdFromIncrementing(String displayId) {
        
        Matcher matcher = INC_DISPLAYID_PATTERN.matcher(displayId);

        if (matcher.matches()) {
            String baseDisplayId = matcher.group(1);
            String toReturn = baseDisplayId == null ? displayId : baseDisplayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning base displayId: [{}] for id: [{}]", toReturn, displayId);
            return toReturn;
        }  
        
        return displayId;
    }
    
    public static int getIncrementFromIncrementing(String displayId) {
        
        Matcher matcher = INC_DISPLAYID_PATTERN.matcher(displayId);

        if (matcher.matches()) {
            String increment = matcher.group(2);
            int toReturn = increment == null ? 0 : Integer.parseInt(increment);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning increment: [{}] for id: [{}]", toReturn, displayId);
            return toReturn;
        }  
        
        return 0;
    }
    
    public static String incrementVersion(String version) {
        Matcher matcher = VERSION_PATTERN.matcher(version);
        if(!matcher.matches()) {
            LOGGER.warn("Version does not match version pattern: {}", version);
            return null;
        }
       
        String prefix = matcher.group(1);
        String digits = matcher.group(2);
        String postfix = matcher.group(3);
        
        if(digits == null || digits.isEmpty()) {
            LOGGER.warn("Version does not match version pattern: {}", version);
            return null;
        }
        
        try {
            int intValue =  Integer.parseInt(digits) + 1;
            return prefix + intValue + postfix;
        } catch(NumberFormatException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
    }

    public static String getPrefixFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String prefix = matcher.group(1);
            String toReturn = prefix == null ? new String() : prefix;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning prefix: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        }  

        return new String();
    }

    public static String getDisplayIdFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(8);
            if(displayId == null || displayId.isEmpty())
                displayId = matcher.group(4);
            String toReturn = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning displayId: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        } else {
            LOGGER.warn("Could not get display ID from {}", id.toASCIIString());
        }      
        
        return new String();
    }
    
    public static String getParentDisplayIdFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            
            String displayId = matcher.group(6);
            String toReturn =  displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning parent displayId: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        } else {
            LOGGER.warn("Could not get parent display ID from {}", id.toASCIIString());
        }     
        
        return new String();
    }
    
    public static String getChildDisplayIdFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(8);
            String toReturn =  displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning child displayId: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        } else {
            LOGGER.warn("Could not get child display ID from {}", id.toASCIIString());
        }
        
        return new String();
    }
    
    public static String getNamespaceSeperatorFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(2);
            String toReturn  = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning namespace seperator: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        }      
        
        return new String();
    }
    
    public static String getChildSeperatorFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(7);
            String toReturn = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning child seperator: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        }      
        
        return new String();
    }
    
    public static String getVersionSeperatorFromId(URI id) {
      
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(10);
            String toReturn = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning version seperator: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        }      
        
        return new String();
    }
  
    public static String getVersionFromId(String id) {
        Matcher matcher = ID_PATTERN.matcher(id);

        if (matcher.matches()) {
            String displayId = matcher.group(11);
            String toReturn = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning version: [{}] for id: [{}]", toReturn, id);
            return toReturn;
        }      
        
        return new String();
    }
    
    public static String getVersionFromId(URI id) {
        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(11);
            String toReturn = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning version: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        }      
        
        return new String();
    }
    
    public static String getPersistentIdFromId(URI id) {


        Matcher matcher = ID_PATTERN.matcher(id.toASCIIString());

        if (matcher.matches()) {
            String toReturn  = matcher.group(1) + matcher.group(2) + matcher.group(3);
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning persistentId: [{}] for id: [{}]", toReturn, id.toASCIIString());
            return toReturn;
        }      
        
        return new String();
    }
    
    public static String getDisplayIdFromTopLevelPersistentId(URI persistentId) {
        Matcher matcher = ID_PATTERN.matcher(persistentId.toASCIIString());

        if (matcher.matches()) {
            String displayId = matcher.group(3);
            String toReturn = displayId == null ? new String() : displayId;
            if(LOGGER.isTraceEnabled())
                LOGGER.trace("Returning displayId: [{}] for top level persistentid: [{}]", toReturn, persistentId.toASCIIString());
            return toReturn;
        }      
        
        return new String();
    }
    
    public static String getParentChildDisplayId(SBIdentity id) {
        return id.getPersistentId().toASCIIString().substring(id.getUriPrefix().length() + 1).replaceAll("/", "_");
    }
    
}
