/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.event;

/**
 *
 * @author owengilfellon
 */
    
public enum SBGraphEventType implements SBEventType {

    ADDED,
    MODIFIED,
    REMOVED,
    VISIBLE,
    HIDDEN;
    
    @Override
    public String getType() {
        return name();
    }
}
