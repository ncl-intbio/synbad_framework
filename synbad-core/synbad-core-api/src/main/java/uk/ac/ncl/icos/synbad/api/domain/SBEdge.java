/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.api.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * The base class for all edges within SynBad.
 * @author owengilfellon
 * @param <N> The type of the objects that the edge joins.
 * @param <E> The type of the data associated with this edge, for example, a
 * label or predicate.
 */
public interface SBEdge<N, E> extends SBEntity, Serializable {

    /**
     * 
     * @return the source of this edge.
     */
    public N getFrom();
    
    /**
     * 
     * @return the target of this edge.
     */
    public N getTo();
    
    /**
     * 
     * @return the data associated with this edge, commonly a <code>URI</code>.
     */
    public E getEdge();

    public class DefaultEdge<N, E>  implements SBEdge<N, E> {

        private static final long serialVersionUID = 8642169151789903451L;
        private final N from;
        private final N to;
        private final E edge;

        public DefaultEdge(E edge, N from, N to) {
            this.edge = edge;
            this.from = from;
            this.to = to;
        }

        @Override
        public E getEdge() {
            return edge;
        }
        
        @Override
        public N getFrom() {
            return from;
        }

        @Override
        public N getTo() {
            return to;
        }
        
        @Override
        public boolean equals(Object obj) {
            if(obj == null)
                 return false;
            if(obj == this)
                 return true;
            if(!(DefaultEdge.class.isAssignableFrom(obj.getClass())))
                 return false;

            DefaultEdge node = (DefaultEdge) obj;
            return node.getEdge().equals(edge) &&
                    node.getFrom().equals(from) &&
                    node.getTo().equals(to);
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 7 * hash + Objects.hashCode(edge);
            hash = 19 * hash + Objects.hashCode(from);
            hash = 53 * hash + Objects.hashCode(to);
            return hash;
        }
    }
}
