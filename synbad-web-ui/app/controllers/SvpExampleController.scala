package controllers

import java.net.URI

import controllers.json.{SvpModelJson, ValuedObj}
import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace
import uk.ac.ncl.icos.synbad.svp.example.{ExampleFactory, ExampleModuleFactory}
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule

import scala.util.Try

@Singleton
class SvpExampleController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {

  def requestToWorkspace(json: JsValue): Option[SBWorkspace] = {
    (json \ "workspace").asOpt[String]
      .flatMap(uri => Try(URI.create(uri)).toOption)
      .map(uri => ctx.getWorkspaceManager().getWorkspace(uri))
  }

  def createAutoRegulation = Action { request =>

    val workspace = requestToWorkspace(request.body.asJson.get)
    if(workspace.isEmpty)
      BadRequest("Workspace must be specified.")

    val ws = workspace.get
    val model:SvpModel = ExampleModuleFactory.createAutoRegulation(ws, ws.getContextIds)
     Ok(Json.obj(
      "workspace" -> ws.getIdentity.toASCIIString,
      "model" -> Json.toJson(SvpModelJson.parse(model))))
  }

  def createSubtilinReceiver= Action { request =>

    val workspace = requestToWorkspace(request.body.asJson.get)
    if(workspace.isEmpty)
      BadRequest("Workspace must be specified.")

    val ws = workspace.get
    val model:SvpModule = ExampleFactory.setupWorkspace(ws, Array.apply(URI.create("http://synbad.org/subtilinReceiverContext/1.0")))
    Ok(Json.obj(
      "workspace" -> ws.getIdentity.toASCIIString,
      "model" -> Json.toJson(ValuedObj.parse(model))))
  }
}
