package controllers

import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.mvc._


@Singleton
class ModelController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {
/*
  def createModel = Action { request =>
    val json:JsValue = request.body.asJson.get
    val workspace = (json \ "workspace").asOpt[String].map(ws => URI.create(ws))
    val id = (json \ "identity").asOpt[String].map(ws => URI.create(ws))
    if(workspace.isEmpty)
      BadRequest("workspace must be specified, and be a valid URI.")
    else if(id.isEmpty)
      BadRequest("identity must be specified, and be a valid URI.")
    else {
      val ws = ctx.getWorkspaceManager().getWorkspace(workspace.get)
      val obj = Option(ws.getObject(id.get, classOf[SvpModule], ws.getContextIds))
      if(obj.isEmpty)
        BadRequest(s"Could not retrieve an SvpModule for ${id.get.toASCIIString}")
      obj.map(o => ctx.getModelManager().createModel(classOf[SvpModel], o))
        .map(m => Ok(Json.obj("models" -> Json.arr(Json.toJson(SvpModelJson.parse(m))))))
        .getOrElse(InternalServerError("Could not retrieve model."))
    }
  }

  def getModel = Action { request =>
    val json:JsValue = request.body.asJson.get
    val modelId = (json \ "modelId").asOpt[Long]
    modelId.map(id => ctx.getModelManager().createModel(classOf[SvpModel], id))
      .map(m => Ok(Json.obj(
        "models" -> Json.arr(Json.toJson(SvpModelJson.parse(m))))))
      .getOrElse(BadRequest)
//      .getOrElse({
//        val models = ctx.getModelManager()..getModels(classOf[SvpModel]).asScala
//        Ok(Json.obj("models" -> Json.toJson(
//          models.map(mod => SvpModelJson.parse(mod)))))
//      });
  }*/
}
