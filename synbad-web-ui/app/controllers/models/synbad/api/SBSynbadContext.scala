package controllers.models.synbad.api

import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager
import uk.ac.ncl.icos.synbad.model.SBModelManager
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager

trait SBSynbadContext {

  def getWorkspaceManager():SBWorkspaceManager

  def getObjectFactoryManager(): SBObjectFactoryManager

  def getDataDefinitionManager(): SBDataDefManager

  def getModelManager(): SBModelManager

}


