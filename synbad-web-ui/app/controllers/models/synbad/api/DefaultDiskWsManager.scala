package controllers.models.synbad.api

import uk.ac.ncl.icos.synbad.datadefinition.SBDataDefManager
import uk.ac.ncl.icos.synbad.model.{DefaultSBModelFactory, SBModelManager}
import uk.ac.ncl.icos.synbad.workspace.{DefaultSBWorkspaceManagerDisk, SBWorkspaceManager}
import uk.ac.ncl.icos.synbad.workspace.objects.SBObjectFactoryManager

class DefaultDiskWsManager extends SBSynbadContext {

  var dataDefManager: Option[SBDataDefManager] = None
  var objFacManager: Option[SBObjectFactoryManager] = None
  var wsManager: Option[SBWorkspaceManager] = None
  var modelManager: Option[SBModelManager] = None

  override def getDataDefinitionManager(): SBDataDefManager = {
    if(dataDefManager.isEmpty)
      dataDefManager = Some(SBDataDefManager.getManager)
    dataDefManager.get
  }

  override def getWorkspaceManager(): SBWorkspaceManager = {
    if(wsManager.isEmpty)
      wsManager = Some(new DefaultSBWorkspaceManagerDisk)
    wsManager.get
  }

  override def getObjectFactoryManager(): SBObjectFactoryManager = {
    if(objFacManager.isEmpty)
      objFacManager = Some(new SBObjectFactoryManager)
    objFacManager.get
  }

  override def getModelManager(): SBModelManager = {
    if(modelManager.isEmpty)
      modelManager = Some(new DefaultSBModelFactory)
    modelManager.get
  }
}
