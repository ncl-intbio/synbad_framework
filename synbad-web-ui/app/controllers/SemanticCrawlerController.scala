package controllers

import java.net.URI

import com.owengilfellon.synbad.data.semanticwiki.obj.SemanticWikiObj
import com.owengilfellon.synbad.data.semanticwiki.{DefaultSemanticWikiCrawler, SemanticWikiCrawler}
import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace

import scala.collection.JavaConverters._

@Singleton
class SemanticCrawlerController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {

  private val DEFAULT_CONTEXT:URI = URI.create("http://wwww.owengilfellon.com/defaultContext")

  def crawl = Action { request =>

    val json = request.body.asJson.get
    val workspace = (json \ "workspace").as[String]
    val start = (json \ "id").as[String]
    var maxCount = if ((json \ "count").isDefined)  (json \ "count").as[Int] else 10

    if(workspace == null)
      BadRequest("Workspace must be specified.")

    val wsUri: URI = URI.create(workspace)

    if(start == null)
      BadRequest("Start ID must be specified.")

    val startUri: URI = URI.create(start)


    val crawler: SemanticWikiCrawler = DefaultSemanticWikiCrawler

    if(crawler == null)
      InternalServerError("Could not retrieve wiki crawler")


    val ws:SBWorkspace = ctx.getWorkspaceManager().getWorkspace(wsUri)
    crawler.setWorkspace(ws)
    val crawled:List[String] = crawler.crawl(startUri, maxCount, DEFAULT_CONTEXT).asScala.map(c => c.getIdentity.toASCIIString).toList


    Ok(Json.obj(
      "crawled" -> Json.toJson(crawled)
    ))
  }

  def resolve = Action { request =>
    val json = request.body.asJson.get
    val workspace = if ((json \ "workspace").isDefined) (json \ "workspace").as[String] else null
    val id = if  ((json \ "id").isDefined) (json \ "id").as[String] else null
    val objType = if  ((json \ "type").isDefined) (json \ "type").as[String] else null

    if(workspace == null)
      BadRequest("Workspace must be specified.")

    val wsUri: URI = URI.create(workspace)

    if(id == null)
      BadRequest("ID to resolve must be specified.")


    val startUri: URI = URI.create(id)

    val crawler: SemanticWikiCrawler = new DefaultSemanticWikiCrawler

    if(crawler == null)
      InternalServerError("Could not retrieve wiki crawler")

    val ws:SBWorkspace = ctx.getWorkspaceManager().getWorkspace(wsUri)
    val objectType:URI = if (objType == null) null else URI.create(objType)
    val objectClass:Class[_ <: SemanticWikiObj] = if (objectType == null) classOf[SemanticWikiObj] else ctx.getObjectFactoryManager().getClassFromType(objectType).asInstanceOf[Class[ _ <: SemanticWikiObj]]
    crawler.setWorkspace(ws)

    val v: SemanticWikiObj = crawler.resolve(objectClass, startUri, DEFAULT_CONTEXT)

    Ok(Json.obj(
      "result" -> asJson(v, ws)
    ))
  }
}
