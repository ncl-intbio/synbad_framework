package controllers

import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._

@Singleton
class HomeController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {

  def appSummary = Action {
    Ok(Json.obj("content" -> ctx.getWorkspaceManager().listWorkspaces().size()))
  }
}
