package controllers

import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.mvc._


@Singleton
class ViewController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {
/*
  def createView = Action { request =>
    val json:JsValue = request.body.asJson.get
    val modelId = (json \ "modelId").asOpt[Long]
    if(modelId.isEmpty)
      BadRequest("modelId must be specified")
    else {
      modelId.map(id => ctx.getModelManager().createModel(classOf[SvpModel], id))
        .map(m => m.createView())
        .map(v => Ok(Json.obj("view" -> Json.toJson(ViewJson.parse(v)))))
        .getOrElse(InternalServerError("Could not create view."))
    }
  }

  def getView = Action { request =>
    val json:JsValue = request.body.asJson.get
    val modelId = (json \ "modelId").asOpt[Long]
    val viewId = (json \ "viewId").asOpt[Long]
    if(modelId.isEmpty)
      BadRequest("modelId must be specified")
    else if(viewId.isEmpty)
      BadRequest("viewId must be specified")
    else {
      val model = Option(ctx.getModelManager().createModel(classOf[SvpModel], modelId.get))
      model.flatMap(m => m.getViews.asScala.find(v => v.getId == viewId.get))
        .map(v => Ok(Json.obj("view" -> Json.toJson(ViewJson.parse(v)))))
        .getOrElse(InternalServerError("Could not create view."))
    }
  }*/
}
