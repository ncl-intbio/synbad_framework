package controllers.json

import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Writes, _}
import uk.ac.ncl.icos.synbad.api.domain.SBValue // Combinator syntax

case class PredicateValue(predicate: String, value: SBValue)

object PredicateValue {

  implicit val predicateValueWrites: Writes[PredicateValue] = (
    (JsPath \ "predicate").write[String] and
      (JsPath \ "value").write[SBValue]) (unlift(PredicateValue.unapply))

  implicit val predicateValueReads: Reads[PredicateValue] = (
    (JsPath \ "predicate").read[String] and
      (JsPath \ "value").read[SBValue]) (PredicateValue.apply _)

}
