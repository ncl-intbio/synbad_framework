package controllers.json

import java.net.URI

import play.api.libs.functional.syntax.unlift
import play.api.libs.json.{JsPath, Reads, Writes}
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import uk.ac.ncl.icos.synbad.api.domain.SBEdge
import uk.ac.ncl.icos.synbad.graph.{SBGlobalEdgeProvider, SBGlobalNodeProvider}
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel
import uk.ac.ncl.icos.synbad.view.SBView
import uk.ac.ncl.icos.synbad.view.`object`.{SBViewEdge, SBViewIdentified}

import collection.JavaConverters._

case class ViewJson(
              id: Long,
              nodes: Seq[ViewObj],
              edges: Seq[ViewE],
              modelId: Long)

object ViewJson {

  def parse[T <:  SBView[_,_]](obj: T): ViewJson = new ViewJson(
    obj.getId,
    obj.asInstanceOf[SBGlobalNodeProvider[_]].nodeSet().asScala.map(n => n.asInstanceOf[SBViewIdentified[_]]).map(n => ViewObj.parse(n)).toSeq,
    obj.asInstanceOf[SBGlobalEdgeProvider[_, _]].edgeSet().asScala.map(e => e.asInstanceOf[SBViewEdge[_ <: SBViewIdentified[_], _, _ <: SBEdge[_, _ <: URI]]])
      .map(e => ViewE.parse(e)).toSeq,
    obj.getModel[SvpModel].getId
  )

  implicit val viewJsonWrites: Writes[ViewJson] = (
      (JsPath \ "id").write[Long] and
      (JsPath \ "nodes").write[Seq[ViewObj]] and
      (JsPath \ "edges").write[Seq[ViewE]] and
      (JsPath \ "modelId").write[Long]) (unlift(ViewJson.unapply))

  implicit val viewJsonReads: Reads[ViewJson] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "nodes").read[Seq[ViewObj]] and
      (JsPath \ "edges").read[Seq[ViewE]] and
      (JsPath \ "modelId").read[Long]) (ViewJson.apply _)
}

