package controllers.json

import java.net.URI

import play.api.libs.functional.syntax.unlift
import play.api.libs.json.{JsPath, Reads, Writes}
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import uk.ac.ncl.icos.synbad.api.domain.SBEdge
import uk.ac.ncl.icos.synbad.view.`object`.{SBViewEdge, SBViewIdentified}

case class ViewE(
              from: ViewObj,
              to: ViewObj,
              edge: URI)


object ViewE {
  def parse(edge: SBViewEdge[_ <: SBViewIdentified[_], _, _ <: SBEdge[_, _ <: URI]]): ViewE = new ViewE(
    ViewObj.parse(edge.getFrom),
    ViewObj.parse(edge.getTo),
    edge.getEdge.getEdge)

  implicit val viewEWrites: Writes[ViewE] = (
    (JsPath \ "from").write[ViewObj] and
      (JsPath \ "to").write[ViewObj] and
      (JsPath \ "edge").write[URI] ) (unlift(ViewE.unapply))

  implicit val viewEReads: Reads[ViewE] = (
    (JsPath \ "from").read[ViewObj] and
      (JsPath \ "to").read[ViewObj] and
      (JsPath \ "edge").read[URI] ) (ViewE.apply _)
}


