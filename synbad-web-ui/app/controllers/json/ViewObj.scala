package controllers.json

import java.net.URI

import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Reads, Writes}
import uk.ac.ncl.icos.synbad.api.domain.SBIdentified
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel
import uk.ac.ncl.icos.synbad.svp.view.SvpPView
import uk.ac.ncl.icos.synbad.view.`object`.SBViewIdentified


case class ViewObj(
              id: Long,
              viewId: Long,
              modelId: Long,
              objId: URI,
              objType: URI,
              workspaceId: URI,
              contexts: Seq[URI])


object ViewObj {

  def parse(obj: SBViewIdentified[_ <: SBIdentified]): ViewObj = new ViewObj(
    obj.getId,
    obj.getView[SvpPView].getId,
    obj.getView[SvpPView].getModel[SvpModel].getId,
    obj.getIdentity,
    obj.getObject.getType,
    obj.getWorkspace.getIdentity,
    obj.getContexts
  )

  implicit val viewObjWrites: Writes[ViewObj] = (
    (JsPath \ "id").write[Long] and
    (JsPath \ "viewId").write[Long] and
    (JsPath \ "modelId").write[Long] and
    (JsPath \ "objectId").write[URI] and
    (JsPath \ "objectType").write[URI] and
    (JsPath \ "workspaceId").write[URI] and
    (JsPath \ "contexts").write[Seq[URI]]) (unlift(ViewObj.unapply))

  implicit val viewObjReads: Reads[ViewObj] = (
    (JsPath \ "id").read[Long] and
    (JsPath \ "viewId").read[Long] and
    (JsPath \ "modelId").read[Long] and
    (JsPath \ "objectId").read[URI] and
    (JsPath \ "objectType").read[URI] and
    (JsPath \ "workspaceId").read[URI] and
    (JsPath \ "contexts").read[Seq[URI]]) (ViewObj.apply _)
}
