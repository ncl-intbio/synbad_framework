package controllers.json

import play.api.libs.functional.syntax.{unlift, _}
import play.api.libs.json.Reads._
import play.api.libs.json.{JsPath, Writes, _}
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel

import scala.collection.JavaConverters._

case class SvpModelJson(
                      id: Long,
                      rootNode: ValuedObj,
                      rootViewNode: ViewObj,
                      views: Seq[ViewJson])

object SvpModelJson {

  def parse(obj: SvpModel): SvpModelJson= new SvpModelJson(
    obj.getId,
    ValuedObj.parse(obj.getRootNode),
    ViewObj.parse(obj.getViewRoot),
    obj.getViews.asScala.map(v => ViewJson.parse(v)).toSeq)

  implicit val svpModelWrites: Writes[SvpModelJson] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "rootNode").write[ValuedObj] and
      (JsPath \ "rootViewNode").write[ViewObj] and
      (JsPath \ "views").write[Seq[ViewJson]]) (unlift(SvpModelJson.unapply))

  implicit val svpModelReads: Reads[SvpModelJson] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "rootNode").read[ValuedObj] and
      (JsPath \ "rootViewNode").read[ViewObj] and
      (JsPath \ "views").read[Seq[ViewJson]]) (SvpModelJson.apply _)
}
