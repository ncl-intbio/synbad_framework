package controllers

import java.net.URI

import play.api.libs.json._
import uk.ac.ncl.icos.synbad.api.domain.SBValue

package object json {


  implicit val uriWriter = new Writes[URI] {
    def writes(uri: URI): JsValue = Json.toJson(uri.toASCIIString)
  }

  implicit val uriReader = new Reads[URI] {
    def reads(uri: JsValue): JsResult[URI] = JsSuccess(URI.create(uri.as[String]))
  }

  // ID Reads & Writes
  implicit val sbvWriter = new Writes[SBValue] {
    def writes(value: SBValue): JsValue = if(value.isBoolean)
      Json.toJson(value.asBoolean())
    else if(value.isDouble)
      Json.toJson(value.asDouble())
    else if(value.isInt)
      Json.toJson(value.asInt())
    else if(value.isURI)
      Json.toJson(value.asURI().toASCIIString)
    else
      Json.toJson(value.asString())
  }

  implicit val sbvReader = new Reads[SBValue] {
    def reads(value: JsValue): JsResult[SBValue] = JsSuccess(SBValue.parseValue(value.as[String]))
  }
}
