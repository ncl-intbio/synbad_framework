package controllers.json

import java.net.URI

import play.api.libs.functional.syntax.unlift
import play.api.libs.json.{JsPath, Writes}
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import uk.ac.ncl.icos.synbad.api.domain.{SBValue, SBValued}

import collection.JavaConverters._

case class ValuedObj(
              identity: URI,
              objectType: URI,
              workspaceId: URI,
              values: Map[String, SBValue],
              outgoingEdges: Map[String, SBValue],
              incomingEdges: Map[String, SBValue])

object ValuedObj {

  def parse(obj: SBValued): ValuedObj = {
    val ws = obj.getWorkspace
    new ValuedObj(
      obj.getIdentity,
      obj.getType,
      ws.getIdentity,
      obj.getPredicates.asScala.flatMap(p => obj.getValues(p).asScala.map(v => (p, v))).toMap,
      ws.outgoingEdges(obj, obj.getContexts).asScala.map(e => (e.getEdge.toASCIIString, SBValue.parseValue(e.getTo))).toMap,
      ws.incomingEdges(obj, obj.getContexts).asScala.map(e => (e.getEdge.toASCIIString, SBValue.parseValue(e.getTo))).toMap)
  }

  implicit val valuedObjWrites: Writes[ValuedObj] = (
    (JsPath \ "identity").write[URI] and
      (JsPath \ "objectType").write[URI] and
      (JsPath \ "workspaceId").write[URI] and
      (JsPath \ "values").write[Map[String, SBValue]] and
      (JsPath \ "outgoingEdges").write[Map[String, SBValue]] and
      (JsPath \ "incomingEdges").write[Map[String, SBValue]]) (unlift(ValuedObj.unapply))

  implicit val valuedObjReads: Reads[ValuedObj] = (
    (JsPath \ "identity").read[URI] and
      (JsPath \ "objectType").read[URI] and
      (JsPath \ "workspaceId").read[URI] and
      (JsPath \ "values").read[Map[String, SBValue]] and
      (JsPath \ "outgoingEdges").read[Map[String, SBValue]] and
      (JsPath \ "incomingEdges").read[Map[String, SBValue]]) (ValuedObj.apply _)



}
