import play.api.libs.json._
import uk.ac.ncl.icos.synbad.api.domain.{SBValue, SBValued}
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace

import scala.collection.JavaConverters._

package object controllers {

  def asJson(valued: SBValued, ws:SBWorkspace):JsValue = {
    val valuedObj: Map[String, JsValue]= valued.getPredicates.asScala
      .map(p => valued.getValues(p).asScala
        .foldLeft(Map[String, JsValue]())((obj, v) => obj + ((p, asJson(v)))))
      .foldLeft( Map(
        "identity" -> Json.toJson(valued.getIdentity.toASCIIString),
        "contextId" -> Json.arr(valued.getContexts.map(c => Json.toJson(c.toASCIIString))))
      ) ((vo, m) => vo ++ m)

    val outgoing:JsValue = Json.toJson(ws.outgoingEdges(valued, valued.getContexts).asScala
      .foldLeft(Map[String, JsValue]())((valObj, edge) => valObj + ((edge.getEdge.toASCIIString, Json.toJson(edge.getTo.toASCIIString)))))

    Json.toJson(valuedObj + (("outgoing", outgoing)))
  }

  def asJson( value: SBValue):JsValue =
    if(value.isBoolean)
      Json.toJson(value.asBoolean())
    else if(value.isDouble)
      Json.toJson(value.asDouble())
    else if(value.isInt)
      Json.toJson(value.asInt())
    else if(value.isURI)
      Json.toJson(value.asURI().toASCIIString)
    else
      Json.toJson(value.asString())
}


