package controllers

import java.net.URI

import controllers.json.ValuedObj
import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import uk.ac.ncl.icos.synbad.api.domain.SBValued

import scala.collection.JavaConverters._
import scala.util.Try

@Singleton
class WorkspaceController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {

  def getObjects = Action { request =>

    val json:JsValue = request.body.asJson.get

    (json \ "workspace").asOpt[String]
      .flatMap(uri => Try(URI.create(uri)).toOption)
      .map(uri => ctx.getWorkspaceManager().getWorkspace(uri)).map(ws => {
        val objectClass = (json \ "type").asOpt[String]
          .flatMap(s => Try(URI.create(s)).toOption)
          .map(t => ctx.getObjectFactoryManager().getClassFromType(t)).getOrElse(classOf[SBValued])

        val objects = (json \ "identity").asOpt[String]
          .flatMap(s => Try(URI.create(s)).toOption)
          .map(uri => List(ws.getObject(uri, objectClass, ws.getContextIds)))
          .getOrElse(ws.getObjects(objectClass, ws.getContextIds).asScala)
          .map(v => ValuedObj.parse(v))
          .map(v => Json.toJson(v)).toList

        Ok(Json.obj(
          "workspace" -> ws.getIdentity.toASCIIString,
          "objects" -> Json.toJson(objects)))}).getOrElse(BadRequest("Workspace must be specified."))
  }
}
