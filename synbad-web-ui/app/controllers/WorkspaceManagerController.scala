package controllers

import java.net.URI
import java.util.stream.Collectors

import controllers.models.synbad.api.SBSynbadContext
import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._

import scala.util.Try

@Singleton
class WorkspaceManagerController @Inject()(cc: ControllerComponents, ctx: SBSynbadContext) extends AbstractController(cc) {

  def getWorkspace = Action { request =>
    val json = request.body.asJson.get
    (json \ "url").asOpt[String]
      .flatMap(s => Try(URI.create(s)).toOption)
      .map(uri => ctx.getWorkspaceManager().getWorkspace(uri))
      .map(ws => Ok(Json.obj(
        "workspace" -> ws.getIdentity.toASCIIString,
        "contextIds" -> Json.toJson(ws.getContextIds.map(_.toASCIIString))
      ))).getOrElse( BadRequest("Could not retrieve workspace"))
  }

  def listWorkspaces = Action {
     val list:Array[String] = ctx.getWorkspaceManager().listWorkspaces().stream
       .map[String]( _.toASCIIString )
       .collect(Collectors.toSet())
       .toArray(new Array[String](0))
     Ok(Json.obj("content" -> Json.toJson(list)))
  }
}
