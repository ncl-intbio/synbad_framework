#!/bin/bash

until $(curl --header "Content-type: application/json" --request POST --data '{"url":"http://www.owengilfellon.com/testWorkspace"}' http://localhost:9001/api/ws/get); do
    printf '.'
    sleep 5
done;

until $(curl --header "Content-type: application/json" --request POST --data '{"workspace":"http://www.owengilfellon.com/testWorkspace", "id":"http://tardis.wikia.com/wiki/Special:URIResolver/Iris_Wildthyme", "count":5}' http://localhost:9001/api/crawler/crawl); do
    printf '.'
    sleep 5
done;

until $(curl --header "Content-type: application/json" --request POST --data '{"workspace":"http://www.owengilfellon.com/testWorkspace"}' http://localhost:9001/api/obj/get); do
    printf '.'
    sleep 5
done;

until $(curl --header "Content-type: application/json" --request POST --data '{"workspace":"http://www.owengilfellon.com/testWorkspace"}' http://localhost:9001/api/example/subtilinreceiver); do
    printf '.'
    sleep 5
done;

until $(curl --header "Content-type: application/json" --request POST --data '{"workspace":"http://www.owengilfellon.com/testWorkspace"}' http://localhost:9001/api/example/autoregulation); do
    printf '.'
    sleep 5
done;

until $(curl --header "Content-type: application/json" --request POST --data '{"modelId":0}' http://localhost:9001/api/view/create); do
    printf '.'
    sleep 5
done;

until $(curl --header "Content-type: application/json" --request POST --data '{"modelId":0, "viewId":2}' http://localhost:9001/api/view/get); do
    printf '.'
    sleep 5
done;

