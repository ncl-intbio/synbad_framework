name := """synbad-web-ui"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava).settings(
  watchSources ++= (baseDirectory.value / "public/ui" ** "*").get
)

resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += "com.h2database" % "h2" % "1.4.196"
libraryDependencies += "com.lightbend.play" %% "play-socket-io" % "1.0.0-beta-2"
libraryDependencies ++= Seq(
  "org.apache.jena" % "jena-base" % "3.8.0",
  "org.apache.jena" % "jena-core" % "3.8.0",
  "org.apache.jena" % "jena-iri" % "3.8.0",
  "org.apache.jena" % "jena-arq" % "3.8.0",
  "org.apache.jena" % "jena-tdb" % "3.8.0",
  "org.apache.jena" % "jena-shaded-guava" % "3.8.0"
)