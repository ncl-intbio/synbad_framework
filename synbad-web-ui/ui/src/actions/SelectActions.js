export const ADD_SELECTION = "ADD_SELECTION"
export const REMOVE_SELECTION = "REMOVE_SELECTION"
export const SET_SELECTION = "SET_SELECTION"

export function addToSelection(objects) {
    return {
        type: ADD_SELECTION,
        selection: objects
    }
}

export function setSelection(objects) {
    return {
        type: SET_SELECTION,
        selection: objects
    }
}

export function removeFromSelection(objects) {
    return {
        type: REMOVE_SELECTION,
        selection: objects
    }
}