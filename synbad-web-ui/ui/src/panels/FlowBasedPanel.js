import React, {Component} from 'react';
import FlowBasedEntity from '../components/FlowBasedEntity'
import ValueObject from '../components/ValueObject'

class FlowBasedPanel extends Component {

  constructor(props) {
    super(props);
    this.getObjForNode.bind(this);
    this.state = { entities: []};
    this.retrieveView();
  }

  // retrieve the view specified in props

  async retrieveView() {
    fetch('/api/view/get', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        credentials: "same-origin"
      },
      body: JSON.stringify({
        "modelId": this.props.modelId,
        "viewId": this.props.viewId
      })
    })
    .then(data => data.json())
    .then(response => {
      this.setState( { view: response.view });
      return response.view;
    })
    .then(view => {
      let nodes = view.nodes.map(async (node) => {
        let entity = await this.getObjForNode(node);
        this.setState( { entities: [...this.state.entities, entity] } )
      });
    })
  }

  // map the nodes in the view to retrieved objects

  async getObjForNode(node) {
    return fetch('/api/obj/get', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        credentials: "same-origin"
      },
      body: JSON.stringify({
        "workspace": node.workspaceId,
        "type": node.objectType,
        "identity": node.objectId
      })
    })
    .then(data => data.json())
    .then((o) => <FlowBasedEntity displayId={o.objects[0].values["http://sbols.org/v2#displayId"]}/>);
  }

  // render
  
  render() {
    if(!!this.state && !!this.state.entities ) {
      return (<div>{this.state.entities}</div>);
    } else {
      return (<div>Loading....</div>);
    }
  }
}

export default FlowBasedPanel;