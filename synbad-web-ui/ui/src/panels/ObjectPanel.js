import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ValueObject from '../components/ValueObject'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Provider } from 'react-redux';

const styles = theme => ({
  card: {
    minWidth: 275,
    maxWidth: 500,
    backgroundColor: '#eee',
    margin:'1em',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: '1.5em',
  },
  pos: {
    marginBottom: 12,
  },
})



class ObjectPanel extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.getObjects();
  }

  
  async componentDidMount() {
    
  }

  getObjects() {

    fetch('/api/obj/get', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        credentials: "same-origin"
      },
      body: JSON.stringify({
        "workspace": this.props.workspace,
        ...this.props.objecttype && { "type": this.props.objecttype }
      })
    })
    .then(data => data.json())
    .then(response => {
        let listobjs = response.objects.map((obj) => {
            let displayId = !!obj["values"]["http://www.w3.org/2000/01/rdf-schema#label"] ? 
              obj["values"]["http://www.w3.org/2000/01/rdf-schema#label"] : obj["values"]["http://sbols.org/v2#displayId"];
            return <ValueObject displayId={displayId} object={obj}/>;
          }
        )

        this.setState({objects: listobjs}) ;
    })
    .catch(function(e) {
        console.log(e);
    });
  }

  render() {
    return (
        <div className="object-panel">
          {this.state.objects}
        </div>
    );
  }
}

ObjectPanel.propTypes = {
  objects: PropTypes.array.isRequired,
  workspace: PropTypes.string.isRequired,
  objecttype: PropTypes.string.isRequired
};

export default withStyles(styles)(ObjectPanel);
