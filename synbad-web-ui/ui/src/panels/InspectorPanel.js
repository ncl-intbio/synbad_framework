import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ValueObject from '../components/ValueObject'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Provider } from 'react-redux';

import { connect } from 'react-redux';


import TextField from '@material-ui/core/TextField';



class InternalInspectorPanel extends React.Component {

  constructor(props) {
    super(props);
  }

  getDisplayId() {
    if(this.props.selection.length > 0) {
     
    var displayName = !!this.props.selection[0]["values"]["http://sbols.org/v2#displayId"] 
    ? this.props.selection[0]["values"]["http://sbols.org/v2#displayId"] 
    : this.props.selection[0]["values"]["http://www.w3.org/2000/01/rdf-schema#label"];

    var version = !!this.props.selection[0]["values"]["http://sbols.org/v2#version"] 
    ? this.props.selection[0]["values"]["http://sbols.org/v2#version"]
    : 1.0;

    var type = !!this.props.selection[0]["values"]["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"] 
    ? this.props.selection[0]["values"]["http://www.w3.org/1999/02/22-rdf-syntax-ns#type"]
    : "";

    var ext = !!this.props.selection[0]["values"]["http://www.synbad.org/extends"] 
    ? this.props.selection[0]["values"]["http://www.synbad.org/extends"]
    : "";

      return (<form className="inspector-fields" onSubmit={this.props.onSubmit}><TextField
        id="displayid"
        label="Display ID"
        //className={classes.textField}
        className={"display-id-field"}
        value={displayName}
        onChange={this.props.onChange}
        margin="normal"
      />
      <TextField
        id="version"
        label="Version"
        className={"version-field"}
        value={version}
        onChange={this.props.onChange}
        margin="normal"
      />
      <TextField
        id="type"
        label="Type"
        className={"type-field"}
        value={type}
        onChange={this.props.onChange}
        margin="normal"
      />
      <TextField
        id="extends"
        label="Extends"
        className={"extends-field"}
        value={ext}
        onChange={this.props.onChange}
        margin="normal"
      />
      <input type="submit" value="Submit" />
      </form>);
 }
    else return "";

  }

  
  render() {
    return (
        <div className="inspector-panel">
          {this.getDisplayId()}
        </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    selection: state.selection
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
   return { 
     onChange: (event) => { console.log(event); },
     onSubmit: (event) => { console.log("SUBMIT! " + event); }
    };
  

}

const InspectorPanel = connect(mapStateToProps, mapDispatchToProps)(InternalInspectorPanel);


export default InspectorPanel;
