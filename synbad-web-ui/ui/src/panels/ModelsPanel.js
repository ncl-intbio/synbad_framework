import React, {Component} from 'react';
import FlowBasedEntity from '../components/FlowBasedEntity'
import ValueObject from '../components/ValueObject'

class ModelsPanel extends Component {

  constructor(props) {
    super(props);
    this.getObjForNode.bind(this);
    this.state = { entities: []};
    this.retrieveModels();
  }

  // retrieve the view specified in props

  async retrieveModels  () {
    fetch('/api/model/get', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        credentials: "same-origin"
      },
      body: "{}"
    })
    .then(data => data.json())
    .then(response => {
      this.setState( { models: response.models });
      return response.models;
    })
    .then(models => {
      let nodes = models.map(async (model) => {
        let entity = await this.getObjForNode(model.rootNode);
        this.setState( { entities: [...this.state.entities, entity] } )
      });
    })
  }

  // map the nodes in the view to retrieved objects

  async getObjForNode(node) {
    return fetch('/api/obj/get', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        credentials: "same-origin"
      },
      body: JSON.stringify({
        "workspace": node.workspaceId,
        "type": node.objectType,
        "identity": node.identity
      })
    })
    .then(data => data.json())
    .then((o) => <FlowBasedEntity displayId={o.objects[0].values["http://sbols.org/v2#displayId"]}/>);
  }

  // render
  
  render() {
    if(!!this.state && !!this.state.entities ) {
      return (<div>{this.state.entities}</div>);
    } else {
      return (<div>Loading....</div>);
    }
  }
}

export default ModelsPanel;