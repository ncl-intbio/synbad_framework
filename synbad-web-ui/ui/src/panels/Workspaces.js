import React, {Component} from 'react';

import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import { withStyles } from '@material-ui/core/styles';
import WorkIcon from '@material-ui/icons/Work';


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
   // backgroundColor: theme.palette.background.paper,
  },
});

class Workspaces extends Component {
  constructor(props) {
    super(props);
    this.state = { workspace: '' };
    this.getWorkspace()
  }

  async componentDidMount() {
  }

  getWorkspace() {

    const { classes } = this.props;

    return fetch('/api/ws/list', {
        accept: "application/json"
      })
    .then(data => data.json())
    .then(response => {
        let listitems = response.content.map((ws) =>
        <ListItem>
          <Avatar>
            <WorkIcon />
          </Avatar> 
          <ListItemText primary={ ws } secondary="Hello!" />
        </ListItem>
       )

        this.setState( { workspace: 
          <List className={classes.root}>
            { listitems }
          </List> } )
     })
    .catch(function(e) {
        console.log(e);
    });
  }

  parseJSON(response) {
    return response.json();
  }

  render() {
    return (
        <div className="workspaces">
          {this.state.workspace}
        </div>
    );
  }
}

export default withStyles(styles)(Workspaces);