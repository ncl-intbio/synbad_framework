import React, {Component} from 'react';

class SimplePanel extends Component {

  constructor(props) {
    super(props);
  }
  
  render() {
    return (
        <div className="object-panel">
          Hello World!
        </div>
    );
  }
}

export default SimplePanel;

