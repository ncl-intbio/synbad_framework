import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
// import ActionTypes from '../messaging/constants';

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
};

class SBDrawer extends React.Component {

  constructor(props) {
    super(props);
    // Dispatcher.register(this._registerToActions.bind(this));
    this.state = {
      open: props.open
    }
  }

  // _registerToActions(action) {
  //   switch(action.actionType) {
  //       case ActionTypes.TOGGLE_DRAWER:
  //           this._toggleDrawer(!this.state.open);
  //       break;
  //   }
  // }

  _toggleDrawer(openState) {
    if(this.state.open !== openState)
      this.setState({open: openState});
  }

  render() {
    const { classes } = this.props;

    const sideList = (
      <div className={classes.list}>
        <List>Hello</List>
        <Divider />
        <List>World</List>
      </div>
    );

    return (
      <div>
      <Drawer open={this.state.open} onClose={() => this._toggleDrawer(false)}>
          <div tabIndex={0} role="button" 
          onClick={() => this._toggleDrawer(false)} onKeyDown={() => this._toggleDrawer(false)}>
            {sideList}
          </div>
        </Drawer> 
      </div>
    );
  }
}

SBDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SBDrawer);