import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

// import ActionTypes from '../messaging/constants';
// import Dispatcher from '../messaging/Dispatcher';

const styles = {
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -18,
    marginRight: 10,
  },
};


class SBAppBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  doToggle() {
    // Dispatcher.dispatch({
    //     actionType: ActionTypes.TOGGLE_DRAWER
    // });
    this.setState({open: !this.state.open});
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar variant="dense">
           <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.doToggle.bind(this)}>
              <MenuIcon/>
           </IconButton> 
            <Typography variant="title" color="inherit">
              SynBad
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

SBAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SBAppBar);