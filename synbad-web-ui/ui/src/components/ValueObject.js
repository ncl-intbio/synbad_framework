import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { setSelection } from '../actions/SelectActions'
import promoterSvg from '../images/sbolv/promoter-specification.svg';
import rbsSvg from '../images/sbolv/ribosome-entry-site-specification.svg';
import cdsSvg from '../images/sbolv/cds.svg';
import terminatorSvg from '../images/sbolv/terminator-specification.svg';


class BaseObject extends Component {

  constructor(props) {
    super(props);
    this.isSelected.bind(this);
  }

 isSelected() {
    for(var i = 0; i < this.props.selection.length; i++) {
      if(this.props.selection[i]["identity"] == this.props.object.identity)
        return true;
    }
   return false;
  }

  render() {
    return (
        <div className={"valued-object" + (this.isSelected() ? " selected" : "")} onClick={this.props.onClick} >
          <img src={promoterSvg} width="40px" height="40px"/>
          <div className="title">{this.props.displayId }</div>
        </div>
    );
  }
}

BaseObject.propTypes = {
  object: PropTypes.object.isRequired,
  selection: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  
  return {
    selection: state.selection
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {

  return {
    onClick: () => {
      dispatch(setSelection( ownProps.object ));
    }
  }
};

const ValueObject = connect(mapStateToProps, mapDispatchToProps)(BaseObject);

export default ValueObject;

