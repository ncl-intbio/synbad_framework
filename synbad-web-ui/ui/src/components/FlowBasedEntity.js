import React, {Component} from 'react';

import promoterSvg from '../images/sbolv/promoter-specification.svg';
import rbsSvg from '../images/sbolv/ribosome-entry-site-specification.svg';
import cdsSvg from '../images/sbolv/cds.svg';
import terminatorSvg from '../images/sbolv/terminator-specification.svg';

class FlowBasedEntity extends Component {

  constructor(props) {
    super(props);
    this.state = {displayId: props.displayId}
  }
  
  render() {
    return (
      <div className="valued-object">
        <img src={promoterSvg} width="40px" height="40px"/>
        <div className="title">{this.state.displayId}</div>
      </div>
    );
  }
}

export default FlowBasedEntity;

