import React from 'react';
import ReactDOM from 'react-dom';
import 'typeface-roboto';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import synbadApp from './application/StateReducer';

const store = createStore(synbadApp);


function wrapComponent(Component) {
    class Wrapped extends React.Component {
        render() {
            return (
                <Provider store={store}>
                    <Component {...this.props}/>
                </Provider>
            );
        }
    }
    return Wrapped;
}




const WrappedApp = wrapComponent(App)

ReactDOM.render(<WrappedApp/>, document.getElementById('root'));
registerServiceWorker();
