import {combineReducers} from 'redux';
import { ADD_SELECTION, SET_SELECTION, REMOVE_SELECTION } from '../actions/SelectActions';

function selection(state = [], action) {
    switch(action.type) {
        case ADD_SELECTION:
            return [ 
                ...state, 
                ...action.selection
            ]
        case SET_SELECTION:
            return [ 
                action.selection
            ]
        case REMOVE_SELECTION:
            return [ 
                state.filter(selected => !action.indexes.includes(selected))
            ]
        default:
            return state;
    }
}


const synbadApp = combineReducers(
    {
        selection
    }
)

export default synbadApp