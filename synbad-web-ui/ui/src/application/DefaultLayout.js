
// const defStack = {
//     type: 'stack',
//     content:[{
//       type: 'react-component',
//       component: 'objects',
//       componentState: { label: 'Module Definitions' },
//       props: {
//         objects: [],
//         workspace: "http://www.owengilfellon.com/testWorkspace",
//         objecttype: "http://sbols.org/v2#ModuleDefinition"
//       },
//       title: "Module Definitions"
//   },{
//     type: 'react-component',
//     component: 'objects',
//     componentState: { label: 'Component Definitions' },
//     props: {
//       objects: [],
//       workspace: "http://www.owengilfellon.com/testWorkspace",
//       objecttype: "http://sbols.org/v2#ComponentDefinition"
//     },
//     title: "Component Definitions"
//     }]
//   };
  
  // const insStack = { 
  //   type: 'stack',
  //   content:[{
  //     type: 'react-component',
  //     component: 'objects',
  //     componentState: { label: 'Subject Objects' },
  //     props: {
  //       objects: [],
  //       workspace: "http://www.owengilfellon.com/testWorkspace",
  //       objecttype: "http://semantic-mediawiki.org/swivt/1.0#Subject"
  //     },
  //     title: "Subject Objects"
  //   },{
  //     type: 'react-component',
  //     component: 'objects',
  //     componentState: { label: 'Functional Components' },
  //     props: {
  //       objects: [],
  //       workspace: "http://www.owengilfellon.com/testWorkspace",
  //       objecttype: "http://sbols.org/v2#FunctionalComponent"
  //     },
  //     title: "Functional Components"
  //   },{
  //     type: 'react-component',
  //     component: 'objects',
  //     componentState: { label: 'Modules' },
  //     props: {
  //       objects: [],
  //       workspace: "http://www.owengilfellon.com/testWorkspace",
  //       objecttype: "http://sbols.org/v2#Module"
  //     },
  //     title: "Modules"
  //   }]
  // };
  
  const layout = {
    content: [{
        type: 'row',
        content:[{
            type: 'react-component',
            component: 'workspaces',
            componentState: { label: 'Workspaces' },
            title: "Workspaces",
            isClosable: false,
            hasHeaders: false
        }
      //   ,{
      //     type: 'react-component',
      //     component: 'flowpanel',
      //     componentState: { label: 'Flow Based Panel' },
      //     title: "Flow Based Panel",
      //     props: {
      //       modelId: 0,
      //       viewId: 2
      //     }
      //   },{
      //     type: 'react-component',
      //     component: 'models-panel',
      //     componentState: { label: 'Models' },
      //     title: "Models"
      //   },{
      //     type: 'column',
      //     content:[defStack, insStack]
      // },{
      //   type: 'react-component',
      //   component: 'inspector',
      //   componentState: { label: 'Inspector' },
      //   title: "Inspector"
      // }
    ]
    }]
  };


  

  export default layout