import React, {Component} from 'react';

import GoldenLayout from "golden-layout";
import ReactDOM from 'react-dom';
import AppBar from "./components/SBAppBar"
import Drawer from "./components/SBDrawer"
import Workspaces from "./panels/Workspaces"
import ObjectPanel from "./panels/ObjectPanel"
import FlowBasedPanel from "./panels/FlowBasedPanel"
import ModelsPanel from "./panels/ModelsPanel"
import InspectorPanel from "./panels/InspectorPanel"
import SimplePanel from "./panels/SimplePanel"
import './App.css';
import SBDrawer from './components/SBDrawer';
import DefaultLayout from './application/DefaultLayout';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import synbadApp from './application/StateReducer';

const store = createStore(synbadApp);

window.React = React;
window.ReactDOM = ReactDOM;

class App extends Component {

  layout = null;

  constructor(props) {
    super(props);
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.state = {
      title: '', 
      drawerOpen: false
    };
    this._initialiseLayout();
  }

  wrapComponent(Component) {
    class Wrapped extends React.Component {
      render() {
        return (
            <Provider store={store}>
                <Component {...this.props}/>
            </Provider>
        );
      }
    }
    return Wrapped;
  };

  _initialiseLayout() {
    this.layout = new GoldenLayout( DefaultLayout, document.getElementById('root'));
    this.layout.registerComponent( 'workspaces',  this.wrapComponent(Workspaces));
    this.layout.registerComponent( 'objects',  this.wrapComponent(ObjectPanel));
    this.layout.registerComponent( 'flowpanel',  this.wrapComponent(FlowBasedPanel));
    this.layout.registerComponent( 'models-panel', this.wrapComponent(ModelsPanel));
    this.layout.registerComponent( 'inspector', this.wrapComponent(InspectorPanel));
    this.layout.init();
  }

  updateDimensions() {
      let update_width  = window.innerWidth;
      let update_height = window.innerHeight;
      this.layout.updateSize(update_width, update_height);
  }

  async componentDidMount() {
    window.addEventListener("resize", this.updateDimensions.bind(this));
  }

  async componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions.bind(this));
  }

  toggleDrawer(open) {
    this.setState({
      drawerOpen: open
    });
  };

  render() {
    return (
      <div id="menus">
        <SBDrawer/>
        <AppBar/> 
      </div>
    );
  }
}

export default App;