/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.icos.synbad.example.svp.model;

import java.net.URI;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ncl.icos.synbad.api.actions.SBAction;
import uk.ac.ncl.icos.synbad.api.domain.SBIdentity;
import uk.ac.ncl.icos.synbad.api.workspace.SBIdentityFactory;
import uk.ac.ncl.icos.synbad.api.workspace.SBWorkspace;
import uk.ac.ncl.icos.synbad.datadefinition.types.ComponentType;
import uk.ac.ncl.icos.synbad.model.SBModelManager;
import uk.ac.ncl.icos.synbad.svp.actions.SvpIdentityHelper;
import uk.ac.ncl.icos.synbad.svp.actions.builders.CelloActionBuilder;
import uk.ac.ncl.icos.synbad.svp.actions.builders.SBCelloBuilder;
import uk.ac.ncl.icos.synbad.svp.datadef.SynBadPortState;
import uk.ac.ncl.icos.synbad.svp.impl.SvpModel;
import uk.ac.ncl.icos.synbad.svp.obj.SvpModule;
import uk.ac.ncl.icos.synbad.svp.view.SvpPView;
import uk.ac.ncl.icos.synbad.workspace.SBWorkspaceManager;

/**
 *
 * @author owen
 */
public class AutoRegulationModel {

    private final static Logger LOGGER = LoggerFactory.getLogger(AutoRegulationModel.class);
    
    private final static URI WORKSPACE_ID = URI.create("http://www.synbad.org/updatertests/1.0");
    private final static URI[] CONTEXTS = new URI[] { URI.create("http://www.synbad.org/updatertests/1.0") };
    
    private final static String PREFIX = "http://www.synbad.org";
    private final static String VERSION = "1.0";

    public static void main(String[] args) {
        new AutoRegulationModel().setupModel();
    }
    
    public void setupModel() {
        
        LOGGER.info("Creating Auto-regulation SVP model");

        // Retrieve Workspace Manager and create workspace
        
        LOGGER.debug("Retrieving workspace...");
        
        SBWorkspaceManager m = Lookup.getDefault().lookup(SBWorkspaceManager.class);
        SBWorkspace ws = m.getWorkspace(WORKSPACE_ID);
        
        // TO-DO: Is this needed?
        
        ws.createContext(CONTEXTS[0]);
        
        LOGGER.debug("Constructing identities...");
    
        // Retrieve an identity factory to assist with identity creation
 
        SBIdentityFactory f = ws.getIdentityFactory();
        
        // Create identities for model entities
        
        SBIdentity rootIdentity = f.getIdentity(PREFIX, "root", VERSION);
        SBIdentity generator = f.getIdentity(PREFIX, "generator", VERSION);
        SBIdentity sink = f.getIdentity(PREFIX, "sink", VERSION);
        SBIdentity proX = f.getIdentity(PREFIX, "pX", VERSION);
        SBIdentity cdsA = f.getIdentity(PREFIX, "A", VERSION);
        SBIdentity actId = SvpIdentityHelper.createActivationIdentity(cdsA, ComponentType.Protein, SynBadPortState.Default, proX);
        
        LOGGER.debug("Constructing action...");
        
        // Use the CelloActionBuilder to construct an SVP model using modules
        
        SBCelloBuilder b = new CelloActionBuilder(ws, CONTEXTS)
                
            // Create a root module for containing the other modules
                
            .createSvm(rootIdentity)
                
            // Create a PoPS generator definition, specifying the promoter SVP identity
                
            .createGenerator(generator)
                .setPromoterIdentity(proX).builder()
                
            // Create a PoPS sink definition, specifying the CDS SVP identity
                
            .createSink(sink)
                .setCdsIdentity(cdsA).builder()
                
            // Add the PoPS generator and sink to the root module
                
            .addSvm(rootIdentity, generator, null)
            .addSvm(rootIdentity, sink, null)
                
            // Create an activation interaction between the CDS and Promoter SVPs
                
            .createActivateUnit(actId)
                .setModulatorOwner(sink)
                .setModulator(cdsA)
                .setModulated(proX).builder();
        
        // Build the action and perform
        
        SBAction action = b.build();
        
        LOGGER.info("Performing action...");
        
        ws.perform(action);
        
        // Retrieve the root module from the workspace

        SvpModule rootSvm = ws.getObject(rootIdentity.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModule generatorSvm = ws.getObject(generator.getIdentity(), SvpModule.class, CONTEXTS);
        SvpModule sinkSvm = ws.getObject(sink.getIdentity(), SvpModule.class, CONTEXTS);
        
        debugObject(rootSvm);
        debugObject(generatorSvm);
        debugObject(sinkSvm);
        
        LOGGER.info("Retrieving model...");

        // Retrieve the model factory and construct an SVP Model from the root module.
        
        SBModelManager mf = Lookup.getDefault().lookup(SBModelManager.class);
        SvpModel model = mf.createModel(SvpModel.class, rootSvm);
        
        LOGGER.debug("Retrieving view...");
        
        // Retrieve view from model
        
        SvpPView view = model.getMainView();
        
        // Close the model and the workspace to free resources
        
        LOGGER.info("Closing model and workspace...");
        
        model.close();
        m.closeWorkspace(WORKSPACE_ID);
        
    }
    
    public void debugObject(SvpModule svm) {

        String interactions = svm.getSviInstances().stream().map(i -> i.getName()).reduce((String t, String u) -> t +", " + u).orElse("");
        String children = svm.getOrderedChildren().stream().map(i -> i.getName()).reduce((String t, String u) -> t +", " + u).orElse("");   
        String port = svm.getPorts().stream().map(i -> i.getName()).reduce((String t, String u) -> t +", " + u).orElse("");
        String dnadef = svm.getDnaDefinition().getDisplayId();
      
        LOGGER.info("SVM: {}", svm.getDisplayId());
        LOGGER.info("\tChildren: {}", children);
        LOGGER.info("\tPort: {}", port);
        LOGGER.info("\tInteractions: {}", interactions);
        LOGGER.info("\tDNA Definition: {}", dnadef);

    }
}
