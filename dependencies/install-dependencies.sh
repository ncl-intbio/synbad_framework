#!/bin/bash

mvn install:install-file -Dfile=http-util-1.0.4.jar -DgroupId=uk.ac.ncl.intbio -DartifactId=http-util -Dversion=1.0.4 -Dpackaging=jar
mvn install:install-file -Dfile=io-util-1.0.4.jar -DgroupId=uk.ac.ncl.intbio -DartifactId=io-util -Dversion=1.0.4 -Dpackaging=jar
mvn install:install-file -Dfile=virtualparts-entity-1.0.5.jar -DgroupId=uk.ac.ncl.intbio -DartifactId=virtualparts-entity -Dversion=1.0.5 -Dpackaging=jar
mvn install:install-file -Dfile=virtualparts-model-1.0.5.jar -DgroupId=uk.ac.ncl.intbio -DartifactId=virtualparts-model -Dversion=1.0.5 -Dpackaging=jar
mvn install:install-file -Dfile=virtualparts-JParts-1.0.5-withDependencies.jar -DgroupId=uk.ac.ncl.intbio -DartifactId=virtualparts-JParts -Dversion=1.0.5 -Dpackaging=jar